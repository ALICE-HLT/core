#ifndef ALI_HLT_SHUTTLE_DB_CONNECTOR_HPP
#define ALI_HLT_SHUTTLE_DB_CONNECTOR_HPP

#include "AliHLTCalibrationObject.hpp"

#include <mysql++.h>
#include <string>


/**
 * Class that represents the connection to the Alice HLT Shuttle portal
 * MySQL DB.
 * This class covers the initialization (connecting to the DB) and
 * storing of a calibration meta data in the database.
 *
 * @date 23.04.2007
 *
 * @author Sebastian Bablok <Sebastian.Bablok@uib.no>
 */
class AliHLTShuttleDBConnector {
	public:
		/**
		 * Standard C-Tor for the Shuttle DB Connector.
		 * When calling this constructor, the connector is not initialized
		 * during object creation. You have to call init afterwards, before
		 * you can use the connector.
		 */
		AliHLTShuttleDBConnector();

		/**
		 * Constructor, which is also able to intialize the the connector.
		 *
		 * @param db name of the database
		 * @param host hostname or IP of MySQL DB server
		 * @param user user to connect to the database
		 * @param passwd the password that shall be used.
		 *
		 * @throw the constructor can throw an exception, if the initialization
		 *			can not be fullfilled ()
		 */
		AliHLTShuttleDBConnector(std::string db, std::string host,
				std::string user, std::string passwd);

		/**
		 * Destructor for the FES connector.
		 */
		~AliHLTShuttleDBConnector();


		/**
		 * Function to test if connector has an established connection to the
		 * DB.
		 *
		 * @return true if connected, else false.
		 */
		bool isConnected();

		/**
		 * Function to connect to a MySQL DB according to the handed in parameters.
		 *
		 * @param db name of the database
		 * @param host hostname or IP of MySQL DB server
		 * @param user user to connect to the database
		 * @param passwd the password that shall be used.
		 *
		 * @return ALIHLT_OK if connecting was successful, else an error code
		 *			is returned (@see AliHLTShuttlePortalDefines.h).
		 */
		int connect(std::string db, std::string host, std::string user,
				std::string passwd);

		/**
		 * Function to disconnect from the current MySQL database and server.
		 */
		void disconnect();

		/**
		 * Function to set a new entry (row) in the database. This sets the 
		 * complete row, even some fields are left empty.
		 *
		 * @param calibObject calibration object containing all required
		 *			information for the entry
		 *			(must be in state ALIHLT_RECEIVED)
		 *
		 * @return ALIHLT_OK in case of success, else an error code
		 */
		int setEntry(AliHLTCalibrationObject& calibObject);

		/**
		 * Function to update an entry (row) in the database. This updates only
		 * the fields for time_created, fileSize, checksum and versionNumber. 
		 * The row to update is defined by the unique key given by the calib
		 * object.
		 *
		 * @param calibObject calibration object that contains all required
		 *			information for updating the entry
		 *			(should be in state ALIHLT_FES_FILLED)
		 *
		 * @return ALIHLT_OK in case of success, else an error code
		 */
		int updateEntry(AliHLTCalibrationObject& calibObject);

		/**
		 * Function to remove an entry (row) from the DB
		 *
		 * @param calibObject calibration object refering to the entry that
		 *			shall be removed
		 *
		 * @return ALIHLT_OK in case of success, else an error code
		 */
		int removeEntry(AliHLTCalibrationObject& calibObject);

// since verion 0.2: change in storing new versions of same object
// now a link with calibration object name points to the latest version
// this has to be tested first and included/updated in the DB as well

		/**
		 * Function to test if calib object already exists and retrieve its
		 * latest version number. If the calib object has no corresponding 
		 * entry in the DB, a new one is included to the DB. The latest 
		 * version number is automatically updated in the calib object 
		 * before leaving the function.
		 *
		 * @param calibObject calibration object containing all required
		 *			information for the testing
		 *			(must be in state ALIHLT_RECEIVED)
		 *
		 * @return ALIHLT_OK in case of success, else an error code
		 */
		int testEntry(AliHLTCalibrationObject& calibObject);


	private:

		/**
		 * Member indicating, if connector is initialized.
		 */
		bool mConnected;

		/**
		 * Pointer to the MySQL++ connection for the database.
		 */
		mysqlpp::Connection mCon;

		/**
		 * Database to which the connector is/shall be connected
		 */
		std::string mDb;

		/**
		 * Host of DB
		 */
		std::string mHost;

		/**
		 * User to connect to DB.
		 */
		std::string mUser;

		/**
		 * Password to connect to DB.
		 */
		std::string mPasswd;



};

inline bool AliHLTShuttleDBConnector::isConnected() {
	return mConnected;
}


#endif // ALI_HLT_SHUTTLE_DB_CONNECTOR_HPP
