#include "AliHLTShuttlePortal.hpp"

#include "AliHLTShuttlePortalDefines.h"

using namespace std;

/*
AliHLTShuttlePortal::AliHLTShuttlePortal(string fesBasePath, string db,
		string host, string user, string passwd) {


}
*/

AliHLTShuttlePortal::~AliHLTShuttlePortal() {

}


bool AliHLTShuttlePortal::initStoreMutex() {
	return ((pthread_mutex_init(&mStoreMutex, NULL) == 0) ? true : false);
}


bool AliHLTShuttlePortal::deinitStoreMutex() {
	return ((pthread_mutex_destroy(&mStoreMutex) == 0) ? true : false);
}


int AliHLTShuttlePortal::init(string fesBasePath, string db, string host,
		string user, string passwd) {
	int nRet = ALIHLT_OK;

	// initialize FES connector
	nRet = mFEScon.init(fesBasePath, mpLogger);
	if (nRet != ALIHLT_OK) {
		mpLogger->logMessage(AliHLTShuttlePortalLogger::mFatal,
				"Init of FXS failed: " +
				AliHLTShuttlePortalLogger::convertReturnCode(nRet));
		return nRet;
	}

	// initialize DB connctor
	nRet = mDBcon.connect(db, host, user, passwd);
	if (nRet != ALIHLT_OK) {
		mpLogger->logMessage(AliHLTShuttlePortalLogger::mFatal,
				"MySQL connection failed: " +
				AliHLTShuttlePortalLogger::convertReturnCode(nRet));
		return nRet;
	}

	if (!initStoreMutex()) {
		mpLogger->logMessage(AliHLTShuttlePortalLogger::mFatal,
				"Inititialization of Store Mutex failed.");
		return ALIHLT_MUTEX_INIT_FAILED;		 
	}

	mIsInit = true;
	mpLogger->logMessage(AliHLTShuttlePortalLogger::mInfo,
				"Shuttle Portal initialization process: " +
				AliHLTShuttlePortalLogger::convertReturnCode(nRet));
	return nRet;
}


void AliHLTShuttlePortal::deinit() {
	mDBcon.disconnect();
	deinitStoreMutex();
	mIsInit = false;
	mpLogger->logMessage(AliHLTShuttlePortalLogger::mInfo,
			"Shuttle Portal has been deinitialized.");
}


int AliHLTShuttlePortal::storeCalibObject(AliHLTCalibrationObject& calibObject) {
	int nRet = 0;
	
	// lock mutex
	if (!lockStoreMutex()) {
		return ALIHLT_MUTEX_LOCK_FAILED;
	}
	
	nRet = doStore(calibObject);

	// unlock mutex
	if (!unlockStoreMutex()) {
		if (deinitStoreMutex()) {
			if (!initStoreMutex()) {
				mpLogger->logMessage(AliHLTShuttlePortalLogger::mError,
						"Unlock of mutex failed. Recovery via reinit failed as well.");
				nRet = ALIHLT_SHUTTLE_PORTAL_FATAL_ERROR;
			}
		} else {
			mpLogger->logMessage(AliHLTShuttlePortalLogger::mError,
						"Unlock of mutex failed. Recovery via reinit failed as well.");
			nRet = ALIHLT_SHUTTLE_PORTAL_FATAL_ERROR;
		}
	}
	
	return nRet;
}

int AliHLTShuttlePortal::doStore(AliHLTCalibrationObject& calibObject) {
	int nRet = ALIHLT_OK;

	if (!mIsInit) {
		mpLogger->logMessage(AliHLTShuttlePortalLogger::mError,
				"Shuttle Portal connectors have not been initialized, unable to store object.");
		return ALIHLT_NOT_INIT;
	}

	mpLogger->logMessage(AliHLTShuttlePortalLogger::mDebug,
			"Calibration object is in state: " +
			AliHLTShuttlePortalLogger::convertState(calibObject.getStoreStatus()));

	// test setting of calib meta data in DB using DB connector
	if (mDBcon.isConnected()) {
//		nRet = mDBcon.setEntry(calibObject);  // new procedure starts with testEntry
		nRet = mDBcon.testEntry(calibObject);
		if (calibObject.isRenamed()) {
			mpLogger->logMessage(AliHLTShuttlePortalLogger::mDebug,
					"DB Connector: Calibration fileID has been renamed to "
					+ calibObject.getFileId());
		}
		if (nRet != ALIHLT_OK) {
			mpLogger->logMessage(AliHLTShuttlePortalLogger::mError,
					"DB Connector: " +
					AliHLTShuttlePortalLogger::convertReturnCode(nRet));
#ifdef __DEBUG
			cout << "Set Entry failed ... should exit" << endl;
#endif
			return nRet;
		}

	} else {
		mpLogger->logMessage(AliHLTShuttlePortalLogger::mFatal,
				"DB Connector has no connection to MySQL DB.");
		return ALIHLT_NOT_INIT;
	}

	mpLogger->logMessage(AliHLTShuttlePortalLogger::mDebug,
			"Calibration object is in state: " +
			AliHLTShuttlePortalLogger::convertState(calibObject.getStoreStatus()));

	// store calibration file in FES via FES connector
	if (mFEScon.isInit()) {
		nRet = mFEScon.storeCalibrationBLOB(calibObject);
		if (nRet != ALIHLT_OK) {
			mpLogger->logMessage(AliHLTShuttlePortalLogger::mError,
					"FXS Connector: " +
					AliHLTShuttlePortalLogger::convertReturnCode(nRet));

			// storing failed so remove DB entry as well if this is the first version (v. 0)
			if (mDBcon.isConnected() && (calibObject.getVersionNumber() == 0)) {
				nRet = mDBcon.removeEntry(calibObject);
				if (nRet != ALIHLT_OK) {
					mpLogger->logMessage(AliHLTShuttlePortalLogger::mError,
							"DB Connector: " +
							AliHLTShuttlePortalLogger::convertReturnCode(nRet));
#ifdef __DEBUG
					cout << "Removing of DB entry failed." << endl;
#endif
				}
			} else {
				mpLogger->logMessage(AliHLTShuttlePortalLogger::mFatal,
						"DB Connector has no connection to MySQL DB.");
			}
#ifdef __DEBUG
			cout << "Storing in FES failed ... exiting" << endl;
#endif
			return nRet;
		}
		mpLogger->logMessage(AliHLTShuttlePortalLogger::mInfo,
				"Storing Calibration BLOB to FXS under " +
				mFEScon.getBasePath() + "/" + calibObject.getFilePath());
	} else {
		mpLogger->logMessage(AliHLTShuttlePortalLogger::mFatal,
				"FXS Connector has no connection to FXS.");
		return ALIHLT_NOT_INIT;
	}

	mpLogger->logMessage(AliHLTShuttlePortalLogger::mDebug,
			"Calibration object is in state: " +
			AliHLTShuttlePortalLogger::convertState(calibObject.getStoreStatus()));

	// update entry DB after storing Calibartion object in FES
	if (mDBcon.isConnected()) {
		nRet = mDBcon.updateEntry(calibObject);
		if (nRet != ALIHLT_OK) {
			mpLogger->logMessage(AliHLTShuttlePortalLogger::mError,
					"Updating of DB entry failed (entry remains without file size, timestamp, checksum and version): " +
					AliHLTShuttlePortalLogger::convertReturnCode(nRet));
#ifdef __DEBUG
			cout << "Update DB failed ... should exit" << endl;
#endif
			// But let the data remain in DB and FES, ?? Should link point to latest valid version ??
			// (only entry (fileSize, checksum, time_created, version) in DB is not correct)
			return nRet;
		}
		mpLogger->logMessage(AliHLTShuttlePortalLogger::mInfo,
				"Storing meta data to MySQL DB: " +
				AliHLTShuttlePortalLogger::convertReturnCode(nRet));
		mpLogger->logMessage(AliHLTShuttlePortalLogger::mDebug,
				"Stored meta data in MySQL DB: run(" +
				calibObject.getRunNumberString() + "), detector(" +
				calibObject.getDetector() + "), fileID(" +
				calibObject.getFileId() + "), filePath(" +
				calibObject.getFilePath() + "), ddlNumbers(" +
				calibObject.getDDLnumbers() + "), time_created(" +
				calibObject.getTimeCreatedString() + "), fileSize(" +
				calibObject.getFileSizeString() + "), checksum(" +
				calibObject.getFileChecksum() + "), version(" +
				calibObject.getVersionNumberString() + ").");
	} else {
		mpLogger->logMessage(AliHLTShuttlePortalLogger::mFatal,
				"DB Connector has no connection to MySQL DB.");
		return ALIHLT_NOT_INIT;
	}

	mpLogger->logMessage(AliHLTShuttlePortalLogger::mDebug,
			"Calibration object is in state: " +
			AliHLTShuttlePortalLogger::convertState(calibObject.getStoreStatus()));

	return nRet;
}

