#ifndef ALI_HLT_CALIB_DB_STRUCT
#define ALI_HLT_CALIB_DB_STRUCT

#ifdef __USE_SSQLS

#include <mysql++.h>
#include <custom.h>

#include <string>

// The following is calling a very complex macro which will create
// "struct AliHLTCalib_DBstruct", which represents an entry in
// ALICE HLT Shuttle MySQL DB (hlt_logbook)
//
// plus methods to help populate the class from a MySQL row.  See the
// SSQLS sections in the user manual of MySQL++ for further details.


/**
 * Macro to create a struct representing an entry in ALICE HLT Shuttle MySQL
 * DB (hlt_logbook). In addition certain functions to populate the class
 * from a MySQL row are added. For further details see MySQL++.
 */
sql_create_10(calib_data,
	5, // comparsion for equal calib_data-structs -> unique key is filePath -> 5
	10, // for init c-tor and set function
	mysqlpp::sql_bigint, run,
	mysqlpp::sql_char, detector,
	mysqlpp::sql_char, fileId,
	mysqlpp::sql_char, DDLnumbers,
	mysqlpp::sql_char, filePath,
	mysqlpp::sql_double, time_created,
	mysqlpp::sql_double, time_processed,
	mysqlpp::sql_double, time_deleted,
	mysqlpp::sql_bigint, fileSize,
	mysqlpp::sql_char, fileChecksum);


typedef calib_data AliHLTCalib_DBstruct;

#endif // __USE_SSQLS

#endif // ALI_HLT_CALIB_DB_STRUCT
