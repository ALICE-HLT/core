#include "AliHLTCalibrationObject.hpp"

#include "AliHLTShuttlePortalDefines.h"

#include <sys/time.h>
#include <time.h>
#include <cstring>


AliHLTCalibrationObject::AliHLTCalibrationObject(int runNumber, const char* detector,
		const char* fileID, const char* ddlNumbers, void* blob, int blobSize,
		int blobType) {
    mRunNumber = runNumber;
	strncpy(mDetector, detector, 3);
	mDetector[3] = 0;
	strncpy(mFileID, fileID, 127);
    mFileID[127] = 0;
    strncpy(mDDLNumbers, ddlNumbers, 63);
    mDDLNumbers[63] = 0;
    strncpy(mOrigFileID, fileID, 127);
    mOrigFileID[127] = 0;
	renameFileID(0);
	//mFilePath is set inside "renameFileID(..);"
//  mFilePath[sprintf(mFilePath, "%d/%s/%s/%s", mRunNumber, mDetector,
//			mDDLNumbers, mFileID)] = 0;
	mLinkPath[sprintf(mLinkPath, "%d/%s/%s/%s", mRunNumber, mDetector,
            mDDLNumbers, mOrigFileID)] = 0;

	mCalibrationBLOB = blob;
	mBLOBSize = blobSize;
	// TODO check for valid blob type, when they are defined
	mBLOBType = blobType;

    mTimeCreated = 0;
    mTimeProcessed = 0;
    mTimeDeleted = 0;
    mSize = 0;
    mFileChecksum[0] = 0;

    mRenamed = false;
    mStoreStatus = ALIHLT_RECEIVED;
}

AliHLTCalibrationObject::AliHLTCalibrationObject(AliHLTCalibrationObject& copyObj) {
	mRunNumber = copyObj.mRunNumber;
	memcpy(mDetector, copyObj.mDetector, 4);
	memcpy(mFileID, copyObj.mFileID, 128);
	memcpy(mOrigFileID, copyObj.mOrigFileID, 128);
	memcpy(mDDLNumbers, copyObj.mDDLNumbers, 64);
	memcpy(mFilePath, copyObj.mFilePath, 256);
	memcpy(mLinkPath, copyObj.mLinkPath, 256);
	mTimeCreated = copyObj.mTimeCreated;
	mTimeProcessed = copyObj.mTimeProcessed;
	mTimeDeleted = copyObj.mTimeDeleted;
	mSize = copyObj.mSize;
	memcpy(mFileChecksum, copyObj.mFileChecksum, 64);
	mCalibrationBLOB = copyObj.mCalibrationBLOB;
	mBLOBSize = copyObj.mBLOBSize;
	mBLOBType = copyObj.mBLOBType;
	mRenamed = copyObj.mRenamed;
	mStoreStatus = copyObj.mStoreStatus;
	mVersionNumber = copyObj.mVersionNumber;
	
#ifdef __USE_SSQLS
	mStoredCalibEntry = copyObj.mStoredCalibEntry;
#endif // __USE_SSQLS
}


AliHLTCalibrationObject::AliHLTCalibrationObject() {
 // C-Tor is disabled since it is private
}


AliHLTCalibrationObject::~AliHLTCalibrationObject() {

}


AliHLTCalibrationObject& AliHLTCalibrationObject::operator=(
			AliHLTCalibrationObject& rhs) {
	// if same address, they are already equal
	if (this == &rhs) {
		return *this;
	}
    mRunNumber = rhs.mRunNumber;
    memcpy(mDetector, rhs.mDetector, 4);
    memcpy(mFileID, rhs.mFileID, 128);
    memcpy(mOrigFileID, rhs.mOrigFileID, 128);
    memcpy(mDDLNumbers, rhs.mDDLNumbers, 64);
    memcpy(mFilePath, rhs.mFilePath, 256);
	memcpy(mLinkPath, rhs.mLinkPath, 256);
    mTimeCreated = rhs.mTimeCreated;
    mTimeProcessed = rhs.mTimeProcessed;
    mTimeDeleted = rhs.mTimeDeleted;
    mSize = rhs.mSize;
    memcpy(mFileChecksum, rhs.mFileChecksum, 64);
    mCalibrationBLOB = rhs.mCalibrationBLOB;
    mBLOBSize = rhs.mBLOBSize;
    mBLOBType = rhs.mBLOBType;
    mRenamed = rhs.mRenamed;
    mStoreStatus = rhs.mStoreStatus;
	mVersionNumber = rhs.mVersionNumber;
	
#ifdef __USE_SSQLS
	mStoredCalibEntry = rhs.mStoredCalibEntry;
#endif // __USE_SSQLS

	return *this;
}


bool AliHLTCalibrationObject::operator==(AliHLTCalibrationObject& rhs) {
	// if same address, they must be equal, else check the members

	return ((this == &rhs) || (
			(mRunNumber == rhs.mRunNumber) &&
			(strncmp(mDetector, rhs.mDetector, 4) == 0) &&
			(strncmp(mFileID, rhs.mFileID, 128) == 0) &&
			(strncmp(mOrigFileID, rhs.mOrigFileID, 128) == 0) &&
			(strncmp(mDDLNumbers, rhs.mDDLNumbers, 64) == 0) &&
			(strncmp(mFilePath, rhs.mFilePath, 256) == 0) &&
			(strncmp(mLinkPath, rhs.mLinkPath, 256) == 0) &&
			(mTimeCreated == rhs.mTimeCreated) &&
			(mTimeProcessed == rhs.mTimeProcessed) &&
			(mTimeDeleted == rhs.mTimeDeleted) &&
			(mSize == rhs.mSize) &&
			(strncmp(mFileChecksum, rhs.mFileChecksum, 64) == 0) &&
			(mCalibrationBLOB == rhs.mCalibrationBLOB) &&
			(mBLOBSize == rhs.mBLOBSize) &&
			(mBLOBType == rhs.mBLOBType) &&
			(mRenamed == rhs.mRenamed) &&
			(mStoreStatus == rhs.mStoreStatus) &&
			(mVersionNumber == rhs.mVersionNumber))) ? true : false;
}


bool AliHLTCalibrationObject::renameFileID(unsigned short num) {
	char postfix[CALIB_FILE_SIZE_OF_POSTFIX];
	unsigned short pos = 0;

	if ((num > ALIHLT_CALIB_OBJECT_MAX_VERSION) && (num > 0)) {
		return false;
	}

	postfix[sprintf(postfix, "_%d", num)] = 0;

	// check for length of fileID
	pos = strlen(mOrigFileID);
	if (pos >= (128 - CALIB_FILE_SIZE_OF_POSTFIX)) {
		pos = (127 - CALIB_FILE_SIZE_OF_POSTFIX);
	}
	strncpy((mFileID + pos), postfix, CALIB_FILE_SIZE_OF_POSTFIX);

	mFilePath[sprintf(mFilePath, "%d/%s/%s/%s", mRunNumber, mDetector,
            mDDLNumbers, mFileID)] = 0;
	mRenamed = true;
	mVersionNumber = num;
	return true;
}

bool AliHLTCalibrationObject::createTimestampCreated() {
/*
//	long long time_ll = 0;
	char buffer[32];
	FILE* commandHandler = popen("echo `date +%s`", "r");

	if (commandHandler != 0) {
		fgets(buffer, sizeof(buffer), commandHandler);
		pclose(commandHandler);

		mTimeCreated = atoll(buffer);
		return true;
	}
*/
// TODO setting of time failed -> logmessage
	return false;
}

#ifdef __USE_SSQLS
AliHLTCalib_DBstruct AliHLTCalibrationObject::createDBEntryStruct(bool store) {
	AliHLTCalib_DBstruct dbEntry(mRunNumber, mDetector, mFileID, mDDLNumbers,
			mFilePath, mTimeCreated, mTimeProcessed, mTimeDeleted, mSize,
			mFileChecksum);

	if (store) {
		mStoredCalibEntry.set(mRunNumber, mDetector, mFileID, mDDLNumbers,
			mFilePath, mTimeCreated, mTimeProcessed, mTimeDeleted, mSize,
			mFileChecksum);
	}
	return dbEntry;
}
#endif // __USE_SSQLS


/*
std::string AliHLTCalibrationObject::getFilePath() {
#ifdef __DEBUG
	std::cout << "Retrieved FilePath: " << mFilePath << std::endl;
#endif
	std::string path(mFilePath);
	return path;
}
*/
