#include "AliHLTShuttlePortalFileLogger.hpp"

#include <fstream>
#include <sstream>
#include <sys/stat.h>
#include <sys/types.h>

using namespace std;



AliHLTShuttlePortalFileLogger::AliHLTShuttlePortalFileLogger(std::string filename) {
	mLogFilename = filename;
}

AliHLTShuttlePortalFileLogger::AliHLTShuttlePortalFileLogger(std::string filename,
				AliHLTShuttlePortalLogLevel logLevel) :
				AliHLTShuttlePortalLogger(logLevel), mLogFilename(filename)  {

}


AliHLTShuttlePortalFileLogger::~AliHLTShuttlePortalFileLogger() {

}


int AliHLTShuttlePortalFileLogger::pushLogMessage(
			AliHLTShuttlePortalLogLevel logLevel, string msg) {
	int nRet = ALI_HLT_LOGGING_OK;

	ofstream file(mLogFilename.c_str(), ios::out | ios::app);
	if (file.is_open()) {
		time_t timeVal;
		struct tm* now;
		char tempDate[30];
		stringstream msgLine;

		time(&timeVal);
		now = localtime(&timeVal);
		int length = strftime(tempDate, 30, "%Y-%m-%d %H:%M:%S", now);
		// terminate with '\0'
        tempDate[length] = 0;

        msgLine << tempDate << " - [" << convertLogLevel(logLevel) << "]: " << msg << endl;

		file.write(msgLine.str().c_str(), msgLine.str().length());
		file.close();
	} else {
		nRet = ALI_HLT_LOGGING_FAILED;
	}


	return nRet;
}

int AliHLTShuttlePortalFileLogger::pushLogMessage(string msg) {
	int nRet = ALI_HLT_LOGGING_OK;

	ofstream file(mLogFilename.c_str(), ios::out | ios::app);
	if (file.is_open()) {
		time_t timeVal;
		struct tm* now;
		char tempDate[30];
		stringstream msgLine;

		time(&timeVal);
		now = localtime(&timeVal);
		int length = strftime(tempDate, 30, "%Y-%m-%d %H:%M:%S", now);
		// terminate with '\0'
        tempDate[length] = 0;

        msgLine << tempDate << " - " << msg << endl;

		file.write(msgLine.str().c_str(), msgLine.str().length());
		file.close();
	} else {
		nRet = ALI_HLT_LOGGING_FAILED;
	}


	return nRet;
}
