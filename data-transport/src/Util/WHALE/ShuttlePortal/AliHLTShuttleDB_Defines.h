#ifndef ALI_HLT_SHUTTLE_DB_DEFINES_H
#define ALI_HLT_SHUTTLE_DB_DEFINES_H


/**
 * Define for the table name of the HLT log book
 */
#define ALI_HLT_SHUTTLE_DB_TABLE "calib_data"

/**
 * Define for the row file path
 */
#define ALI_HLT_SHUTTLE_DB_FILE_PATH "filePath"

/**
 * Define for the row time_created
 */
#define ALI_HLT_SHUTTLE_DB_TIME_CREATED "time_created"

/**
 * Define for the row file size
 */
#define ALI_HLT_SHUTTLE_DB_FILE_SIZE "size"

/**
 * Define for the row file checksum
 */
#define ALI_HLT_SHUTTLE_DB_FILE_CHECKSUM "fileChecksum"

/**
 * Define for the row file version number
 */
#define ALI_HLT_SHUTTLE_DB_FILE_VERSION_NUMBER "versionNumber"

/**
 * Define for the table unique key
 */
#define ALI_HLT_SHUTTLE_DB_KEY ALI_HLT_SHUTTLE_DB_FILE_PATH

/**
 * Define for choosing one complete line of the calib_data table
 */
#define ALI_HLT_SHUTTLE_DB_COMPLETE_TABLE "calib_data(run, detector, fileId, DDLnumbers, filePath, time_created, time_processed, time_deleted, size, fileChecksum, versionNumber)"

#endif // ALI_HLT_SHUTTLE_DB_DEFINES_H
