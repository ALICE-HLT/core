#include "AliHLTFESConnector.hpp"

#include "AliHLTShuttlePortalDefines.h"
#include "AliHLTShuttleFES_Defines.h"

#include <fstream>
#include <iostream>
#include <sstream>

#include <sys/stat.h>
#include <sys/types.h>
#include <cerrno>
#include <unistd.h>
#include <cstring>

using namespace std;

AliHLTFESConnector::AliHLTFESConnector() {
	mIsInit = false;
	mBasePath = "";
	mpLogger = 0;
}

AliHLTFESConnector::AliHLTFESConnector(string fesBasePath,
			AliHLTShuttlePortalLogger* aLogger) {
	mIsInit = false;
	mBasePath = "";
	mpLogger = 0;

	if (init(fesBasePath, aLogger) < 0) {
		if (mpLogger != 0) {
			mpLogger->logMessage(AliHLTShuttlePortalLogger::mFatal,
					"Initialization of FXS connector failed.");
		}

		// TODO !!! throw exception !!!
//		throw
	}
}

AliHLTFESConnector::~AliHLTFESConnector() {

}

int AliHLTFESConnector::init(string fesBasePath,
			AliHLTShuttlePortalLogger* aLogger) {
	fstream file;

	// test if base starts in "/"
	if (fesBasePath[0] != '/') {
		return ALIHLT_FES_INVALID_BASEPATH;
	}

	// check if base path directory is reachable
	file.open((fesBasePath + "/dummy.txt").c_str(), ios::out);
	if (!file.is_open()) {
		file.close();
		return ALIHLT_FES_INVALID_BASEPATH;
	}
	file.close();
	remove((fesBasePath + "/dummy.txt").c_str());

	mBasePath = fesBasePath;
#	ifdef __DEBUG
	cout << "basePath: " << mBasePath << endl;
#	endif

	mpLogger = aLogger;
	mIsInit = true;
	return ALIHLT_OK;
}


int AliHLTFESConnector::storeCalibrationBLOB(AliHLTCalibrationObject& calibObject) {

	// fill FES with BLOB
	int nRet = ALIHLT_FES_CONNECTION_FAILED;
#if 0 // Unused variable
	int retVal = 0;
#endif
	string path;
	string linkPath;
	const char* calibData = (const char*) calibObject.getCalibrationBLOB();
	int fileSize = calibObject.getBLOBSize();
	string checksum;

	// create directory for storing file
	if (!createDir(calibObject)) {
		return ALIHLT_FES_DIRECTORY_PATH_ERROR;
	}

	// store calibration to file
	path = mBasePath + "/" + calibObject.getFilePath();
	linkPath = mBasePath + "/" + calibObject.getLinkPath();
	ofstream file(path.c_str(), ios::out | ios::binary);
	if (file.is_open()) {
		file.write(calibData, fileSize);

		// before setting new file size, checksum and timestamp, check if
		// (re-) setting of link is successfull, else stay to old settings if pos
		if (createLink(path.c_str(), linkPath.c_str(), (
					(calibObject.getVersionNumber() == 0 ? false : true))) != 0) {
#ifdef __DEBUG
			cout << "Error cannot (re-)set link to lastest file version." << endl;
#endif
			return ALIHLT_FES_SETTING_LINK_FAILED;
		}

		// get file size and store in calibObject
//		file.seekp(ios::end);
		fileSize = file.tellp();
		calibObject.setFileSize(fileSize);
		file.close();

		// get checksum and store in calibObject
		if (!calculateMD5Sum(path, &checksum)) {
			if (mpLogger != 0) {
				mpLogger->logMessage(AliHLTShuttlePortalLogger::mWarning,
						"FXS connector: " +
						AliHLTShuttlePortalLogger::convertReturnCode(
						ALIHLT_FES_CALCULATING_CHECKSUM_FAILED));
			}
#ifdef __DEBUG
			cout << "Calculating of Checksum failed" << endl;
#endif
		}
		if (!calibObject.setChecksum(checksum)) {
			// setting of checksum failed do soemthing
#ifdef __DEBUG
			cout << "Setting of Checksum in Calibration Object failed" << endl;
#endif
		}

		// set time stamp created
		if (!calibObject.createTimestampCreated()) {
			// TODO logmessage: creating double precision time stamp failed, using only long
#ifdef __DEBUG
			cout << "Using single precision timestamp." << endl;
#endif
			time_t timeLong = time(0);
			calibObject.setTimeCreated(timeLong);
			/*if (mpLogger != 0) {
				mpLogger->logMessage(AliHLTShuttlePortalLogger::mWarning,
						"FXS connector: Using only single precision timestamp for time_created.");
			}*/
		}

		calibObject.setStoreStatus(ALIHLT_FES_FILLED);
		nRet = ALIHLT_OK;
	} else {
#ifdef __DEBUG
		cout << "ERROR while opening file" << endl;
#endif
		calibObject.setStoreStatus(ALIHLT_STORAGE_ERROR);
		nRet = ALIHLT_FES_STORING_FAILED;
	}

	return nRet;
}


bool AliHLTFESConnector::calculateMD5Sum(string file, string* result) {
	string exeStr = "md5sum " + file;
	char buffer[512];
	FILE* commandHandler = popen(exeStr.c_str(), "r");

	result->clear();
	if (commandHandler != 0) {
		while (fgets(buffer, sizeof(buffer), commandHandler) != 0) {
			*result += buffer;
		}
		pclose(commandHandler);

		if (!result->empty()) {
			result->erase(result->find_first_of(' '));
			return true;
		}
	}
	return false;
}


bool AliHLTFESConnector::createDir(AliHLTCalibrationObject& calibObject) {
	int retVal;
#if 0 // Unused variable
	int error;
#endif
	stringstream path;

	path << mBasePath << "/" << calibObject.getRunNumber() << "/" <<
			calibObject.getDetector() << "/" << calibObject.getDDLnumbers();

#ifdef __DEBUG
	cout << "Testing directory path: " << path.str() << endl;
#endif
	// test, if already existing
	retVal = mkdir(path.str().c_str(), ALIHLT_FES_DIRECTORY_PERMISSION);
	if (retVal != 0) {
		// don't combine the two if's !
		if (errno == EEXIST) {
			return true;
		}
		// continue creation
	} else {
		// ok now it exists (seems that only the last directory was missing)
		return true;
	}

	// does not exist, so create it. first Run number
	path.str(""); // empty buffer
	path << mBasePath << "/" << calibObject.getRunNumber();
	retVal = mkdir(path.str().c_str(), ALIHLT_FES_DIRECTORY_PERMISSION);
	if ((retVal != 0) && (errno != EEXIST)) {
#ifdef __DEBUG
		char* errStr;
		error = errno;
		cout << "Unable to create storage path:" << path.str() << endl;
		errStr = strerror(error);
		cout << "System error[" << error << "]: " << errStr << endl;
#endif
		return false;
	}
#ifdef __DEBUG
	cout << "Created directory path: " << path.str() << endl;
#endif

	// now the detector
	path << "/" << calibObject.getDetector();

	retVal = mkdir(path.str().c_str(), ALIHLT_FES_DIRECTORY_PERMISSION);
	if ((retVal != 0) && (errno != EEXIST)) {
#ifdef __DEBUG
		char* errStr;
		cout << "Unable to create storage path:" << path.str() << endl;
		errStr = strerror(errno);
		cout << "System error[" << errno << "]: " << errStr << endl;
#endif
		return false;
	}
#ifdef __DEBUG
	cout << "Created directory path: " << path.str() << endl;
#endif

	// and finally the DDL number
	path << "/" << calibObject.getDDLnumbers();

	retVal = mkdir(path.str().c_str(), ALIHLT_FES_DIRECTORY_PERMISSION);
	if ((retVal != 0) && (errno != EEXIST)) {
#ifdef __DEBUG
		char* errStr;
		cout << "Unable to create storage path:" << path.str() << endl;
		errStr = strerror(errno);
		cout << "System error[" << errno << "]: " << errStr << endl;
#endif
		return false;
	}
#ifdef __DEBUG
	cout << "Created directory path: " << path.str() << endl;
#endif

	// so, now the complete directory should be there
	return true;
}


int AliHLTFESConnector::createLink(const char* targetName, const char* linkName,
			bool relink) {
	int retVal = 0;

	if (relink) {
		// remove old link
		retVal = unlink(linkName);
		if ((retVal != 0) && (errno != ENOENT)) {
			// unrecoverable error, exiting function
			// TODO make log entry in returned function
			return errno;
		}
		// set new link
		retVal = symlink(targetName, linkName);
		
	} else {
		// set link
		retVal = symlink(targetName, linkName);
		if ((retVal != 0) && (errno == EEXIST)) {
			retVal = createLink(targetName, linkName, true);
		}
	}
	
	return retVal;
}


/*
// For the error of creating a dir in the FES
string AliHLTFESConnector::convertErrno(int num) {
	string sRet;
	switch (num) {
		case (EPERM):
			//The filesystem containing pathname does not support the creation of directories.
			sRet = "EPERM";
			break;

		case (EEXIST):
			// pathname already exists (not necessarily as a directory).
			// This includes the case where pathname is a symbolic link, dangling or not.
			sRet = "EEXIST";
			break;

		case (EFAULT):
			// pathname points outside your accessible address space.
			sRet = "EFAULT";
			break;

		case (EACCES):
			// The parent directory does not allow write permission to the process,
			// or one of the directories in pathname did not allow search (execute) permission.
			sRet = "EACCES";
			break;

		case (ENAMETOOLONG):
			//pathname was too long.
			sRet = "ENAMETOOLONG";
			break;

		case (ENOENT):
			// A directory component in pathname does not exist or is a dangling symbolic link.
			sRet = "ENOENT";
			break;

		case (ENOTDIR):
			// A component used as a directory in pathname is not, in fact, a directory.
			sRet = "ENOTDIR";
			break;

		case (ENOMEM):
			//Insufficient kernel memory was available.
			sRet = "ENOMEM";
			break;

		case (EROFS):
			// pathname refers to a file on a read-only filesystem.
			sRet = "EROFS";
			break;

		case (ELOOP):
			// Too many symbolic links were encountered in resolving pathname.
			sRet = "ELOOP";
			break;

		case (ENOSPC):
			// The device containing pathname has no room for the new directory.
			sRet = "ENOSPC";
			break;

		case (ENOSPC):
			// The new directory cannot be created because the user's disk quota is exhausted.
			sRet = "ENOSPC";
			break;

		default:
			// Unknown error
			sRet = "UNKNOWN";
	}
	return sRet;
}
*/

