#ifndef ALI_HLT_SHUTTLE_FES_DEFINES_H
#define ALI_HLT_SHUTTLE_FES_DEFINES_H



/**
 * Define for the permission of the directories in the FES
 */
#define ALIHLT_FES_DIRECTORY_PERMISSION S_IFDIR|S_IRWXU|S_IRWXG|S_IROTH|S_IXOTH // Directory | Read,write, execute User/Group | read,execute all


#endif // ALI_HLT_SHUTTLE_FES_DEFINES_H

