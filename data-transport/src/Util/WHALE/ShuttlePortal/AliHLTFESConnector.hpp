#ifndef ALI_HLT_FES_CONNECTOR_HPP
#define ALI_HLT_FES_CONNECTOR_HPP

#include "AliHLTCalibrationObject.hpp"
#include "AliHLTShuttlePortalLogger.hpp"

#include <string>

/**
 * Class that represents the connection to the Alice HLT Shuttle portal
 * FileExchangeServer (FES - renamed to FXS).
 * This class covers the initialization (setting of base path to FES) and
 * storing of a calibration object to the FES.
 *
 * @date 21.04.2007
 *
 * @author Sebastian Bablok <Sebastian.Bablok@uib.no>
 */
class AliHLTFESConnector {
	public:
		/**
		 * Standard C-Tor for the FileExchangeSever Connector.
		 * When calling this constructor, the connector is not initialized
		 * during object creation. You have to call init afterwards, before
		 * you can use the connector.
		 */
		AliHLTFESConnector();

		/**
		 * Constructor, which is also able to intialize the the connector.
		 *
		 * @param fesBasePath the path to the base directory of the
		 *			FileExchangeServer.
		 * @param aLogger pointer to the Shuttle Portal Logger
		 *
		 * @throw the constructor can throw an exception, if the initialization
		 *			can not be fullfilled (base path of FES not reachable)
		 */
		AliHLTFESConnector(std::string fesBasePath,
				AliHLTShuttlePortalLogger* aLogger);

		/**
		 * Destructor for the FES connector.
		 */
		~AliHLTFESConnector();

		/**
		 * Initialization function for the FES connector.
		 * This function sets the base path for the FileExchangeServer and
		 * tests, if the path is reachable. Only if this is successful, the
		 * the connector is really intialized.
		 * If the base path shall be reset, just call init with new path.
		 *
		 * @param fesBasePath the path to the base directory of the
         *          FileExchangeServer.
		 * @param aLogger pointer to the Shuttle Portal Logger
		 *
		 * @return ALIHLT_OK, in case of successful initialization, else an
		 *			error code is returned (possible errors: base directory of
		 *			FES is not reachable.)
		 */
		int init(std::string fesBasePath, AliHLTShuttlePortalLogger* aLogger);

		/**
		 * Function to test if connector is fully initialized.
		 *
		 * @return true, if fully initialized, else false
		 */
		bool isInit();

		/**
		 * Getter for the current base path of the FES.
		 * If the connector has not been initialized, the path string is empty.
		 *
		 * @return the path to the root directory of the FileExchangeServer.
		 */
		std::string getBasePath();

		/**
		 * Function to store a CalibrationObject in the FileExchangeServer (FES).
		 * The function stors the calibration data under the base path of the FES
		 * plus the relative path provided by the meta data of the Calibration
		 * object. The status of the AliHLTCalibrationObject is changed according
		 * to the new state (ALIHLT_FES_FILLED, ALIHLT_STORAGE_ERROR).
		 * @see AliHLTShuttlePortalDefines.h
		 *
		 * @param calibObject reference to the AliHLTCalibrationObject object,
		 * 			that shall be stored into the FES.
		 *
		 * @return ALIHLT_OK, if storing has been successful, else an error
		 *			code is returned.
		 */
		int storeCalibrationBLOB(AliHLTCalibrationObject& calibObject);

	private:
		/**
		 * Function to caluclate the md5sum (checksum) of the calibration object
		 *
		 * @param file from which the checksum shall be calculated
		 * @param result pointer to store the result
		 *
		 * @return true, if successful
		 */
		bool calculateMD5Sum(std::string file, std::string* result);

		/**
		 * Function to create directory structure for storing calibration file
		 *
		 * @param calibObject calibration object, which contains all subparts
		 *			of the path
		 *
		 * @return true, if successful, else false
		 */
		bool createDir(AliHLTCalibrationObject& calibObject);

		/**
		 * Function to (re-) set the link for the current calibration object
		 * If this is the first version ("_0" as suffix) of this object the link
		 * is created. If the link is already existing (suffix > 0), the old 
		 * link is removed and a new one is set to the latest version of the 
		 * calibration object
		 *
		 * @param targetName name of the target file for the link (can include
		 * 				the whole path)
		 * @param linkName name of the link to be set (can include the whole 
		 * 				path)
		 * @param relink indicates if the link is expected to be already 
		 * 				existing. If so the old one is removed and a new one is
		 * 				set
		 * 
		 * @return zero on success, else an error code is returned
		 */
		int createLink(const char* targetName, const char* linkName, bool relink);

		/**
		 * Member indicating, if connector is initialized.
		 */
		bool mIsInit;

		/**
		 * Member for storing the base path to the root directory of the
		 * FileExchangeServer.
		 */
		std::string mBasePath;

		/**
		 * Member pointer to the Shuttle Portal Logger
		 *
		 */
		AliHLTShuttlePortalLogger* mpLogger;

};

inline bool AliHLTFESConnector::isInit() {
	return mIsInit;
}

inline std::string AliHLTFESConnector::getBasePath() {
	return mBasePath;
}

#endif // ALI_HLT_FES_CONNECTOR_HPP


