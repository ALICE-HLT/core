#include "AliHLTShuttlePortal.hpp"
#include "AliHLTCalibrationObject.hpp"
#include "AliHLTShuttlePortalDefines.h"

#include "AliHLTShuttlePortalFileLogger.hpp"

#include <fstream>
#include <iostream>
#include <stdlib.h>

using namespace std;

int main(int argc, char** argv) {
	int nRet = 0;
	AliHLTShuttlePortalFileLogger aLogger("Portal.log");
	AliHLTShuttlePortal myportal(&aLogger);
	AliHLTCalibrationObject* obj;
	int run = 21;
	string fileID = "SelfCalibrated2.root";
	int size = 0;
	char* calibData = 0;

	if (argc >= 2) {
		run = atoi(argv[1]);
	}

	if (argc >= 3) {
		fileID = argv[2];
	}

	ifstream file(fileID.c_str(), ios::in | ios::binary | ios::ate);

	if (file.is_open()) {
		size = file.tellg();
		calibData = new char[size];
		file.seekg (0, ios::beg);
		file.read (calibData, size);
		file.close();
	} else {
		cout << "Error in reading file! exiting... " << endl;

		return -1;
	}

	cout << "starting to init connector" << endl;
	
	nRet = myportal.init("/opt/FXS", 
					"hlt_logbook", "localhost", "hlt", "H7T-5huttle-D8");
//	nRet = myportal.init("/home/fes", "hlt_logbook", "portal-vobox.internal", "hlt", "H7T-5huttle-D8");
	if (nRet != ALIHLT_OK) {
		cout << "Error while initialating ShuttlePortal: " << nRet << "!" << endl;
	    delete[] calibData;
		return -1;
	}

	cout << "Init of connector successful, trying to store object ..." << endl;

	obj = new AliHLTCalibrationObject(run, "IFT", fileID.c_str(),
				"0123456789", (void*) calibData, size, ALIHLT_CALIB_DATA_BIN);
	nRet = myportal.storeCalibObject(*obj);
	if (nRet != ALIHLT_OK) {
		cout << "Error while storing calibration object: " << AliHLTShuttlePortalLogger::convertReturnCode(nRet)
			   << "(" << nRet << ")" << endl;
	} else {
		cout << "Successfully stored calibration object." << endl;
	}

	myportal.deinit();

	cout << "Deinit of connector finished..." << endl;
	
	delete obj;	
	delete[] calibData;

	return 0;
}

