#!/bin/bash

BASEPATH=`dirname $0`
CREATEFILES=""
FIRSTFILE=

for (( i=0; i<216 ; i++ )) ; do
  if [ ! -f TPC_$i.ddl ] ; then
    echo File TPC_$i.ddl not found
    CREATEFILES="$CREATEFILES TPC_$i.ddl"
  else
    if [ -z "$FIRSTFILE" ] ; then
      FIRSTFILE=TPC_$i.ddl
    fi
  fi
done

if [ -n "$FIRSTFILE" ] ; then
  echo Creating missing files as empty files using template file $FIRSTFILE 
  for p in $CREATEFILES ; do
    echo Creating file $p
    ${BASEPATH}/bin/`uname -s`-`uname -m`/DDLHeaderCopy $FIRSTFILE $p
  done
  exit 0
fi

echo No TPC ddl file found for old scheme, trying new scheme

CREATEFILES=""
FIRSTFILE=
for (( i=768; i<984 ; i++ )) ; do
  if [ ! -f TPC_$i.ddl ] ; then
    echo File TPC_$i.ddl not found
    CREATEFILES="$CREATEFILES TPC_$i.ddl"
  else
    if [ -z "$FIRSTFILE" ] ; then
      FIRSTFILE=TPC_$i.ddl
    fi
  fi
done

if [ -z "$FIRSTFILE" ] ; then
  echo No TPC ddl file found. Aborting
  exit -1
fi

echo Creating missing files as empty files using template file $FIRSTFILE 
for p in $CREATEFILES ; do
  echo Creating file $p
  ${BASEPATH}/bin/`uname -s`-`uname -m`/DDLHeaderCopy $FIRSTFILE $p
done

