/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <climits>
#include <cerrno>
#include <cstring>

#define DDLHEADERWORDCOUNT 8
#define DDLLENGTHWORDOFFSET 0

#if USHRT_MAX==4294967295
typedef unsigned short uint32;
#else // USHRT_MAX==4294967295

#if UINT_MAX==4294967295
typedef unsigned int uint32;
#else // UINT_MAX==4294967295

#if ULONG_MAX==4294967295l
typedef unsigned long uint32;
#else // ULONG_MAX==4294967295l

#error Could not typedef uint32

#endif // ULONG_MAX==4294967295l

#endif // UINT_MAX==4294967295

#endif // USHRT_MAX==4294967295

int main( int argc, char** argv )
    {
    if ( argc != 3 )
	{
	std::cerr << "Usage: " << argv[0] << " <infile> <outfile>" << std::endl;
	return -1;
	}

    int infh=-1, outfh=-1;

    infh = open( argv[1], O_RDONLY );
    if ( infh == -1 )
	{
	std::cerr << "Cannot open infile '" << argv[1] <<"': "
	     << strerror(errno) << std::endl;
	return -1;
	}
    outfh = open( argv[2], O_WRONLY|O_TRUNC|O_CREAT, 0666 );
    if ( outfh == -1 )
	{
	std::cerr << "Cannot open outfile '" << argv[2] <<"': "
	     << strerror(errno) << std::endl;
	return -1;
	}
    uint32 header[ DDLHEADERWORDCOUNT ];
    int ret;
    ret = read( infh, header, sizeof(header[0])*DDLHEADERWORDCOUNT );
    if ( ret != sizeof(header[0])*DDLHEADERWORDCOUNT )
	{
	std::cerr << "Error reading " << sizeof(header[0])*DDLHEADERWORDCOUNT 
	     << " bytes from infile " << argv[1] << std::endl;
	return -1;
	}
    close( infh );
    header[ DDLLENGTHWORDOFFSET ] = sizeof(header[0])*DDLHEADERWORDCOUNT;
    ret = write( outfh, header, sizeof(header[0])*DDLHEADERWORDCOUNT );
    if ( ret != sizeof(header[0])*DDLHEADERWORDCOUNT )
	{
	std::cerr << "Error writing " << sizeof(header[0])*DDLHEADERWORDCOUNT 
	     << " bytes  to outfile " << argv[2] << std::endl;
	return -1;
	}
    close( outfh );
    return 0;
    }






/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
