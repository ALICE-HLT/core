/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: TCPHOMERDataProvider.cpp 497 2005-12-02 08:56:24Z timm $ 
**
***************************************************************************
*/


#include "MLUCThread.hpp"
#include "MLUCString.hpp"
#include "MLUCDynamicAllocCache.hpp"
#include "MLUCTypes.h"
#include "MLUCLog.hpp"
#include "MLUCLogServer.hpp"
#include "MLUCConditionSem.hpp"
#include "MLUCCmdlineParser.hpp"
#include "HOMERData.h"
#include "HOMERWriter.h"
#include <pthread.h>
#include <sys/uio.h>
#include <vector>
#include <cerrno>
#include <sys/socket.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <signal.h>
#include <fcntl.h>
#include <cstdlib>



class TCPHOMERDataProvider: public MLUCThread
    {
    public:

	struct TConnectionData
	    {
		TConnectionData( int sock, bool bin ):
		    fSocket( sock ),
		    fBinary( bin )
			{}
// 		TConnectionData()
// 			{
// 			fSocket = -1;
// 			fBinary = false;
// 			}
		int fSocket;
		bool fBinary;
	    };

	TCPHOMERDataProvider( unsigned short portNr, bool allowBlockingMode );
	virtual ~TCPHOMERDataProvider();

	void Quit();
	bool Quitted();

	void SetTimeout( unsigned long timeout_us )
		{
		fTimeout = timeout_us;
		}

// 	void GetNonblockingConnectionSockets( std::vector<TConnectionData>& sockets );
// 	void GetBlockingConnectionSockets( std::vector<TConnectionData>& sockets );

	void CloseConnection( int sock );

	virtual TState Start();
	
	void Write( unsigned blockcnt, iovec* blocks );

    protected:

	enum TSendMode { kNoData = 0, kNextEvent = 1, kMultipleEvents = 2, kAllEvents = 3 };
	enum TDataMode { kBinary = 0, kASCII = 1 };

	struct TPublicWriteData
	    {
		unsigned fBlockCnt;
		iovec* fBlocks;
		unsigned long fConnections;
		unsigned long fConnectionsDone;
	    };
	
	struct TPrivateWriteData
	    {
		bool fBinary;
		TPublicWriteData* fPublic;
		unsigned long fWritten;
	    };
	
	struct TReadData
	    {
		MLUCString fCmd;
 		unsigned fBufferSize;
		unsigned fBufferStart;
		uint8 fBuffer[64];
	    };

	struct TConnection
	    {
		int fSocket;
		TSendMode fSendMode;
		TDataMode fDataMode;
		TReadData fReadData;
		TPrivateWriteData fWriteData;
	    };

	virtual void Run();

	int OpenSocket();
	int CloseSocket();

	int Listen( int& newConnection, std::vector<TConnection>& readSockets, std::vector<TConnection>& writeSockets );

	int ReadCommand( TConnection& connData, MLUCString& cmd, bool first, bool& moreData );
	bool ProcessCommand( TConnection& connData );

	virtual void EventRequested( bool /*eventStream*/ ) {};
	virtual void StopEvents() {};

	void AddConnection( int sock );
// 	bool GetConnection( int sock, TConnection& connData );
	bool UpdateConnection( const TConnection& connData );

	//void GetAllConnectionSockets( std::vector<int>& sockets );
	void GetAllConnectionSockets( std::vector<TConnection>& sockets );

	int WriteToConnection( TConnection& connData, const TPublicWriteData& pwd, bool blocking );
	int WriteToConnection( int connSock, unsigned blockcnt, iovec* blocks, bool blocking, unsigned long& written );

	iovec* CloneBlocks( unsigned blockCnt, const iovec* blocks );

	void ReleaseBlocks( unsigned blockCnt, iovec* blocks );

	void Release( TPublicWriteData* pwd );

	unsigned short fListenPort;

	int fListenSocket;

	unsigned long fTimeout; // In microsecs.

	std::vector<TConnection> fSockets;
	pthread_mutex_t fSocketMutex;
	pthread_mutex_t fWriteDataMutex;

	bool fQuit;
	bool fQuitted;

	bool fBlockingModeAllowed;

	MLUCDynamicAllocCache fAllocCache;

    private:
    };



TCPHOMERDataProvider::TCPHOMERDataProvider( unsigned short portNr, bool allowBlockingMode ):
    fAllocCache( 6, true, false )
    {
    fListenPort = portNr;
    fQuit = false;
    fQuitted = true;
    fTimeout = 0;
    fBlockingModeAllowed = allowBlockingMode;
    pthread_mutex_init( &fSocketMutex, NULL );
    pthread_mutex_init( &fWriteDataMutex, NULL );
    }

TCPHOMERDataProvider::~TCPHOMERDataProvider()
    {
    pthread_mutex_destroy( &fSocketMutex );
    pthread_mutex_destroy( &fWriteDataMutex );
    while ( fSockets.size()>0 )
	{
	if ( fSockets.begin()->fWriteData.fPublic )
	    Release( fSockets.begin()->fWriteData.fPublic );
	close( fSockets.begin()->fSocket );
	fSockets.erase( fSockets.begin() );
	}
    close( fListenSocket );
    }

void TCPHOMERDataProvider::Quit()
    {
    fQuit = true;
    }

bool TCPHOMERDataProvider::Quitted()
    {
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 2000000;
    gettimeofday( &start, NULL );
    fQuit = true;
    while ( deltaT<timeLimit && !fQuitted )
	{
	usleep( 10000 );
	fQuit = true;
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    return fQuitted;
    }

// void TCPHOMERDataProvider::GetBlockingConnectionSockets( std::vector<TConnectionData>& sockets )
//     {
//     std::vector<TConnection>::iterator iter, end;
//     pthread_mutex_lock( &fSocketMutex );
//     iter = fSockets.begin();
//     end = fSockets.end();
//     while ( iter != end )
// 	{
// 	if ( iter->fSendMode == kAllEvents )
// 	    socket.push_back( TConnectionData( iter->fSocket, iter->fDataMode==kBinary ) );
// 	iter++;
// 	}
//     pthread_mutex_unlock( &fSocketMutex );
//     }

// void TCPHOMERDataProvider::GetNonblockingConnectionSockets( std::vector<TConnectionData>& sockets )
//     {
//     std::vector<TConnection>::iterator iter, end;
//     pthread_mutex_lock( &fSocketMutex );
//     iter = fSockets.begin();
//     end = fSockets.end();
//     while ( iter != end )
// 	{
// 	if ( iter->fSendMode == kNextEvent )
// 	    socket.push_back( TConnectionData( iter->fSocket, iter->fDataMode==kBinary ) );
// 	iter++;
// 	}
//     pthread_mutex_unlock( &fSocketMutex );
//     }

//void TCPHOMERDataProvider::GetAllConnectionSockets( std::vector<int>& sockets )
void TCPHOMERDataProvider::GetAllConnectionSockets( std::vector<TConnection>& sockets )
    {
    pthread_mutex_lock( &fSocketMutex );
    sockets = fSockets;
    pthread_mutex_unlock( &fSocketMutex );
    }

MLUCThread::TState TCPHOMERDataProvider::Start()
    {
    int ret;
    ret = OpenSocket();
    if ( ret )
	return MLUCThread::kFailed;
    return MLUCThread::Start();
    }

void TCPHOMERDataProvider::Write( unsigned blockCnt, iovec* blocks )
    {
    TPublicWriteData* pwd=NULL;
    std::vector<TConnection> blockingConnections;
    std::vector<TConnection>::iterator iter, end;
    pthread_mutex_lock( &fSocketMutex );
    iter = fSockets.begin();
    end = fSockets.end();
    while ( iter != end )
	{
	if ( (iter->fSendMode == kNextEvent || iter->fSendMode == kMultipleEvents ) &&
	   !iter->fWriteData.fPublic )
	    {
	    if ( !pwd )
		{
		pwd = reinterpret_cast<TPublicWriteData*>( fAllocCache.Get( sizeof(TPublicWriteData) ) );
		if ( !pwd )
		    {
		    LOG( MLUCLog::kError, "TCPHOMERDataProvider::Write", "Out of memory" )
			<< "Out of memory allocating public write data for connection with socket "
			<< MLUCLog::kDec << iter->fSocket << "." << ENDLOG;
		    iter++; 
		    continue;
		    }
		pwd->fBlockCnt = blockCnt;
		pwd->fBlocks = CloneBlocks( blockCnt, blocks );
		if ( !pwd->fBlocks )
		    {
		    LOG( MLUCLog::kError, "TCPHOMERDataProvider::Write", "Out of memory" )
			<< "Out of memory allocating block copy for connection with socket "
			<< iter->fSocket << "." << ENDLOG;
		    fAllocCache.Release( (uint8*)pwd );
		    iter++; 
		    continue;
		    }
		pwd->fConnections = 0;
		pwd->fConnectionsDone = 0;
		}
	    iter->fWriteData.fBinary = (iter->fDataMode==kBinary);
	    iter->fWriteData.fPublic = pwd;
	    iter->fWriteData.fWritten = 0;
	    if ( iter->fSendMode == kNextEvent )
		iter->fSendMode = kNoData;
	    pwd->fConnections++;
	    LOG( MLUCLog::kDebug, "TCPHOMERDataProvider::Write", "Data marked" )
		<< "Data marked for sending to connection with socket "
		<< MLUCLog::kDec << iter->fSocket << " (connection "
		<< pwd->fConnections << ")." << ENDLOG;
	    }
	if ( iter->fSendMode == kAllEvents )
	    blockingConnections.push_back( *iter );
	iter++;
	}
    pthread_mutex_unlock( &fSocketMutex );

    TPublicWriteData spwd;
    spwd.fBlockCnt = blockCnt;
    spwd.fBlocks = blocks;

    iter = blockingConnections.begin();
    end = blockingConnections.end();
    int ret;
    unsigned long successes=0;
    while ( iter != end )
	{
	ret = WriteToConnection( *iter, spwd, true );
	if ( ret )
	    {
	    LOG( MLUCLog::kError, "TCPHOMERDataProvider::Write", "Error writing data" )
		<< "Error writing data to blocking connection with socket "
		<< MLUCLog::kDec << iter->fSocket << ": " << strerror(ret)
		<< " (" << ret << ")." << ENDLOG;
	    }
	else
	    successes++;
	iter++;
	}
    
    LOG( MLUCLog::kDebug, "TCPHOMERDataProvider::Write", "Data written" )
	<< "Data written to " << MLUCLog::kDec << successes
	<< " of " << blockingConnections.size() << " blocking connections."
	<< ENDLOG;
    }


	
void TCPHOMERDataProvider::Run()
    {
    fQuitted = false;
    fQuit = false;
    int ret;
    int newConnection;
    std::vector<TConnection> readSockets;
    std::vector<TConnection> writeSockets;
    while ( !fQuit )
	{
	ret = Listen( newConnection, readSockets, writeSockets );
	std::vector<TConnection>::iterator iter, end;
	if ( readSockets.size()>0 )
	    {
	    iter = readSockets.begin();
	    end = readSockets.end();
	    while ( iter != end )
		{
		MLUCString cmd;
		bool moreData;
		bool first = true;
		TConnection connData = *iter;
		do
		    {
		    cmd = "";
		    ret = ReadCommand( connData, cmd, first, moreData );
		    LOG( MLUCLog::kDebug, "TCPHOMERDataProvider::Run", "ReadCommand" )
			<< MLUCLog::kDec << "ret: " << ret << " - cmd: '" << cmd.c_str()
			<< "' - first: " << (first ? "yes" : "no") << " - moreData: "
			<< (moreData ? "yes" : "no" ) << "." << ENDLOG;
		    first = false;
		    if ( ret && ret!=EAGAIN && ret!=EINTR && ret!=ECONNABORTED )
			{
			LOG( MLUCLog::kError, "TCPHOMERDataProvider::Run", "Error reading socket" )
			    << "Error reading command from socket " << MLUCLog::kDec << iter->fSocket
			    << ": " << strerror(ret) << " (" << ret << ")." << ENDLOG;
			CloseConnection( iter->fSocket );
			}
		    else if ( ret==ECONNABORTED )
			{
			LOG( MLUCLog::kDebug, "TCPHOMERDataProvider::Run", "Connection closed" )
			    << "Connection from socket " << MLUCLog::kDec << iter->fSocket
			    << " closed by remote side." << ENDLOG;
			CloseConnection( iter->fSocket );
			}
		    else if ( (ret==EAGAIN || ret==EINTR) && cmd.Length()>0 )
			{
			connData.fReadData.fCmd += cmd;
			LOG( MLUCLog::kDebug, "TCPHOMERDataProvider::Run", "Command fragment" )
			    << "Received command fragment: '" << cmd.c_str() << "'." << ENDLOG;
			}
		    else if ( cmd.Length()>0 )
			{
			LOG( MLUCLog::kDebug, "TCPHOMERDataProvider::Run", "Command fragment" )
			    << "Received command fragment: '" << cmd.c_str() << "'." << ENDLOG;
			connData.fReadData.fCmd += cmd;
			LOG( MLUCLog::kDebug, "TCPHOMERDataProvider::Run", "Command received" )
			    << "Received command: '" << connData.fReadData.fCmd.c_str() << "'." << ENDLOG;
			if ( !ProcessCommand( connData ) )
			    {
			    LOG( MLUCLog::kError, "TCPHOMERDataProvider::Run", "Error processing command" )
				<< "Error processing command '" << cmd.c_str() << "' from socket " << MLUCLog::kDec << iter->fSocket
				<< "." << ENDLOG;
			    CloseConnection( iter->fSocket );
			    }
			connData.fReadData.fCmd = "";
			}
		    // else nothing read
		    }
		while ( moreData && !ret );

		if ( !UpdateConnection( connData ) )
		    {
		    if ( connData.fWriteData.fPublic )
			Release( connData.fWriteData.fPublic );
		    }
		// XXX TODO: After failed update and if data to write in background, release pwd
		iter++;
		}
	    }
	if ( writeSockets.size()>0 )
	    {
	    iter = writeSockets.begin();
	    end = writeSockets.end();
	    while ( iter != end )
		{
		TConnection connData = *iter;
		
		int ret;
		LOG( MLUCLog::kDebug, "TCPHOMERDataProvider::Run", "Writing data" )
		    << "Writing background data to socket " << MLUCLog::kDec << iter->fSocket 
		    << ": " << MLUCLog::kDec << connData.fWriteData.fPublic->fBlockCnt
		    << " blocks - mode " << ( connData.fWriteData.fBinary ? "binary" : "ascii" )
		    << " - " << connData.fWriteData.fWritten << " bytes already written." << ENDLOG;
		ret = WriteToConnection( connData, *connData.fWriteData.fPublic, false );
		LOG( MLUCLog::kDebug, "TCPHOMERDataProvider::Run", "Data written" )
		    << "Wrote background data: " << MLUCLog::kDec << connData.fWriteData.fWritten 
		    << " bytes written now." << ENDLOG;
		if ( ret )
		    {
		    LOG( MLUCLog::kError, "TCPHOMERDataProvider::Run", "Error writing to connection" )
			<< "Error writing to connection for socket " << MLUCLog::kDec << iter->fSocket
			<< ": " << strerror(ret) << " (" << ret << ")." << ENDLOG;
		    CloseConnection( connData.fSocket );
		    }

		if ( !UpdateConnection( connData ) )
		    {
		    if ( connData.fWriteData.fPublic )
			Release( connData.fWriteData.fPublic );
		    }
		iter++;
		}
	    }
	if ( newConnection!=-1 )
	    AddConnection( newConnection );
	}
    CloseSocket();
    fQuitted = true;
    }

int TCPHOMERDataProvider::ReadCommand( TConnection& connData, MLUCString& cmd, bool first, bool& moreData )
    {
    cmd = "";
    if ( connData.fReadData.fBufferSize == 0 )
	{
	moreData = false;
	int ret;
	ret = read( connData.fSocket, connData.fReadData.fBuffer, 64 );
	if ( ret<0 )
	    return errno;
	if ( ret==0 && first )
	    return ECONNABORTED;
	connData.fReadData.fBufferSize = ret;
	connData.fReadData.fBufferStart = 0;
	}
    bool cmdFull=true;
    if ( connData.fReadData.fBufferSize > 0 )
	{
	unsigned n = connData.fReadData.fBufferStart;
	while ( n<connData.fReadData.fBufferSize+connData.fReadData.fBufferStart && connData.fReadData.fBuffer[n] != '\n' )
	    {
	    cmd += connData.fReadData.fBuffer[n];
	    n++;
	    }
	if ( n<connData.fReadData.fBufferSize+connData.fReadData.fBufferStart && connData.fReadData.fBuffer[n]=='\n' )
	    n++;
	else
	    cmdFull = false;
	connData.fReadData.fBufferSize -= (n-connData.fReadData.fBufferStart);
	connData.fReadData.fBufferStart = n;
	}
    if ( connData.fReadData.fBufferSize>0 )
	moreData = true;
    else
	moreData = false;
    if ( cmdFull )
	return 0;
    else
	return EAGAIN;
    }

bool TCPHOMERDataProvider::ProcessCommand( TConnection& connData )
    {
    if ( connData.fReadData.fCmd == "GET ONE" ||
	 connData.fReadData.fCmd.Substr( 0, strlen("REPLAY ID ") ) == "REPLAY ID " ||
	 connData.fReadData.fCmd.Substr( 0, strlen("FIRST ORBIT EVENT ") ) == "FIRST ORBIT EVENT " )
	{
	connData.fSendMode = kNextEvent;
	LOG( MLUCLog::kDebug, "TCPHOMERDataProvider::ProcessCommand", "Next Event Mode" )
	    << "Connection for socket " << MLUCLog::kDec << connData.fSocket
	    << " switching to NextEvent Mode." << ENDLOG;
	EventRequested( false );
	}
    else if ( connData.fReadData.fCmd == "GET ALL" && fBlockingModeAllowed )
	{
	connData.fSendMode = kAllEvents;
	LOG( MLUCLog::kDebug, "TCPHOMERDataProvider::ProcessCommand", "All Events Mode" )
	    << "Connection for socket " << MLUCLog::kDec << connData.fSocket
	    << " switching to AllEvents Mode." << ENDLOG;
	EventRequested( true );
	}
    else if ( connData.fReadData.fCmd == "GET NON" )
	{
	connData.fSendMode = kNoData;
	LOG( MLUCLog::kDebug, "TCPHOMERDataProvider::ProcessCommand", "No Event Mode" )
	    << "Connection for socket " << MLUCLog::kDec << connData.fSocket
	    << " switching to NoEvent Mode." << ENDLOG;
	StopEvents();
	}
    else if ( connData.fReadData.fCmd == "MOD ASC" )
	{
	connData.fDataMode = kASCII;
	LOG( MLUCLog::kDebug, "TCPHOMERDataProvider::ProcessCommand", "ASCII Mode" )
	    << "Connection for socket " << MLUCLog::kDec << connData.fSocket
	    << " switching to ASCII Mode." << ENDLOG;
	}
    else if ( connData.fReadData.fCmd == "MOD BIN" )
	{
	connData.fDataMode = kBinary;
	LOG( MLUCLog::kDebug, "TCPHOMERDataProvider::ProcessCommand", "Binary Mode" )
	    << "Connection for socket " << MLUCLog::kDec << connData.fSocket
	    << " switching to Binary Mode." << ENDLOG;
	}
    else
	return false;
    return true;
    }


int TCPHOMERDataProvider::OpenSocket()
    {
    if ( fListenSocket == -1 )
	{
	close( fListenSocket );
	fListenSocket = -1;
	}
    int ret;
    struct protoent* proto;
    proto = getprotobyname( "tcp" );
    if ( !proto )
	{
	ret = errno;
	LOG( MLUCLog::kError, "TCPHOMERDataProvider::OpenSocket", "Protocol File Error" )
	    << "Error reading TCP protocol number from /etc/protocols file: " 
	    << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	return ret;
	}
    int one=1;
    fListenSocket = socket( AF_INET, SOCK_STREAM, proto->p_proto );
    if ( fListenSocket == -1 )
	{
	ret = errno;
	LOG( MLUCLog::kError, "TCPHOMERDataProvider::OpenSocket", "Socket Creation Error" )
	    << "Error creating TCP socket: " 
	    << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	return ret;
	}
    
    ret = setsockopt( fListenSocket, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one) );
    if ( ret )
	{
	LOG( MLUCLog::kError, "TCPHOMERDataProvider::OpenSocket", "Socket reuse addr" )
	    << "Error setting sockets options to reuse addr: "
	    << strerror(errno) << " (" << MLUCLog::kDec << errno << ")." << ENDLOG;
	}

    struct sockaddr_in sock_addr;
    sock_addr.sin_family = AF_INET;
    sock_addr.sin_port = htons( fListenPort );
    sock_addr.sin_addr.s_addr = htonl( INADDR_ANY );
    memset(&(sock_addr.sin_zero), '\0', 8);
    ret = bind( fListenSocket,  (const sockaddr*)&sock_addr, sizeof(struct sockaddr) );
    if ( ret==-1 )
	{
	ret = errno;
	LOG( MLUCLog::kError, "TCPHOMERDataProvider::OpenSocket", "Socket Bind Error" )
	    << "Error bind'ing TCP socket on port " << MLUCLog::kDec << fListenPort
	    << " (0x" << MLUCLog::kHex << fListenPort << "): "
	    << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	close( fListenSocket );
	fListenSocket = -1;
	return ret;
	}

    ret = listen( fListenSocket, 5 );
    if ( ret == -1 )
	{
	ret = errno;
	LOG( MLUCLog::kError, "TCPHOMERDataProvider::OpenSocket", "Socket Listen Error" )
	    << "Error listen'ing with TCP socket on port " << MLUCLog::kDec << fListenPort
	    << " (0x" << MLUCLog::kHex << fListenPort << "): "
	    << strerror(ret) << " (" << MLUCLog::kDec << ret << ")." << ENDLOG;
	close( fListenSocket );
	fListenSocket = -1;
	return ret;
	}
    return 0;
    }

int TCPHOMERDataProvider::CloseSocket()
    {
    close( fListenSocket );
    fListenSocket = -1;
    return 0;
    }

int TCPHOMERDataProvider::Listen( int& newConnection, std::vector<TConnection>& readSocks, std::vector<TConnection>& writeSocks )
    {
    struct timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = 100000;

    fd_set readSockets, writeSockets;
    int ret, highest_sock;
    readSocks.clear();
    writeSocks.clear();

    FD_ZERO( &readSockets);
    FD_ZERO( &writeSockets);
    FD_SET( fListenSocket, &readSockets );
    highest_sock = fListenSocket;

    std::vector<TConnection> socketList;
    GetAllConnectionSockets( socketList );
    std::vector<TConnection>::iterator iter, end;
    iter = socketList.begin();
    end = socketList.end();
    while ( iter != end )
	{
	FD_SET( iter->fSocket, &readSockets );
 	if ( iter->fSocket > highest_sock )
	    highest_sock = iter->fSocket;
	if ( iter->fWriteData.fPublic )
	    {
	    FD_SET( iter->fSocket, &writeSockets );
	    LOG( MLUCLog::kDebug, "TCPHOMERDataProvider::Listen", "Adding write socket" )
		<< "Adding socket " << MLUCLog::kDec << iter->fSocket << " to write candidate list." << ENDLOG;
	    }
	iter++;
	}

    newConnection = -1;
    readSocks.clear();
    writeSocks.clear();

    ret = select( highest_sock+1, &readSockets, &writeSockets, NULL, &tv );
    if ( ret<0 )
	return ret;
    if ( ret==0 )
	return 0;
    
    if ( FD_ISSET( fListenSocket, &readSockets ) )
	{
	struct sockaddr_in addr;
	socklen_t addrLen;
	addrLen = sizeof(addr);
	newConnection = accept( fListenSocket, (struct sockaddr*)&addr, &addrLen );
	}
    
    iter = socketList.begin();
    end = socketList.end();
    while ( iter != end && (readSocks.size()+writeSocks.size()<(unsigned)ret) )
	{
	if ( FD_ISSET( iter->fSocket, &readSockets ) )
	    {
	    readSocks.push_back( *iter );
	    }
	if ( FD_ISSET( iter->fSocket, &writeSockets ) )
	    {
	    LOG( MLUCLog::kDebug, "TCPHOMERDataProvider::Listen", "Adding write socket" )
		<< "Adding socket " << MLUCLog::kDec << iter->fSocket << " to writable list." << ENDLOG;
	    writeSocks.push_back( *iter );
	    }
	iter++;
	}
    return 0;
    }
 
void TCPHOMERDataProvider::AddConnection( int sock )
    {
    // XXX TODO: Initialize read/write data
    fcntl( sock, F_SETFL, O_NONBLOCK );
    TConnection connData;
    connData.fSocket = sock;
    connData.fSendMode = kNoData;
    connData.fDataMode = kASCII;
    connData.fReadData.fCmd = "";
    connData.fReadData.fBufferSize = 0;
    connData.fReadData.fBufferStart = 0;
    connData.fWriteData.fBinary = false;
    connData.fWriteData.fPublic = NULL;
    connData.fWriteData.fWritten = 0;
    pthread_mutex_lock( &fSocketMutex );
    fSockets.push_back( connData );
    pthread_mutex_unlock( &fSocketMutex );
    }

// bool TCPHOMERDataProvider::GetConnection( int sock, TConnection& connData )
//     {
//     std::vector<TConnection>::iterator iter, end;
//     bool found=false;
//     pthread_mutex_lock( &fSocketMutex );
//     iter = fSockets.begin();
//     end = fSockets.end();
//     while ( iter != end )
// 	{
// 	if ( iter->fSocket == sock )
// 	    {
// 	    connData = *iter;
// 	    found=true;
// 	    break;
// 	    }
// 	iter++;
// 	}
//     pthread_mutex_unlock( &fSocketMutex );
//     return found;
//     }

bool TCPHOMERDataProvider::UpdateConnection( const TConnection& connData )
    {
    std::vector<TConnection>::iterator iter, end;
    bool found=false;
    pthread_mutex_lock( &fSocketMutex );
    iter = fSockets.begin();
    end = fSockets.end();
    while ( iter != end )
	{
	if ( iter->fSocket == connData.fSocket )
	    {
	    *iter = connData;
	    found=true;
	    break;
	    }
	iter++;
	}
    pthread_mutex_unlock( &fSocketMutex );
    return found;
    }


void TCPHOMERDataProvider::CloseConnection( int sock )
    {
    pthread_mutex_lock( &fSocketMutex );
    std::vector<TConnection>::iterator iter, end;
    iter = fSockets.begin();
    end = fSockets.end();
    while ( iter != end )
	{
	if ( iter->fSocket == sock )
	    {
	    if ( iter->fWriteData.fPublic )
		Release( iter->fWriteData.fPublic );
	    fSockets.erase( iter );
	    break;
	    }
	iter++;
	}
    pthread_mutex_unlock( &fSocketMutex );
    close( sock );
    }

int TCPHOMERDataProvider::WriteToConnection( TConnection& connData, const TPublicWriteData& pwd, bool blocking )
    {
    iovec ioVecs[pwd.fBlockCnt+1]; // 1 more for initial size block.
    uint32 totalSize=0, totalSizeNBO, realTotalSize;
    char totalSizeStr[ 64 ];
    for ( unsigned n = 0; n < pwd.fBlockCnt; n++ )
	{
	totalSize += pwd.fBlocks[n].iov_len;
	ioVecs[n+1] = pwd.fBlocks[n];
	}
    // totalSize does NOT include itself!!!
    // But realTotalSize does
    realTotalSize = totalSize;
    if ( connData.fDataMode == kBinary )
	{
	totalSizeNBO = htonl( totalSize );
	ioVecs[0].iov_base = &totalSizeNBO;
	ioVecs[0].iov_len = sizeof(totalSizeNBO);
	}
    else if ( connData.fDataMode == kASCII )
	{
	sprintf( totalSizeStr, "%u\n", totalSize );
	ioVecs[0].iov_base = totalSizeStr;
	ioVecs[0].iov_len = strlen(totalSizeStr);
	}
    realTotalSize += ioVecs[0].iov_len;

    unsigned long written=0;
    if ( !blocking )
	written = connData.fWriteData.fWritten;

    int ret = WriteToConnection( connData.fSocket, pwd.fBlockCnt+1, ioVecs, blocking, written );
    if ( ret )
	{
	LOG( MLUCLog::kError, "TCPHOMERDataProvider::WriteToConnection", "Write error" )
	    << "Write error writing data to connection with socket " << MLUCLog::kDec
	    << connData.fSocket << ": " << strerror(ret) << " (" << ret << ")." << ENDLOG;
	}
    else if ( !blocking && written == realTotalSize )
	{
	Release( connData.fWriteData.fPublic );
	connData.fWriteData.fPublic = NULL;
	connData.fWriteData.fWritten = 0;
	}
    else if ( !blocking )
	{
	connData.fWriteData.fWritten = written;
	}
    
    return ret;
    }

int TCPHOMERDataProvider::WriteToConnection( int connSock, unsigned blockcnt, iovec* blocks, bool blocking, unsigned long& szWritten )
    {
    unsigned nextBuffer = 0;
    uint32 toWrite[ blockcnt ];
    toWrite[0] = blocks[0].iov_len;
	LOG( MLUCLog::kDebug, "TCPHOMERDataProvider::WriteToConnection", "Blocks" )
	    << "Block 0 size: " << MLUCLog::kDec
	    << blocks[0].iov_len << " - to write: " << toWrite[0]
	    << ENDLOG;
    for ( unsigned ic=1; ic<blockcnt; ic++ )
	{
	toWrite[ic] = toWrite[ic-1]+blocks[ic].iov_len;
	LOG( MLUCLog::kDebug, "TCPHOMERDataProvider::WriteToConnection", "Blocks" )
	    << "Block " << MLUCLog::kDec << ic << " size: " 
	    << blocks[ic].iov_len << " - to write: " << toWrite[ic]
	    << ENDLOG;
	}
    uint32 totalToWrite = toWrite[blockcnt-1];
    bool retry=false;
    unsigned tempCount, bi, tempOffset;
    struct iovec ioVecs[blockcnt];
    bool first = true;
    int ret, retval = 0;

    if ( szWritten >= totalToWrite )
	return 0;
    while ( nextBuffer<blockcnt && toWrite[nextBuffer]<=szWritten )
	nextBuffer++;
    LOG( MLUCLog::kDebug, "TCPHOMERDataProvider::WriteToConnection", "Start/Resume" )
	<< "szWritten: " << MLUCLog::kDec << szWritten << " - nextBuffer: "
	<< nextBuffer << ENDLOG;

    while ( szWritten < totalToWrite )
	{
	retry = true;
	retval=0;
	do
	    {
	    tempCount=0;
	    bi = nextBuffer;
	    if ( bi>0 )
		tempOffset = szWritten-toWrite[bi-1];
	    else
		tempOffset = szWritten;
	    while ( bi<blockcnt )
		{
		LOG( MLUCLog::kDebug, "TCPHOMERDataProvider::WriteToConnection", "Buffer offsets" )
		    << "Block " << MLUCLog::kDec << (unsigned)bi << "(" << tempCount << ") is being written from offset "
		    << tempOffset 
		    << " - Length to be written: " << (unsigned)(blocks[bi].iov_len-tempOffset) << " of "
		    << (unsigned)(blocks[bi].iov_len) << " bytes." << ENDLOG;
		ioVecs[tempCount].iov_base = (void*)( ((uint8*)blocks[bi].iov_base)+tempOffset );
		ioVecs[tempCount].iov_len = blocks[bi].iov_len-tempOffset;
		tempCount++;
		bi++;
		tempOffset = 0;
		}
	    ret = writev( connSock, ioVecs, tempCount );
	    LOG( MLUCLog::kDebug, "TCPHOMERDataProvider::WriteToConnection", "writev" )
		<< "writev return value: " << MLUCLog::kDec << ret << ENDLOG;
	    if ( ret > 0 )
		{
		first = true;
		szWritten += ret;
		if ( szWritten<totalToWrite )
		    {
		    while ( nextBuffer<blockcnt && toWrite[nextBuffer]<=szWritten )
			nextBuffer++;
		    }
		}
	    if ( ret==0 && !first )
		{
		retval = EPIPE;
		ret = -1;
		LOG( MLUCLog::kError, "TCPHOMERDataProvider::WriteToConnection", "Write error" )
		    << "Connection error on writev call: " << strerror(retval) << " (" 
		    << MLUCLog::kDec << retval << ")." << ENDLOG;
		break;
		}
	    if ( ret < 0 && errno!=EAGAIN && errno!=EINTR )
		{
		retval = errno;
		LOG( MLUCLog::kError, "TCPHOMERDataProvider::WriteToConnection", "Write error" )
		    << "Error on writev call: " << strerror(retval) << " (" 
		    << MLUCLog::kDec << retval << ")." << ENDLOG;
		break;
		}
	    if ( ret==0 || (ret < 0 && errno==EAGAIN) || !blocking )
		retry = false;
	    }
	while ( retry && szWritten < totalToWrite );
	if ( szWritten >= totalToWrite )
	    break;
	if ( retval != 0 )
	    break;
	if ( !blocking )
	    break;
	do
	    {
	    fd_set sockets;
	    struct timeval tv, *ptv;
	    FD_ZERO( &sockets );
	    FD_SET( connSock, &sockets );
	    if ( fTimeout )
		{
		tv.tv_sec = fTimeout/1000000;
		tv.tv_usec = fTimeout-tv.tv_sec*1000000;
		ptv = &tv;
		}
	    else
		ptv = NULL;
	    errno = 0;
	    ret = select( connSock+1, NULL, &sockets, NULL, ptv );
	    }
	while ( ret<=0 && (errno==EINTR || errno==EAGAIN) );
	if ( ret == 0 )
	    {
	    LOG( MLUCLog::kError, "TCPHOMERDataProvider::WriteToConnection", "Timeout expired" )
		<< "Timeout expired while trying to do transfer: " << strerror(errno) << " (" 
		    << MLUCLog::kDec << errno << ")." << ENDLOG;
	    retval = ETIMEDOUT;
	    break;
	    }
	if ( ret < 0 )
	    {
	    LOG( MLUCLog::kError, "TCPHOMERDataProvider::WriteToConnection", "Select error" )
		<< "Error on select while trying to do transfer: " << strerror(errno) << " (" 
		<< MLUCLog::kDec << errno << ")." << ENDLOG;
	    retval =  ENXIO;
	    break;
	    }
	first = false;
	}
    return retval;
    }


iovec* TCPHOMERDataProvider::CloneBlocks( unsigned blockCnt, const iovec* blocks )
    {
    if ( !blockCnt || ! blocks )
	return NULL;
    iovec* newBlocks = reinterpret_cast<iovec*>( fAllocCache.Get( blockCnt*sizeof(iovec) ) );
    if ( !newBlocks )
	return NULL;
    memset( newBlocks, 0, blockCnt*sizeof(iovec) );
    for ( unsigned n = 0; n < blockCnt; n++ )
	{
	newBlocks[n].iov_base = reinterpret_cast<void*>( fAllocCache.Get( blocks[n].iov_len ) );
	if ( !newBlocks[n].iov_base )
	    {
	    ReleaseBlocks( n, newBlocks );
	    return NULL;
	    }
	memcpy( newBlocks[n].iov_base, blocks[n].iov_base, blocks[n].iov_len );
	newBlocks[n].iov_len = blocks[n].iov_len;
	}
    return newBlocks;
    }

void TCPHOMERDataProvider::ReleaseBlocks( unsigned blockCnt, iovec* blocks )
    {
    if ( blocks )
	{
	for ( unsigned n = 0; n < blockCnt; n++ )
	    {
	    if ( blocks[n].iov_base )
		fAllocCache.Release( (uint8*)blocks[n].iov_base );
	    }
	fAllocCache.Release( (uint8*)blocks );
	}
    }

void TCPHOMERDataProvider::Release( TPublicWriteData* pwd )
    {
    if ( pwd )
	{
	pthread_mutex_lock( &fWriteDataMutex );
	pwd->fConnectionsDone++;
	if ( pwd->fConnectionsDone == pwd->fConnections )
	    {
	    ReleaseBlocks( pwd->fBlockCnt, pwd->fBlocks );
	    fAllocCache.Release( (uint8*)pwd );
	    }
	pthread_mutex_unlock( &fWriteDataMutex );
	}
    }



class DataProvider: public TCPHOMERDataProvider
    {
    public:
	DataProvider( unsigned short portNr, bool allowBlockingMode, MLUCConditionSem<int>& condSem ):
	    TCPHOMERDataProvider( portNr, allowBlockingMode ),
	    fConditionSem( condSem )
		{
		}

	virtual void EventRequested( bool /*eventStream*/ )
		{
		LOG( MLUCLog::kDebug, "DataProvider::EventRequested", "Event requested" )
		    << "Event requested." << ENDLOG;
		fConditionSem.Signal();
		LOG( MLUCLog::kDebug, "DataProvider::EventRequested", "Event request forwarded" )
		    << "Event request forwarded." << ENDLOG;
		};
	virtual void StopEvents() {};

    protected:
	MLUCConditionSem<int>& fConditionSem;
    };


bool gQuit=false;

void QuitSignalHandler( int sig );

void QuitSignalHandler( int )
{
    gQuit = true;
}

  

struct BlockData
    {
	homer_uint64 fDescriptor[kCount_64b_Words];
	MLUCString fFilename;
	uint8* fData;
    };

struct EventData
    {
	homer_uint64 fDescriptor[kCount_64b_Words];
	std::vector<BlockData> fBlocks;
	EventData()
		{
		}
    };

int main( int argc, char** argv )
    {
    unsigned short portNr=~(unsigned short)0;
    bool portNrSet = false;

    MLUCStdoutLogServer stdoutLog;
    gLog.AddServer( stdoutLog );

    signal( SIGQUIT, QuitSignalHandler );

    int inFH = -1;
    int i = 1;
    const char* error = NULL;
    int errorArg = -1;
    int errorParam = -1;
    std::vector<EventData> events;
    EventData curEvent;
    BlockData curBlock;
    HOMERBlockDescriptor homerDescriptor( curBlock.fDescriptor );
    homer_uint64 dataType;
    homer_uint32 dataOrig = ~(homer_uint32)0;
    homer_uint32 dataSpec = ~(homer_uint32)0;
    memset( &dataType, '*', 8 );
    memset( &dataOrig, '*', 4 );
    memset( curBlock.fDescriptor, 0, sizeof(homer_uint64)*kCount_64b_Words );
    homerDescriptor.Initialize();
    homerDescriptor.SetType( dataType );
    homerDescriptor.SetSubType1( dataOrig );
    homerDescriptor.SetSubType2( dataSpec );
    curBlock.fFilename = "";
    curBlock.fData = NULL;
    MLUCString errorStringText;

    while ( i < argc )
	{
	if ( !strcmp( "-infile", argv[i] ) )
	    {
	    if ( i+1>=argc )
		{
		error = "Missing input filename";
		errorArg = i;
		break;
		}
	    inFH = open( argv[i+1], O_RDONLY );
	    if ( inFH == -1 )
		{
		error = "Unable to open input file for reading";
		errorArg = i;
		errorParam = i+1;
		break;
		}
	    close( inFH );
	    curBlock.fFilename = argv[i+1];
	    curEvent.fBlocks.push_back( curBlock );
	    curBlock.fFilename = "";
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-datatype" ) )
	    {
	    if ( i+2 >= argc )
		{
		error = "Missing data type or origin specifier";
		errorArg = i;
		break;
		}
	    unsigned int j;
	    for ( j = 0; j < strlen(argv[i+1]) && j < 8; j++ )
		((uint8*)(&dataType))[7-j] = argv[i+1][j];
	    for ( ; j < 8; j++ )
		((uint8*)(&dataType))[7-j] = ' ';
	    for ( j = 0; j < strlen(argv[i+2]) && j < 4; j++ )
		((uint8*)(&dataOrig))[3-j] = argv[i+2][j];
	    for ( ; j < 4; j++ )
		((uint8*)(&dataOrig))[3-j] = ' ';
	    homerDescriptor.SetType( dataType );
	    homerDescriptor.SetSubType1( dataOrig );
	    i += 3;
	    continue;
	    }
	if ( !strcmp( argv[i], "-dataspec" ) )
	    {
	    if ( i+1 >= argc )
		{
		error = "Missing data specification specifier";
		errorArg = i;
		break;
		}
	    char* cpErr;
	    dataSpec = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		error = "Error converting specification specifier";
		errorArg = i;
		errorParam = i+1;
		break;
		}
	    homerDescriptor.SetSubType2( dataSpec );
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-port" ) )
	    {
	    if ( i+1 >= argc )
		{
		error = "Missing port number specifier";
		errorArg = i;
		break;
		}
	    char* cpErr;
	    portNr = (unsigned short)strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		error = "Error converting port number specifier";
		errorArg = i;
		errorParam = i+1;
		break;
		}
	    portNrSet = true;
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-V" ) )
	    {
	    if ( i+1 >= argc )
		{
		error = "Missing verbosity specifier";
		errorArg = i;
		break;
		}
	    char* cpErr;
	    MLUCLog::gLogLevel = (MLUCLog::TLogLevel)strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		error = "Error converting verbosity specifier";
		errorArg = i;
		errorParam = i+1;
		break;
		}
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-nextevent" ) )
	    {
	    events.push_back( curEvent );
	    curEvent.fBlocks.clear();
	    memset( curBlock.fDescriptor, 0, sizeof(homer_uint64)*kCount_64b_Words );
	    homerDescriptor.Initialize();
	    homerDescriptor.SetType( dataType );
	    homerDescriptor.SetSubType1( dataOrig );
	    homerDescriptor.SetSubType2( dataSpec );
	    i += 1;
	    continue;
	    }
	if ( !strcmp( argv[i], "-commandfile" ) )
	    {
	    if ( i+1 >= argc )
		{
		error = "Missing file name specifier";
		errorArg = i;
		break;
		}
	    std::ifstream infile( argv[i+1] );
	    if ( !infile.good() )
		{
		error = "Cannot open file";
		errorArg = i;
		errorParam = i+1;
		break;
		}
	    unsigned long lineNr=0;
	    while ( infile.good() && !error )
		{
		++lineNr;
		MLUCString line;
		char tmpBuf[1024];
		do
		    {
		    memset( tmpBuf, 0, 1024 );
		    infile.getline( tmpBuf, 1024 );
		    line += tmpBuf;
		    }
		while ( infile.gcount()==1024 && tmpBuf[1022]!='\n' && infile.good() );
		if ( !infile.good() && !infile.eof() )
		    break;
		vector<MLUCString> args;
		if ( MLUCCmdlineParser::ParseCmd( line.c_str(), args ) && args.size()>0 )
		    {
		    if ( args[0]=="infile" )
			{
			if ( 1>=args.size() )
			    {
			    char tmpText[512];
			    errorStringText = "Error parsing command input file '";
			    errorStringText += argv[i+1];
			    errorStringText += "' line ";
			    sprintf( tmpText, "%lu", lineNr );
			    errorStringText += tmpText;
			    errorStringText += " (command 'infile')";
			    errorStringText += ": ";
			    errorStringText += "Missing input filename";
			    error = errorStringText.c_str();
			    break;
			    }
			inFH = open( args[1].c_str(), O_RDONLY );
			if ( inFH == -1 )
			    {
			    char tmpText[512];
			    errorStringText = "Error parsing command input file '";
			    errorStringText += argv[i+1];
			    errorStringText += "' line ";
			    sprintf( tmpText, "%lu", lineNr );
			    errorStringText += tmpText;
			    errorStringText += " (command 'infile')";
			    errorStringText += ": ";
			    errorStringText += "Unable to open input file for reading";
			    error = errorStringText.c_str();
			    break;
			    }
			close( inFH );
			curBlock.fFilename = args[1];
			curEvent.fBlocks.push_back( curBlock );
			curBlock.fFilename = "";
			}
		    if ( args[0]=="datatype" )
			{
			if ( 2 >= args.size() )
			    {
			    char tmpText[512];
			    errorStringText = "Error parsing command input file '";
			    errorStringText += argv[i+1];
			    errorStringText += "' line ";
			    sprintf( tmpText, "%lu", lineNr );
			    errorStringText += tmpText;
			    errorStringText += " (command 'infile')";
			    errorStringText += ": ";
			    errorStringText += "Missing data type or origin specifier";
			    error = errorStringText.c_str();
			    break;
			    }
			unsigned int j;
			for ( j = 0; j < strlen(args[1].c_str()) && j < 8; j++ )
			    ((uint8*)(&dataType))[7-j] = args[1][j];
			for ( ; j < 8; j++ )
			    ((uint8*)(&dataType))[7-j] = ' ';
			for ( j = 0; j < strlen(args[2].c_str()) && j < 4; j++ )
			    ((uint8*)(&dataOrig))[3-j] = args[2][j];
			for ( ; j < 4; j++ )
			    ((uint8*)(&dataOrig))[3-j] = ' ';
			homerDescriptor.SetType( dataType );
			homerDescriptor.SetSubType1( dataOrig );
			}
		    if ( args[0]=="dataspec" )
			{
			if ( 1 >= args.size() )
			    {
			    char tmpText[512];
			    errorStringText = "Error parsing command input file '";
			    errorStringText += argv[i+1];
			    errorStringText += "' line ";
			    sprintf( tmpText, "%lu", lineNr );
			    errorStringText += tmpText;
			    errorStringText += " (command 'infile')";
			    errorStringText += ": ";
			    errorStringText += "Missing data specification specifier";
			    error = errorStringText.c_str();
			    break;
			    }
			char* cpErr;
			dataSpec = strtoul( args[1].c_str(), &cpErr, 0 );
			if ( *cpErr )
			    {
			    char tmpText[512];
			    errorStringText = "Error parsing command input file '";
			    errorStringText += argv[i+1];
			    errorStringText += "' line ";
			    sprintf( tmpText, "%lu", lineNr );
			    errorStringText += tmpText;
			    errorStringText += " (command 'infile')";
			    errorStringText += ": ";
			    errorStringText += "Error converting specification specifier";
			    error = errorStringText.c_str();
			    break;
			    }
			homerDescriptor.SetSubType2( dataSpec );
			}
		    if ( args[0]=="nextevent" )
			{
			events.push_back( curEvent );
			curEvent.fBlocks.clear();
			memset( curBlock.fDescriptor, 0, sizeof(homer_uint64)*kCount_64b_Words );
			homerDescriptor.Initialize();
			homerDescriptor.SetType( dataType );
			homerDescriptor.SetSubType1( dataOrig );
			homerDescriptor.SetSubType2( dataSpec );
			}
		    }
		}
	    if ( !infile.good() && !infile.eof() )
		{
		error = "Error reading from file";
		errorArg = i;
		errorParam = i+1;
		break;
		}
	    i += 2;
	    continue;
	    }
	error = "Unknown argument";
	errorArg = i;
	errorParam = -1;
	break;
	}
    if ( !error )
	{
	if ( !portNrSet )
	    {
	    error = "No port number specified (e.g. via -port command line argument)";
	    }
	}
    if ( error )
	{
	fprintf( stderr, "Usage: %s -port <portnr> (-datatype <data-type> <data-origin>) (-dataspec <data-specification>) (-V <verbostity>) (-infile <input-file-name>) (-nextevent) (-commandfile <commandfilename>)\n", argv[0] );
 	fprintf( stderr, "Error: %s\n", error );
	if ( errorArg>=0 )
	    fprintf( stderr, "Offending argument: %s\n", argv[errorArg] );
	if ( errorParam>=0 )
	    fprintf( stderr, "Offending parameter: %s\n", argv[errorParam] );
	return -1;
	}
    if ( curEvent.fBlocks.size()>0 )
	events.push_back( curEvent );

    

    MLUCConditionSem<int> signal;
    signal.Unlock();
    DataProvider provider( portNr, false, signal );

    provider.Start();

    unsigned long evtNr = 0;

    signal.Lock();
    while ( !gQuit )
	{
	if ( signal.Wait( 500 ) /* && !gQuit*/ )
	    {
	    LOG( MLUCLog::kDebug, "TCPHOMERDataProvider main", "Readout requested" )
		<< "Readout of one event requested." << ENDLOG;
	    HOMERWriter writer;
	    unsigned long evtNdx = evtNr % events.size();
	    vector<BlockData>::iterator iter, end;
	    iter = events[evtNdx].fBlocks.begin();
	    end = events[evtNdx].fBlocks.end();
	    while ( iter != end )
		{
		homerDescriptor.UseHeader( iter->fDescriptor );
		inFH = open( iter->fFilename.c_str(), O_RDONLY );
		if ( inFH == -1 )
		    {
		    fprintf( stderr, "Error opening file '%s': '%s' (%d)\n",
			     iter->fFilename.c_str(),
			     strerror(errno), errno );
		    homerDescriptor.SetBlockSize( 0 );
		    iter->fData = NULL;
		    }
		else
		    {
		    off_t sz = lseek( inFH, 0, SEEK_END );
		    lseek( inFH, 0, SEEK_SET );
		    iter->fData = new uint8[ sz ];
		    if ( !iter->fData )
			{
			homerDescriptor.SetBlockSize( 0 );
			close( inFH );
			}
		    else
			{
			unsigned long curSize = 0;
			int ret;
			do
			    {
			    ret = read( inFH, iter->fData+curSize, sz-curSize );
			    if ( ret >= 0 )
				{
				curSize += (unsigned long)ret;
				if ( curSize >= (unsigned long)sz )
				    break;
				}
			    else
				{
				fprintf( stderr, "%s error reading data from input file after %lu bytes.\n", 
					 argv[0], curSize );
				sz = curSize;
				}
			    }
			while ( ret >0 );
			homerDescriptor.SetBlockSize( sz );
			close( inFH );
			}
		    }
		writer.AddBlock( iter->fDescriptor, iter->fData );
		iter++;
		}

	    uint8* allData = NULL;
	    unsigned long totalSize = writer.GetTotalMemorySize();
	    allData = new uint8[totalSize];
	    if ( allData )
		{
		writer.Copy( allData, 2, evtNr, 0, ~(uint32)0 );
		struct iovec ioBlock;
		ioBlock.iov_base = allData;
		ioBlock.iov_len = totalSize;
		provider.Write( 1, &ioBlock );
		delete [] allData;
		allData = NULL;
		}
	    iter = events[evtNdx].fBlocks.begin();
	    end = events[evtNdx].fBlocks.end();
	    while ( iter != end )
		{
		if ( iter->fData )
		    {
		    delete [] iter->fData;
		    iter->fData = NULL;
		    }
		homerDescriptor.SetBlockSize( 0 );
		iter++;
		}
	    evtNr++;
	    }

	}
    signal.Unlock();


    provider.Quit();
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 2000000;
    gettimeofday( &start, NULL );
    while ( deltaT<timeLimit && !provider.Quitted() )
	{
	usleep(0);
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}

    return 0;
    }


/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: TCPHOMERDataProvider.cpp 497 2005-12-02 08:56:24Z timm $ 
**
***************************************************************************
*/
