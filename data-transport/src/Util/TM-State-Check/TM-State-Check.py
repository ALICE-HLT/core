#!/usr/bin/env python


import string, sys, os, os.path, time, datetime

uname=os.uname()
target = uname[0]+"-"+uname[4]+"-debug"
base = os.path.dirname( sys.argv[0] )
if os.path.basename( base )==target and os.path.basename( os.path.dirname( base ) )=="bin":
    sys.path.append( os.path.join( os.path.dirname( os.path.dirname( base ) ), "lib", target ) )
sys.path.append( os.path.join( base, "modules" ) )
sys.path.append( os.path.join( base, "modules", target+"-links" ) )
target = uname[0]+"-"+uname[4]+"-releasedebug"
sys.path.append( os.path.join( base, "modules", target+"-links" ) )

import TMControl

def UsageErrorExit( msg ):
    print "Usage: "+sys.argv[0]+" -address <local-address> -control <remote-address> (-runtime <running_time_s>) (-iterations <iteration-count>) (-sendrunnr) (-startparameters <parameters for start command (ECS CONFIGURE)>) (-connectparameters <parameters for connect command (ECS ENGAGE)>) (-poststartruncommand <command to execute after reaching running state>) (-prestopruncommand <command to execute prior to executing stop_run command>) (-posterrorcommand <command to execute after an error has been encountered>)"
    print msg
    sys.exit(1)

args = sys.argv[1:]
address = None
remoteAddress = None
running_time_s = 0
iterations = 0
sendRunNr = False
params = { "startparams":"", "connectparams":"", "runnumber":"" }
postStartRunCmd = None
preStopRunCmd = None
postErrorCmd = None

while len(args)>0:
    if args[0]=="-address":
        if len(args)<2:
            UsageErrorExit( "Missing -address parameter" )
        address = args[1]
        args=args[2:]
    elif args[0]=="-control":
        if len(args)<2:
            UsageErrorExit( "Missing -control parameter" )
        remoteAddress = args[1]
        args=args[2:]
    elif args[0]=="-runtime":
        if len(args)<2:
            UsageErrorExit( "Missing -runtime parameter" )
        try:
            running_time_s = int( args[1],0 )
        except ValueError:
            UsageErrorExit( "Error converting -runtime parameter" )
        args=args[2:]
    elif args[0]=="-iterations":
        if len(args)<2:
            UsageErrorExit( "Missing -iterations parameter" )
        try:
            iterations = int( args[1],0 )
        except ValueError:
            UsageErrorExit( "Error converting -iterations parameter" )
        args=args[2:]
    elif args[0]=="-sendrunnr":
        sendRunNr = True
        args=args[1:]
    elif args[0]=="-startparameters":
        if len(args)<2:
            UsageErrorExit( "Missing -startparameters parameter" )
        params["startparams"] = args[1]
        args=args[2:]
    elif args[0]=="-connectparameters":
        if len(args)<2:
            UsageErrorExit( "Missing -connectparameters parameter" )
        params["connectparams"] = args[1]
        args=args[2:]
    elif args[0]=="-poststartruncommand":
        if len(args)<2:
            UsageErrorExit( "Missing -poststartruncommand parameter" )
        postStartRunCmd = args[1]
        args=args[2:]
    elif args[0]=="-prestopruncommand":
        if len(args)<2:
            UsageErrorExit( "Missing -prestopruncommand parameter" )
        preStopRunCmd = args[1]
        args=args[2:]
    elif args[0]=="-posterrorcommand":
        if len(args)<2:
            UsageErrorExit( "Missing -posterrorcommand parameter" )
        postErrorCmd = args[1]
        args=args[2:]
    else:
        UsageErrorExit( "Unknown parameter "+args[0] )
    

if address == None:
    UsageErrorExit( "Local address has to be specified (e.g. via -address parameter" )
if remoteAddress == None:
    UsageErrorExit( "Remote address to be controlled has to be specified (e.g. via -control parameter" )



##if len(sys.argv)<3 or len(sys.argv)>5:
##    print "Usage: "+sys.argv[0]+" <local-address> <remote-address> (<running_time_s>) (<iteration-count>)"
##    sys.exit(1)

##if len(sys.argv)>=4:
##    running_time_s=int(sys.argv[3])
##else:
##    running_time_s=0

##if len(sys.argv)==5:
##    iterations=int(sys.argv[4])
##else:
##    iterations=0
    
control=TMControl.TMControl( address, remoteAddress )
control.SetSleepTime( 0.25 )
control.SetQuiet( False )
control.Init()
control.SetCommandVariables( params )

#allowed_state_wait_count=240
#allowed_state_wait_count=600
allowed_state_wait_count=1200
#allowed_state_wait_count=10000
#pause_resume_iter_count=2
pause_resume_iter_count=0

state="slaves_dead"
cnt=0
expected_state=state


state_list_start=[
    [ [ "slaves_dead" ], [ [ "slaves_dead", "start_slaves", allowed_state_wait_count, 0 ], [ "starting_slaves", None, allowed_state_wait_count, 0 ], [ "TIMEOUT", None, allowed_state_wait_count, 0 ], [ "", None, allowed_state_wait_count, 0 ] ], ["processes_dead"] ],
    [ [ "processes_dead" ], [ [ "processes_dead", "start${startparams}", allowed_state_wait_count, 0 ], [ "starting", None, allowed_state_wait_count, 0 ], [ "TIMEOUT", None, allowed_state_wait_count, 0 ], [ "", None, allowed_state_wait_count, 0 ] ], ["ready_local"] ],
    [ [ "ready_local" ], [ [ "ready_local", "connect${connectparams}", allowed_state_wait_count, 0 ], [ "connecting", None, allowed_state_wait_count, 0 ], [ "TIMEOUT", None, allowed_state_wait_count, 0 ], [ "", None, allowed_state_wait_count, 0 ] ], ["ready"] ],
    [ [ "ready" ], [ [ "ready", "start_run", allowed_state_wait_count, 0 ], [ "starting_run", None, allowed_state_wait_count, 0 ], [ "TIMEOUT", None, allowed_state_wait_count, 0 ], [ "", None, allowed_state_wait_count, 0 ] ], ["running", "busy"] ],
    ]

state_list_stop=[
    [ [ "running", "paused", "busy" ], [ [ "running", "stop_run", allowed_state_wait_count, 0 ], [ "busy", "stop_run", allowed_state_wait_count, 0 ], [ "paused", "stop_run", allowed_state_wait_count, 0 ], [ "stopping_run", None, allowed_state_wait_count, 0 ], [ "TIMEOUT", None, allowed_state_wait_count, 0 ], [ "", None, allowed_state_wait_count, 0 ] ], ["ready"] ],
    [ [ "ready" ], [ [ "ready", "disconnect", allowed_state_wait_count, 0 ], [ "disconnecting", None, allowed_state_wait_count, 0 ], [ "TIMEOUT", None, allowed_state_wait_count, 0 ], [ "", None, allowed_state_wait_count, 0 ] ], ["ready_local"] ],
    [ [ "ready_local" ], [ [ "ready_local", "stop", allowed_state_wait_count, 0 ], [ "stopping", None, allowed_state_wait_count, 0 ], [ "TIMEOUT", None, allowed_state_wait_count, 0 ], [ "", None, allowed_state_wait_count, 0 ] ], ["processes_dead"] ],
    [ [ "processes_dead" ], [ [ "processes_dead", "kill_slaves", allowed_state_wait_count, 0 ], [ "deinitializing", None, allowed_state_wait_count, 0 ], [ "TIMEOUT", None, allowed_state_wait_count, 0 ], [ "", None, allowed_state_wait_count, 0 ], [ "slave_kill", None, allowed_state_wait_count, 0 ] ], ["slaves_dead"] ],
    ]

expected_states=state_list_start[0][0]
state=control.QueryStateChecked()

while state in expected_states and (not iterations or cnt<iterations):
    now = datetime.datetime.now()
    print "Iteration",cnt,now.isoformat()
    # Go from slaves_dead to running
    if sendRunNr:
        params["runnumber"] = ";RUN_NUMBER=%u" % (cnt+1)
    else:
        params["runnumber"] = ""
    for sl in state_list_start:
        expected_states=sl[0]
        if not state in expected_states:
            break
        expected_states=sl[2]
        state = control.Control( sl[1], expected_states, 0 )
        if not state in expected_states:
            break
        
    if not state in expected_states:
        break

    if postStartRunCmd!=None:
        os.system( postStartRunCmd )

    if pause_resume_iter_count:
        # Alternate between running and paused a couple of times
        # Every second iteration stay in paused and let the stopping
        # process below start from paused instead of running
        if cnt % 2:
            expected_states=[ "paused" ]
            state = control.Control( [ [ "running", "pause", allowed_state_wait_count, 0 ], [ "busy", "pause", allowed_state_wait_count, 0 ], [ "pausing", None, allowed_state_wait_count, 0 ] ], expected_states, 0 )
            if not state in expected_states:
                break
            for i in range(pause_resume_iter_count):
                expected_states=[ "running", "busy" ]
                state = control.Control( [ [ "paused", "resume", allowed_state_wait_count, 0 ], [ "resuming", None, allowed_state_wait_count, 0 ] ], expected_states, 0 )
                if not state in expected_states:
                    break
                expected_states=[ "paused" ]
                state = control.Control( [ [ "running", "pause", allowed_state_wait_count, 0 ], [ "busy", "pause", allowed_state_wait_count, 0 ], [ "pausing", None, allowed_state_wait_count, 0 ] ], expected_states, 0 )
                if not state in expected_states:
                    break
                pass
        else:
            for i in range(pause_resume_iter_count):
                expected_states=[ "paused" ]
                state = control.Control( [ [ "running", "pause", allowed_state_wait_count, 0 ], [ "busy", "pause", allowed_state_wait_count, 0 ], [ "pausing", None, allowed_state_wait_count, 0 ] ], expected_states, 0 )
                if not state in expected_states:
                    break
                expected_states=[ "running", "busy" ]
                state = control.Control( [ [ "paused", "resume", allowed_state_wait_count, 0 ], [ "resuming", None, allowed_state_wait_count, 0 ] ], expected_states, 0 )
                if not state in expected_states:
                    break

    if not state in expected_states:
        break

    if running_time_s:
        print "Allowing processing for",running_time_s," s"
        time.sleep( running_time_s )
        print "Stopping processing"
        
    if preStopRunCmd!=None:
        os.system( preStopRunCmd )

    # Go from running or paused to slaves_dead
    for sl in state_list_stop:
        expected_states=sl[0]
        if not state in expected_states:
            break
        expected_states=sl[2]
        state = control.Control( sl[1], expected_states, 0 )
        if not state in expected_states:
            break
    if not state in expected_states:
        break


        
    cnt=cnt+1
    if not iterations or cnt<iterations:
        time.sleep( 15 )

if not state in expected_states:
    print "Error: Expected state: '"+str(expected_states)+"' - Found state: '"+state+"' in iteration %d." % cnt
    control.SendCmd( "DUMPSTATES" )
    if postErrorCmd!=None:
        os.system( postErrorCmd )
    
control.Deinit()

if not state in expected_states:
    sys.exit(1)
else:
    sys.exit(0)
