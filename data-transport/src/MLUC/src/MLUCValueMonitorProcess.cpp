#include "MLUCString.hpp"
#include "MLUCValueMonitorProcess.hpp"

#include <stdio.h>

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

void MLUCValueMonitorProcess_Time::Init(int pid)
{
  fpid = pid;
  fFilename << "/proc/" << pid << "/stat";
  fFirstReadOut = true;
}

// -----------------------------------------------------------------------------

int MLUCValueMonitorProcess_Time::GetValue( uint64& MeasuredValue )
{
  FILE *file;
  int res;

  int  pid;
  char comm[128];
  char state;
  int  ppid, pgrp, session, tty, tpgid;
  unsigned long flags, min_flt, cmin_flt, maj_flt, cmaj_flt, utime, systime;
  long  cutime, csystime, priority, p_nice, timeout, it_real_value;
  unsigned long start_time, vsize;
  long rss;
  unsigned long rss_rlim, start_code, end_code, start_stack, kstk_esp, kstk_eip;
/*     signal, blocked, sigignore, sigcatch,   */ /* can't use */
  unsigned long wchan, nswap, cnswap;
  int/* , exit_signal  */ lproc;

#if __GNUC__ >= 3
  file = fopen(fFilename.str().c_str(), "r");
#else
  file = fopen(fFilename.str(), "r");
#endif
  if(!file) {
    LOG( MLUCLog::kError, "MLUCValueMonitorProcess_Time:GetValue","Error opening file" ) 
#if __GNUC__ >= 3
      << "Could not open "<< fFilename.str().c_str() << ENDLOG;
#else
    << "Could not open "<< fFilename.str() << ENDLOG;
#endif
    return ENOENT;
  }
  
  // read cpu user and system time from /proc/[id]/stat
  res = fscanf(file,
       "%d %s "
       "%c "
       "%d %d %d %d %d "
       "%lu %lu %lu %lu %lu %lu %lu "
       "%ld %ld %ld %ld %ld %ld "
       "%lu %lu "
       "%ld "
       "%lu %lu %lu %lu %lu %lu "
       "%*s %*s %*s %*s " /* discard, no RT signals & Linux 2.1 used hex */
       "%lu %lu %lu %*d %d",
       &pid, comm, 
       &state,
       &ppid, &pgrp, &session, &tty, &tpgid,
       &flags, &min_flt, &cmin_flt, &maj_flt, &cmaj_flt, &utime, &systime,
       &cutime, &csystime, &priority, &p_nice, &timeout, &it_real_value,
       &start_time, &vsize,
       &rss,
       &rss_rlim, &start_code, &end_code, &start_stack, &kstk_esp, &kstk_eip,
/*     signal, blocked, sigignore, sigcatch,   */ /* can't use */
       &wchan, &nswap, &cnswap /* , &exit_signal  */, &lproc);
  if(res != 34) {
    fclose(file);
    LOG( MLUCLog::kError, "MLUCValueMonitorProcess_Time:GetValue","Error reading file" )
#if __GNUC__ >= 3
      << "Could not read data from" << fFilename.str().c_str() << ENDLOG;    
#else
    << "Could not read data from" << fFilename.str() << ENDLOG;    
#endif
    return EINVAL;
  }

  MeasuredValue = (utime + systime) / 100;

  fclose(file);
  return 0;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

void MLUCValueMonitorProcess_CpuUsage::Init(int pid)
{

  fpid = pid;
  fFilename << "/proc/" << pid << "/stat";
  fFirstReadOut = true;
}

// -----------------------------------------------------------------------------

int MLUCValueMonitorProcess_CpuUsage::GetValue( uint64& MeasuredValue )
{
  FILE *file, *globalStatFile;
  int res;
  unsigned long user_val, nice_val, sys_val, idle_val;
  unsigned long TotalCpuTime, ProcessTime;

  int  pid;
  char comm[128];
  char state;
  int  ppid, pgrp, session, tty, tpgid;
  unsigned long flags, min_flt, cmin_flt, maj_flt, cmaj_flt, utime, systime;
  long  cutime, csystime, priority, p_nice, timeout, it_real_value;
  unsigned long start_time, vsize;
  long rss;
  unsigned long rss_rlim, start_code, end_code, start_stack, kstk_esp, kstk_eip;
  unsigned long wchan, nswap, cnswap;
  int exit_signal, lproc;

  // open file /proc/[id]/cpu 
#if __GNUC__ >=3
  file = fopen(fFilename.str().c_str(), "r");
#else
  file = fopen(fFilename.str(), "r");
#endif
  if(!file) {
    LOG( MLUCLog::kError, "MLUCValueMonitorProcess_CpuUsage:GetValue","Error opening file" ) 
#if __GNUC__ >=3
      << "Could not open " << fFilename.str().c_str() << ENDLOG;
#else
      << "Could not open " << fFilename.str() << ENDLOG;
#endif

    return ENOENT;
  }
  // open file /proc/stat
  globalStatFile = fopen("/proc/stat", "r");
  if(!globalStatFile) {
    fclose(file);
    LOG( MLUCLog::kError, "MLUCValueMonitorProcess_CpuUsage:GetValue",
	 "Error opening file" ) << "Could not open /proc/stat" << ENDLOG;
    return ENOENT;
  }

  // read cpu user and system time from /proc/[id]/stat
  res = fscanf
      (file,
       "%d %s "
       "%c "
       "%d %d %d %d %d "
       "%lu %lu %lu %lu %lu %lu %lu "
       "%ld %ld %ld %ld %ld %ld "
       "%lu %lu "
       "%ld "
       "%lu %lu %lu %lu %lu %lu "
       "%lu %lu %lu "
       "%d %d",
       &pid, 
       comm, 
       &state,
       &ppid, &pgrp, &session, &tty, &tpgid,
       &flags, &min_flt, &cmin_flt, &maj_flt, &cmaj_flt, &utime, &systime,
       &cutime, &csystime, &priority, &p_nice, &timeout, &it_real_value,
       &start_time, &vsize,
       &rss,
       &rss_rlim, &start_code, &end_code, &start_stack, &kstk_esp, &kstk_eip,
       &wchan, &nswap, &cnswap, &exit_signal, &lproc);
  if(res != 35) {
    fclose(globalStatFile);
    fclose(file);
    LOG( MLUCLog::kError, "MLUCValueMonitorProcess_CpuUsage:GetValue","Error reading file" )
#if __GNUC__ >=3
      << "Could not read data from"<< fFilename.str().c_str() << ENDLOG; 
#else
      << "Could not read data from"<< fFilename.str() << ENDLOG; 
#endif
    return EINVAL;
  }

  // read global cpu time from /proc/stat
  res = fscanf(globalStatFile, "cpu %lu %lu %lu %lu ",
	       &user_val, &nice_val, &sys_val, &idle_val);
  if(res != 4) { 
    fclose(globalStatFile);
    fclose(file);
    LOG( MLUCLog::kError, "MLUCValueMonitorProcess_CpuUsage:GetValue",
	 "Error reading file" ) << "Could not read data from /proc/stat"
                                << ENDLOG;
    return EINVAL;
  }

  fclose(globalStatFile);
  fclose(file);

  TotalCpuTime = user_val + nice_val + sys_val + idle_val;
  ProcessTime = utime + systime;
  
  if(fFirstReadOut) {
    fOldTotalCpuTime = TotalCpuTime;
    fOldProcessTime  = ProcessTime;
    fFirstReadOut    = false;
    return ENOTSUP; // first readout (op not supported)
  } 
  
  MeasuredValue = (uint64) ((ProcessTime - fOldProcessTime) * 100.0 / 
			    (float)(TotalCpuTime - fOldTotalCpuTime) + .5);
  //MeasuredValue = (ProcessTime - fOldProcessTime) * 100 / 
  //                (TotalCpuTime - fOldTotalCpuTime);
  
  fOldTotalCpuTime = TotalCpuTime;
  fOldProcessTime  = ProcessTime;
  return 0;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

void MLUCValueMonitorProcess_CpuUsage_User::Init(int pid)
{

  fpid = pid;
  fFilename << "/proc/" << pid << "/stat";
  fFirstReadOut = true;
}

// -----------------------------------------------------------------------------

int MLUCValueMonitorProcess_CpuUsage_User::GetValue( uint64& MeasuredValue )
{
  FILE *file, *globalStatFile;
  int res;
  unsigned long user_val, nice_val, sys_val, idle_val;
  unsigned long TotalCpuTime, ProcessTime;

  int  pid;
  char comm[128];
  char state;
  int  ppid, pgrp, session, tty, tpgid;
  unsigned long flags, min_flt, cmin_flt, maj_flt, cmaj_flt, utime, systime;
  long  cutime, csystime, priority, p_nice, timeout, it_real_value;
  unsigned long start_time, vsize;
  long rss;
  unsigned long rss_rlim, start_code, end_code, start_stack, kstk_esp, kstk_eip;
  unsigned long wchan, nswap, cnswap;
  int exit_signal, lproc;

#if __GNUC__ >=3
  file = fopen(fFilename.str().c_str(), "r");
#else
  file = fopen(fFilename.str(), "r");
#endif
  if(!file) {
    LOG( MLUCLog::kError, "MLUCValueMonitorProcess_CpuUsage_User:GetValue","Error opening file" ) 
#if __GNUC__ >=3
      << "Could not open "<< fFilename.str().c_str() << ENDLOG;
#else
      << "Could not open "<< fFilename.str() << ENDLOG;
#endif
    return ENOENT;
  }
  
  // open file /proc/stat
  globalStatFile = fopen("/proc/stat", "r");
  if(!globalStatFile) {
    fclose(file);
    LOG( MLUCLog::kError, "MLUCValueMonitorProcess_CpuUsage_User:GetValue",
	 "Error opening file" ) << "Could not open /proc/stat" << ENDLOG;
    return ENOENT;
  }
  // read cpu system time from /proc/[id]/stat
  res = fscanf
      (file,
       "%d %s "
       "%c "
       "%d %d %d %d %d "
       "%lu %lu %lu %lu %lu %lu %lu "
       "%ld %ld %ld %ld %ld %ld "
       "%lu %lu "
       "%ld "
       "%lu %lu %lu %lu %lu %lu "
       "%lu %lu %lu "
       "%d %d",
       &pid, 
       comm, 
       &state,
       &ppid, &pgrp, &session, &tty, &tpgid,
       &flags, &min_flt, &cmin_flt, &maj_flt, &cmaj_flt, &utime, &systime,
       &cutime, &csystime, &priority, &p_nice, &timeout, &it_real_value,
       &start_time, &vsize,
       &rss,
       &rss_rlim, &start_code, &end_code, &start_stack, &kstk_esp, &kstk_eip,
       &wchan, &nswap, &cnswap, &exit_signal, &lproc);
  if(res != 35) {
    fclose(globalStatFile);
    fclose(file);
    LOG( MLUCLog::kError, "MLUCValueMonitorProcess_CpuUsage_User:GetValue","Error reading file" ) 
#if __GNUC__ >=3
      << "Could not read data from" << fFilename.str().c_str() << ENDLOG; 
#else
      << "Could not read data from" << fFilename.str() << ENDLOG; 
#endif
    return EINVAL;
  }

  // read global cpu time from /proc/stat
  res = fscanf(globalStatFile, "cpu %lu %lu %lu %lu ",
	       &user_val, &nice_val, &sys_val, &idle_val);
  if(res != 4) { 
    fclose(globalStatFile);
    fclose(file);
    LOG( MLUCLog::kError, "MLUCValueMonitorProcess_CpuUsage_User:GetValue",
	 "Error reading file" ) << "Could not read data from /proc/stat"
                                << ENDLOG;
    return EINVAL;
  }

  fclose(globalStatFile);
  fclose(file);

  
  TotalCpuTime = user_val + nice_val + sys_val + idle_val;
  ProcessTime = utime;
  
  if(fFirstReadOut) {
    fOldTotalCpuTime = TotalCpuTime;
    fOldProcessTime  = ProcessTime;
    fFirstReadOut    = false;
    return ENOTSUP; // first readout (op not supported)
  } 
  
  MeasuredValue = (uint64) ((ProcessTime - fOldProcessTime) * 100.0 / 
			    (float)(TotalCpuTime - fOldTotalCpuTime) + .5);

  fOldTotalCpuTime = TotalCpuTime;
  fOldProcessTime  = ProcessTime;
  return 0;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

void MLUCValueMonitorProcess_CpuUsage_System::Init(int pid)
{

  fpid = pid;
  fFilename << "/proc/" << pid << "/stat";
  fFirstReadOut = true;
}

// -----------------------------------------------------------------------------

int MLUCValueMonitorProcess_CpuUsage_System::GetValue( uint64& MeasuredValue )
{
  FILE *file, *globalStatFile;
  int res;
  unsigned long user_val, nice_val, sys_val, idle_val;
  unsigned long TotalCpuTime, ProcessTime;

  int  pid;
  char comm[128];
  char state;
  int  ppid, pgrp, session, tty, tpgid;
  unsigned long flags, min_flt, cmin_flt, maj_flt, cmaj_flt, utime, systime;
  long  cutime, csystime, priority, p_nice, timeout, it_real_value;
  unsigned long start_time, vsize;
  long rss;
  unsigned long rss_rlim, start_code, end_code, start_stack, kstk_esp, kstk_eip;
  unsigned long wchan, nswap, cnswap;
  int exit_signal, lproc;

#if __GNUC__ >=3
  file = fopen(fFilename.str().c_str(), "r");
#else
  file = fopen(fFilename.str(), "r");
#endif

  if(!file) {
    LOG( MLUCLog::kError, "MLUCValueMonitorProcess_CpuUsage_System:GetValue","Error opening file" ) 
#if __GNUC__ >=3
      << "Could not open "<< fFilename.str().c_str() << ENDLOG;
#else
      << "Could not open "<< fFilename.str() << ENDLOG;
#endif
    return ENOENT;
  }
  
  // open file /proc/stat
  globalStatFile = fopen("/proc/stat", "r");
  if(!globalStatFile) {
    fclose(file);
    LOG( MLUCLog::kError, "MLUCValueMonitorProcess_CpuUsage_System:GetValue",
	 "Error opening file" ) << "Could not open /proc/stat" << ENDLOG;
    return ENOENT;
  }
  // read cpu system time from /proc/[id]/stat
  res = fscanf
      (file,
       "%d %s "
       "%c "
       "%d %d %d %d %d "
       "%lu %lu %lu %lu %lu %lu %lu "
       "%ld %ld %ld %ld %ld %ld "
       "%lu %lu "
       "%ld "
       "%lu %lu %lu %lu %lu %lu "
       "%lu %lu %lu "
       "%d %d",
       &pid, 
       comm, 
       &state,
       &ppid, &pgrp, &session, &tty, &tpgid,
       &flags, &min_flt, &cmin_flt, &maj_flt, &cmaj_flt, &utime, &systime,
       &cutime, &csystime, &priority, &p_nice, &timeout, &it_real_value,
       &start_time, &vsize,
       &rss,
       &rss_rlim, &start_code, &end_code, &start_stack, &kstk_esp, &kstk_eip,
       &wchan, &nswap, &cnswap, &exit_signal, &lproc);
  if(res != 35) {
    fclose(globalStatFile);
    fclose(file);
    LOG( MLUCLog::kError, "MLUCValueMonitorProcess_CpuUsage_System:GetValue","Error reading file" )
#if __GNUC__ >=3
      << "Could not read data from" << fFilename.str().c_str() << ENDLOG; 
#else
      << "Could not read data from" << fFilename.str() << ENDLOG; 
#endif
    return EINVAL;
  }

  // read global cpu time from /proc/stat
  res = fscanf(globalStatFile, "cpu %lu %lu %lu %lu ",
	       &user_val, &nice_val, &sys_val, &idle_val);
  if(res != 4) { 
    fclose(globalStatFile);
    fclose(file);
    LOG( MLUCLog::kError, "MLUCValueMonitorProcess_CpuUsage_System:GetValue",
	 "Error reading file" ) << "Could not read data from /proc/stat"
                                << ENDLOG;
    return EINVAL;
  }

  fclose(globalStatFile);
  fclose(file);

  
  TotalCpuTime = user_val + nice_val + sys_val + idle_val;
  ProcessTime = systime;
  
  if(fFirstReadOut) {
    fOldTotalCpuTime = TotalCpuTime;
    fOldProcessTime  = ProcessTime;
    fFirstReadOut    = false;
    return ENOTSUP; // first readout (op not supported)
  } 
  
  MeasuredValue = (uint64) ((ProcessTime - fOldProcessTime) * 100.0 / 
			    (float)(TotalCpuTime - fOldTotalCpuTime) + .5);
  //  MeasuredValue = (ProcessTime - fOldProcessTime) * 100 / 
  //                (TotalCpuTime - fOldTotalCpuTime);

  fOldTotalCpuTime = TotalCpuTime;
  fOldProcessTime  = ProcessTime;
  return 0;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

void MLUCValueMonitorProcess_Size::Init(int pid)
{
  fpid = pid;
  fFilename << "/proc/" << pid << "/statm";
  fPageSize = getpagesize() / 1024; // size of a memory page in KBytes
}

// -----------------------------------------------------------------------------

int MLUCValueMonitorProcess_Size::GetValue( uint64& MeasuredValue )
{
  FILE *file;
  int res;
  unsigned int size, resident, share, trs, drs, lrs, dt;
  const int NRES = 7; // numbers of values to be read

#if __GNUC__ >=3
  file = fopen(fFilename.str().c_str(), "r");
#else
  file = fopen(fFilename.str(), "r");
#endif
  if(!file) {
    LOG( MLUCLog::kError, "MLUCValueMonitorProcess_Size:GetValue","Error opening file" )
#if __GNUC__ >=3
      << "Could not open "<< fFilename.str().c_str() << ENDLOG;
#else
      << "Could not open "<< fFilename.str() << ENDLOG;
#endif
    return ENOENT;
  }

  res = fscanf(file, "%u %u %u %u %u %u %u", 
	      &size, &resident, &share, &trs, &drs, &lrs, &dt);

  fclose(file);

  // test if reading was successful
  if(res != NRES) {
    LOG(MLUCLog::kError, "MLUCValueMonitorProcess_Mem_Shared:GetValue","Error reading file" ) 
#if __GNUC__ >=3
      << "Error reading data from " << fFilename.str().c_str() 
#else
      << "Error reading data from " << fFilename.str() 
#endif
      << ". Number of datasets should be " << NRES 
      << ", but reading " << res << "." << ENDLOG;
    return EINVAL;
  }

  MeasuredValue = size * fPageSize;

  return 0;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

void MLUCValueMonitorProcess_Mem_Resident::Init(int pid)
{
  fpid = pid;
  fFilename << "/proc/" << pid << "/statm";
  fPageSize = getpagesize() / 1024; // size of a memory page in KBytes
}

// -----------------------------------------------------------------------------

int MLUCValueMonitorProcess_Mem_Resident::GetValue( uint64& MeasuredValue )
{
  FILE *file;
  int res;
  unsigned int size, resident, share, trs, drs, lrs, dt;
  const int NRES = 7; // numbers of values to be read

#if __GNUC__ >=3
  file = fopen(fFilename.str().c_str(), "r");
#else
  file = fopen(fFilename.str(), "r");
#endif

  if(!file) {
    LOG( MLUCLog::kError, "MLUCValueMonitorProcess_Mem_Resident:GetValue","Error opening file" ) 
#if __GNUC__ >=3
      << "Could not open "<< fFilename.str().c_str() << ENDLOG;
#else
    << "Could not open "<< fFilename.str() << ENDLOG;
#endif

    return ENOENT; 
  }

  res = fscanf(file, "%u %u %u %u %u %u %u", 
	      &size, &resident, &share, &trs, &drs, &lrs, &dt);

  fclose(file);

  // test if reading was successful
  if(res != NRES) {
    LOG(MLUCLog::kError, "MLUCValueMonitorProcess_Mem_Shared:GetValue","Error reading file" )
#if __GNUC__ >=3
      << "Error reading data from " << fFilename.str().c_str() 
#else
      << "Error reading data from " << fFilename.str() 
#endif
      << ". Number of datasets should be " << NRES 

      << ", but reading " << res << "." << ENDLOG;
    return EINVAL;
  }

  MeasuredValue = resident * fPageSize;

  return 0;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

void MLUCValueMonitorProcess_Mem_Shared::Init(int pid)
{
  fpid = pid;
  fFilename << "/proc/" << pid << "/statm";
  fPageSize = getpagesize() / 1024; // size of a memory page in KBytes
}

// -----------------------------------------------------------------------------

int MLUCValueMonitorProcess_Mem_Shared::GetValue( uint64& MeasuredValue )
{
  FILE *file;
  int res;
  unsigned int size, resident, share, trs, drs, lrs, dt;
  const int NRES = 7; // numbers of values to be read

#if __GNUC__ >=3
  file = fopen(fFilename.str().c_str(), "r");
#else
  file = fopen(fFilename.str(), "r");
#endif

  if(!file) {
    LOG( MLUCLog::kError, "MLUCValueMonitorProcess_Mem_Shared:GetValue","Error opening file" )
#if __GNUC__ >=3
      << "Could not open "<< fFilename.str().c_str() << ENDLOG;
#else
      << "Could not open "<< fFilename.str() << ENDLOG;
#endif
    return ENOENT; 
  }

  res = fscanf(file, "%u %u %u %u %u %u %u", 
	      &size, &resident, &share, &trs, &drs, &lrs, &dt);

  fclose(file);

  // test if reading was successful
  if(res != NRES) {
    LOG(MLUCLog::kError, "MLUCValueMonitorProcess_Mem_Shared:GetValue","Error reading file" )
#if __GNUC__ >=3
      << "Error reading data from " << fFilename.str().c_str() 
#else
      << "Error reading data from " << fFilename.str() 
#endif
      << ". Number of datasets should be " << NRES 
      << ", but reading " << res << "." << ENDLOG;
    return EINVAL;
  }

  MeasuredValue =share * fPageSize;

  return 0;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

void MLUCValueMonitorProcess_Mem_Stack::Init(int pid)
{
  fpid = pid;
  fFilename << "/proc/" << pid << "/statm";
  fPageSize = getpagesize() / 1024; // size of a memory page in KBytes
}

// -----------------------------------------------------------------------------

int MLUCValueMonitorProcess_Mem_Stack::GetValue( uint64& MeasuredValue )
{
  FILE *file;
  int res;
  unsigned int size, resident, share, trs, drs, lrs, dt;
  const int NRES = 7; // numbers of values to be read

#if __GNUC__ >=3
  file = fopen(fFilename.str().c_str(), "r");
#else
  file = fopen(fFilename.str(), "r");
#endif

  if(!file) {
    LOG( MLUCLog::kError, "MLUCValueMonitorProcess_Mem_Stack:GetValue","Error opening file" )
#if __GNUC__ >=3
      << "Could not open "<< fFilename.str().c_str() << ENDLOG;
#else
      << "Could not open "<< fFilename.str() << ENDLOG;
#endif
    return ENOENT; 
  }

  res = fscanf(file, "%u %u %u %u %u %u %u", 
	      &size, &resident, &share, &trs, &drs, &lrs, &dt);

  fclose(file);

  // test if reading was successful
  if(res != NRES) {
    LOG(MLUCLog::kError, "MLUCValueMonitorProcess_Mem_Shared:GetValue","Error reading file" )
#if __GNUC__ >=3
      << "Error reading data from " << fFilename.str().c_str() 
#else
      << "Error reading data from " << fFilename.str() 
#endif
      << ". Number of datasets should be " << NRES 
      << ", but reading " << res << "." << ENDLOG;
    return EINVAL;
  }

  MeasuredValue = drs * fPageSize;

  return 0;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

void MLUCValueMonitorProcess_Mem_Library::Init(int pid)
{
  fpid = pid;
  fFilename << "/proc/" << pid << "/statm";
  fPageSize = getpagesize() / 1024; // size of a memory page in KBytes
}

// -----------------------------------------------------------------------------

int MLUCValueMonitorProcess_Mem_Library::GetValue( uint64& MeasuredValue )
{
  FILE *file;
  int res;
  unsigned int size, resident, share, trs, drs, lrs, dt;
  const int NRES = 7; // numbers of values to be read

#if __GNUC__ >=3
  file = fopen(fFilename.str().c_str(), "r");
#else
  file = fopen(fFilename.str(), "r");
#endif
  if(!file) {
    LOG( MLUCLog::kError, "MLUCValueMonitorProcess_Mem_Library:GetValue","Error opening file" )
#if __GNUC__ >=3
      << "Could not open "<< fFilename.str().c_str() << ENDLOG;
#else
<< "Could not open "<< fFilename.str() << ENDLOG;
#endif

    return ENOENT; 
  }

  res = fscanf(file, "%u %u %u %u %u %u %u", 
	      &size, &resident, &share, &trs, &drs, &lrs, &dt);

  fclose(file);

  // test if reading was successful
  if(res != NRES) {
    LOG(MLUCLog::kError, "MLUCValueMonitorProcess_Mem_Shared:GetValue","Error reading file" )
#if __GNUC__ >=3
      << "Error reading data from " << fFilename.str().c_str() 
#else
      << "Error reading data from " << fFilename.str() 
#endif
      << ". Number of datasets should be " << NRES 
      << ", but reading " << res << "." << ENDLOG;
    return EINVAL;
  }

  MeasuredValue = lrs * fPageSize;

  return 0;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

void MLUCValueMonitorProcess_Mem_Dirty::Init(int pid)
{
  fpid = pid;
  fFilename << "/proc/" << pid << "/statm";
}

// -----------------------------------------------------------------------------

int MLUCValueMonitorProcess_Mem_Dirty::GetValue( uint64& MeasuredValue )
{
  FILE *file;
  int res;
  unsigned int size, resident, share, trs, drs, lrs, dt;
  const int NRES = 7; // numbers of values to be read

#if __GNUC__ >=3
  file = fopen(fFilename.str().c_str(), "r");
#else
  file = fopen(fFilename.str(), "r");
#endif
  if(!file) {
    LOG( MLUCLog::kError, "MLUCValueMonitorProcess_Mem_Dirty:GetValue","Error opening file" ) 
#if __GNUC__ >=3
      << "Could not open "<< fFilename.str().c_str() << ENDLOG;
#else
      << "Could not open "<< fFilename.str() << ENDLOG;
#endif
    return ENOENT; 
  }

  res = fscanf(file, "%u %u %u %u %u %u %u", 
	      &size, &resident, &share, &trs, &drs, &lrs, &dt);

  fclose(file);

  // test if reading was successful
  if(res != NRES) {
    LOG(MLUCLog::kError, "MLUCValueMonitorProcess_Mem_Shared:GetValue","Error reading file" )
#if __GNUC__ >=3
      << "Error reading data from " << fFilename.str().c_str() 
#else
      << "Error reading data from " << fFilename.str() 
#endif
      << ". Number of datasets should be " << NRES 
      << ", but reading " << res << "." << ENDLOG;
    return EINVAL;
  }

  MeasuredValue = dt;

  return 0;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------


