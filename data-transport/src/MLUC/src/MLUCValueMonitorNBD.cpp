/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Arne Wiebalck 
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCValueMonitorNBD.hpp"
#include <fstream>

MLUCValueMonitorNBD::MLUCValueMonitorNBD( const char* filename, 
					    const char* fileDescrHeader, 
					    const char* output_prefix, 
					    const char* output_suffix, 
					    const char* avg_output_prefix, 
					    const char* avg_output_suffix, 
					    unsigned int average_items, 
					    int what ) :
	
    MLUCValueMonitor( filename, 
		      fileDescrHeader, 
		      output_prefix, 
		      output_suffix, 
		      avg_output_prefix, 
		      avg_output_suffix, 
		      average_items ),
    fMonitorQuantity( what )
    {
    fFirstReadOut   = true;
    fBytesNow = 0;	
      
    }

MLUCValueMonitorNBD::MLUCValueMonitorNBD ( const char* output_prefix, 
					     const char* output_suffix, 
					     const char* avg_output_prefix, 
					     const char* avg_output_suffix, 
					     unsigned int average_items,
					     int what ) :
	
    MLUCValueMonitor( output_prefix, 
		      output_suffix, 
		      avg_output_prefix, 
		      avg_output_suffix, 
		      average_items ),
    fMonitorQuantity( what )
    {
    fFirstReadOut = true;
    fBytesNow = 0;
    }

MLUCValueMonitorNBD::MLUCValueMonitorNBD ( const char* filename, 
					     const char* fileDescrHeader, 
					     unsigned int average_items,
					     int what ) : 

    MLUCValueMonitor( filename, 
		      fileDescrHeader, 
		      average_items ),
    fMonitorQuantity( what )
    {
    fFirstReadOut = true;
    fBytesNow = 0;
    }


MLUCValueMonitorNBD::MLUCValueMonitorNBD ( unsigned int average_items,
					     int what ) : 

    MLUCValueMonitor( average_items ),
    fMonitorQuantity( what )
    {
    fFirstReadOut = true;
    fBytesNow = 0;
    }


int MLUCValueMonitorNBD::GetValue( uint64& measuredValue ) 
    {
	
    char   kLine[5000];   
	
    std::ifstream in( "/proc/nbdinfo" );
    if( !in ) 
	{
	LOG( MLUCLog::kError, "MLUCValueMonitorNBD:GetValue", "Error opening file" )
	    << "Could not open /proc/nbdinfo" << ENDLOG;
	return ENOENT;
	}
    else
	{
	float Bytes = 0;
	char  Unit  = 0;
	bool  found = false;

	while( !in.eof() && !found ) 
	    {
	    in.getline( kLine, 5000 );
	    
	    if( sscanf( kLine, "[a] B/s now: %f%c ", &Bytes, &Unit ) > 0 ) 
		{
		found = true;
		break;
		}
	    }



	switch( Unit ) 
	    {
	    case 'K':
		fBytesNow = (unsigned long) (Bytes * 1024);
		break;
	    case 'M': 
		fBytesNow = (unsigned long) (Bytes * 1024 * 1024);
		break;
	    case 'G':
		fBytesNow = (unsigned long) (Bytes * 1024 * 1024 * 1024);
		break;
	    case 'T':
		fBytesNow = (unsigned long) (Bytes * 1024 * 1024 * 1024 *1024);
		break;
	    case 'P': 
		fBytesNow = (unsigned long) (Bytes * 1024 * 1024 * 1024 *1024 * 1024);
		break;
	    default:
		fBytesNow = 0;
		break;
	    }

	measuredValue = fBytesNow;
	return 0;
	}
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Arne Wiebalck 
**
** $Id$ 
**
***************************************************************************
*/
