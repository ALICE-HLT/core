#include <string>
#if __GNUC__ ==2
#include <strstream>
#endif
#include <sstream>
#include <fstream>
#include <stdio.h>

#include "MLUCValueMonitorMem.hpp"

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

int GetMemVal(unsigned MemType, unsigned &val) 
{
  
  string pattern, pattern2;
  bool bSecondRun = false;
  std::ifstream fin( "/proc/meminfo" );
  if (!fin) {
    return ENOENT;
  }
 
  switch (MemType) {
  case MEMTOTAL:
    pattern = "MemTotal:";
    break;
  case MEMFREE:
    pattern = "MemFree:";
    break;
  case MEMUSED:
    pattern = "MemTotal:";
    pattern2 = "MemFree:";
    bSecondRun = true;
    break;
  case MEMSHARED:
    pattern = "MemShared:";
    break;
  case BUFFERS:
    pattern = "Buffers:";
    break;
  case CACHED:
    pattern = "Cached:";
    break;
  case SWAPTOTAL:
    pattern = "SwapTotal:";
    break;
  case SWAPFREE:
    pattern = "SwapFree:";
    break;
  case SWAPUSED:
    pattern = "SwapTotal:";
    pattern2 = "SwapFree:";
    bSecondRun = true;
    break;
  }
  
  string line;
  while (getline(fin, line)) {
#if __GNUC__>=3
    istringstream s(line.c_str());
#else
    istrstream s(line.c_str(), line.size());
#endif
    string token;
    s >> token;
    if (token == pattern) {
      if(!(s >> val)) {
	//      if (sscanf(token.c_str(),"%u", &val) <=0 ) {
	return EINVAL;
      }
      break;
    }
  }

  if(bSecondRun) {
    while (getline(fin, line)) {
#if __GNUC__>=3
      istringstream s(line.c_str());
#else
      istrstream s(line.c_str(), line.size());
#endif
      string token;
      s >> token;
      if (token == pattern2) {
	unsigned tmp;
	if(!(s >> tmp)) {
	//	if (sscanf(token.c_str(),"%u", &tmp) <=0 ) {
	  return EINVAL;
	}
	val -= tmp; // difference between SwapTotal and SwapFree
	break;
      }
    }
  }
  
  return 0;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

void MLUCValueMonitorMem_Total::Init()
{
  fMemType = MEMTOTAL;
}

// -----------------------------------------------------------------------------

int MLUCValueMonitorMem_Total::GetValue( uint64& MeasuredValue )
{
  int res;
  unsigned val;

  res = GetMemVal(fMemType, val);
  if(res == ENOENT) {
    LOG( MLUCLog::kError, "MLUCValueMonitorMem_Total:GetValue",
	 "Error opening file" ) << "Could not open /proc/meminfo" << ENDLOG;
  }
  if(res != 0)
    return res;

  MeasuredValue = val; 

  return 0;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

void MLUCValueMonitorMem_Free::Init()
{
  fMemType = MEMFREE;
}

// -----------------------------------------------------------------------------

int MLUCValueMonitorMem_Free::GetValue( uint64& MeasuredValue )
{
  int res;
  unsigned val;

  res = GetMemVal(fMemType, val);
  if(res == ENOENT) {
    LOG( MLUCLog::kError, "MLUCValueMonitorMem_Free:GetValue", 
	 "Error opening file" ) << "Could not open /proc/meminfo" << ENDLOG;
  }
  if(res != 0)
    return res;

  MeasuredValue = val; 

  return 0;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

void MLUCValueMonitorMem_Used::Init()
{
  fMemType = MEMUSED;
}
// -----------------------------------------------------------------------------
int MLUCValueMonitorMem_Used::GetValue( uint64& MeasuredValue )
{
  int res;
  unsigned val;

  res = GetMemVal(fMemType, val);
  if(res == ENOENT) {
    LOG( MLUCLog::kError, "MLUCValueMonitorMem_Free:GetValue", 
	 "Error opening file" ) << "Could not open /proc/meminfo" << ENDLOG;
  }
  if(res != 0)
    return res;

  MeasuredValue = val; 

  return 0;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

void MLUCValueMonitorMem_Shared::Init()
{
  fMemType = MEMSHARED;
}

// -----------------------------------------------------------------------------

int MLUCValueMonitorMem_Shared::GetValue( uint64& MeasuredValue )
{
  int res;
  unsigned val;

  res = GetMemVal(fMemType, val);
  if(res == ENOENT) {
    LOG( MLUCLog::kError, "MLUCValueMonitorMem_Shared:GetValue", 
	 "Error opening file" ) << "Could not open /proc/meminfo" << ENDLOG;
  }
  if(res != 0)
    return res;

  MeasuredValue = val; 

  return 0;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

void MLUCValueMonitorMem_Buffers::Init()
{
  fMemType = BUFFERS;
}

// -----------------------------------------------------------------------------

int MLUCValueMonitorMem_Buffers::GetValue( uint64& MeasuredValue )
{
  int res;
  unsigned val;

  res = GetMemVal(fMemType, val);
  if(res == ENOENT) {
    LOG( MLUCLog::kError, "MLUCValueMonitorMem_Buffers:GetValue", 
	 "Error opening file" ) << "Could not open /proc/meminfo" << ENDLOG;
  }
  if(res != 0)
    return res;

  MeasuredValue = val; 

  return 0;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

void MLUCValueMonitorMem_Cached::Init()
{
  fMemType = CACHED;
}

// -----------------------------------------------------------------------------

int MLUCValueMonitorMem_Cached::GetValue( uint64& MeasuredValue )
{
  int res;
  unsigned val;

  res = GetMemVal(fMemType, val);
  if(res == ENOENT) {
    LOG( MLUCLog::kError, "MLUCValueMonitorMem_Cached:GetValue",
	 "Error opening file" ) << "Could not open /proc/meminfo" << ENDLOG;
  }
  if(res != 0)
    return res;

  MeasuredValue = val; 

  return 0;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

void MLUCValueMonitorMem_SwapTotal::Init()
{
  fMemType = SWAPTOTAL;
}

// -----------------------------------------------------------------------------

int MLUCValueMonitorMem_SwapTotal::GetValue( uint64& MeasuredValue )
{
  int res;
  unsigned val;

  res = GetMemVal(fMemType, val);
  if(res == ENOENT) {
    LOG( MLUCLog::kError, "MLUCValueMonitorMem_SwapTotal:GetValue", 
	 "Error opening file" ) << "Could not open /proc/meminfo" << ENDLOG;
  }
  if(res != 0)
    return res;

  MeasuredValue = val; 

  return 0;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

void MLUCValueMonitorMem_SwapFree::Init()
{
  fMemType = SWAPFREE;
}

// -----------------------------------------------------------------------------

int MLUCValueMonitorMem_SwapFree::GetValue( uint64& MeasuredValue )
{
  int res;
  unsigned val;

  res = GetMemVal(fMemType, val);
  if(res == ENOENT) {
    LOG( MLUCLog::kError, "MLUCValueMonitorMem_SwapFree:GetValue", 
	 "Error opening file" ) << "Could not open /proc/meminfo" << ENDLOG;
  }
  if(res != 0)
    return res;

  MeasuredValue = val; 

  return 0;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

void MLUCValueMonitorMem_SwapUsed::Init()
{
  fMemType = SWAPUSED;
}

// -----------------------------------------------------------------------------

int MLUCValueMonitorMem_SwapUsed::GetValue( uint64& MeasuredValue )
{
  int res;
  unsigned val;

  res = GetMemVal(fMemType, val);
  if(res == ENOENT) {
    LOG( MLUCLog::kError, "MLUCValueMonitorMem_SwapUsed:GetValue", 
	 "Error opening file" ) << "Could not open /proc/meminfo" << ENDLOG;
  }
  if(res != 0)
    return res;

  MeasuredValue = val; 

  return 0;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
