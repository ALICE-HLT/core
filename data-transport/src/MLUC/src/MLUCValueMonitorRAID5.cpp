/*
***************************************************************************
**
** $Author$ - Initial Version by Arne Wiebalck 
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCValueMonitorRAID5.hpp"
#include <stdio.h>

MLUCValueMonitorRAID5 :: MLUCValueMonitorRAID5( const char* filename, 
												const char* fileDescrHeader, 
												const char* output_prefix, 
												const char* output_suffix, 
												const char* avg_output_prefix, 
												const char* avg_output_suffix, 
												unsigned int average_items, 
												unsigned int RAID_ID ) :

	MLUCValueMonitor( filename, 
					  fileDescrHeader, 
					  output_prefix, 
					  output_suffix, 
					  avg_output_prefix, 
					  avg_output_suffix, 
					  average_items )
{
	fID = RAID_ID;
}

MLUCValueMonitorRAID5 :: MLUCValueMonitorRAID5 ( const char* output_prefix, 
												 const char* output_suffix, 
												 const char* avg_output_prefix, 
												 const char* avg_output_suffix, 
												 unsigned int average_items,
												 unsigned int RAID_ID ) :
	
	MLUCValueMonitor( output_prefix, 
					  output_suffix, 
					  avg_output_prefix, 
					  avg_output_suffix, 
					  average_items )
{
	fID = RAID_ID;

}

MLUCValueMonitorRAID5 :: MLUCValueMonitorRAID5 ( const char* filename, 
												 const char* fileDescrHeader, 
												 unsigned int average_items,
												 unsigned int RAID_ID ) : 
  
	MLUCValueMonitor( filename, 
					  fileDescrHeader, 
					  average_items )
{
	fID = RAID_ID;
}

MLUCValueMonitorRAID5 :: MLUCValueMonitorRAID5 ( unsigned int average_items,
												 unsigned int ) : 

	MLUCValueMonitor( average_items )
{
}

int MLUCValueMonitorRAID5 :: GetNumberOfDevices( int ID, uint64& status ) {

	int  whichRAID = -1;
	bool found     = false;
	char kLine[500];   
	int  total = 0, active = 0; 

	std::ifstream in( "/proc/mdstat" );
	if( !in ) 
		{
			LOG( MLUCLog::kError, "MLUCValueMonitorRAID5:GetValue", "Error opening file" )
				<< "Could not open /proc/mdstat" << ENDLOG;
			return ENOENT;
		}
	else
		{
			while( !in.eof() && !found ) {
				in.getline( kLine, 500 );
	
				if( sscanf( kLine, "md%u ", &whichRAID ) > 0 ) {
					if( whichRAID == ID ) {
						in.getline( kLine, 500 );
	    
						int len = strlen( kLine );
						for( int i=0; i<len; i++ ) {
							if( *(kLine+i) == '[' ) {
								sscanf( kLine+i, "[%d/%d] ", &total, &active );
								//fprintf( stderr, "total %d, active %d\n", total, active );
								if( total == active ) {
									for( int j=0; j<total; j++ ) {
										status |= DEVICE_OK << 2*j;
									}
									return total;
								}
							}
						}
					}
				}
			}
		}
 
	return total;
}


int MLUCValueMonitorRAID5 :: GetValue( uint64& MeasuredValue ) {

	char kLine[500];   

	MeasuredValue = 0;
	int total = GetNumberOfDevices( fID, MeasuredValue );
	int active_counter = 0;

	if( MeasuredValue == 0 ) {
		// if we are here, we know something is wrong
		// either we have a faulty device noone knows about or
		// we are in resync
		std::ifstream in( "/proc/mdstat" );
		if( !in ) 
			{
				LOG( MLUCLog::kError, "MLUCValueMonitorRAID5:GetValue", "Error opening file" )
					<< "Could not open /proc/mdstat" << ENDLOG;
				return ENOENT;
			}
		else
			{
				int counter = 0;
				bool faulty  = false;
				int theOne = -1;
				int whichRAID = -1;

				while( !in.eof() ) {
					in.getline( kLine, 500 );
	
					if( sscanf( kLine, "md%u ", &whichRAID ) > 0 ) {

						if( whichRAID == fID ) {
	    
							int len = strlen( kLine );

							for( int i=0; i<len; i++ ) {
								if( *(kLine+i) == '(' ) {
									faulty = true; // not yet removed
								}
								if( *(kLine+i) == '[' ) {
									active_counter++;
								}
							}

							if( active_counter != total ) { // removed and added again
								faulty = true;			
							}

							// which one is it?
							in.getline( kLine, 500 );
							while( *(kLine+counter) != '[' ) counter++;
							counter++;
							while( *(kLine+counter) != '[' ) counter++; 
							counter++;
							for( int j=0; j<total; j++ ) {
								if( *(kLine+counter+j) != '_' ) {
									continue;
								}
								theOne = j;
							}
	    	   
							// assemble the return value
							for( int k=0; k<total; k++ ) {
								if( k == theOne && faulty )
									MeasuredValue |= FAULTY << 2*k;
								else if( k == theOne && !faulty )
									MeasuredValue |= RESYNC << 2*k;
								else
									MeasuredValue |= DEVICE_OK << 2*k;		
							}
						}
					}
				}
			}
    
	}

	return 0;
}


