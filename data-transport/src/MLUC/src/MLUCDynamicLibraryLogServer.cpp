#include "MLUCLog.hpp"
#include "MLUCDynamicLibraryLogServer.hpp"
#include <dlfcn.h>

MLUCDynamicLibraryLogServer::MLUCDynamicLibraryLogServer(const char* pLibName, const char* pFacilityName)
    {
    // TODO: remove when using TaskManager...
    //     MLUCStdoutLogServer stdLog;
    //     gLog.AddServer(stdLog);
    
    fLibName = pLibName;
    fFacilityName = pFacilityName;
    
    // TODO: try to use MLUCDynamicLibrary class instead
    fLogServer = NULL;

    fHandle = dlopen(fLibName.c_str(), RTLD_LAZY);
    
    if ( fHandle )
	{
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
	CreateLogServerFunc createLogServer = (CreateLogServerFunc) dlsym(fHandle, "CreateLogServer_v2");
#else
	CreateLogServerFunc createLogServer = (CreateLogServerFunc) dlsym(fHandle, "CreateLogServer");
#endif
	if ( createLogServer )
	    fLogServer = createLogServer(fFacilityName.c_str());
	else
	    {
	    LOG( MLUCLog::kError, "MLUCDynamicLibraryLogServer::MLUCDynamicLibraryLogServer", "Cannot load log server creation function" )
		<< "Unable to load log server creation function ('CreateLogServer') from shared library '" << fLibName.c_str() << "': " << dlerror() << "." << ENDLOG;
	    }
	}
    else
	{
	LOG( MLUCLog::kError, "MLUCDynamicLibraryLogServer::MLUCDynamicLibraryLogServer", "Cannot open shared library" )
	    << "Unable to open shared library '" << fLibName.c_str() << "': " << dlerror() << "." << ENDLOG;
	}

    }

MLUCDynamicLibraryLogServer::~MLUCDynamicLibraryLogServer()
    {
    if ( fHandle )
	dlclose(fHandle);
    }

void MLUCDynamicLibraryLogServer::LogLine( const char* origin, const char* keyword, const char* file,
                                           int linenr, const char* compile_date, const char* compile_time,
                                           MLUCLog::TLogLevel logLvl, const char* hostname, const char* id,
                                           
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
					   unsigned long runNumber, 
#endif
					   uint32 ldate, uint32 ltime_s, uint32 ltime_us, 
					   uint32 msgNr, const char* line )
    {
    if ( fLogServer )
	fLogServer->LogLine(origin, keyword, file, linenr, compile_date, compile_time, logLvl,
			    hostname, id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
			    runNumber, 
#endif
			    ldate, ltime_s, ltime_us, msgNr, line);
    
    }

void MLUCDynamicLibraryLogServer::LogLine( const char* origin, const char* keyword, const char* file,
                                           int linenr, const char* compile_date, const char* compile_time,
                                           MLUCLog::TLogLevel logLvl, const char* hostname, const char* id,
                                           
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
					   unsigned long runNumber, 
#endif
					   uint32 ldate, uint32 ltime_s, uint32 ltime_us, 
					   uint32 msgNr, uint32 subMsgNr, const char* line )
    {
    if ( fLogServer )
	fLogServer->LogLine(origin, keyword, file, linenr, compile_date, compile_time, logLvl,
			    hostname, id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
			    runNumber, 
#endif
			    ldate, ltime_s, ltime_us, msgNr, subMsgNr, line);
    }
