/*
***************************************************************************
**
** $Author$ - Initial Version by Arne Wiebalck & Timm Morten Steinbeck 
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCValueMonitorCPU.hpp"
#include <stdio.h>

MLUCValueMonitorCPU :: MLUCValueMonitorCPU( const char* filename, 
					    const char* fileDescrHeader, 
					    const char* output_prefix, 
					    const char* output_suffix, 
					    const char* avg_output_prefix, 
					    const char* avg_output_suffix, 
					    unsigned int average_items, 
					    short cpu_number ) :

	MLUCValueMonitor( filename, 
			  fileDescrHeader, 
			  output_prefix, 
			  output_suffix, 
			  avg_output_prefix, 
			  avg_output_suffix, 
			  average_items )
{
	kCPU_Number     = cpu_number; 
	kFirstReadOut   = true;
	kNumber_Of_CPUs   = 0;
	GetNumberOfCPUs();
}

MLUCValueMonitorCPU :: MLUCValueMonitorCPU ( const char* output_prefix, 
					     const char* output_suffix, 
					     const char* avg_output_prefix, 
					     const char* avg_output_suffix, 
					     unsigned int average_items,
					     short cpu_number ) :
	
	MLUCValueMonitor( output_prefix, 
			  output_suffix, 
			  avg_output_prefix, 
			  avg_output_suffix, 
			  average_items )
{
	kCPU_Number = cpu_number;
	kFirstReadOut = true;
}

MLUCValueMonitorCPU :: MLUCValueMonitorCPU ( const char* filename, 
					     const char* fileDescrHeader, 
					     unsigned int average_items,
					     short cpu_number ) : 

	MLUCValueMonitor( filename, 
			  fileDescrHeader, 
			  average_items )
{
	kCPU_Number = cpu_number;
	kFirstReadOut = true;
}

MLUCValueMonitorCPU :: MLUCValueMonitorCPU ( unsigned int average_items,
					     short cpu_number ) : 

	MLUCValueMonitor( average_items )
{
	kCPU_Number = cpu_number;
	kFirstReadOut = true;
}

int MLUCValueMonitorCPU :: CalculateValue( enum kDataType type, uint64& Value ) {
	
	unsigned long kOldSum, kNewSum, diff;
	
	kOldSum =   kUserOldValue +  kNiceOldValue +  kSysOldValue +  kIdleOldValue;
	kNewSum =   kUserNewValue +  kNiceNewValue +  kSysNewValue +  kIdleNewValue;
	
	diff = kNewSum - kOldSum;

	if( diff ) {
		switch( type ) {
		
		case USER:
			Value = ( ( kUserNewValue - kUserOldValue ) * 100 ) / ( kNewSum - kOldSum );
			break;
		case NICE:
			Value = ( ( kNiceNewValue - kNiceOldValue ) * 100 ) / ( kNewSum - kOldSum );
			break;
		case SYS:
			Value = ( ( kSysNewValue - kSysOldValue ) * 100 ) / ( kNewSum - kOldSum );
			break;
		case IDLE:
			Value = ( ( kIdleNewValue - kIdleOldValue ) * 100 ) / ( kNewSum - kOldSum );
			break;
		case USED:
			Value = ( ( (kUserNewValue+kSysNewValue+kNiceNewValue) - (kUserOldValue+kSysOldValue+kNiceOldValue) ) * 100 ) / ( kNewSum - kOldSum );
			break;
		default:
			return EINVAL;
		}

	} else {
		
		Value = 0;
	}

	return 0;
}


int MLUCValueMonitorCPU :: GetNumberOfCPUs() {
	
	char kLine[500]; // be on the safe side ;-)
	int  dummy;

	std :: ifstream in( "/proc/cpuinfo" );
	if( !in ) {
		LOG( MLUCLog::kError, "MLUCValueMonitorCPU:GetNumberOfCPUs", "Error opening file" )
			<< "Could not open /proc/cpuinfo" << ENDLOG;
		return ENOENT;
	} else {
		while( !in.eof() ) {
			in.getline( kLine, 500 );
			if( sscanf( kLine, "processor : %d ", &dummy) > 0 ) {
				kNumber_Of_CPUs++;
			}
		}
	}
	if( kCPU_Number > kNumber_Of_CPUs - 1  ) {
		LOG( MLUCLog::kWarning, "MLUCValueMonitorCPU:GetNumberOfCPUs", "Wrong CPU ID" )
			<< "Cannot monitor CPU " <<  kCPU_Number << ", since the system has only " 
			<< kNumber_Of_CPUs << " CPU(s). Will read out overall cpu usage." << ENDLOG;	

// 		kCPU_Number = -1;		
	}



	return 0;
}

MLUCValueMonitorCPU_User :: MLUCValueMonitorCPU_User( const char* filename, 
						      const char* fileDescrHeader, 
						      const char* output_prefix, 
						      const char* output_suffix, 
						      const char* avg_output_prefix, 
						      const char* avg_output_suffix, 
						      unsigned int average_items, 
						      short cpu_number ) :

	MLUCValueMonitorCPU( filename, 
			     fileDescrHeader, 
			     output_prefix, 
			     output_suffix, 
			     avg_output_prefix, 
			     avg_output_suffix, 
			     average_items,
			     cpu_number )
{
}

MLUCValueMonitorCPU_User :: MLUCValueMonitorCPU_User ( const char* output_prefix, 
						       const char* output_suffix, 
						       const char* avg_output_prefix, 
						       const char* avg_output_suffix, 
						       unsigned int average_items,
						       short cpu_number ) :
	
	MLUCValueMonitorCPU( output_prefix, 
			     output_suffix, 
			     avg_output_prefix, 
			     avg_output_suffix, 
			     average_items,
			     cpu_number )
{
}

MLUCValueMonitorCPU_User :: MLUCValueMonitorCPU_User ( const char* filename, 
						       const char* fileDescrHeader, 
						       unsigned int average_items,
						       short cpu_number ) :

	MLUCValueMonitorCPU( filename, 
			     fileDescrHeader, 
			     average_items,
			     cpu_number )
{
}


MLUCValueMonitorCPU_User :: MLUCValueMonitorCPU_User ( unsigned int average_items,
						       short cpu_number ) :

	MLUCValueMonitorCPU( average_items,
			     cpu_number )
{
}


MLUCValueMonitorCPU_Nice :: MLUCValueMonitorCPU_Nice( const char* filename, 
						      const char* fileDescrHeader, 
						      const char* output_prefix, 
						      const char* output_suffix, 
						      const char* avg_output_prefix, 
						      const char* avg_output_suffix, 
						      unsigned int average_items, 
						      short cpu_number ) :

	MLUCValueMonitorCPU( filename, 
			     fileDescrHeader, 
			     output_prefix, 
			     output_suffix, 
			     avg_output_prefix, 
			     avg_output_suffix, 
			     average_items,
			     cpu_number )
{	
}

MLUCValueMonitorCPU_Nice :: MLUCValueMonitorCPU_Nice ( const char* output_prefix, 
						       const char* output_suffix, 
						       const char* avg_output_prefix, 
						       const char* avg_output_suffix, 
						       unsigned int average_items,
						       short cpu_number ) :
	
	MLUCValueMonitorCPU( output_prefix, 
			     output_suffix, 
			     avg_output_prefix, 
			     avg_output_suffix, 
			     average_items,
			     cpu_number )
{
}

MLUCValueMonitorCPU_Nice :: MLUCValueMonitorCPU_Nice ( const char* filename, 
						       const char* fileDescrHeader, 
						       unsigned int average_items,
						       short cpu_number ) :

	MLUCValueMonitorCPU( filename, 
			     fileDescrHeader, 
			     average_items,
			     cpu_number )
{
}


MLUCValueMonitorCPU_Nice :: MLUCValueMonitorCPU_Nice ( unsigned int average_items,
						       short cpu_number ) :

	MLUCValueMonitorCPU( average_items,
			     cpu_number )
{
}


MLUCValueMonitorCPU_Sys :: MLUCValueMonitorCPU_Sys( const char* filename, 
						    const char* fileDescrHeader, 
						    const char* output_prefix, 
						    const char* output_suffix, 
						    const char* avg_output_prefix, 
						    const char* avg_output_suffix, 
						    unsigned int average_items, 
						    short cpu_number ) :

	MLUCValueMonitorCPU( filename, 
			     fileDescrHeader, 
			     output_prefix, 
			     output_suffix, 
			     avg_output_prefix, 
			     avg_output_suffix, 
			     average_items,
			     cpu_number )
{
}


MLUCValueMonitorCPU_Sys :: MLUCValueMonitorCPU_Sys ( const char* output_prefix, 
						     const char* output_suffix, 
						     const char* avg_output_prefix, 
						     const char* avg_output_suffix, 
						     unsigned int average_items,
						     short cpu_number ) :
	
	MLUCValueMonitorCPU( output_prefix, 
			     output_suffix, 
			     avg_output_prefix, 
			     avg_output_suffix, 
			     average_items,
			     cpu_number )
{
}


MLUCValueMonitorCPU_Sys :: MLUCValueMonitorCPU_Sys( const char* filename, 
						    const char* fileDescrHeader, 
						    unsigned int average_items,
						    short cpu_number ) :

	MLUCValueMonitorCPU( filename, 
			     fileDescrHeader, 
			     average_items,
			     cpu_number )
{
}


MLUCValueMonitorCPU_Sys :: MLUCValueMonitorCPU_Sys( unsigned int average_items,
						    short cpu_number ) :

	MLUCValueMonitorCPU( average_items,
			     cpu_number )
{
}


MLUCValueMonitorCPU_Idle :: MLUCValueMonitorCPU_Idle( const char* filename, 
						      const char* fileDescrHeader, 
						      const char* output_prefix, 
						      const char* output_suffix, 
						      const char* avg_output_prefix, 
						      const char* avg_output_suffix, 
						      unsigned int average_items, 
						      short cpu_number ) :

	MLUCValueMonitorCPU( filename, 
			     fileDescrHeader, 
			     output_prefix, 
			     output_suffix, 
			     avg_output_prefix, 
			     avg_output_suffix, 
			     average_items,
			     cpu_number )
{
}

MLUCValueMonitorCPU_Idle :: MLUCValueMonitorCPU_Idle ( const char* output_prefix, 
						       const char* output_suffix, 
						       const char* avg_output_prefix, 
						       const char* avg_output_suffix, 
						       unsigned int average_items,
						       short cpu_number ) :
	
	MLUCValueMonitorCPU( output_prefix, 
			     output_suffix, 
			     avg_output_prefix, 
			     avg_output_suffix, 
			     average_items,
			     cpu_number )
{
}

MLUCValueMonitorCPU_Idle :: MLUCValueMonitorCPU_Idle ( const char* filename, 
						       const char* fileDescrHeader, 
						       unsigned int average_items,
						       short cpu_number ) :

	MLUCValueMonitorCPU( filename, 
			     fileDescrHeader, 
			     average_items,
			     cpu_number )
{
}



MLUCValueMonitorCPU_Idle :: MLUCValueMonitorCPU_Idle ( unsigned int average_items,
						       short cpu_number ) :

	MLUCValueMonitorCPU( average_items,
			     cpu_number )
{
}



MLUCValueMonitorCPU_Used :: MLUCValueMonitorCPU_Used( const char* filename, 
						      const char* fileDescrHeader, 
						      const char* output_prefix, 
						      const char* output_suffix, 
						      const char* avg_output_prefix, 
						      const char* avg_output_suffix, 
						      unsigned int average_items, 
						      short cpu_number ) :

	MLUCValueMonitorCPU( filename, 
			     fileDescrHeader, 
			     output_prefix, 
			     output_suffix, 
			     avg_output_prefix, 
			     avg_output_suffix, 
			     average_items,
			     cpu_number )
{
}

MLUCValueMonitorCPU_Used :: MLUCValueMonitorCPU_Used ( const char* output_prefix, 
						       const char* output_suffix, 
						       const char* avg_output_prefix, 
						       const char* avg_output_suffix, 
						       unsigned int average_items,
						       short cpu_number ) :
	
	MLUCValueMonitorCPU( output_prefix, 
			     output_suffix, 
			     avg_output_prefix, 
			     avg_output_suffix, 
			     average_items,
			     cpu_number )
{
}

MLUCValueMonitorCPU_Used :: MLUCValueMonitorCPU_Used ( const char* filename, 
						       const char* fileDescrHeader, 
						       unsigned int average_items,
						       short cpu_number ) :

	MLUCValueMonitorCPU( filename, 
			     fileDescrHeader, 
			     average_items,
			     cpu_number )
{
}


MLUCValueMonitorCPU_Used :: MLUCValueMonitorCPU_Used ( unsigned int average_items,
						       short cpu_number ) :

	MLUCValueMonitorCPU( average_items,
			     cpu_number )
{
}


int MLUCValueMonitorCPU_User :: GetValue( uint64& MeasuredValue ) {
	
	char   kLine[5000];    // intr line can be quite long
	
	std :: ifstream in( "/proc/stat" );
	if( !in ) {
		LOG( MLUCLog::kError, "MLUCValueMonitorCPU_User:GetValue", "Error opening file" )
			<< "Could not open /proc/stat" << ENDLOG;
		return ENOENT;
	} else {
		int no;
		unsigned long user_val, nice_val, sys_val, idle_val;
		
                switch( kCPU_Number ) {
		case -1: // overall cpu usage
			while( !in.eof() ) {
				in.getline( kLine, 5000 );
				if( sscanf( kLine, "cpu %lu %lu %lu %lu", &user_val, &nice_val, &sys_val, &idle_val ) == 4 ){
					if( kFirstReadOut ) {
						kUserOldValue = user_val;
						kNiceOldValue = nice_val;
						kSysOldValue  = sys_val;
						kIdleOldValue = idle_val;
						
						kFirstReadOut = false;
						return ENOTSUP; // first readout (op not supported)
					} else {
						kUserNewValue = user_val;
						kNiceNewValue = nice_val;
						kSysNewValue  = sys_val;
						kIdleNewValue = idle_val;
						
						CalculateValue( USER, MeasuredValue );
						
						kUserOldValue = kUserNewValue;
						kNiceOldValue = kNiceNewValue;
						kSysOldValue  = kSysNewValue;
						kIdleOldValue = kIdleNewValue;
						
						return 0;
					}
				}
			}

		default: // look at cpu 0, 1, 2 or what ever you have ...
		    kUserNewValue = ~(unsigned long)0;
		    kNiceNewValue = ~(unsigned long)0;
		    kSysNewValue  = ~(unsigned long)0;
		    kIdleNewValue = ~(unsigned long)0;
			while( !in.eof() ) {
				in.getline( kLine, 5000 );
				if( sscanf( kLine,"cpu %d %lu %lu %lu %lu", &no, &user_val, &nice_val, &sys_val, &idle_val ) == 5 ){
					if( no == kCPU_Number ) {
						if( kFirstReadOut ) {
							kUserOldValue = user_val;
							kNiceOldValue = nice_val;
							kSysOldValue  = sys_val;
							kIdleOldValue = idle_val;
							
							kFirstReadOut = false;
							return ENOTSUP; // first readout (op not supported)
						} else {
							kUserNewValue = user_val;
							kNiceNewValue = nice_val;
							kSysNewValue  = sys_val;
							kIdleNewValue = idle_val;
							
							CalculateValue( USER, MeasuredValue );
							
														
						}
					}
				}
			}
			kUserOldValue = kUserNewValue;
			kNiceOldValue = kNiceNewValue;
			kSysOldValue  = kSysNewValue;
			kIdleOldValue = kIdleNewValue;
			break;
		}
	}
	return 0;
}

int MLUCValueMonitorCPU_Nice :: GetValue( uint64& MeasuredValue ) {

	char   kLine[5000];    // intr line can be quite long
	
	std :: ifstream in( "/proc/stat" );
	if( !in ) {
		LOG( MLUCLog::kError, "MLUCValueMonitorCPU_Nice:GetValue", "Error opening file" )
			<< "Could not open /proc/stat" << ENDLOG;
		return ENOENT;
	} else {
		int no;
		unsigned long user_val, nice_val, sys_val, idle_val;
		switch( kCPU_Number ) {
				
		case -1: // overall cpu usage
			while( !in.eof() ) {
				in.getline( kLine, 5000 );
				if( sscanf( kLine, "cpu %lu %lu %lu %lu", &user_val, &nice_val, &sys_val, &idle_val ) == 4 ){
					if( kFirstReadOut ) {
						kUserOldValue = user_val;
						kNiceOldValue = nice_val;
						kSysOldValue  = sys_val;
						kIdleOldValue = idle_val;
						
						kFirstReadOut = false;
						return ENOTSUP; // first readout (op not supported)
					} else {
						kUserNewValue = user_val;
						kNiceNewValue = nice_val;
						kSysNewValue  = sys_val;
						kIdleNewValue = idle_val;
						
						CalculateValue( NICE, MeasuredValue );
						
						kUserOldValue = kUserNewValue;
						kNiceOldValue = kNiceNewValue;
						kSysOldValue  = kSysNewValue;
						kIdleOldValue = kIdleNewValue;
						
						return 0;
					}
				}
			}

		default: // look at cpu 0, 1, 2 or what ever you have ...
		    kUserNewValue = ~(unsigned long)0;
		    kNiceNewValue = ~(unsigned long)0;
		    kSysNewValue  = ~(unsigned long)0;
		    kIdleNewValue = ~(unsigned long)0;
			while( !in.eof() ) {
				in.getline( kLine, 5000 );
				if( sscanf( kLine,"cpu %d %lu %lu %lu %lu", &no, &user_val, &nice_val, &sys_val, &idle_val ) == 5 ){
					if( no == kCPU_Number ) {
						if( kFirstReadOut ) {
							kUserOldValue = user_val;
							kNiceOldValue = nice_val;
							kSysOldValue  = sys_val;
							kIdleOldValue = idle_val;
							
							kFirstReadOut = false;
							return ENOTSUP; // first readout (op not supported)
						} else {
							kUserNewValue = user_val;
							kNiceNewValue = nice_val;
							kSysNewValue  = sys_val;
							kIdleNewValue = idle_val;
							
							CalculateValue( NICE, MeasuredValue );
							
						}
					}
				}
			}
			kUserOldValue = kUserNewValue;
			kNiceOldValue = kNiceNewValue;
			kSysOldValue  = kSysNewValue;
			kIdleOldValue = kIdleNewValue;
			
			break;
		}
	}
	return 0;
}

int MLUCValueMonitorCPU_Sys :: GetValue( uint64& MeasuredValue ) {
	
	char   kLine[5000];    // intr line can be quite long
	
	std :: ifstream in( "/proc/stat" );
	if( !in ) {
		LOG( MLUCLog::kError, "MLUCValueMonitorCPU_Sys:GetValue", "Error opening file" )
			<< "Could not open /proc/stat" << ENDLOG;
		return ENOENT;
	} else {
		int no;
		unsigned long user_val, nice_val, sys_val, idle_val;
		switch( kCPU_Number ) {
				
		case -1: // overall cpu usage
			while( !in.eof() ) {
				in.getline( kLine, 5000 );
				if( sscanf( kLine, "cpu %lu %lu %lu %lu", &user_val, &nice_val, &sys_val, &idle_val ) == 4 ){
					if( kFirstReadOut ) {
						kUserOldValue = user_val;
						kNiceOldValue = nice_val;
						kSysOldValue  = sys_val;
						kIdleOldValue = idle_val;
						
						kFirstReadOut = false;
						return ENOTSUP; // first readout (op not supported)
					} else {
						kUserNewValue = user_val;
						kNiceNewValue = nice_val;
						kSysNewValue  = sys_val;
						kIdleNewValue = idle_val;
						
						CalculateValue( SYS, MeasuredValue );
						
						kUserOldValue = kUserNewValue;
						kNiceOldValue = kNiceNewValue;
						kSysOldValue  = kSysNewValue;
						kIdleOldValue = kIdleNewValue;
						
						return 0;
					}
				}
			}

		default: // look at cpu 0, 1, 2 or what ever you have ...
		    kUserNewValue = ~(unsigned long)0;
		    kNiceNewValue = ~(unsigned long)0;
		    kSysNewValue  = ~(unsigned long)0;
		    kIdleNewValue = ~(unsigned long)0;
			while( !in.eof() ) {
				in.getline( kLine, 5000 );
				if( sscanf( kLine,"cpu %d %lu %lu %lu %lu", &no, &user_val, &nice_val, &sys_val, &idle_val ) == 5 ){
					if( no == kCPU_Number ) {
						if( kFirstReadOut ) {
							kUserOldValue = user_val;
							kNiceOldValue = nice_val;
							kSysOldValue  = sys_val;
							kIdleOldValue = idle_val;
							
							kFirstReadOut = false;
							return ENOTSUP; // first readout (op not supported)
						} else {
							kUserNewValue = user_val;
							kNiceNewValue = nice_val;
							kSysNewValue  = sys_val;
							kIdleNewValue = idle_val;
							
							CalculateValue( SYS, MeasuredValue );
							
						}
					}
				}
			}
			kUserOldValue = kUserNewValue;
			kNiceOldValue = kNiceNewValue;
			kSysOldValue  = kSysNewValue;
			kIdleOldValue = kIdleNewValue;
														
			break;
		}
	}
	return 0;
}

int MLUCValueMonitorCPU_Idle :: GetValue( uint64& MeasuredValue ) {
	
	char   kLine[5000];    // intr line can be quite long
	
	std :: ifstream in( "/proc/stat" );
	if( !in ) {
		LOG( MLUCLog::kError, "MLUCValueMonitorCPU_Idle:GetValue", "Error opening file" )
			<< "Could not open /proc/stat" << ENDLOG;
		return ENOENT;
	} else {
		int no;
		unsigned long user_val, nice_val, sys_val, idle_val;
		switch( kCPU_Number ) {
				
		case -1: // overall cpu usage
			while( !in.eof() ) {
				in.getline( kLine, 5000 );
				if( sscanf( kLine, "cpu %lu %lu %lu %lu", &user_val, &nice_val, &sys_val, &idle_val ) == 4 ){
					if( kFirstReadOut ) {
						kUserOldValue = user_val;
						kNiceOldValue = nice_val;
						kSysOldValue  = sys_val;
						kIdleOldValue = idle_val;
						
						kFirstReadOut = false;
						return ENOTSUP; // first readout (op not supported)
					} else {
						kUserNewValue = user_val;
						kNiceNewValue = nice_val;
						kSysNewValue  = sys_val;
						kIdleNewValue = idle_val;
						
						CalculateValue( IDLE, MeasuredValue );
						
						kUserOldValue = kUserNewValue;
						kNiceOldValue = kNiceNewValue;
						kSysOldValue  = kSysNewValue;
						kIdleOldValue = kIdleNewValue;
						
						return 0;
					}
				}
			}

		default: // look at cpu 0, 1, 2 or what ever you have ...
		    kUserNewValue = ~(unsigned long)0;
		    kNiceNewValue = ~(unsigned long)0;
		    kSysNewValue  = ~(unsigned long)0;
		    kIdleNewValue = ~(unsigned long)0;
			while( !in.eof() ) {
				in.getline( kLine, 5000 );
				if( sscanf( kLine,"cpu %d %lu %lu %lu %lu", &no, &user_val, &nice_val, &sys_val, &idle_val ) == 5 ){
					if( no == kCPU_Number ) {
						if( kFirstReadOut ) {
							kUserOldValue = user_val;
							kNiceOldValue = nice_val;
							kSysOldValue  = sys_val;
							kIdleOldValue = idle_val;
							
							kFirstReadOut = false;
							return ENOTSUP; // first readout (op not supported)
						} else {
							kUserNewValue = user_val;
							kNiceNewValue = nice_val;
							kSysNewValue  = sys_val;
							kIdleNewValue = idle_val;
							
							CalculateValue( IDLE, MeasuredValue );
							
						}
					}
				}
			}
			kUserOldValue = kUserNewValue;
			kNiceOldValue = kNiceNewValue;
			kSysOldValue  = kSysNewValue;
			kIdleOldValue = kIdleNewValue;
														
			break;
		}
	}
	return 0;
}



int MLUCValueMonitorCPU_Used :: GetValue( uint64& MeasuredValue ) {
	
	char   kLine[5000];    // intr line can be quite long
	
	std :: ifstream in( "/proc/stat" );
	if( !in ) {
		LOG( MLUCLog::kError, "MLUCValueMonitorCPU_Used:GetValue", "Error opening file" )
			<< "Could not open /proc/stat" << ENDLOG;
		return ENOENT;
	} else {
		int no;
		unsigned long user_val, nice_val, sys_val, idle_val;
		switch( kCPU_Number ) {
				
		case -1: // overall cpu usage
			while( !in.eof() ) {
				in.getline( kLine, 5000 );
				if( sscanf( kLine, "cpu %lu %lu %lu %lu", &user_val, &nice_val, &sys_val, &idle_val ) == 4 ){
					if( kFirstReadOut ) {
						kUserOldValue = user_val;
						kNiceOldValue = nice_val;
						kSysOldValue  = sys_val;
						kIdleOldValue = idle_val;
						
						kFirstReadOut = false;
						return ENOTSUP; // first readout (op not supported)
					} else {
						kUserNewValue = user_val;
						kNiceNewValue = nice_val;
						kSysNewValue  = sys_val;
						kIdleNewValue = idle_val;
						
						CalculateValue( USED, MeasuredValue );
					
						kUserOldValue = kUserNewValue;
						kNiceOldValue = kNiceNewValue;
						kSysOldValue  = kSysNewValue;
						kIdleOldValue = kIdleNewValue;
						
						return 0;
					}
				}
			}

		default: // look at cpu 0, 1, 2 or what ever you have ...
		    kUserNewValue = ~(unsigned long)0;
		    kNiceNewValue = ~(unsigned long)0;
		    kSysNewValue  = ~(unsigned long)0;
		    kIdleNewValue = ~(unsigned long)0;
			while( !in.eof() ) {
				in.getline( kLine, 5000 );
				if( sscanf( kLine,"cpu %d %lu %lu %lu %lu", &no, &user_val, &nice_val, &sys_val, &idle_val ) == 5 ){
					if( no == kCPU_Number ) {
						if( kFirstReadOut ) {
							kUserOldValue = user_val;
							kNiceOldValue = nice_val;
							kSysOldValue  = sys_val;
							kIdleOldValue = idle_val;
							
							kFirstReadOut = false;
							return ENOTSUP; // first readout (op not supported)
						} else {
							kUserNewValue = user_val;
							kNiceNewValue = nice_val;
							kSysNewValue  = sys_val;
							kIdleNewValue = idle_val;
							
							CalculateValue( USED, MeasuredValue );
							
						}
					}
				}
			}
			kUserOldValue = kUserNewValue;
			kNiceOldValue = kNiceNewValue;
			kSysOldValue  = kSysNewValue;
			kIdleOldValue = kIdleNewValue;
			
			break;
		}
	}
	return 0;
}

