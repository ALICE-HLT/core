/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCBufferingLogServer.hpp"


MLUCBufferingLogServer::MLUCBufferingLogServer( unsigned int bufferCount, unsigned int waitTime_ms ):
    fMonitorThread( this ), fBufferCount( bufferCount ), fWaitTime( waitTime_ms )
    {
    fQuitMonitor = false;
    pthread_mutex_init( &fMutex, NULL );
    fLastLogTime.tv_sec = fLastLogTime.tv_usec = 0;
    }

MLUCBufferingLogServer::~MLUCBufferingLogServer()
    {
    pthread_mutex_destroy( &fMutex );
    while ( fLogMsgs.size()>0 )
	{
	//free( fLogMsgs.begin()->fLine );
	fLogMsgs.erase( fLogMsgs.begin() );
	}
    }
	    
void MLUCBufferingLogServer::StartThread()
    {
    fQuitMonitor = false;
    fMonitorThread.Start();
    }

void MLUCBufferingLogServer::StopThread()
    {
    fQuitMonitor = true;
    usleep( 20000 );
    }
	    
void MLUCBufferingLogServer::AddServer( MLUCLogServer* server )
    {
    fServers.insert( fServers.end(), server );
    }

void MLUCBufferingLogServer::DelServer( MLUCLogServer* server )
    {
    vector<MLUCLogServer*>::iterator iter = fServers.begin();
    while ( iter!=fServers.end() )
	{
	if ( *iter==server )
	    {
	    fServers.erase( iter );
	    return;
	    }
	iter++;
	}
    }
void MLUCBufferingLogServer::LogLine( const char* origin, const char* keyword, const char* file, int linenr, 
				      const char* compile_date, const char* compile_time, MLUCLog::TLogLevel logLvl, 
				      const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
				      unsigned long runNumber, 
#endif
				      
				      uint32 ldate, uint32 ltime_s, uint32 ltime_us, uint32 msgNr, const char* line )
    {
    TLogMsgData msgData( origin, keyword, file, linenr, compile_date, compile_time, logLvl, hostname, 
			 id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
			 runNumber, 
#endif
			 ldate, ltime_s, ltime_us, msgNr, strdup( line ) );

    pthread_mutex_lock( &fMutex );
    if ( fLogMsgs.size()>=fBufferCount )
	{
	//free( fLogMsgs.begin()->fLine );
	fLogMsgs.erase( fLogMsgs.begin() );
	}
    fLogMsgs.insert( fLogMsgs.end(), msgData );
    gettimeofday( &fLastLogTime, NULL );
    pthread_mutex_unlock( &fMutex );
    }

void MLUCBufferingLogServer::LogLine( const char* origin, const char* keyword, const char* file, int linenr, 
				      const char* compile_date, const char* compile_time, MLUCLog::TLogLevel logLvl, 
				      const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
				      unsigned long runNumber, 
#endif
				      uint32 ldate, 
				      uint32 ltime_s, uint32 ltime_us, uint32 msgNr, uint32 subMsgNr, const char* line )
    {
    TLogMsgData msgData( origin, keyword, file, linenr, compile_date, compile_time, logLvl, hostname, 
			 id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
			 runNumber, 
#endif
			 ldate, ltime_s, ltime_us, msgNr, subMsgNr, strdup( line ) );

    pthread_mutex_lock( &fMutex );
    if ( fLogMsgs.size()>=fBufferCount )
	{
	//free( fLogMsgs.begin()->fLine );
	fLogMsgs.erase( fLogMsgs.begin() );
	}
    fLogMsgs.insert( fLogMsgs.end(), msgData );
    gettimeofday( &fLastLogTime, NULL );
    pthread_mutex_unlock( &fMutex );
    }

void MLUCBufferingLogServer::FlushMessages()
    {
    vector<MLUCLogServer*>::iterator serverIter, serverEnd;
    vector<TLogMsgData>::iterator msgIter, msgEnd;
    pthread_mutex_lock( &fMutex );
    msgIter = fLogMsgs.begin();
    msgEnd = fLogMsgs.end();
    while( fLogMsgs.size()>0 )
	{
	msgIter = fLogMsgs.begin();
	serverIter = fServers.begin();
	serverEnd = fServers.end();
	while ( serverIter != serverEnd )
	    {
	    if ( msgIter->fHasSubMsgNr )
		{
		(*serverIter)->LogLine( msgIter->fOrigin.c_str(), msgIter->fKeyword.c_str(), msgIter->fFile, msgIter->fLineNr,
					msgIter->fCompileDate, msgIter->fCompileTime, msgIter->fLogLevel, msgIter->fHostname.c_str(), 
					msgIter->fID.c_str(), 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
					msgIter->fRunNumber, 
#endif
					msgIter->fLDate, msgIter->fLTime_s, 
					msgIter->fLTime_us, msgIter->fMsgNr, msgIter->fSubMsgNr, msgIter->fLine.c_str() );
		}
	    else
		{
		(*serverIter)->LogLine( msgIter->fOrigin.c_str(), msgIter->fKeyword.c_str(), msgIter->fFile, msgIter->fLineNr,
					msgIter->fCompileDate, msgIter->fCompileTime, msgIter->fLogLevel, msgIter->fHostname.c_str(),
					msgIter->fID.c_str(), 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
					msgIter->fRunNumber, 
#endif
					msgIter->fLDate, msgIter->fLTime_s, 
					msgIter->fLTime_us, msgIter->fMsgNr, msgIter->fLine.c_str() );
		}
	    serverIter++;
	    }
	//free( msgIter->fLine );
	fLogMsgs.erase( msgIter );
	}
    fLastLogTime.tv_sec = fLastLogTime.tv_usec = 0;
    pthread_mutex_unlock( &fMutex );
    }

void MLUCBufferingLogServer::MonitoringFunc()
    {
    struct timeval now;
    unsigned int dt;
    while ( !fQuitMonitor )
	{
	if ( fLastLogTime.tv_sec!=0 || fLastLogTime.tv_usec!=0 )
	    {
	    gettimeofday( &now, NULL );
	    dt = (now.tv_sec-fLastLogTime.tv_sec)*1000 + (now.tv_usec-fLastLogTime.tv_usec)/1000;
	    if ( dt >= fWaitTime )
		{
		FlushMessages();
		}
	    }
	usleep( 10000 );
	}
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
