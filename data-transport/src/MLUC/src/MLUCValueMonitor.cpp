/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCValueMonitor.hpp"
#include <sys/time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>


MLUCValueMonitor::MLUCValueMonitor( unsigned int average_items )
    {
    fFilename = "";
    fDoFileOutput = false;
    fFile = -1;
    fFileDescrHeader = "";

    fOutputPrefix = "";
    fOutputSuffix = "";
    fAvgOutputPrefix = "";
    fAvgOutputSuffix = "";
    fDoStdoutOutput = false;
    fTimeStampOutput = true;

    fAverageHistory = (uint64*)malloc( sizeof(uint64)*average_items );
    if ( !fAverageHistory )
	{
	}
    fHistoryItemCount = average_items;
    fHistoryAvailable = 0;
    fLastHistory = (unsigned int)-1;
    }

MLUCValueMonitor::MLUCValueMonitor( const char* filename, const char* fileDescrHeader, 
				    const char* output_prefix, const char* output_suffix, 
				    const char* avg_output_prefix, const char* avg_output_suffix, 
				    unsigned int average_items )
    {
    fFilename = filename;
    fDoFileOutput = true;
    fFile = -1;
    fFileDescrHeader = fileDescrHeader;

    fOutputPrefix = output_prefix;
    fOutputSuffix = output_suffix;
    fAvgOutputPrefix = avg_output_prefix;
    fAvgOutputSuffix = avg_output_suffix;
    fDoStdoutOutput = true;
    fTimeStampOutput = true;

    fAverageHistory = (uint64*)malloc( sizeof(uint64)*average_items );
    if ( !fAverageHistory )
	{
	}
    fHistoryItemCount = average_items;
    fHistoryAvailable = 0;
    fLastHistory = (unsigned int)-1;
    }

MLUCValueMonitor::MLUCValueMonitor( const char* output_prefix, const char* output_suffix, 
				    const char* avg_output_prefix, const char* avg_output_suffix, 
				    unsigned int average_items )
    {
    fFilename = "";
    fDoFileOutput = false;
    fFile = -1;
    fFileDescrHeader = "";

    fOutputPrefix = output_prefix;
    fOutputSuffix = output_suffix;
    fAvgOutputPrefix = avg_output_prefix;
    fAvgOutputSuffix = avg_output_suffix;
    fDoStdoutOutput = true;
    fTimeStampOutput = true;

    fAverageHistory = (uint64*)malloc( sizeof(uint64)*average_items );
    if ( !fAverageHistory )
	{
	}
    fHistoryItemCount = average_items;
    fHistoryAvailable = 0;
    fLastHistory = 0;
    }

MLUCValueMonitor::MLUCValueMonitor( const char* filename, const char* fileDescrHeader, unsigned int average_items )
    {
    fFilename = filename;
    fDoFileOutput = true;
    fFile = -1;
    fFileDescrHeader = fileDescrHeader;

    fOutputPrefix = "";
    fOutputSuffix = "";
    fAvgOutputPrefix = "";
    fAvgOutputSuffix = "";
    fDoStdoutOutput = false;
    fTimeStampOutput = true;

    fAverageHistory = (uint64*)malloc( sizeof(uint64)*average_items );
    if ( !fAverageHistory )
	{
	}
    fHistoryItemCount = average_items;
    fHistoryAvailable = 0;
    fLastHistory = 0;
    }

MLUCValueMonitor::MLUCValueMonitor( const char* fileDescrHeader, unsigned int average_items )
    {
    fFilename = "";
    fDoFileOutput = false;
    fFile = -1;
    fFileDescrHeader = fileDescrHeader;

    fOutputPrefix = "";
    fOutputSuffix = "";
    fAvgOutputPrefix = "";
    fAvgOutputSuffix = "";
    fDoStdoutOutput = false;
    fTimeStampOutput = true;

    fAverageHistory = (uint64*)malloc( sizeof(uint64)*average_items );
    if ( !fAverageHistory )
	{
	}
    fHistoryItemCount = average_items;
    fHistoryAvailable = 0;
    fLastHistory = 0;
    }

MLUCValueMonitor::~MLUCValueMonitor()
    {
    if ( fAverageHistory )
	free( fAverageHistory );
    }

int MLUCValueMonitor::GetValue( uint64& measuredValue, struct timeval& timestamp )
    {
    struct timeval t1, t2, avg;
    int retval;
    gettimeofday( &t1, NULL );
    retval = GetValue( measuredValue );
    gettimeofday( &t2, NULL );
    if ( t2.tv_sec > t1.tv_sec )
	{
	t2.tv_usec += (t2.tv_sec-t1.tv_sec)*1000000;
	t2.tv_sec = t1.tv_sec;
	}
    avg.tv_sec = t1.tv_sec;
    avg.tv_usec = (t1.tv_usec+t2.tv_usec)/2;
    while ( avg.tv_usec>1000000 )
	{
	avg.tv_sec += 1;
	avg.tv_usec -= 1000000;
	}
    timestamp = avg;
    return retval;
    }


int MLUCValueMonitor::OutputValue( uint64 value, bool outputAvg )
    {
    struct timeval now;
    gettimeofday( &now, NULL );
    return OutputValue( value, now, outputAvg );
    }

int MLUCValueMonitor::OutputValue( uint64 value, const struct timeval& now, bool outputAvg )
    {
    struct tm* t;
    unsigned int date, time_s, time_us;
    t = localtime( &(now.tv_sec) );
    t->tm_mon++;
    t->tm_year += 1900;
    date = t->tm_year*10000+t->tm_mon*100+t->tm_mday;
    time_s = t->tm_hour*10000+t->tm_min*100+t->tm_sec;
    time_us = now.tv_usec;

    int i, nextHist = fLastHistory+1;
    uint64 avg = 0;
    if ( nextHist>=fHistoryItemCount )
	nextHist = 0;
    fAverageHistory[ nextHist ] = value;
    if ( fHistoryAvailable < fHistoryItemCount )
	fHistoryAvailable++;

    for ( i = 0; i < fHistoryAvailable; i++ )
	{
	avg += fAverageHistory[ i ];
	}
    avg /= fHistoryAvailable;
    fLastHistory = nextHist;
    if ( fDoStdoutOutput )
	{
	if ( fTimeStampOutput )
	    printf( "%08u.%06u.%06u: ", date, time_s, time_us );
	printf( "%s%20Lu%s\n", fOutputPrefix.c_str(), (unsigned long long)value, fOutputSuffix.c_str() );
	if ( fHistoryItemCount > 0 && outputAvg )
	    {
	    if ( fTimeStampOutput )
		printf( "                        " );
	    printf( "%s%20Lu%s\n", 
		    fAvgOutputPrefix.c_str(), (unsigned long long)avg, fAvgOutputSuffix.c_str() );
	    }
	
	}
    if ( fDoFileOutput && fFile!=-1 )
	{
	int ret;
	ret = write( fFile, &now, sizeof(now) );
	if ( ret != sizeof(now) )
	    return errno;
	ret = write( fFile, &value, sizeof(value) );
	if ( ret != sizeof(value) )
	    return errno;
	}
    return 0;
    }

int MLUCValueMonitor::StartMonitoring()
    {
    uint32 endian = 0x01020304;
    if ( fDoFileOutput && fFile==-1 )
	{
	int ret;
	fFile = open( fFilename.c_str(), O_WRONLY|O_CREAT|O_EXCL, 0666 );
	if ( fFile==-1 )
	    return errno;

	// First write a 32bit uint to enable determination of the byte order
	// the originating machine.
	ret = write( fFile, &endian, 4 );
	if ( ret != 4 )
	    return errno;

	// Write a 32 bit uint holding the size of a struct timeval structure
	// used for storing the timestamps of the measured values.
	uint32 timesize = sizeof(struct timeval);
	ret = write( fFile, &timesize, 4 );
	if ( ret != 4 )
	    return errno;

	// Write the file description header.
	uint32 fileDescrSize = strlen( fFileDescrHeader.c_str() );
	ret = write( fFile, &fileDescrSize, 4 );
	if ( ret != 4 )
	    return errno;
	
	ret = write( fFile, fFileDescrHeader.c_str(), fileDescrSize );
	if ( ret != (int)fileDescrSize )
	    return errno;
	}
    return 0;
    }

int MLUCValueMonitor::StopMonitoring()
    {
    if ( fDoFileOutput )
	{
	if ( fFile )
	    close( fFile );
	else
	    return ENODEV;
	}
    return 0;
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
