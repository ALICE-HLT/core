/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include <MLUCDynamicLibrary.hpp>
#include <errno.h>


MLUCDynamicLibrary::MLUCDynamicLibrary()
    {
    fLibName = NULL;
    fHandle = NULL;
    }
MLUCDynamicLibrary::~MLUCDynamicLibrary()
    {
    if ( fHandle )
	Close();
    }

int MLUCDynamicLibrary::Open( const char* libName )
    {
    if ( !libName )
	return ENODEV;
    fLibName = libName;
    //fHandle = dlopen( fLibName, RTLD_NOW|RTLD_GLOBAL );
    fHandle = dlopen( fLibName, RTLD_LAZY|RTLD_GLOBAL );
    if ( !fHandle )
	{
	fLastError = dlerror();
	return EIO;
	}
    return 0;
    }
	
int MLUCDynamicLibrary::Close()
    {
    if ( fHandle )
	dlclose( fHandle );
    return 0;
    }

int MLUCDynamicLibrary::GetSym( const char* symName, void*& sym )
    {
    char* err;
    sym = dlsym( fHandle, symName );
    err = dlerror();
    if ( err != NULL )
	{
	fLastError = err;
	return EIO;
	}
    return 0;
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
