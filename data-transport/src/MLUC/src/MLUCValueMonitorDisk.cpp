/*
***************************************************************************
**
** $Author$ - Initial Version by Arne Wiebalck 
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCValueMonitorDisk.hpp"
#include <fstream>
#include <string>
#if __GNUC__>=3
#include <sstream>
#include <iosfwd>
#else
#include <sstream>
#include <strstream>
#endif

MLUCValueMonitorDisk :: MLUCValueMonitorDisk( const char* filename, 
					      const char* fileDescrHeader, 
					      const char* output_prefix, 
					      const char* output_suffix, 
					      const char* avg_output_prefix, 
					      const char* avg_output_suffix, 
					      unsigned int average_items, 
					      short throughput_type, 
					      short disk_id ) :
	
	MLUCValueMonitor( filename, 
			  fileDescrHeader, 
			  output_prefix, 
			  output_suffix, 
			  avg_output_prefix, 
			  avg_output_suffix, 
			  average_items )
{
	fType_Of_Throughput = throughput_type;
	fDisk_ID            = disk_id; 
	fFirstReadOut       = true;
	fNumber_Of_Disks    = 0;
	//	GetNumberOfDisks();
	
	fOld_Sum = 0; 
        fNew_Sum = 0;	
      
}

MLUCValueMonitorDisk :: MLUCValueMonitorDisk ( const char* output_prefix, 
					       const char* output_suffix, 
					       const char* avg_output_prefix, 
					       const char* avg_output_suffix, 
					       unsigned int average_items,
					       short throughput_type, 
					       short disk_id ) :
	
	MLUCValueMonitor( output_prefix, 
			  output_suffix, 
			  avg_output_prefix, 
			  avg_output_suffix, 
			  average_items )
{
	fType_Of_Throughput = throughput_type;
	fDisk_ID            = disk_id;
	fFirstReadOut       = true;
	fNumber_Of_Disks    = 0;
	//	GetNumberOfDisks();

	fOld_Sum = 0; 
        fNew_Sum = 0;
}

MLUCValueMonitorDisk :: MLUCValueMonitorDisk ( const char* filename, 
					       const char* fileDescrHeader, 
					       unsigned int average_items,
					       short throughput_type, 
					       short disk_id ) : 

	MLUCValueMonitor( filename, 
			  fileDescrHeader, 
			  average_items )
{
	fType_Of_Throughput = throughput_type;
	fDisk_ID            = disk_id;
	fFirstReadOut       = true;
	fNumber_Of_Disks    = 0;
	//	GetNumberOfDisks();
	
        fOld_Sum = 0; 
	fNew_Sum = 0;
}

MLUCValueMonitorDisk :: MLUCValueMonitorDisk ( unsigned int average_items,
					       short throughput_type, 
					       short disk_id ) : 

	MLUCValueMonitor( average_items )
{
	fType_Of_Throughput = throughput_type;
	fDisk_ID            = disk_id;
	fFirstReadOut       = true;
	fNumber_Of_Disks    = 0;
	//	GetNumberOfDisks();
	
        fOld_Sum = 0; 
	fNew_Sum = 0;
}

int MLUCValueMonitorDisk :: GetNumberOfDisks() {
	
	char kLine[500]; // be on the safe side ;-)
	int  dummy;

	std :: ifstream in( "/proc/stat" );
	if( !in ) {
		LOG( MLUCLog::kError, "MLUCValueMonitorDisk:GetNumberOfDisks", "Error opening file" )
			<< "Could not open /proc/stat" << ENDLOG;
		return ENOENT;
	} else {
		while( !in.eof() ) {
			in.getline( kLine, 500 );
			// only 1 disk
			if( sscanf( kLine, " disk_io: (3,%d): ", &dummy) > 0 ) {
				fNumber_Of_Disks++;
				// 2 disks
				if( sscanf( kLine, " disk_io: (3,%d): (%d,%d,%d,%d) (3,%d):", &dummy, &dummy, &dummy, &dummy, &dummy, &dummy ) > 0 ) {
					fNumber_Of_Disks++;
					// 3 disks
					if( sscanf( kLine, " disk_io: (3,%d): (%d,%d,%d,%d) (3,%d): (%d,%d,%d,%d) (3,%d): ", 
						    &dummy , &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy ) > 0 ) {
						fNumber_Of_Disks++;
						// 4 disks
						if( sscanf( kLine, " disk_io: (3,%d): (%d,%d,%d,%d) (3,%d): (%d,%d,%d,%d) (3,%d): (%d,%d,%d,%d) (3,%d): ", 
							    &dummy , &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, &dummy, 
							    &dummy, &dummy, &dummy, &dummy, &dummy ) > 0 ) {
							fNumber_Of_Disks++;
						}
					}
				}
			}
		}
	}
	if( fDisk_ID > fNumber_Of_Disks - 1  ) {
		LOG( MLUCLog::kWarning, "MLUCValueMonitorDisk:GetNumberOfDisks", "Wrong Disk ID" )
			<< "Cannot monitor disk" << fDisk_ID << ", since the system has only " 
			<< fNumber_Of_Disks << " disks. Will read out first disk!" 
			<< ENDLOG;	
	}

	return 0;
}

int MLUCValueMonitorDisk :: GetValue( uint64& MeasuredValue ) {
	
        unsigned int major, minor, req_read, req_written, blocks_read, blocks_written, dummy;
	//      char   kLine[500];   
	//	bool found = false;
	const unsigned int IDE0 = 3;
	const unsigned int IDE1 = 22;
	const unsigned int IDE2 = 33;
	const unsigned int IDE3 = 34;
	const unsigned int IDE4 = 56;
	const unsigned int IDE5 = 57;

	std :: ifstream in( "/proc/stat" );
	if( !in ) {
		LOG( MLUCLog::kError, "MLUCValueMonitorDisk:GetValue", "Error opening file" )
			<< "Could not open /proc/stat" << ENDLOG;
		return ENOENT;
	}
	
	string line;
	bool bDiskFound = false;
	while (!bDiskFound && getline(in, line)) {
	  int no = 0;
	  
#if __GNUC__>=3
	  istringstream s(line.c_str());
#else
	  istrstream s(line.c_str(), line.size());
#endif
	  string token;
	  
	  s >> token; // read whitespace seperated string into token
	  if(token != "disk_io:") {
	    continue;
	  }
	  // search for the right disk id
	  while (!bDiskFound && s >> token) {
	    sscanf( token.c_str(), "(%u,%u):(%u,%u,%u,%u,%u)", &major,&minor, &dummy, &req_read,
		    &blocks_read, &req_written, &blocks_written );
	    if (major == IDE0 || major == IDE1 || major == IDE2 ||
	        major == IDE3 || major == IDE4 || major == IDE5) {
	      if (fDisk_ID == (int) minor) {
		bDiskFound = true;
	      }
	    }
	    no++;
	  }
	}
	
	if(!bDiskFound) {
	  return ENOENT;
	}
	
	
	switch (fType_Of_Throughput) {
	case 0: // reqs read 
	  fNew_Sum = req_read;
	  break;
	case 1: // reqs write 
	  fNew_Sum = req_written;
	  break;
	case  2: // blocks read 
	  fNew_Sum = blocks_read;
	  break;
	case  3: // blocks written 
	  fNew_Sum = blocks_written;
	  break;
	case  4: // bytes read - CAUTION: this is based on an assumed block size of 512 Bytes! 
	  fNew_Sum = blocks_read * 512;
	  break;
	case  5: // bytes written - CAUTION: this is based on an assumed block size of 512 Bytes! 
	  fNew_Sum = blocks_written * 512;
	  break;
	}

	if (fFirstReadOut) {
	  fOld_Sum = fNew_Sum;
	  
	  fFirstReadOut = false;
	  return ENOTSUP; // first readout (op not supported)
	}
	
	MeasuredValue = fNew_Sum - fOld_Sum;
	fOld_Sum = fNew_Sum;

	/* else {
		int no;
		unsigned int req_read, req_written, blocks_read, blocks_written, dummy;
		long tmp_sum = 0;

		char tmp_devid[32];
		sprintf( tmp_devid, "-%d", fDisk_ID );
		//printf( "Disk ID is %d, no is %d\n", fDisk_ID, no );
		switch ( fType_Of_Throughput ) {
		
		case  0: // reqs read 
				
		  tmp_sum = 0;
		  while( !in.eof() ) {

		    in.getline( kLine, 500 );

		    if( sscanf( kLine, "disk_io: (3,%u):(%u,%u,%u,%u,%u)", 
				&no, &dummy, &req_read, &blocks_read, &req_written, &blocks_written ) > 0 ) {
		      if( no == fDisk_ID ) {
			found = true;
			tmp_sum += req_read;
			break;
		      }
		    }
		  }
		  if( fFirstReadOut ) {
		    fOld_Sum = tmp_sum;
		    fNew_Sum = tmp_sum;

		    fFirstReadOut = false;
		    return ENOTSUP; // first readout (op not supported)
		  } else {
		    fOld_Sum = fNew_Sum;
		    fNew_Sum = tmp_sum;

		  } 
					
		  MeasuredValue = fNew_Sum - fOld_Sum;
		  if ( !found )
		    MeasuredValue = ~(uint64)0;
		  return 0;
		  
		  case  1: // reqs write 
				
		    tmp_sum = 0;
		    while( !in.eof() ) {
		      
		      in.getline( kLine, 500 );
		      
		    if( sscanf( kLine, "disk_io: (3,%u):(%u,%u,%u,%u,%u)", 
				&no, &dummy, &req_read, &blocks_read, &req_written, &blocks_written ) > 0 ) {
		      if( no == fDisk_ID ) {
			found = true;
			tmp_sum += req_written;
			break;
		      }
		    }
		  }
		  if( fFirstReadOut ) {
		    fOld_Sum = tmp_sum;
		    fNew_Sum = tmp_sum;

		    fFirstReadOut = false;
		    return ENOTSUP; // first readout (op not supported)
		  } else {
		    fOld_Sum = fNew_Sum;
		    fNew_Sum = tmp_sum;

		  } 
					
		  MeasuredValue = fNew_Sum - fOld_Sum;
		  if ( !found )
		    MeasuredValue = ~(uint64)0;
		  return 0;
		  


		  case  2: // blocks read 
				
		    tmp_sum = 0;
		    while( !in.eof() ) {
		      
		      in.getline( kLine, 500 );
		      
		    if( sscanf( kLine, "disk_io: (3,%u):(%u,%u,%u,%u,%u)", 
				&no, &dummy, &req_read, &blocks_read, &req_written, &blocks_written ) > 0 ) {
		      if( no == fDisk_ID ) {
			found = true;
			tmp_sum += blocks_read;
			break;
		      }
		    }
		  }
		  if( fFirstReadOut ) {
		    fOld_Sum = tmp_sum;
		    fNew_Sum = tmp_sum;

		    fFirstReadOut = false;
		    return ENOTSUP; // first readout (op not supported)
		  } else {
		    fOld_Sum = fNew_Sum;
		    fNew_Sum = tmp_sum;

		  } 
					
		  MeasuredValue = fNew_Sum - fOld_Sum;
		  if ( !found )
		    MeasuredValue = ~(uint64)0;
		  return 0;

		  case  3: // blocks written 
				
		    tmp_sum = 0;
		    while( !in.eof() ) {
		      
		      in.getline( kLine, 500 );
		      
		    if( sscanf( kLine, "disk_io: (3,%u):(%u,%u,%u,%u,%u)", 
				&no, &dummy, &req_read, &blocks_read, &req_written, &blocks_written ) > 0 ) {
		      if( no == fDisk_ID ) {
			
			//printf( "no %d, fdisk_id %d\n", no, fDisk_ID );
			found = true;
			tmp_sum += blocks_written;
			break;
		      }
		    }
		  }
		  if( fFirstReadOut ) {
		    fOld_Sum = tmp_sum;
		    fNew_Sum = tmp_sum;

		    fFirstReadOut = false;
		    return ENOTSUP; // first readout (op not supported)
		  } else {
		    fOld_Sum = fNew_Sum;
		    fNew_Sum = tmp_sum;

		  } 
					
		  MeasuredValue = fNew_Sum - fOld_Sum;
		  if ( !found )
		    MeasuredValue = ~(uint64)0;
		  return 0;
	
		case  4: // bytes read - CAUTION: this is based on an assumed block size of 512 Bytes! 
				
		    tmp_sum = 0;
		    while( !in.eof() ) {
		      
		      in.getline( kLine, 500 );
		      
		    if( sscanf( kLine, "disk_io: (3,%u):(%u,%u,%u,%u,%u)", 
				&no, &dummy, &req_read, &blocks_read, &req_written, &blocks_written ) > 0 ) {
		      if( no == fDisk_ID ) {
			
			//printf( "no %d, fdisk_id %d\n", no, fDisk_ID );
			found = true;
			tmp_sum += blocks_read * 512;
			break;
		      }
		    }
		  }
		  if( fFirstReadOut ) {
		    fOld_Sum = tmp_sum;
		    fNew_Sum = tmp_sum;

		    fFirstReadOut = false;
		    return ENOTSUP; // first readout (op not supported)
		  } else {
		    fOld_Sum = fNew_Sum;
		    fNew_Sum = tmp_sum;

		  } 
					
		  MeasuredValue = fNew_Sum - fOld_Sum;
		  if ( !found )
		    MeasuredValue = ~(uint64)0;
		  return 0;

		case  5: // bytes written - CAUTION: this is based on an assumed block size of 512 Bytes! 
				
		    tmp_sum = 0;
		    while( !in.eof() ) {
		      
		      in.getline( kLine, 500 );
		      
		    if( sscanf( kLine, "disk_io: (3,%u):(%u,%u,%u,%u,%u)", 
				&no, &dummy, &req_read, &blocks_read, &req_written, &blocks_written ) > 0 ) {
		      if( no == fDisk_ID ) {
			
			//printf( "no %d, fdisk_id %d\n", no, fDisk_ID );
			found = true;
			tmp_sum += blocks_written * 512;
			break;
		      }
		    }
		  }
		  if( fFirstReadOut ) {
		    fOld_Sum = tmp_sum;
		    fNew_Sum = tmp_sum;

		    fFirstReadOut = false;
		    return ENOTSUP; // first readout (op not supported)
		  } else {
		    fOld_Sum = fNew_Sum;
		    fNew_Sum = tmp_sum;

		  } 
					
		  MeasuredValue = fNew_Sum - fOld_Sum;
		  if ( !found )
		    MeasuredValue = ~(uint64)0;
		  return 0;
	

	 
		}
		
		}*/
	return 0;
}
