/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCSimpleStackMachineAssembler.hpp"
#include "MLUCSimpleStackMachine.hpp"
#include <cstring>
#include <cstdlib>

static inline bool operator<( MLUCString const& s1, MLUCString const& s2 )
    {
    return strcmp( s1.c_str(), s2.c_str() )<0;
    }

static const unsigned gkCodeArrayIncrement = 32;


MLUCSimpleStackMachineAssembler::MLUCSimpleStackMachineAssembler():
    fIP(0),
    fCodeWordCount(0),
    fCodeAllocationWordCount(0),
    fCode(NULL)
    {
    }

MLUCSimpleStackMachineAssembler::~MLUCSimpleStackMachineAssembler()
    {
    SetCodeAllocation( 0 );
    }

void MLUCSimpleStackMachineAssembler::AddSymbol( const char* ID, uint64 value )
    {
    fSymbols.insert( std::pair<MLUCString,uint64>( MLUCString(ID), value ) );
    }


MLUCSimpleStackMachineAssembler::TError MLUCSimpleStackMachineAssembler::Assemble( MLUCString const& text )
    {
    fLabels.clear();
    SetCodeAllocation( 0 );


    std::vector<MLUCString> lines;
    text.Split( lines, ';' );
    std::vector<MLUCString>::iterator lineIter, lineEnd;
    std::vector<MLUCString> label;
    std::vector<MLUCString> tokens;
    TError err;
    unsigned line;

    unsigned loopCnt=0;
    while ( loopCnt<2 )
	{
	if ( loopCnt<1 )
	    fFirstPass = true;
	else
	    fFirstPass = false;
	fIP = 0;
	line=0;
	lineIter = lines.begin();
	lineEnd = lines.end();
	while ( lineIter != lineEnd )
	    {
	    tokens.clear();
	    lineIter->Split( label, ':' );
	    if ( label.size()==2 )
		{
		if ( fFirstPass )
		    {
		    label[0].Split( tokens );
		    StripEmptyTokens( tokens );
		    fLabels.insert( std::pair<MLUCString,unsigned long>( MLUCString(tokens[0]), fIP ) );
		    }
		label[1].Split( tokens );
		}
	    else if ( label.size()==1 )
		label[0].Split( tokens );
	    else
		return TError( TError::kLabelError, "Too many labels in line" );
	    
	    StripEmptyTokens( tokens );
	    err = Assemble( tokens );
	    if ( !err.Ok() )
		return TError( err, lineIter->c_str(), line );
	    ++lineIter;
	    ++line;
	    }
	loopCnt++;
	}


    return TError();
    }

unsigned long MLUCSimpleStackMachineAssembler::GetCodeWordCount() const
    {
    return fCodeWordCount;
    }

uint64 const* MLUCSimpleStackMachineAssembler::GetCode() const
    {
    return fCode;
    }

MLUCSimpleStackMachineAssembler::TError MLUCSimpleStackMachineAssembler::AddWords( unsigned long wordCnt, uint64* words )
    {
    if ( !fFirstPass )
	{
	if ( fCodeWordCount+wordCnt > fCodeAllocationWordCount )
	    {
	    TError ret = SetCodeAllocation( fCodeWordCount+wordCnt+gkCodeArrayIncrement );
	    if ( !ret.Ok() )
		return ret;
	    }
	for ( unsigned long nn=0; nn<wordCnt; nn++, fCodeWordCount++ )
	    fCode[fCodeWordCount] = words[nn];
	}
    fIP += wordCnt;
    return TError();
    }

MLUCSimpleStackMachineAssembler::TError MLUCSimpleStackMachineAssembler::AddWords( std::vector<uint64> words )
    {
    if ( !fFirstPass )
	{
	unsigned long wordCnt = words.size();
	if ( fCodeWordCount+wordCnt > fCodeAllocationWordCount )
	    {
	    TError ret = SetCodeAllocation( fCodeWordCount+wordCnt+gkCodeArrayIncrement );
	    if ( !ret.Ok() )
		return ret;
	    }
	for ( unsigned long nn=0; nn<wordCnt; nn++, fCodeWordCount++ )
	    fCode[fCodeWordCount] = words[nn];
	}
    fIP += words.size();
    return TError();
    }

MLUCSimpleStackMachineAssembler::TError MLUCSimpleStackMachineAssembler::AddWord( uint64 word )
    {
    if ( !fFirstPass )
	{
	if ( fCodeWordCount+1 > fCodeAllocationWordCount )
	    {
	    TError ret = SetCodeAllocation( fCodeWordCount+1+gkCodeArrayIncrement );
	    if ( !ret.Ok() )
		return ret;
	    }
	fCode[fCodeWordCount++] = word;
	}
    ++fIP;
    return TError();
    }

MLUCSimpleStackMachineAssembler::TError MLUCSimpleStackMachineAssembler::AddWords( uint64 word0, uint64 word1 )
    {
    if ( !fFirstPass )
	{
	if ( fCodeWordCount+2 > fCodeAllocationWordCount )
	    {
	    TError ret = SetCodeAllocation( fCodeWordCount+2+gkCodeArrayIncrement );
	    if ( !ret.Ok() )
		return ret;
	    }
	fCode[fCodeWordCount++] = word0;
	fCode[fCodeWordCount++] = word1;
	}
    fIP += 2;
    return TError();
    }

MLUCSimpleStackMachineAssembler::TError MLUCSimpleStackMachineAssembler::AddWords( uint64 word0, uint64 word1, uint64 word2 )
    {
    if ( !fFirstPass )
	{
	if ( fCodeWordCount+3 > fCodeAllocationWordCount )
	    {
	    TError ret = SetCodeAllocation( fCodeWordCount+3+gkCodeArrayIncrement );
	    if ( !ret.Ok() )
		return ret;
	    }
	fCode[fCodeWordCount++] = word0;
	fCode[fCodeWordCount++] = word1;
	fCode[fCodeWordCount++] = word2;
	}
    fIP += 3;
    return TError();
    }


MLUCSimpleStackMachineAssembler::TError MLUCSimpleStackMachineAssembler::Assemble( std::vector<MLUCString>& tokens )
    {
    if ( tokens.size()<=0 )
	return TError();
    if ( tokens[0]=="NOP" )
	{
	return AddWord( MLUCSimpleStackMachine::kNOP );
	}
    else if ( tokens[0]=="HALT" )
	{
	return AddWord( MLUCSimpleStackMachine::kHALT );
	}
    else if ( tokens[0]=="LNOT" )
	{
	return AddWord( MLUCSimpleStackMachine::kLNOT );
	}
    else if ( tokens[0]=="LAND" )
	{
	return AddWord( MLUCSimpleStackMachine::kLAND );
	}
    else if ( tokens[0]=="LOR" )
	{
	return AddWord( MLUCSimpleStackMachine::kLOR );
	}
    else if ( tokens[0]=="LXOR" )
	{
	return AddWord( MLUCSimpleStackMachine::kLXOR );
	}
    else if ( tokens[0]=="BNOT" )
	{
	return AddWord( MLUCSimpleStackMachine::kBNOT );
	}
    else if ( tokens[0]=="BAND" )
	{
	return AddWord( MLUCSimpleStackMachine::kBAND );
	}
    else if ( tokens[0]=="BOR" )
	{
	return AddWord( MLUCSimpleStackMachine::kBOR );
	}
    else if ( tokens[0]=="BXOR" )
	{
	return AddWord( MLUCSimpleStackMachine::kBXOR );
	}
    else if ( tokens[0]=="BSHIFTLEFT" )
	{
	return AddWord( MLUCSimpleStackMachine::kBSHIFTLEFT );
	}
    else if ( tokens[0]=="BSHIFTRIGHT" )
	{
	return AddWord( MLUCSimpleStackMachine::kBSHIFTRIGHT );
	}
    else if ( tokens[0]=="ADD" )
	{
	return AddWord( MLUCSimpleStackMachine::kADD );
	}
    else if ( tokens[0]=="SUB" )
	{
	return AddWord( MLUCSimpleStackMachine::kSUB );
	}
    else if ( tokens[0]=="MULT" )
	{
	return AddWord( MLUCSimpleStackMachine::kMULT );
	}
    else if ( tokens[0]=="DIV" )
	{
	return AddWord( MLUCSimpleStackMachine::kDIV );
	}
    else if ( tokens[0]=="MOD" )
	{
	return AddWord( MLUCSimpleStackMachine::kMOD );
	}
    else if ( tokens[0]=="INVERSE" )
	{
	return AddWord( MLUCSimpleStackMachine::kINVERSE );
	}
    else if ( tokens[0]=="EQUAL" )
	{
	return AddWord( MLUCSimpleStackMachine::kEQUAL );
	}
    else if ( tokens[0]=="LESS" )
	{
	return AddWord( MLUCSimpleStackMachine::kLESS );
	}
    else if ( tokens[0]=="GREATER" )
	{
	return AddWord( MLUCSimpleStackMachine::kGREATER );
	}
    else if ( tokens[0]=="ISNULL" )
	{
	return AddWord( MLUCSimpleStackMachine::kISNULL );
	}
    else if ( tokens[0]=="NEGATIVE" )
	{
	return AddWord( MLUCSimpleStackMachine::kNEGATIVE );
	}
    else if ( tokens[0]=="POSITIVE" )
	{
	return AddWord( MLUCSimpleStackMachine::kPOSITIVE );
	}
    else if ( tokens[0]=="JUMP" )
	{
	uint64 jumpAddr=0;
	if ( !fFirstPass )
	    {
	    std::map<MLUCString,unsigned long>::iterator thisLabel = fLabels.find( tokens[1] );
	    if ( thisLabel==fLabels.end() )
		return TError( TError::kInvalidJumpTargetLabel, "Jump target label not found" );
	    jumpAddr = fLabels[tokens[1]];
	    }
	return AddWords( MLUCSimpleStackMachine::kJUMP, jumpAddr );
	}
    else if ( tokens[0]=="JUMPNULL" )
	{
	uint64 jumpAddr=0;
	if ( !fFirstPass )
	    {
	    std::map<MLUCString,unsigned long>::iterator thisLabel = fLabels.find( tokens[1] );
	    if ( thisLabel==fLabels.end() )
		return TError( TError::kInvalidJumpTargetLabel, "Jump target label not found" );
	    jumpAddr = fLabels[tokens[1]];
	    }
	return AddWords( MLUCSimpleStackMachine::kJUMPNULL, jumpAddr );
	}
    else if ( tokens[0]=="JUMPNOTNULL" )
	{
	uint64 jumpAddr=0;
	if ( !fFirstPass )
	    {
	    std::map<MLUCString,unsigned long>::iterator thisLabel = fLabels.find( tokens[1] );
	    if ( thisLabel==fLabels.end() )
		return TError( TError::kInvalidJumpTargetLabel, "Jump target label not found" );
	    jumpAddr = fLabels[tokens[1]];
	    }
	return AddWords( MLUCSimpleStackMachine::kJUMPNOTNULL, jumpAddr );
	}
    else if ( tokens[0]=="DROP" )
	{
	return AddWord( MLUCSimpleStackMachine::kDROP );
	}
    else if ( tokens[0]=="CLEAR" )
	{
	return AddWord( MLUCSimpleStackMachine::kCLEAR );
	}
    else if ( tokens[0]=="SWAP" )
	{
	return AddWord( MLUCSimpleStackMachine::kSWAP );
	}
    else if ( tokens[0]=="SWAPNR" )
	{
	return AddWord( MLUCSimpleStackMachine::kSWAPNR );
	}
    else if ( tokens[0]=="MOVETOTOPNR" )
	{
	return AddWord( MLUCSimpleStackMachine::kMOVETOTOPNR );
	}
    else if ( tokens[0]=="PUSHCONST" )
	{
	uint64 constant;
	TError ret = GetParameter( tokens[1], constant );
	if ( !ret.Ok() )
	    return ret;
	return AddWords( MLUCSimpleStackMachine::kPUSHCONST, constant );
	}
    else if ( tokens[0]=="DUP" )
	{
	return AddWord( MLUCSimpleStackMachine::kDUP );
	}
    else
	return TError( TError::kUknonwnMnemonic, "Unknown mnemonic" );
    return TError();
    }


MLUCSimpleStackMachineAssembler::TError MLUCSimpleStackMachineAssembler::GetParameter( MLUCString const& token, uint64& constant )
    {
    std::map<MLUCString,uint64>::iterator thisSymbol = fSymbols.find( token );
    if ( thisSymbol==fSymbols.end() )
	{
	char* cpErr;
	constant = strtoul( token.c_str(), &cpErr, 0 );
	if ( *cpErr != '\0' )
	    return TError( TError::kUInt64ConstantParseError, "Parse error for uint64 constant or unknown symbol" );
	}
    else
	constant = fSymbols[token];
    return TError();
    }

void MLUCSimpleStackMachineAssembler::StripEmptyTokens( std::vector<MLUCString>& tokens )
    {
    std::vector<MLUCString>::iterator iter, end;
    bool found=true;
    while ( found )
	{
	found=false;
	iter = tokens.begin();
	end = tokens.end();
	while ( iter != end )
	    {
	    if ( iter->IsEmpty() || iter->IsWhiteSpace() )
		{
		tokens.erase( iter );
		found=true;
		break;
		}
	    iter++;
	    }
	}
    }


MLUCSimpleStackMachineAssembler::TError MLUCSimpleStackMachineAssembler::SetCodeAllocation( unsigned long newSize )
    {
    if ( newSize < fCodeWordCount && newSize > 0 )
	return TError( TError::TCodeAllocationError, "Not allowed to allocate smaller amount of memory than present (except for 0)" );
    uint64 *newCode = NULL;
    if ( newSize>0 )
	{
	newCode = new uint64[ newSize ];
	if ( !newCode )
	    return TError( TError::kOutOfMemory, "Out of memory" );
	}

    if ( newCode && fCode )
	memcpy( newCode, fCode, sizeof(uint64)*fCodeWordCount );
    else
	fCodeWordCount = 0;
	
    fCodeAllocationWordCount = newSize;
    if ( fCode )
	delete [] fCode;
    fCode = newCode;
    return TError();
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
