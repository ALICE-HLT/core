
#if 0
#define DEBUG
#endif

#include "MLUCTCPSocketInterface.hpp"
#include "MLUCMutex.hpp"
#include <cerrno>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#ifdef DEBUG
#include <iostream>
#endif
#include <cstdio>
#include <cstring>
#include <sys/time.h>
#include <time.h>

template<typename TMutexType,typename TMutexLockerType>
MLUCTCPSocketInterface<TMutexType,TMutexLockerType>::MLUCTCPSocketInterface():
    fQuitVal( true ),
    fQuit( &fQuitVal ),
    fQuitted( true ),
    fSelectSleepInterval_us( 500000 ),
    fListenSocket( -1 ),
    fBackLog( 5 ),
    fInListenerLoop(false)
    {
    }

template<typename TMutexType,typename TMutexLockerType>
MLUCTCPSocketInterface<TMutexType,TMutexLockerType>::~MLUCTCPSocketInterface()
    {
    while ( !fSockets.empty()>0 )
	{
	if ( fSockets.begin()->fSocket!=-1 )
	    close( fSockets.begin()->fSocket );
	fSockets.erase( fSockets.begin() );
	}
    if (fListenSocket !=-1 )
	close( fListenSocket );
    }

template<typename TMutexType,typename TMutexLockerType>
int MLUCTCPSocketInterface<TMutexType,TMutexLockerType>::Bind( unsigned short port )
    {
    struct protoent* proto;
    proto = getprotobyname( "tcp" );
    if ( !proto )
	{
	return ECANCELED;
	}

    int ret, one=1;
    fListenSocket = socket( AF_INET, SOCK_STREAM, proto->p_proto );
    if ( fListenSocket == -1 )
	{
	return errno;
	}
    
    ret = setsockopt( fListenSocket, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one) );
    int socketSize = 1024*1024;
    ret = setsockopt( fListenSocket, SOL_SOCKET, SO_SNDBUF, (char *)&socketSize, sizeof(int));
    struct sockaddr_in sock_addr;
    sock_addr.sin_family = AF_INET;
    sock_addr.sin_port = htons( port );
    sock_addr.sin_addr.s_addr = htonl( INADDR_ANY );
    memset(&(sock_addr.sin_zero), '\0', 8);
    ret = bind( fListenSocket,  (const sockaddr*)&sock_addr, sizeof(struct sockaddr) );
    if ( ret==-1 )
	{
	ret = errno;
	close( fListenSocket );
	fListenSocket = -1;
	return errno;
	}

    ret = listen( fListenSocket, fBackLog );
    if ( ret == -1 )
	{
	ret = errno;
	close( fListenSocket );
	fListenSocket = -1;
	return ret;
	}
    return 0;
    }

template<typename TMutexType,typename TMutexLockerType>
int MLUCTCPSocketInterface<TMutexType,TMutexLockerType>::Connect( char const* host, short const port, int& newSock )
    {
    newSock = -1;
    struct protoent* proto;
    proto = getprotobyname( "tcp" );
    if ( !proto )
	return errno;
    int newSocket = socket( AF_INET, SOCK_STREAM, proto->p_proto );
    if ( newSocket == -1 )
	return errno;
    int ret;
    int one=1;
    ret = setsockopt( newSocket, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one) );
    struct timeval newTimeout;
    newTimeout.tv_sec = 0;
    newTimeout.tv_usec = 0;
    setsockopt( newSocket, SOL_SOCKET, SO_SNDTIMEO, &newTimeout, sizeof(newTimeout) );// XXX return value ignored, will try to work without this setting
    newTimeout.tv_sec = 0;
    newTimeout.tv_usec = 0;
    setsockopt( newSocket, SOL_SOCKET, SO_RCVTIMEO, &newTimeout, sizeof(newTimeout) );// XXX return value ignored, will try to work without this setting

    struct hostent* he;
    unsigned ip = (unsigned)-1;
    he = gethostbyname( host );
    if ( he == NULL )
	{
	close( newSocket );
	return EINVAL;
	}
    ip = (unsigned)ntohl( ((struct in_addr*)he->h_addr_list[0])->s_addr );
    if ( ip == (unsigned)-1 )
	{
	close( newSocket );
	return EINVAL;
	}
    struct sockaddr_in dest_addr;
    dest_addr.sin_family = AF_INET;
    dest_addr.sin_port = htons(port);
    dest_addr.sin_addr.s_addr = htonl( (unsigned long)ip );
    memset(&(dest_addr.sin_zero), '\0', 8);
    do
	{
	ret = connect( newSocket, (struct sockaddr *)&dest_addr, sizeof(struct sockaddr) );
	if ( ret==-1 && errno!=EAGAIN )
	    {
	    ret = errno;
	    close( newSocket );
	    return ret;
	    }
	}
    while ( ret==-1 && errno==EAGAIN );
    ret = fcntl( newSocket, F_GETFL );
    if ( ret!=-1 )
	{
	if ( !(ret & O_NONBLOCK) )
	    {
	    ret = ret | O_NONBLOCK;
	    fcntl( newSocket, F_SETFL, ret ); // XXX return value ignored, will try to work without this setting
	    }
	}
    MLUCTCPSocketBaseInterface::TSocketData sd( newSocket );
    TMutexLockerType locker( fSocketMutex );
    fSockets.push_back( sd );
    locker.Unlock();
    newSock = newSocket;
    return 0;
    }
	
template<typename TMutexType,typename TMutexLockerType>
int MLUCTCPSocketInterface<TMutexType,TMutexLockerType>::Disconnect( int sock )
    {
    std::vector<MLUCTCPSocketBaseInterface::TSocketData>::iterator sock_iter;
    TMutexLockerType locker( fSocketMutex );
    if ( FindSocket( sock, sock_iter ) )
	{
	close( sock_iter->fSocket );
	fSockets.erase( sock_iter );
	return 0;
	}
    return EINVAL;
    }


template<typename TMutexType,typename TMutexLockerType>
int MLUCTCPSocketInterface<TMutexType,TMutexLockerType>::AddListener( MLUCTCPSocketListener* listener )
    {
    return AddListener( listener, true );
    }

template<typename TMutexType,typename TMutexLockerType>
int MLUCTCPSocketInterface<TMutexType,TMutexLockerType>::DelListener( MLUCTCPSocketListener* listener )
    {
    return DelListener( listener, true );
    }

template<typename TMutexType,typename TMutexLockerType>
int MLUCTCPSocketInterface<TMutexType,TMutexLockerType>::RequestRead( int sock, bool req )
    {
    std::vector<MLUCTCPSocketBaseInterface::TSocketData>::iterator sock_iter;
    TMutexLockerType locker( fSocketMutex );
    if ( FindSocket( sock, sock_iter ) )
	{
	if ( req )
	    sock_iter->fReadRequest++;
	else
	    sock_iter->fReadRequest--;
	return 0;
	}
    return EINVAL;
    }

template<typename TMutexType,typename TMutexLockerType>
int MLUCTCPSocketInterface<TMutexType,TMutexLockerType>::RequestWrite( int sock, bool req )
    {
    std::vector<MLUCTCPSocketBaseInterface::TSocketData>::iterator sock_iter;
    TMutexLockerType locker( fSocketMutex );
    if ( FindSocket( sock, sock_iter ) )
	{
	if ( req )
	    sock_iter->fWriteRequest++;
	else
	    sock_iter->fWriteRequest--;
	return 0;
	}
    return EINVAL;
    }


template<typename TMutexType,typename TMutexLockerType>
int MLUCTCPSocketInterface<TMutexType,TMutexLockerType>::Read( int sock, uint8* data, const unsigned long sz )
    {
    TMutexLockerType locker( fSocketMutex );
    if ( !FindSocket( sock ) )
	{
	errno = EINVAL;
	return -1;
	}
    locker.Unlock();
    unsigned long readBytes=0;
    int ret;
    do
	{
	errno = 0;
	ret = read( sock, data+readBytes, sz-readBytes );
#if 0
#ifdef DEBUG
	std::cout << "Network::Read ret: " << std::dec << ret << std::endl;
#endif
#endif
	if ( ret<0 && errno!=EAGAIN && errno!=EINTR )
	    return ret;
	else if ( ret==0 )
	    {
	    errno = ECONNABORTED;
	    return -1;
	    }
	else if ( ret>0 )
	    {
#if 0
#ifdef DEBUG
		{
		char tmpChar[ ret+1 ];
		memset( tmpChar, 0, ret+1 );
		strncpy( tmpChar, reinterpret_cast<const char*>( data+readBytes ), ret );
		tmpChar[ret]='\0';
		std::cout << "Network::Read (RAW) '" << tmpChar << "' ("
			  << ret << " bytes) (socket " << sock << ")." << std::endl;
		for ( unsigned ij=0; ij<ret; ij++ )
		    {
		    std::cout << "Network::Read (RAW/Char) '" << tmpChar[ij] << "' ("
			      << (int)tmpChar[ij] << ")." << std::endl;
		    }
		}
#endif
#endif
	    readBytes += ret;
	    if ( readBytes>=sz )
		break;
	    }
	fd_set read_sockets;
	do
	    {
	    FD_ZERO( &read_sockets);
	    FD_SET( sock, &read_sockets );
	    struct timeval tv;
	    tv.tv_sec = 0;
	    tv.tv_usec = 0;
	    ret = select( sock+1, &read_sockets, NULL, NULL, &tv );
	    }
	while ( ret<0 && (errno==EAGAIN || errno==EINTR) );
	if ( ret==0 )
	    return readBytes;
	if ( ret<0 )
	    return ret;
	}
    while ( readBytes<sz );
    return readBytes;
    }

template<typename TMutexType,typename TMutexLockerType>
int MLUCTCPSocketInterface<TMutexType,TMutexLockerType>::Write( int sock, uint8 const* data, const unsigned long sz )
    {
    TMutexLockerType locker( fSocketMutex );
    if ( !FindSocket( sock ) )
	{
	errno = EINVAL;
	return -1;
	}
    locker.Unlock();
#if 0
#ifdef DEBUG
    for ( unsigned long ij=0; ij<sz; ij++ )
	{
	if ( data[ij]=='\0' )
	    std::cout << "WARNING: Attempt to write char 0 in data (pos " << std::dec << ij << ")." << std::endl;
	}
#endif
#endif
    unsigned long writeBytes=0;
    int ret;
    do
	{
	errno = 0;
	ret = write( sock, data+writeBytes, sz-writeBytes );
#if 0
#ifdef DEBUG
	std::cout << "Network::Write ret: " << std::dec << ret << std::endl;
#endif
#endif
	if ( ret<0 && errno!=EAGAIN && errno!=EINTR )
	    {
#ifdef DEBUG
	    std::cerr << strerror(errno) << " (" << errno << ")." << std::endl;
#endif
	    return ret;
	    }
	else if ( ret>0 )
	    {
#if 0
#ifdef DEBUG
		{
		char tmpChar[ ret+1 ];
		memset( tmpChar, 0, ret+1 );
		strncpy( tmpChar, reinterpret_cast<const char*>( data+writeBytes ), ret );
		tmpChar[ret]='\0';
		std::cout << "Network::Write (RAW) '" << tmpChar << "' ("
			  << ret << " bytes) (socket " << sock << ")." << std::endl;
		}
#endif
#endif
	    writeBytes += ret;
	    if ( writeBytes>=sz )
		break;
	    }
	fd_set write_sockets;
	do
	    {
	    FD_ZERO( &write_sockets);
	    FD_SET( sock, &write_sockets );
	    struct timeval tv;
	    tv.tv_sec = 0;
	    tv.tv_usec = 0;
	    ret = select( sock+1, NULL, &write_sockets, NULL, &tv );
	    }
	while ( ret<0 && (errno==EAGAIN || errno==EINTR) );
	if ( ret==0 )
	    return writeBytes;
	if ( ret<0 )
	    return ret;
	}
    while ( writeBytes<sz );
    return writeBytes;
    }

template<typename TMutexType,typename TMutexLockerType>
int MLUCTCPSocketInterface<TMutexType,TMutexLockerType>::NetworkLoop()
    {
    fQuitted = false;
    *fQuit = false;
    TMutexLockerType socketLocker( fSocketMutex, false );
    TMutexLockerType listenerLocker( fListenerMutex, false );
    TMutexLockerType removeListenerLocker( fRemoveListenerMutex, false );
    while ( !(*fQuit) )
	{
	fd_set read_sockets, write_sockets, err_sockets;
	FD_ZERO( &err_sockets);
	FD_ZERO( &read_sockets);
	FD_ZERO( &write_sockets);
	if ( fListenSocket!=-1 )
	    {
	    FD_SET( fListenSocket, &read_sockets );
	    FD_SET( fListenSocket, &err_sockets );
	    }
	int highest_sock = fListenSocket;
	std::vector<MLUCTCPSocketBaseInterface::TSocketData>::iterator sock_iter, sock_end;
	socketLocker.Lock();
	sock_iter = fSockets.begin();
	sock_end = fSockets.end();
	while ( sock_iter != sock_end )
	    {
	    FD_SET( sock_iter->fSocket, &err_sockets );
	    if ( sock_iter->fReadRequest )
		FD_SET( sock_iter->fSocket, &read_sockets );
	    if ( sock_iter->fWriteRequest )
		FD_SET( sock_iter->fSocket, &write_sockets );
	    if ( sock_iter->fSocket > highest_sock )
		highest_sock = sock_iter->fSocket;
	    sock_iter++;
	    }
	socketLocker.Unlock();

	struct timeval tv;
	tv.tv_sec = fSelectSleepInterval_us / 1000000;
	tv.tv_usec = fSelectSleepInterval_us % 1000000;
	printf( "TRACE %s:%d\n", __FILE__, __LINE__ );
	int ret = select( highest_sock+1, &read_sockets, &write_sockets, &err_sockets, &tv );
	printf( "TRACE %s:%d\n", __FILE__, __LINE__ );
	if ( *fQuit )
	    break;
	if ( ( ret==-1 && (errno==EAGAIN || errno==EINTR) ) || ret==0 )
	    continue;
	if ( ret<0 )
	    return errno;

	std::vector<MLUCTCPSocketListener*>::iterator listener_iter, listener_end, remove_listener_iter, remove_listener_end;

	listenerLocker.Lock();
	fInListenerLoop = true;
	listener_iter = fListeners.begin();
	listener_end = fListeners.end();
	while ( listener_iter != listener_end )
	    {
	    bool found=false;
	    removeListenerLocker.Lock();
	    remove_listener_iter = fRemoveListeners.begin();
	    remove_listener_end = fRemoveListeners.end();
	    while ( remove_listener_iter != remove_listener_end )
		{
		if ( *remove_listener_iter==*listener_iter )
		    {
		    found=true;
		    break;
		    }
		remove_listener_iter++;
		}
	    removeListenerLocker.Unlock();
	    (*listener_iter)->Period( *this );
	    listener_iter++;
	    }
	fInListenerLoop = false;
	removeListenerLocker.Lock();
	remove_listener_iter = fRemoveListeners.begin();
	remove_listener_end = fRemoveListeners.end();
	while ( remove_listener_iter != remove_listener_end )
	    {
	    DelListener( *remove_listener_iter, false );
	    remove_listener_iter++;
	    }
	fRemoveListeners.clear();
	removeListenerLocker.Unlock();
	listenerLocker.Unlock();

	if ( fListenSocket!=-1 && FD_ISSET( fListenSocket, &err_sockets ) )
	    {
	    // Listen socket signals error, we close up and leave loop.
	    socketLocker.Lock();
	    listenerLocker.Lock();
	    sock_iter = fSockets.begin();
	    sock_end = fSockets.end();
	    while ( sock_iter != sock_end )
		{
		if ( FD_ISSET( sock_iter->fSocket, &err_sockets ) )
		    {
		    close( sock_iter->fSocket );
		    listener_iter = fListeners.begin();
		    listener_end = fListeners.end();
		    fInListenerLoop = true;
		    while ( listener_iter != listener_end )
			{
			bool found=false;
			removeListenerLocker.Lock();
			remove_listener_iter = fRemoveListeners.begin();
			remove_listener_end = fRemoveListeners.end();
			while ( remove_listener_iter != remove_listener_end )
			    {
			    if ( *remove_listener_iter==*listener_iter )
				{
				found=true;
				break;
				}
			    remove_listener_iter++;
			    }
			removeListenerLocker.Unlock();
			if ( !found )
			    (*listener_iter)->Closed( *this, sock_iter->fSocket );
			listener_iter++;
			}
		    fInListenerLoop = false;
		    removeListenerLocker.Lock();
		    remove_listener_iter = fRemoveListeners.begin();
		    remove_listener_end = fRemoveListeners.end();
		    while ( remove_listener_iter != remove_listener_end )
			{
			DelListener( *remove_listener_iter, false );
			remove_listener_iter++;
			}
		    fRemoveListeners.clear();
		    removeListenerLocker.Unlock();
		    }
		sock_iter++;
		}
	    listenerLocker.Unlock();
	    socketLocker.Unlock();
	    close( fListenSocket );
	    return ENXIO;
	    }
	if ( fListenSocket!=-1 && FD_ISSET( fListenSocket, &read_sockets ) )
	    {
	    //New connection
	    struct sockaddr_in incomingAddr;
	    socklen_t incomingAddrLen = sizeof(incomingAddr);
	    int newSocket = accept( fListenSocket, (struct sockaddr*)&incomingAddr, &incomingAddrLen );
	    int one=1;
	    ret = setsockopt( newSocket, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one) );
	    ret = fcntl( newSocket, F_GETFL );
	    if ( ret!=-1 )
		{
		if ( !(ret & O_NONBLOCK) )
		    {
		    ret = ret | O_NONBLOCK;
		    fcntl( newSocket, F_SETFL, ret ); // XXX return value ignored, will try to work without this setting
		    }
		}
	    struct timeval newTimeout;
	    newTimeout.tv_sec = 0;
	    newTimeout.tv_usec = 0;
	    setsockopt( newSocket, SOL_SOCKET, SO_SNDTIMEO, &newTimeout, sizeof(newTimeout) );// XXX return value ignored, will try to work without this setting
	    newTimeout.tv_sec = 0;
	    newTimeout.tv_usec = 0;
	    setsockopt( newSocket, SOL_SOCKET, SO_RCVTIMEO, &newTimeout, sizeof(newTimeout) );// XXX return value ignored, will try to work without this setting
	    fInListenerLoop = true;
	    bool reject = false;
	    MLUCTCPSocketBaseInterface::TSocketData sd ( newSocket );
	    socketLocker.Lock();
	    fSockets.push_back( sd );
	    socketLocker.Unlock();
	    listenerLocker.Lock();
	    listener_iter = fListeners.begin();
	    listener_end = fListeners.end();
	    while ( listener_iter != listener_end )
		{
		bool found=false;
		removeListenerLocker.Lock();
		remove_listener_iter = fRemoveListeners.begin();
		remove_listener_end = fRemoveListeners.end();
		while ( remove_listener_iter != remove_listener_end )
		    {
		    if ( *remove_listener_iter==*listener_iter )
			{
			found=true;
			break;
			}
		    remove_listener_iter++;
		    }
		removeListenerLocker.Unlock();
		if ( !found && (*listener_iter)->NewConnection( *this, newSocket, &incomingAddr, incomingAddrLen ) )
		    {
		    reject = true;
		    break;
		    }
		listener_iter++;
		}
	    fInListenerLoop = false;
	    removeListenerLocker.Lock();
	    remove_listener_iter = fRemoveListeners.begin();
	    remove_listener_end = fRemoveListeners.end();
	    while ( remove_listener_iter != remove_listener_end )
		{
		DelListener( *remove_listener_iter, false );
		remove_listener_iter++;
		}
	    fRemoveListeners.clear();
	    removeListenerLocker.Unlock();
	    listenerLocker.Unlock();
	    socketLocker.Lock();
	    if ( reject && FindSocket( newSocket, sock_iter ) )
		{
		fSockets.erase( sock_iter );
		close( newSocket );
		}
	    socketLocker.Unlock();
	    }
	// First remove error sockets
	bool found=false;
	do
	    {
	    found=false;
	    socketLocker.Lock();
	    sock_iter = fSockets.begin();
	    sock_end = fSockets.end();
	    while ( sock_iter != sock_end )
		{
		if ( FD_ISSET( sock_iter->fSocket, &err_sockets ) )
		    {
		    close( sock_iter->fSocket );
		    found=true;
		    break;
		    }
		sock_iter++;
		}
	    if ( found )
		{
		fSockets.erase( sock_iter );
		socketLocker.Unlock();
		fInListenerLoop = true;
		listenerLocker.Lock();
		listener_iter = fListeners.begin();
		listener_end = fListeners.end();
		while ( listener_iter != listener_end )
		    {
		    bool found=false;
		    removeListenerLocker.Lock();
		    remove_listener_iter = fRemoveListeners.begin();
		    remove_listener_end = fRemoveListeners.end();
		    while ( remove_listener_iter != remove_listener_end )
			{
			if ( *remove_listener_iter==*listener_iter )
			    {
			    found=true;
			    break;
			    }
			remove_listener_iter++;
			}
		    removeListenerLocker.Unlock();
		    if ( !found )
			(*listener_iter)->Closed( *this, sock_iter->fSocket );
		    listener_iter++;
		    }
		fInListenerLoop = false;
		removeListenerLocker.Lock();
		remove_listener_iter = fRemoveListeners.begin();
		remove_listener_end = fRemoveListeners.end();
		while ( remove_listener_iter != remove_listener_end )
		    {
		    DelListener( *remove_listener_iter, false );
		    remove_listener_iter++;
		    }
		fRemoveListeners.clear();
		removeListenerLocker.Unlock();
		listenerLocker.Unlock();
		}
	    else
		socketLocker.Unlock();

	    }
	while ( found );
	socketLocker.Lock();
	listenerLocker.Lock();
	sock_iter = fSockets.begin();
	sock_end = fSockets.end();
	std::vector<MLUCTCPSocketBaseInterface::TSocketData> closeSockets;
	while ( sock_iter != sock_end )
	    {
	    bool doClose=false;
	    if ( sock_iter->fReadRequest && FD_ISSET( sock_iter->fSocket, &read_sockets ) )
		{
		fInListenerLoop = true;
		listener_iter = fListeners.begin();
		listener_end = fListeners.end();
		while ( listener_iter != listener_end )
		    {
		    bool found=false;
		    removeListenerLocker.Lock();
		    remove_listener_iter = fRemoveListeners.begin();
		    remove_listener_end = fRemoveListeners.end();
		    while ( remove_listener_iter != remove_listener_end )
			{
			if ( *remove_listener_iter==*listener_iter )
			    {
			    found=true;
			    break;
			    }
			remove_listener_iter++;
			}
		    removeListenerLocker.Unlock();
		    if ( !found && (*listener_iter)->Readable( *this, sock_iter->fSocket ) )
			{
			closeSockets.push_back( sock_iter->fSocket );
			doClose=true;
			break;
			}
		    listener_iter++;
		    }
		fInListenerLoop = false;
		removeListenerLocker.Lock();
		remove_listener_iter = fRemoveListeners.begin();
		remove_listener_end = fRemoveListeners.end();
		while ( remove_listener_iter != remove_listener_end )
		    {
		    DelListener( *remove_listener_iter, false );
		    remove_listener_iter++;
		    }
		fRemoveListeners.clear();
		removeListenerLocker.Unlock();
		}
	    if ( !doClose && sock_iter->fWriteRequest && FD_ISSET( sock_iter->fSocket, &write_sockets ) )
		{
		fInListenerLoop = true;
		listener_iter = fListeners.begin();
		listener_end = fListeners.end();
		while ( listener_iter != listener_end )
		    {
		    bool found=false;
		    removeListenerLocker.Lock();
		    remove_listener_iter = fRemoveListeners.begin();
		    remove_listener_end = fRemoveListeners.end();
		    while ( remove_listener_iter != remove_listener_end )
			{
			if ( *remove_listener_iter==*listener_iter )
			    {
			    found=true;
			    break;
			    }
			remove_listener_iter++;
			}
		    removeListenerLocker.Unlock();
		    if ( !found && (*listener_iter)->Writeable( *this, sock_iter->fSocket ) )
			{
			closeSockets.push_back( sock_iter->fSocket );
			doClose=true;
			break;
			}
		    listener_iter++;
		    }
		fInListenerLoop = false;
		removeListenerLocker.Lock();
		remove_listener_iter = fRemoveListeners.begin();
		remove_listener_end = fRemoveListeners.end();
		while ( remove_listener_iter != remove_listener_end )
		    {
		    DelListener( *remove_listener_iter, false );
		    remove_listener_iter++;
		    }
		fRemoveListeners.clear();
		removeListenerLocker.Unlock();
		}
	    sock_iter++;
	    }
	listenerLocker.Unlock();
	socketLocker.Unlock();
	while ( closeSockets.size()>0 )
	    {
	    socketLocker.Lock();
	    listenerLocker.Lock();
	    if ( FindSocket( closeSockets.begin()->fSocket, sock_iter ) )
		{
		close( sock_iter->fSocket );
		fSockets.erase( sock_iter );
		fInListenerLoop = true;
		listener_iter = fListeners.begin();
		listener_end = fListeners.end();
		while ( listener_iter != listener_end )
		    {
		    bool found=false;
		    removeListenerLocker.Lock();
		    remove_listener_iter = fRemoveListeners.begin();
		    remove_listener_end = fRemoveListeners.end();
		    while ( remove_listener_iter != remove_listener_end )
			{
			if ( *remove_listener_iter==*listener_iter )
			    {
			    found=true;
			    break;
			    }
			remove_listener_iter++;
			}
		    removeListenerLocker.Unlock();
		    if ( !found )
			(*listener_iter)->Closed( *this, sock_iter->fSocket );
		    listener_iter++;
		    }
		fInListenerLoop = false;
		removeListenerLocker.Lock();
		remove_listener_iter = fRemoveListeners.begin();
		remove_listener_end = fRemoveListeners.end();
		while ( remove_listener_iter != remove_listener_end )
		    {
		    DelListener( *remove_listener_iter, false );
		    remove_listener_iter++;
		    }
		fRemoveListeners.clear();
		removeListenerLocker.Unlock();
		}
	    listenerLocker.Unlock();
	    socketLocker.Unlock();
	    closeSockets.erase( closeSockets.begin() );
	    }
	}
    fQuitted = true;
    return 0;
    }

template<typename TMutexType,typename TMutexLockerType>
bool MLUCTCPSocketInterface<TMutexType,TMutexLockerType>::QuitNetworkLoop( bool wait )
    {
    *fQuit = true;
    struct timeval start, now;
    unsigned long long deltaT = 0;
    const unsigned long long timeLimit = 10000000;
    gettimeofday( &start, NULL );
    while ( wait && !fQuitted && deltaT<timeLimit )
	{
	usleep(0);
	gettimeofday( &now, NULL );
	deltaT = ((unsigned long long)(now.tv_sec - start.tv_sec))*1000000ULL + ((unsigned long long)(now.tv_usec - start.tv_usec));
	}
    return fQuitted;
    }

template<typename TMutexType,typename TMutexLockerType>
void MLUCTCPSocketInterface<TMutexType,TMutexLockerType>::SetQuitVariable( bool* quit )
    {
    if ( !quit )
	return;
    fQuit = quit;
    }

template<typename TMutexType,typename TMutexLockerType>
bool MLUCTCPSocketInterface<TMutexType,TMutexLockerType>::FindSocket( int sock, std::vector<MLUCTCPSocketBaseInterface::TSocketData>::iterator& iter )
    {
    std::vector<MLUCTCPSocketBaseInterface::TSocketData>::iterator sock_iter, sock_end;
    sock_iter = fSockets.begin();
    sock_end = fSockets.end();
    while ( sock_iter != sock_end )
	{
	if ( sock_iter->fSocket == sock )
	    {
	    iter = sock_iter;
	    return true;
	    }
	sock_iter++;
	}
    return false;
    }

template<typename TMutexType,typename TMutexLockerType>
int MLUCTCPSocketInterface<TMutexType,TMutexLockerType>::AddListener( MLUCTCPSocketListener* listener, bool lock )
    {
    if ( !listener )
	return EINVAL;
    TMutexLockerType( fListenerMutex, lock );
    fListeners.push_back( listener );
    return 0;
    }

template<typename TMutexType,typename TMutexLockerType>
int MLUCTCPSocketInterface<TMutexType,TMutexLockerType>::DelListener( MLUCTCPSocketListener* listener, bool lock )
    {
    if ( !listener )
	return EINVAL;
    TMutexLockerType( fListenerMutex, lock );
    std::vector<MLUCTCPSocketListener*>::iterator iter, end;
    iter = fListeners.begin();
    end = fListeners.end();
    while ( iter != end )
	{
	if ( *iter == listener )
	    {
	    if ( fInListenerLoop )
		{
		TMutexLockerType( fRemoveListenerMutex, lock );
		fRemoveListeners.push_back( listener );
		}
	    else
		fListeners.erase( iter );
	    return 0;
	    }
	iter++;
	}
    return ENODEV;
    }

static MLUCLockedTCPSocketInterface gDummyLockedTCPSocketInterface;
static MLUCUnlockedTCPSocketInterface gDummyUnlockedTCPSocketInterface;
