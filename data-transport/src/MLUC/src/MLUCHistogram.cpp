/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCHistogram.hpp"
#include <memory.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

MLUCHistogram::MLUCHistogram( double lower, double upper, double width )
    {
    fEntryCnt = 0;
    fOverflows = 0;
    fUnderflows = 0;
    fLower = lower;
    fUpper = upper;
    fBinWidth = width;
    fBinCnt = (uint32)( (fUpper-fLower)/fBinWidth );
    fBins = (uint32*)malloc( sizeof(uint32)*fBinCnt );
    if ( !fBins )
	fBinCnt = 0;
    }

	//MLUCHistogram( const char* filename );

uint32 MLUCHistogram::GetBinCount()
    {
    return fBinCnt;
    }

uint32 MLUCHistogram::GetBinEntryCnt( uint32 ndx )
    {
    if ( ndx<fBinCnt )
	return fBins[ fBinCnt ];
    return ~(uint32)0;
    }

double MLUCHistogram::GetBinLow( uint32 ndx )
    {
    if ( ndx<fBinCnt )
	return ndx*fBinWidth+fLower;
    return 0;
    }

double MLUCHistogram::GetBinHigh( uint32 ndx )
    {
    if ( ndx<fBinCnt )
	return (ndx+1)*fBinWidth+fLower;
    return 0;
    }

uint32 MLUCHistogram::GetOverflows()
    {
    return fOverflows;
    }

uint32 MLUCHistogram::GetUnderflows()
    {
    return fUnderflows;
    }

uint64 MLUCHistogram::GetEntryCnt()
    {
    return fEntryCnt;
    }

// void MLUCHistogram::GetStats( double& mean, double& rms )
//     {
//     uint64 i, n, sum = 0;
//     mean = 0;
//     for ( i = 0; i < fBinCnt; i++ )
// 	{
// 	mean += fBins[i]*(fLower+i*fBinWidth+fBinWidth/2);
// 	}

//     sum = 0;
//     for ( i = 0; i < fBinCnt; i++ )
// 	{
// 	n = (mean-(fLower+i*fBinWidth+fBinWidth/2));
// 	sum += fBins[i]*n*n;
// 	}
//     mean /= fEntryCnt;
//     rms /= fEntryCnt;
//     }

// void MLUCHistogram::GetStats( double& mean, double& rms, double& median )
//     {
//     GetStats( mean, rms );
//     uint64 last = -1, i, sum=0, half = (fEntryCnt >> 2);
//     i = 0;
//     while ( sum < half )
// 	{
// 	sum += fBins[i];
// 	last = i;
// 	i++;
// 	}
//     if ( sum == half )
// 	{
// 	median = fLower+fBinWidth*half;
// 	}
//     else
// 	{
// 	median = fLower+fBinWidth*half+fBinWidth/2;
// 	}
//     }


int MLUCHistogram::Write( const char* filename )
    {
    if ( !fBinCnt )
	return EINVAL;
    int fd, ret;

    fd = open( filename, O_WRONLY|O_CREAT|O_TRUNC, 0666 );
    if ( fd==-1 )
	return errno;
    ret = write( fd, &fUpper, sizeof(fUpper) );
    if ( ret != sizeof(fUpper) )
	{
	close( fd );
	return errno;
	}
    ret = write( fd, &fLower, sizeof(fLower) );
    if ( ret != sizeof(fLower) )
	{
	close( fd );
	return errno;
	}
    ret = write( fd, &fBinWidth, sizeof(fBinWidth) );
    if ( ret != sizeof(fBinWidth) )
	{
	close( fd );
	return errno;
	}
    ret = write( fd, &fOverflows, sizeof(fOverflows) );
    if ( ret != sizeof(fOverflows) )
	{
	close( fd );
	return errno;
	}
    ret = write( fd, &fUnderflows, sizeof(fUnderflows) );
    if ( ret != sizeof(fUnderflows) )
	{
	close( fd );
	return errno;
	}
    ret = write( fd, fBins, sizeof(fBins[0])*fBinCnt );
    if ( ret != (int)(sizeof(fBins[0])*fBinCnt) )
	{
	close( fd );
	return errno;
	}
    close( fd );
    return 0;
    }
	
// int Read( const char* filename );




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
