/************************************************************************
 **
 **
 ** This file is property of and copyright by the Technical Computer
 ** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
 ** University, Heidelberg, Germany, 2001
 ** This file has been written by Timm Morten Steinbeck, 
 ** timm@kip.uni-heidelberg.de
 **
 **
 ** See the file license.txt for details regarding usage, modification,
 ** distribution and warranty.
 ** Important: This file is provided without any warranty, including
 ** fitness for any particular purpose.
 **
 **
 ** Newer versions of this file's package will be made available from 
 ** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
 ** or the corresponding page of the Heidelberg Alice Level 3 group.
 **
 *************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Arne Wiebalck 
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCValueMonitorCR.hpp"
#include <fstream>

MLUCValueMonitorCR::MLUCValueMonitorCR( const char* filename, 
										const char* fileDescrHeader, 
										const char* output_prefix, 
										const char* output_suffix, 
										const char* avg_output_prefix, 
										const char* avg_output_suffix, 
										unsigned int average_items, 
										int cmd) :
	
    MLUCValueMonitor( filename, 
					  fileDescrHeader, 
					  output_prefix, 
					  output_suffix, 
					  avg_output_prefix, 
					  avg_output_suffix, 
					  average_items )
{
    fFirstReadOut   = true;
    fOldBlocks = 0;	
    fNewBlocks = 0;	
	fCmd = cmd;
}

MLUCValueMonitorCR::MLUCValueMonitorCR ( const char* output_prefix, 
										 const char* output_suffix, 
										 const char* avg_output_prefix, 
										 const char* avg_output_suffix, 
										 unsigned int average_items,
										 int cmd) :
	
    MLUCValueMonitor( output_prefix, 
					  output_suffix, 
					  avg_output_prefix, 
					  avg_output_suffix, 
					  average_items )
{
    fFirstReadOut = true;
    fOldBlocks = 0;	
    fNewBlocks = 0;	
	fCmd = cmd;
}

MLUCValueMonitorCR::MLUCValueMonitorCR ( const char* filename, 
										 const char* fileDescrHeader, 
										 unsigned int average_items,
										 int cmd ) : 

    MLUCValueMonitor( filename, 
					  fileDescrHeader, 
					  average_items )
{
    fFirstReadOut = true;
    fOldBlocks = 0;	
    fNewBlocks = 0;	
	fCmd = cmd;
}


MLUCValueMonitorCR::MLUCValueMonitorCR ( unsigned int average_items,
										 int cmd ) : 

    MLUCValueMonitor( average_items )
{
    fFirstReadOut = true;
    fOldBlocks = 0;	
    fNewBlocks = 0;	
	fCmd = cmd;
}


int MLUCValueMonitorCR::GetValue( uint64& measuredValue ) 
{
	
    char   kLine[5000];   
	
    std::ifstream in( "/proc/crstat" );
    if( !in ) 
		{
			LOG( MLUCLog::kError, "MLUCValueMonitorCR:GetValue", "Error opening file" )
				<< "Could not open /proc/crstat" << ENDLOG;
			return ENOENT;
		}
    else
		{
			unsigned long long unused;
			bool  found = false;

			fOldBlocks = fNewBlocks;

			switch( fCmd )
				{
				case 0: // READ
					while( !in.eof() && !found ) 
						{
							in.getline( kLine, 5000 );
							
							if( sscanf( kLine, " READs %llu, done %llu ", &unused, &fNewBlocks ) > 0 ) 
								{
									found = true;
									break;
								}
						}
				case 1: // WRITE
					while( !in.eof() && !found ) 
						{
							in.getline( kLine, 5000 );

							if( sscanf( kLine, " WRITEs %llu, re-issued %llu, done %llu ", &unused, &unused, &fNewBlocks ) > 0 ) 
								{
									found = true;
									break;
								}
						}
				}

			if( fFirstReadOut ) {
				
				fFirstReadOut = false;
				return EINVAL;
			}
	
			measuredValue = fNewBlocks - fOldBlocks;
			return 0;
		}
}


/*
***************************************************************************
**
** $Author$ - Initial Version by Arne Wiebalck 
**
** $Id$ 
**
***************************************************************************
*/
