#include <string>
#include <fstream>
#if __GNUC__ ==2
#include <strstream>
#endif
#include <sstream>
#include <stdio.h>

#include "MLUCValueMonitorInterrupt.hpp"

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

void MLUCValueMonitorInterrupt::Init(const char* intrid, int cpuid)
{
  fIntr  = intrid;
  fIntr  += ":";   
  //  fPattern = intrid + ":";
  fCpuId = cpuid;
  fFirstReadOut = true;
  fNCpus = GetNumberOfCpus();
}

// -----------------------------------------------------------------------------

unsigned MLUCValueMonitorInterrupt::GetNumberOfCpus(void)
{
  int ncpus = 0;
  std::ifstream fin( "/proc/interrupts" );
  if (!fin) { // if file can not be opened 
    return 1; // the number of cpu's is 1 (since one cpu is always there :))
  }

  string line;
  if (!getline(fin, line)) {
    return 1;
  }
  
  unsigned long pos = 0;
  while ((pos = line.find("CPU", pos)) != string::npos) {
    pos += 3;
    ncpus++;
  }

  if (ncpus < 1) { // on unexpected errors
    ncpus = 1;     // set number of cpu's to 1
  }

  return (unsigned) ncpus;
}

// -----------------------------------------------------------------------------

unsigned MLUCValueMonitorInterrupt::GetNumberOfCpus2(void)
{
  int ncpus = -1;
  std::ifstream fin( "/proc/stat" );
  if (!fin) { // if file can not be opened 
    return 1; // the number of cpu's is 1 (since one cpu is always there :))
  }

  string line;
  while (getline(fin, line)) {
    int pos = line.find("cpu");
    if(pos == 0) { // if line starts with "cpu" increment counter
      ncpus++;
    }
  }
  if (ncpus < 1) { // on unexpected errors
    ncpus = 1;     // set number of cpu's to 1
  }

  return (unsigned) ncpus;
}

// -----------------------------------------------------------------------------

int MLUCValueMonitorInterrupt::GetValue( uint64& MeasuredValue )
{
  bool bIntrFound = false;
  bool bReadSuccessful = false;
  uint64 tmp;
  unsigned val;

  if(fCpuId >= (int) fNCpus) {
    LOG( MLUCLog::kError, "MLUCValueMonitorInterrupt:GetValue", 
	 "Cpu ID is wrong" ) << "Cpu ID: " << fCpuId  << " Number of cpu's:" 
			     << fNCpus << ENDLOG;
    return EINVAL;
  }
  
  std::ifstream fin( "/proc/interrupts" );
  if (!fin) {
    LOG( MLUCLog::kError, "MLUCValueMonitorInterrupt:GetValue", 
	 "Error opening file" ) << "Could not open /proc/interrupts" << ENDLOG;
    return ENOENT;
  }

  string line;
  if (fIntr == "all:") {
    val = 0;
    getline(fin, line); // skip first line
    while (getline(fin, line)) {
      string dummy;
#if __GNUC__>=3
      istringstream s(line.c_str());
#else
      istrstream s(line.c_str(), line.size());
#endif
      s >> dummy;    // skip first column.
      
      if (fCpuId == INTR_ALLCPUS) {
	// sum all interrupts from all cup's 
	for (unsigned i = 0; i < fNCpus; i++) { // loop over all cpu's
	  if(s >> tmp) {
	    val += tmp;
	  }
	}
      } else {
	// sum all interrupts from specified cpu
	for (int i = 0; i <= fCpuId; i++) { 
	  if (s >> tmp) {
	    bReadSuccessful = true;
	  } else {
	    bReadSuccessful = false;
	  }
	}
	if (bReadSuccessful) {
	  val += tmp;
	}
      }
    }
  } else { 
    // search for specified interrupt
    bIntrFound = false;
    while (!bIntrFound && getline(fin, line)) {
      string token;
#if __GNUC__>=3
      istringstream s(line.c_str());
#else
      istrstream s(line.c_str(), line.size());
#endif
      // loop over columns in this line
      while(s >> token) {
	if (fIntr.find(token) == 0) { 
	  bIntrFound = true;
	  break;
	}
      }
    }
    
    if(!bIntrFound) {
      LOG( MLUCLog::kError, "MLUCValueMonitorInterrupt:GetValue", 
	   "Wrong interrupt" ) << "no interrupt " << fIntr.c_str()  << ENDLOG;
      return EINVAL;
    }

    string dummy;
#if __GNUC__>=3
    istringstream s(line.c_str());
#else
    istrstream s(line.c_str(), line.size());
#endif
    s >> dummy;    // skip first column.
    if (fCpuId == INTR_ALLCPUS) {
      // sum specified interrupt from all cpu's
      val = 0;
      for (unsigned i = 0; i < fNCpus; i++) {
	if(s >> tmp) {
	  val += tmp;
	}
      }
    } else {
      // get specified interrupt from specified cpu
      for (int i = 0; i <= fCpuId; i++) { 
	if (s >> tmp) {
	  bReadSuccessful = true;
	} else {
	  bReadSuccessful = false;
	}
      }
      // last readout in previous loop should be successful,
      //  otherwise an error has ocurred.
      if(!bReadSuccessful) {
      LOG( MLUCLog::kError, "MLUCValueMonitorInterrupt:GetValue", 
	   "Wrong interrupt" ) << "no interrupt " << fIntr.c_str()  << ENDLOG;
	return EINVAL;
      } 
      val = tmp;
    }
  }

  if(fFirstReadOut) {
    fOldIntrVal = val;
    fFirstReadOut = false;

    return ENOTSUP; // first readout (op not supported)
  }

  MeasuredValue = val - fOldIntrVal;
  fOldIntrVal = val;

  return 0;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
