/*
***************************************************************************
**
** $Author$ - Initial Version by Arne Wiebalck 
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCValueMonitorMountPoint.hpp"
#include <stdio.h>

MLUCValueMonitorMountPoint :: MLUCValueMonitorMountPoint( const char* filename, 
							  const char* fileDescrHeader, 
							  const char* output_prefix, 
							  const char* output_suffix, 
							  const char* avg_output_prefix, 
							  const char* avg_output_suffix, 
							  unsigned int average_items, 
							  const char* fs_path ) :

  MLUCValueMonitor( filename, 
		    fileDescrHeader, 
		    output_prefix, 
		    output_suffix, 
		    avg_output_prefix, 
		    avg_output_suffix, 
		    average_items )
{
  path = fs_path;
}

MLUCValueMonitorMountPoint :: MLUCValueMonitorMountPoint ( const char* output_prefix, 
							   const char* output_suffix, 
							   const char* avg_output_prefix, 
							   const char* avg_output_suffix, 
							   unsigned int average_items,
							   const char* fs_path ) :
	
  MLUCValueMonitor( output_prefix, 
		    output_suffix, 
		    avg_output_prefix, 
		    avg_output_suffix, 
		    average_items )
{
  path = fs_path;
}

MLUCValueMonitorMountPoint :: MLUCValueMonitorMountPoint ( const char* filename, 
							   const char* fileDescrHeader, 
							   unsigned int average_items,
							   const char *fs_path ) : 

  MLUCValueMonitor( filename, 
		    fileDescrHeader, 
		    average_items )
{
  path = fs_path;
}

MLUCValueMonitorMountPoint :: MLUCValueMonitorMountPoint ( unsigned int average_items,
							   const char * ) : 

  MLUCValueMonitor( average_items )
{
}




int MLUCValueMonitorMountPoint :: GetValue( uint64& MeasuredValue ) {

  statfs( path, &buf );
  
  // free size in MB
  MeasuredValue = (uint64)((float)buf.f_bsize * (float)( (buf.f_blocks - buf.f_bfree) )  ) / (1024*1024);
  return 0;
}


