/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCLogServer.hpp"
#include <stdio.h>
#include <iomanip>
#include <sstream>
#include <iosfwd>
#include <syslog.h>
#include <glob.h>
#include <cerrno>
#include <cstdlib>

using namespace std;

void MLUCStdoutLogServer::LogLine( const char* origin, const char* keyword, const char* file, int linenr, 
				   const char* compile_date, const char* compile_time, MLUCLog::TLogLevel logLvl, 
				   const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
				   unsigned long runNumber, 
#endif
				   uint32 ldate, 
				   uint32 ltime_s, uint32 ltime_us, uint32 msgNr, const char* line )
	{
	const char* clvl;
	switch ( logLvl )
		{
		case MLUCLog::kBenchmark: clvl = "Benchmark"; break;
		case MLUCLog::kDebug: clvl = "Debug"; break;
		case MLUCLog::kInformational: clvl = "Information"; break;
		case MLUCLog::kWarning: clvl = "Warning"; break;
		case MLUCLog::kError: clvl = "Error"; break;
		case MLUCLog::kFatal: clvl = "Fatal Error"; break;
		case MLUCLog::kImportant: clvl = "Important"; break;
		default: clvl = "Unknown"; break;
		}
	printf( "%s%s%s %08d.%06d.%06d", hostname, (id ? ":" : ""), (id ? id : ""), ldate, ltime_s, ltime_us );
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
	if ( runNumber )
	    {
	    printf( " Run %12lu", runNumber );
	    }
#endif
	printf( " %10u [%s/%d (%s/%s)] (%s/%s) %s: %s\n", 
		msgNr, file, linenr, compile_date, compile_time, origin, 
		keyword, clvl, line );
	}

void MLUCStdoutLogServer::LogLine( const char* origin, const char* keyword, const char* file, int linenr, 
				   const char* compile_date, const char* compile_time, MLUCLog::TLogLevel logLvl, 
				   const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
				   unsigned long runNumber, 
#endif
				   uint32 ldate, 
				   uint32 ltime_s, uint32 ltime_us, uint32 msgNr, uint32 subMsgNr, const char* line )
	{
	const char* clvl;
	switch ( logLvl )
		{
		case MLUCLog::kBenchmark: clvl = "Benchmark"; break;
		case MLUCLog::kDebug: clvl = "Debug"; break;
		case MLUCLog::kInformational: clvl = "Information"; break;
		case MLUCLog::kWarning: clvl = "Warning"; break;
		case MLUCLog::kError: clvl = "Error"; break;
		case MLUCLog::kFatal: clvl = "Fatal Error"; break;
		case MLUCLog::kImportant: clvl = "Important"; break;
		default: clvl = "Unknown"; break;
		}
	printf( "%s%s%s %08d.%06d.%06d", hostname, (id ? ":" : ""), (id ? id : ""), ldate, ltime_s, ltime_us );
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
	if ( runNumber )
	    {
	    printf( " Run %12lu", runNumber );
	    }
#endif
	printf( " %10u/%10u [%s/%d (%s/%s)] (%s/%s)%s: %s\n", 
		msgNr, subMsgNr, file, linenr, compile_date, compile_time, origin, 
		keyword, clvl, line );
	}

void MLUCStderrLogServer::LogLine( const char* origin, const char* keyword, const char* file, int linenr, 
				   const char* compile_date, const char* compile_time, MLUCLog::TLogLevel logLvl, 
				   const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
				   unsigned long runNumber, 
#endif
				   uint32 ldate, 
				   uint32 ltime_s, uint32 ltime_us, uint32 msgNr, const char* line )
	{
	const char* clvl;
	switch ( logLvl )
		{
		case MLUCLog::kBenchmark: clvl = "Benchmark"; break;
		case MLUCLog::kDebug: clvl = "Debug"; break;
		case MLUCLog::kInformational: clvl = "Information"; break;
		case MLUCLog::kWarning: clvl = "Warning"; break;
		case MLUCLog::kError: clvl = "Error"; break;
		case MLUCLog::kFatal: clvl = "Fatal Error"; break;
		case MLUCLog::kImportant: clvl = "Important"; break;
		default: clvl = "Unknown"; break;
		}
	fprintf( stderr, "%s%s%s %08d.%06d.%06d", hostname, (id ? ":" : ""), (id ? id : ""), ldate, ltime_s, ltime_us );
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
	if ( runNumber )
	    {
	    fprintf( stderr, " Run %12lu", runNumber );
	    }
#endif
	fprintf( stderr, " %10u [%s/%d (%s/%s)] (%s/%s)%s: %s\n", 
		 msgNr, file, linenr, compile_date, compile_time, origin, 
		 keyword, clvl, line );
	}

void MLUCStderrLogServer::LogLine( const char* origin, const char* keyword, const char* file, int linenr, 
				   const char* compile_date, const char* compile_time, MLUCLog::TLogLevel logLvl, 
				   const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
				   unsigned long runNumber, 
#endif
				   uint32 ldate, 
				   uint32 ltime_s, uint32 ltime_us, uint32 msgNr, uint32 subMsgNr, const char* line )
	{
	const char* clvl;
	switch ( logLvl )
		{
		case MLUCLog::kBenchmark: clvl = "Benchmark"; break;
		case MLUCLog::kDebug: clvl = "Debug"; break;
		case MLUCLog::kInformational: clvl = "Information"; break;
		case MLUCLog::kWarning: clvl = "Warning"; break;
		case MLUCLog::kError: clvl = "Error"; break;
		case MLUCLog::kFatal: clvl = "Fatal Error"; break;
		case MLUCLog::kImportant: clvl = "Important"; break;
		default: clvl = "Unknown"; break;
		}
	fprintf( stderr, "%s%s%s %08d.%06d.%06d", hostname, (id ? ":" : ""), (id ? id : ""), ldate, ltime_s, ltime_us );
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
	if ( runNumber )
	    {
	    fprintf( stderr, " Run %12lu", runNumber );
	    }
#endif
	fprintf( stderr, " %10u/%10u [%s/%d (%s/%s)] (%s/%s)%s: %s\n", 
		 msgNr, subMsgNr, file, linenr, compile_date, compile_time, origin, 
		keyword, clvl, line );
	}




MLUCStreamLogServer::MLUCStreamLogServer( ostream& str ):
	fStream( str )
	{
	}


void MLUCStreamLogServer::LogLine( const char* origin, const char* keyword, const char* file, int linenr, 
				   const char* compile_date, const char* compile_time, MLUCLog::TLogLevel logLvl, 
				   const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
				   unsigned long runNumber, 
#endif
				   uint32 ldate, 
				   uint32 ltime_s, uint32 ltime_us, uint32 msgNr, const char* line )
	{
	const char* clvl;
	switch ( logLvl )
		{
		case MLUCLog::kBenchmark: clvl = "Benchmark"; break;
		case MLUCLog::kDebug: clvl = "Debug"; break;
		case MLUCLog::kInformational: clvl = "Information"; break;
		case MLUCLog::kWarning: clvl = "Warning"; break;
		case MLUCLog::kError: clvl = "Error"; break;
		case MLUCLog::kFatal: clvl = "Fatal Error"; break;
		case MLUCLog::kImportant: clvl = "Important"; break;
		default: clvl = "Unknown"; break;
		}
	fStream << setfill( '0' ) << hostname << (id ? ":" : "") << (id ? id : "") << " " << setw( 8 ) << ldate
		<< "." << setw( 6 ) << ltime_s << setw( 1 ) << "." << setw( 6 ) << ltime_us;
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
	if ( runNumber )
	    {
	    fStream << " Run " << setw( 12 ) << runNumber;
	    }
#endif
	fStream << setw( 10 ) << setfill( ' ' ) << msgNr
		<< " [" << file << "/" << setw( 0 )
		<< linenr << " (" << compile_date << "/" << compile_time << ")] (" << origin << "/"
		<< keyword << ") " << clvl << ": " << line << endl;
	}

void MLUCStreamLogServer::LogLine( const char* origin, const char* keyword, const char* file, int linenr, 
				   const char* compile_date, const char* compile_time, MLUCLog::TLogLevel logLvl, 
				   const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
				   unsigned long runNumber, 
#endif
				   uint32 ldate, 
				   uint32 ltime_s, uint32 ltime_us, uint32 msgNr, uint32 subMsgNr, const char* line )
	{
	const char* clvl;
	switch ( logLvl )
		{
		case MLUCLog::kBenchmark: clvl = "Benchmark"; break;
		case MLUCLog::kDebug: clvl = "Debug"; break;
		case MLUCLog::kInformational: clvl = "Information"; break;
		case MLUCLog::kWarning: clvl = "Warning"; break;
		case MLUCLog::kError: clvl = "Error"; break;
		case MLUCLog::kFatal: clvl = "Fatal Error"; break;
		case MLUCLog::kImportant: clvl = "Important"; break;
		default: clvl = "Unknown"; break;
		}
	bool leftSet = (fStream.flags() & ios::left);
	fStream << setfill( '0' ) << hostname << (id ? ":" : "") << (id ? id : "") << " " << setw( 8 ) << ldate
		<< "." << setw( 6 ) << ltime_s << setw( 1 ) << "." <<  setw( 6 ) << ltime_us;
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
	if ( runNumber )
	    {
	    fStream << " Run " << setw( 12 ) << runNumber;
	    }
#endif
	fStream << setw( 10 ) << setfill( ' ' ) << msgNr
		<< "/" << setw( 10 ) << setiosflags( ios::left ) << subMsgNr;
	if ( !leftSet )
	    fStream << resetiosflags( ios::left );
	fStream << " [" << file << "/" << setw( 0 )
		<< linenr << " (" << compile_date << "/" << compile_time << ")] (" << origin << "/"
		<< keyword << ") " << clvl << ": " << line << endl;
	}


void MLUCFilteredStdoutLogServer::LogLine( const char* origin, const char* keyword, const char*, int, 
					   const char*, const char*, MLUCLog::TLogLevel logLvl, const char*, 
					   const char*, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
					   unsigned long runNumber, 
#endif
					   uint32, uint32, uint32, 
					   uint32 msgNr, const char* line )
    {
    const char* clvl;
    switch ( logLvl )
	{
	case MLUCLog::kBenchmark: clvl = "Benchmark"; break;
	case MLUCLog::kDebug: clvl = "Debug"; break;
	case MLUCLog::kInformational: clvl = "Information"; break;
	case MLUCLog::kWarning: clvl = "Warning"; break;
	case MLUCLog::kError: clvl = "Error"; break;
	case MLUCLog::kFatal: clvl = "Fatal Error"; break;
	case MLUCLog::kImportant: clvl = "Important"; break;
	default: clvl = "Unknown"; break;
	}
//     printf( "%s %08d.%06d %10u (%s/%s) %s: %s\n", hostname, ldate, ltime,
// 	    msgNr, (const char*)origin, 
// 	    (const char*)keyword, clvl, (const char*)line );
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
    if ( runNumber )
	{
	printf( "Run %12lu ", runNumber );
	}
#endif
    printf( "%10u (%s/%s) %s: %s\n", msgNr, origin, 
	    keyword, clvl, line );
    }

void MLUCFilteredStdoutLogServer::LogLine( const char* origin, const char* keyword, const char*, int, 
					   const char*, const char*, MLUCLog::TLogLevel logLvl, const char*, 
					   const char*, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
					   unsigned long runNumber, 
#endif
					   uint32, uint32, uint32, 
					   uint32 msgNr, uint32 subMsgNr, const char* line )
    {
    const char* clvl;
    switch ( logLvl )
	{
	case MLUCLog::kBenchmark: clvl = "Benchmark"; break;
	case MLUCLog::kDebug: clvl = "Debug"; break;
	case MLUCLog::kInformational: clvl = "Information"; break;
	case MLUCLog::kWarning: clvl = "Warning"; break;
	case MLUCLog::kError: clvl = "Error"; break;
	case MLUCLog::kFatal: clvl = "Fatal Error"; break;
	case MLUCLog::kImportant: clvl = "Important"; break;
	default: clvl = "Unknown"; break;
	}
//     printf( "%s %08d.%06d %10u/%10u (%s/%s)%s: %s\n", hostname, ldate, ltime,
// 	    msgNr, subMsgNr, (const char*)origin, 
// 	    (const char*)keyword, clvl, (const char*)line );
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
    if ( runNumber )
	{
	printf( "Run %12lu ", runNumber );
	}
#endif
    printf( "%10u/%10u (%s/%s)%s: %s\n", msgNr, subMsgNr, origin, 
	    keyword, clvl, line );
    }


void MLUCFilteredStderrLogServer::LogLine( const char* origin, const char* keyword, const char*, int, 
					   const char*, const char*, MLUCLog::TLogLevel logLvl, const char*, 
					   const char*, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
					   unsigned long runNumber, 
#endif
					   uint32, uint32, uint32, 
					   uint32 msgNr, const char* line )
    {
    const char* clvl;
    switch ( logLvl )
	{
	case MLUCLog::kBenchmark: clvl = "Benchmark"; break;
	case MLUCLog::kDebug: clvl = "Debug"; break;
	case MLUCLog::kInformational: clvl = "Information"; break;
	case MLUCLog::kWarning: clvl = "Warning"; break;
	case MLUCLog::kError: clvl = "Error"; break;
	case MLUCLog::kFatal: clvl = "Fatal Error"; break;
	case MLUCLog::kImportant: clvl = "Important"; break;
	default: clvl = "Unknown"; break;
	}
//     printf( "%s %08d.%06d %10u (%s/%s) %s: %s\n", hostname, ldate, ltime,
// 	    msgNr, (const char*)origin, 
// 	    (const char*)keyword, clvl, (const char*)line );
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
    if ( runNumber )
	{
	fprintf( stderr, "Run %12lu ", runNumber );
	}
#endif
    fprintf( stderr, "%10u (%s/%s) %s: %s\n", msgNr, origin, 
	    keyword, clvl, line );
    fflush( stderr );
    }

void MLUCFilteredStderrLogServer::LogLine( const char* origin, const char* keyword, const char*, int, 
					   const char*, const char*, MLUCLog::TLogLevel logLvl, const char*, 
					   const char*, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
					   unsigned long runNumber, 
#endif
					   uint32, uint32, uint32, 
					   uint32 msgNr, uint32 subMsgNr, const char* line )
    {
    const char* clvl;
    switch ( logLvl )
	{
	case MLUCLog::kBenchmark: clvl = "Benchmark"; break;
	case MLUCLog::kDebug: clvl = "Debug"; break;
	case MLUCLog::kInformational: clvl = "Information"; break;
	case MLUCLog::kWarning: clvl = "Warning"; break;
	case MLUCLog::kError: clvl = "Error"; break;
	case MLUCLog::kFatal: clvl = "Fatal Error"; break;
	case MLUCLog::kImportant: clvl = "Important"; break;
	default: clvl = "Unknown"; break;
	}
//     printf( "%s %08d.%06d %10u/%10u (%s/%s)%s: %s\n", hostname, ldate, ltime,
// 	    msgNr, subMsgNr, (const char*)origin, 
// 	    (const char*)keyword, clvl, (const char*)line );
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
    if ( runNumber )
	{
	fprintf( stderr, "Run %12lu ", runNumber );
	}
#endif
    fprintf( stderr, "%10u/%10u (%s/%s)%s: %s\n", msgNr, subMsgNr, origin, 
	    keyword, clvl, line );
    fflush( stderr );
    }



MLUCSysLogServer::MLUCSysLogServer( const char* ident, int facility )
    {
    fIdent = ident;
    fFacility = facility;
    openlog( fIdent.c_str(), LOG_CONS|LOG_NDELAY|LOG_PID, fFacility );
    }

MLUCSysLogServer::~MLUCSysLogServer()
    {
    closelog();
    }

void MLUCSysLogServer::LogLine( const char* origin, const char* keyword, const char* file, int linenr, 
				const char* compile_date, const char* compile_time, MLUCLog::TLogLevel logLvl, 
				const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
				unsigned long runNumber, 
#endif
				uint32 ldate, 
				uint32 ltime_s, uint32 ltime_us, uint32 msgNr, const char* line )
	{
	const char* clvl = "Unknown Error";
	int priority = LOG_EMERG;
	switch ( logLvl )
		{
		case MLUCLog::kBenchmark: clvl = "Benchmark"; priority = LOG_DEBUG; break;
		case MLUCLog::kDebug: clvl = "Debug"; priority = LOG_DEBUG; break;
		case MLUCLog::kInformational: clvl = "Information"; priority = LOG_INFO; break;
		case MLUCLog::kWarning: clvl = "Warning"; priority = LOG_WARNING; break;
		case MLUCLog::kError: clvl = "Error"; priority = LOG_ERR; break;
		case MLUCLog::kFatal: clvl = "Fatal Error"; priority = LOG_CRIT; break;
		case MLUCLog::kImportant: clvl = "Important";  priority = LOG_CRIT; break;
		default: clvl = "Unknown"; break;
		}
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
	if ( runNumber )
	    syslog( priority, "%s%s%s %08d.%06d.%06d Run %12lu %10u [%s/%d (%s/%s)] (%s/%s) %s: %s\n", hostname, (id ? ":" : ""), (id ? id : ""), ldate, ltime_s, ltime_us,
		    runNumber, msgNr, file, linenr, compile_date, compile_time, origin, 
		    keyword, clvl, line );
	else
#endif
	    syslog( priority, "%s%s%s %08d.%06d.%06d %10u [%s/%d (%s/%s)] (%s/%s) %s: %s\n", hostname, (id ? ":" : ""), (id ? id : ""), ldate, ltime_s, ltime_us,
		    msgNr, file, linenr, compile_date, compile_time, origin, 
		    keyword, clvl, line );
	}

void MLUCSysLogServer::LogLine( const char* origin, const char* keyword, const char* file, int linenr, 
				const char* compile_date, const char* compile_time, MLUCLog::TLogLevel logLvl, 
				const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
				unsigned long runNumber, 
#endif
				uint32 ldate, 
				uint32 ltime_s, uint32 ltime_us, uint32 msgNr, uint32 subMsgNr, const char* line )
	{
	const char* clvl = "Unknown Error";
	int priority = LOG_EMERG;
	switch ( logLvl )
		{
		case MLUCLog::kBenchmark: clvl = "Benchmark"; priority = LOG_DEBUG; break;
		case MLUCLog::kDebug: clvl = "Debug"; priority = LOG_DEBUG; break;
		case MLUCLog::kInformational: clvl = "Information"; priority = LOG_INFO; break;
		case MLUCLog::kWarning: clvl = "Warning"; priority = LOG_WARNING; break;
		case MLUCLog::kError: clvl = "Error"; priority = LOG_ERR; break;
		case MLUCLog::kFatal: clvl = "Fatal Error"; priority = LOG_CRIT; break;
		case MLUCLog::kImportant: clvl = "Important";  priority = LOG_CRIT; break;
		default: clvl = "Unknown"; break;
		}
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
	if ( runNumber )
	    syslog( priority, "%s%s%s %08d.%06d.%06d Run %12lu %10u/%10u [%s/%d (%s/%s)] (%s/%s)%s: %s\n", hostname, (id ? ":" : ""), (id ? id : ""), ldate, ltime_s, ltime_us,
		    runNumber, msgNr, subMsgNr, file, linenr, compile_date, compile_time, origin, 
		    keyword, clvl, line );
	else
#endif
	    syslog( priority, "%s%s%s %08d.%06d.%06d %10u/%10u [%s/%d (%s/%s)] (%s/%s)%s: %s\n", hostname, (id ? ":" : ""), (id ? id : ""), ldate, ltime_s, ltime_us,
		    msgNr, subMsgNr, file, linenr, compile_date, compile_time, origin, 
		    keyword, clvl, line );
	}





MLUCFileLogServer::MLUCFileLogServer( const char* file_prefix, const char* file_suffix, 
			   unsigned long msg_count_per_file, unsigned long overlap_msg_count, unsigned long max_file_count )
    {
    fFilePrefix = file_prefix;
    fFileSuffix = file_suffix;
    fMsgsPerFile = msg_count_per_file;
    fOverlapMsgs = overlap_msg_count;
    if ( fOverlapMsgs >= fMsgsPerFile )
	fOverlapMsgs = 0;
    fMaxFileCount = max_file_count;
    fCurrentLineCount = fCurrentFileCount = 0;
    //OpenNextFile();
    }

MLUCFileLogServer::MLUCFileLogServer( const char* directory, const char* file_prefix, const char* file_suffix, 
			   unsigned long msg_count_per_file, unsigned long overlap_msg_count, unsigned long max_file_count )
    {
    fFilePrefix = file_prefix;
    fFileSuffix = file_suffix;
    fMsgsPerFile = msg_count_per_file;
    fOverlapMsgs = overlap_msg_count;
    if ( fOverlapMsgs >= fMsgsPerFile )
	fOverlapMsgs = 0;
    fMaxFileCount = max_file_count;
    fCurrentLineCount = fCurrentFileCount = 0;
    fDirectory = directory;
    //OpenNextFile();
    }

void MLUCFileLogServer::LogLine( const char* origin, const char* keyword, const char* file, int linenr, 
				 const char* compile_date, const char* compile_time, MLUCLog::TLogLevel logLvl, 
				 const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
				 unsigned long runNumber, 
#endif
				 uint32 ldate, 
				 uint32 ltime_s, uint32 ltime_us, uint32 msgNr, const char* line )
    {
    if ( !fFile.is_open() )
	OpenNextFile();
    const char* clvl = "Unknown Error";
    switch ( logLvl )
	{
	case MLUCLog::kBenchmark: clvl = "Benchmark"; break;
	case MLUCLog::kDebug: clvl = "Debug"; break;
	case MLUCLog::kInformational: clvl = "Information"; break;
	case MLUCLog::kWarning: clvl = "Warning"; break;
	case MLUCLog::kError: clvl = "Error"; break;
	case MLUCLog::kFatal: clvl = "Fatal Error"; break;
	case MLUCLog::kImportant: clvl = "Important";  break;
	default: clvl = "Unknown"; break;
	}
    fFile << setfill( '0' ) << hostname << (id ? ":" : "") << (id ? id : "")  << " " << setw( 8 ) << ldate
	  << "." << setw( 6 ) << ltime_s << setw( 1 ) << "." << setw( 6 ) << ltime_us;
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
    if ( runNumber )
	{
	fFile << " Run " << setw( 12 ) << runNumber;
	}
#endif
    fFile << setw( 10 ) << setfill( ' ' ) << msgNr
	  << " [" << file << "/" << setw( 0 )
	  << linenr << " (" << compile_date << "/" << compile_time << ")] (" << origin << "/"
	  << keyword << ") " << clvl << ": " << line << endl;
    fCurrentLineCount++;
    if ( fCurrentLineCount >= fMsgsPerFile )
	{
	CloseFile();
	}
    if ( fCurrentLineCount < fMsgsPerFile && fCurrentLineCount >= fMsgsPerFile-fOverlapMsgs )
	{
	ostringstream str;
	MLUCString fileline( "" );
	fileline += hostname;
	if ( id )
	    {
	    fileline += ":";
	    fileline += id;
	    }
	fileline += " ";
	str << setfill( '0' ) << setw( 8 ) << ldate << ends;
	fileline += str.str().c_str();
	str.seekp( 0, ios::beg );
	fileline += ".";
	str << setw( 6 ) << ltime_s << setw( 1 ) << "." << ends;
	fileline += str.str().c_str();
	str.seekp( 0, ios::beg );
	str << setw( 6 ) << ltime_us;
	fileline += str.str().c_str();
	str.seekp( 0, ios::beg );
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
	if ( runNumber )
	    {
	    str << " Run " << setw( 12 ) << runNumber;
	    fileline += str.str().c_str();
	    str.seekp( 0, ios::beg );
	    }
#endif
	str << setw( 10 ) << setfill( ' ' ) << msgNr << ends;
	fileline += str.str().c_str();
	str.seekp( 0, ios::beg );
	fileline += " [";
	fileline += file;
	fileline += "/";
	str << setw( 0 ) << linenr << " (" << ends;
	fileline += str.str().c_str();
	str.seekp( 0, ios::beg );
	fileline += compile_date;
	fileline += "/";
	fileline += compile_time;
	fileline += ")] (";
	fileline += origin;
	fileline += "/";
	fileline += keyword;
	fileline += ") ";
	fileline += clvl;
	fileline += ": ";
	fileline += line;
	fOverlapLines.insert( fOverlapLines.end(), fileline );
	}
    }

void MLUCFileLogServer::LogLine( const char* origin, const char* keyword, const char* file, int linenr, 
				 const char* compile_date, const char* compile_time, MLUCLog::TLogLevel logLvl, 
				 const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
				 unsigned long runNumber, 
#endif
				 uint32 ldate, 
				 uint32 ltime_s, uint32 ltime_us, uint32 msgNr, uint32 subMsgNr, const char* line )
    {
    if ( !fFile.is_open() )
	OpenNextFile();
    const char* clvl;
    switch ( logLvl )
	{
	case MLUCLog::kBenchmark: clvl = "Benchmark"; break;
	case MLUCLog::kDebug: clvl = "Debug"; break;
	case MLUCLog::kInformational: clvl = "Information"; break;
	case MLUCLog::kWarning: clvl = "Warning"; break;
	case MLUCLog::kError: clvl = "Error"; break;
	case MLUCLog::kFatal: clvl = "Fatal Error"; break;
	case MLUCLog::kImportant: clvl = "Important";  break;
	default: clvl = "Unknown"; break;
	}
    bool leftSet = (fFile.flags() & ios::left);
    fFile << setfill( '0' ) << hostname << (id ? ":" : "") << (id ? id : "")  << " " << setw( 8 ) << ldate
	    << "." << setw( 6 ) << ltime_s << setw( 1 ) << "." <<  setw( 6 ) << ltime_us;
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
    if ( runNumber )
	{
	fFile << " Run " << setw( 12 ) << runNumber;
	}
#endif
    fFile << setw( 10 ) << setfill( ' ' ) << msgNr
	  << "/" << setw( 10 ) << setiosflags( ios::left ) << subMsgNr;
    if ( !leftSet )
	fFile << resetiosflags( ios::left );
    fFile << " [" << file << "/" << setw( 0 )
	  << linenr << " (" << compile_date << "/" << compile_time << ")] (" << origin << "/"
	  << keyword << ") " << clvl << ": " << line << endl;
    fCurrentLineCount++;
    if ( fCurrentLineCount >= fMsgsPerFile )
	{
	CloseFile();
	}
    if ( fCurrentLineCount < fMsgsPerFile && fCurrentLineCount >= fMsgsPerFile-fOverlapMsgs )
	{
	ostringstream str;
	MLUCString fileline( "" );
	fileline += hostname;
	if ( id )
	    {
	    fileline += ":";
	    fileline += id;
	    }
	fileline += " ";
	str << setfill( '0' ) << setw( 8 ) << ldate << ends;
	fileline += str.str().c_str();
	str.seekp( 0, ios::beg );
	fileline += ".";
	str << setw( 6 ) << ltime_s << setw( 1 ) << "." << ends;
	fileline += str.str().c_str();
	str.seekp( 0, ios::beg );
	str << setw( 6 ) << ltime_us;
	fileline += str.str().c_str();
	str.seekp( 0, ios::beg );
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
	if ( runNumber )
	    {
	    str << " Run " << setw( 12 ) << runNumber;
	    fileline += str.str().c_str();
	    str.seekp( 0, ios::beg );
	    }
#endif
	str << setw( 10 ) << setfill( ' ' ) << msgNr << ends;
	fileline += str.str().c_str();
	str.seekp( 0, ios::beg );
	fileline += "/";
	leftSet = (str.flags() & ios::left);
	str << setw( 10 ) << setiosflags( ios::left ) << subMsgNr;
	if ( !leftSet )
	    str << resetiosflags( ios::left );
	fileline += str.str().c_str();
	str.seekp( 0, ios::beg );
	fileline += " [";
	fileline += file;
	fileline += "/";
	str << setw( 0 ) << linenr << " (" << ends;
	fileline += str.str().c_str();
	str.seekp( 0, ios::beg );
	fileline += compile_date;
	fileline += "/";
	fileline += compile_time;
	fileline += ")] (";
	fileline += origin;
	fileline += "/";
	fileline += keyword;
	fileline += ") ";
	fileline += clvl;
	fileline += ": ";
	fileline += line;
	fOverlapLines.insert( fOverlapLines.end(), fileline );
	}
    }


void MLUCFileLogServer::CloseFile()
    {
    fFile.close();
    fCurrentLineCount = 0;
    }

void MLUCFileLogServer::OpenNextFile()
    {
    if ( fMaxFileCount && fCurrentFileCount >= fMaxFileCount )
	return;
    char tmp[ 64 ];
    unsigned long highestFileCount=0, fileCount;
    glob_t globDat;
    // Build the new filename;
    MLUCString nextFilename, prefix;
    //sprintf( tmp, "0x........", fCurrentFileCount );
    if ( fDirectory.Length()>0 )
	nextFilename = fDirectory;
    else
	nextFilename = ".";
    nextFilename += "/";
    nextFilename += fFilePrefix;
    prefix = nextFilename;
    nextFilename += "0x????????";
    nextFilename += fFileSuffix;
    globDat.gl_offs = 0;
    globDat.gl_pathc = 0;
    globDat.gl_pathv = NULL;
    int ret = glob( nextFilename.c_str(), GLOB_MARK, NULL, &globDat );
    if ( !ret && globDat.gl_pathc>0 )
	{
	for ( unsigned n=0; n<globDat.gl_pathc; n++ )
	    {
	    char tmpNr[11];
	    char* cpErr;
	    strncpy( tmpNr, globDat.gl_pathv[n]+strlen(prefix.c_str()), 10 );
	    tmpNr[10] = '\0';
	    fileCount = strtoul( tmpNr, &cpErr, 0 );
	    if ( *cpErr != '\0' )
		fileCount=0;
	    if ( fileCount > highestFileCount )
		highestFileCount = fileCount;
	    }
	fCurrentFileCount = highestFileCount+1;
	}
    globfree( &globDat );
    
    // Build the new filename;
    sprintf( tmp, "0x%08lX", fCurrentFileCount );
    if ( fDirectory.Length()>0 )
	fCurrentFileName = fDirectory;
    else
	fCurrentFileName = ".";
    fCurrentFileName += "/";
    fCurrentFileName += fFilePrefix;
    fCurrentFileName += tmp;
    fCurrentFileName += fFileSuffix;
    
    // Try to open the file
    fFile.open( fCurrentFileName.c_str() );
    if ( !fFile.is_open() )
	{
	cerr << "Error opening log file '" << fCurrentFileName.c_str() << "' for output.\n";
	return;
	}
    fCurrentFileCount++;
    fCurrentLineCount = 0;

    // Now log the overlap messages into 
    vector<MLUCString>::iterator iter, end;
    iter = fOverlapLines.begin();
    end = fOverlapLines.end();
    while ( iter != end )
	{
	fCurrentLineCount++;
	fFile << iter->c_str() << "\n";
	iter++;
	}
    fOverlapLines.clear();
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
