/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCThreadFIFO.hpp"
#ifdef DEBUG
#include <stdio.h>
#endif
#include <errno.h>
#include <string.h>

MLUCThreadFIFO::MLUCThreadFIFO( uint32 bufferSizeB, uint32 emergencySizeB )
	{
	// Allocate (ordinary) buffer, set indices to zero 
	// and the size to the appropriate value
	fBuffer = new uint8[ bufferSizeB ];
	if ( fBuffer )
		fBufferSize = bufferSizeB;
	else
		fBufferSize = 0;
	fBufferRead = fBufferWrite = fBufferWriteEnd = 0;
	fLastBufferWrite = false;

	// Allocate emergency buffer, set index to zero 
	// and the size to the appropriate value
	fEmergency = new uint8[ emergencySizeB ];
	if ( fEmergency )
		fEmergencySize = emergencySizeB;
	else
		fEmergencySize = 0;
	fEmergencyWrite = 0;

	// Initialize mutex semaphores
	pthread_mutex_init( &fAccessSem, NULL );
	//pthread_mutex_init( &fCondSem, NULL );
	pthread_cond_init( &fCond, NULL );
	}

MLUCThreadFIFO::~MLUCThreadFIFO()
	{
	// Free ordinary buffer
	if ( fBuffer )
		delete [] fBuffer;
	fBuffer = NULL;
	fBufferSize = fBufferRead = fEmergencyWrite = fBufferWriteEnd = 0;
	fLastBufferWrite = false;

	// Free emergency buffer
	if ( fEmergency )
		delete [] fEmergency;
	fEmergency = NULL;
	fEmergencyWrite = 0;
	
	// Destroy mutex sempahores
	if ( pthread_mutex_trylock( &fAccessSem )==EBUSY )
	    pthread_mutex_unlock( &fAccessSem );
	pthread_mutex_destroy( &fAccessSem );
#ifdef DEBUG
	//printf( "        DEBUG %p   ~MLUCThread: Trying to unlock DataSem\n", this );
#endif 
	//pthread_mutex_destroy( &fCondSem );
	pthread_cond_destroy( &fCond );
	}

MLUCThreadFIFO::TError MLUCThreadFIFO::Write( const MLUCThreadMsg* msg )
	{
	uint32 free;
	uint32 len = msg->fLength;
	// Is there a message to write?
	if ( len==0 )
		return kWrongMsgLength;
	// IS the message too large for the whole buffer?
	if ( len > fBufferSize )
		return kBufferTooSmall;
	pthread_mutex_lock( &fAccessSem );
	if ( fBufferRead==fBufferWrite )
		{
		// Is the buffer all full...
		if ( fLastBufferWrite )
			{
			pthread_mutex_unlock( &fAccessSem );
			return kBufferFull;
			}
		else
			free = fBufferSize; // ... or all empty
		}
	else
		{
		// Determine the free space in the buffer (between write and read index)
		if ( fBufferWrite < fBufferRead )
			// The write pointer wrapped around and the read pointer did not
			free = (fBufferRead-fBufferWrite);
		else
			// Either both of the indices or neither wrapped around
			free = (fBufferRead+fBufferSize-fBufferWrite);
		}
#ifdef DEBUG
	printf( "        DEBUG %p   Free: %d - Needed: %d\n", this, free, len );
#endif
	// Do we have enough free mem in the buffer?
	if ( len > free )
		{
		// No we don't!
		pthread_mutex_unlock( &fAccessSem );
		return kBufferFull;
		}

	//fBufferWriteEnd = 0;
	if ( fBufferWrite >= fBufferRead )
		{
		// A wrap around could occur, check if the message fits
		// completey in the space before the end.
		if ( fBufferSize <= len+fBufferWrite )
			{
			// We would wrap around in the message which we want to avoid.
			// Check if we can fit the message in the space between the beginning
			// and the read index.
#ifdef DEBUG
			printf( "        DEBUG %p   Possible wrap around: fBufferSize %u - fBufferWrite %u - len %u\n", 
				this, fBufferSize, fBufferWrite, len );
#endif
			if ( fBufferRead < len )
				{
				// We cannot fit in.
				pthread_mutex_unlock( &fAccessSem );
				return kBufferFull;
				}
			// We can actually fit in...
			fBufferWriteEnd = fBufferWrite;
			fBufferWrite = 0;
			}
		}
#ifdef DEBUG
	printf( "        DEBUG %p   Write: fBufferRead: %d - fBufferWrite: %d - fBufferWriteEnd: %d - fBufferSize: %d\n",
		this, fBufferRead, fBufferWrite, fBufferWriteEnd, fBufferSize );
#endif
	// Copy the data into the buffer
	memcpy( fBuffer+fBufferWrite, msg, len );
	// Increment the write index
	fBufferWrite += len;
	if ( fBufferWrite==fBufferSize )
		fBufferWrite = 0;
	fLastBufferWrite = true;
	pthread_cond_broadcast( &fCond );
	pthread_mutex_unlock( &fAccessSem );
	return kOk;
	}

MLUCThreadFIFO::TError MLUCThreadFIFO::ReserveSlot( uint32 msgLength, MLUCThreadMsg*& msgPtr )
    {
    uint32 free;
    // Is there a message to write?
    if ( msgLength==0 )
	return kWrongMsgLength;
    // IS the message too large for the whole buffer?
    if ( msgLength > fBufferSize )
	return kBufferTooSmall;
    pthread_mutex_lock( &fAccessSem );
    if ( fBufferRead==fBufferWrite )
	{
	// Is the buffer all full...
	if ( fLastBufferWrite )
	    {
	    pthread_mutex_unlock( &fAccessSem );
	    return kBufferFull;
	    }
	else
	    free = fBufferSize; // ... or all empty
	}
    else
	{
	// Determine the free space in the buffer (between write and read index)
	if ( fBufferWrite < fBufferRead )
	    // The write pointer wrapped around and the read pointer did not
	    free = fBufferRead-fBufferWrite;
	else
	    // Either both of the indices or neither wrapped around
	    free = fBufferRead+fBufferSize-fBufferWrite;
	}
#ifdef DEBUG
    printf( "        DEBUG %p   Free: %d - Needed: %d\n", this, free, msgLength );
#endif
    // Do we have enough free mem in the buffer?
    if ( msgLength > free )
	{
	// No we don't!
	pthread_mutex_unlock( &fAccessSem );
	return kBufferFull;
	}
    
    //fBufferWriteEnd = 0;
    if ( fBufferWrite >= fBufferRead )
	{
	// A wrap around could occur, check if the message fits
	// completey in the space before the end.
	if ( fBufferSize <= msgLength+fBufferWrite )
	    {
	    // We would wrap around in the message which we want to avoid.
	    // Check if we can fit the message in the space between the beginning
	    // and the read index.
#ifdef DEBUG
	    printf( "        DEBUG %p   Possible wrap around: fBufferSize %u - fBufferWrite %u - msgLength %u\n", 
		    this, fBufferSize, fBufferWrite, msgLength );
#endif
	    if ( fBufferRead < msgLength )
		{
		// We cannot fit in.
		pthread_mutex_unlock( &fAccessSem );
		return kBufferFull;
		}
	    // We can actually fit in...
	    fBufferWriteEnd = fBufferWrite;
	    fBufferWrite = 0;
	    }
	}
#ifdef DEBUG
    printf( "        DEBUG %p   Write: fBufferRead: %d - fBufferWrite: %d - fBufferWriteEnd: %d - fBufferSize: %d\n",
	    this, fBufferRead, fBufferWrite, fBufferWriteEnd, fBufferSize );
#endif
    // Copy the data into the buffer
    msgPtr = (MLUCThreadMsg*)( fBuffer+fBufferWrite );
//     // Increment the write index
//     fBufferWrite += msgLength;
//     if ( fBufferWrite==fBufferSize )
// 	fBufferWrite = 0;
//     fLastBufferWrite = true;
//     pthread_cond_broadcast( &fCond );
//     pthread_mutex_unlock( &fAccessSem );
    return kOk;
    }


MLUCThreadFIFO::TError MLUCThreadFIFO::CommitMsg( uint32 msgLength )
    {
    uint32 free;
    // Is there a message to write?
    if ( msgLength==0 )
	{
	pthread_mutex_unlock( &fAccessSem );
	return kWrongMsgLength;
	}
    // Is the message too large for the whole buffer?
    if ( msgLength > fBufferSize )
	{
	pthread_mutex_unlock( &fAccessSem );
	return kBufferTooSmall;
	}
//     pthread_mutex_lock( &fAccessSem );
    if ( fBufferRead==fBufferWrite )
	{
	// Is the buffer all full...
	if ( fLastBufferWrite )
	    {
	    pthread_mutex_unlock( &fAccessSem );
	    return kBufferFull;
	    }
	else
	    free = fBufferSize; // ... or all empty
	}
    else
	{
	// Determine the free space in the buffer (between write and read index)
	if ( fBufferWrite < fBufferRead )
	    // The write pointer wrapped around and the read pointer did not
	    free = fBufferRead-fBufferWrite;
	else
	    // Either both of the indices or neither wrapped around
	    free = fBufferRead+fBufferSize-fBufferWrite;
	}
#ifdef DEBUG
    printf( "        DEBUG %p   Free: %d - Needed: %d\n", this, free, msgLength );
#endif
    // Do we have enough free mem in the buffer?
    if ( msgLength > free )
	{
	// No we don't!
	pthread_mutex_unlock( &fAccessSem );
	return kBufferFull;
	}
    
    //fBufferWriteEnd = 0;
//     if ( fBufferWrite >= fBufferRead )
// 	{
// 	// A wrap around could occur, check if the message fits
// 	// completey in the space before the end.
// 	if ( fBufferSize <= msgLength+fBufferWrite )
// 	    {
// 	    // We would wrap around in the message which we want to avoid.
// 	    // Check if we can fit the message in the space between the beginning
// 	    // and the read index.
// #ifdef DEBUG
// 	    printf( "        DEBUG %p   Possible wrap around: fBufferSize %u - fBufferWrite %u - msgLength %u\n", 
// 		    this, fBufferSize, fBufferWrite, msgLength );
// #endif
// 	    if ( fBufferRead < msgLength )
// 		{
// 		// We cannot fit in.
// 		pthread_mutex_unlock( &fAccessSem );
// 		return kBufferFull;
// 		}
// 	    // We can actually fit in...
// 	    fBufferWriteEnd = fBufferWrite;
// 	    fBufferWrite = 0;
// 	    }
// 	}
#ifdef DEBUG
    printf( "        DEBUG %p   Write: fBufferRead: %d - fBufferWrite: %d - fBufferWriteEnd: %d - fBufferSize: %d\n",
	    this, fBufferRead, fBufferWrite, fBufferWriteEnd, fBufferSize );
#endif
    MLUCThreadMsg* msgPtr = (MLUCThreadMsg*)( fBuffer+fBufferWrite );
    if ( msgPtr->fLength != msgLength )
	{
	pthread_mutex_unlock( &fAccessSem );
	return kWrongMsgLength;
	}
    // Increment the write index
    fBufferWrite += msgLength;
    if ( fBufferWrite==fBufferSize )
	fBufferWrite = 0;
    fLastBufferWrite = true;
    pthread_cond_broadcast( &fCond );
    pthread_mutex_unlock( &fAccessSem );
    return kOk;
    }



MLUCThreadFIFO::TError MLUCThreadFIFO::WaitForData()
	{
	pthread_mutex_lock( &fAccessSem );
#ifdef DEBUG
	printf( "        DEBUG %p   WaitForData start: fBufferRead: %d - fBufferWrite: %d - fBufferWriteEnd: %d - fBufferSize: %d\n",
		this, fBufferRead, fBufferWrite, fBufferWriteEnd, fBufferSize );
#endif

	if ( (fBufferRead==fBufferWrite && !fLastBufferWrite) && fEmergencyWrite<=0 )
		{
#ifdef DEBUG
		printf( "        DEBUG %p   WaitForData: Waiting for sempahore unlock...\n", this );
#endif
		pthread_cond_wait( &fCond, &fAccessSem );
		pthread_mutex_unlock( &fAccessSem );
		}
	else
		pthread_mutex_unlock( &fAccessSem );
#ifdef DEBUG
	printf( "        DEBUG %p   WaitForData end: fBufferRead: %d - fBufferWrite: %d - fBufferWriteEnd: %d - fBufferSize: %d\n",
		this, fBufferRead, fBufferWrite, fBufferWriteEnd, fBufferSize );
#endif
	return kOk;
	}

MLUCThreadFIFO::TError MLUCThreadFIFO::Read( MLUCThreadMsg*& msg )
	{
	pthread_mutex_lock( &fAccessSem );
	TError e;
	if ( fEmergencyWrite > 0 )
		// We have an emergency message waiting
		e = ReadEmergency( msg );
	else
		// No emergency message, try the ordinary ones...
		e = ReadOrdinary( msg );
// 	if ( (fBufferRead==fBufferWrite && !fLastBufferWrite) && fEmergencyWrite<=0 )
// 		{
// 		pthread_mutex_trylock( &fDataSem );
// 		}
	pthread_mutex_unlock( &fAccessSem );
	return e;
	}


MLUCThreadMsg* MLUCThreadFIFO::GetNextMsg()
	{
	pthread_mutex_lock( &fAccessSem );
#ifdef DEBUG
	printf( "        DEBUG %p   GetNextMsg: fBufferRead: %d - fBufferWrite: %d - fBufferWriteEnd: %d - fBufferSize: %d\n",
		this, fBufferRead, fBufferWrite, fBufferWriteEnd, fBufferSize );
#endif
	if ( fEmergencyWrite > 0 )
		{
#ifdef DEBUG
		printf( "        DEBUG %p   Returning emergency message\n", this );
#endif
		// There is an emergency message waiting
		pthread_mutex_unlock( &fAccessSem );
		return (MLUCThreadMsg*)fEmergency;
		}
	// No emergency message, try the ordinary ones.
	if ( fBufferRead==fBufferWrite && !fLastBufferWrite )
		{
		// Buffer is empty, no ordinary message
		pthread_mutex_unlock( &fAccessSem );
		return NULL;
		}
	// Check if a wrap around occured in the meantime...
	if ( fBufferRead==fBufferWriteEnd )
		fBufferWriteEnd = fBufferRead = 0;

	// Return the ordinary message.
	MLUCThreadMsg* p = (MLUCThreadMsg*)(fBuffer+fBufferRead);
	pthread_mutex_unlock( &fAccessSem );
	return p;
	}

MLUCThreadFIFO::TError MLUCThreadFIFO::FreeNextMsg()
	{
	pthread_mutex_lock( &fAccessSem );
	if ( fEmergencyWrite>0 )
		{
		// There is an emergency message waiting
#ifdef DEBUG
		printf( "        DEBUG %p   Freeing emergency message\n", this );
#endif
		fEmergencyWrite = 0;
		pthread_mutex_unlock( &fAccessSem );
		return kOk;
		}
	// No emergency message, try the ordinary ones.
	if ( fBufferRead==fBufferWrite && !fLastBufferWrite )
		{
		// Buffer is empty, no ordinary message
		pthread_mutex_unlock( &fAccessSem );
		return kBufferEmpty;
		}
	// Free the ordinary message
	// See how much we have to free...
	uint32 len = fBuffer[ fBufferRead ];
	// Increment the read index by the appropriate amount
	fBufferRead += len;
#ifdef PARANOID
	if ( fBufferRead>=fBufferSize )
		printf( "        DEBUG %p   fBufferRead too big (%d of %d)\n", this, fBufferRead, fBufferSize );
#endif
	if ( fBufferRead==fBufferWriteEnd || fBufferRead==fBufferSize )
		// We reached the point where we wrapped around when writing
		fBufferWriteEnd = fBufferRead = 0;
	fLastBufferWrite = false;
// 	if ( (fBufferRead==fBufferWrite && !fLastBufferWrite) && fEmergencyWrite<=0 )
// 		{
// 		pthread_mutex_trylock( &fDataSem );
// 		}

	pthread_mutex_unlock( &fAccessSem );
	return kOk;
	}

MLUCThreadFIFO::TError MLUCThreadFIFO::WriteEmergency( const MLUCThreadMsg* msg )
	{
	uint32 len = msg->fLength;
	if ( len > fEmergencySize )
		// The buffer is too small to hold the total message.
		return kBufferTooSmall;
	pthread_mutex_lock( &fAccessSem );
	if ( fEmergencyWrite > 0 )
		{
		// There is already something in the emergency buffer.
		pthread_mutex_unlock( &fAccessSem );
		return kBufferFull;
		}
	// Copy the message into the buffer
	memcpy( fEmergency, msg, len );
	// Increment the write pointer.
	fEmergencyWrite += len;
	pthread_cond_broadcast( &fCond );
	pthread_mutex_unlock( &fAccessSem );
	return kOk;
	}

MLUCThreadFIFO::TError MLUCThreadFIFO::ReadOrdinary( MLUCThreadMsg*& msg )
	{
#ifdef PARANOID
	// Check if we are allowed to be here in the first place
	if ( fEmergencyWrite>0 )
		return kInternalError;
#endif // PARANOID

#ifdef DEBUG 
	printf( "        DEBUG %p   ReadOrdinary: fBufferRead: %d - fBufferWrite: %d - fBufferWriteEnd: %d - fBufferSize: %d\n",
		this, fBufferRead, fBufferWrite, fBufferWriteEnd, fBufferSize );
#endif

	if ( fBufferRead==fBufferWrite && !fLastBufferWrite )
		{
		// Buffer is empty, no message available
		msg = NULL;
		return kBufferEmpty;
		}

	// Check if a wrap around occured in the meantime...
	if ( fBufferRead==fBufferWriteEnd )
		fBufferWriteEnd = fBufferRead = 0;

	// Determine how long the message to return is.
	//uint32 len = *(uint32*)(fBuffer+fBufferRead);
	uint32 len = ((MLUCThreadMsg*)(fBuffer+fBufferRead))->fLength;

#ifdef DEBUG
	printf( "        DEBUG %p   ReadOrdinary: Found message: %u length at position %u\n", this, len, fBufferRead );
#endif


	// Try to allocate space for the new message
	uint8*p = new uint8[ len ];
	msg = (MLUCThreadMsg*)p;
	if ( !p )
		// Allcoation did not succeed
		return kOutOfMemory;
	// Copy the message into the buffer
	memcpy( p, fBuffer+fBufferRead, len );
	// Increment read pointer
	fBufferRead += len;
	if ( fBufferRead==fBufferWriteEnd || fBufferRead==fBufferSize )
		// We have reached the point where we wrapped around when writing
		fBufferRead = 0;
	fLastBufferWrite = false;
	return kOk;
	}

MLUCThreadFIFO::TError MLUCThreadFIFO::ReadEmergency( MLUCThreadMsg*& msg )
	{
#ifdef PARANOID
	// Check if we are allowed to be here in the first place
	if ( fEmergencyWrite<=0 )
		return kInternalError;
#endif // PARANOID
	// Determine how long the message to return is.
	//uint32 len = *fEmergency;
	uint32 len = ((MLUCThreadMsg*)(fEmergency))->fLength;
	// Try to allocate space for the new message
	uint8*p = new uint8[ len ];
	msg = (MLUCThreadMsg*)p;
	if ( !p )
		// Allcoation did not succeed
		return kOutOfMemory;
	// Copy the message into the buffer
	memcpy( p, fEmergency, len );
	// Set write index to zero.
	fEmergencyWrite = 0;
	return kOk;
	}





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
