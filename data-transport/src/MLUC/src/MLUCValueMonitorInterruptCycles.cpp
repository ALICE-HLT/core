#include <string>
#include <fstream>
#if __GNUC__ ==2
#include <strstream>
#endif
#include <sstream>
#include <stdio.h>

#include "MLUCValueMonitorInterruptCycles.hpp"

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

void MLUCValueMonitorInterruptCycles::Init(const char* intrid, int cpuid)
{
  fIntr  = intrid;
  fIntr  += ":";   
  //  fPattern = intrid + ":";
  fCpuId = cpuid;
  fFirstReadOut = true;
  fNCpus = GetNumberOfCpus();
}

// -----------------------------------------------------------------------------

unsigned MLUCValueMonitorInterruptCycles::GetNumberOfCpus(void)
{
  int ncpus = 0;
  std::ifstream fin( "/proc/interrupts_cycles" );
  if (!fin) { // if file can not be opened 
    return 1; // the number of cpu's is 1 (since one cpu is always there :))
  }

  string line;
  if (!getline(fin, line)) {
    return 1;
  }
  
  unsigned long pos = 0;
  while ((pos = line.find("CPU", pos)) != string::npos) {
    pos += 3;
    ncpus++;
  }

  if (ncpus < 1) { // on unexpected errors
    ncpus = 1;     // set number of cpu's to 1
  }

  return (unsigned) ncpus;
}

// -----------------------------------------------------------------------------

unsigned MLUCValueMonitorInterruptCycles::GetNumberOfCpus2(void)
{
  int ncpus = -1;
  std::ifstream fin( "/proc/stat" );
  if (!fin) { // if file can not be opened 
    return 1; // the number of cpu's is 1 (since one cpu is always there :))
  }

  string line;
  while (getline(fin, line)) {
    int pos = line.find("cpu");
    if(pos == 0) { // if line starts with "cpu" increment counter
      ncpus++;
    }
  }
  if (ncpus < 1) { // on unexpected errors
    ncpus = 1;     // set number of cpu's to 1
  }

  return (unsigned) ncpus;
}

// -----------------------------------------------------------------------------

int MLUCValueMonitorInterruptCycles::GetValue( uint64& MeasuredValue )
    {
  bool bIntrFound = false;
  bool bReadSuccessful = false;
  uint64 tmp;
  unsigned long long val;
  unsigned long long dummy1, sum=0;
  char   cline[5000];
  
  if(fCpuId >= (int) fNCpus) 
      {
      LOG( MLUCLog::kError, "MLUCValueMonitorInterruptCycles:GetValue", 
	   "Cpu ID is wrong" ) << "Cpu ID: " << fCpuId  << " Number of cpu's:" 
			       << fNCpus << ENDLOG;
      return EINVAL;
      }
  
  std::ifstream fin( "/proc/interrupts_cycles" );
  if (!fin) 
      {
      LOG( MLUCLog::kError, "MLUCValueMonitorInterruptCycles:GetValue", 
	   "Error opening file" ) << "Could not open /proc/interrupts_cycles" << ENDLOG;
      return ENOENT;
      }

  std :: ifstream ins( "/proc/stat" );
  if( !ins )
      {
      LOG( MLUCLog::kError, "MLUCValueMonitorCycles_Used:GetValue", "Error opening file" )
	  << "Could not open /proc/stat" << ENDLOG;
      return ENOENT;
      }
  else
      {
      int no;
      unsigned long long tmpsum;
      // Find the global number of available cycles (one CPU or summed over all CPUs)
      switch( fCpuId )
	  {
	  
	  case INTRCYCLES_ALLCPUS: // overall cpu usage
	      while( !ins.eof() ) 
		  {
		  ins.getline( cline, 5000 );
		  if( sscanf( cline, "cycles %Lu %Lu %Lu %Lu %Lu", &dummy1, &dummy1, &dummy1, &dummy1, &tmpsum ) == 5 )
		      {
		      sum = tmpsum;
		      break;
		      // 		  if( kFirstReadOut ) 
		      // 		      {
		      // 		      fSumCyclesOld  = sum;
		      // 		      //return ENOTSUP; // first readout (op not supported)
		      // 		      } 
		      // 		  else
		      // 		      {
		      // 		      fSumCyclesNew  = sum;
		      
		      // 		      //CalculateValue( MeasuredValue );
		      // // 						MeasuredValue = ( ( kUsedCyclesNew+kIRQCyclesNew+kSoftIRQCyclesNew - 
		      // // 								    (kUsedCyclesOld+kIRQCyclesOld+kSoftIRQCyclesOld) ) * 100 ) / ( fSumCyclesNew - fSumCyclesOld );
		      
		      // 		      fSumCyclesOld  = fSumCyclesNew;
		      
// 		      }
		      }
		  }
	  default: // look at cpu 0, 1, 2 or what ever you have ...
	      while( !ins.eof() ) 
		  {
		  ins.getline( cline, 5000 );
		  if( sscanf( cline,"cycles%d %Lu %Lu %Lu %Lu %Lu", &no, &dummy1, &dummy1, &dummy1, &dummy1, &tmpsum ) == 6 )
		      {
		      if( no == fCpuId ) 
			  {
			  sum = tmpsum;
			  break;
			  // 			  if( kFirstReadOut ) 
			  // 			      {
			  // 			      fSumCyclesOld  = sum;
			  
			  // 			      //kFirstReadOut = false;
			  // 			      //return ENOTSUP; // first readout (op not supported)
			  // 			      }
			  // 			  else 
			  // 			      {
			  // 			      fSumCyclesNew  = sum;
			  
			  // 			      //CalculateValue( MeasuredValue );
			  // // 			      MeasuredValue = ( ( kUsedCyclesNew+kIRQCyclesNew+kSoftIRQCyclesNew - 
			  // // 						  (kUsedCyclesOld+kIRQCyclesOld+kSoftIRQCyclesOld) ) * 100 ) / ( fSumCyclesNew - fSumCyclesOld );
			  // 			      }
			  }
		      }
		  }
	  }
      }
	      
  string line;
  if (fIntr == "all:") {
    val = 0;
    getline(fin, line); // skip first line
    while (getline(fin, line)) {
      string dummy2;
#if __GNUC__>=3
      istringstream s(line.c_str());
#else
      istrstream s(line.c_str(), line.size());
#endif
      s >> dummy2;    // skip first column.
      
      if (fCpuId == INTRCYCLES_ALLCPUS) {
	// sum all interrupts_cycles from all cup's 
	for (unsigned i = 0; i < fNCpus; i++) { // loop over all cpu's
	  if(s >> tmp) {
	    val += tmp;
	  }
	}
      } else {
	// sum all interrupts_cycles from specified cpu
	for (int i = 0; i <= fCpuId; i++) { 
	  if (s >> tmp) {
	    bReadSuccessful = true;
	  } else {
	    bReadSuccessful = false;
	  }
	}
	if (bReadSuccessful) {
	  val += tmp;
	}
      }
    }
  } else { 
    // search for specified interrupt
    bIntrFound = false;
    while (!bIntrFound && getline(fin, line)) {
      string token;
#if __GNUC__>=3
      istringstream s(line.c_str());
#else
      istrstream s(line.c_str(), line.size());
#endif
      // loop over columns in this line
      while(s >> token) {
	if (fIntr.find(token) == 0) { 
	  bIntrFound = true;
	  break;
	}
      }
    }
    
    if(!bIntrFound) {
      LOG( MLUCLog::kError, "MLUCValueMonitorInterruptCycles:GetValue", 
	   "Wrong interrupt" ) << "no interrupt " << fIntr.c_str()  << ENDLOG;
      return EINVAL;
    }

    string dummy3;
#if __GNUC__>=3
    istringstream s(line.c_str());
#else
    istrstream s(line.c_str(), line.size());
#endif
    s >> dummy3;    // skip first column.
    if (fCpuId == INTRCYCLES_ALLCPUS) {
      // sum specified interrupt from all cpu's
      val = 0;
      for (unsigned i = 0; i < fNCpus; i++) {
	if(s >> tmp) {
	  val += tmp;
	}
      }
    } else {
      // get specified interrupt from specified cpu
      for (int i = 0; i <= fCpuId; i++) { 
	if (s >> tmp) {
	  bReadSuccessful = true;
	} else {
	  bReadSuccessful = false;
	}
      }
      // last readout in previous loop should be successful,
      //  otherwise an error has ocurred.
      if(!bReadSuccessful) {
      LOG( MLUCLog::kError, "MLUCValueMonitorInterruptCycles:GetValue", 
	   "Wrong interrupt" ) << "no interrupt " << fIntr.c_str()  << ENDLOG;
	return EINVAL;
      } 
      val = tmp;
    }
  }

  if(fFirstReadOut) {
    fOldIntrCycles = val;
    fOldSumCycles = sum;
    fFirstReadOut = false;
    

    return ENOTSUP; // first readout (op not supported)
  }

// 			      MeasuredValue = ( ( kUsedCyclesNew+kIRQCyclesNew+kSoftIRQCyclesNew - 
// 						  (kUsedCyclesOld+kIRQCyclesOld+kSoftIRQCyclesOld) ) * 100 ) / ( fSumCyclesNew - fSumCyclesOld );
  MeasuredValue = ((val - fOldIntrCycles) * 100) / (sum-fOldSumCycles);
  fOldIntrCycles = val;
  fOldSumCycles = sum;

  return 0;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
