/*
***************************************************************************
**
** $Author$ - Initial Version by Arne Wiebalck 
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCValueMonitorNet.hpp"

MLUCValueMonitorNet :: MLUCValueMonitorNet( const char* filename, 
											const char* fileDescrHeader, 
											const char* output_prefix, 
											const char* output_suffix, 
											const char* avg_output_prefix, 
											const char* avg_output_suffix, 
											unsigned int average_items, 
											short throughput_type, 
											short net_number ) :
	
	MLUCValueMonitor( filename, 
					  fileDescrHeader, 
					  output_prefix, 
					  output_suffix, 
					  avg_output_prefix, 
					  avg_output_suffix, 
					  average_items )
{
	fType_Of_Throughput = throughput_type;
	fNet_Number     = net_number; 
	fFirstReadOut   = true;
	fNumber_Of_NetDevs   = 0;
	GetNumberOfNetDevs();
	
	fOld_Sum = 0; 
	fNew_Sum = 0;	
      
}

MLUCValueMonitorNet :: MLUCValueMonitorNet ( const char* output_prefix, 
											 const char* output_suffix, 
											 const char* avg_output_prefix, 
											 const char* avg_output_suffix, 
											 unsigned int average_items,
											 short throughput_type, 
											 short net_number ) :
	
	MLUCValueMonitor( output_prefix, 
					  output_suffix, 
					  avg_output_prefix, 
					  avg_output_suffix, 
					  average_items )
{
	fType_Of_Throughput = throughput_type;
	fNet_Number = net_number;
	fFirstReadOut = true;
	fNumber_Of_NetDevs   = 0;
	GetNumberOfNetDevs();

	fOld_Sum = 0; 
	fNew_Sum = 0;
}

MLUCValueMonitorNet :: MLUCValueMonitorNet ( const char* filename, 
											 const char* fileDescrHeader, 
											 unsigned int average_items,
											 short throughput_type, 
											 short net_number ) : 

	MLUCValueMonitor( filename, 
					  fileDescrHeader, 
					  average_items )
{
	fType_Of_Throughput = throughput_type;
	fNet_Number = net_number;
	fFirstReadOut = true;
	fNumber_Of_NetDevs   = 0;
	GetNumberOfNetDevs();
	
	fOld_Sum = 0; 
	fNew_Sum = 0;
}

MLUCValueMonitorNet :: MLUCValueMonitorNet ( unsigned int average_items,
											 short throughput_type, 
											 short net_number ) : 

	MLUCValueMonitor( average_items )
{
	fType_Of_Throughput = throughput_type;
	fNet_Number = net_number;
	fFirstReadOut = true;
	fNumber_Of_NetDevs   = 0;
	GetNumberOfNetDevs();
	
	fOld_Sum = 0; 
	fNew_Sum = 0;
}

int MLUCValueMonitorNet :: GetNumberOfNetDevs() {
	
	char kLine[500]; // be on the safe side ;-)
	int  dummy;

	std :: ifstream in( "/proc/net/dev" );
	if( !in ) {
		LOG( MLUCLog::kError, "MLUCValueMonitorNet:GetNumberOfNetDevs", "Error opening file" )
			<< "Could not open /proc/net/dev" << ENDLOG;
		return ENOENT;
	} else {
		while( !in.eof() ) {
			in.getline( kLine, 500 );
			if( sscanf( kLine, " eth%d: ", &dummy) > 0 ) {
				fNumber_Of_NetDevs++;
			}
		}
	}
	if( fNet_Number > fNumber_Of_NetDevs - 1  ) {
		LOG( MLUCLog::kWarning, "MLUCValueMonitorNet:GetNumberOfNetDevs", "Wrong Network Interface ID" )
			<< "Cannot monitor eth" << fNumber_Of_NetDevs << ", since the system has only " 
			<< fNumber_Of_NetDevs << " net interfaces. Will read out overall network traffic." 
			<< ENDLOG;	

		// 		fNet_Number = -1;		
	}

	return 0;
}

int MLUCValueMonitorNet :: GetValue( uint64& MeasuredValue ) {
	
	char   kLine[500];   
	bool found = false;
	
	std :: ifstream in( "/proc/net/dev" );
	if( !in ) {
		LOG( MLUCLog::kError, "MLUCValueMonitorNet:GetValue", "Error opening file" )
			<< "Could not open /proc/net/dev" << ENDLOG;
		return ENOENT;
	} else {
		int no;
		unsigned long b_in, b_out, p_in, p_out, e_in, e_out, c_out, dummy;
		unsigned long tmp_sum = 0;

		// 		struct timeval now;
		// 		struct tm* t;
		// 		unsigned int date, time_s, time_us;
		// 		char filePrefix[128];
		// 		char charDate[32];

		char tmp_devid[32];
		
		switch( fNet_Number ) {
		case -1: // overall network traffic
		
			no = 0;
			b_in = 0;
			b_out = 0;
			p_in = 0;
			p_out = 0;
			e_in = 0;
			e_out = 0;
			c_out = 0;
			
			switch ( fType_Of_Throughput ) {
			case -1: // measure overall network traffic in/out in bytes, integrated over all devices


				while( !in.eof() ) {
					in.getline( kLine, 500 );
					
					if( sscanf( kLine, " eth%d : %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu ", 
								&no, &b_in, &p_in, &dummy, &dummy, &dummy, 
								&dummy, &dummy, &dummy, &b_out, &p_out, &dummy, &dummy, &dummy, 
								&dummy, &dummy, &dummy ) > 0 ) {
						tmp_sum += b_in + b_out;
					}
				}
				if( fFirstReadOut ) {
					fOld_Sum = tmp_sum;
					fNew_Sum = tmp_sum;
					fFirstReadOut = false;
					return ENOTSUP; // first readout (op not supported)
				} else {
					fOld_Sum = fNew_Sum;
					fNew_Sum = tmp_sum;
				}
				MeasuredValue = fNew_Sum - fOld_Sum;
				return 0;


			case  0: // measure bytes in, integrated over all devices 

				
				tmp_sum = 0;
				while( !in.eof() ) {
						
					in.getline( kLine, 500 );

					if( sscanf( kLine, " eth%d : %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu ", 
								&no, &b_in, &p_in, &dummy, &dummy, &dummy, 
								&dummy, &dummy, &dummy, &b_out, &p_out, &dummy, &dummy, &dummy, 
								&dummy, &dummy, &dummy ) > 0 ) {
						tmp_sum += b_in;
					}
				}
				if( fFirstReadOut ) {
					fOld_Sum = tmp_sum;
					fNew_Sum = tmp_sum;

					fFirstReadOut = false;
					return ENOTSUP; // first readout (op not supported)
				} else {
					fOld_Sum = fNew_Sum;
					fNew_Sum = tmp_sum;
				}
					
				MeasuredValue = fNew_Sum - fOld_Sum;
				return 0;
	
					
			case  1: // measure bytes out, integrated over all devices

				tmp_sum = 0;
				while( !in.eof() ) {

					in.getline( kLine, 500 );
				
					if( sscanf( kLine, " eth%d : %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu ", 
								&no, &b_in, &p_in, &dummy, &dummy, &dummy, 
								&dummy, &dummy, &dummy, &b_out, &p_out, &dummy, &dummy, &dummy, 
								&dummy, &dummy, &dummy ) > 0 ) {
						tmp_sum += b_out;
					}
				}
				if( fFirstReadOut ) {
					fOld_Sum = tmp_sum;
					fNew_Sum = tmp_sum;

					fFirstReadOut = false;
					return ENOTSUP; // first readout (op not supported)
				} else {
					fOld_Sum = fNew_Sum;
					fNew_Sum = tmp_sum;

				} 
					
				MeasuredValue = fNew_Sum - fOld_Sum;
				return 0;					
			
	
			case  2: // measure packets in, integrated over all devices

				tmp_sum = 0;
				while( !in.eof() ) {

					in.getline( kLine, 500 );

					if( sscanf( kLine, " eth%d : %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu ", 
								&no, &b_in, &p_in, &dummy, &dummy, &dummy, 
								&dummy, &dummy, &dummy, &b_out, &p_out, &dummy, &dummy, &dummy, 
								&dummy, &dummy, &dummy ) > 0 ) {
						tmp_sum += p_in;
					}
				}
				if( fFirstReadOut ) {
					fOld_Sum = tmp_sum;
					fNew_Sum = tmp_sum;

					fFirstReadOut = false;
					return ENOTSUP; // first readout (op not supported)
				} else {
					fOld_Sum = fNew_Sum;
					fNew_Sum = tmp_sum;

				} 
					
				MeasuredValue = fNew_Sum - fOld_Sum;
				return 0;
			       	
					
			case  3: // measure packets out, integrated over all devices

				tmp_sum = 0;
				while( !in.eof() ) {

					in.getline( kLine, 500 );
				
					if( sscanf( kLine, " eth%d : %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu ", 
								&no, &b_in, &p_in, &dummy, &dummy, &dummy, 
								&dummy, &dummy, &dummy, &b_out, &p_out, &dummy, &dummy, &dummy, 
								&dummy, &dummy, &dummy ) > 0 ) {
						tmp_sum += p_out;
					}
				}
				if( fFirstReadOut ) {
					fOld_Sum = tmp_sum;
					fNew_Sum = tmp_sum;

					fFirstReadOut = false;
					return ENOTSUP; // first readout (op not supported)
				} else {
					fOld_Sum = fNew_Sum;
					fNew_Sum = tmp_sum;
										
				} 
					
				MeasuredValue = fNew_Sum - fOld_Sum;
				return 0;


			case 4: // measure errors in, integrated over all devices

				tmp_sum = 0;
				while( !in.eof() ) {

					//static int n = 0;
					in.getline( kLine, 500 );
				
					if( sscanf( kLine, " eth%d : %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu ", 
								&no, &b_in, &p_in, &e_in, &dummy, &dummy, 
								&dummy, &dummy, &dummy, &b_out, &p_out, &e_out, &dummy, &dummy, 
								&c_out, &dummy, &dummy ) > 0 ) {
						tmp_sum += e_in;
					}
				}
				if( fFirstReadOut ) {
					fOld_Sum = tmp_sum;
					fNew_Sum = tmp_sum;

					fFirstReadOut = false;
					return ENOTSUP; // first readout (op not supported)
				} else {
					fOld_Sum = fNew_Sum;
					fNew_Sum = tmp_sum;
				}

				MeasuredValue = fNew_Sum - fOld_Sum;
				return 0;


			case 5: // measure errors out, integrated over all devices

				tmp_sum = 0;
				while( !in.eof() ) {

					in.getline( kLine, 500 );
				
					if( sscanf( kLine, " eth%d : %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu ", 
								&no, &b_in, &p_in, &e_in, &dummy, &dummy, 
								&dummy, &dummy, &dummy, &b_out, &p_out, &e_out, &dummy, &dummy, 
								&c_out, &dummy, &dummy ) > 0 ) {
						tmp_sum += e_out;
					}
				}
				if( fFirstReadOut ) {
					fOld_Sum = tmp_sum;
					fNew_Sum = tmp_sum;

					fFirstReadOut = false;
					return ENOTSUP; // first readout (op not supported)
				} else {
					fOld_Sum = fNew_Sum;
					fNew_Sum = tmp_sum;
				}

				MeasuredValue = fNew_Sum - fOld_Sum;
				return 0;


			case 6: // measure collisions out, integrated over all devices

				tmp_sum = 0;
				while( !in.eof() ) {

					in.getline( kLine, 500 );
				
					if( sscanf( kLine, " eth%d : %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu ", 
								&no, &b_in, &p_in, &e_in, &dummy, &dummy, 
								&dummy, &dummy, &dummy, &b_out, &p_out, &e_out, &dummy, &dummy, 
								&c_out, &dummy, &dummy ) > 0 ) {
						tmp_sum += c_out;
					}
				}
				if( fFirstReadOut ) {
					fOld_Sum = tmp_sum;
					fNew_Sum = tmp_sum;

					fFirstReadOut = false;
					return ENOTSUP; // first readout (op not supported)
				} else {
					fOld_Sum = fNew_Sum;
					fNew_Sum = tmp_sum;
				}

				MeasuredValue = fNew_Sum - fOld_Sum;
				return 0;


			case 7: // measure errors and collisions, integrated over all devices

				tmp_sum = 0;
				while( !in.eof() ) {

					in.getline( kLine, 500 );
				
					if( sscanf( kLine, " eth%d : %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu ", 
								&no, &b_in, &p_in, &e_in, &dummy, &dummy, 
								&dummy, &dummy, &dummy, &b_out, &p_out, &e_out, &dummy, &dummy, 
								&c_out, &dummy, &dummy ) > 0 ) {
						tmp_sum += e_in + e_out + c_out;
					}
				}
				if( fFirstReadOut ) {
					fOld_Sum = tmp_sum;
					fNew_Sum = tmp_sum;

					fFirstReadOut = false;
					return ENOTSUP; // first readout (op not supported)
				} else {
					fOld_Sum = fNew_Sum;
					fNew_Sum = tmp_sum;
				}

				MeasuredValue = fNew_Sum - fOld_Sum;
				return 0;

			}
		default: // look at network interface 0, 1, 2 or how many ever you can afford ...
			
			sprintf( tmp_devid, "-%d", fNet_Number );
			switch ( fType_Of_Throughput ) {
			case -1: // total net I/O through specified network interface
									
				tmp_sum = 0;
				while( !in.eof() ) {

					in.getline( kLine, 500 );

					if( sscanf( kLine, " eth%d : %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu ", 
								&no, &b_in, &p_in, &dummy, &dummy, &dummy, 
								&dummy, &dummy, &dummy, &b_out, &p_out, &dummy, &dummy, &dummy, 
								&dummy, &dummy, &dummy ) > 0 ) {
						if( no == fNet_Number ) {
							found = true;
							tmp_sum += b_in + b_out;
							break;
						}
					}
				}
				if( fFirstReadOut ) {
					fOld_Sum = tmp_sum;
					fNew_Sum = tmp_sum;

					fFirstReadOut = false;
					return ENOTSUP; // first readout (op not supported)
				} else {
					fOld_Sum = fNew_Sum;
					fNew_Sum = tmp_sum;

				} 
					
				MeasuredValue = fNew_Sum - fOld_Sum;
				if ( !found )
				    MeasuredValue = ~(uint64)0;
				return 0;
			       
				
			case  0: // input (bytes) through specified network interface 
				
				tmp_sum = 0;
				while( !in.eof() ) {

					in.getline( kLine, 500 );

					if( sscanf( kLine, " eth%d : %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu ", 
								&no, &b_in, &p_in, &dummy, &dummy, &dummy, 
								&dummy, &dummy, &dummy, &b_out, &p_out, &dummy, &dummy, &dummy, 
								&dummy, &dummy, &dummy ) > 0 ) {
						if( no == fNet_Number ) {
							found = true;
							tmp_sum += b_in;
							break;
						}
					}
				}
				if( fFirstReadOut ) {
					fOld_Sum = tmp_sum;
					fNew_Sum = tmp_sum;

					fFirstReadOut = false;
					return ENOTSUP; // first readout (op not supported)
				} else {
					fOld_Sum = fNew_Sum;
					fNew_Sum = tmp_sum;

				} 
					
				MeasuredValue = fNew_Sum - fOld_Sum;
				if ( !found )
				    MeasuredValue = ~(uint64)0;
				return 0;
				

			case  1: // output (bytes) through specified network interface 

				tmp_sum = 0;
				while( !in.eof() ) {

					in.getline( kLine, 500 );

					if( sscanf( kLine, " eth%d : %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu ", 
								&no, &b_in, &p_in, &dummy, &dummy, &dummy, 
								&dummy, &dummy, &dummy, &b_out, &p_out, &dummy, &dummy, &dummy, 
								&dummy, &dummy, &dummy ) > 0 ) {
						if( no == fNet_Number ) {
							found = true;
							tmp_sum += b_out;
							break;
						}
					}
				}
				if( fFirstReadOut ) {
					fOld_Sum = tmp_sum;
					fNew_Sum = tmp_sum;

					fFirstReadOut = false;
					return ENOTSUP; // first readout (op not supported)
				} else {
					fOld_Sum = fNew_Sum;
					fNew_Sum = tmp_sum;

				} 
					
				MeasuredValue = fNew_Sum - fOld_Sum;
				if ( !found )
				    MeasuredValue = ~(uint64)0;
				return 0;


			case  2: // input (packets) through specified network interface 
				
				tmp_sum = 0;
				while( !in.eof() ) {

					in.getline( kLine, 500 );

					if( sscanf( kLine, " eth%d : %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu ", 
								&no, &b_in, &p_in, &dummy, &dummy, &dummy, 
								&dummy, &dummy, &dummy, &b_out, &p_out, &dummy, &dummy, &dummy, 
								&dummy, &dummy, &dummy ) > 0 ) {
						if( no == fNet_Number ) {
							found = true;
							tmp_sum += p_in;
							break;
						}
					}
				}
				if( fFirstReadOut ) {
					fOld_Sum = tmp_sum;
					fNew_Sum = tmp_sum;

					fFirstReadOut = false;
					return ENOTSUP; // first readout (op not supported)
				} else {
					fOld_Sum = fNew_Sum;
					fNew_Sum = tmp_sum;

				} 
					
				MeasuredValue = fNew_Sum - fOld_Sum;
				if ( !found )
				    MeasuredValue = ~(uint64)0;
				return 0;
				

			case  3: // output (packets) through specified network interface 
				
				tmp_sum = 0;
				while( !in.eof() ) {

					in.getline( kLine, 500 );

					if( sscanf( kLine, " eth%d : %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu ", 
								&no, &b_in, &p_in, &dummy, &dummy, &dummy, 
								&dummy, &dummy, &dummy, &b_out, &p_out, &dummy, &dummy, &dummy, 
								&dummy, &dummy, &dummy ) > 0 ) {
						if( no == fNet_Number ) {
							found = true;
							tmp_sum += p_out;
							break;
						}
					}
				}
				if( fFirstReadOut ) {
					fOld_Sum = tmp_sum;
					fNew_Sum = tmp_sum;

					fFirstReadOut = false;
					return ENOTSUP; // first readout (op not supported)
				} else {
					fOld_Sum = fNew_Sum;
					fNew_Sum = tmp_sum;

				} 
					
				MeasuredValue = fNew_Sum - fOld_Sum;
				if ( !found )
				    MeasuredValue = ~(uint64)0;
				return 0;


			case  4: // input (errors) through specified network interface 
				
				tmp_sum = 0;
				while( !in.eof() ) {

					in.getline( kLine, 500 );

					if( sscanf( kLine, " eth%d : %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu ", 
								&no, &b_in, &p_in, &e_in, &dummy, &dummy, 
								&dummy, &dummy, &dummy, &b_out, &p_out, &e_out, &dummy, &dummy, 
								&c_out, &dummy, &dummy ) > 0 ) {
						if( no == fNet_Number ) {
							found = true;
							tmp_sum += e_in;
							break;
						}
					}
				}
				if( fFirstReadOut ) {
					fOld_Sum = tmp_sum;
					fNew_Sum = tmp_sum;

					fFirstReadOut = false;
					return ENOTSUP; // first readout (op not supported)
				} else {
					fOld_Sum = fNew_Sum;
					fNew_Sum = tmp_sum;

				} 
					
				MeasuredValue = fNew_Sum - fOld_Sum;
				if ( !found )
				    MeasuredValue = ~(uint64)0;
				return 0;


			case  5: // output (errors) through specified network interface 
				
				tmp_sum = 0;
				while( !in.eof() ) {

					in.getline( kLine, 500 );

					if( sscanf( kLine, " eth%d : %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu ", 
								&no, &b_in, &p_in, &e_in, &dummy, &dummy, 
								&dummy, &dummy, &dummy, &b_out, &p_out, &e_out, &dummy, &dummy, 
								&c_out, &dummy, &dummy ) > 0 ) {
						if( no == fNet_Number ) {
							found = true;
							tmp_sum += e_out;
							break;
						}
					}
				}
				if( fFirstReadOut ) {
					fOld_Sum = tmp_sum;
					fNew_Sum = tmp_sum;

					fFirstReadOut = false;
					return ENOTSUP; // first readout (op not supported)
				} else {
					fOld_Sum = fNew_Sum;
					fNew_Sum = tmp_sum;

				} 
					
				MeasuredValue = fNew_Sum - fOld_Sum;
				if ( !found )
				    MeasuredValue = ~(uint64)0;
				return 0;


			case  6: // output (collisions) through specified network interface 
				
				tmp_sum = 0;
				while( !in.eof() ) {

					in.getline( kLine, 500 );

					if( sscanf( kLine, " eth%d : %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu ", 
								&no, &b_in, &p_in, &e_in, &dummy, &dummy, 
								&dummy, &dummy, &dummy, &b_out, &p_out, &e_out, &dummy, &dummy, 
								&c_out, &dummy, &dummy ) > 0 ) {
						if( no == fNet_Number ) {
							found = true;
							tmp_sum += c_out;
							break;
						}
					}
				}
				if( fFirstReadOut ) {
					fOld_Sum = tmp_sum;
					fNew_Sum = tmp_sum;

					fFirstReadOut = false;
					return ENOTSUP; // first readout (op not supported)
				} else {
					fOld_Sum = fNew_Sum;
					fNew_Sum = tmp_sum;

				} 
					
				MeasuredValue = fNew_Sum - fOld_Sum;
				if ( !found )
				    MeasuredValue = ~(uint64)0;
				return 0;


			case  7: // total errors and collisions through specified network interface 
				
				tmp_sum = 0;
				while( !in.eof() ) {

					in.getline( kLine, 500 );

					if( sscanf( kLine, " eth%d : %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu ", 
								&no, &b_in, &p_in, &e_in, &dummy, &dummy, 
								&dummy, &dummy, &dummy, &b_out, &p_out, &e_out, &dummy, &dummy, 
								&c_out, &dummy, &dummy ) > 0 ) {
						if( no == fNet_Number ) {
							found = true;
							tmp_sum += e_in + e_out + c_out;
							break;
						}
					}
				}
				if( fFirstReadOut ) {
					fOld_Sum = tmp_sum;
					fNew_Sum = tmp_sum;

					fFirstReadOut = false;
					return ENOTSUP; // first readout (op not supported)
				} else {
					fOld_Sum = fNew_Sum;
					fNew_Sum = tmp_sum;

				} 
					
				MeasuredValue = fNew_Sum - fOld_Sum;
				if ( !found )
				    MeasuredValue = ~(uint64)0;
				return 0;

			}
			
			break;
			
		}
	}
	
	return 0;
}
