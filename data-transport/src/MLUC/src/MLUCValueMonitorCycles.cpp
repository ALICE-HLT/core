/*
***************************************************************************
**
** $Author$ - Initial Version by Arne Wiebalck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCValueMonitorCycles.hpp"
#include <stdio.h>

MLUCValueMonitorCycles :: MLUCValueMonitorCycles( const char* filename, 
						  const char* fileDescrHeader, 
						  const char* output_prefix, 
						  const char* output_suffix, 
						  const char* avg_output_prefix, 
						  const char* avg_output_suffix, 
						  unsigned int average_items, 
						  MLUCValueMonitorCycles::kDataType dataType,
						  short cpu_number ) :

	MLUCValueMonitor( filename, 
			  fileDescrHeader, 
			  output_prefix, 
			  output_suffix, 
			  avg_output_prefix, 
			  avg_output_suffix, 
			  average_items )
{
	kCPU_Number     = cpu_number; 
	fDataType       = dataType;
	kFirstReadOut   = true;
	kNumber_Of_CPUs   = 0;
	GetNumberOfCPUs();
}

MLUCValueMonitorCycles :: MLUCValueMonitorCycles ( const char* output_prefix, 
					     const char* output_suffix, 
					     const char* avg_output_prefix, 
					     const char* avg_output_suffix, 
					     unsigned int average_items,
					     MLUCValueMonitorCycles::kDataType dataType,
					     short cpu_number ) :
	
	MLUCValueMonitor( output_prefix, 
			  output_suffix, 
			  avg_output_prefix, 
			  avg_output_suffix, 
			  average_items )
{
	kCPU_Number = cpu_number;
	fDataType       = dataType;
	kFirstReadOut = true;
}

MLUCValueMonitorCycles :: MLUCValueMonitorCycles ( const char* filename, 
					     const char* fileDescrHeader, 
					     unsigned int average_items,
					     MLUCValueMonitorCycles::kDataType dataType,
					     short cpu_number ) : 

	MLUCValueMonitor( filename, 
			  fileDescrHeader, 
			  average_items )
{
	kCPU_Number = cpu_number;
	fDataType       = dataType;
	kFirstReadOut = true;
}

MLUCValueMonitorCycles :: MLUCValueMonitorCycles ( unsigned int average_items,
						   MLUCValueMonitorCycles::kDataType dataType,
						   short cpu_number ) : 
    
	MLUCValueMonitor( average_items )
{
	kCPU_Number = cpu_number;
	fDataType       = dataType;
	kFirstReadOut = true;
}

int MLUCValueMonitorCycles :: GetNumberOfCPUs() {
	
	char kLine[500]; // be on the safe side ;-)
	int  dummy;

	std :: ifstream in( "/proc/cpuinfo" );
	if( !in ) {
		LOG( MLUCLog::kError, "MLUCValueMonitorCycles:GetNumberOfCPUs", "Error opening file" )
			<< "Could not open /proc/cpuinfo" << ENDLOG;
		return ENOENT;
	} else {
		while( !in.eof() ) {
			in.getline( kLine, 500 );
			if( sscanf( kLine, "processor : %d ", &dummy) > 0 ) {
				kNumber_Of_CPUs++;
			}
		}
	}
	if( kCPU_Number > kNumber_Of_CPUs - 1  ) {
		LOG( MLUCLog::kWarning, "MLUCValueMonitorCycles:GetNumberOfCPUs", "Wrong CPU ID" )
			<< "Cannot monitor CPU " <<  kCPU_Number << ", since the system has only " 
			<< kNumber_Of_CPUs << " CPU(s). Will read out overall cpu usage." << ENDLOG;	

	}



	return 0;
}


int MLUCValueMonitorCycles :: GetValue( uint64& MeasuredValue ) {
	
	char   kLine[5000];    // intr line can be quite long
	
	std :: ifstream in( "/proc/stat" );
	if( !in ) {
		LOG( MLUCLog::kError, "MLUCValueMonitorCycles:GetValue", "Error opening file" )
			<< "Could not open /proc/stat" << ENDLOG;
		return ENOENT;
	} else {
		int no;
		unsigned long long used, irq, softirq, idle, sum;
		switch( kCPU_Number ) {
				
		case -1: // overall cpu usage
			while( !in.eof() ) {
				in.getline( kLine, 5000 );
				if( sscanf( kLine, "cycles %Lu %Lu %Lu %Lu %Lu", &used, &irq, &softirq, &idle, &sum ) == 5 ){
					if( kFirstReadOut ) {
						kUsedCyclesOld = used;
						kIRQCyclesOld = irq;
						kSoftIRQCyclesOld = softirq;
						kIdleCyclesOld = idle;
						kSumCyclesOld  = sum;

						kFirstReadOut = false;
						return ENOTSUP; // first readout (op not supported)
					} else {
						kUsedCyclesNew = used;
						kIRQCyclesNew = irq;
						kSoftIRQCyclesNew = softirq;
						kIdleCyclesNew = idle;
						kSumCyclesNew  = sum;
						break;
					}
				}
			}

		default: // look at cpu 0, 1, 2 or what ever you have ...
		    while( !in.eof() ) {
				in.getline( kLine, 5000 );
				if( sscanf( kLine,"cycles%d %Lu %Lu %Lu %Lu %Lu", &no, &used, &irq, &softirq, &idle, &sum ) == 6 ){
					if( no == kCPU_Number ) {
						if( kFirstReadOut ) {
							kUsedCyclesOld = used;
							kIRQCyclesOld = irq;
							kSoftIRQCyclesOld = softirq;
							kIdleCyclesOld = idle;
							kSumCyclesOld  = sum;
													
							kFirstReadOut = false;
							return ENOTSUP; // first readout (op not supported)
						} else {
							kUsedCyclesNew = used;
							kIRQCyclesNew = irq;
							kSoftIRQCyclesNew = softirq;
							kIdleCyclesNew = idle;
							kSumCyclesNew  = sum;

							break;
							
							//CalculateValue( MeasuredValue );
// 							MeasuredValue = ( ( kUsedCyclesNew+kIRQCyclesNew+kSoftIRQCyclesNew - 
// 									    (kUsedCyclesOld+kIRQCyclesOld+kSoftIRQCyclesOld) ) * 100 ) / ( kSumCyclesNew - kSumCyclesOld );
						}
					}
				}
			}
// 		    kUsedCyclesOld = kUsedCyclesNew;
// 		    kIRQCyclesOld = kIRQCyclesNew;
// 		    kSoftIRQCyclesOld = kSoftIRQCyclesNew;
// 		    kIdleCyclesOld = kIdleCyclesNew;
// 		    kSumCyclesOld  = kSumCyclesNew;
		    
		    break;
		}
	}

	//CalculateValue( MeasuredValue );
	switch ( fDataType )
	    {
	    case USER:
		if ( kSumCyclesNew != kSumCyclesOld )
		    MeasuredValue = ( ( kUsedCyclesNew - 
					(kUsedCyclesOld) ) * 100 ) / ( kSumCyclesNew - kSumCyclesOld );
		else
		    MeasuredValue = ~(unsigned long long)0;
		break;
	    case INTR:
		if ( kSumCyclesNew != kSumCyclesOld )
		    MeasuredValue = ( ( kIRQCyclesNew - 
					(kIRQCyclesOld) ) * 100 ) / ( kSumCyclesNew - kSumCyclesOld );
		else
		    MeasuredValue = ~(unsigned long long)0;
		break;
	    case SOFTIRQ:
		if ( kSumCyclesNew != kSumCyclesOld )
		    MeasuredValue = ( ( kSoftIRQCyclesNew - 
					(kSoftIRQCyclesOld) ) * 100 ) / ( kSumCyclesNew - kSumCyclesOld );
		else
		    MeasuredValue = ~(unsigned long long)0;
		break;
	    case IDLE:
		if ( kSumCyclesNew != kSumCyclesOld )
		    MeasuredValue = ( ( (kSumCyclesNew-kUsedCyclesNew-kIRQCyclesNew-kSoftIRQCyclesNew) - 
					(kSumCyclesOld-kUsedCyclesOld-kIRQCyclesOld-kSoftIRQCyclesOld) ) * 100 ) / ( kSumCyclesNew - kSumCyclesOld );
		else
		    MeasuredValue = ~(unsigned long long)0;
		break;
	    case USED:
		if ( kSumCyclesNew != kSumCyclesOld )
		    MeasuredValue = ( ( kUsedCyclesNew+kIRQCyclesNew+kSoftIRQCyclesNew - 
					(kUsedCyclesOld+kIRQCyclesOld+kSoftIRQCyclesOld) ) * 100 ) / ( kSumCyclesNew - kSumCyclesOld );
		else
		    MeasuredValue = ~(unsigned long long)0;
		break;
	    }

	kUsedCyclesOld = kUsedCyclesNew;
	kIRQCyclesOld = kIRQCyclesNew;
	kSoftIRQCyclesOld = kSoftIRQCyclesNew;
	kIdleCyclesOld = kIdleCyclesNew;
	kSumCyclesOld  = kSumCyclesNew;
						
	return 0;
}
	
