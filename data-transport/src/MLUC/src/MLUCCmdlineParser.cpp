/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCCmdlineParser.hpp"

bool MLUCCmdlineParser::ParseCmd( const char* cmd, vector<MLUCString>& cmdTokens )
    {
    const char* iter;
    bool backslash=false;
    bool quoted = false;
    bool wasSpace = false;
    char quotedchar = 0;
    MLUCString tok;

    iter = cmd;
    while ( *iter != 0 )
	{
	switch ( *iter )
	    {
	    case '\\':
		if ( backslash )
		    tok += *iter;
		backslash=!backslash;
		wasSpace = false;
		break;
	    case '\"':
	    case '\'':
// 	    case '�':
// 	    case '`':
		if ( !backslash && quoted && *iter==quotedchar )
		    {
		    //printf( "Going unquoted: '%c' - '%c'\n", quotedchar, *iter );
		    quoted = false;
		    quotedchar = 0;
		    }
		else if ( !backslash && !quoted )
		    {
		    quoted = true;
		    quotedchar = *iter;
		    //printf( "Going quoted: '%c'\n", quotedchar );
		    }
		else if ( backslash || quoted )
		    tok += *iter;
		backslash=false;
		wasSpace = false;
		break;
	    case ' ':
	    case '\t':
	    case '\n':
		if ( !backslash && !quoted && !wasSpace)
		    {
		    wasSpace = true;
		    cmdTokens.insert( cmdTokens.end(), tok );
		    tok = "";
		    }
		else if ( (backslash || quoted) )
		    {
		    tok += *iter;
		    wasSpace = false;
		    }
		else
		    wasSpace = true;
		backslash = false;
		break;
	    default:
		tok += *iter;
		backslash = false;
		wasSpace = false;
		break;
	    }
	iter++;
	}
    if ( backslash || quoted )
	return false;
    if ( tok.Length()>0 )
	cmdTokens.insert( cmdTokens.end(), tok );
    return true;
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
