#include <string>
#if __GNUC__ ==2
#include <strstream>
#endif
#include <sstream>
#include <fstream>
#include <stdio.h>

#include "MLUCValueMonitorContextSwitches.hpp"

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

void MLUCValueMonitorContextSwitches::Init()
{
  fFirstReadOut = true;
}

// -----------------------------------------------------------------------------

int MLUCValueMonitorContextSwitches::GetValue( uint64& MeasuredValue )
{
  unsigned val;

  std::ifstream fin( "/proc/stat" );
  if (!fin) {
    LOG( MLUCLog::kError, "MLUCValueMonitorContextSwitches:GetValue", 
	 "Error opening file" ) << "Could not open /proc/stat" << ENDLOG;
    return ENOENT;
  }
  
  string line;
  while (getline(fin, line)) {
#if __GNUC__>=3
    istringstream s(line.c_str());
#else
    istrstream s(line.c_str(), line.size());
#endif
    string token;
    s >> token;
    if (token == "ctxt") {
      if(!(s >> val)) {
	return EINVAL;
      }
    }
  }

  if(fFirstReadOut) {
    fOldContextSwitchVal = val;
    fFirstReadOut = false;

    return ENOTSUP; // first readout (op not supported)
  }

  MeasuredValue = val - fOldContextSwitchVal;
  fOldContextSwitchVal = val;

  return 0;
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
