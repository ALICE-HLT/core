/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCDynamicAllocCache.hpp"
#include <string.h>
// #include "MLUCLog.hpp"

#if 0
#define DEBUGMEMORYLEAK
#endif

MLUCDynamicAllocCache::MLUCDynamicAllocCache( unsigned cntExp2, bool grow, bool thorough ):
    fThorough( thorough ), fGrow( grow ), fPtr2IndexMap( 10, (cntExp2 >= sizeof(unsigned int)*8) ? 1 : (cntExp2<1 ? 1 : cntExp2), grow )
    {
    pthread_mutex_init( &fMutex, NULL );
    if ( (cntExp2 >= sizeof(unsigned int)*8) || cntExp2<1 )
	cntExp2 = 1;
    fUnusedCnt = fOrigDataSize = fDataSize = 1 << cntExp2;
    fUsedCnt = 0;
    fMask = fDataSize-1;
    fLastNdx = 0;
    fLastReleaseNdx = 0;
    fTooLargeShift = 3;
    fPtrs = new TPtrData[ fDataSize ];
    fPtr2IndexMap.SetPreDivisor( 16 );
    }

MLUCDynamicAllocCache::~MLUCDynamicAllocCache()
    {
#ifdef PROFILE_INDEXED_VECTOR
    LOG( MLUCLog::kImportant, "MLUCDynamicAllocCache", "Profile Data" )
	<< "MLUCIndexedVector for MLUCDynamicAllocCache 0x" << MLUCLog::kHex << (unsigned long long)this
	<< " is at 0x" << (unsigned long long)&fPtr2IndexMap << ENDLOG;
#endif
#ifndef DEBUGMEMORYLEAK
    if ( fPtrs )
	{
	for ( unsigned long ii=0; ii<fDataSize; ii++ )
	    {
	    if ( fPtrs[ii].fPtr )
		{
		delete [] fPtrs[ii].fPtr;
		fPtrs[ii].fPtr = NULL;
		}
	    }
	delete [] fPtrs;
	}
    pthread_mutex_destroy( &fMutex );
#endif
    }


uint8* MLUCDynamicAllocCache::Get( unsigned long size, bool thorough )
    {
    if ( !size )
	return NULL;
#ifdef DEBUGMEMORYLEAK
    return new uint8[ size ];
#else
    pthread_mutex_lock( &fMutex );
    TPtrData* tmp;
    TPtr ptr = NULL;
    unsigned long smallestNdx = ~(unsigned long)0;
    unsigned long smallestSize = 0;
    unsigned long firstEmptyNdx = ~(unsigned long)0;
    bool emptyFound = false;
    unsigned long firstUnusedNdx = ~(unsigned long)0;
    unsigned long firstUnusedSize = 0;
    unsigned long firstMatchNdx = ~(unsigned long)0;
    unsigned long firstMatchSize = 0;
    unsigned long ndx = fLastNdx;
    bool found=false;
    unsigned long usedCnt=0;
    unsigned long usableCnt=0;
    unsigned long foundNdx=~0UL;

    
    do
	{
	tmp = fPtrs+ndx;
	if ( tmp->fUsed )
	    usedCnt++;
	else
	    usableCnt++;

	if ( !emptyFound && !tmp->fPtr )
	    {
	    emptyFound = true;
	    firstEmptyNdx = ndx;
	    }
	if ( !firstUnusedSize && !tmp->fUsed && tmp->fPtr )
	    {
	    firstUnusedSize = tmp->fSize;
	    firstUnusedNdx = ndx;
	    }
	if ( !firstMatchSize && !tmp->fUsed && tmp->fPtr && tmp->fSize>=size )
	    {
	    firstMatchSize = tmp->fSize;
	    firstMatchNdx = ndx;
	    if ( !thorough )
		{
		found = true;
		foundNdx = ndx;
		break;
		}
	    }
	if ( thorough && !tmp->fUsed && tmp->fSize>=size && (!smallestSize || tmp->fSize<smallestSize) )
	    {
	    smallestSize = tmp->fSize;
	    smallestNdx = ndx;
	    if ( tmp->fSize==size )
		{
		found = true;
		foundNdx = ndx;
		break;
		}
	    }
	    

	ndx = (ndx+1) & fMask;
	}
    //while ( usedCnt<fUsedCnt && usableCnt<1 && ndx!=fLastNdx );
    while ( ((thorough && usedCnt<fUsedCnt) || usableCnt<1) && ndx!=fLastNdx );

    if ( !found && thorough && smallestSize )
	{
	found = true;
	foundNdx = smallestNdx;
	}

    if ( !found && firstMatchSize )
	{
	found = true;
	foundNdx = firstMatchNdx;
	}
    if ( !found && firstUnusedSize )
	{
	found = true;
	foundNdx = firstUnusedNdx;
	}
    if ( !found && emptyFound )
	{
	found = true;
	foundNdx = firstEmptyNdx;
	}

    if ( !found && fGrow )
	{
	unsigned long newSize = fDataSize << 1;
	TPtrData* newPtrs = new TPtrData[ newSize ];
	if ( newPtrs )
	    {
	    memcpy( newPtrs, fPtrs, fDataSize*sizeof(TPtrData) );
	    delete [] fPtrs;
	    fPtrs = newPtrs;
	    foundNdx = fDataSize; // field at beginning of new part is certainly available.
	    fUnusedCnt += fDataSize;
	    fDataSize = newSize;
	    fMask = fDataSize-1;
	    found = true;
	    }
	}

    if ( found )
	{
	tmp = fPtrs+foundNdx;
	TPtr tmpP;
	if ( !tmp->fPtr )
	    {
	    tmp->fPtr = new uint8[ size ];
	    if ( tmp->fPtr )
		{
		tmp->fSize = size;
		ptr = tmp->fPtr;
		}
	    }
	else if ( (tmp->fSize > (size<<fTooLargeShift)) || tmp->fSize<size )
	    {
	    tmpP = new uint8[ size ];
	    if ( tmpP )
		{
		delete [] tmp->fPtr;
		tmp->fPtr = tmpP;
		tmp->fSize = size;
		ptr = tmp->fPtr;
		}
	    else if ( tmp->fSize<size )
		ptr = NULL;
	    }
	else
	    ptr = tmp->fPtr;
	}

    if ( ptr )
	{
	tmp->fUsed = true;
	fUsedCnt++;
	fUnusedCnt--;
	fLastNdx = foundNdx;
	fPtr2IndexMap.Add( foundNdx, reinterpret_cast<unsigned long>(ptr) );
	}

    pthread_mutex_unlock( &fMutex );
    return ptr;
#endif
    }


void MLUCDynamicAllocCache::Release( uint8* ptr )
    {
    if ( !ptr )
	return;
#ifdef DEBUGMEMORYLEAK
    delete [] ptr;
#else
    pthread_mutex_lock( &fMutex );

    unsigned long mapIndex;
    if ( fPtr2IndexMap.FindElement( reinterpret_cast<unsigned long>(ptr), mapIndex ) )
	{
	unsigned long listIndex = fPtr2IndexMap.Get( mapIndex );
	if ( (fPtrs+listIndex)->fPtr==ptr && (fPtrs+listIndex)->fUsed )
	    {
	    (fPtrs+listIndex)->fUsed = false;
	    fUnusedCnt++;
	    fUsedCnt--;
	    fLastReleaseNdx = (listIndex+1) & fMask;
	    }
	
	fPtr2IndexMap.Remove( mapIndex );
	}
    else
	{
	unsigned long ndxF = fLastReleaseNdx;
	unsigned long ndxB = (fLastReleaseNdx-1) & fMask;
	unsigned long stopNdx = (fLastReleaseNdx+(fDataSize>>1)) & fMask;
	do
	    {
	    if ( (fPtrs+ndxF)->fPtr==ptr && (fPtrs+ndxF)->fUsed )
		{
		(fPtrs+ndxF)->fUsed = false;
		fUnusedCnt++;
		fUsedCnt--;
		fLastReleaseNdx = (ndxF+1) & fMask;
		break;
		}
	    if ( (fPtrs+ndxB)->fPtr==ptr && (fPtrs+ndxB)->fUsed )
		{
		(fPtrs+ndxB)->fUsed = false;
		fUnusedCnt++;
		fUsedCnt--;
		fLastReleaseNdx = (ndxB-1) & fMask;
		break;
		}
	    ndxF = (ndxF+1) & fMask;
	    ndxB = (ndxB-1) & fMask;
	    }
	while ( ndxF != stopNdx );
	}

    if ( fGrow && (fUsedCnt < (fDataSize>>2)) && (fDataSize>fOrigDataSize) )
	{
	Shrink();
	}

    pthread_mutex_unlock( &fMutex );
#endif
    }


void MLUCDynamicAllocCache::Shrink()
    {
    TPtrData* tmp;
    unsigned long newSize = fDataSize >> 1;
    TPtrData* newPtrs = new TPtrData[ newSize ];
    if ( newPtrs )
	{
	unsigned long i;
	unsigned long unusedCnt=0;
	TPtrData* dest = newPtrs;
	unsigned long destIndex = 0;
	fPtr2IndexMap.Clear();
	for ( i = 0; i < fDataSize; i++ )
	    {
	    tmp = fPtrs+i;
	    if ( tmp->fUsed )
		{
		fPtr2IndexMap.Add( destIndex, reinterpret_cast<unsigned long>(tmp->fPtr) );
		*dest++ = *tmp;
		++destIndex;
		}
	    else
		{
		if ( fUsedCnt+unusedCnt<newSize )
		    {
		    *dest++ = *tmp;
		    ++destIndex;
		    unusedCnt++;
		    }
		else if ( tmp->fPtr )
		    {
		    delete [] tmp->fPtr;
		    tmp->fPtr = NULL;
		    }
		}
	    }
	delete [] fPtrs;
	fPtrs = newPtrs;
	//fUnusedCnt -= fDataSize;
	fUnusedCnt = unusedCnt;
	fDataSize = newSize;
	fMask = fDataSize-1;
	fLastReleaseNdx = fLastNdx = 0;
	}
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
