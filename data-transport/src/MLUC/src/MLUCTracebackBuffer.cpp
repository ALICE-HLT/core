/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCTracebackBuffer.hpp"

MLUCTracebackBuffer::MLUCTracebackBuffer():
    fEntries( 10 )
    {
    }

MLUCTracebackBuffer::~MLUCTracebackBuffer()
    {
    Clear();
    }

void MLUCTracebackBuffer::AddEntry( const char* file, int lineNr, const char* identifier, const char* value )
    {
    Entry entry;
    entry.fType = kString;
    unsigned long len = strlen(value )+1;
    entry.fValue.fString = new char[ len ];
    if ( !entry.fValue.fString )
	{
	LOG( MLUCLog::kError, "MLUCTracebackBuffer::AddEntry", "Out of memory" )
	    << "Out of memory allocating char array of " << MLUCLog::kDec
	    << len << " bytes." << ENDLOG;
	return;
	}
    strcpy( entry.fValue.fString, value );
    entry.fFile = file;
    entry.fLine = lineNr;
    entry.fIdentifier = identifier;
    fEntries.Add( entry );
    }
void MLUCTracebackBuffer::AddEntry( const char* file, int lineNr, const char* identifier, signed char value )
    {
    Entry entry;
    entry.fType = kSChar;
    entry.fValue.fSChar = value;
    entry.fFile = file;
    entry.fLine = lineNr;
    entry.fIdentifier = identifier;
    fEntries.Add( entry );
    }

void MLUCTracebackBuffer::AddEntry( const char* file, int lineNr, const char* identifier, unsigned char value )
    {
    Entry entry;
    entry.fType = kUChar;
    entry.fValue.fUChar = value;
    entry.fFile = file;
    entry.fLine = lineNr;
    entry.fIdentifier = identifier;
    fEntries.Add( entry );
    }

void MLUCTracebackBuffer::AddEntry( const char* file, int lineNr, const char* identifier, signed short value )
    {
    Entry entry;
    entry.fType = kSShort;
    entry.fValue.fSShort = value;
    entry.fFile = file;
    entry.fLine = lineNr;
    entry.fIdentifier = identifier;
    fEntries.Add( entry );
    }

void MLUCTracebackBuffer::AddEntry( const char* file, int lineNr, const char* identifier, unsigned short value )
    {
    Entry entry;
    entry.fType = kUShort;
    entry.fValue.fUShort = value;
    entry.fFile = file;
    entry.fLine = lineNr;
    entry.fIdentifier = identifier;
    fEntries.Add( entry );
    }

void MLUCTracebackBuffer::AddEntry( const char* file, int lineNr, const char* identifier, signed int value )
    {
    Entry entry;
    entry.fType = kSInt;
    entry.fValue.fSInt = value;
    entry.fFile = file;
    entry.fLine = lineNr;
    entry.fIdentifier = identifier;
    fEntries.Add( entry );
    }

void MLUCTracebackBuffer::AddEntry( const char* file, int lineNr, const char* identifier, unsigned int value )
    {
    Entry entry;
    entry.fType = kUInt;
    entry.fValue.fUInt = value;
    entry.fFile = file;
    entry.fLine = lineNr;
    entry.fIdentifier = identifier;
    fEntries.Add( entry );
    }

void MLUCTracebackBuffer::AddEntry( const char* file, int lineNr, const char* identifier, signed long value )
    {
    Entry entry;
    entry.fType = kSLong;
    entry.fValue.fSLong = value;
    entry.fFile = file;
    entry.fLine = lineNr;
    entry.fIdentifier = identifier;
    fEntries.Add( entry );
    }

void MLUCTracebackBuffer::AddEntry( const char* file, int lineNr, const char* identifier, unsigned long value )
    {
    Entry entry;
    entry.fType = kULong;
    entry.fValue.fULong = value;
    entry.fFile = file;
    entry.fLine = lineNr;
    entry.fIdentifier = identifier;
    fEntries.Add( entry );
    }

void MLUCTracebackBuffer::AddEntry( const char* file, int lineNr, const char* identifier, signed long long value )
    {
    Entry entry;
    entry.fType = kSLongLong;
    entry.fValue.fSLongLong = value;
    entry.fFile = file;
    entry.fLine = lineNr;
    entry.fIdentifier = identifier;
    fEntries.Add( entry );
    }

void MLUCTracebackBuffer::AddEntry( const char* file, int lineNr, const char* identifier, unsigned long long value )
    {
    Entry entry;
    entry.fType = kULongLong;
    entry.fValue.fULongLong = value;
    entry.fFile = file;
    entry.fLine = lineNr;
    entry.fIdentifier = identifier;
    fEntries.Add( entry );
    }

void MLUCTracebackBuffer::AddEntry( const char* file, int lineNr, const char* identifier, float value )
    {
    Entry entry;
    entry.fType = kFloat;
    entry.fValue.fFloat = value;
    entry.fFile = file;
    entry.fLine = lineNr;
    entry.fIdentifier = identifier;
    fEntries.Add( entry );
    }

void MLUCTracebackBuffer::AddEntry( const char* file, int lineNr, const char* identifier, double value )
    {
    Entry entry;
    entry.fType = kDouble;
    entry.fValue.fDouble = value;
    entry.fFile = file;
    entry.fLine = lineNr;
    entry.fIdentifier = identifier;
    fEntries.Add( entry );
    }

#if 0
void MLUCTracebackBuffer::AddEntry( const char* file, int lineNr, const char* identifier, long double value )
    {
    Entry entry;
    entry.fType = kLongDouble;
    entry.fValue.fLongDouble = value;
    entry.fFile = file;
    entry.fLine = lineNr;
    entry.fIdentifier = identifier;
    fEntries.Add( entry );
    }
#endif

void MLUCTracebackBuffer::AddEntry( const char* file, int lineNr, const char* identifier, bool value )
    {
    Entry entry;
    entry.fType = kBool;
    entry.fValue.fBool = value;
    entry.fFile = file;
    entry.fLine = lineNr;
    entry.fIdentifier = identifier;
    fEntries.Add( entry );
    }

void MLUCTracebackBuffer::AddEntry( const char* file, int lineNr, const char* identifier, const void* value )
    {
    Entry entry;
    entry.fType = kPtr;
    entry.fValue.fPtr = value;
    entry.fFile = file;
    entry.fLine = lineNr;
    entry.fIdentifier = identifier;
    fEntries.Add( entry );
    }

void MLUCTracebackBuffer::AddArrayEntry( const char* file, int lineNr, const char* identifier, uint8* value, unsigned long len )
    {
    if ( !value )
	{
	LOG( MLUCLog::kError, "MLUCTracebackBuffer::AddArrayEntry", "NULL pointer" )
	    << "Array pointer is NULL." << ENDLOG;
	return;
	}
    Entry entry;
    entry.fType = kArray;
    entry.fValue.fArray.fPtr = new uint8[ len ];
    if ( !entry.fValue.fArray.fPtr )
	{
	LOG( MLUCLog::kError, "MLUCTracebackBuffer::AddArrayEntry", "Out of memory" )
	    << "Out of memory allocating byte array of " << MLUCLog::kDec
	    << len << " bytes." << ENDLOG;
	return;
	}
    entry.fValue.fArray.fLength = len;
    memcpy( entry.fValue.fArray.fPtr, value, len );
    entry.fFile = file;
    entry.fLine = lineNr;
    entry.fIdentifier = identifier;
    fEntries.Add( entry );
    }

void MLUCTracebackBuffer::AddFixedArrayEntry( const char* file, int lineNr, const char* identifier, uint8* value, unsigned long len )
    {
    if ( len > FIXEDARRAYLENGTH )
	len = FIXEDARRAYLENGTH;
    if ( !value )
	{
	LOG( MLUCLog::kError, "MLUCTracebackBuffer::AddFixedArrayEntry", "NULL pointer" )
	    << "Array pointer is NULL." << ENDLOG;
	return;
	}
    Entry entry;
    entry.fType = kFixedArray;
    memcpy( entry.fValue.fFixedArray.fData, value, len );
    entry.fFile = file;
    entry.fLine = lineNr;
    entry.fIdentifier = identifier;
    fEntries.Add( entry );
    }


void MLUCTracebackBuffer::Clear()
    {
    Entry empty;
    while ( fEntries.GetCnt()>0 )
	{
	if ( fEntries.GetFirstPtr()->fType == kString && fEntries.GetFirstPtr()->fValue.fString )
	    delete [] fEntries.GetFirstPtr()->fValue.fString;
	if ( fEntries.GetFirstPtr()->fType == kArray && fEntries.GetFirstPtr()->fValue.fArray.fPtr )
	    delete [] fEntries.GetFirstPtr()->fValue.fArray.fPtr;
	*(fEntries.GetFirstPtr()) = empty;
	fEntries.RemoveFirst();
	}
    }

void MLUCTracebackBuffer::DumpToLog( MLUCLog::TLogLevel loglevel, const char* keyword, const char* msgPrefix )
    {
    LogData logData( loglevel, keyword, msgPrefix );

    fEntries.Iterate( EntryLogIterationFunc, &logData );
    }

void MLUCTracebackBuffer::EntryLogIterationFunc( const Entry& el, void* arg )
    {
    switch ( el.fType )
	{
	case kUndefined:
	    LOG( ((LogData*)arg)->fLoglevel, "MLUCTracebackBuffer::DumpToLog", ((LogData*)arg)->fKeyword )
		<< ((LogData*)arg)->fMsgPrefix << ": Undefined value  @ ["
		<< el.fFile << "/" << MLUCLog::kDec << el.fLine << "]."
		<< ENDLOG;
	    break;
	case kString:
	    LOG( ((LogData*)arg)->fLoglevel, "MLUCTracebackBuffer::DumpToLog", ((LogData*)arg)->fKeyword )
		<< ((LogData*)arg)->fMsgPrefix << ": string " << el.fIdentifier << " @ ["
		<< el.fFile << "/" << MLUCLog::kDec << el.fLine << "]: "
		<< el.fValue.fString << "." << ENDLOG;
	    break;
	case kSChar:
	    LOG( ((LogData*)arg)->fLoglevel, "MLUCTracebackBuffer::DumpToLog", ((LogData*)arg)->fKeyword )
		<< ((LogData*)arg)->fMsgPrefix << ": signed char " << el.fIdentifier << " @ ["
		<< el.fFile << "/" << MLUCLog::kDec << el.fLine << "]: "
		<< MLUCLog::kDec << el.fValue.fSChar << " / 0x" << MLUCLog::kHex
		<< el.fValue.fSChar << "." << ENDLOG;
	    break;
	case kUChar:
	    LOG( ((LogData*)arg)->fLoglevel, "MLUCTracebackBuffer::DumpToLog", ((LogData*)arg)->fKeyword )
		<< ((LogData*)arg)->fMsgPrefix << ": unsigned char " << el.fIdentifier << " @ ["
		<< el.fFile << "/" << MLUCLog::kDec << el.fLine << "]: "
		<< MLUCLog::kDec << el.fValue.fUChar << " / 0x" << MLUCLog::kHex
		<< el.fValue.fUChar << "." << ENDLOG;
	    break;
	case kSShort:
	    LOG( ((LogData*)arg)->fLoglevel, "MLUCTracebackBuffer::DumpToLog", ((LogData*)arg)->fKeyword )
		<< ((LogData*)arg)->fMsgPrefix << ": signed short " << el.fIdentifier << " @ ["
		<< el.fFile << "/" << MLUCLog::kDec << el.fLine << "]: "
		<< MLUCLog::kDec << el.fValue.fSShort << " / 0x" << MLUCLog::kHex
		<< el.fValue.fSShort << "." << ENDLOG;
	    break;
	case kUShort:
	    LOG( ((LogData*)arg)->fLoglevel, "MLUCTracebackBuffer::DumpToLog", ((LogData*)arg)->fKeyword )
		<< ((LogData*)arg)->fMsgPrefix << ": unsigned short " << el.fIdentifier << " @ ["
		<< el.fFile << "/" << MLUCLog::kDec << el.fLine << "]: "
		<< MLUCLog::kDec << el.fValue.fUShort << " / 0x" << MLUCLog::kHex
		<< el.fValue.fUShort << "." << ENDLOG;
	    break;
	case kSInt:
	    LOG( ((LogData*)arg)->fLoglevel, "MLUCTracebackBuffer::DumpToLog", ((LogData*)arg)->fKeyword )
		<< ((LogData*)arg)->fMsgPrefix << ": signed int " << el.fIdentifier << " @ ["
		<< el.fFile << "/" << MLUCLog::kDec << el.fLine << "]: "
		<< MLUCLog::kDec << el.fValue.fSInt << " / 0x" << MLUCLog::kHex
		<< el.fValue.fSInt << "." << ENDLOG;
	    break;
	case kUInt:
	    LOG( ((LogData*)arg)->fLoglevel, "MLUCTracebackBuffer::DumpToLog", ((LogData*)arg)->fKeyword )
		<< ((LogData*)arg)->fMsgPrefix << ": unsigned int " << el.fIdentifier << " @ ["
		<< el.fFile << "/" << MLUCLog::kDec << el.fLine << "]: "
		<< MLUCLog::kDec << el.fValue.fUInt << " / 0x" << MLUCLog::kHex
		<< el.fValue.fUInt << "." << ENDLOG;
	    break;
	case kSLong:
	    LOG( ((LogData*)arg)->fLoglevel, "MLUCTracebackBuffer::DumpToLog", ((LogData*)arg)->fKeyword )
		<< ((LogData*)arg)->fMsgPrefix << ": signed long " << el.fIdentifier << " @ ["
		<< el.fFile << "/" << MLUCLog::kDec << el.fLine << "]: "
		<< MLUCLog::kDec << el.fValue.fSLong << " / 0x" << MLUCLog::kHex
		<< el.fValue.fSLong << "." << ENDLOG;
	    break;
	case kULong:
	    LOG( ((LogData*)arg)->fLoglevel, "MLUCTracebackBuffer::DumpToLog", ((LogData*)arg)->fKeyword )
		<< ((LogData*)arg)->fMsgPrefix << ": unsigned long " << el.fIdentifier << " @ ["
		<< el.fFile << "/" << MLUCLog::kDec << el.fLine << "]: "
		<< MLUCLog::kDec << el.fValue.fULong << " / 0x" << MLUCLog::kHex
		<< el.fValue.fULong << "." << ENDLOG;
	    break;
	case kSLongLong:
	    LOG( ((LogData*)arg)->fLoglevel, "MLUCTracebackBuffer::DumpToLog", ((LogData*)arg)->fKeyword )
		<< ((LogData*)arg)->fMsgPrefix << ": signed long long " << el.fIdentifier << " @ ["
		<< el.fFile << "/" << MLUCLog::kDec << el.fLine << "]: "
		<< MLUCLog::kDec << el.fValue.fSLongLong << " / 0x" << MLUCLog::kHex
		<< el.fValue.fSLongLong << "." << ENDLOG;
	    break;
	case kULongLong:
	    LOG( ((LogData*)arg)->fLoglevel, "MLUCTracebackBuffer::DumpToLog", ((LogData*)arg)->fKeyword )
		<< ((LogData*)arg)->fMsgPrefix << ": unsigned long long " << el.fIdentifier << " @ ["
		<< el.fFile << "/" << MLUCLog::kDec << el.fLine << "]: "
		<< MLUCLog::kDec << el.fValue.fULongLong << " / 0x" << MLUCLog::kHex
		<< el.fValue.fULongLong << "." << ENDLOG;
	    break;
	case kFloat:
	    LOG( ((LogData*)arg)->fLoglevel, "MLUCTracebackBuffer::DumpToLog", ((LogData*)arg)->fKeyword )
		<< ((LogData*)arg)->fMsgPrefix << ": float " << el.fIdentifier << " @ ["
		<< el.fFile << "/" << MLUCLog::kDec << el.fLine << "]: "
		<< el.fValue.fFloat << "." << ENDLOG;
	    break;
	case kDouble:
	    LOG( ((LogData*)arg)->fLoglevel, "MLUCTracebackBuffer::DumpToLog", ((LogData*)arg)->fKeyword )
		<< ((LogData*)arg)->fMsgPrefix << ": double " << el.fIdentifier << " @ ["
		<< el.fFile << "/" << MLUCLog::kDec << el.fLine << "]: "
		<< el.fValue.fDouble << "." << ENDLOG;
	    break;
#if 0
	case kLongDouble:
	    LOG( ((LogData*)arg)->fLoglevel, "MLUCTracebackBuffer::DumpToLog", ((LogData*)arg)->fKeyword )
		<< ((LogData*)arg)->fMsgPrefix << ": long double " << el.fIdentifier << " @ ["
		<< el.fFile << "/" << MLUCLog::kDec << el.fLine << "]: "
		<< el.fValue.fLongDouble << "." << ENDLOG;
	    break;
#else
	case kLongDouble:
	    LOG( ((LogData*)arg)->fLoglevel, "MLUCTracebackBuffer::DumpToLog", ((LogData*)arg)->fKeyword )
		<< ((LogData*)arg)->fMsgPrefix << ": long double " << el.fIdentifier << " @ ["
		<< el.fFile << "/" << MLUCLog::kDec << el.fLine << "]: "
		<< "long double values currently not handled. We are sorry, please try again later..." << ENDLOG;
	    break;
#endif
	case kBool:
	    LOG( ((LogData*)arg)->fLoglevel, "MLUCTracebackBuffer::DumpToLog", ((LogData*)arg)->fKeyword )
		<< ((LogData*)arg)->fMsgPrefix << ": bool " << el.fIdentifier << " @ ["
		<< el.fFile << "/" << MLUCLog::kDec << el.fLine << "]: "
		<< ( el.fValue.fBool ? "true" : "false" ) << "." << ENDLOG;
	    break;
	case kPtr:
	    LOG( ((LogData*)arg)->fLoglevel, "MLUCTracebackBuffer::DumpToLog", ((LogData*)arg)->fKeyword )
		<< ((LogData*)arg)->fMsgPrefix << ": void* " << el.fIdentifier << " @ ["
		<< el.fFile << "/" << MLUCLog::kDec << el.fLine << "]: 0x"
		<< MLUCLog::kHex << (unsigned long)el.fValue.fPtr << " / " << MLUCLog::kDec
		<< (unsigned long)el.fValue.fPtr << "." << ENDLOG;
	    break;
	case kArray:
	    {
	    MLUCString dump;
	    char tmp[ 128];
// 	    LOG( ((LogData*)arg)->fLoglevel, "MLUCTracebackBuffer::DumpToLog", ((LogData*)arg)->fKeyword )
// 		<< ((LogData*)arg)->fMsgPrefix << ": " << MLUCLog::kDec << el.fValue.fArray.fLength
// 		<< " Byte Array Pointer: " << el.fIdentifier << " @ ["
// 		<< el.fFile << "/" << MLUCLog::kDec << el.fLine << "]: 0x"
// 		<< MLUCLog::kHex << (unsigned long)el.fValue.fArray.fPtr << " / " << MLUCLog::kDec
// 		<< (unsigned long)el.fValue.fArray.fPtr << "." << ENDLOG;
	    for ( unsigned long i = 0; i < el.fValue.fArray.fLength; i++ )
		{
		sprintf( tmp, "0x%02X", (unsigned)el.fValue.fArray.fPtr[i] );
		dump += tmp;
		if ( i<el.fValue.fArray.fLength-1 )
		    dump += " ";
		}
	    LOG( ((LogData*)arg)->fLoglevel, "MLUCTracebackBuffer::DumpToLog", ((LogData*)arg)->fKeyword )
		<< ((LogData*)arg)->fMsgPrefix << ": " << MLUCLog::kDec << el.fValue.fArray.fLength
		<< " Byte Array " << el.fIdentifier << " @ ["
		<< el.fFile << "/" << MLUCLog::kDec << el.fLine << "]: " << dump.c_str() << "." << ENDLOG;
	    break;
	    }
	case kFixedArray:
	    {
	    MLUCString dump;
	    char tmp[ 128];
	    for ( unsigned long i = 0; i < FIXEDARRAYLENGTH; i++ )
		{
		sprintf( tmp, "0x%02X", (unsigned)el.fValue.fFixedArray.fData[i] );
		dump += tmp;
		if ( i<FIXEDARRAYLENGTH-1 )
		    dump += " ";
		}
	    LOG( ((LogData*)arg)->fLoglevel, "MLUCTracebackBuffer::DumpToLog", ((LogData*)arg)->fKeyword )
		<< ((LogData*)arg)->fMsgPrefix << ": " << MLUCLog::kDec << FIXEDARRAYLENGTH
		<< " Byte Fixed Array " << el.fIdentifier << " @ ["
		<< el.fFile << "/" << MLUCLog::kDec << el.fLine << "]: " << dump.c_str() << "." << ENDLOG;
	    break;
	    }
	}
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
