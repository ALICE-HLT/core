/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCLog.hpp"
#include "MLUCLogServer.hpp"
#include "MLUCLog.h"
#include <cstdarg>
#include <cstdlib>



//MLUCLog::TLogLevel MLUCLog::gLogLevel = MLUCLog::kDebug;
//MLUCLog::TLogLevel& gLogLevel = MLUCLog::gLogLevel;
int MLUCLog::gLogLevel = MLUCLog::kAll;
int& gLogLevel = MLUCLog::gLogLevel;
int* gLogLevelP = &MLUCLog::gLogLevel;

MLUCLog MLUCLog::gLog;
MLUCLog& gLog = MLUCLog::gLog;
MLUCLog* gLogP = &MLUCLog::gLog;

#ifdef __NO_INLINE__
const unsigned MLUCLog::gkStackTraceSkipLevels=3;
#else
const unsigned MLUCLog::gkStackTraceSkipLevels=1;
#endif


MLUCLog::MLUCLog():
    fStackTraceLRUList(1024)
	{
	pthread_mutexattr_t ma;
	pthread_mutexattr_init( &ma );
	pthread_mutexattr_settype( &ma, PTHREAD_MUTEX_RECURSIVE_NP );
	pthread_mutex_init( &fMutex, &ma );
	fFormat = kHex;
	fPrec = 0;
	fSequence=0;
	fServers = new MLUCLogServer*[1]; // Optimist, assuming this will not fail; if this fails, any recovery is probably in vain anyway...
	fServers[0]=NULL;
	fStackTraceComparisonDepth = 0;
	fStackTraceComparisonBuffer = NULL;
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
	fRunNumber = 0;
#endif
	fStackTraceReductionMinTimeLimit_us = 5000000;
	fStackTraceReductionStart = 4;
	fStackTraceReductionIncrement = 10;
	}


MLUCLog::~MLUCLog()
	{
	if ( fServers )
	    delete [] fServers;
	fServers = NULL;
	if ( fStackTraceComparisonBuffer )
	    delete [] fStackTraceComparisonBuffer;
	fStackTraceComparisonBuffer = NULL;
	pthread_mutex_destroy( &fMutex );
	}


void MLUCLog::AddServer( MLUCLogServer* server )
	{
	//fServers.insert( fServers.end(), server );
	// A bit more complicated in order to keep fServers always
	// in a consistent state if currently running thread is interrupted
	// in this method.
	MLUCLogServer** oldServers=NULL;
	MLUCTHREAD_PUSH_CLEANUP_FUNC( MLUCThread::MutexUnlockCleanup, &fMutex );
	pthread_mutex_lock( &fMutex );
	oldServers = fServers;
	unsigned cnt=0;
	while ( fServers && fServers[cnt]!=NULL )
	    cnt++;
	MLUCLogServer** newServers = new MLUCLogServer*[cnt+2];
	memcpy( newServers, oldServers, cnt*sizeof(MLUCLogServer*) );
	newServers[cnt] = server;
	newServers[cnt+1] = NULL;
	fServers = newServers;
	//pthread_mutex_unlock( &fMutex );
	MLUCTHREAD_POP_CLEANUP_FUNC( true );
	if ( oldServers )
	    delete [] oldServers;
	}

void MLUCLog::DelServer( MLUCLogServer* server )
	{
	MLUCTHREAD_PUSH_CLEANUP_FUNC( MLUCThread::MutexUnlockCleanup, &fMutex );
	pthread_mutex_lock( &fMutex );
	MLUCLogServer** oldServers = fServers;
	unsigned cnt=0;
	while ( fServers && fServers[cnt]!=NULL )
	    cnt++;
	MLUCLogServer** newServers = new MLUCLogServer*[cnt]; // cnt+1 used currently, one will be removed
	unsigned long si=0, di=0;
	while ( fServers && fServers[si]!=NULL )
	    {
	    if ( fServers[si]==server )
		{
		++si;
		}
	    else
		{
		newServers[di]=fServers[si];
		++si;
		++di;
		}
	    }
	if ( si > di )
	    {
	    // Removal took place
	    newServers[di]=NULL; // Since we did not copy one element, we have one slot left at the end for the NULL delimiter
	    fServers = newServers;
	    delete [] oldServers;
	    }
	else
	    delete [] newServers;
	MLUCTHREAD_POP_CLEANUP_FUNC( true );
	//pthread_mutex_unlock( &fMutex );

// 	vector<MLUCLogServer*>::iterator iter = fServers.begin();
// 	while ( iter!=fServers.end() )
// 		{
// 		if ( *iter==server )
// 			{
// 			fServers.erase( iter );
// 			return;
// 			}
// 		iter++;
// 		}
	}


void MLUCLog::FlushServers( const char* origin, const char* keyword, const char* file, int linenr, 
			    const char* compile_date, const char* compile_time, MLUCLog::TLogLevel logLevel, 
			    const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
			    unsigned long runNumber, 
#endif
			    uint32 ldate, 
			    uint32 ltime_s, uint32 ltime_us, uint32 msgNr, bool isMod, uint32 subMsgNr, 
			    const char* line )
    {
    MLUCTHREAD_PUSH_CLEANUP_FUNC( MLUCThread::MutexUnlockCleanup, &fMutex );
    pthread_mutex_lock( &fMutex );
    bool suppress = false;
    unsigned long long suppressedCnt=0;
    MLUCString suppressedMsg;

    if ( fStackTraceComparisonDepth>0 && fStackTraceComparisonBuffer )
        {
        int count = backtrace( fStackTraceComparisonBuffer, gkStackTraceSkipLevels+fStackTraceComparisonDepth );
        for ( unsigned ii=count;ii<gkStackTraceSkipLevels+fStackTraceComparisonDepth; ii++ )
            fStackTraceComparisonBuffer[ii] = NULL;
        memcpy( fStackTraceComparisonElement.fStackTrace, fStackTraceComparisonBuffer+gkStackTraceSkipLevels, fStackTraceComparisonDepth*sizeof(void*) );
        MLUCLRUList<TStackTraceElement>::TIterator iter;
        iter = fStackTraceLRUList.Update( fStackTraceComparisonElement );
        struct timeval now;
        gettimeofday( &now, NULL );
        unsigned long long tdiff = ((unsigned long long)(now.tv_sec-iter->fLastOccurance.tv_sec))*1000000 + (unsigned long long)(now.tv_usec-iter->fLastOccurance.tv_usec);
        iter->fLastOccurance = now;
        if ( tdiff < fStackTraceReductionMinTimeLimit_us )
            {
            if ( iter->fCnt > fStackTraceReductionStart )
                {
                unsigned long long fact = fStackTraceReductionIncrement;
                while ( (iter->fCnt >= fact*fStackTraceReductionIncrement) && (fact*fStackTraceReductionIncrement>fact) ) // Take care of overflow to avoid endless loop, just in case....
                    fact *= fStackTraceReductionIncrement;
                if ( fact!=iter->fCnt )
                    suppress = true;
                }
            iter->fCnt++;
            }
        else
            {
            iter->fCnt = 0;
            iter->fLastLogCnt = 0;
            }
        if ( !suppress )
            {
            suppressedCnt = iter->fCnt-iter->fLastLogCnt;
            iter->fLastLogCnt = iter->fCnt;
            if ( suppressedCnt>1 )
                {
                char tmp[256];
                snprintf( tmp, 256, "%Lu", suppressedCnt-1 );
                suppressedMsg = tmp;
                suppressedMsg += " similar messages suppressed before the last message (";
                suppressedMsg += line;
                suppressedMsg += ")";
                }
            }
        }
   
    unsigned cnt=0;
    if ( !isMod )
	{
	while ( fServers && fServers[cnt]!=NULL )
	    {
        if ( !suppress )
            {
            fServers[cnt]->LogLine( origin, keyword, file, linenr, compile_date,
                                    compile_time, logLevel, hostname, id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
                                    runNumber,
#endif
                                    ldate, ltime_s, ltime_us, msgNr, line );
            if ( suppressedCnt>1 )
                {
                fServers[cnt]->LogLine( origin, keyword, file, linenr, compile_date,
                                        compile_time, logLevel, hostname, id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
                                        runNumber,
#endif
                                        ldate, ltime_s, ltime_us, msgNr, suppressedMsg.c_str() );
                }
            }
	    cnt++;
	    }
	}
    else
	{
	while ( fServers[cnt]!=NULL )
	    {
        if ( !suppress )
            {
            fServers[cnt]->LogLine( origin, keyword, file, linenr, compile_date,
                                    compile_time, logLevel, hostname, id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
                                    runNumber,
#endif
                                    ldate, ltime_s, ltime_us, msgNr, subMsgNr, line );
            if ( suppressedCnt>1 )
                {
                fServers[cnt]->LogLine( origin, keyword, file, linenr, compile_date,
                                        compile_time, logLevel, hostname, id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
                                        runNumber,
#endif
                                        ldate, ltime_s, ltime_us, msgNr, subMsgNr, suppressedMsg.c_str() );
                }
            }
	    cnt++;
	    }
	}
    MLUCTHREAD_POP_CLEANUP_FUNC( true );
    //pthread_mutex_unlock( &fMutex );
    }


bool MLUCLog::SetStackTraceComparisonDepth( unsigned long depth )
    {
    pthread_mutex_lock( &fMutex );
    if ( fStackTraceComparisonDepth==depth )
        {
        pthread_mutex_unlock( &fMutex );
        return true;
        }
    void** tmp = NULL;
    if ( depth )
        tmp = new void*[depth+gkStackTraceSkipLevels];
    if ( tmp || !depth )
        {
        void** tmp1 = NULL;
        if ( depth )
            tmp1= new void*[depth];
        if ( tmp1 || !depth )
            {
            if ( tmp1 )
                memset( tmp1, 0, sizeof(void*)*depth );
            MLUCLRUList<TStackTraceElement>::TIterator iter, end;
            iter = fStackTraceLRUList.Begin();
            end = fStackTraceLRUList.End();
            std::vector<void**> elemTraces;
            while ( iter != end )
                {
                void** tmp2 = NULL;
                if ( depth )
                    tmp2 = new void*[depth];
                if ( !tmp2 && depth )
                    {
                    while ( !elemTraces.empty() )
                        {
                        if ( *(elemTraces.begin()) )
                            delete [] *(elemTraces.begin());
                        elemTraces.erase( elemTraces.begin() );
                        }
                    if ( tmp1 )
                        delete [] tmp1;
                    if ( tmp )
                        delete [] tmp;
                    pthread_mutex_unlock( &fMutex );
                    return false;
                    }
                memset( tmp2, 0, sizeof(void*)*depth );
                elemTraces.push_back( tmp2 );
                ++iter;
                }
            if ( fStackTraceComparisonBuffer )
                delete [] fStackTraceComparisonBuffer;
            fStackTraceComparisonBuffer = tmp;
            fStackTraceComparisonDepth = depth;
            if ( fStackTraceComparisonElement.fStackTrace )
                delete [] fStackTraceComparisonElement.fStackTrace;
            fStackTraceComparisonElement.fStackTrace = tmp1;
            fStackTraceComparisonElement.fStackTraceDepth = depth;
            iter = fStackTraceLRUList.Begin();
            end = fStackTraceLRUList.End();
            while ( iter != end )
                {
                unsigned long cnt=iter->fStackTraceDepth<depth ? iter->fStackTraceDepth : depth;
                if ( *(elemTraces.begin()) && iter->fStackTrace )
                    memcpy( *(elemTraces.begin()), iter->fStackTrace, cnt*sizeof(void*) );
                if ( iter->fStackTrace )
                    delete [] iter->fStackTrace;
                iter->fStackTrace = *(elemTraces.begin());
                elemTraces.erase( elemTraces.begin() );
                ++iter;
                }
            }
        else
            {
            if ( tmp )
                delete [] tmp;
            }
        }
    bool success = fStackTraceComparisonDepth==depth;
    pthread_mutex_unlock( &fMutex );
    return success;
    }



/* ---------------------------------------- **
** And now the C support/wrapper functions. **
** ---------------------------------------- */

unsigned gCLogLevel = 0;

void LogMsg( unsigned logLvl, const char* origin, const char* keyword, const char* file, int line, 
	     const char* compile_date, const char* compile_time, char* fmt, ... )
    {

    MLUCLog::TLogLevel lvl=(MLUCLog::TLogLevel)0;
    unsigned i=1;
    while ( i>0 )
	{
	switch ( logLvl & i )
	    {
	    case MLUCLOG_LVL_NONE:
		lvl = (MLUCLog::TLogLevel)( (unsigned)lvl | MLUCLog::kNone );
		break;
	    case MLUCLOG_LVL_BENCHMARK:
		lvl = (MLUCLog::TLogLevel)( (unsigned)lvl | MLUCLog::kBenchmark );
		break;
	    case MLUCLOG_LVL_DEBUG:
		lvl = (MLUCLog::TLogLevel)( (unsigned)lvl | MLUCLog::kDebug );
		break;
	    case MLUCLOG_LVL_INFO:
		lvl = (MLUCLog::TLogLevel)( (unsigned)lvl | MLUCLog::kInformational );
		break;
	    case MLUCLOG_LVL_WARNING:
		lvl = (MLUCLog::TLogLevel)( (unsigned)lvl | MLUCLog::kWarning );
		break;
	    case MLUCLOG_LVL_ERROR:
		lvl = (MLUCLog::TLogLevel)( (unsigned)lvl | MLUCLog::kError );
		break;
	    case MLUCLOG_LVL_FATAL:
		lvl = (MLUCLog::TLogLevel)( (unsigned)lvl | MLUCLog::kFatal );
		break;
	    case MLUCLOG_LVL_IMPORTANT:
		lvl = (MLUCLog::TLogLevel)( (unsigned)lvl | MLUCLog::kImportant );
		break;
	    case MLUCLOG_LVL_PRIMARY:
		lvl = (MLUCLog::TLogLevel)( (unsigned)lvl | MLUCLog::kPrimary );
		break;
	    default:
		break;
	    }
	i <<= 1;
	}

    /* Guess we need no more than 1024 bytes. */
    int n, size = 1024;
    char *p;
    va_list ap;
    p = (char*)malloc (size);
    if ( !p )
	{
	LOG( MLUCLog::kFatal, "LogMsg", "Internal - Out of memory" )
	    << "Out of memory trying to allocate C message buffer of "
	    << MLUCLog::kDec << size << " bytes." << ENDLOG;
	return;
	}
    while (1)
	{
	/* Try to print in the allocated space. */
	va_start(ap, fmt);
	n = vsnprintf (p, size, fmt, ap);
	va_end(ap);
	/* If that worked, return the string. */
	if (n > -1 && n < size)
	    break;
	/* Else try again with more space. */
	if (n > -1)    /* glibc 2.1 */
	    size = n+1; /* precisely what is needed */
	else           /* glibc 2.0 */
	    size *= 2;  /* twice the old size */
	char* p_tmp = (char*)realloc (p, size);
	if ( !p_tmp )
	    {
	    LOG( MLUCLog::kFatal, "LogMsg", "Internal - Out of memory" )
		<< "Out of memory trying to allocate C message buffer of "
		<< MLUCLog::kDec << size << " bytes." << ENDLOG;
	    //have to free original prt if realloc call failed
	    free(p);
	    return;
	    }
	p=p_tmp;
	}
    MLUCTHREAD_PUSH_CLEANUP_FUNC( MLUCThread::MutexUnlockCleanup, &gLog.fMutex );
    gLog.LogMsg( lvl, origin, keyword, file, line, 
		 compile_date, compile_time ) << p << (MLUCLog::kEnd);
    MLUCTHREAD_POP_CLEANUP_FUNC( true );

    }

static MLUCFilteredStdoutLogServer* gCStdoutLogServer = NULL;
static MLUCFilteredStderrLogServer* gCStderrLogServer = NULL;
static MLUCFileLogServer* gCFileLogServer = NULL;


void SetLogOutput( unsigned type, const void* param1, const void* param2 )
    {
    switch ( type )
	{
	case MLUCLOG_OUT_STDOUT:
	    if ( !gCStdoutLogServer )
		{
		gCStdoutLogServer = new MLUCFilteredStdoutLogServer;
		gLog.AddServer( gCStdoutLogServer );
		}
	    break;
	case MLUCLOG_OUT_STDERR:
	    if ( !gCStderrLogServer )
		{
		gCStderrLogServer = new MLUCFilteredStderrLogServer;
		gLog.AddServer( gCStderrLogServer );
		}
	    break;
	case MLUCLOG_OUT_FILE:
	    if ( !gCFileLogServer )
		{
		if ( param2 )
		    gCFileLogServer = new MLUCFileLogServer( (const char*)param2, 
							     (const char*)param1, ".log", 
							     10000 );
		else
		    gCFileLogServer = new MLUCFileLogServer( (const char*)param1, ".log", 
							     10000 );
		gLog.AddServer( gCFileLogServer );
		}
	    break;
	}
    }

void RemoveLogOutput( unsigned type )
    {
    switch ( type )
	{
	case MLUCLOG_OUT_STDOUT:
	    if ( gCStdoutLogServer )
		{
		gLog.DelServer( gCStdoutLogServer );
		delete gCStdoutLogServer;
		gCStdoutLogServer = NULL;
		}
	    break;
	case MLUCLOG_OUT_STDERR:
	    if ( gCStderrLogServer )
		{
		gLog.DelServer( gCStderrLogServer );
		delete gCStderrLogServer;
		gCStderrLogServer = NULL;
		}
	    break;
	case MLUCLOG_OUT_FILE:
	    if ( gCFileLogServer )
		{
		gLog.DelServer( gCFileLogServer );
		delete gCFileLogServer;
		gCFileLogServer = NULL;
		}
	    break;
	}
    }

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
