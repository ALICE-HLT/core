/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCFifo.hpp"
#ifdef PARANOIA
#include "MLUCLog.hpp"
#endif
#include <errno.h>


MLUCFifo::MLUCFifo( unsigned initialSizeExp2, unsigned initialEmergencySizeExp2, bool grow ):
    fGrow( grow )
#ifdef PARANOID_LOCKING
    ,
    fMutexLocker( fMutex, false )
#endif
    {
    fBuffer.fBufferOrigSize = 1 << initialSizeExp2;
    fBuffer.fBufferSize = fBuffer.fBufferOrigSize;
    fEmergency.fBufferOrigSize = 1 << initialEmergencySizeExp2;
    fEmergency.fBufferSize = fEmergency.fBufferOrigSize;
    fBuffer.fBuffer = (uint8*)malloc( fBuffer.fBufferSize );
    if ( !fBuffer.fBuffer )
	{
	fEmergency.fBuffer = NULL;
	return;
	}
    fEmergency.fBuffer = (uint8*)malloc( fEmergency.fBufferSize );
    if ( !fEmergency.fBuffer )
	{
	free( fBuffer.fBuffer );
	fBuffer.fBuffer = NULL;
	return;
	}
    fBuffer.fBufferRead = fBuffer.fBufferWrite = 0;
    fBuffer.fBufferReadWriteTmp = fBuffer.fBufferWriteReadTmp = 0;
    fBuffer.fMaxShrinkPart = 3;
    fBuffer.fWriteWrap = false;
#ifdef PARANOIA
    memset( fBuffer.fBuffer, 0, fBuffer.fBufferSize );
    fBuffer.fReserveBufferSize = fBuffer.fBufferSize;
    fBuffer.fReserveBufferRead = 0;
    fBuffer.fReserveBufferWrite = 0;
    fBuffer.fGrowCnt = fBuffer.fShrinkCnt = 0;
#endif
    pthread_mutex_init( &fBuffer.fRelocMutex, NULL );
    fBuffer.fSignal = &fSignal;
    fEmergency.fBufferRead = fEmergency.fBufferWrite = 0;
    fEmergency.fBufferReadWriteTmp = fEmergency.fBufferWriteReadTmp = 0;
    fEmergency.fMaxShrinkPart = 3;
    fEmergency.fWriteWrap = false;
#ifdef PARANOIA
    memset( fEmergency.fBuffer, 0, fEmergency.fBufferSize );
    fEmergency.fReserveBufferSize = fEmergency.fBufferSize;
    fEmergency.fReserveBufferRead = 0;
    fEmergency.fReserveBufferWrite = 0;
    fEmergency.fGrowCnt = fEmergency.fShrinkCnt = 0;
#endif
    pthread_mutex_init( &fEmergency.fRelocMutex, NULL );
    fEmergency.fSignal = &fSignal;
#ifdef INITIAL_CONDITIONSEM_UNLOCK
    // Define this when condition semaphore objects should always be unlocked initially.
    fSignal.Unlock();
#endif
    }

MLUCFifo::~MLUCFifo()
    {
    if ( fBuffer.fBuffer )
	free( fBuffer.fBuffer );
    if ( fEmergency.fBuffer )
	free( fEmergency.fBuffer );
    pthread_mutex_destroy( &fBuffer.fRelocMutex );
    pthread_mutex_destroy( &fEmergency.fRelocMutex );
    }

MLUCFifo::operator bool() const
    {
    return fBuffer.fBuffer!=NULL && fEmergency.fBuffer!=NULL;
    }

int MLUCFifo::Write( const MLUCFifoMsg* msg )
    {
#ifdef PARANOID_LOCKING
    MLUCMutex::TLocker locker( fMutex );
#endif
    return WriteTo( fBuffer, msg, fGrow );
    }

int MLUCFifo::ReserveMsgSlot( unsigned len, MLUCFifoMsg*& msgPtr )
    {
#ifdef PARANOID_LOCKING
    fMutexLocker.Lock();
#endif
    int ret = ReserveMsgSlotTo( fBuffer, len, msgPtr, fGrow );
#ifdef PARANOID_LOCKING
    if ( ret )
	fMutexLocker.Unlock();
#endif
    return ret;
    }

int MLUCFifo::CommitMsg( unsigned len )
    {
    int ret = CommitMsgTo( fBuffer, len, fGrow );
#ifdef PARANOID_LOCKING
    fMutexLocker.Unlock();
#endif
    return ret;
    }

int MLUCFifo::WaitForData()
    {
#ifdef PARANOID_LOCKING
    MLUCMutex::TLocker locker( fMutex );
#endif
    int ret;
    fSignal.Lock();
    if ( fEmergency.fBufferWrite != fEmergency.fBufferRead ||
	 fBuffer.fBufferWrite != fBuffer.fBufferRead )
	{
	fSignal.Unlock();
	return 0;
	}
#ifdef PARANOID_LOCKING
    locker.Unlock();
#endif
    fSignal.Wait();
#ifdef PARANOID_LOCKING
    locker.Lock();
#endif
    if ( fEmergency.fBufferWrite != fEmergency.fBufferRead ||
	 fBuffer.fBufferWrite != fBuffer.fBufferRead )
	ret = 0;
    else
	ret = ENXIO;
    fSignal.Unlock();
    return ret;
    }

int MLUCFifo::Read( MLUCFifoMsg*& msg )
    {
#ifdef PARANOID_LOCKING
    MLUCMutex::TLocker locker( fMutex );
#endif
    if ( fEmergency.fBufferWrite != fEmergency.fBufferRead )
	return ReadFrom( fEmergency, msg, fGrow );
    else
	return ReadFrom( fBuffer, msg, fGrow );
    }

int MLUCFifo::FreeMsg( MLUCFifoMsg* msg )
    {
    if ( !msg )
	return EFAULT;
    delete [] msg;
    return 0;
    }

int MLUCFifo::GetNextMsg( MLUCFifoMsg*& msg )
    {
#ifdef PARANOID_LOCKING
    fMutexLocker.Lock();
#endif
    if ( fEmergency.fBufferWrite != fEmergency.fBufferRead )
	{
	int ret = GetNextMsgFrom( fEmergency, msg, fGrow );
#ifdef PARANOID_LOCKING
	if ( ret )
	    fMutexLocker.Unlock();
#endif
	return ret;
	}
    else
	{
	int ret = GetNextMsgFrom( fBuffer, msg, fGrow );
#ifdef PARANOID_LOCKING
	if ( ret )
	    fMutexLocker.Unlock();
#endif
	return ret;
	}
    }

int MLUCFifo::FreeNextMsg( MLUCFifoMsg* msg )
    {
    if ( ((uint8*)msg)>=fEmergency.fBuffer && ((uint8*)msg)<fEmergency.fBuffer+fEmergency.fBufferSize )
	{
	int ret = FreeNextMsgFrom( fEmergency, msg, fGrow );
#ifdef PARANOID_LOCKING
	fMutexLocker.Unlock();
#endif
	return ret;
	}
    if ( ((uint8*)msg)>=fBuffer.fBuffer && ((uint8*)msg)<fBuffer.fBuffer+fBuffer.fBufferSize )
	{
	int ret = FreeNextMsgFrom( fBuffer, msg, fGrow );
#ifdef PARANOID_LOCKING
	fMutexLocker.Unlock();
#endif
	return ret;
	}
#ifdef PARANOID_LOCKING
    fMutexLocker.Unlock();
#endif
    return EBADMSG;
    }

int MLUCFifo::WriteEmergency( const MLUCFifoMsg* msg )
    {
#ifdef PARANOID_LOCKING
    MLUCMutex::TLocker locker( fMutex );
#endif
    return WriteTo( fEmergency, msg, fGrow );
    }


int MLUCFifo::Resize( TBufferData& buffer,bool grow )
    {
    uint8* tmpPtr;
    unsigned long newSize;
    int ret;
    if ( grow )
	{
	buffer.fSignal->Unlock();
	ret = pthread_mutex_lock( &buffer.fRelocMutex );
	if ( ret )
	    {
	    buffer.fSignal->Lock();
	    return ENXIO;
	    }
	buffer.fSignal->Lock();
	newSize = buffer.fBufferSize << 1;
	tmpPtr = (uint8*)malloc( newSize );
	if ( !tmpPtr )
	    {
	    //buffer.fSignal->Unlock();
	    ret = pthread_mutex_unlock( &buffer.fRelocMutex );
	    return ENOMEM;
	    }
	// Note: We double the size...
	if ( buffer.fBufferWrite<buffer.fBufferRead )
	    {
	    // The used area is split into two parts, one at the beginning
	    // and one at the end of the (old) array.
	    // In other words, the buffer has partly wrapped around.
	    // We copy the block at the end of the old buffer to the end of the new buffer...
	    memcpy( tmpPtr+newSize-(buffer.fBufferSize-buffer.fBufferRead), buffer.fBuffer+buffer.fBufferRead,
		    buffer.fBufferSize-buffer.fBufferRead );
	    // ... and the block at the beginning of the old buffer to the beginning of the new buffer.
	    memcpy( tmpPtr, buffer.fBuffer, buffer.fBufferWrite );
	    buffer.fBufferRead += buffer.fBufferSize;
	    buffer.fBufferReadWriteTmp = buffer.fBufferRead;
	    }
	else
	    {
	    memcpy( tmpPtr, buffer.fBuffer+buffer.fBufferRead, buffer.fBufferWrite-buffer.fBufferRead );
	    buffer.fBufferWrite -= buffer.fBufferRead;
	    buffer.fBufferReadWriteTmp = buffer.fBufferRead = 0;
	    }
	buffer.fBufferSize = newSize;
	free( buffer.fBuffer );
	buffer.fBuffer = tmpPtr;
	buffer.fGrowCnt++;
	//buffer.fSignal->Unlock();
	ret = pthread_mutex_unlock( &buffer.fRelocMutex );
	return 0;
	}
    else
	{
	buffer.fSignal->Lock();
	unsigned long usedSize;
	usedSize = GetUsedSpace( buffer, true );
	if ( usedSize >= buffer.fBufferSize>>buffer.fMaxShrinkPart || buffer.fBufferOrigSize>=buffer.fBufferSize )
	    {
	    buffer.fSignal->Unlock();
	    return 0;
	    }

	newSize = buffer.fBufferSize >> 1;
	tmpPtr = (uint8*)malloc( newSize );
	if ( !tmpPtr )
	    {
	    buffer.fSignal->Unlock();
	    return ENOMEM;
	    }
	if ( buffer.fBufferWrite<buffer.fBufferRead )
	    {
	    // We have the used area split into two parts, one at the beginning
	    // and one at the end of the (old) array.
	    // In other words, the buffer has partly wrapped around.
	    // Note: We half the size and use at most one quarter of the old buffer.
	    memcpy( tmpPtr, buffer.fBuffer, buffer.fBufferWrite );
	    memcpy( tmpPtr+newSize-(buffer.fBufferSize-buffer.fBufferRead), buffer.fBuffer+buffer.fBufferRead,
		    buffer.fBufferSize-buffer.fBufferRead );
	    buffer.fBufferRead -= newSize;
	    }
	else
	    {
	    memcpy( tmpPtr, buffer.fBuffer+buffer.fBufferRead,
		    buffer.fBufferWrite-buffer.fBufferRead );
	    buffer.fBufferWrite -= buffer.fBufferRead;
	    buffer.fBufferRead = 0;
	    }
	buffer.fBufferSize = newSize;
	free( buffer.fBuffer );
	buffer.fBuffer = tmpPtr;
	buffer.fShrinkCnt++;
	buffer.fSignal->Unlock();
	return 0;
	}
    }


int MLUCFifo::ReadFrom( TBufferData& buffer, MLUCFifoMsg*& msg, bool grow )
    {
    int ret, ret2;
    MLUCFifoMsg* tmpMsg;
    msg = NULL;
    ret = GetNextMsgFrom( buffer, tmpMsg, grow );
    if ( ret )
	return ret;
    unsigned len = tmpMsg->fLength;
    // Try to allocate space for the new message
    uint8* p = new uint8[ len ];
    msg = (MLUCFifoMsg*)p;
    if ( !p )
	{
	// Allcoation did not succeed
	ret = ENOMEM;
	}
    else
	{
	// Copy the message into the buffer
	memcpy( p, tmpMsg, len );
	}
    ret2 = FreeNextMsgFrom( buffer, msg, grow );
    if ( ret )
	return ret;
    return ret2;

//     int ret;
//     ret = pthread_mutex_lock( &buffer.fRelocMutex );
//     if ( ret )
// 	{
// 	return ENXIO;
// 	}
// #ifdef PARANOID
//     // Sanity check:
//     if ( buffer.fReadCnt > buffer.fWriteCnt || (buffer.fReadCnt==buffer.fWriteCnt && buffer.fBufferRead!=buffer.fBufferWrite) )
// 	{
// 	ret = pthread_mutex_unlock( &buffer.fRelocMutex );
// 	if ( ret )
// 	    {
// 	    return ENXIO;
// 	    }
// 	return EDOM;
// 	}
// #endif
    
//     if ( buffer.fBufferRead==buffer.fBufferWrite && buffer.fReadCnt>=buffer.fWriteCnt )
// 	{
// 	// Buffer is empty, no message available
// 	msg = NULL;
// 	ret = pthread_mutex_unlock( &buffer.fRelocMutex );
// 	if ( ret )
// 	    {
// 	    return ENXIO;
// 	    }
// 	return EAGAIN;
// 	}
    
//     // Determine how long the message to return is.
//     unsigned len = ((MLUCFifoMsg*)(buffer.fBuffer+buffer.fBufferRead))->fLength;

//     if ( buffer.fBufferRead+len >= buffer.fBufferSize )
// 	{
// 	// The message does not fit in the space between where we last left
// 	// of reading and the end of the buffer. So a wrap around must have 
// 	// occured when writing and we do the same.
// 	buffer.fBufferRead = 0;
// 	unsigned newLen = ((MLUCFifoMsg*)(buffer.fBuffer+buffer.fBufferRead))->fLength;
// 	if ( newLen != len )
// 	    {
// 	    // This is definitely and absolutely an internal error!!!
// 	    ret = pthread_mutex_unlock( &buffer.fRelocMutex );
// 	    if ( ret )
// 		{
// 		return ENXIO;
// 		}
// 	    return EDOM;
// 	    }
// 	}

//     // Try to allocate space for the new message
//     uint8* p = new uint8[ len ];
//     msg = (MLUCFifoMsg*)p;
//     if ( !p )
// 	{
// 	// Allcoation did not succeed
// 	ret = pthread_mutex_unlock( &buffer.fRelocMutex );
// 	if ( ret )
// 	    {
// 	    return ENXIO;
// 	    }
// 	return ENOMEM;
// 	}
    
//     // Copy the message into the buffer
//     memcpy( p, buffer.fBuffer+buffer.fBufferRead, len );
    
//     // Increment read pointer
//     buffer.fBufferRead += len;
//     if ( buffer.fBufferRead+sizeof(MLUCFifoMsg::fLength) > buffer.fBufferSize )
// 	{
// 	// We do not even have space between the read end and the end of 
// 	// the buffer to read a length indicator from there, so we wrap around.
// 	buffer.fBufferRead = 0;
// 	}
//     buffer.fReadCnt++;
    
//     if ( !fGrow )
// 	{
// 	ret = pthread_mutex_unlock( &buffer.fRelocMutex );
// 	if ( ret )
// 	    {
// 	    return ENXIO;
// 	    }
// 	return 0;
// 	}
    
//     // Now we check, wether we have to shrink again.
//     unsigned long free;
//     free = buffer.fBufferWrite - buffer.fBufferRead;
//     if ( free==0 && fWriteCnt > fReadCnt )
// 	free = 0;
//     else if ( buffer.fBufferWrite < buffer.fBufferRead )
// 	free += buffer.fBufferRead - buffer.fBufferWrite;
//     else if ( free>fBufferSize )
// 	free= 0;
//     if ( free < fBufferSize>>2 && buffer.fBufferOrigSize<buffer.fBufferSize )
// 	Resize( buffer, false );

//     ret = pthread_mutex_unlock( &buffer.fRelocMutex );
//     if ( ret )
// 	{
// 	return ENXIO;
// 	}
//     return 0;
    }


int MLUCFifo::WriteTo( TBufferData& buffer, const MLUCFifoMsg* msg, bool grow )
    {
    int ret;
    unsigned len = msg->fLength;
    MLUCFifoMsg* tmpMsg;
    ret = ReserveMsgSlotTo( buffer, len, tmpMsg, grow );
    if ( ret )
	return ret;
    memcpy( tmpMsg, msg, len );
    ret = CommitMsgTo( buffer, len, grow );
    return ret;

//     unsigned long free;
//     unsigned len = msg->fLength;
//     // Is there a message to write?
//     if ( len==0 )
// 	return EBADMSG;
//     // IS the message too large for the whole buffer?
//     if ( len > buffer.fBufferSize )
// 	return EBADMSG;
//     fSignal.Lock();
// #ifdef PARANOID
//     // Sanity check:
//     if ( buffer.fReadCnt > buffer.fWriteCnt || (buffer.fReadCnt==buffer.fWriteCnt && buffer.fBufferRead!=buffer.fBufferWrite) )
// 	{
// 	fSignal.Unlock();
// 	return EDOM;
// 	}
// #endif
//     free = 0;
//     if ( buffer.fBufferRead==buffer.fBufferWrite )
// 	{
// 	// Is the buffer all full...
// 	if ( buffer.fWriteCnt > buffer.fReadCnt )
// 	    {
// 	    if ( !fGrow || Resize( buffer, true ) )
// 		{
// 		fSignal.Unlock();
// 		return ENOSPC;
// 		}
// 	    free = 0;
// 	    }
// 	else
// 	    free = buffer.fBufferSize; // ... or all empty
// 	}
//     if ( !free )
// 	{
// 	// Determine the free space in the buffer (between write and read index)
// 	if ( buffer.fBufferWrite < buffer.fBufferRead )
// 	    // The write pointer wrapped around and the read pointer did not
// 	    free = buffer.fBufferRead-buffer.fBufferWrite;
// 	else
// 	    // Either both of the indices or neither wrapped around
// 	    free = buffer.fBufferRead+buffer.fBufferSize-buffer.fBufferWrite;
// 	}

// 	// Do we have enough free mem in the buffer?
//     if ( len > free )
// 	{
// 	// No we don't!
// 	if ( !fGrow || Resize( buffer, true ) )
// 	    {
// 	    fSignal.Unlock();
// 	    return ENOSPC;
// 	    }
// 	}

//     if ( buffer.fBufferWrite >= buffer.fBufferRead )
// 	{
// 	// A wrap around could occur, check if the message fits
// 	// completey in the space before the end.
// 	if ( buffer.fBufferSize <= len+buffer.fBufferWrite )
// 	    {
// 	    // We would wrap around in the message which we want to avoid.
// 	    // Check if we can fit the message in the space between the beginning
// 	    // and the read index.
// 	    if ( buffer.fBufferRead < len )
// 		{
// 		// We cannot fit in.
// 		if ( !fGrow || Resize( buffer, true ) )
// 		    {
// 		    fSignal.Unlock();
// 		    return ENOSPC;
// 		    }
// 		}
// 	    // We can actually fit in...
// 	    // ..., but first we have to write the length in the space before we wrap around...
// 	    MLUCFifoMsg* tmpMsg = (MLUCFifoMsg*)( buffer.fBuffer+buffer.fBufferWrite );
// 	    tmpMsg->fLength = len;
// 	    // ... and now we wrap around.
// 	    buffer.fBufferWrite = 0;
// 	    }
// 	}
//     // Copy the data into the buffer
//     memcpy( buffer.fBuffer+buffer.fBufferWrite, msg, len );
//     // Increment the write index
//     buffer.fBufferWrite += len;
//     if ( buffer.fBufferWrite+sizeof(MLUCFifoMsg::fLength) > buffer.fBufferSize )
// 	buffer.fBufferWrite = 0;
//     buffer.fWriteCnt++;
//     fSignal.Unlock();
//     fSignal.Signal();
//     return 0;
    }


int MLUCFifo::GetNextMsgFrom( TBufferData& buffer, MLUCFifoMsg*& msg, bool )
    {
    int ret;
    msg = NULL;
    ret = pthread_mutex_lock( &buffer.fRelocMutex );
    if ( ret )
	{
	return ENXIO;
	}
    buffer.fBufferWriteReadTmp = buffer.fBufferWrite;
    if ( buffer.fBufferRead==buffer.fBufferWriteReadTmp )
	{
	// Buffer is empty, no message available
	msg = NULL;
	ret = pthread_mutex_unlock( &buffer.fRelocMutex );
	return EAGAIN;
	}
    
    // Determine how long the message to return is.
    unsigned len;
#if __GNUC__>=3
    if ( buffer.fBufferRead+sizeof( ((MLUCFifoMsg*)0)->fLength ) > buffer.fBufferSize )
#else
    if ( buffer.fBufferRead+sizeof(MLUCFifoMsg::fLength) > buffer.fBufferSize )
#endif
	{
	buffer.fBufferRead = 0;
	len = ((MLUCFifoMsg*)(buffer.fBuffer))->fLength;
	}
    else
	{
	len = ((MLUCFifoMsg*)(buffer.fBuffer+buffer.fBufferRead))->fLength;
    
	if ( buffer.fBufferRead+len > buffer.fBufferSize )
	    {
	    // The message does not fit in the space between where we last left
	    // of reading and the end of the buffer. So a wrap around must have 
	    // occured when writing and we do the same.
	    buffer.fBufferRead = 0;
	    unsigned newLen = ((MLUCFifoMsg*)(buffer.fBuffer+buffer.fBufferRead))->fLength;
	    if ( newLen != len )
		{
		// This is definitely and absolutely an internal error!!!
		ret = pthread_mutex_unlock( &buffer.fRelocMutex );
		return EDOM;
		}
	    }
	}

    msg = (MLUCFifoMsg*)( buffer.fBuffer+buffer.fBufferRead );
    return 0;
    }
    
int MLUCFifo::FreeNextMsgFrom( TBufferData& buffer, MLUCFifoMsg* msg, bool grow )
    {
    int ret;
    
    if ( buffer.fBufferRead==buffer.fBufferWriteReadTmp )
	{
	// Buffer is empty, no message available
	msg = NULL;
	ret = pthread_mutex_unlock( &buffer.fRelocMutex );
	return EAGAIN;
	}
    
    // Determine how long the message to free is.
    // Note: The distinction of cases in getNextMsgFrom is not necessary here,
    // as buffer.fBufferRead has already been wrapped around in case of problems.
    unsigned len = ((MLUCFifoMsg*)(buffer.fBuffer+buffer.fBufferRead))->fLength;

    // Increment read pointer
    buffer.fBufferRead += len;
    
    if ( grow )
	{
	unsigned long used;
	used = GetUsedSpace( buffer, true );
	if ( used < buffer.fBufferSize>>buffer.fMaxShrinkPart && buffer.fBufferOrigSize<buffer.fBufferSize )
	    Resize( buffer, false );
	}

    ret = pthread_mutex_unlock( &buffer.fRelocMutex );
    return 0;
    }


int MLUCFifo::ReserveMsgSlotTo( TBufferData& buffer, unsigned len, MLUCFifoMsg*& msgPtr, bool grow )
    {
    unsigned long free_space, free1, free2;
    bool wrap;
    msgPtr = NULL;
    // Is there a message to write?
    if ( len==0 )
	return EBADMSG;
    // Is the message too large for the whole buffer?
    buffer.fSignal->Lock();
    buffer.fBufferReadWriteTmp = buffer.fBufferRead;
    while ( len > buffer.fBufferSize )
	{
	if ( !grow || Resize( buffer, true ) )
	    {
	    buffer.fSignal->Unlock();
	    return ENOSPC;
	    }
	}
    free_space = 0;
    while ( free_space < len )
	{
	GetUsableSpace( buffer, wrap, free1, free2 );
	free_space = free1;
	if ( wrap )
	    {
	    if ( free1 < len )
		free_space = free2;
	    }
	if ( free_space > buffer.fBufferSize )
	    {
	    LOG( MLUCLog::kFatal, "MLUCFifo::CommitMsgTo", "Internal Error" )
		<< "Internal error: free space > size: " << MLUCLog::kDec
		<< "free1: " << free1 << " - free2: " << free2
		<< " - wrap: " << (wrap ? "true" : "false")
		<< " - size: " << buffer.fBufferSize
		<< " - len: " << len << "." << ENDLOG;
	    DumpBufferData( buffer, MLUCLog::kFatal );
	    }
	if ( free_space < len )
	    {
	    if ( !grow || Resize( buffer, true ) )
		{
		buffer.fSignal->Unlock();
		return ENOSPC;
		}
	    }
	}

    if ( wrap && free1 < len )
	{
	// We can actually fit in, but only in the wrapped around part...
	buffer.fWriteWrap = true;
	msgPtr = (MLUCFifoMsg*)( buffer.fBuffer );
	}
    else
	{
	buffer.fWriteWrap = false;
	msgPtr = (MLUCFifoMsg*)( buffer.fBuffer+buffer.fBufferWrite );
	}

#ifdef PARANOIA
    buffer.fReserveBufferSize = buffer.fBufferSize;
    buffer.fReserveBufferRead = buffer.fBufferReadWriteTmp;
    buffer.fReserveBufferWrite = buffer.fBufferWrite;
#endif

    return 0;
    }

int MLUCFifo::CommitMsgTo( TBufferData& buffer, unsigned len, bool )
    {
    unsigned long free_space, free1, free2;
    bool wrap;
    // Is there a message to write?
    if ( len==0 )
	{
	buffer.fSignal->Unlock();
	return EBADMSG;
	}
    // Is the message too large for the whole buffer?
    if ( len > buffer.fBufferSize )
	{
#ifdef PARANOIA
	LOG( MLUCLog::kFatal, "MLUCFifo::CommitMsgTo", "Not enough space 1" )
	    << "Not enough space 1: len: " << MLUCLog::kDec << len << " - fBufferSize: " << buffer.fBufferSize
	    << " - fBufferReadWriteTmp: " << buffer.fBufferReadWriteTmp << " - fBufferWrite: " << buffer.fBufferWrite
	    << " ---- fReserveBufferSize: " << MLUCLog::kDec << buffer.fReserveBufferSize
	    << " - fReserveBufferRead: " << buffer.fReserveBufferRead << " - fReserveBufferWrite: " << buffer.fReserveBufferWrite
	    << "." << ENDLOG;
#endif
	buffer.fSignal->Unlock();
	return ENOSPC;
	}
    free_space = 0;
    GetUsableSpace( buffer, wrap, free1, free2 );
    // basically this should have been taken care of already,
    // so we don't go through any bells and whistles here.

    if ( wrap && free1<len && !buffer.fWriteWrap )
	{
	LOG( MLUCLog::kFatal, "MLUCFifo::CommitMsgTo", "Internal Error" )
	    << "Internal error: Found wrap around in CommitMsgTo: free1: " << MLUCLog::kDec << free1 << " - free2: " << free2 << " - len: " << len
	    << " - fBufferSize: " << MLUCLog::kDec << buffer.fBufferSize
	    << " - fBufferReadWriteTmp: " << buffer.fBufferReadWriteTmp << " - fBufferWrite: " << buffer.fBufferWrite
	    << " ---- fReserveBufferSize: " << MLUCLog::kDec << buffer.fReserveBufferSize
	    << " - fReserveBufferRead: " << buffer.fReserveBufferRead << " - fReserveBufferWrite: " << buffer.fReserveBufferWrite
	    << "." << ENDLOG;
	buffer.fSignal->Unlock();
	return ENXIO;
	}
    if ( buffer.fWriteWrap && !wrap )
	{
	LOG( MLUCLog::kFatal, "MLUCFifo::CommitMsgTo", "Internal Error" )
	    << "Internal error: Inconsistent wrap around data in CommitMsgTo: free1: " << MLUCLog::kDec << free1 << " - free2: " << free2 << " - len: " << len
	    << " - fBufferSize: " << MLUCLog::kDec << buffer.fBufferSize
	    << " - fBufferReadWriteTmp: " << buffer.fBufferReadWriteTmp << " - fBufferWrite: " << buffer.fBufferWrite
	    << " ---- fReserveBufferSize: " << MLUCLog::kDec << buffer.fReserveBufferSize
	    << " - fReserveBufferRead: " << buffer.fReserveBufferRead << " - fReserveBufferWrite: " << buffer.fReserveBufferWrite
	    << "." << ENDLOG;
	buffer.fSignal->Unlock();
	return ENXIO;
	}
    if ( buffer.fWriteWrap )
	free_space = free2;
    else
	free_space = free1;
    if ( free_space < len )
	{
#ifdef PARANOIA
	LOG( MLUCLog::kFatal, "MLUCFifo::CommitMsgTo", "Not enough space 2" )
	    << "Not enough space 2: free1: " << MLUCLog::kDec << free1 << " - free2: " << free2 << " - len: " << len
	    << " - fBufferSize: " << MLUCLog::kDec << buffer.fBufferSize
	    << " - fBufferReadWriteTmp: " << buffer.fBufferReadWriteTmp << " - fBufferWrite: " << buffer.fBufferWrite
	    << " ---- fReserveBufferSize: " << MLUCLog::kDec << buffer.fReserveBufferSize
	    << " - fReserveBufferRead: " << buffer.fReserveBufferRead << " - fReserveBufferWrite: " << buffer.fReserveBufferWrite
	    << "." << ENDLOG;
#endif
	buffer.fSignal->Unlock();
	return ENOSPC;
	}
    // Increment the write index
    if ( buffer.fWriteWrap )
	{
	// properly do the wrap around:
	// We write the length of the message we could not fit in at the end 
	// at the beginning of the free area at the end, if the free area is even big
	// enough for this.
#if __GNUC__>=3
	if ( free1 >= sizeof( ((MLUCFifoMsg*)0)->fLength ) )
#else
	if ( free1 >= sizeof(MLUCFifoMsg::fLength) )
#endif
	    ((MLUCFifoMsg*)( buffer.fBuffer+buffer.fBufferWrite ))->fLength = len;
	buffer.fBufferWrite = len;
	}
    else
	buffer.fBufferWrite += len;
    buffer.fSignal->Unlock();
    buffer.fSignal->Signal();
    return 0;
    }


void MLUCFifo::GetUsableSpace( TBufferData& buffer, bool& wrap, unsigned long& part1, unsigned long& part2, bool update )
    {
    if ( update )
	buffer.fBufferReadWriteTmp = buffer.fBufferRead;
    unsigned long tmpR = buffer.fBufferReadWriteTmp;
    if ( tmpR == buffer.fBufferWrite )
	{
	if ( tmpR==0 )
	    {
	    wrap = false;
	    if ( buffer.fBufferSize>0 )
		part1 = buffer.fBufferSize-1;
	    else
		part1 = 0;
	    part2 = 0;
	    }
	else
	    {
	    wrap = true;
	    part1 = buffer.fBufferSize-tmpR;
	    if ( tmpR>0 )
		part2 = tmpR-1;
	    else
		{
		part2=~(unsigned long)0;
		part1=~(unsigned long)0;
		/*part2 = 0;
		  part1--;*/
		}
	    }
	return;
	}

    if ( buffer.fBufferWrite < tmpR )
	{
	wrap = false;
	part1 = tmpR-buffer.fBufferWrite-1;
	part2 = 0;
	}
    else
	{
	wrap = true;
	part1 = buffer.fBufferSize-buffer.fBufferWrite;
	if ( tmpR>0 )
	    part2 = tmpR-1;
	else
	    {
	    part2 = 0;
	    part1--;
	    }
	}
    }

unsigned long MLUCFifo::GetUsedSpace( TBufferData& buffer, bool update )
    {
    if ( update )
	buffer.fBufferWriteReadTmp = buffer.fBufferWrite;
    unsigned long tmpW = buffer.fBufferWriteReadTmp;
    if ( tmpW == buffer.fBufferRead )
	    return buffer.fBufferSize;
    
    if ( buffer.fBufferRead < tmpW )
	return tmpW-buffer.fBufferRead;
    else
	return tmpW+buffer.fBufferSize-buffer.fBufferRead;
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
