/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCThread.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

pthread_mutex_t MLUCThread::fgThreadDirMutex = PTHREAD_MUTEX_INITIALIZER;
std::list<pthread_t> MLUCThread::fgThreadDir;


MLUCThread::MLUCThread()
#if 0
:
    fFifo( 1024, 256 )
#endif
    {
    pthread_attr_init( &fThreadAttrs );
    fRunning = false;
    fStarted = false;
    }

#if 0
MLUCThread::MLUCThread( uint32 bufferSize, uint32 emergencySize ):
    fFifo( bufferSize, emergencySize )
    {
    pthread_attr_init( &fThreadAttrs );
    }
#endif

MLUCThread::~MLUCThread()
    {
    pthread_attr_destroy( &fThreadAttrs );
    }


MLUCThread::TState MLUCThread::Start()
    {
    pthread_mutex_lock( &fgThreadDirMutex );
    if ( fgThreadDir.empty() )
	fgThreadDir.insert( fgThreadDir.begin(), pthread_self() );
    pthread_mutex_unlock( &fgThreadDirMutex );
    int ret;
    //ret = pthread_create( &fThread, &fThreadAttrs, ThreadStart, this );
    ret = pthread_create( &fThread, NULL, ThreadStart, this );
    fStarted = !ret;
    if ( fStarted )
	{
	pthread_mutex_lock( &fgThreadDirMutex );
	fgThreadDir.insert( fgThreadDir.end(), fThread );
	pthread_mutex_unlock( &fgThreadDirMutex );
	}
    return !ret ? kStarted : kFailed;
    }


MLUCThread::TState MLUCThread::Abort()
    {
    int ret;
    ret = pthread_cancel( fThread );
    fStarted = false;
    pthread_mutex_lock( &fgThreadDirMutex );
    std::list<pthread_t>::iterator iter = fgThreadDir.begin();
    std::list<pthread_t>::iterator end = fgThreadDir.end();
    while ( iter != end )
	{
	if ( *iter==fThread )
	    {
	    fgThreadDir.erase( iter );
	    break;
	    }
	++iter;
	}
    pthread_mutex_unlock( &fgThreadDirMutex );
    return !ret ? kAborted : kFailed;
    }


MLUCThread::TState MLUCThread::QuickRestart()
    {
    return kFailed;
    }

MLUCThread::TState MLUCThread::Join()
    {
    if ( !fStarted || pthread_join( fThread, NULL ) )
	return kFailed;
    fStarted = false;
    pthread_mutex_lock( &fgThreadDirMutex );
    std::list<pthread_t>::iterator iter = fgThreadDir.begin();
    std::list<pthread_t>::iterator end = fgThreadDir.end();
    while ( iter != end )
	{
	if ( *iter==fThread )
	    {
	    fgThreadDir.erase( iter );
	    break;
	    }
	++iter;
	}
    pthread_mutex_unlock( &fgThreadDirMutex );
    return kJoined;
    }

MLUCThread::TState MLUCThread::Nice()
    {
    return kFailed;
    }

void MLUCThread::MutexUnlockCleanup( void* mutex )
    {
    pthread_mutex_unlock( (pthread_mutex_t*)mutex );
    }


void* MLUCThread::ThreadStart( void* obj )
    {
    if ( !obj )
	return NULL;
    MLUCThread* thr = (MLUCThread*)obj;
    thr->fRunning = true;
    thr->Run();
    thr->fRunning = false;
    pthread_mutex_lock( &fgThreadDirMutex );
    std::list<pthread_t>::iterator iter = fgThreadDir.begin();
    std::list<pthread_t>::iterator end = fgThreadDir.end();
    while ( iter != end )
	{
	if ( *iter==thr->fThread )
	    {
	    fgThreadDir.erase( iter );
	    break;
	    }
	++iter;
	}
    pthread_mutex_unlock( &fgThreadDirMutex );
    pthread_exit( NULL );
    //return NULL;
    }


void MLUCThread::Kill( int signal, bool excludeThisThread )
    {
    pthread_mutex_lock( &fgThreadDirMutex );
    std::list<pthread_t> threads( fgThreadDir );
    pthread_mutex_unlock( &fgThreadDirMutex );
    pthread_t me = pthread_self();
    std::list<pthread_t>::iterator iter = threads.begin();
    std::list<pthread_t>::iterator end = threads.end();
    while ( iter != end )
	{
	if ( *iter!=me || !excludeThisThread )
	    pthread_kill( *iter, signal );
	++iter;
	}
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
