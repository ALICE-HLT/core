/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#define EXTENDEDMLUCSTACKMACHINE

#include "MLUCSimpleStackMachineAssembler.hpp"
#include "MLUCSimpleStackMachine.hpp"
#include <cstring>


MLUCSimpleStackMachine::MLUCSimpleStackMachine():
    fCodeWordCount(0),
    fCodeWords(NULL),
    fIP(0),
    fHalted(false),
    fStackSize(0),
    fStackAllocSize(0),
    fStack(NULL)

    {
    }

MLUCSimpleStackMachine::~MLUCSimpleStackMachine()
    {
    if ( fCodeWords )
	delete [] fCodeWords;
    if ( fStack )
	delete [] fStack;
    }

MLUCSimpleStackMachine::TError MLUCSimpleStackMachine::SetCode( unsigned long wordCount, uint64 const* words )
    {
    if ( fCodeWords )
	delete [] fCodeWords;
    fCodeWords=NULL;
    fCodeWordCount=0;
    if ( (wordCount && !words) )
	return TError( TError::kInvalidCodeInputData, "Number of code words unequal 0, but no words provided" );
    if ( !wordCount )
	return TError();

    fCodeWords = new uint64[wordCount];
    if ( !fCodeWords )
	return TError( TError::kOutOfMemory, "Out of memory trying to allocate memory for code words" );
    memcpy( fCodeWords, words, sizeof(uint64)*wordCount );
    fCodeWordCount = wordCount;

    Reset();
    
    return TError();
    }


MLUCSimpleStackMachine::TError MLUCSimpleStackMachine::SetCode( MLUCSimpleStackMachineAssembler const& assembler )
    {
    return SetCode( assembler.GetCodeWordCount(), assembler.GetCode() );
    }


MLUCSimpleStackMachine::TError MLUCSimpleStackMachine::Run()
    {
    if ( fHalted || fIP>=fCodeWordCount )
	return TError( TError::kHalted, "Machine already halted on run attempt" );
    TError ret;
    while ( !fHalted && fIP<fCodeWordCount )
	{
	ret = ExecNextStatement();
	if ( !ret.Ok() )
	    return ret;
	}
    return TError();
    }

MLUCSimpleStackMachine::TError MLUCSimpleStackMachine::Step()
    {
    if ( fHalted || fIP>=fCodeWordCount )
	return TError( TError::kHalted, "Machine already halted on single step attempt" );
    return ExecNextStatement();
    }

MLUCSimpleStackMachine::TError MLUCSimpleStackMachine::Reset()
    {
    fIP = 0;
    fHalted = false;
    ClearStack();
    return TError();
    }


MLUCSimpleStackMachine::TError MLUCSimpleStackMachine::GetResult( uint64& result ) const
    {
    if ( fStackSize<=0 )
	return TError( TError::kEmptyStack, "Stack is empty on attempt to get result" );
    result = fStack[fStackSize-1];
    return TError();
    }

void MLUCSimpleStackMachine::GetStack( std::vector<uint64>& stack ) const
    {
    stack.reserve( fStackSize );
    for ( unsigned long nn=0; nn<fStackSize; ++nn )
	stack.push_back( fStack[nn] );
    }


MLUCSimpleStackMachine::TError MLUCSimpleStackMachine::Pop( uint64& val, bool noShrink )
    {
    if ( fStackSize<=0 )
	return TError( TError::kEmptyStack, "Stack is already empty on pop attempt" );
    --fStackSize;
    val = fStack[fStackSize];
    if ( (fStackSize < (fStackAllocSize<<2)) && !noShrink )
	{
	unsigned long newStackSize = fStackAllocSize<<1;
	uint64* newStack = new uint64[newStackSize];
	if ( !newStack )
	    return TError( TError::kOutOfMemory, "Out of memory trying to allocate memory for stack shrinking" );
	memcpy( newStack, fStack, fStackSize*sizeof(uint64) );
	delete [] fStack;
	fStack = newStack;
	fStackAllocSize = newStackSize;
	}
    return TError();
    }

MLUCSimpleStackMachine::TError MLUCSimpleStackMachine::Peek( uint64& val )
    {
    if ( fStackSize<=0 )
	return TError( TError::kEmptyStack, "Stack is already empty on peek attempt" );
    val = fStack[fStackSize-1];
    return TError();
    }

MLUCSimpleStackMachine::TError MLUCSimpleStackMachine::Push( uint64 val )
    {
    if ( fStackSize>=fStackAllocSize )
	{
	unsigned long newStackSize = fStackAllocSize<<1;
	if ( newStackSize<=0 )
	    newStackSize = 4; // Initial value
	uint64* newStack = new uint64[newStackSize];
	if ( !newStack )
	    return TError( TError::kOutOfMemory, "Out of memory trying to allocate memory for stack growth" );
	if ( fStack )
	    {
	    memcpy( newStack, fStack, fStackSize*sizeof(uint64) );
	    delete [] fStack;
	    }
	fStack = newStack;
	fStackAllocSize = newStackSize;
	}
    fStack[fStackSize] = val;
    ++fStackSize;
    return TError();
    }

MLUCSimpleStackMachine::TError MLUCSimpleStackMachine::ChangeTop( uint64 val )
    {
    if ( fStackSize<=0 )
	return TError( TError::kEmptyStack, "Stack is empty on attempt to modify top" );
    fStack[fStackSize-1] = val;
    return TError();
    }


MLUCSimpleStackMachine::TError MLUCSimpleStackMachine::ClearStack()
    {
    if ( fStack )
	{
	delete [] fStack;
	fStack = NULL;
	fStackAllocSize = 0;
	fStackSize = 0;
	}
    return TError();
    }


MLUCSimpleStackMachine::TError MLUCSimpleStackMachine::ExecNextStatement()
    {
    uint64 value0=0, value1=0;
    switch ( (TInstructions)(fCodeWords[fIP]) )
	{
	case kNOP:
	    ++fIP;
	    break;
	case kHALT:
	    ++fIP;
	    fHalted = true;
	    break;
	case kLNOT:
	    if ( MLUCSTACKEMPTY() )
		return TError( TError::kNotENoughOperandsOnStack, "Not enough operands on stack for LNOT" );
	    MLUCTOPOFSTACK() = !MLUCTOPOFSTACK();
	    ++fIP;
	    break;
	case kLAND:
	    if ( !MLUCCHECKSTACKSIZE(2) )
		return TError( TError::kNotENoughOperandsOnStack, "Not enough operands on stack for LAND" );
	    Pop( value0 );
	    MLUCTOPOFSTACK() = MLUCTOPOFSTACK() && value0;
	    ++fIP;
	    break;
	case kLOR:
	    if ( !MLUCCHECKSTACKSIZE(2) )
		return TError( TError::kNotENoughOperandsOnStack, "Not enough operands on stack for LOR" );
	    Pop( value0 );
	    MLUCTOPOFSTACK() = MLUCTOPOFSTACK() || value0;
	    ++fIP;
	    break;
	case kLXOR:
	    if ( !MLUCCHECKSTACKSIZE(2) )
		return TError( TError::kNotENoughOperandsOnStack, "Not enough operands on stack for LXOR" );
	    Pop( value0 );
	    MLUCTOPOFSTACK() = (!MLUCTOPOFSTACK() && value0) || (MLUCTOPOFSTACK() && !value0);
	    ++fIP;
	    break;
	case kBNOT:
	    if ( MLUCSTACKEMPTY() )
		return TError( TError::kNotENoughOperandsOnStack, "Not enough operands on stack for BNOT" );
	    MLUCTOPOFSTACK() = ~MLUCTOPOFSTACK();
	    ++fIP;
	    break;
	case kBAND:
	    if ( !MLUCCHECKSTACKSIZE(2) )
		return TError( TError::kNotENoughOperandsOnStack, "Not enough operands on stack for BAND" );
	    Pop( value0 );
	    MLUCTOPOFSTACK() = MLUCTOPOFSTACK() & value0;
	    ++fIP;
	    break;
	case kBOR:
	    if ( !MLUCCHECKSTACKSIZE(2) )
		return TError( TError::kNotENoughOperandsOnStack, "Not enough operands on stack for BOR" );
	    Pop( value0 );
	    MLUCTOPOFSTACK() = MLUCTOPOFSTACK() | value0;
	    ++fIP;
	    break;
	case kBXOR:
	    if ( !MLUCCHECKSTACKSIZE(2) )
		return TError( TError::kNotENoughOperandsOnStack, "Not enough operands on stack for BXOR" );
	    Pop( value0 );
	    MLUCTOPOFSTACK() = MLUCTOPOFSTACK() ^ value0;
	    ++fIP;
	    break;
	case kBSHIFTLEFT:
	    if ( !MLUCCHECKSTACKSIZE(2) )
		return TError( TError::kNotENoughOperandsOnStack, "Not enough operands on stack for BSHIFTLEFT" );
	    Pop( value0 );
	    MLUCTOPOFSTACK() = MLUCTOPOFSTACK() << value0;
	    ++fIP;
	    break;
	case kBSHIFTRIGHT:
	    if ( !MLUCCHECKSTACKSIZE(2) )
		return TError( TError::kNotENoughOperandsOnStack, "Not enough operands on stack for BSHIFTRIGHT" );
	    Pop( value0 );
	    MLUCTOPOFSTACK() = MLUCTOPOFSTACK() >> value0;
	    ++fIP;
	    break;
	case kADD:
	    if ( !MLUCCHECKSTACKSIZE(2) )
		return TError( TError::kNotENoughOperandsOnStack, "Not enough operands on stack for ADD" );
	    Pop( value0 );
	    MLUCTOPOFSTACK() = MLUCTOPOFSTACK() + value0;
	    ++fIP;
	    break;
	case kSUB:
	    if ( !MLUCCHECKSTACKSIZE(2) )
		return TError( TError::kNotENoughOperandsOnStack, "Not enough operands on stack for SUB" );
	    Pop( value0 );
	    MLUCTOPOFSTACK() = MLUCTOPOFSTACK() - value0;
	    ++fIP;
	    break;
	case kMULT:
	    if ( !MLUCCHECKSTACKSIZE(2) )
		return TError( TError::kNotENoughOperandsOnStack, "Not enough operands on stack for MULT" );
	    Pop( value0 );
	    MLUCTOPOFSTACK() = MLUCTOPOFSTACK() * value0;
	    ++fIP;
	    break;
	case kDIV:
	    if ( !MLUCCHECKSTACKSIZE(2) )
		return TError( TError::kNotENoughOperandsOnStack, "Not enough operands on stack for DIV" );
	    Pop( value0 );
	    MLUCTOPOFSTACK() = MLUCTOPOFSTACK() / value0;
	    ++fIP;
	    break;
	case kMOD:
	    if ( !MLUCCHECKSTACKSIZE(2) )
		return TError( TError::kNotENoughOperandsOnStack, "Not enough operands on stack for MOD" );
	    Pop( value0 );
	    MLUCTOPOFSTACK() = MLUCTOPOFSTACK() % value0;
	    ++fIP;
	    break;
	case kINVERSE:
	    if ( MLUCSTACKEMPTY() )
		return TError( TError::kNotENoughOperandsOnStack, "Not enough operands on stack for INVERSE" );
	    MLUCTOPOFSTACK() = -MLUCTOPOFSTACK();
	    ++fIP;
	    break;
	case kEQUAL:
	    if ( !MLUCCHECKSTACKSIZE(2) )
		return TError( TError::kNotENoughOperandsOnStack, "Not enough operands on stack for EQUAL" );
	    Pop( value0 );
	    MLUCTOPOFSTACK() = (MLUCTOPOFSTACK() == value0);
	    ++fIP;
	    break;
	case kLESS:
	    if ( !MLUCCHECKSTACKSIZE(2) )
		return TError( TError::kNotENoughOperandsOnStack, "Not enough operands on stack for LESS" );
	    Pop( value0 );
	    MLUCTOPOFSTACK() = (MLUCTOPOFSTACK() < value0);
	    ++fIP;
	    break;
	case kGREATER:
	    if ( !MLUCCHECKSTACKSIZE(2) )
		return TError( TError::kNotENoughOperandsOnStack, "Not enough operands on stack for GREATER" );
	    Pop( value0 );
	    MLUCTOPOFSTACK() = (MLUCTOPOFSTACK() > value0);
	    ++fIP;
	    break;
	case kISNULL:
	    if ( MLUCSTACKEMPTY() )
		return TError( TError::kNotENoughOperandsOnStack, "Not enough operands on stack for ISNULL" );
	    MLUCTOPOFSTACK() = (MLUCTOPOFSTACK()==0);
	    ++fIP;
	    break;
	case kNEGATIVE:
	    if ( MLUCSTACKEMPTY() )
		return TError( TError::kNotENoughOperandsOnStack, "Not enough operands on stack for NEGATIVE" );
	    MLUCTOPOFSTACK() = (((long)MLUCTOPOFSTACK())<0);
	    ++fIP;
	    break;
	case kPOSITIVE:
	    if ( MLUCSTACKEMPTY() )
		return TError( TError::kNotENoughOperandsOnStack, "Not enough operands on stack for POSITIVE" );
	    MLUCTOPOFSTACK() = (((long)MLUCTOPOFSTACK())>0);
	    ++fIP;
	    break;
	case kJUMP:
	    if ( fIP+1>=fCodeWordCount )
		return TError( TError::kMissingOperand, "Missing jump address operand for JUMP instruction" );
	    fIP = fCodeWords[fIP+1];
	    break;
	case kJUMPNULL:
	    if ( fIP+1>=fCodeWordCount )
		return TError( TError::kMissingOperand, "Missing jump address operand for JUMPNULL instruction" );
	    if ( MLUCSTACKEMPTY() )
		return TError( TError::kNotENoughOperandsOnStack, "Not enough operands on stack for JUMPNULL" );
	    Pop( value0 );
	    if ( value0==0 )
		fIP = fCodeWords[fIP+1];
	    else
		fIP += 2;
	    break;
	case kJUMPNOTNULL:
	    if ( fIP+1>=fCodeWordCount )
		return TError( TError::kMissingOperand, "Missing jump address operand for JUMPNOTNULL instruction" );
	    if ( MLUCSTACKEMPTY() )
		return TError( TError::kNotENoughOperandsOnStack, "Not enough operands on stack for JUMPNOTNULL" );
	    Pop( value0 );
	    if ( value0!=0 )
		fIP = fCodeWords[fIP+1];
	    else
		fIP += 2;
	    break;
	case kDROP:
	    if ( MLUCSTACKEMPTY() )
		return TError( TError::kNotENoughOperandsOnStack, "Not enough operands on stack for DROP" );
	    Pop( value0 );
	    ++fIP;
	    break;
	case kCLEAR:
	    ClearStack();
	    ++fIP;
	    break;
	case kSWAP:
	    if ( !MLUCCHECKSTACKSIZE(2) )
		return TError( TError::kNotENoughOperandsOnStack, "Not enough operands on stack for SWAP" );
	    value0 = fStack[fStackSize-2];
	    fStack[fStackSize-2] = fStack[fStackSize-1];
	    fStack[fStackSize-1] = value0;
	    ++fIP;
	    break;
	case kSWAPNR:
	    if ( !MLUCCHECKSTACKSIZE(3) )
		return TError( TError::kNotENoughOperandsOnStack, "Not enough operands on stack for SWAPNR" );
	    Pop( value1 );
	    value0 = fStack[fStackSize-1-value1];
	    fStack[fStackSize-1-value1] = fStack[fStackSize-1];
	    fStack[fStackSize-1] = value0;
	    ++fIP;
	    break;
	case kMOVETOTOPNR:
	    if ( !MLUCCHECKSTACKSIZE(3) )
		return TError( TError::kNotENoughOperandsOnStack, "Not enough operands on stack for MOVETOTOPNR" );
	    Pop( value1 );
	    value0 = fStack[fStackSize-1-value1];
	    for ( unsigned long ii=fStackSize-1-value1; ii<fStackSize-1; ++ii )
		fStack[ii] = fStack[ii+1];
	    fStack[fStackSize-1] = value0;
	    ++fIP;
	    break;
	case kPUSHCONST:
	    if ( fIP+1>=fCodeWordCount )
		return TError( TError::kMissingOperand, "Missing jump address operand for PUSHCONST instruction" );
	    Push( fCodeWords[fIP+1] );
	    fIP += 2;
	    break;
	case kDUP:
	    if ( fStackSize<=0 )
		return TError( TError::kEmptyStack, "Stack is empty on DUP attempt" );
	    Push( MLUCTOPOFSTACK() );
	    ++fIP;
	    break;
				   
	default:
	    return TError( TError::kUnknownInstructionCode, "Unknown instruction code" );
	}
    return TError();
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
