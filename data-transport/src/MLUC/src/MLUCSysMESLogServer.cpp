#include "MLUCLogServer.hpp"
#include "MLUCSysMESLogServer.hpp"
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <limits.h>


struct event{
    const char *name;         /**< Sollte Herkunkt und Typ des Events eindeutig kennzeichen*/
    const char *info;         /**< Informationsteil des events */
    const char *severity;     /**< severity klasse des events (zwischen "1" und "9") */
    
    
    const char *defaultreaction;    /**< Defaultreaction die auf dem client ausgefuert werden soll*/
    const char *save_client;        /**< "0" Event soll nicht lokal gespeichert werden; "3" Lokale speicherung erwuenscht  */  
    const char *checkrule;          /**< "0"/"1" Gibt an ob das Event auf dem SysMES server gegen Rules gematched werden soll*/
    const char *savelaem;           /**< "true"/"false" Gibt an ob das Event serverseitig gespeichert werden soll*/ 
};

struct simple_event{
    const char *name;         /**< Sollte Herkunkt und Typ des Events eindeutig kennzeichen*/
    const char *info;         /**< Informationsteil des events */
    const char *severity;     /**< severity klasse des events (zwischen "1" und "9") */
};

static int write_event_to_handler(int pipe, const char init,const char *name, const char* default_action,const char* save_on_client,const char *severity, 
                            const char* info, const char* time_to_respond, const char* save_laem, const char *checkrule);

/** \fn  int inject_event(struct simple_event *value);
 *  Sendet ein event an SysMES.
 *  \return Anzahl der versendeten Bytes oder -1 falls ein Fehler auftrat
 */
static int inject_event(struct event *value);

#if 0
// Seems to be unused
/** \fn  int inject_simple_event(struct simple_event *value);
 *  Sendet ein simple_event an SysMES.
 *  Folgende defaultwerte werden gesetzt:
 *   - defaultreaction = "none"
 *   - save_client = "0"
 *   - checkrule = "false"
 *   - savelaem = "1"
 *  \return Anzahl der versendeten Bytes oder -1 falls ein Fehler auftrat
 */
static int inject_simple_event(struct simple_event *value);
#endif

#define DEFAULT_MSG_SZ 128

using namespace std;

void MLUCSysMESLogServer::LogLine( const char* origin, const char* keyword, const char* file, int linenr, 
				   const char* compile_date, const char* compile_time, MLUCLog::TLogLevel logLvl, 
				   const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
				   unsigned long runNumber, 
#endif
				   uint32 ldate, 
				   uint32 ltime_s, uint32 ltime_us, uint32 msgNr, const char* line )
	{
	const char* clvl;
	switch ( logLvl )
		{
		/*
		case MLUCLog::kBenchmark: clvl = "TM_Benchmark"; break;
		case MLUCLog::kDebug: clvl = "TM_Debug"; break;
		*/
		case MLUCLog::kInformational: clvl = "TM_Information"; break;
		case MLUCLog::kWarning: clvl = "TM_Warning"; break;
		case MLUCLog::kError: clvl = "TM_Error"; break;
		case MLUCLog::kFatal: clvl = "TM_Fatal Error"; break;
		case MLUCLog::kImportant: clvl = "TM_Information";  break;
		default: return;
		}
	char* info = new char[ DEFAULT_MSG_SZ ];
	int sz = 0;
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
	if ( runNumber )
	    sz = snprintf(info, DEFAULT_MSG_SZ * sizeof(char),"%s%s%s %08d.%06d.%06d %12lu %10u [%s/%d (%s/%s)] (%s/%s) %s: %s\n", hostname, (id ? ":" : ""), (id ? id : ""), ldate, ltime_s, ltime_us,
			  runNumber, msgNr, file, linenr, compile_date, compile_time, origin, 
			  keyword, clvl, line );
	else
#endif
	    sz = snprintf(info, DEFAULT_MSG_SZ * sizeof(char),"%s%s%s %08d.%06d.%06d %10u [%s/%d (%s/%s)] (%s/%s) %s: %s\n", hostname, (id ? ":" : ""), (id ? id : ""), ldate, ltime_s, ltime_us,
			  msgNr, file, linenr, compile_date, compile_time, origin, 
			  keyword, clvl, line );
	if( sz >= (int)(DEFAULT_MSG_SZ *  sizeof(char)) )
		{
		delete info;
		info = new char[ (sz + 1) / sizeof(char) ];
		snprintf(info, sz + 1, "%s%s%s %08d.%06d.%06d %10u [%s/%d (%s/%s)] (%s/%s) %s: %s\n", hostname, (id ? ":" : ""), (id ? id : ""), ldate, ltime_s, ltime_us,
		msgNr, file, linenr, compile_date, compile_time, origin, 
		keyword, clvl, line );
		}		
	struct event evnt;
	evnt.name = clvl;
	evnt.info = info;
	evnt.severity = "5";
	evnt.defaultreaction = "none";
	evnt.save_client = "0";
	evnt.checkrule = "0";
	evnt.savelaem = "true";
	inject_event(&evnt);
	delete info;
	}
	

void MLUCSysMESLogServer::LogLine( const char* origin, const char* keyword, const char* file, int linenr, 
				   const char* compile_date, const char* compile_time, MLUCLog::TLogLevel logLvl, 
				   const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
				   unsigned long runNumber, 
#endif
				   uint32 ldate, 
				   uint32 ltime_s, uint32 ltime_us, uint32 msgNr, uint32 subMsgNr, const char* line )
	{
	const char* clvl;
	switch ( logLvl )
		{
		/*
		case MLUCLog::kBenchmark: clvl = "TM_Benchmark"; break;
		case MLUCLog::kDebug: clvl = "TM_Debug"; break;
		*/
		case MLUCLog::kInformational: clvl = "TM_Information"; break;
		case MLUCLog::kWarning: clvl = "TM_Warning"; break;
		case MLUCLog::kError: clvl = "TM_Error"; break;
		case MLUCLog::kFatal: clvl = "TM_Fatal Error"; break;
		case MLUCLog::kImportant: clvl = "TM_Information";  break;
		default: return;
		}
	char* info = new char[ DEFAULT_MSG_SZ ];
	int sz = 0;
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
	if ( runNumber )
	    sz = snprintf(info, DEFAULT_MSG_SZ * sizeof(char), "%s%s%s %08d.%06d.%06d %12lu %10u/%10u [%s/%d (%s/%s)] (%s/%s)%s: %s\n", hostname, (id ? ":" : ""), (id ? id : ""), ldate, ltime_s, ltime_us,
			  runNumber, msgNr, subMsgNr, file, linenr, compile_date, compile_time, origin, 
			  keyword, clvl, line );
	else
#endif
	    sz = snprintf(info, DEFAULT_MSG_SZ * sizeof(char), "%s%s%s %08d.%06d.%06d %10u/%10u [%s/%d (%s/%s)] (%s/%s)%s: %s\n", hostname, (id ? ":" : ""), (id ? id : ""), ldate, ltime_s, ltime_us,
			  msgNr, subMsgNr, file, linenr, compile_date, compile_time, origin, 
			  keyword, clvl, line );
	if( sz >= (int)(DEFAULT_MSG_SZ *  sizeof(char)) )
		{
		delete info;
		info = new char[ (sz + 1) / sizeof(char) ];
		snprintf(info, sz + 1, "%s%s%s %08d.%06d.%06d %10u/%10u [%s/%d (%s/%s)] (%s/%s)%s: %s\n", hostname, (id ? ":" : ""), (id ? id : ""), ldate, ltime_s, ltime_us,
		msgNr, subMsgNr, file, linenr, compile_date, compile_time, origin, 
		keyword, clvl, line );
		}
	struct event evnt;
	evnt.name = clvl;
	evnt.info = info;
	evnt.severity = "5";
	evnt.defaultreaction = "none";
	evnt.save_client = "0";
	evnt.checkrule = "0";
	evnt.savelaem = "true";
	inject_event(&evnt);
	delete info;	
	}


static const char * const  FIFO_PATH="/opt/sysmes/dist/ext_event.fifo";


int write_event_to_handler(int pipe, const char init, const char *name, const char* default_action,const char* save_on_client,const char *severity, 
                            const char* info, const char* time_to_respond, const char* save_laem, const char *checkrule)
{
    char *my_send_buffer;
    int my_messagesize;
    int bsend;
    int i; 
    
    //berechne die groesse der Nachricht
    my_messagesize=strlen(name)+ strlen(default_action)+ strlen(save_on_client)
                         +strlen(severity)+strlen(info)+strlen(time_to_respond)+strlen(save_laem)+strlen(checkrule)+21;

    //Teste ob die Schreiboperation atomar sein wird                         
    if (my_messagesize>=PIPE_BUF){
        return -1;
    }
    
    my_send_buffer = (char *) malloc((my_messagesize+1)*sizeof(char));
    
    if (my_send_buffer == NULL){
        return -1;
    }
    
    sprintf(my_send_buffer,"%c\033A%s\033#%s\033#%s\033#%s\033#%s\033#%s\033#%s\033#%s\033#\033E",
                init,name,default_action,save_on_client,severity,info,time_to_respond,save_laem,checkrule);
         
    //Versuche 5 mal die Nachricht and den Handler zu versenden           
    for (i=0; i<5; i++){   
            bsend = write(pipe,my_send_buffer,my_messagesize);
            if ((bsend!=-1) || (errno != EAGAIN)) break;
            usleep(10000);
    }
    
    free(my_send_buffer);
    
    if ((bsend == -1) || (bsend != my_messagesize)){
        return -1;
    }
    else return my_messagesize;
        
}


int inject_event(struct event *value){
    int ext_fifo_write;    
    char severity[3];
    int bytessend;
    
    
    if ((ext_fifo_write = open(FIFO_PATH, O_WRONLY | O_NONBLOCK))==-1){
        return -1;
    }  
 
    
    if ((value->severity[0] -'0' >=5) && (value->severity[0] -'0'<=9)){        
        severity[0] = '2';
        severity[1] = value->severity[0];
        severity[2] = '\0';
    } else {
        severity[0] = value->severity[0];   
        severity[1] = '\0';
    }
 
    
    bytessend =  write_event_to_handler(ext_fifo_write,'E',value->name,value->defaultreaction,value->save_client,
                                    severity,value->info,"0",value->savelaem,value->checkrule);
    
        
    close(ext_fifo_write);
    
    return bytessend;   
}



int inject_simple_event(struct simple_event *value){
    int ext_fifo_write;    
    char severity[3];
    int bytessend;
        
    if ((ext_fifo_write = open(FIFO_PATH, O_WRONLY | O_NONBLOCK))==-1){
        return -1;
    }  
    
    if ((value->severity[0] -'0' >=5) && (value->severity[0] -'0'<=9)){        
        severity[0] = '2';
        severity[1] = value->severity[0];
        severity[2] = '\0';
    } else {
        severity[0] = value->severity[0];   
        severity[1] = '\0';
    }
    
    bytessend = write_event_to_handler(ext_fifo_write,'E',value->name,"none","0",
                                    severity,value->info,"0","true","0");
                                    
    close(ext_fifo_write);
    
    return bytessend;
    
}
