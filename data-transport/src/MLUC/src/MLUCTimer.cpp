/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#define __USE_UNIX98 1

#include "MLUCTimer.hpp"
#include <pthread.h>
#include <time.h>
#include <errno.h>


MLUCTimer::MLUCTimer():
    fThread( *this )
    {
    pthread_mutex_init( &fAccessSem, NULL );
    pthread_mutex_init( &fConditionSem, NULL );
    pthread_cond_init( &fCondition, NULL );
    fNextID = 0;
    }

MLUCTimer::~MLUCTimer()
    {
    pthread_mutex_destroy( &fAccessSem );
    pthread_mutex_destroy( &fConditionSem );
    pthread_cond_destroy( &fCondition );
    }

void MLUCTimer::Start()
    {
    gettimeofday( &fTimerStarted, NULL );
    fThread.Start();
    }

void MLUCTimer::Stop()
    {
    fQuit = true;
    pthread_mutex_lock( &fConditionSem );
    pthread_cond_broadcast( &fCondition );
    pthread_mutex_unlock( &fConditionSem );
    }

uint32 MLUCTimer::AddTimeout( uint32 timeout_ms, MLUCTimerCallback* callback, uint64 notData )
    {
    TimeoutData data;
    data.fID = fNextID++;
    data.fTimeout = timeout_ms;
    data.fCallback = callback;
    data.fNotData = notData;
    pthread_mutex_lock( &fAccessSem );
    InsertTimeout( data );
    pthread_mutex_unlock( &fAccessSem );
    pthread_mutex_lock( &fConditionSem );
    pthread_cond_broadcast( &fCondition );
    pthread_mutex_unlock( &fConditionSem );
    return data.fID;
    }

void MLUCTimer::CancelTimeout( uint32 id )
    {
    bool found = false;
    vector<TimeoutData>::iterator iter, end;
    pthread_mutex_lock( &fAccessSem );
    iter = fTimeouts.begin();
    end = fTimeouts.end();
    while ( iter != end )
	{
	if ( iter->fID == id )
	    {
	    fTimeouts.erase( iter );
	    found = true;
	    break;
	    }
	iter++;
	}
    pthread_mutex_unlock( &fAccessSem );
    if ( found )
	{
	pthread_mutex_lock( &fConditionSem );
	pthread_cond_broadcast( &fCondition );
	pthread_mutex_unlock( &fConditionSem );
	}
    }

void MLUCTimer::NewTimeout( uint32 id, uint32 new_timeout_ms )
    {
    bool found = false;
    TimeoutData newTimeout;
    vector<TimeoutData>::iterator iter, end;
    pthread_mutex_lock( &fAccessSem );
    iter = fTimeouts.begin();
    end = fTimeouts.end();
    while ( iter != end )
	{
	if ( iter->fID == id )
	    {
	    newTimeout = *iter;
	    newTimeout.fTimeout = new_timeout_ms;
	    fTimeouts.erase( iter );
	    InsertTimeout( newTimeout );
	    found = true;
	    break;
	    }
	iter++;
	}
    pthread_mutex_unlock( &fAccessSem );
    if ( found )
	{
	pthread_mutex_lock( &fConditionSem );
	pthread_cond_broadcast( &fCondition );
	pthread_mutex_unlock( &fConditionSem );
	}
    }


MLUCTimer::MLUCTimerThread::MLUCTimerThread( MLUCTimer& timer ):
    fTimer( timer )
    {
    }

void MLUCTimer::MLUCTimerThread::Run()
    {
    fTimer.TimerLoop();
    }

void MLUCTimer::TimerLoop()
    {
    pthread_mutex_lock( &fConditionSem );
    fQuit = false;
    int ret;
    struct timespec wakeup_time;
    struct timeval tv;
    while ( !fQuit )
	{
	pthread_mutex_lock( &fAccessSem );
	if ( fTimeouts.size()>0 )
	    {
	    gettimeofday( &tv, NULL );
	    wakeup_time.tv_sec = tv.tv_sec + fTimeouts[0].fTimeout/1000;
	    wakeup_time.tv_nsec = (tv.tv_usec*1000) + (fTimeouts[0].fTimeout%1000)*1000000;
	    if ( wakeup_time.tv_nsec >= 1000000000 )
		{
		wakeup_time.tv_sec++;
		wakeup_time.tv_nsec -= 1000000000;
		}
	    pthread_mutex_unlock( &fAccessSem );
	    ret = pthread_cond_timedwait( &fCondition, &fConditionSem, &wakeup_time );
	    }
	else
	    {
	    pthread_mutex_unlock( &fAccessSem );
	    ret = pthread_cond_wait( &fCondition, &fConditionSem );
	    }
	if ( !fQuit && ret==ETIMEDOUT )
	    TimerExpired();
	}
    pthread_mutex_unlock( &fConditionSem );
    }

void MLUCTimer::InsertTimeout( TimeoutData& data )
    {
    uint64 diff;
    struct timeval tv;
    vector<TimeoutData>::iterator iter, end;
    gettimeofday( &tv, NULL );
    if ( tv.tv_usec < fTimerStarted.tv_usec )
	{
	tv.tv_usec += 1000000;
	tv.tv_sec -= 1;
	}
    diff = 1000*(tv.tv_sec-fTimerStarted.tv_sec)+(tv.tv_usec-fTimerStarted.tv_usec)/1000;
    data.fTimeout += diff;
    iter = fTimeouts.begin();
    end = fTimeouts.end();
    while ( iter != end && iter->fTimeout < data.fTimeout )
	iter++;
    fTimeouts.insert( iter, data );
    }

void MLUCTimer::TimerExpired()
    {
    uint64 diff;
    struct timeval tv;
    vector<TimeoutData> expiredTimeouts;
    pthread_mutex_lock( &fAccessSem );
    gettimeofday( &tv, NULL );
    if ( tv.tv_usec < fTimerStarted.tv_usec )
	{
	tv.tv_usec += 1000000;
	tv.tv_sec -= 1;
	}
    diff = 1000*(tv.tv_sec-fTimerStarted.tv_sec)+(tv.tv_usec-fTimerStarted.tv_usec)/1000;
    fTimerStarted = tv;
    vector<TimeoutData>::iterator iter, end;
    iter = fTimeouts.begin();
    end = fTimeouts.end();
    expiredTimeouts.reserve( fTimeouts.size() );
    while ( iter != end )
	{
	if ( iter->fTimeout < diff )
	    iter->fTimeout = 0;
	else
	    iter->fTimeout -= diff;
	//if ( iter->fTimeout <= 0 )
	//iter->fCallback->TimerExpired( iter->fNotData );
	iter++;
	}
    while ( fTimeouts.size()>0 && fTimeouts.begin()->fTimeout <= 0 )
	{
	expiredTimeouts.insert( expiredTimeouts.end(), *fTimeouts.begin() );
	fTimeouts.erase( fTimeouts.begin() );
	}
    pthread_mutex_unlock( &fAccessSem );

    if ( expiredTimeouts.size()>0 )
	{
	pthread_mutex_unlock( &fConditionSem );
	iter = expiredTimeouts.begin();
	end = expiredTimeouts.end();
	while ( iter != end )
	    {
	    iter->fCallback->TimerExpired( iter->fNotData );
	    iter++;
	    }
	pthread_mutex_lock( &fConditionSem );
	}
    }




void MLUCTimerSignal::TimerExpired( uint64 notData )
    {
    AddNotificationData( notData );
    Signal();
    }





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
