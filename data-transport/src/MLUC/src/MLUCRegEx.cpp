/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCRegEx.hpp"

MLUCRegEx::MLUCRegEx()
    {
    fError = "";
    fRegExSet = false;
    }
    
MLUCRegEx::~MLUCRegEx()
    {
    if ( fRegExSet )
	regfree( &fRegEx );
    }

bool MLUCRegEx::SetPattern( const char* pattern, TPatternFlags flags )
    {
    if ( fRegExSet )
	regfree( &fRegEx );
    int ret;
    int cflags=0;
    if ( flags & kPFExtended )
	cflags |= REG_EXTENDED;
    if ( flags & kPFNoCase )
	cflags |= REG_ICASE;
    if ( flags & kPFNoNewLineWildcard )
	cflags |= REG_NEWLINE;
    ret = regcomp( &fRegEx, pattern, cflags);
    if ( ret )
	{
	size_t errSize;
	errSize = regerror( ret,  &fRegEx, NULL, 0 );
	char* errBuf = new char[ errSize+1 ];
	if ( errBuf )
	    {
	    regerror( ret,  &fRegEx, errBuf, errSize+1 );
	    fError = errBuf;
	    delete [] errBuf;
	    }
	else
	    fError = "Out of memory getting error description!";
	return false;
	}
    fRegExSet = true;
    return true;
    }

bool MLUCRegEx::Search( const char* haystack, TSearchFlags flags )
    {
    vector<MLUCString>tmp;
    return Search( haystack, tmp, flags );
    }
	
bool MLUCRegEx::Search( const char* haystack, vector<MLUCString>& subStrs, TSearchFlags flags )
    {
    if ( !fRegExSet )
	{
	fError = "No regex pattern set.";
	return false;
	}
    int ret;
    int eflags=0;
    if ( flags & kSFNoBOL )
	eflags |= REG_NOTBOL;
    if ( flags & kSFNoEOL )
	eflags |= REG_NOTEOL;
    size_t nmatch=0;
    regmatch_t *pmatch=NULL;
    
    if ( subStrs.size()>0 )
	{
	pmatch = new regmatch_t[ subStrs.size()+1 ];
	if ( pmatch )
	    nmatch = subStrs.size()+1;
	}
    ret = regexec( &fRegEx, haystack, nmatch, pmatch, eflags );
    if ( ret )
	{
	if ( pmatch )
	    delete [] pmatch;
	return false;
	}
    if ( nmatch<=0 )
	{
	if ( pmatch )
	    delete [] pmatch;
	return true;
	}
    size_t i;
    MLUCString text( haystack );
    for ( i = 0; i < nmatch-1; i++ )
	{
// 	printf( "Match %d: so: %d - eo: %d\n", i, pmatch[i+1].rm_so, pmatch[i+1].rm_eo );
// 	printf( "Match %d: so: %x - eo: %x\n", i, pmatch[i+1].rm_so, pmatch[i+1].rm_eo );
	if ( pmatch[i+1].rm_so!=-1 && pmatch[i+1].rm_eo!=-1 )
	    {
	    subStrs[i] = text.Substr( pmatch[i+1].rm_so, pmatch[i+1].rm_eo-pmatch[i+1].rm_so );
	    }
	else
	    subStrs[i] = "";
	}
	if ( pmatch )
	    delete [] pmatch;
    return true;
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
