/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCString.hpp"
#include <string.h>
#include <ctype.h>


MLUCString::MLUCString()
    {
    fContents = NULL;
    fAllocLength = fLength = 0;
    fDefault = 0;
    fSizeIncrement = 0;
    }
	
MLUCString::MLUCString( const char* content )
    {
    if ( !content )
	{
	fContents = NULL;
	fAllocLength = fLength = 0;
	fDefault = 0;
	}
    else
	{
	fDefault = 0;
	TSizeType len = strlen( content );
	fContents = new char[ len+1 ];
	if ( !fContents )
	    fAllocLength = fLength = 0;
	else
	    {
	    fAllocLength = fLength = len;
	    strcpy( fContents, content );
	    }
	}
    fSizeIncrement = 0;
    }

MLUCString::MLUCString( const MLUCString& content )
    {
    fDefault = 0;
    TSizeType len = content.fLength;
    fContents = new char[ len+1 ];
    if ( !fContents )
	fAllocLength = fLength = 0;
    else
	{
	fAllocLength = fLength = len;
	memcpy( fContents, content.fContents, len );
	fContents[ len ] = 0;
	}
    fSizeIncrement = 0;
    }

MLUCString::MLUCString( const char& c )
    {
    fDefault = 0;
    TSizeType len = 1;
    fContents = new char[ 2 ];
    if ( !fContents )
	fAllocLength = fLength = 0;
    else
	{
	fAllocLength = fLength = len;
	fContents[0] = c;
	fContents[1] = 0;
	}
    fSizeIncrement = 0;
    }

MLUCString::~MLUCString()
    {
    if ( fContents )
	delete [] fContents;
    fContents = NULL;
    }

MLUCString& MLUCString::Append( const MLUCString& str )
    {
    if ( !str.fContents || str.fLength == 0 )
	return *this;
    TSizeType newLen = fLength + str.fLength;
    if ( fAllocLength<newLen )
	{
	unsigned long newAllocLen;
	if ( fSizeIncrement )
	    {
	    newAllocLen = fAllocLength+fSizeIncrement;
	    while ( newAllocLen<newLen )
		newAllocLen += fSizeIncrement;
	    }
	else
	    {
	    newAllocLen = fAllocLength+fAllocLength;
	    if ( newAllocLen<newLen )
		newAllocLen = newLen;
	    }
	char* tmpCont;
	tmpCont = new char[ newAllocLen+1 ];
	if ( !tmpCont )
	    return *this;
	if ( fContents )
	    {
	    memcpy( tmpCont, fContents, fLength );
	    delete [] fContents;
	    }
	fContents = tmpCont;
	fAllocLength = newAllocLen;
	}
    memcpy( fContents+fLength, str.fContents, str.fLength );
    fContents[ newLen ] = 0;
    fLength = newLen;
    return *this;
    }
	
MLUCString& MLUCString::Append( const char* str )
    {
    if ( !str || strlen( str ) == 0 )
	return *this;
    TSizeType newLen = fLength + strlen( str );
    if ( fAllocLength<newLen )
	{
	unsigned long newAllocLen;
	if ( fSizeIncrement )
	    {
	    newAllocLen = fAllocLength+fSizeIncrement;
	    while ( newAllocLen<newLen )
		newAllocLen += fSizeIncrement;
	    }
	else
	    {
	    newAllocLen = fAllocLength+fAllocLength;
	    if ( newAllocLen<newLen )
		newAllocLen = newLen;
	    }
	char* tmpCont;
	tmpCont = new char[ newAllocLen+1 ];
	if ( !tmpCont )
	    return *this;
	if ( fContents )
	    {
	    memcpy( tmpCont, fContents, fLength );
	    delete [] fContents;
	    }
	fContents = tmpCont;
	fAllocLength = newAllocLen;
	}
    strcpy( fContents+fLength, str );
    fContents[ newLen ] = 0;
    fLength = newLen;
    return *this;
    }

MLUCString& MLUCString::Append( const char& c )
    {
    TSizeType newLen = fLength + 1;
    if ( fAllocLength<newLen )
	{
	unsigned long newAllocLen;
	if ( fSizeIncrement )
	    {
	    newAllocLen = fAllocLength+fSizeIncrement;
	    while ( newAllocLen<newLen )
		newAllocLen += fSizeIncrement;
	    }
	else
	    {
	    newAllocLen = fAllocLength+fAllocLength;
	    if ( newAllocLen<newLen )
		newAllocLen = newLen;
	    }
	char* tmpCont;
	tmpCont = new char[ newAllocLen+1 ];
	if ( !tmpCont )
	    return *this;
	if ( fContents )
	    {
	    memcpy( tmpCont, fContents, fLength );
	    delete [] fContents;
	    }
	fContents = tmpCont;
	fAllocLength = newAllocLen;
	}
    fContents[ fLength ] = c;
    fContents[ newLen ] = 0;
    fLength = newLen;
    return *this;
    }


MLUCString& MLUCString::Set( const MLUCString& str )
    {
    if ( !str.fLength )
	{
	fLength = 0;
	if ( fContents )
	    fContents[0] = 0;
	return *this;
	}
    if ( fContents && fAllocLength<str.fLength )
	{
	delete [] fContents;
	fContents = NULL;
	fAllocLength = 0;
	}
    if ( !fContents )
	{
	fContents = new char[ str.fLength+1 ];
	if ( !fContents )
	    return *this;
	else
	    fAllocLength = str.fLength;
	}
    memcpy( fContents, str.fContents, str.fLength );
    fLength = str.fLength;
    fContents[ fLength ] = 0;
    return *this;
    }

MLUCString& MLUCString::Set( const char* str )
    {
    if ( !str || strlen(str)==0 )
	{
	if ( fContents )
	    fContents[0] = 0;
	fLength = 0;
	return *this;
	}
    TSizeType len = strlen(str);
    if ( fContents && fAllocLength<len )
	{
	delete [] fContents;
	fContents = NULL;
	fAllocLength = 0;
	}
    if ( !fContents )
	{
	fContents = new char[ len+1 ];
	if ( !fContents )
	    return *this;
	else
	    fAllocLength = len;
	}
    strcpy( fContents, str );
    fLength = len;
    return *this;
    }

MLUCString& MLUCString::Set( const char& c )
    {
    if ( fContents && fAllocLength<1 )
	{
	delete [] fContents;
	fContents = NULL;
	fAllocLength = 0;
	}
    if ( !fContents )
	{
	fContents = new char[ 2 ];
	if ( !fContents )
	    return *this;
	else
	    fAllocLength = 1;
	}
    fContents[ 0 ] = c;
    fContents[ 1 ] = 0;
    fLength = 1;
    return *this;
    }

bool MLUCString::operator== ( const MLUCString& str ) const
    {
    if ( fLength != str.fLength )
	return false;
    if ( !fLength ) // fLength==str.fLength here, so both are empty
	return true;
    if ( memcmp( fContents, str.fContents, fLength ) )
	return false;
    return true;
    }

bool MLUCString::operator== ( const char* str ) const
    {
    if ( !str )
	{
	if ( !fContents || !fLength )
	    return true;
	else
	    return false;
	}
    TSizeType len = strlen( str );
    if ( !len && (!fLength || !fContents) )
	return true;
    if ( fLength != len )
	return false;
    if ( !fLength ) // fLength==len==strlen(str) here, so both are empty, should actually be covered by above case ' if ( !len && (!fLength || !fContents) ) '
	return true;
    if ( strcmp( fContents, str ) )
	return false;
    return true;
    }

bool MLUCString::operator== ( const char& c ) const
    {
    if ( fLength==1 && fContents[0]==c )
	return true;
    else
	return false;
    }


bool MLUCString::operator!= ( const MLUCString& str ) const
    {
    if ( fLength != str.fLength )
	return true;
    if ( !fLength ) // fLength==str.fLength here, so both are empty
	return false;
    if ( (!fContents && str.fContents) || (fContents && !str.fContents) )
	return true;
    if ( fContents && str.fContents && memcmp( fContents, str.fContents, fLength ) )
	return true;
    return false;
    }

bool MLUCString::operator!= ( const char* str ) const
    {
    if ( !str )
	{
	if ( !fContents || !fLength )
	    return false;
	else
	    return true;
	}
    TSizeType len = strlen( str );
    if ( fLength != len ) //  || (!fContents && len) ?? Should not be necessary?
	return true;
    if ( !fLength ) // fLength==len==strlen(str) here, so both are empty
	return false;
    if ( strcmp( fContents, str ) )
	return true;
    return false;
    }

bool MLUCString::operator!= ( const char& c ) const
    {
    if ( fLength==1 && fContents[0]==c )
	return false;
    else
	return true;
    }

bool MLUCString::IsEmpty() const
    {
    if ( !fLength || !fContents )
	return true;
    return false;
    }

bool MLUCString::IsWhiteSpace() const
    {
    if ( !fLength || !fContents )
	return false;
    TSizeType n;
    for ( n = 0; n<fLength; n++ )
	{
	switch ( fContents[n] )
	    {
	    case ' ':
	    case '\n':
	    case '\t':
	    case 0:
		break;
	    default:
		return false;
	    }
	}
    return true;
    }

void MLUCString::ToLower()
    {
    TSizeType n;
    for ( n = 0; n<fLength; n++ )
	fContents[n] = tolower( fContents[n] );
    }

void MLUCString::ToUpper()
    {
    TSizeType n;
    for ( n = 0; n<fLength; n++ )
	fContents[n] = toupper( fContents[n] );
    }


// Very simple and stupid search algorithm. I know there are better ones, but I don't 
// have time too look them up, and in MY stuff this class (and in particular this 
// function) is not used in very performance critical areas.
MLUCString::TSizeType MLUCString::Find( const char* substr, TSizeType startPos ) const
    {
    if ( !fLength || !fContents || !substr )
	return kNoPos;
    char* start = fContents+startPos;
    const char* tmpSub;
    char* tmpMain;
    bool found = false;
    TSizeType substrlen = strlen(substr);
    TSizeType mainlen = fLength-startPos;
    while ( mainlen >= substrlen )
	{
	tmpSub = substr;
	tmpMain = start;
	found = true;
	while ( *tmpSub )
	    {
	    if ( *tmpSub++!=*tmpMain++ )
		{
		found = false;
		break;
		}
	    }
	if ( found )
	    return start-fContents;
	start++;
	mainlen--;
	}
    return kNoPos;
    }

MLUCString::TSizeType MLUCString::Find( const char& c, TSizeType startPos ) const
    {
    if ( !fLength || !fContents )
	return kNoPos;
    char* start = fContents+startPos;
    char* end = fContents+fLength;
    while ( start < end )
	{
	if ( *start==c )
	    return start-fContents;
	start++;
	}
    return kNoPos;
    }

MLUCString::TSizeType MLUCString::Find( const MLUCString& substr, TSizeType startPos ) const
    {
    if ( !fLength || !fContents || !substr.fLength || !substr.fContents )
	return kNoPos;
    char* start = fContents+startPos;
    char* tmpSub;
    char* tmpMain;
    char* subEnd = substr.fContents+substr.fLength;
    bool found = false;
    TSizeType substrlen = substr.fLength;
    TSizeType mainlen = fLength-startPos;
    while ( mainlen >= substrlen )
	{
	tmpSub = substr.fContents;
	tmpMain = start;
	found = true;
	while ( tmpSub!=subEnd )
	    {
	    if ( *tmpSub++!=*tmpMain++ )
		{
		found = false;
		break;
		}
	    }
	if ( found )
	    return start-fContents;
	start++;
	mainlen--;
	}
    return kNoPos;
    }

MLUCString MLUCString::Substr( MLUCString::TSizeType start, MLUCString::TSizeType len ) const
    {
    MLUCString tmp;
    tmp.SetSizeIncrement(len);
    TSizeType n = start;
    TSizeType end = start+len;
    while ( n<end && n<fLength )
	tmp += fContents[n++];
    tmp.SetSizeIncrement(0);
    return tmp;
    }

MLUCString MLUCString::Substr( MLUCString::TSizeType start ) const
    {
    MLUCString tmp;
    TSizeType n = start;
    tmp.SetSizeIncrement(fLength-start+1);
    while ( n<fLength )
	tmp += fContents[n++];
    tmp.SetSizeIncrement(0);
    return tmp;
    }

void MLUCString::Erase( MLUCString::TSizeType start, MLUCString::TSizeType len )
    {
    if ( !fLength || !fContents )
	return;
    char* dest = fContents+start;
    if ( start>=fLength )
	return;
    if ( start+len>fLength )
	len = fLength-start;
    char* src = fContents+start+len;
    char* end = fContents+fLength;
    while ( src<end )
	*dest++=*src++;
    *dest = 0;
    fLength -= len;
    }

void MLUCString::Split( std::vector<MLUCString>& tokens, char separator ) const
    {
    MLUCString tmp;
    tokens.clear();
    TSizeType startPos=0, lastStartPos=0;
    while ( startPos != kNoPos )
	{
	startPos = Find( separator, lastStartPos );
	if ( startPos != kNoPos )
	    {
	    tmp = Substr( lastStartPos, startPos-lastStartPos );
	    lastStartPos = startPos+1;
	    }
	else
	    tmp = Substr( lastStartPos );
	tokens.push_back( tmp );
	}
    }

void MLUCString::Split( std::vector<MLUCString>& tokens, const char* separators ) const
    {
    MLUCString tmp;
    tokens.clear();
    TSizeType startPos=0, lastStartPos=0;
    unsigned sepCnt = strlen(separators);
    while ( startPos != kNoPos )
	{
	startPos = kNoPos;
	for ( unsigned nn=0; nn<sepCnt; nn++ )
	    {
	    TSizeType tmpPos = Find( separators[nn], lastStartPos );
	    if ( tmpPos < startPos )
		startPos = tmpPos;
	    }
	if ( startPos != kNoPos )
	    {
	    tmp = Substr( lastStartPos, startPos-lastStartPos );
	    lastStartPos = startPos+1;
	    }
	else
	    tmp = Substr( lastStartPos );
	tokens.push_back( tmp );
	}
    }

MLUCString MLUCString::TimeStamp2String( unsigned long secs, unsigned long microsecs )
    {
    MLUCString str;
    if ( secs==0 && (microsecs==0 || microsecs==~0UL) )
	{
	str = "00000000.000000";
	if ( microsecs==0 )
	    str += ".000000";
	return str;
	}
    char tmp[256];
    time_t t_secs = (time_t)secs;
    tm* t = localtime( &(t_secs) );
    t->tm_mon++;
    t->tm_year += 1900;
    unsigned date = t->tm_year*10000+t->tm_mon*100+t->tm_mday;
    unsigned time_s = t->tm_hour*10000+t->tm_min*100+t->tm_sec;
    snprintf( tmp, 256, "%08u.%06u", date, time_s );
    str = tmp;
    if ( microsecs != ~0UL )
	{
	snprintf( tmp, 256, ".%06lu", microsecs );
	str += tmp;
	}
    return str;
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
