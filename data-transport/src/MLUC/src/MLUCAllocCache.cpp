/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCAllocCache.hpp"

MLUCAllocCache::MLUCAllocCache( unsigned long size, unsigned cntExp2 ):
    fSize( size ), fFree( cntExp2 ), fUsed( cntExp2 )
    {
    unsigned long i, cnt;
    cnt = 1 << cntExp2;
    uint8* tmp;
    bool failed = false;
    fOk = true;
    for ( i = 0; i < cnt; i++ )
	{
	tmp = new uint8[ size ];
	if ( tmp )
	    fFree.Add( tmp );
	else
	    {
	    failed = true;
	    break;
	    }
	}
    
    if ( failed )
	{
	fOk = false;
	while ( fFree.GetCnt()>0 )
	    {
	    tmp = fFree.GetFirst();
	    delete [] tmp;
	    fFree.RemoveFirst();
	    }
	}
    pthread_mutex_init( &fMutex, NULL );
    }

MLUCAllocCache::~MLUCAllocCache()
    {
    uint8* tmp;
    pthread_mutex_destroy( &fMutex );
    while ( fFree.GetCnt()>0 )
	{
	tmp = fFree.GetFirst();
	delete [] tmp;
	fFree.RemoveFirst();
	}
    }

uint8* MLUCAllocCache::Get()
    {
    pthread_mutex_lock( &fMutex );
    uint8* tmp;
    if ( fFree.GetCnt()>0 )
	{
	tmp = fFree.GetFirst();
	fFree.RemoveFirst();
	fUsed.Add( tmp );
	pthread_mutex_unlock( &fMutex );
	return tmp;
	}
    else
	{
	tmp = new uint8[ fSize ];
	if ( tmp )
	    fAllocated.Add( tmp );
	pthread_mutex_unlock( &fMutex );
	return tmp;
	}
    }
void MLUCAllocCache::Release( uint8* ptr )
    {
    unsigned long ndx;
    pthread_mutex_lock( &fMutex );
    if ( MLUCVectorSearcher<TPtr,TPtr>::FindElement( fUsed, &FindPtr, ptr, ndx ) )
	{
	fUsed.Remove( ndx );
	fFree.Add( ptr );
	}
    else
	{
	if ( MLUCVectorSearcher<TPtr,TPtr>::FindElement( fAllocated, &FindPtr, ptr, ndx ) )
	    {
	    fAllocated.Remove( ndx );
	    delete [] ptr;
	    }
	}
    pthread_mutex_unlock( &fMutex );
    }

bool MLUCAllocCache::FindPtr( const TPtr& ptr, const TPtr& ref )
    {
    return ptr == ref;
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
