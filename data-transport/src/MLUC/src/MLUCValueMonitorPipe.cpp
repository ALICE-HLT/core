/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck 
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCValueMonitorPipe.hpp"
#include <fstream>

MLUCValueMonitorPipe::MLUCValueMonitorPipe( const char* filename, 
					    const char* fileDescrHeader, 
					    const char* output_prefix, 
					    const char* output_suffix, 
					    const char* avg_output_prefix, 
					    const char* avg_output_suffix, 
					    unsigned int average_items, 
					    int what ) :
	
    MLUCValueMonitor( filename, 
		      fileDescrHeader, 
		      output_prefix, 
		      output_suffix, 
		      avg_output_prefix, 
		      avg_output_suffix, 
		      average_items ),
    fMonitorQuantity( what )
    {
    fFirstReadOut   = true;
    fOldSum = 0; 
    fNewSum = 0;	
      
    }

MLUCValueMonitorPipe::MLUCValueMonitorPipe ( const char* output_prefix, 
					     const char* output_suffix, 
					     const char* avg_output_prefix, 
					     const char* avg_output_suffix, 
					     unsigned int average_items,
					     int what ) :
	
    MLUCValueMonitor( output_prefix, 
		      output_suffix, 
		      avg_output_prefix, 
		      avg_output_suffix, 
		      average_items ),
    fMonitorQuantity( what )
    {
    fFirstReadOut = true;
    fOldSum = 0; 
    fNewSum = 0;
    }

MLUCValueMonitorPipe::MLUCValueMonitorPipe ( const char* filename, 
					     const char* fileDescrHeader, 
					     unsigned int average_items,
					     int what ) : 

    MLUCValueMonitor( filename, 
		      fileDescrHeader, 
		      average_items ),
    fMonitorQuantity( what )
    {
    fFirstReadOut = true;
    fOldSum = 0; 
    fNewSum = 0;
    }


MLUCValueMonitorPipe::MLUCValueMonitorPipe ( unsigned int average_items,
					     int what ) : 

    MLUCValueMonitor( average_items ),
    fMonitorQuantity( what )
    {
    fFirstReadOut = true;
    fOldSum = 0; 
    fNewSum = 0;
    }


int MLUCValueMonitorPipe::GetValue( uint64& measuredValue ) 
    {
	
    char   kLine[5000];   
	
    std::ifstream in( "/proc/stat" );
    if( !in ) 
	{
	LOG( MLUCLog::kError, "MLUCValueMonitorPipe:GetValue", "Error opening file" )
	    << "Could not open /proc/stat" << ENDLOG;
	return ENOENT;
	}
    else 
	{
	unsigned long p_in=0, p_out=0;
	bool found = false;



	while( !in.eof() && !found ) 
	    {
	    in.getline( kLine, 5000 );
	    
	    if( sscanf( kLine, "pipe %lu %lu ", &p_in, &p_out ) > 0 ) 
		{
		found = true;
		break;
		}
	    }
	
	fOldSum = fNewSum;
	switch( fMonitorQuantity ) 
	    {
	    case 0:
		fNewSum = p_in + p_out;
		break;
	    case 1: 
		fNewSum = p_in;
		break;
	    case 2:
		fNewSum = p_out;
		break;
	    case 3:
		fNewSum = p_out - p_in;
		break;
	    case 4: 
		fNewSum = (p_in+p_out)/2;
		break;
	    default:
		fNewSum = 0;
		break;
	    }
	
	if( fFirstReadOut ) 
	    {
	    fOldSum = fNewSum;
	    fFirstReadOut = false;
	    return ENOTSUP; // first readout (op not supported)
	    } 
	measuredValue = fNewSum-fOldSum;
	return 0;
	}
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Arne Wiebalck 
**
** $Id$ 
**
***************************************************************************
*/
