/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCTimedTask.hpp"
#include "MLUCTimer.hpp"

MLUCTimedTaskHandler::MLUCTimedTaskHandler():
    fTaskThread( *this ), fTimerThread( *this )
    {
    pthread_mutex_init( &fDataMutex, NULL );
    pthread_mutex_init( &fTaskEndMutex, NULL );
    fTaskStarted = false;
    fTaskEnded = false;
    fTaskTimedOut = false;
    fSignalTaskEnd = true;
    fTimer = NULL;
    fTask = NULL;
    fTaskThread.Start();
    fTimerThread.Start();
    }

MLUCTimedTaskHandler::MLUCTimedTaskHandler( MLUCTimer* timer ):
    fTaskThread( *this ), fTimerThread( *this )
    {
    pthread_mutex_init( &fDataMutex, NULL );
    pthread_mutex_init( &fTaskEndMutex, NULL );
    fTaskStarted = false;
    fTaskEnded = false;
    fTaskTimedOut = false;
    fSignalTaskEnd = true;
    fTask = NULL;
    fTimer = timer;
    //fTaskThread.Start();
    fTimerThread.Start();
    }

MLUCTimedTaskHandler::~MLUCTimedTaskHandler()
    {
    fTaskThread.Abort();
    fTimerThread.Abort();
    pthread_mutex_destroy( &fDataMutex );
    pthread_mutex_destroy( &fTaskEndMutex );
    }

bool MLUCTimedTaskHandler::RunTask( MLUCTimedTask* task, uint32& timeout_ms, bool& taskCompleted, bool waitForCompletion )
    {
    taskCompleted = false;
    if ( !task )
	return false;
    if ( !fTimer )
	return false;
    pthread_mutex_lock( &fDataMutex );
    if ( fTaskStarted )
	{
	pthread_mutex_unlock( &fDataMutex );
	return false;
	}
    fTaskThread.Start();
    fTask = task;
    fTaskStarted = true;
    fSignalTaskEnd = waitForCompletion;
    pthread_mutex_unlock( &fDataMutex );
    fTaskEndedSignal.Lock();
    fTaskStartSignal.Signal();
    fTimer->AddTimeout( timeout_ms, &fTimeoutSignal, 0 );
    if ( !fSignalTaskEnd )
	{
	taskCompleted = false;
	return true;
	}
    fTaskEndedSignal.Wait();
    fTaskEndedSignal.Unlock();
    taskCompleted = !fTaskTimedOut;
    return true;
    }

bool MLUCTimedTaskHandler::RunTask( MLUCTimedTask& task, uint32& timeout_ms, bool& taskCompleted, bool waitForCompletion )
    {
    return RunTask( &task, timeout_ms, taskCompleted, waitForCompletion );
    }

bool MLUCTimedTaskHandler::HasTaskFinished()
    {
    return fTaskEnded;
    }

bool MLUCTimedTaskHandler::HasTaskFinished( bool& taskCompleted )
    {
    taskCompleted = !fTaskTimedOut;
    return fTaskEnded;
    }


void MLUCTimedTaskHandler::RunTask()
    {
    fTaskStartSignal.Wait();
    fTask->DoTask();
    fTimeoutSignal.AddNotificationData( 1 );
    fTimeoutSignal.Signal();
    }

void MLUCTimedTaskHandler::RunTimeout()
    {
    while ( 1 )
	{
	fTimeoutSignal.Wait();
	while ( fTimeoutSignal.HaveNotificationData() )
	    {
	    bool success = (bool)fTimeoutSignal.PopNotificationData();
	    fTimeoutSignal.Unlock();
	    pthread_mutex_lock( &fTaskEndMutex );
	    if ( !fTaskEnded )
		{
		if ( success )
		    {
		    fTaskEnded = true;
		    fTaskTimedOut = false;
		    pthread_mutex_unlock( &fTaskEndMutex );
		    fTask->TaskCompleted();
		    }
		else
		    {
		    fTaskEnded = true;
		    fTaskTimedOut = true;
		    pthread_mutex_unlock( &fTaskEndMutex );
		    fTask->TaskTimedOut( fTaskThread );
		    }
		fTimeoutSignal.Lock();
		if ( fSignalTaskEnd )
		    fTaskEndedSignal.Signal();
		}
	    else
		{
		pthread_mutex_unlock( &fTaskEndMutex );
		fTimeoutSignal.Lock();
		}
	    }
	}
    }

void MLUCTimedTaskHandler::MLUCTimedTaskHandlerTaskThread::Run()
    {
    fTaskHandler.RunTask();
    }

void MLUCTimedTaskHandler::MLUCTimedTaskHandlerTimeoutThread::Run()
    {
    fTaskHandler.RunTimeout();
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
