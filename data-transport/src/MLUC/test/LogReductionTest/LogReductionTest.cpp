/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/


#include "MLUCLog.hpp"
#include "MLUCLogServer.hpp"
#include <cstdlib>
#include <ctime>


void TestFunction0();
void TestFunction1();

void TestFunction0()
    {
    TestFunction1();
    }

static unsigned gCounter=0;

void TestFunction1()
    {
    LOG( MLUCLog::kDebug, "TestFunction1", "Test Output" )
        << "Test output random number " << MLUCLog::kDec << gCounter << " : " << random() << ENDLOG;
    ++gCounter;
    }


int main( int, char** )
    {
    srandom(time(NULL));
    MLUCStdoutLogServer logOut;
    gLog.AddServer( logOut );

#if 1
    gLog.SetStackTraceComparisonDepth( 1 );
#endif

    for ( unsigned i=0; i<20; i++ )
        {
        TestFunction0();
        usleep( 1100000 );
        }
    
    LOG( MLUCLog::kDebug, "main", "Test Output" )
        << "Phase 1 done" << ENDLOG;

    for ( unsigned i=0; i<2000; i++ )
        {
        TestFunction0();
        }

    LOG( MLUCLog::kDebug, "main", "Test Output" )
        << "Phase 2 done" << ENDLOG;

    return 0;
    
    }

    



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
