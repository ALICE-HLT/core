/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCThread.hpp"
#include "MLUCFifo.hpp"
#include "MLUCLog.hpp"
#include "MLUCLogServer.hpp"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <errno.h>


#define OUTPUTFILEREADER "fifo-output-reader.dat"
#define OUTPUTFILEWRITER "fifo-output-writer.dat"

#define MAX_RANDOM_SIZE 131072
#define MAX_FIXED_SIZE 524288
#define MAX_MSG_COUNT 256

#define DATA_SOURCE_SIZE 32768

const unsigned kDataMsg = 1;
const unsigned kQuitMsg = 2;

class FifoTestThread: public MLUCThread
    {
    public:

	FifoTestThread( MLUCFifo& fifo );

	virtual void Run();

	bool HasFinished()
		{
		return fQuitted;
		}

	void Quit()
		{
		fQuit = true;
		}

    protected:

	MLUCFifo& fFifo;

	bool fQuitted;
	bool fQuit;

    private:
    };

FifoTestThread::FifoTestThread( MLUCFifo& fifo ):
    fFifo( fifo )
    {
    fQuitted = true;
    fQuit = false;
    }

void FifoTestThread::Run()
    {
    fQuitted = false;
    MLUCFifoMsg* msg;
    int ret;
    unsigned cnt;
    int fd;
    unsigned long dataCnt=0;
    int repeats = 0;
    fd = open( OUTPUTFILEREADER, O_WRONLY|O_TRUNC|O_CREAT, 0666 );
    if ( fd==-1 )
	{
	LOG( MLUCLog::kError, "FifoTest::Run", "Output File Error" )
	    << "Error opening ouptut file '" << OUTPUTFILEREADER
	    << "': " << strerror(errno) << " (" << MLUCLog::kDec
	    << errno << ")..." << ENDLOG;
	}	
    while ( !fQuit )
	{
	fFifo.WaitForData();
	ret = fFifo.GetNextMsg( msg );
	if ( ret )
	    {
	    LOG( MLUCLog::kError, "FifoTest::Run", "GetNextMsg Error" )
		<< "Error getting the next message: " << strerror(ret)
		<< MLUCLog::kDec << " (" << ret << ")." << ENDLOG;
	    break;
	    }

	switch ( msg->fType )
	    {
	    case kDataMsg:
		LOG( MLUCLog::kDebug, "FifoTest::Run", "Data Read" )
		    << "Msg " << MLUCLog::kDec << dataCnt << " length: "
		    << msg->fLength << "." << ENDLOG;
		dataCnt++;
		if ( fd != -1 )
		    {
		    cnt=0;
		    do
			{
			ret = write( fd, ((uint8*)msg)+cnt, msg->fLength-cnt );
			if ( ret>0 )
			    cnt += ret;
			else
			    {
			    if ( errno==EINTR )
				continue;
			    else
				break;
			    }
			if ( cnt != msg->fLength )
			    repeats++;
			}
		    while ( cnt < msg->fLength && errno== EINTR );
		    if ( cnt != msg->fLength )
			{
			LOG( MLUCLog::kError, "FifoTest::Run", "Data Write Error" )
			    << "Error writing output data to file. Data written: "
			    << MLUCLog::kDec << cnt << " - returned: " 
			    << ret << " - Message: 0x" << MLUCLog::kHex
			    << (unsigned long)msg << " - Message Length: " << MLUCLog::kDec
			    << msg->fLength << " - Error: " << strerror(errno)
			    << " (" << errno << ")." << ENDLOG;
			}
		    }
		break;

	    case kQuitMsg:
		fQuit = true;
		break;
	    }

	ret = fFifo.FreeNextMsg( msg );
	if ( ret )
	    {
	    LOG( MLUCLog::kError, "FifoTest::Run", "FreeNextMsg Error" )
		<< "Error freeing the next message: " << strerror(ret)
		<< MLUCLog::kDec << " (" << ret << ")." << ENDLOG;
	    break;
	    }
	}
    if ( fd != -1 )
	close( fd );
    LOG( MLUCLog::kInformational, "FifoTest::Run", "Write Repeats" )
	<< "File write repeats: " << MLUCLog::kDec << repeats << "." << ENDLOG;
    fQuitted = true;
    }

int main( int, char** )
    {
    MLUCStdoutLogServer stdOut;
    gLog.AddServer( stdOut );
    //gLogLevel = MLUCLog::kAll;
    gLogLevel = MLUCLog::kAll & ~(MLUCLog::kDebug);
    int fd;
    int ret;
    unsigned i, len, fixedSize = 1, cnt, n;
    unsigned long dataCnt = 0;
    int repeats = 0;
    uint8* dataSource;
    uint8* dataDest;

    MLUCFifo fifo( 8, 5, true );
    MLUCFifoMsg* msg;
    MLUCFifoMsg quitMsg;
    MLUCFifoMsg dataMsg;
    FifoTestThread testThread( fifo );
    srandom( time(NULL) );
    quitMsg.fLength = sizeof(MLUCFifoMsg);
    quitMsg.fType = kQuitMsg;
    dataMsg.fLength = sizeof(MLUCFifoMsg);
    dataMsg.fType = kDataMsg;

    dataSource = (uint8*)malloc( DATA_SOURCE_SIZE );
    if ( !dataSource )
	{
	LOG( MLUCLog::kError, "FifoTest", "Output File Error" )
	    << "Out of memory trying to allocate data source of "
	    << MLUCLog::kDec << DATA_SOURCE_SIZE << " (0x"
	    << MLUCLog::kHex << DATA_SOURCE_SIZE << ") bytes."
	    << ENDLOG;
	return -1;
	}

    fd = open( OUTPUTFILEWRITER, O_WRONLY|O_TRUNC|O_CREAT, 0666 );
    if ( fd==-1 )
	{
	LOG( MLUCLog::kError, "FifoTest", "Output File Error" )
	    << "Error opening ouptut file '" << OUTPUTFILEWRITER
	    << "': " << strerror(errno) << " (" << MLUCLog::kDec
	    << errno << ")..." << ENDLOG;
	}

    testThread.Start();

    for ( i = 0; i < MAX_MSG_COUNT; i++ )
	{
	if ( i & 1 )
	    {
	    len = fixedSize;
	    fixedSize <<= 1;
	    if ( fixedSize > MAX_FIXED_SIZE )
		fixedSize = 1;
	    }
	else
	    {
	    len = (unsigned)( (((double)random())/((double)RAND_MAX))*MAX_RANDOM_SIZE );
	    }
	len += dataMsg.fLength;
	ret = fifo.ReserveMsgSlot( len, msg );
	if ( ret == ENOSPC )
	    continue;
	if ( ret )
	    {
	    LOG( MLUCLog::kError, "FifoTest", "Msg Slot Reserve Error" )
		<< "Error reserving message slot for message of size "
		<< MLUCLog::kDec << len << " (0x" << MLUCLog::kHex
		<< len << "): " << strerror(ret)
		<< MLUCLog::kDec << " (" << ret << ")." << ENDLOG;
	    break;
	    }
	memcpy( msg, &dataMsg, dataMsg.fLength );
	msg->fLength = len;
	cnt = dataMsg.fLength;
	dataDest = (uint8*)msg;
	while ( cnt < len )
	    {
	    if ( len-cnt < DATA_SOURCE_SIZE )
		n = len-cnt;
	    else
		n = DATA_SOURCE_SIZE;
	    memcpy( dataDest+cnt, dataSource, n );
	    cnt += n;
	    }
	if ( fd!=-1 )
	    {
	    cnt=0;
	    do
		{
		ret = write( fd, ((uint8*)msg)+cnt, msg->fLength-cnt );
		if ( ret>0 )
		    cnt += ret;
		else
		    {
		    if ( errno==EINTR )
			continue;
		    else
			break;
		    }
		if ( cnt != msg->fLength )
		    {
		    repeats++;
		    }
		}
	    while ( cnt < msg->fLength && errno== EINTR );
	    if ( cnt != msg->fLength )
		{
		LOG( MLUCLog::kError, "FifoTest", "Data Write Error" )
		    << "Error writing output data to file. Data written: "
		    << MLUCLog::kDec << cnt << " - returned: " 
		    << ret << " - Error: " << strerror(errno)
		    << " (" << errno << ")." << ENDLOG;
		}
	    }
	LOG( MLUCLog::kDebug, "FifoTest", "Data Write" )
	    << "Msg " << MLUCLog::kDec << dataCnt << " length: "
	    << msg->fLength << "." << ENDLOG;
	dataCnt++;
	ret = fifo.CommitMsg( msg->fLength );
	if ( ret )
	    {
	    LOG( MLUCLog::kError, "FifoTest", "Msg Commit Error" )
		<< "Error commiting message of size "
		<< MLUCLog::kDec << len << " (0x" << MLUCLog::kHex
		<< len << "): " << strerror(ret)
		<< MLUCLog::kDec << " (" << ret << ")." << ENDLOG;
	    break;
	    }
	}
    
    LOG( MLUCLog::kInformational, "FifoTest::Run", "Write Repeats" )
	<< "File write repeats: " << MLUCLog::kDec << repeats << "." << ENDLOG;
    if ( i == MAX_MSG_COUNT )
	{
	LOG( MLUCLog::kDebug, "FifoTest", "Quitting" )
	    << "Writing ordinary quit message." << ENDLOG;
	fifo.Write( &quitMsg );
	cnt = 1000000;
	}
    else
	{
	LOG( MLUCLog::kWarning, "FifoTest", "Quitting" )
	    << "Writing emergency quit message." << ENDLOG;
	fifo.WriteEmergency( &quitMsg );
	cnt = 100;
	}
    LOG( MLUCLog::kInformational, "FifoTest::Run", "Fifo resizes" )
	<< "Grows: " << fifo.GetGrowCnt() 
	<< " - Shrinks: " << fifo.GetShrinkCnt()
	<< "." << ENDLOG;
    n = 0;
    while ( !testThread.HasFinished() && n++<cnt )
	usleep( 250000 );
    if ( testThread.HasFinished() )
	testThread.Join();
    else
	testThread.Abort();

    
    return 0;
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
