/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/


#include "MLUCLRUList.hpp"
#include "MLUCTests.hpp"
#include <iostream>

MLUCINITTESTS();

struct TestData
    {
	unsigned fNdx;
	unsigned fCnt;
	bool fUsed;
	TestData( unsigned ndx=0 ):
	    fNdx(ndx),
	    fCnt(0),
	    fUsed(false)
		{
		}
	void Update()
		{
		++fCnt;
		fUsed = true;
		}
	bool operator==( const TestData& comp )
		{
		return comp.fNdx==fNdx;
		}
    };

#define SIZE_LIMIT 1
int main( int, char** )
    {
#ifdef SIZE_LIMIT
    MLUCLRUList<TestData> lruList(10);
#else
    MLUCLRUList<TestData> lruList;
#endif
    MLUCLRUList<TestData>::TIterator iter, end;

    iter = lruList.Begin();
    end = lruList.End();
    MLUCTEST( iter==end, "List Content Check" );

    TestData td0(1);
    iter = lruList.Update( td0 );
    MLUCTEST( iter->fNdx==1, "Update Item 1" );

    iter->Update();
    MLUCTEST( iter->fCnt==1, "Update Item 1 Content" );
    MLUCTEST( iter->fUsed, "Update Item 1 Used" );

    unsigned cnt=0;
#if 0
    // Only for fixed size LRU list (not just LRU list with size limit)
    iter = lruList.Begin();
    end = lruList.End();
    ++iter;
    while ( iter != end )
	{
	MLUCTEST( !iter->fUsed, "Update Item 1 Unused Items" );
	MLUCTEST( iter->fCnt==0, "Update Item 1 Unused Item Cnts" );
	++cnt;
	++iter;
	}
    MLUCTEST( cnt==9, "Update Item 1 Unused Item Count" );
#endif
    

    TestData td1(2);
    iter = lruList.Update( td1 );
    MLUCTEST( iter->fNdx==2, "Update Item 2" );
    iter->Update();
    MLUCTEST( iter->fCnt==1, "Update Item 2 Content" );
    MLUCTEST( iter->fUsed, "Update Item 2 Used" );

    iter = lruList.Begin();
    end = lruList.End();
    cnt=0;
    MLUCTEST( iter->fUsed, "Update Item 2 First Item" );
    MLUCTEST( iter->fCnt==1, "Update Item 2 First Item Cnt" );
    MLUCTEST( iter->fNdx==2, "Update Item 2 First Item Content" );
    ++iter;
    MLUCTEST( iter->fUsed, "Update Item 2 Second Item" );
    MLUCTEST( iter->fCnt==1, "Update Item 2 Second Item Cnt" );
    MLUCTEST( iter->fNdx==1, "Update Item 2 Second Item Content" );
#if 0
    // Only for fixed size LRU list (not just LRU list with size limit)
    ++iter;
    while ( iter != end )
	{
	MLUCTEST( !iter->fUsed, "Update Item 2 Unused Items" );
	MLUCTEST( iter->fCnt==0, "Update Item 2 Unused Item Cnts" );
	++cnt;
	++iter;
	}
    MLUCTEST( cnt==8, "Update Item 2 Unused Item Count" );
#endif
    

    iter = lruList.Begin();
    end = lruList.End();
    while ( iter != end )
	{
	*iter = TestData();
	++iter;
	}

    lruList.Update( TestData(11) )->Update();
    for ( unsigned nn=10;nn>=1;nn-- )
	{
	for ( unsigned mm=10;mm>=nn;mm-- )
	    {
	    lruList.Update( TestData(mm) )->Update();
	    }
	}

    iter = lruList.Begin();
    end = lruList.End();
    cnt=1;
    while ( iter != end )
	{
	MLUCTEST( iter->fUsed, "Update All Items Item Used" );
	MLUCTEST( iter->fCnt==cnt, "Update All Items Item Cnt" );
	if ( !MLUCLASTTESTOK() )
	    {
	    std::cerr << "iter->fNdx: " << iter->fNdx << " - iter->fCnt: " << iter->fCnt << " - cnt: " << cnt << std::endl;
	    }
	MLUCTEST( iter->fNdx==cnt, "Update All Items Item Content" );
	if ( !MLUCLASTTESTOK() )
	    {
	    std::cerr << "iter->fNdx: " << iter->fNdx << " - iter->fCnt: " << iter->fCnt << " - cnt: " << cnt << std::endl;
	    }
	++cnt;
	++iter;
	}

#ifdef SIZE_LIMIT
    MLUCTEST( 11==cnt, "Update All Items Item Count" );
#else
    MLUCTEST( 12==cnt, "Update All Items Item Count" );
#endif

    MLUCTESTEND();
    }

    



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
