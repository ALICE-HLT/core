/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCMultiReaderMutex.hpp"
#include "MLUCThread.hpp"
#include "MLUCMutex.hpp"
#include <vector>
#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <iostream>
#include <climits>
#include <unistd.h>

#if 0
#define EXHAUSTIVE_TEST
#endif
#if 0
#define VERBOSE
#endif

#ifdef EXHAUSTIVE_TEST
const unsigned gkMaxSleepTime_us = 1000; // 0.001 s
const unsigned gkMaxWriterCount = 200;
const unsigned gkMaxReaderCount = 400;
const unsigned gkTestDuration = 100000000; // 100 s
#else
const unsigned gkMaxSleepTime_us = 100; // 0.001 s
const unsigned gkMaxWriterCount = 20;
const unsigned gkMaxReaderCount = 40;
const unsigned gkTestDuration = 1000000; // 100 s
#endif

class TestThread: public MLUCThread
    {
    public:
	TestThread( MLUCMultiReaderMutex& mutex, bool writer, unsigned& readerCount, unsigned& writerCount, MLUCMutex& safetyMutex, unsigned& violationCount, bool& quit ):
	    fMutex(mutex),
	    fWriter(writer),
	    fReaderCount(readerCount),
	    fWriterCount(writerCount),
	    fSafetyMutex( safetyMutex ),
	    fViolationCount( violationCount ),
	    fQuit( quit ),
	    fIterationCount(0)
		{
		}

	unsigned long GetIterationCount() const
		{
		return fIterationCount;
		}

    protected:

	virtual void Run()
		{
		std::cout << "Starting " << pthread_self() << std::endl;
		RandomSleep();
		while ( !fQuit )
		    {
		    if ( fWriter )
			{
#ifdef MULTIREADERMUTEX_DEBUG
			fMutex.PrintDebugState();
#endif
			MLUCMultiReaderMutex::TWriteLocker writeLock( fMutex );
			unsigned readerCount, writerCount;
			bool violation=false;
			if ( true )
			    {
			    MLUCMutex::TLocker safetyLock( fSafetyMutex );
			    ++fWriterCount;
			    writerCount = fWriterCount;
			    readerCount = fReaderCount;
			    if ( fWriterCount!=1 || fReaderCount>0 )
				violation = true;
			    if ( violation )
				++fViolationCount;
			    Output( "writer enter", readerCount, writerCount, violation, fViolationCount );
			    }
#ifdef MULTIREADERMUTEX_DEBUG
			fMutex.PrintDebugState();
#endif
		
			RandomSleep();
			violation = false;
#ifdef MULTIREADERMUTEX_DEBUG
			fMutex.PrintDebugState();
#endif

			if ( true )
			    {
			    MLUCMutex::TLocker safetyLock( fSafetyMutex );
			    writerCount = fWriterCount;
			    readerCount = fReaderCount;
			    if ( fWriterCount!=1 || fReaderCount>0 )
				violation = true;
			    if ( violation )
				++fViolationCount;
			    --fWriterCount;
			    Output( "writer leave", readerCount, writerCount, violation, fViolationCount );
			    }
#ifdef MULTIREADERMUTEX_DEBUG
			fMutex.PrintDebugState();
#endif

			}
		    else
			{
#ifdef MULTIREADERMUTEX_DEBUG
			fMutex.PrintDebugState();
#endif

			MLUCMultiReaderMutex::TReadLocker readLock( fMutex );
			unsigned readerCount, writerCount;
			bool violation=false;
			if ( true )
			    {
			    MLUCMutex::TLocker safetyLock( fSafetyMutex );
			    ++fReaderCount;
			    writerCount = fWriterCount;
			    readerCount = fReaderCount;
			    if ( fWriterCount>0 )
				violation = true;
			    if ( violation )
				++fViolationCount;
			    Output( "reader enter", readerCount, writerCount, violation, fViolationCount );
			    }
#ifdef MULTIREADERMUTEX_DEBUG
			fMutex.PrintDebugState();
#endif
			    
			RandomSleep();
			violation = false;
#ifdef MULTIREADERMUTEX_DEBUG
			fMutex.PrintDebugState();
#endif

			if ( true )
			    {
			    MLUCMutex::TLocker safetyLock( fSafetyMutex );
			    writerCount = fWriterCount;
			    readerCount = fReaderCount;
			    if ( fWriterCount>0 || fReaderCount<1 )
				violation = true;
			    if ( violation )
				++fViolationCount;
			    --fReaderCount;
			    Output( "reader leave", readerCount, writerCount, violation, fViolationCount );
			    }
#ifdef MULTIREADERMUTEX_DEBUG
			fMutex.PrintDebugState();
#endif
			}
#ifdef MULTIREADERMUTEX_DEBUG
		    fMutex.PrintDebugState();
#endif
		    RandomSleep();
		    ++fIterationCount;
#ifdef MULTIREADERMUTEX_DEBUG
		    fMutex.PrintDebugState();
#endif
		    }
		
		}

#ifdef VERBOSE
	void Output( const char* text, unsigned readerCount, unsigned writerCount, bool violation, unsigned violationCount )
#else
	void Output( const char*, unsigned, unsigned, bool, unsigned )
#endif
		{
#ifdef VERBOSE
		std::cout << "Thread " << (unsigned)GetThreadID() << " " << text << " - readers: " << readerCount
			  << " - writers: " << writerCount << " - violation: " << (violation ? "yes" : "no")
			  << " - violation count: " << violationCount << std::endl;
#endif
		}

	void RandomSleep()
		{
		unsigned long long sleepTime = ((unsigned long long)gkMaxSleepTime_us*(unsigned long long)random())/ULONG_MAX;
		usleep( (useconds_t)sleepTime );
		}

	MLUCMultiReaderMutex& fMutex;
	bool fWriter;
	unsigned& fReaderCount;
	unsigned& fWriterCount;

	MLUCMutex& fSafetyMutex;
	unsigned& fViolationCount;

	bool& fQuit;

	unsigned long fIterationCount;
	
    };


int main( int /*argc*/, char** /*argv*/ )
    {
    srandom( time(NULL) );

    unsigned readerCount = (unsigned)( ((unsigned long long)gkMaxReaderCount*(unsigned long long)random())/UINT_MAX );
    unsigned writerCount = (unsigned)( ((unsigned long long)gkMaxWriterCount*(unsigned long long)random())/UINT_MAX );
    std::cout << readerCount << " reader threads - " << writerCount << " writer threads" << std::endl;

    std::vector<TestThread*> testThreads;
    testThreads.reserve( readerCount+writerCount );

    MLUCMultiReaderMutex testMutex;
    unsigned readersInLock=0, writersInLock=0;
    MLUCMutex safetyMutex;
    unsigned violationCount=0;
    bool quit=false;

    for ( unsigned nr=0; nr<readerCount; ++nr )
	{
	testThreads.push_back( new TestThread( testMutex, false, readersInLock, writersInLock, safetyMutex, violationCount, quit ) );
	}

    for ( unsigned nw=0; nw<writerCount; ++nw )
	{
	testThreads.push_back( new TestThread( testMutex, true, readersInLock, writersInLock, safetyMutex, violationCount, quit ) );
	}

    for ( unsigned nt=0; nt<(readerCount+writerCount); ++nt )
	{
	testThreads[nt]->Start();
	}



    usleep( gkTestDuration );
    quit = true;

    for ( unsigned nr=0; nr<readerCount; ++nr )
	{
	testThreads[nr]->Join();
	std::cout << "Reader thread " << nr << " " << testThreads[nr]->GetIterationCount() << " iterations" << std::endl;
	delete testThreads[nr];
	testThreads[nr] = NULL;
	}


    for ( unsigned nw=0; nw<writerCount; ++nw )
	{
	testThreads[readerCount+nw]->Join();
	std::cout << "Writer thread " << nw << " " << testThreads[readerCount+nw]->GetIterationCount() << " iterations" << std::endl;
	delete testThreads[readerCount+nw];
	testThreads[readerCount+nw] = NULL;
	}


    if ( violationCount<=0 )
	std::cout << "No violations found" << std::endl;
    else
	std::cout << violationCount << " violations found" << std::endl;

    return violationCount;
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
