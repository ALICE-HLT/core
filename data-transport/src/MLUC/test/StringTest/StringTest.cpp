/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCString.hpp"
#include "MLUCLog.hpp"
#include "MLUCLogServer.hpp"

int main( int, char** )
    {
    MLUCStdoutLogServer sOut;
    gLog.AddServer( sOut );


    MLUCString str1;

    LOG( MLUCLog::kDebug, "StringTest", "String 1.1" )
	<< "str1: '" << str1.c_str() << "'" << ENDLOG;

    MLUCString str2( "Test" );
    LOG( MLUCLog::kDebug, "StringTest", "String 2.1" )
	<< "str2: '" << str2.c_str() << "'" << ENDLOG;
    if ( str1 == str2 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str1 == str2" << ENDLOG;
	}
    if ( str1 != str2 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str1 != str2" << ENDLOG;
	}

    MLUCString str3( str2 );
    LOG( MLUCLog::kDebug, "StringTest", "String 3.1" )
	<< "str3: '" << str3.c_str() << "'" << ENDLOG;
    if ( str1 == str3 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str1 == str3" << ENDLOG;
	}
    if ( str1 != str3 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str1 != str3" << ENDLOG;
	}
    if ( str2 == str3 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str2 == str3" << ENDLOG;
	}
    if ( str2 != str3 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str2 != str3" << ENDLOG;
	}

    str1 = "Test1";
    LOG( MLUCLog::kDebug, "StringTest", "String 1.2" )
	<< "str1: '" << str1.c_str() << "'" << ENDLOG;
    if ( str1 == str2 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str1 == str2" << ENDLOG;
	}
    if ( str1 != str2 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str1 != str2" << ENDLOG;
	}
    if ( str1 == str3 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str1 == str3" << ENDLOG;
	}
    if ( str1 != str3 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str1 != str3" << ENDLOG;
	}

    str3 = "Test3";
    LOG( MLUCLog::kDebug, "StringTest", "String 3.2" )
	<< "str3: '" << str3.c_str() << "'" << ENDLOG;
    if ( str1 == str3 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str1 == str3" << ENDLOG;
	}
    if ( str1 != str3 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str1 != str3" << ENDLOG;
	}
    if ( str2 == str3 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str2 == str3" << ENDLOG;
	}
    if ( str2 != str3 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str2 != str3" << ENDLOG;
	}

    str2 = str3;
    LOG( MLUCLog::kDebug, "StringTest", "String 2.2" )
	<< "str2: '" << str2.c_str() << "'" << ENDLOG;
    if ( str1 == str2 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str1 == str2" << ENDLOG;
	}
    if ( str1 != str2 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str1 != str2" << ENDLOG;
	}
    if ( str2 == str3 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str2 == str3" << ENDLOG;
	}
    if ( str2 != str3 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str2 != str3" << ENDLOG;
	}

    str2 += str1;
    LOG( MLUCLog::kDebug, "StringTest", "String 2.3" )
	<< "str2: '" << str2.c_str() << "'" << ENDLOG;
    if ( str1 == str2 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str1 == str2" << ENDLOG;
	}
    if ( str1 != str2 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str1 != str2" << ENDLOG;
	}
    if ( str2 == str3 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str2 == str3" << ENDLOG;
	}
    if ( str2 != str3 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str2 != str3" << ENDLOG;
	}

    str1 += "Test2";
    LOG( MLUCLog::kDebug, "StringTest", "String 1.3" )
	<< "str1: '" << str1.c_str() << "'" << ENDLOG;
    if ( str1 == str2 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str1 == str2" << ENDLOG;
	}
    if ( str1 != str2 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str1 != str2" << ENDLOG;
	}
    if ( str1 == str3 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str1 == str3" << ENDLOG;
	}
    if ( str1 != str3 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str1 != str3" << ENDLOG;
	}

    MLUCString str4;
    if ( str1 == str4 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str1 == str4" << ENDLOG;
	}
    if ( str1 != str4 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str1 != str4" << ENDLOG;
	}
    if ( str2 == str4 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str2 == str4" << ENDLOG;
	}
    if ( str2 != str4 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str2 != str4" << ENDLOG;
	}
    if ( str3 == str4 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str3 == str4" << ENDLOG;
	}
    if ( str3 != str4 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str3 != str4" << ENDLOG;
	}
    str4 += str3;
    LOG( MLUCLog::kDebug, "StringTest", "String 4.1" )
	<< "str4: '" << str4.c_str() << "'" << ENDLOG;
    if ( str1 == str4 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str1 == str4" << ENDLOG;
	}
    if ( str1 != str4 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str1 != str4" << ENDLOG;
	}
    if ( str2 == str4 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str2 == str4" << ENDLOG;
	}
    if ( str2 != str4 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str2 != str4" << ENDLOG;
	}
    if ( str3 == str4 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str3 == str4" << ENDLOG;
	}
    if ( str3 != str4 )
	{
	LOG( MLUCLog::kDebug, "StringTest", "String Compare" )
	    << "str3 != str4" << ENDLOG;
	}


    str4 = "0:A;1:B;2:C;";
    std::vector<MLUCString> tokens;
    str4.Split( tokens, ';' );
    std::vector<MLUCString>::iterator iter, end;
    iter = tokens.begin();
    end = tokens.end();
    while ( iter != end )
	{
	LOG( MLUCLog::kDebug, "StringTest", "Split Test" )
	    << "str4: '" << str4.c_str() << "'" 
	    << " - token: '" << iter->c_str() << "'"
	    << ENDLOG;
	std::vector<MLUCString> parts;
	iter->Split( parts, ':' );
	if ( parts.size()==2 )
	    {
	    LOG( MLUCLog::kDebug, "StringTest", "Split Test" )
		<< "   part[0]: '" << parts[0].c_str() << "'"
		<< " - part[1]: '" << parts[1].c_str() << "'"
		<< ENDLOG;
	    }
	iter++;
	}

    return 0;
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
