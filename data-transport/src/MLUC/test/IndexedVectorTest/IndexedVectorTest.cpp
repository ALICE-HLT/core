/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCIndexedVector.hpp"
#include <vector>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

//#define SMALL_TEST
#ifdef SMALL_TEST
#define MAX_ADD_CNT 8UL
#else
#define MAX_ADD_CNT 1024UL
#endif

#define USE_ID_COUNTER
//#define OLD_STATES

struct TestData
    {
	uint64 fID;
	uint64 f0;
	uint32 f1;
	uint32 f2;
	uint32 f3;
	uint32 f4;
    };

MLUCString& operator<<( MLUCString& str, const unsigned long l )
    {
    char tmp[4096];
    sprintf( tmp, "%lu", l );
    str += tmp;
    return str;
    }

unsigned long gen_rand( unsigned long max )
    {
    return (unsigned long) (((double)max)*rand()/(RAND_MAX+1.0));
    }

int main( int argc, char** argv )
    {
#ifdef SMALL_TEST
    //MLUCIndexedVector<TestData,uint64> storage( 2, 7 );
    MLUCIndexedVector<TestData,uint64> storage( 2, 3 );
#else
    //MLUCIndexedVector<TestData,uint64> storage( 8, 16 );
    MLUCIndexedVector<TestData,uint64> storage( 10, 9 );
#endif
    std::vector<uint64> stored;
#ifdef OLD_STATES
    std::vector<MLUCString> lastStorageStates;
    MLUCString storageState;
    const unsigned long storageStateLimit = 4;
#endif
#ifdef USE_ID_COUNTER
    uint64 idCounter = 0ULL;
#endif

    if ( argc != 1 && argc != 2 )
	{
	fprintf( stderr, "Usage: %s (<seed>)\n", argv[0] );
	return -1;
	}
    int seed;
    if ( argc == 2 )
	{
	char* cpErr = NULL;
	seed = (int)strtol( argv[1], &cpErr, 0 );
	if ( *cpErr )
	    {
	    fprintf( stderr, "Error converting seed argument '%s'\n", argv[1] );
	    return -1;
	    }
	}
    else
	seed = time(NULL);
    printf( "Using seed %d\n", seed );

    srand( seed );

    unsigned long cnt = 0;

#ifdef OLD_STATES
    storage.AsString( storageState );
    lastStorageStates.push_back( storageState );
#endif

    while ( 1 )
	{
	unsigned long addCnt = gen_rand( MAX_ADD_CNT );
	printf( "Adding %lu numbers\n", addCnt );
	for ( unsigned long n = 0; n < addCnt; n++ )
	    {
	    TestData data;
	    bool found;
	    uint64 newID;
	    do
		{
		found = false;
#ifdef USE_ID_COUNTER
		newID = idCounter++;
#else
		newID = gen_rand( ULONG_MAX );
		newID <<= 32;
		newID |= gen_rand( ULONG_MAX );
#endif
		std::vector<uint64>::iterator iter, end;
		iter = stored.begin();
		end = stored.end();
		while ( iter != end )
		    {
		    if ( *iter == newID )
			{
			found=true;
			break;
			}
		    iter++;
		    }
		}
	    while ( found );
	    data.fID = data.f0 = newID;
	    unsigned long dummy;
	    if ( storage.FindElement( newID, dummy ) )
		{
		printf( "Error index %Lu reported as found, which should not be contained in the storage.\n", (unsigned long long)newID );
		MLUCString str;
		storage.AsString( str );
#ifdef OLD_STATES
		for ( unsigned ii=0; ii<lastStorageStates.size(); ii++ )
		    printf( "%s\n\n", lastStorageStates[ii].c_str() );
#endif
		printf( "%s\n", str.c_str() );
		return -1;
		}
	    if ( !storage.Add( data, newID ) )
		{
		printf( "Error adding %Lu to storage\n", (unsigned long long)newID );
		MLUCString str;
		storage.AsString( str );
#ifdef OLD_STATES
		for ( unsigned ii=0; ii<lastStorageStates.size(); ii++ )
		    printf( "%s\n\n", lastStorageStates[ii].c_str() );
#endif
		printf( "%s\n", str.c_str() );
		return -1;
		}
#ifdef OLD_STATES
	    storage.AsString( storageState );
	    lastStorageStates.push_back( storageState );
	    while ( lastStorageStates.size() > storageStateLimit )
		lastStorageStates.erase( lastStorageStates.begin() );
#endif
	    stored.push_back( newID );
	    cnt++;
	    }
	if ( cnt != (unsigned long)stored.size() )
	    {
	    printf( "Internal Error, size mismatch cnt %lu <-> stored.size() %lu\n", 
		     cnt, (unsigned long)stored.size() );
	    return -1;
	    }
	if ( cnt != storage.GetCnt() )
	    {
	    printf( "Error storage thinks it contains %lu members while it should contain %lu\n", 
		     storage.GetCnt(), cnt );
	    MLUCString str;
	    storage.AsString( str );
#ifdef OLD_STATES
	    for ( unsigned ii=0; ii<lastStorageStates.size(); ii++ )
		printf( "%s\n\n", lastStorageStates[ii].c_str() );
#endif
	    printf( "%s\n", str.c_str() );
	    return -1;
	    }
	printf( "%lu entries in storage\n", cnt );
	unsigned long remCnt = gen_rand( cnt+1 );
	printf( "Removing %lu entries.\n", remCnt );
	for ( unsigned long i = 0; i < remCnt; i++ )
	    {
	    unsigned long indexNdx = gen_rand( stored.size() );
	    uint64 index = stored[indexNdx];
	    stored.erase( (stored.begin())+indexNdx );
	    if ( storage.FindElement( index, indexNdx ) )
		{
		TestData *data;
		data = storage.GetPtr( indexNdx );
		if ( data->fID != index )
		    {
		    printf( "Error element at index %lu supposed to have id %Lu has id %Lu\n", indexNdx,
			    (unsigned long long)index, (unsigned long long)data->fID );
		    MLUCString str;
		    storage.AsString( str );
#ifdef OLD_STATES
		    for ( unsigned ii=0; ii<lastStorageStates.size(); ii++ )
			printf( "%s\n\n", lastStorageStates[ii].c_str() );
#endif
		    printf( "%s\n", str.c_str() );
		    return -1;
		    }
		if ( data->f0 != index )
		    {
		    printf( "Error element at index %lu supposed to contain %Lu contains %Lu\n", indexNdx,
			    (unsigned long long)index, (unsigned long long)data->f0 );
		    MLUCString str;
		    storage.AsString( str );
#ifdef OLD_STATES
		    for ( unsigned ii=0; ii<lastStorageStates.size(); ii++ )
			printf( "%s\n\n", lastStorageStates[ii].c_str() );
#endif
		    printf( "%s\n", str.c_str() );
		    return -1;
		    }
		if ( !storage.Remove( indexNdx ) )
		    {
		    printf( "Error removing %Lu from storage\n", (unsigned long long)index );
		    MLUCString str;
		    storage.AsString( str );
#ifdef OLD_STATES
		    for ( unsigned ii=0; ii<lastStorageStates.size(); ii++ )
			printf( "%s\n\n", lastStorageStates[ii].c_str() );
#endif
		    printf( "%s\n", str.c_str() );
		    return -1;
		    }
#ifdef OLD_STATES
		storage.AsString( storageState );
		lastStorageStates.push_back( storageState );
		while ( lastStorageStates.size() > storageStateLimit )
		    lastStorageStates.erase( lastStorageStates.begin() );
#endif
		cnt--;
		}
	    else
		{
		printf( "Error stored index %Lu could not be found.\n", (unsigned long long)index );
		MLUCString str;
		storage.AsString( str );
#ifdef OLD_STATES
		for ( unsigned ii=0; ii<lastStorageStates.size(); ii++ )
		    printf( "%s\n\n", lastStorageStates[ii].c_str() );
#endif
		printf( "%s\n", str.c_str() );
		return -1;
		}
	    }
	if ( cnt != (unsigned long)stored.size() )
	    {
	    printf( "Internal Error, size mismatch cnt %lu <-> stored.size() %lu\n", 
		     cnt, (unsigned long)stored.size() );
	    return -1;
	    }
	if ( cnt != storage.GetCnt() )
	    {
	    printf( "Error storage thinks it contains %lu members while it should contain %lu\n", 
		     storage.GetCnt(), cnt );
	    MLUCString str;
	    storage.AsString( str );
#ifdef OLD_STATES
	    for ( unsigned ii=0; ii<lastStorageStates.size(); ii++ )
		printf( "%s\n\n", lastStorageStates[ii].c_str() );
#endif
	    printf( "%s\n", str.c_str() );
	    return -1;
	    }
	printf( "%lu entries in storage\n", cnt );
	}

    
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
