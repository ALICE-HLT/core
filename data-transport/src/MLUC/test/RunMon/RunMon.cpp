/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck,
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: Monitor.cpp,v 1.27 2003/02/20 11:14:38 timm Exp $ 
**
***************************************************************************
*/

#include "MLUCValueMonitor.hpp"
#include "MLUCValueMonitorCPU.hpp"
#include "MLUCValueMonitorNet.hpp"
#include "MLUCValueMonitorPipe.hpp"
#include "MLUCValueMonitorNBD.hpp"
#include "MLUCValueMonitorDisk.hpp"
#include "MLUCValueMonitorProcess.hpp"
#include "MLUCValueMonitorMem.hpp"
#include "MLUCValueMonitorInterrupt.hpp"
#include "MLUCValueMonitorInterruptCycles.hpp"
#include "MLUCValueMonitorContextSwitches.hpp"
#include "MLUCValueMonitorCycles.hpp"
#include "MLUCLog.hpp"
#include "MLUCLogServer.hpp"
#include <sys/utsname.h>
#include <unistd.h>
#include <fstream>
#include <sstream>
#include <string>
#include <set>
#include <algorithm>
#include <cstring>
#include <cstdio>
#include <cstdlib>

/*
  A note to ease ouptut formatting:
  The first character of the output string should be in column 40 and the last
  one in column 66, to ensure proper alignment in the output.
*/

using namespace std;

bool GetFilenameFromPid(int pid, string& filename);

int main( int argc, char** argv )
    {
    MLUCStdoutLogServer logOut;
    gLog.AddServer( logOut );

    vector<MLUCValueMonitor*> monitors;
    int i;
    const char* errorStr = NULL;
    char* cperr;
    struct timeval now;
    struct tm* t;
    unsigned int date, time_s, time_us;
    gettimeofday( &now, NULL );
    t = localtime( &(now.tv_sec) );
    t->tm_mon++;
    t->tm_year += 1900;
    date = t->tm_year*10000+t->tm_mon*100+t->tm_mday;
    time_s = t->tm_hour*10000+t->tm_min*100+t->tm_sec;
    time_us = now.tv_usec;
    char filePrefix[128];
    char hostName[ SYS_NMLN+1 ];

    string command( "" );
    bool cmdStarted = false;
    vector<long> cpuIDs;
    vector<long> netIDs;
    vector<long> diskIDs;
    bool monitorCPU_USR = false;
    bool monitorCPU_NICE = false;
    bool monitorCPU_SYS = false;
    bool monitorCPU_IDLE = false;
    bool monitorCPU_USED = false;
    bool monitorNet_Bytes_In = false;
    bool monitorNet_Bytes_Out = false;
    bool monitorNet_Packets_In = false;
    bool monitorNet_Packets_Out = false;
    bool monitorNet_Errors_In = false;
    bool monitorNet_Errors_Out = false;
    bool monitorNet_Collisions_Out = false;
    bool monitorNet_Total_Errors = false;
    bool monitorPipe_In = false;
    bool monitorPipe_Out = false;
    bool monitorPipe_Total = false;
    bool monitorPipe_Diff = false;
    bool monitorPipe_Avg = false;
    bool monitorNet = false;
    unsigned int moniCount = 0;
    bool allCPUs = false;
    bool allNets = false;
    char* testID = NULL;
    bool monitorNBD = false;
    bool monitorDisk_Reqs_Read      = false;
    bool monitorDisk_Reqs_Written   = false;
    bool monitorDisk_Blocks_Read    = false;
    bool monitorDisk_Blocks_Written = false;
    bool monitorDisk_Bytes_Read     = false;   // this is based on a assumed
    bool monitorDisk_Bytes_Written  = false;   // block size of 512 bytes

    bool monitorInterrupt       = false;
    vector<string> IntrIds;
    vector<int>    IntrCpuIds;

    bool monitorInterruptCycles  = false;
    vector<string> IntrCyclesIds;
    vector<int>    IntrCyclesCpuIds;

    bool monitorContextSwitches = false;

    bool monitorCyclesUser = false;
    bool monitorCyclesIntr = false;
    bool monitorCyclesSoftIRQ = false;
    bool monitorCyclesIdle = false;
    bool monitorCyclesUsed = false;

    const char* usage = ""\
""\
"Usage: RunMon Options, where Options can be \n"\
""\
"  (-command <command>)                              Specifies the command to\n"
"                                                      run.\n"\
"  (-cpu-usr)                                        Monitor CPU usage by user\n"\
"                                                      processes (all CPUs if\n"\
"                                                      cpu-id is not specified).\n"\
"  (-cpu-nice)                                       Monitor CPU usage by nice\n"\
"                                                      processes (all CPUs if\n"\
"                                                      cpu-id is not specified).\n"\
"  (-cpu-sys)                                        Monitor CPU usage by system\n"\
"                                                      processes (all CPUs if\n"\
"                                                      cpu-id is not specified).\n"\
"  (-cpu-idle)                                       Monitor CPU usage by idle\n"\
"                                                      processes (all CPUs if\n"\
"                                                      cpu-id is not specified).\n"\
"  (-cpu-used)                                       Monitor CPU usage (all CPUs\n"\
"                                                       if cpu-id is not specified).\n"\
"  (-cpu-id)                                         Specify the cpu. If not\n"\
"                                                      specified overall usage\n"\
"                                                      will be taken. Use \"all\"\n"\
"                                                      to also see overall usage.\n"\
"  (-net)                                            Monitor overall (all devs, \n"\
"                                                      in and out net) traffic. \n"\
"  (-net-bytes-in)                                   Monitor incoming bytes (all \n"\
"                                                      devs if net-id is not \n"\
"                                                      specified).\n"\
"  (-net-bytes-out)                                  Monitor outgoing bytes (all \n"\
"                                                      devs if net-id is \n"\
"                                                      not specified). \n"\
"  (-net-packets-in)                                 Monitor incoming packets \n"\
"                                                      (all devs if net-id is\n"\
"                                                      not specified). \n"\
"  (-net-packets-out)                                Monitor outgoing packets \n"\
"                                                      (all devs if net-id is\n"\
"                                                      not specified). \n"\
"  (-net-errors-in)                                  Monitor incoming errors \n"\
"                                                      (all devs if net-id is\n"\
"                                                      not specified). \n"\
"  (-net-errors-out)                                 Monitor outgoing errors \n"\
"                                                      (all devs if net-id is\n"\
"                                                      not specified). \n"\
"  (-net-collisions-out)                             Monitor outgoing collisions \n"\
"                                                      (all devs if net-id is\n"\
"                                                      not specified). \n"\
"  (-net-total-errors)                               Monitor overall erros and collisions  \n"\
"                                                      (all devs if net-id is\n"\
"                                                      not specified). \n"\
"  (-net-id <network interface number>)              Specify the network \n"\
"                                                      interface. If not \n"\
"                                                      specified overall \n"\
"                                                      traffic will be taken.\n"\
"                                                      Use \"all\" to also see \n"\
"                                                      overall usage.\n"\
"  (-pipe-in)                                        Monitor the amount of data \n"\
"                                                      written into pipes.\n"\
"  (-pipe-out)                                       Monitor the amount of data \n"\
"                                                      read from pipes.\n"\
"  (-pipe-total)                                     Monitor the amount of data \n"\
"                                                      read from and written to \n"\
"                                                      pipes.\n"\
"  (-pipe-diff)                                      Monitor the difference in the\n"\
"                                                      amounts of data written to \n"\
"                                                      and read from pipes.\n"\
"  (-pipe-avg)                                       Monitor the average between \n"\
"                                                      the amounts of data read \n"\
"                                                      from and written to pipes.\n"\
"  (-nbd)                                            Monitor nbd throughput (only\n"\
"                                                      device a is supported).\n"\
"  (-disk-reqs-read)                                 Monitor the number of disk\n"\
"                                                      read requests.\n"\
"  (-disk-reqs-written)                              Monitor the number of disk\n"\
"                                                      write reqs.\n"\
"  (-disk-blocks-read)                               Monitor the number of blocks\n"\
"                                                      read from disk.\n"\
"  (-disk-blocks-written)                            Monitor the number of blocks\n"\
"                                                      written to disk.\n"\
"  (-disk-bytes-read)                                Monitor the number of bytes\n"\
"                                                      read from disk. CAUTION:\n"\
"                                                      this is based on a assumed\n"\
"                                                      block size of 512 bytes!\n"\
"  (-disk-bytes-written)                             Monitor the number of bytes\n"\
"                                                      written to disk. CAUTION:\n"\
"                                                      this is based on a assumed\n"\
"                                                      block size of 512 bytes!\n"\
"  (-disk-id <minor device id>)                      Specify the disk, default is\n"\
"                                                      0, normally the first ide \n"\
"                                                      disk\n"\
"  (-cycles-user)                                    Monitor the cycles used by all\n"\
"                                                      processes except the idle process.\n"
"  (-cycles-intr)                                    Monitor the cycles used by all\n"\
"                                                      interrupt handlers.\n"
"  (-cycles-softirq)                                 Monitor the cycles used by all\n"\
"                                                      softirqs (and tasklets).\n"
"  (-cycles-idle)                                    Monitor the cycles used by the\n"\
"                                                    idle process.\n"
"  (-cycles-used)                                    Monitor the global number\n"
"                                                      cycles used.\n"
"  (-intr [id])                                      Monitor the specified \n"
"                                                      interrupt. id is the name\n"
"                                                      of the interrupt. Use \n"
"                                                      \"all\" to monitor the sum\n"
"                                                      of all interrupts. If no \n"
"                                                      id is specified \"all\"\n"
"                                                      is assumed as id.\n"
"  (-intr-cpu-id <cpu id>)                           Specify the CPU for \n"
"                                                      interrupt monitoring. If\n"
"                                                      not specified sum over all\n"
"                                                      CPUs of the interrupt \n"
"                                                      specified by -intr will be\n"
"                                                      monitored. Use also \"all\"\n"
"                                                      to get this effect.\n"
"  (-intr-cycles [id])                               Monitor the CPU cycles\n"
"                                                      used by the specified\n"
"                                                      interrupt. id is the name\n"
"                                                      of the interrupt. Use \n"
"                                                      \"all\" to monitor the sum\n"
"                                                      of all interrupts. If no \n"
"                                                      id is specified \"all\"\n"
"                                                      is assumed as id.\n"
"  (-intr-cycles-cpu-id <cpu id>)                    Specify the CPU for\n"
"                                                      interrupt cycles\n"
"                                                      monitoring. If not\n"
"                                                      specified sum over all\n"
"                                                      CPUs of the interrupt \n"
"                                                      specified by -intr-cycles\n"
"                                                      will be monitored. Use\n"
"                                                      also \"all\" to get this\n"
"                                                      effect.\n"
"  (-context)                                        Monitor number of context \n"
"                                                      switches.\n";

    /*
"  (-pid <process identifier>)                       Specify the process id. \n"
"                                                      This is needed for all \n"
"                                                      args beginning with \n"
"                                                      -proc-*. \n"
"  (-proc-cpu)                                       Monitor the CPU usage of a\n"
"                                                      process. The process is \n"
"                                                      identified by its pid \n"
"                                                      (see -pid).\n"
"  (-proc-cpu-sys)                                   Monitor the CPU usage of a \n"
"                                                      process spend in kernel\n"
"                                                      mode. The process is \n"
"                                                      identified by its pid \n"
"                                                      (see -pid).\n"
"  (-proc-cpu-usr)                                   Monitor the CPU usage of a \n"
"                                                      process spend in user\n"
"                                                      mode. The process is \n"
"                                                      identified by its pid \n"
"                                                      (see -pid).\n"
"  (-proc-mem-total)                                   Monitor the total program \n"
"                                                      size. The process is \n"
"                                                      identified by its pid \n"
"                                                      (see -pid).\n"
"  (-proc-mem-resident)                              Monitor the resident set \n"
"                                                      size of a process. The \n"
"                                                      process is identified by \n"
"                                                      its pid (see -pid).\n"
"  (-proc-mem-shared)                                Monitor the shared memory \n"
"                                                      size of a process. The \n"
"                                                      process is identified by \n"
"                                                      its pid (see -pid).\n"
"  (-proc-mem-stack)                                 Monitor the data/stack size\n"
"                                                      of a process. The process\n"
"                                                      is identified by its pid \n"
"                                                      (see -pid).\n"
"  (-proc-mem-lib)                                   Monitor the library size of\n"
"                                                      a process. The process is \n"
"                                                      identified by its pid \n"
"                                                      (see -pid).\n"
"  (-proc-mem-dirty)                                 Monitor the number of dirty \n"
"                                                      pages of a process. The \n"
"                                                      process is identified by\n"
"                                                      its pid (see -pid).\n"
"  (-mem-total)                                      Monitor the total amount of\n"
"                                                      memory in the system.\n"
"  (-mem-free)                                       Monitor the amount of free \n"
"                                                      memory in the system.\n"
"  (-mem-used)                                       Monitor the amount of used \n"
"                                                      memory in the system.\n"
"  (-mem-shared)                                     Monitor the amount of shared\n"
"                                                      memory in the system.\n"
"  (-mem-buffers)                                    Monitor the amount of buffer\n"
"                                                      memory in the system.\n"
"  (-mem-cached)                                     Monitor the amount of cached\n"
"                                                      data in the system.\n"
"  (-mem-swap-total)                                 Monitor the amount of swap\n"
"                                                      memory in the system.\n"
"  (-mem-swap-free)                                  Monitor the amount of swap\n"
"                                                      memory not used by the \n"
"                                                      system.\n"
"  (-mem-swap-used)                                  Monitor the amount of swap\n"
"                                                      memory used by the system.\n"
     */

    i = 1;
    while ( (i < argc) && !errorStr )
	{
// 	if ( !strcmp( argv[i], "-command" ) )
// 	    {
// 	    if ( argc <= i + 1 )
// 		{
// 		errorStr = "Missing command specification.";
// 		break;
// 		}
// 	    command = argv[i+1];
// 	    i += 2;
// 	    }
// 	else 
	if ( cmdStarted )
	    {
	    command += " ";
	    command += argv[i];
	    i += 1;
	    continue;
	    }
	else if ( !strcmp( argv[i], "--" ) )
	    {
	    cmdStarted = true;
	    i += 1;
	    continue;
	    }
	else if ( !strcmp( argv[i], "-cpu-usr" ) )
	    {
	    monitorCPU_USR = true;
	    moniCount++;
	    i += 1;
	    continue;
	    }
	else if ( !strcmp( argv[i], "-cpu-nice" ) )
	    {
	    monitorCPU_NICE = true;
	    moniCount++;
	    i += 1;
	    continue;
	    }
	else if ( !strcmp( argv[i], "-cpu-sys" ) )
	    {
	    monitorCPU_SYS = true;
	    moniCount++;
	    i += 1;
	    continue;
	    }
	else if ( !strcmp( argv[i], "-cpu-idle" ) )
	    {
	    monitorCPU_IDLE = true;
	    moniCount++;
	    i += 1;
	    continue;
	    }
	else if ( !strcmp( argv[i], "-cpu-used" ) )
	    {
	    monitorCPU_USED = true;
	    moniCount++;
	    i += 1;
	    continue;
	    }
	else if ( !strcmp( argv[i], "-cpu-id" ) )
	    {
	    long cpuID;
	    if ( argc <= i + 1 )
		{
		errorStr = "Missing cpu-id specification.";
		break;
		}
	    if ( !strcmp( argv[i+1], "all" ) )
		allCPUs = true;
	    else
		{
		cpuID = strtol( argv[i+1], &cperr, 0 );
		if ( *cperr )
		    {
		    errorStr = "Error converting numeric cpu-id specifier.";
		    break;
		    }
		cpuIDs.insert( cpuIDs.end(), cpuID );
		}
	    i += 2;
	    continue;
	    }
	else if ( !strcmp( argv[i], "-net" ) )
	    {
	    monitorNet = true;
	    moniCount++;
	    i += 1;
	    continue;
	    }
	else if ( !strcmp( argv[i], "-net-bytes-in" ) )
	    {
	    monitorNet_Bytes_In = true;
	    moniCount++;
	    i += 1;
	    continue;
	    }
	else if ( !strcmp( argv[i], "-net-bytes-out" ) )
	    {
	    monitorNet_Bytes_Out = true;
	    moniCount++;
	    i += 1;
	    continue;
	    }
	else if ( !strcmp( argv[i], "-net-packets-in" ) )
	    {
	    monitorNet_Packets_In = true;
	    moniCount++;
	    i += 1;
	    continue;
	    }
	else if ( !strcmp( argv[i], "-net-packets-out" ) )
	    {
	    monitorNet_Packets_Out = true;
	    moniCount++;
	    i += 1;
	    continue;
	    }
	else if ( !strcmp( argv[i], "-net-errors-in" ) )
	    {
	    monitorNet_Errors_In = true;
	    moniCount++;
	    i += 1;
	    continue;
	    }
	else if ( !strcmp( argv[i], "-net-errors-out" ) )
	    {
	    monitorNet_Errors_Out = true;
	    moniCount++;
	    i += 1;
	    continue;
	    }
	else if ( !strcmp( argv[i], "-net-collisions-out" ) )
	    {
	    monitorNet_Collisions_Out = true;
	    moniCount++;
	    i += 1;
	    continue;
	    }
	else if ( !strcmp( argv[i], "-net-total-errors" ) )
	    {
	    monitorNet_Total_Errors = true;
	    moniCount++;
	    i += 1;
	    continue;
	    }
	else if ( !strcmp( argv[i], "-net-id" ) )
	    {
	    long netID;
	    if ( argc <= i + 1 )
		{
		errorStr = "Missing net-id specification.";
		break;
		}
	    if ( !strcmp( argv[i+1], "all" ) )
		allNets = true;
	    else
		{
		netID = strtol( argv[i+1], &cperr, 0 );
		if ( *cperr )
		    {
		    errorStr = "Error converting numeric net-id specifier.";
		    break;
		    }
		netIDs.insert( netIDs.end(), netID );
		}
	    i += 2;
	    continue;
	    }
	else if ( !strcmp( argv[i], "-pipe-in" ) )
	    {
	    monitorPipe_In = true;
	    moniCount++;
	    i += 1;
	    continue;
	    }
	else if ( !strcmp( argv[i], "-pipe-out" ) )
	    {
	    monitorPipe_Out = true;
	    moniCount++;
	    i += 1;
	    continue;
	    }
	else if ( !strcmp( argv[i], "-pipe-total" ) )
	    {
	    monitorPipe_Total = true;
	    moniCount++;
	    i += 1;
	    continue;
	    }
	else if ( !strcmp( argv[i], "-pipe-diff" ) )
	    {
	    monitorPipe_Diff = true;
	    moniCount++;
	    i += 1;
	    continue;
	    }
	else if ( !strcmp( argv[i], "-pipe-avg" ) )
	    {
	    monitorPipe_Avg = true;
	    moniCount++;
	    i += 1;
	    continue;
	    }
	else if ( !strcmp( argv[i], "-nbd" ) )
	    {
	    monitorNBD = true;
	    moniCount++;
	    i += 1;
	    continue;
	    }
		else if ( !strcmp( argv[i], "-disk-reqs-read" ) )
	    {
	    monitorDisk_Reqs_Read = true;
	    moniCount++;
	    i += 1;
	    continue;
	    }
	else if ( !strcmp( argv[i], "-disk-reqs-written" ) )
	    {
	    monitorDisk_Reqs_Written = true;
	    moniCount++;
	    i += 1;
	    continue;
	    }
	else if ( !strcmp( argv[i], "-disk-blocks-read" ) )
	    {
	    monitorDisk_Blocks_Read = true;
	    moniCount++;
	    i += 1;
	    continue;
	    }
	else if ( !strcmp( argv[i], "-disk-blocks-written" ) )
	    {
	    monitorDisk_Blocks_Written = true;
	    moniCount++;
	    i += 1;
	    continue;
	    }
	else if ( !strcmp( argv[i], "-disk-bytes-read" ) )
	    {
	    monitorDisk_Bytes_Read = true;
	    moniCount++;
	    i += 1;
	    continue;
	    }
	else if ( !strcmp( argv[i], "-disk-bytes-written" ) )
	    {
	    monitorDisk_Bytes_Written = true;
	    moniCount++;
	    i += 1;
	    continue;
	    }
	else if ( !strcmp( argv[i], "-disk-id" ) )
	    {
	    long diskID;
	    if ( argc <= i + 1 )
		{
		errorStr = "Missing disk-id specification.";
		break;
		}
	    diskID = strtol( argv[i+1], &cperr, 0 );
	    if ( *cperr )
	      {
		errorStr = "Error converting numeric disk-id specifier.";
		break;
	      }
	    diskIDs.insert( diskIDs.end(), diskID );
	    i += 2;
	    continue;
	    }
	else if (  !strcmp( argv[i], "-intr" ) ) {
	  monitorInterrupt = true;
	  if ( argc <= i + 1 ) {
	    // if argument is last, set default value
	  //if(find(IntrIds.begin(), IntrIds.end(), "all") == IntrIds.end()) {
	      IntrIds.push_back("all");
	      //}
	    moniCount++;
	    i += 1;
	    continue;
	  }
	  if(*argv[i+1] == '-') { 
	    // if no argument is specified, set default value
	  //if(find(IntrIds.begin(), IntrIds.end(), "all") == IntrIds.end()) {
	      IntrIds.push_back("all");
	      //}
	    moniCount++;
	    i += 1;
	    continue;
	  }
	  //if(find(IntrIds.begin(), IntrIds.end(), argv[i+1]) == IntrIds.end()) {
	    IntrIds.push_back(argv[i+1]);
	    moniCount++;
	    //}
	  i += 2;
	  continue;
	}
	else if (  !strcmp( argv[i], "-intr-cpu-id" ) ) {
	  if ( argc <= i + 1 ) {
	    errorStr = "Missing cpu id.";
	    break;
	  }
	  if (!strcmp(argv[i+1], "all")) {
	    IntrCpuIds.push_back(INTR_ALLCPUS); 
	  } else {
	    int IntrCpuId = strtol( argv[i+1], &cperr, 0 );
	    if ( *cperr ) {
	      errorStr = "Error converting numeric cpu id.";
	      break;
	    }
	    IntrCpuIds.push_back(IntrCpuId);
	  }
	  moniCount++;
	  i += 2;
	  continue;
	}
	else if (  !strcmp( argv[i], "-intr-cycles" ) ) {
	  monitorInterruptCycles = true;
	  if ( argc <= i + 1 ) {
	    // if argument is last, set default value
	  //if(find(IntrIds.begin(), IntrIds.end(), "all") == IntrIds.end()) {
	      IntrCyclesIds.push_back("all");
	      //}
	    moniCount++;
	    i += 1;
	    continue;
	  }
	  if(*argv[i+1] == '-') { 
	    // if no argument is specified, set default value
	  //if(find(IntrIds.begin(), IntrIds.end(), "all") == IntrIds.end()) {
	      IntrCyclesIds.push_back("all");
	      //}
	    moniCount++;
	    i += 1;
	    continue;
	  }
	  //if(find(IntrIds.begin(), IntrIds.end(), argv[i+1]) == IntrIds.end()) {
	    IntrCyclesIds.push_back(argv[i+1]);
	    moniCount++;
	    //}
	  i += 2;
	  continue;
	}
	else if (  !strcmp( argv[i], "-intr-cycles-cpu-id" ) ) {
	  if ( argc <= i + 1 ) {
	    errorStr = "Missing cpu id.";
	    break;
	  }
	  if (!strcmp(argv[i+1], "all")) {
	    IntrCyclesCpuIds.push_back(INTR_ALLCPUS); 
	  } else {
	    int IntrCpuId = strtol( argv[i+1], &cperr, 0 );
	    if ( *cperr ) {
	      errorStr = "Error converting numeric cpu id.";
	      break;
	    }
	    IntrCyclesCpuIds.push_back(IntrCpuId);
	  }
	  moniCount++;
	  i += 2;
	  continue;
	}
	else if (  !strcmp( argv[i], "-context" ) ) {
	  monitorContextSwitches = true;
	  moniCount++;
	  i += 1;
	  continue;
	}
	else if ( !strcmp( argv[i], "-cycles-user" ) )
	    {
	    monitorCyclesUser = true;
	    moniCount++;
	    i += 1;
	    continue;
	    }
	else if ( !strcmp( argv[i], "-cycles-intr" ) )
	    {
	    monitorCyclesIntr = true;
	    moniCount++;
	    i += 1;
	    continue;
	    }
	else if ( !strcmp( argv[i], "-cycles-softirq" ) )
	    {
	    monitorCyclesSoftIRQ = true;
	    moniCount++;
	    i += 1;
	    continue;
	    }
	else if ( !strcmp( argv[i], "-cycles-idle" ) )
	    {
	    monitorCyclesIdle = true;
	    moniCount++;
	    i += 1;
	    continue;
	    }
	else if ( !strcmp( argv[i], "-cycles-used" ) )
	    {
	    monitorCyclesUsed = true;
	    moniCount++;
	    i += 1;
	    continue;
	    }
	else 
	    {
            // print invalid argument 	    
	    MLUCString buf =  "invalid argument: ";
	    buf += argv[i];
	    errorStr = (char*) buf.c_str();

	    LOG( MLUCLog::kError, "Monitor", "Usage" )
		<< "Usage: " << argv[0] << usage << ENDLOG;
	    LOG( MLUCLog::kError, "Monitor", "Usage" )
		<< "       " << errorStr << ENDLOG;
	    return -1;
	    }
	}
    if ( !errorStr && !moniCount )
	errorStr = "Must specify at least one monitoring object.";

    // if process monitoring is enabled but pid is missing then throw error

    if ( errorStr )
	{
	LOG( MLUCLog::kError, "Monitor", "Usage" )
	    << "Usage: " << argv[0] << usage << ENDLOG;
	LOG( MLUCLog::kError, "Monitor", "Usage" )
	    << "       " << errorStr << ENDLOG;
	return -1;
	}

    if ( gethostname( hostName, SYS_NMLN )==-1 )
	{
	if ( testID )
	    sprintf( filePrefix, "Monitor-%08u.%06u.%06u-%s-", date, time_s, time_us, testID );
	else
	    sprintf( filePrefix, "Monitor-%08u.%06u.%06u-", date, time_s, time_us );
	}
    else
	{
	if ( testID )
	    sprintf( filePrefix, "Monitor-%s-%08u.%06u.%06u-%s-", hostName, date, time_s, time_us, testID );
	else
	    sprintf( filePrefix, "Monitor-%s-%08u.%06u.%06u-", hostName, date, time_s, time_us );
	}

    printf( "Running command: '%s'\n", command.c_str() );



    MLUCString output;
    MLUCString output_ave;
    char IDtmp[32];
    MLUCValueMonitor* valMon;
    if ( cpuIDs.size()<=0 || allCPUs )
	cpuIDs.insert( cpuIDs.begin(), -1 );
    vector<long>::iterator cpuIter, cpuEnd;
    cpuIter = cpuIDs.begin();
    cpuEnd = cpuIDs.end();
    while ( cpuIter != cpuEnd )
	{
	sprintf( IDtmp, "-%03ld", *cpuIter );
	if ( monitorCPU_USR )
	    {
	    if ( *cpuIter ==-1 )
		{
		output =               "CPU Usr       :            ";
		output_ave =           "CPU Usr (avg) :            ";
		}
	    else
		{
		output =               "CPU";
		output += IDtmp;
		output +=                     " Usr       :        ";
		output_ave =           "CPU";
		output_ave += IDtmp;
		output_ave +=                 " Usr (avg) :        ";
		}
	    valMon = new MLUCValueMonitorCPU_User( output.c_str(), " %",
						   output_ave.c_str(), " %",
						   1, *cpuIter);
	    if ( valMon )
		monitors.insert( monitors.end(), valMon );
	    }
	if ( monitorCPU_NICE )
	    {
	    if ( *cpuIter ==-1 )
		{
		output =               "CPU Nice       :           ";
		output_ave =           "CPU Nice (avg) :           ";
		}
	    else
		{
		output =               "CPU";
		output += IDtmp;
		output +=                     " Nice       :       ";
		output_ave =           "CPU";
		output_ave += IDtmp;
		output_ave +=                 " Nice (avg) :       ";
		}
	    valMon = new MLUCValueMonitorCPU_Nice( output.c_str(), " %",
						   output_ave.c_str(), " %",
						   1, *cpuIter);
	    if ( valMon )
		monitors.insert( monitors.end(), valMon );
	    }
	if ( monitorCPU_SYS )
	    {
	    if ( *cpuIter ==-1 )
		{
		output =               "CPU Sys       :            ";
		output_ave =           "CPU Sys (avg) :            ";
		}
	    else
		{
		output =               "CPU";
		output += IDtmp;
		output +=                     " Sys       :        ";
		output_ave =           "CPU";
		output_ave += IDtmp;
		output_ave +=                 " Sys (avg) :        ";
		}
	    valMon = new MLUCValueMonitorCPU_Sys( output.c_str(), " %",
						  output_ave.c_str(), " %",
						  1, *cpuIter);
	    if ( valMon )
		monitors.insert( monitors.end(), valMon );
	    }
	if ( monitorCPU_IDLE )
	    {
	    if ( *cpuIter ==-1 )
		{
		output =               "CPU Idle       :           ";
		output_ave =           "CPU Idle (avg) :           ";
		}
	    else
		{
		output =               "CPU";
		output += IDtmp;
		output +=                     " Idle       :       ";
		output_ave =           "CPU";
		output_ave += IDtmp;
		output_ave +=                 " Idle (avg) :       ";
		}
	    valMon = new MLUCValueMonitorCPU_Idle( output.c_str(), " %",
						   output_ave.c_str(), " %",
						   1, *cpuIter);
	    if ( valMon )
		monitors.insert( monitors.end(), valMon );
	    }
	if ( monitorCPU_USED )
	    {
	    if ( *cpuIter ==-1 )
		{
		output =               "CPU Used       :           ";
		output_ave =           "CPU Used (avg) :           ";
		}
	    else
		{
		output =               "CPU";
		output += IDtmp;
		output +=                     " Used       :       ";
		output_ave =           "CPU";
		output_ave += IDtmp;
		output_ave +=                 " Used (avg) :       ";
		}
	    valMon = new MLUCValueMonitorCPU_Used( output.c_str(), " %",
						   output_ave.c_str(), " %",
						   1, *cpuIter);
	    if ( valMon )
		monitors.insert( monitors.end(), valMon );
	    }
	cpuIter++;
	}

    if ( netIDs.size()<=0 || allNets )
	netIDs.insert( netIDs.begin(), -1 );
    vector<long>::iterator netIter, netEnd;
    netIter = netIDs.begin();
    netEnd = netIDs.end();
    while ( netIter != netEnd )
	{
	sprintf( IDtmp, "-%03ld", *netIter );
	if ( monitorNet )
	    {
	    if ( *netIter == -1 )
		{
		output =               "Net       :                ";
		output_ave =           "Net (avg) :                ";
		}
	    else
		{
		output =               "Net";
		output += IDtmp;
		output +=                     "       :            ";
		output_ave =           "Net";
		output_ave += IDtmp;
		output_ave +=                 " (avg) :            ";
		}
	    valMon = new MLUCValueMonitorNet( output.c_str(), " Bytes",
					      output_ave.c_str(), " Bytes",
					      1,
					      -1,
					      *netIter);
	    if ( valMon )
		monitors.insert( monitors.end(), valMon );
	    }
	if ( monitorNet_Bytes_In )
	    {
	    if ( *netIter==-1 )
		{
		output =               "Net Bytes In       :       ";
		output_ave =           "Net Bytes In (avg) :       ";
		}
	    else
		{
		output =               "Net";
		output += IDtmp;
		output +=                     " Bytes In       :   ";
		output_ave =           "Net";
		output_ave += IDtmp;
		output_ave +=                 " Bytes In (avg) :   ";
		}

	    valMon = new MLUCValueMonitorNet( output.c_str(), " Bytes",
					      output_ave.c_str(), " Bytes",
					      1,
					      0,
					      *netIter);
	    if ( valMon )
		monitors.insert( monitors.end(), valMon );
	    }

	if ( monitorNet_Bytes_Out )
	    {
	    if ( *netIter==-1 )
		{
		output =               "Net Bytes Out       :      ";
		output_ave =           "Net Bytes Out (avg) :      ";
		}
	    else
		{
		output =               "Net";
		output += IDtmp;
		output +=                     " Bytes Out     :    ";
		output_ave =           "Net";
		output_ave += IDtmp;
		output_ave +=                 " Bytes Out (avg):   ";
		}

	    valMon = new MLUCValueMonitorNet( output.c_str(), " Bytes",
					      output_ave.c_str(), " Bytes",
					      1,
					      1,
					      *netIter);
	    if ( valMon )
		monitors.insert( monitors.end(), valMon );
	    }

	if ( monitorNet_Packets_In )
	    {
	    if ( *netIter==-1 )
		{
		output =               "Net Packets In     :       ";
		output_ave =           "Net Packets In (avg):      ";
		}
	    else
		{
		output =               "Net";
		output += IDtmp;
		output +=                     " Packets In      :  ";
		output_ave =           "Net";
		output_ave += IDtmp;
		output_ave +=                 " Packets In (avg):  ";
		}

	    valMon = new MLUCValueMonitorNet( output.c_str(), " Packets",
					      output_ave.c_str(), " Packets",
					      1,
					      2,
					      *netIter);
	    if ( valMon )
		monitors.insert( monitors.end(), valMon );
	    }
	if ( monitorNet_Packets_Out )
	    {
	    if ( *netIter==-1 )
		{
		output =               "Net Packets Out       :    ";
		output_ave =           "Net Packets Out (avg) :    ";
		}
	    else
		{
		output =               "Net";
		output += IDtmp;
		output +=                     " Packets Out      : ";
		output_ave =           "Net";
		output_ave += IDtmp;
		output_ave +=                 " Packets Out (avg): ";
		}

	    valMon = new MLUCValueMonitorNet( output.c_str(), " Packets",
					      output_ave.c_str(), " Packets",
					      1,
					      3,
					      *netIter);
	    if ( valMon )
		monitors.insert( monitors.end(), valMon );
	    }

	if ( monitorNet_Errors_In )
	    {
	    if ( *netIter==-1 )
		{
		output =               "Net Errors In     :       ";
		output_ave =           "Net Errors In (avg):      ";
		}
	    else
		{
		output =               "Net";
		output += IDtmp;
		output +=                     " Errors In      :  ";
		output_ave =           "Net";
		output_ave += IDtmp;
		output_ave +=                 " Errors In (avg):  ";
		}

	    valMon = new MLUCValueMonitorNet( output.c_str(), " Errors",
					      output_ave.c_str(), " Errors",
					      1,
					      4,
					      *netIter);
	    if ( valMon )
		monitors.insert( monitors.end(), valMon );
	    }
	if ( monitorNet_Errors_Out )
	    {
	    if ( *netIter==-1 )
		{
		output =               "Net Errors Out       :    ";
		output_ave =           "Net Errors Out (avg) :    ";
		}
	    else
		{
		output =               "Net";
		output += IDtmp;
		output +=                     " Errors Out      : ";
		output_ave =           "Net";
		output_ave += IDtmp;
		output_ave +=                 " Errors Out (avg): ";
		}

	    valMon = new MLUCValueMonitorNet( output.c_str(), " Errors",
					      output_ave.c_str(), " Errors",
					      1,
					      5,
					      *netIter);
	    if ( valMon )
		monitors.insert( monitors.end(), valMon );
	    }
	if ( monitorNet_Collisions_Out )
	    {
	    if ( *netIter==-1 )
		{
		output =               "Net Collisions Out     :       ";
		output_ave =           "Net Collisions Out (avg):      ";
		}
	    else
		{
		output =               "Net";
		output += IDtmp;
		output +=                     " Collisions Out      :  ";
		output_ave =           "Net";
		output_ave += IDtmp;
		output_ave +=                 " Collisions Out (avg):  ";
		}

	    valMon = new MLUCValueMonitorNet( output.c_str(), " Collisions",
					      output_ave.c_str(), " Collisions",
					      1,
					      6,
					      *netIter);
	    if ( valMon )
		monitors.insert( monitors.end(), valMon );
	    }
	if ( monitorNet_Total_Errors )
	    {
	    if ( *netIter==-1 )
		{
		output =               "Net Total Errors       :    ";
		output_ave =           "Net Total Errors (avg) :    ";
		}
	    else
		{
		output =               "Net";
		output += IDtmp;
		output +=                     " Total Errors      : ";
		output_ave =           "Net";
		output_ave += IDtmp;
		output_ave +=                 " Total Errors (avg): ";
		}

	    valMon = new MLUCValueMonitorNet( output.c_str(), " Total Errors",
					      output_ave.c_str(), " Total Errors",
					      1,
					      7,
					      *netIter);
	    if ( valMon )
		monitors.insert( monitors.end(), valMon );
	    }

	netIter++;
	}



    if ( monitorPipe_Total )
	{
	output =                       "Pipe Total       :         ";
	output_ave =                   "Pipe Total (avg) :         ";
	valMon = new MLUCValueMonitorPipe( output.c_str(), " Bytes",
					   output_ave.c_str(), " Bytes",
					   1, 0 );
	if ( valMon )
	    monitors.insert( monitors.end(), valMon );
	}

    if ( monitorPipe_In )
	{
	output =                       "Pipe In       :            ";
	output_ave =                   "Pipe In (avg) :            ";
	valMon = new MLUCValueMonitorPipe( output.c_str(), " Bytes",
					   output_ave.c_str(), " Bytes",
					   1, 1 );
	if ( valMon )
	    monitors.insert( monitors.end(), valMon );
	}

    if ( monitorPipe_Out )
	{
	output =                       "Pipe Out       :           ";
	output_ave =                   "Pipe Out (avg) :           ";
	valMon = new MLUCValueMonitorPipe( output.c_str(), " Bytes",
					   output_ave.c_str(), " Bytes",
					   1, 2 );
	if ( valMon )
	    monitors.insert( monitors.end(), valMon );
	}

    if ( monitorPipe_Diff )
	{
	output =                       "Pipe Diff       :          ";
	output_ave =                   "Pipe Diff (avg) :          ";
	valMon = new MLUCValueMonitorPipe( output.c_str(), " Bytes",
					   output_ave.c_str(), " Bytes",
					   1, 3 );
	if ( valMon )
	    monitors.insert( monitors.end(), valMon );
	}

    if ( monitorPipe_Avg )
	{
	output =                       "Pipe Avg       :           ";
	output_ave =                   "Pipe Avg (avg) :           ";
	valMon = new MLUCValueMonitorPipe( output.c_str(), " Bytes",
					   output_ave.c_str(), " Bytes",
					   1, 4 );
	if ( valMon )
	    monitors.insert( monitors.end(), valMon );
	}


    if ( monitorNBD )
	{
	output =                       "NBD            :           ";
	output_ave =                   "NBD (avg)      :           ";
	valMon = new MLUCValueMonitorNBD( output.c_str(), " Bytes",
					  output_ave.c_str(), " Bytes",
					  1, 4 );
	if ( valMon )
	    monitors.insert( monitors.end(), valMon );
	}

    // disk stuff
    if ( diskIDs.size()<=0 )
      diskIDs.insert( diskIDs.begin(), 0 );
    vector<long>::iterator diskIter, diskEnd;
    diskIter = diskIDs.begin();
    diskEnd = diskIDs.end();
    while ( diskIter != diskEnd )
	{
	sprintf( IDtmp, "-%03ld", *diskIter );
	if ( monitorDisk_Reqs_Read )
	    {
	    if ( *diskIter==0 )
		{
		output =               "Disk Reqs Read       :        ";
		output_ave =           "Disk Reqs Read (avg) :        ";
		}
	    else
		{
		output =               "Disk";
		output += IDtmp;
		output +=                     " Reqs Read         :   ";
		output_ave =           "Disk";
		output_ave += IDtmp;
		output_ave +=                 " Reqs Read (avg)   :   ";
		}

	    valMon = new MLUCValueMonitorDisk( output.c_str(), " Reqs",
					       output_ave.c_str(), " Reqs",
					       1,
					       0,
					       *diskIter);
	    if ( valMon )
		monitors.insert( monitors.end(), valMon );
	    }

	if ( monitorDisk_Reqs_Written )
	    {
	    if ( *diskIter==0 )
		{
		output =               "Disk Reqs Written       :     ";
		output_ave =           "Disk Reqs Written (avg) :     ";
		}
	    else
		{
		output =               "Disk";
		output += IDtmp;
		output +=                     " Reqs Written      :   ";
		output_ave =           "Disk";
		output_ave += IDtmp;
		output_ave +=                 " Reqs Written (avg):   ";
		}

	    valMon = new MLUCValueMonitorDisk( output.c_str(), " Reqs",
					       output_ave.c_str(), " Reqs",
					       1,
					       1,
					       *diskIter);
	    if ( valMon )
		monitors.insert( monitors.end(), valMon );
	    }

	if ( monitorDisk_Blocks_Read )
	    {
	    if ( *diskIter==0 )
		{
		output =               "Disk Blocks Read       :      ";
		output_ave =           "Disk Blocks Read (avg) :      ";
		}
	    else
		{
		output =               "Disk";
		output += IDtmp;
		output +=                     " Blocks Read       :   ";
		output_ave =           "Disk";
		output_ave += IDtmp;
		output_ave +=                 " Blocks Read (avg) :   ";
		}

	    valMon = new MLUCValueMonitorDisk( output.c_str(), " Blks",
					       output_ave.c_str(), " Blks",
					       1,
					       2,
					       *diskIter);
	    if ( valMon )
		monitors.insert( monitors.end(), valMon );
	    }
	if ( monitorDisk_Blocks_Written )
	    {
	    if ( *diskIter==0 )
		{
		output =               "Disk Blocks Written       :   ";
		output_ave =           "Disk Blocks Written (avg) :   ";
		}
	    else
		{
		output =               "Disk";
		output += IDtmp;
		output +=                     " Blocks Written      : ";
		output_ave =           "Disk";
		output_ave += IDtmp;
		output_ave +=                 " Blocks Written (avg): ";
		}

	    valMon = new MLUCValueMonitorDisk( output.c_str(), " Blks",
					       output_ave.c_str(), " Blks",
					       1,
					       3,
					       *diskIter);
	    if ( valMon )
		monitors.insert( monitors.end(), valMon );
	    }
	if ( monitorDisk_Bytes_Read )
	    {
	    if ( *diskIter==0 )
		{
		output =               "Disk Bytes Read       :      ";
		output_ave =           "Disk Bytes Read (avg) :      ";
		}
	    else
		{
		output =               "Disk";
		output += IDtmp;
		output +=                     " Bytes Read      :    ";
		output_ave =           "Disk";
		output_ave += IDtmp;
		output_ave +=                 " Bytes Read (avg):    ";
		}

	    valMon = new MLUCValueMonitorDisk( output.c_str(), " Bytes",
					       output_ave.c_str(), " Bytes",
					       1,
					       4,
					       *diskIter);
	    if ( valMon )
		monitors.insert( monitors.end(), valMon );
	    }


	if ( monitorDisk_Bytes_Written )
	    {
	    if ( *diskIter==0 )
		{
		output =               "Disk Bytes Written        :  ";
		output_ave =           "Disk Bytes Written (avg)  :  ";
		}
	    else
		{
		output =               "Disk";
		output += IDtmp;
		output +=                     " Bytes Written       :";
		output_ave =           "Disk";
		output_ave += IDtmp;
		output_ave +=                 " Bytes Written (avg) :";
		}

	    valMon = new MLUCValueMonitorDisk( output.c_str(), " Bytes",
					       output_ave.c_str(), " Bytes",
					       1,
					       5,
					       *diskIter);
	    if ( valMon )
		monitors.insert( monitors.end(), valMon );
	    }
	diskIter++;
	}


    // -------------------------------------------------------------------------
    // interrupt monitoring
    // -------------------------------------------------------------------------
    if(monitorInterrupt) {
      // if container is empty set to default value
      if(IntrCpuIds.size() == 0) { 
      IntrCpuIds.push_back(INTR_ALLCPUS);
      }

      vector<string>::iterator IterIntrId = IntrIds.begin();
      vector<int>::iterator IterIntrCpuId = IntrCpuIds.begin();
      for(;IterIntrId != IntrIds.end(); IterIntrId++ ) {
      for ( IterIntrCpuId = IntrCpuIds.begin(); IterIntrCpuId != IntrCpuIds.end(); IterIntrCpuId++ ) {
	
	output     = "Interrupt ";
	if(*IterIntrId == "all") {
	  output    += "all";
	  if(*IterIntrCpuId == INTR_ALLCPUS) {
	  } else {
	    sprintf(IDtmp, "%d", *IterIntrCpuId);
	    output    += " (cpu";
	    output    += IDtmp;
	    output    += ")";
	  }
	} else {
	  output += IterIntrId->c_str();
	  if(*IterIntrCpuId == INTR_ALLCPUS) {
	  } else {
	    sprintf(IDtmp, "%d", *IterIntrCpuId);
	    output    += " (cpu";
	    output    += IDtmp;
	    output    += ")";
	  }
	}

	output_ave = output.c_str();
	output_ave +=" (avg) :\t";

	output    += "       :\t";
	valMon = new MLUCValueMonitorInterrupt(output.c_str(), " Intr",
					       output_ave.c_str(), " Intr",
					       1,
					       IterIntrId->c_str(),
					       *IterIntrCpuId);
	if ( valMon )  
	  monitors.insert( monitors.end(), valMon );
      }
      }
    }

    
    // -------------------------------------------------------------------------
    // interrupt cycles  monitoring
    // -------------------------------------------------------------------------

    if(monitorInterruptCycles) {
      // if container is empty set to default value
      if(IntrCyclesCpuIds.size() == 0) { 
      IntrCyclesCpuIds.push_back(INTR_ALLCPUS);
      }

      vector<string>::iterator IterIntrId = IntrCyclesIds.begin();
      vector<int>::iterator IterIntrCpuId = IntrCyclesCpuIds.begin();
      for(;IterIntrId != IntrCyclesIds.end(); IterIntrId++ ) {
      for ( IterIntrCpuId = IntrCyclesCpuIds.begin(); IterIntrCpuId != IntrCyclesCpuIds.end(); IterIntrCpuId++ ) {
	
	output     = "Intr. Cycles ";
	if(*IterIntrId == "all") {
	  output    += "all";
	  if(*IterIntrCpuId == INTR_ALLCPUS) {
	  } else {
	    sprintf(IDtmp, "%d", *IterIntrCpuId);
	    output    += " (cpu";
	    output    += IDtmp;
	    output    += ")";
	  }
	} else {
	  output += IterIntrId->c_str();
	  if(*IterIntrCpuId == INTR_ALLCPUS) {
	  } else {
	    sprintf(IDtmp, "%d", *IterIntrCpuId);
	    output    += " (cpu";
	    output    += IDtmp;
	    output    += ")";
	  }
	}

	output_ave = output.c_str();
	output_ave +=" (avg) :\t";

	output    += "       :\t";
	valMon = new MLUCValueMonitorInterruptCycles( output.c_str(), " %",
						      output_ave.c_str(), " %",
						      1,
						      IterIntrId->c_str(),
						      *IterIntrCpuId);
	if ( valMon )  
	  monitors.insert( monitors.end(), valMon );
      }
      }
    }
    
    // -------------------------------------------------------------------------
    // context switch  monitoring
    // -------------------------------------------------------------------------
    if(monitorContextSwitches) {
      output     = "Context Switches        :    ";
      output_ave = "Context Switchesd (avg) :    ";
      valMon = new MLUCValueMonitorContextSwitches( output.c_str(), "",
						    output_ave.c_str(), "",
						    1);
      if (valMon)
	monitors.insert( monitors.end(), valMon );
    }
    
    /*
    if ( cpuIDs.size()<=0 || allCPUs )
	cpuIDs.insert( cpuIDs.begin(), -1 );
    */
    cpuIter = cpuIDs.begin();
    cpuEnd = cpuIDs.end();
    while ( cpuIter != cpuEnd )
	{
	sprintf( IDtmp, "-%03ld", *cpuIter );
	if ( monitorCyclesUser )
	    {
	    if ( *cpuIter ==-1 )
		{
		output =               "Cycles User       :        ";
		output_ave =           "Cycles User (avg) :        ";
		}
	    else
		{
		output =               "Cycles";
		output += IDtmp;
		output +=                     " User       :    ";
		output_ave =           "Cycles";
		output_ave += IDtmp;
		output_ave +=                 " User (avg) :    ";
		}
	    valMon = new MLUCValueMonitorCycles( output.c_str(), " %",
						 output_ave.c_str(), " %",
						 1, 
						 MLUCValueMonitorCycles::USER, *cpuIter);
	    if ( valMon )
		monitors.insert( monitors.end(), valMon );
	    }
	if ( monitorCyclesIntr )
	    {
	    if ( *cpuIter ==-1 )
		{
		output =               "Cycles Intr       :        ";
		output_ave =           "Cycles Intr (avg) :        ";
		}
	    else
		{
		output =               "Cycles";
		output += IDtmp;
		output +=                     " Intr       :    ";
		output_ave =           "Cycles";
		output_ave += IDtmp;
		output_ave +=                 " Intr (avg) :    ";
		}
	    valMon = new MLUCValueMonitorCycles( output.c_str(), " %",
						 output_ave.c_str(), " %",
						 1, 
						 MLUCValueMonitorCycles::INTR, *cpuIter);
	    if ( valMon )
		monitors.insert( monitors.end(), valMon );
	    }
	if ( monitorCyclesSoftIRQ )
	    {
	    if ( *cpuIter ==-1 )
		{
		output =               "Cycles SoftIRQ       :     ";
		output_ave =           "Cycles SoftIRQ (avg) :     ";
		}
	    else
		{
		output =               "Cycles";
		output += IDtmp;
		output +=                     " SoftIRQ       : ";
		output_ave =           "Cycles";
		output_ave += IDtmp;
		output_ave +=                 " SoftIRQ (avg) : ";
		}
	    valMon = new MLUCValueMonitorCycles( output.c_str(), " %",
						 output_ave.c_str(), " %",
						 1, 
						 MLUCValueMonitorCycles::SOFTIRQ, *cpuIter);
	    if ( valMon )
		monitors.insert( monitors.end(), valMon );
	    }
	if ( monitorCyclesIdle )
	    {
	    if ( *cpuIter ==-1 )
		{
		output =               "Cycles Idle       :        ";
		output_ave =           "Cycles Idle (avg) :        ";
		}
	    else
		{
		output =               "Cycles";
		output += IDtmp;
		output +=                     " Idle       :    ";
		output_ave =           "Cycles";
		output_ave += IDtmp;
		output_ave +=                 " Idle (avg) :    ";
		}
	    valMon = new MLUCValueMonitorCycles( output.c_str(), " %",
						 output_ave.c_str(), " %",
						 1, 
						 MLUCValueMonitorCycles::IDLE, *cpuIter);
	    if ( valMon )
		monitors.insert( monitors.end(), valMon );
	    }
	if ( monitorCyclesUsed )
	    {
	    if ( *cpuIter ==-1 )
		{
		output =               "Cycles Used       :        ";
		output_ave =           "Cycles Used (avg) :        ";
		}
	    else
		{
		output =               "Cycles";
		output += IDtmp;
		output +=                     " Used       :    ";
		output_ave =           "Cycles";
		output_ave += IDtmp;
		output_ave +=                 " Used (avg) :    ";
		}
	    valMon = new MLUCValueMonitorCycles( output.c_str(), " %",
						 output_ave.c_str(), " %",
						 1, 
						 MLUCValueMonitorCycles::USED, *cpuIter);
	    if ( valMon )
		monitors.insert( monitors.end(), valMon );
	    }
	cpuIter++;
	}




    vector<MLUCValueMonitor*>::iterator iter, end;

    iter = monitors.begin();
    end = monitors.end();
    while ( iter != end )
	{
	if ( (*iter) )
	    {
	    (*iter)->SetTimeStampOutput( false );
	    (*iter)->StartMonitoring();
	    }
	iter++;
	}


    struct timeval startt, endt;
    double dt;
    unsigned long long dtl, t0=0;
    int ret;

    for ( i = 0; i < 20 ; i++ )
	{
	gettimeofday( &startt, NULL );
	
	ret = system( "" );
	
	gettimeofday( &endt, NULL );
	if ( ret )
	    {
	    }
	dtl = endt.tv_sec-startt.tv_sec;
	dtl *= 1000000ULL;
	dtl += endt.tv_usec-startt.tv_usec;
	if ( !i || dtl < t0 )
	    t0 = dtl;
	}

    uint64 value;
    iter = monitors.begin();
    end = monitors.end();
    while ( iter != end )
	{
	if ( (*iter) )
	    {
	    ret = (*iter)->GetValue( value );
	    if ( ret )
		{
// 		LOG( MLUCLog::kError, "Monitor", "Error Getting Value" )
// 		    << "Error getting value from monitoring object: "
// 		    << strerror(ret) << " (" << MLUCLog::kDec << ret
// 		    << ")." << ENDLOG;
		iter++;
		continue;
		}
	    //ret = (*iter)->OutputValue( value );
	    }
	iter++;
	}
    printf( "-------------------------------------------------------------------------------\n\n" );
    gettimeofday( &startt, NULL );
    
    ret = system( command.c_str() );

    gettimeofday( &endt, NULL );
    printf( "-------------------------------------------------------------------------------\n\n" );
    if ( ret )
	{
	printf( "Error executing %s: %s (%d)\n", command.c_str(), strerror(errno), errno );
	}
    dtl = endt.tv_sec-startt.tv_sec;
    dtl *= 1000000ULL;
    dtl += endt.tv_usec-startt.tv_usec;
    dt = t0;
    printf( "Min. system (3) overhead :     %16f microsec. (%16.6fs)\n", dt, dt/1000000.0 );
    dt = dtl;
    printf( "Runtime (incl. overhead) :     %16f microsec. (%16.6f s)\n", dt, dt/1000000.0 );
    dtl -= t0;
    dt = dtl;
    printf( "Runtime (wo. overhead) :       %16f microsec. (%16.6f s)\n", dt, dt/1000000.0 );

    iter = monitors.begin();
    end = monitors.end();
    while ( iter != end )
	{
	if ( (*iter) )
	    {
	    ret = (*iter)->GetValue( value );
	    if ( ret )
		{
		LOG( MLUCLog::kError, "Monitor", "Error Getting Value" )
		    << "Error getting value from monitoring object: "
		    << strerror(ret) << " (" << MLUCLog::kDec << ret
		    << ")." << ENDLOG;
		iter++;
		continue;
		}
	    ret = (*iter)->OutputValue( value, false );
	    }
	iter++;
	}



    iter = monitors.begin();
    end = monitors.end();
    while ( iter != end )
	{
	if ( (*iter) )
	    (*iter)->StopMonitoring();
	iter++;
	}


    iter = monitors.begin();
    end = monitors.end();
    while ( iter != end )
	{
	if ( (*iter) )
	    delete (*iter);
	iter++;
	}
    return 0;
    }


bool GetFilenameFromPid(int pid, string& filename)
    {
  ostringstream statfile;

  statfile << "/proc/" << pid << "/status";
  ifstream fin(statfile.str().c_str());

  if(!fin) {
    return false;
  }

  string line;
  do{
    fin >> filename;
  } while(filename != "Name:");
  fin >> filename;

  return true;
}

/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: Monitor.cpp,v 1.27 2003/02/20 11:14:38 timm Exp $ 
**
***************************************************************************
*/
