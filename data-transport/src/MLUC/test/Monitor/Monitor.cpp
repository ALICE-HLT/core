/************************************************************************
 **
 **
 ** This file is property of and copyright by the Technical Computer
 ** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
 ** University, Heidelberg, Germany, 2001
 ** This file has been written by Timm Morten Steinbeck,
 ** timm@kip.uni-heidelberg.de
 **
 **
 ** See the file license.txt for details regarding usage, modification,
 ** distribution and warranty.
 ** Important: This file is provided without any warranty, including
 ** fitness for any particular purpose.
 **
 **
 ** Newer versions of this file's package will be made available from
 ** http://web.kip.uni-heidelberg.de/Hardwinf/L3/
 ** or the corresponding page of the Heidelberg Alice Level 3 group.
 **
 *************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCValueMonitor.hpp"
#include "MLUCValueMonitorCPU.hpp"
#include "MLUCValueMonitorNet.hpp"
#include "MLUCValueMonitorPipe.hpp"
#include "MLUCValueMonitorNBD.hpp"
#include "MLUCValueMonitorCR.hpp"
#include "MLUCValueMonitorDisk.hpp"
#include "MLUCValueMonitorProcess.hpp"
#include "MLUCValueMonitorMem.hpp"
#include "MLUCValueMonitorInterrupt.hpp"
#include "MLUCValueMonitorInterruptCycles.hpp"
#include "MLUCValueMonitorContextSwitches.hpp"
#include "MLUCValueMonitorCycles.hpp"
#include "MLUCValueMonitorMountPoint.hpp"
#include "MLUCValueMonitorRAID5.hpp"
#include "MLUCLog.hpp"
#include "MLUCLogServer.hpp"
#include <signal.h>
#include <sys/utsname.h>
#include <unistd.h>
#include <fstream>
#if __GNUC__ ==2
#include <strstream>
#endif
#include <sstream>
#include <string>
#include <set>
#include <algorithm>

/*
  A note to ease output formatting:
  The first character of the output string should be in column 40 and the last
  one in column 66, to ensure proper alignment in the output.
*/


#ifdef SYS_NMLN
#define MAX_HOSTNAME_LENGTH SYS_NMLN
#else
#ifdef HOST_NAME_MAX
#define MAX_HOSTNAME_LENGTH HOST_NAME_MAX
#else
#ifdef MAXHOSTNAMELEN
#define MAX_HOSTNAME_LENGTH MAXHOSTNAMELEN
#else
#error Do not know how to define maximum hostname length. Cannot find either SYS_NMLN (Linux) nor HOST_NAME_MAX or MAXHOSTNAMELEN (BSD/Darwin/Mac OS X)
#endif
#endif
#endif



bool gQuit = false;

void QuitSignalHandler( int sig );

bool GetFilenameFromPid(int pid, string& filename);

int main( int argc, char** argv )
{
    MLUCStdoutLogServer logOut;
    gLog.AddServer( logOut );

    vector<MLUCValueMonitor*> monitors;
    int i;
    const char* errorStr = NULL;
    char* cperr;
    struct timeval now;
    struct tm* t;
    unsigned int date, time_s, time_us;
    gettimeofday( &now, NULL );
    t = localtime( &(now.tv_sec) );
    t->tm_mon++;
    t->tm_year += 1900;
    date = t->tm_year*10000+t->tm_mon*100+t->tm_mday;
    time_s = t->tm_hour*10000+t->tm_min*100+t->tm_sec;
    time_us = now.tv_usec;
    char filePrefix[128];
    char charDate[32];
    char hostName[ MAX_HOSTNAME_LENGTH+1 ];

    unsigned int updateInterval = 1000;
    unsigned int averageInterval = 10000;
    unsigned int averageCount = 1;
    vector<long> cpuIDs;
    vector<long> netIDs;
    vector<long> diskIDs;
    bool monitorCPU_USR = false;
    bool monitorCPU_NICE = false;
    bool monitorCPU_SYS = false;
    bool monitorCPU_IDLE = false;
    bool monitorCPU_USED = false;
    bool monitorNet_Bytes_In = false;
    bool monitorNet_Bytes_Out = false;
    bool monitorNet_Packets_In = false;
    bool monitorNet_Packets_Out = false;
    bool monitorNet_Errors_In = false;
    bool monitorNet_Errors_Out = false;
    bool monitorNet_Collisions_Out = false;
    bool monitorNet_Total_Errors = false;
    bool monitorPipe_In = false;
    bool monitorPipe_Out = false;
    bool monitorPipe_Total = false;
    bool monitorPipe_Diff = false;
    bool monitorPipe_Avg = false;
    bool monitorNet = false;
    unsigned int moniCount = 0;
    bool allCPUs = false;
    bool allNets = false;
    char* testID = NULL;
    bool monitorNBD = false;
    bool monitorCRread = false;
    bool monitorCRwrite = false;
    bool monitorDisk_Reqs_Read      = false;
    bool monitorDisk_Reqs_Written   = false;
    bool monitorDisk_Blocks_Read    = false;
    bool monitorDisk_Blocks_Written = false;
    bool monitorDisk_Bytes_Read     = false;   // this is based on a assumed
    bool monitorDisk_Bytes_Written  = false;   // block size of 512 bytes
    bool monitorProcess_CpuUsage        = false;
    bool monitorProcess_CpuUsage_System = false;
    bool monitorProcess_CpuUsage_User   = false;
    bool monitorProcess_Size         = false;
    bool monitorProcess_Mem_Resident = false;
    bool monitorProcess_Mem_Shared   = false;
    bool monitorProcess_Mem_Stack    = false;
    bool monitorProcess_Mem_Library  = false;
    bool monitorProcess_Mem_Dirty    = false;
    int processMonitorCnt = 0;
    vector<int> pIDs;
    bool monitorMem_Total       = false;
    bool monitorMem_Free        = false;
    bool monitorMem_Used        = false;
    bool monitorMem_Shared      = false;
    bool monitorMem_Buffers     = false;
    bool monitorMem_Cached      = false;
    bool monitorMem_SwapTotal   = false;
    bool monitorMem_SwapFree    = false;
    bool monitorMem_SwapUsed    = false;

    bool monitorInterrupt       = false;
    vector<string> IntrIds;
    vector<int>    IntrCpuIds;

    bool monitorInterruptCycles  = false;
    vector<string> IntrCyclesIds;
    vector<int>    IntrCyclesCpuIds;

    bool monitorContextSwitches = false;

    bool monitorCyclesUser = false;
    bool monitorCyclesIntr = false;
    bool monitorCyclesSoftIRQ = false;
    bool monitorCyclesIdle = false;
    bool monitorCyclesUsed = false;

    bool monitorMountPoint = false;
    vector<char *> mountPoints;

    bool monitorRAID5 = false;
    vector<int> RAID5Devices;


    const char* usage = ""\
		""\
		"Usage: Monitor Options, where Options can be \n"\
		""\
		"  (-testid)                                         A descriptive text which\n"\
		"                                                    is incorporated into the\n"\
		"                                                    output filenames.\n"\
		"  (-update-interval <update-interval-millisec.>)    The interval between\n"\
		"                                                       updates. \n"\
		"                                                       (default 1000 ms)\n"\
		"  (-average-interval <average-interval-millisec.>)  The average interval. \n"\
		"                                                       (default 10 000 ms)\n"\
		"  (-cpu-usr)                                        Monitor CPU usage by user\n"\
		"                                                      processes (all CPUs if\n"\
		"                                                      cpu-id is not specified).\n"\
		"  (-cpu-nice)                                       Monitor CPU usage by nice\n"\
		"                                                      processes (all CPUs if\n"\
		"                                                      cpu-id is not specified).\n"\
		"  (-cpu-sys)                                        Monitor CPU usage by system\n"\
		"                                                      processes (all CPUs if\n"\
		"                                                      cpu-id is not specified).\n"\
		"  (-cpu-idle)                                       Monitor CPU usage by idle\n"\
		"                                                      processes (all CPUs if\n"\
		"                                                      cpu-id is not specified).\n"\
		"  (-cpu-used)                                       Monitor CPU usage (all CPUs\n"\
		"                                                       if cpu-id is not specified).\n"\
		"  (-cpu-id)                                         Specify the cpu. If not\n"\
		"                                                      specified overall usage\n"\
		"                                                      will be taken. Use \"all\"\n"\
		"                                                      to also see overall usage.\n"\
		"  (-net)                                            Monitor overall (all devs, \n"\
		"                                                      in and out net) traffic. \n"\
		"  (-net-bytes-in)                                   Monitor incoming bytes (all \n"\
		"                                                      devs if net-id is not \n"\
		"                                                      specified).\n"\
		"  (-net-bytes-out)                                  Monitor outgoing bytes (all \n"\
		"                                                      devs if net-id is \n"\
		"                                                      not specified). \n"\
		"  (-net-packets-in)                                 Monitor incoming packets \n"\
		"                                                      (all devs if net-id is\n"\
		"                                                      not specified). \n"\
		"  (-net-packets-out)                                Monitor outgoing packets \n"\
		"                                                      (all devs if net-id is\n"\
		"                                                      not specified). \n"\
		"  (-net-errors-in)                                  Monitor incoming errors \n"\
		"                                                      (all devs if net-id is\n"\
		"                                                      not specified). \n"\
		"  (-net-errors-out)                                 Monitor outgoing errors \n"\
		"                                                      (all devs if net-id is\n"\
		"                                                      not specified). \n"\
		"  (-net-collisions-out)                             Monitor outgoing collisions \n"\
		"                                                      (all devs if net-id is\n"\
		"                                                      not specified). \n"\
		"  (-net-total-errors)                               Monitor overall erros and collisions  \n"\
		"                                                      (all devs if net-id is\n"\
		"                                                      not specified). \n"\
		"  (-net-id <network interface number>)              Specify the network \n"\
		"                                                      interface. If not \n"\
		"                                                      specified overall \n"\
		"                                                      traffic will be taken.\n"\
		"                                                      Use \"all\" to also see \n"\
		"                                                      overall usage.\n"\
		"  (-pipe-in)                                        Monitor the amount of data \n"\
		"                                                      written into pipes.\n"\
		"  (-pipe-out)                                       Monitor the amount of data \n"\
		"                                                      read from pipes.\n"\
		"  (-pipe-total)                                     Monitor the amount of data \n"\
		"                                                      read from and written to \n"\
		"                                                      pipes.\n"\
		"  (-pipe-diff)                                      Monitor the difference in the\n"\
		"                                                      amounts of data written to \n"\
		"                                                      and read from pipes.\n"\
		"  (-pipe-avg)                                       Monitor the average between \n"\
		"                                                      the amounts of data read \n"\
		"                                                      from and written to pipes.\n"\
		"  (-nbd)                                            Monitor nbd throughput (only\n"\
		"                                                      device a is supported).\n"\
		"  (-cr-read )                                       Monitor cr read throughput "\
		"  (-cr-write )                                      Monitor cr write throughput "\
		"  (-disk-reqs-read)                                 Monitor the number of disk\n"\
		"                                                      read requests.\n"\
		"  (-disk-reqs-written)                              Monitor the number of disk\n"\
		"                                                      write reqs.\n"\
		"  (-disk-blocks-read)                               Monitor the number of blocks\n"\
		"                                                      read from disk.\n"\
		"  (-disk-blocks-written)                            Monitor the number of blocks\n"\
		"                                                      written to disk.\n"\
		"  (-disk-bytes-read)                                Monitor the number of bytes\n"\
		"                                                      read from disk. CAUTION:\n"\
		"                                                      this is based on a assumed\n"\
		"                                                      block size of 512 bytes!\n"\
		"  (-disk-bytes-written)                             Monitor the number of bytes\n"\
		"                                                      written to disk. CAUTION:\n"\
		"                                                      this is based on a assumed\n"\
		"                                                      block size of 512 bytes!\n"\
		"  (-disk-id <minor device id>)                      Specify the disk, default is\n"\
		"                                                      0, normally the first ide \n"\
		"                                                      disk\n"\
		"  (-cycles-user)                                    Monitor the cycles used by all\n"\
		"                                                      processes except the idle process.\n"
		"  (-cycles-intr)                                    Monitor the cycles used by all\n"\
		"                                                      interrupt handlers.\n"
		"  (-cycles-softirq)                                 Monitor the cycles used by all\n"\
		"                                                      softirqs (and tasklets).\n"
		"  (-cycles-idle)                                    Monitor the cycles used by the\n"\
		"                                                    idle process.\n"
		"  (-cycles-used)                                    Monitor the global number\n"
		"                                                      cycles used.\n"
		"  (-pid <process identifier>)                       Specify the process id. \n"
		"                                                      This is needed for all \n"
		"                                                      args beginning with \n"
		"                                                      -proc-*. \n"
		"  (-proc-cpu)                                       Monitor the CPU usage of a\n"
		"                                                      process. The process is \n"
		"                                                      identified by its pid \n"
		"                                                      (see -pid).\n"
		"  (-proc-cpu-sys)                                   Monitor the CPU usage of a \n"
		"                                                      process spend in kernel\n"
		"                                                      mode. The process is \n"
		"                                                      identified by its pid \n"
		"                                                      (see -pid).\n"
		"  (-proc-cpu-usr)                                   Monitor the CPU usage of a \n"
		"                                                      process spend in user\n"
		"                                                      mode. The process is \n"
		"                                                      identified by its pid \n"
		"                                                      (see -pid).\n"
		"  (-proc-mem-total)                                   Monitor the total program \n"
		"                                                      size. The process is \n"
		"                                                      identified by its pid \n"
		"                                                      (see -pid).\n"
		"  (-proc-mem-resident)                              Monitor the resident set \n"
		"                                                      size of a process. The \n"
		"                                                      process is identified by \n"
		"                                                      its pid (see -pid).\n"
		"  (-proc-mem-shared)                                Monitor the shared memory \n"
		"                                                      size of a process. The \n"
		"                                                      process is identified by \n"
		"                                                      its pid (see -pid).\n"
		"  (-proc-mem-stack)                                 Monitor the data/stack size\n"
		"                                                      of a process. The process\n"
		"                                                      is identified by its pid \n"
		"                                                      (see -pid).\n"
		"  (-proc-mem-lib)                                   Monitor the library size of\n"
		"                                                      a process. The process is \n"
		"                                                      identified by its pid \n"
		"                                                      (see -pid).\n"
		"  (-proc-mem-dirty)                                 Monitor the number of dirty \n"
		"                                                      pages of a process. The \n"
		"                                                      process is identified by\n"
		"                                                      its pid (see -pid).\n"
		"  (-mem-total)                                      Monitor the total amount of\n"
		"                                                      memory in the system.\n"
		"  (-mem-free)                                       Monitor the amount of free \n"
		"                                                      memory in the system.\n"
		"  (-mem-used)                                       Monitor the amount of used \n"
		"                                                      memory in the system.\n"
		"  (-mem-shared)                                     Monitor the amount of shared\n"
		"                                                      memory in the system.\n"
		"  (-mem-buffers)                                    Monitor the amount of buffer\n"
		"                                                      memory in the system.\n"
		"  (-mem-cached)                                     Monitor the amount of cached\n"
		"                                                      data in the system.\n"
		"  (-mem-swap-total)                                 Monitor the amount of swap\n"
		"                                                      memory in the system.\n"
		"  (-mem-swap-free)                                  Monitor the amount of swap\n"
		"                                                      memory not used by the \n"
		"                                                      system.\n"
		"  (-mem-swap-used)                                  Monitor the amount of swap\n"
		"                                                      memory used by the system.\n"
		"  (-intr [id])                                      Monitor the specified \n"
		"                                                      interrupt. id is the name\n"
		"                                                      of the interrupt. Use \n"
		"                                                      \"all\" to monitor the sum\n"
		"                                                      of all interrupts. If no \n"
		"                                                      id is specified \"all\"\n"
		"                                                      is assumed as id.\n"
		"  (-intr-cpu-id <cpu id>)                           Specify the CPU for \n"
		"                                                      interrupt monitoring. If\n"
		"                                                      not specified sum over all\n"
		"                                                      CPUs of the interrupt \n"
		"                                                      specified by -intr will be\n"
		"                                                      monitored. Use also \"all\"\n"
		"                                                      to get this effect.\n"
		"  (-intr-cycles [id])                               Monitor the CPU cycles\n"
		"                                                      used by the specified\n"
		"                                                      interrupt. id is the name\n"
		"                                                      of the interrupt. Use \n"
		"                                                      \"all\" to monitor the sum\n"
		"                                                      of all interrupts. If no \n"
		"                                                      id is specified \"all\"\n"
		"                                                      is assumed as id.\n"
		"  (-intr-cycles-cpu-id <cpu id>)                    Specify the CPU for\n"
		"                                                      interrupt cycles\n"
		"                                                      monitoring. If not\n"
		"                                                      specified sum over all\n"
		"                                                      CPUs of the interrupt \n"
		"                                                      specified by -intr-cycles\n"
		"                                                      will be monitored. Use\n"
		"                                                      also \"all\" to get this\n"
		"                                                      effect.\n"
		"  (-context)                                        Monitor number of context \n"
		"                                                      switches.\n"
		"  (-mp <mount-point>)                               Monitor usage of a given" 
		"                                                      mountpoint\n"
		"  (-raid <raid-number>)                             Monitor status of a raid5"; 


    i = 1;
    while ( (i < argc) && !errorStr )
		{
			if ( !strcmp( argv[i], "-testid" ) )
				{
					if ( argc <= i + 1 )
						{
							errorStr = "Missing update-interval specification.";
							break;
						}
					testID = argv[i+1];
					i += 2;
				}
			else if ( !strcmp( argv[i], "-update-interval" ) )
				{
					if ( argc <= i + 1 )
						{
							errorStr = "Missing update-interval specification.";
							break;
						}
					updateInterval = strtoul( argv[i+1], &cperr, 0 );
					if ( *cperr )
						{
							errorStr = "Error converting numeric update-interval specifier.";
							break;
						}
					i += 2;
				}
			else if ( !strcmp( argv[i], "-average-interval" ) )
				{
					if ( argc <= i + 1 )
						{
							errorStr = "Missing average-interval specification.";
							break;
						}
					averageInterval = strtoul( argv[i+1], &cperr, 0 );
					if ( *cperr )
						{
							errorStr = "Error converting numeric average-interval specifier.";
							break;
						}
					i += 2;
				}
			else if ( !strcmp( argv[i], "-cpu-usr" ) )
				{
					monitorCPU_USR = true;
					moniCount++;
					i += 1;
					continue;
				}
			else if ( !strcmp( argv[i], "-cpu-nice" ) )
				{
					monitorCPU_NICE = true;
					moniCount++;
					i += 1;
					continue;
				}
			else if ( !strcmp( argv[i], "-cpu-sys" ) )
				{
					monitorCPU_SYS = true;
					moniCount++;
					i += 1;
					continue;
				}
			else if ( !strcmp( argv[i], "-cpu-idle" ) )
				{
					monitorCPU_IDLE = true;
					moniCount++;
					i += 1;
					continue;
				}
			else if ( !strcmp( argv[i], "-cpu-used" ) )
				{
					monitorCPU_USED = true;
					moniCount++;
					i += 1;
					continue;
				}
			else if ( !strcmp( argv[i], "-cpu-id" ) )
				{
					long cpuID;
					if ( argc <= i + 1 )
						{
							errorStr = "Missing cpu-id specification.";
							break;
						}
					if ( !strcmp( argv[i+1], "all" ) )
						allCPUs = true;
					else
						{
							cpuID = strtol( argv[i+1], &cperr, 0 );
							if ( *cperr )
								{
									errorStr = "Error converting numeric cpu-id specifier.";
									break;
								}
							cpuIDs.insert( cpuIDs.end(), cpuID );
						}
					i += 2;
					continue;
				}
			else if ( !strcmp( argv[i], "-net" ) )
				{
					monitorNet = true;
					moniCount++;
					i += 1;
					continue;
				}
			else if ( !strcmp( argv[i], "-net-bytes-in" ) )
				{
					monitorNet_Bytes_In = true;
					moniCount++;
					i += 1;
					continue;
				}
			else if ( !strcmp( argv[i], "-net-bytes-out" ) )
				{
					monitorNet_Bytes_Out = true;
					moniCount++;
					i += 1;
					continue;
				}
			else if ( !strcmp( argv[i], "-net-packets-in" ) )
				{
					monitorNet_Packets_In = true;
					moniCount++;
					i += 1;
					continue;
				}
			else if ( !strcmp( argv[i], "-net-packets-out" ) )
				{
					monitorNet_Packets_Out = true;
					moniCount++;
					i += 1;
					continue;
				}
			else if ( !strcmp( argv[i], "-net-errors-in" ) )
				{
					monitorNet_Errors_In = true;
					moniCount++;
					i += 1;
					continue;
				}
			else if ( !strcmp( argv[i], "-net-errors-out" ) )
				{
					monitorNet_Errors_Out = true;
					moniCount++;
					i += 1;
					continue;
				}
			else if ( !strcmp( argv[i], "-net-collisions-out" ) )
				{
					monitorNet_Collisions_Out = true;
					moniCount++;
					i += 1;
					continue;
				}
			else if ( !strcmp( argv[i], "-net-total-errors" ) )
				{
					monitorNet_Total_Errors = true;
					moniCount++;
					i += 1;
					continue;
				}
			else if ( !strcmp( argv[i], "-net-id" ) )
				{
					long netID;
					if ( argc <= i + 1 )
						{
							errorStr = "Missing net-id specification.";
							break;
						}
					if ( !strcmp( argv[i+1], "all" ) )
						allNets = true;
					else
						{
							netID = strtol( argv[i+1], &cperr, 0 );
							if ( *cperr )
								{
									errorStr = "Error converting numeric net-id specifier.";
									break;
								}
							netIDs.insert( netIDs.end(), netID );
						}
					i += 2;
					continue;
				}
			else if ( !strcmp( argv[i], "-pipe-in" ) )
				{
					monitorPipe_In = true;
					moniCount++;
					i += 1;
					continue;
				}
			else if ( !strcmp( argv[i], "-pipe-out" ) )
				{
					monitorPipe_Out = true;
					moniCount++;
					i += 1;
					continue;
				}
			else if ( !strcmp( argv[i], "-pipe-total" ) )
				{
					monitorPipe_Total = true;
					moniCount++;
					i += 1;
					continue;
				}
			else if ( !strcmp( argv[i], "-pipe-diff" ) )
				{
					monitorPipe_Diff = true;
					moniCount++;
					i += 1;
					continue;
				}
			else if ( !strcmp( argv[i], "-pipe-avg" ) )
				{
					monitorPipe_Avg = true;
					moniCount++;
					i += 1;
					continue;
				}
			else if ( !strcmp( argv[i], "-nbd" ) )
				{
					monitorNBD = true;
					moniCount++;
					i += 1;
					continue;
				}
			else if ( !strcmp( argv[i], "-disk-reqs-read" ) )
				{
					monitorDisk_Reqs_Read = true;
					moniCount++;
					i += 1;
					continue;
				}

			else if ( !strcmp( argv[i], "-cr-read" ) )
				{
					monitorCRread = true;
					moniCount++;
					i += 1;
					continue;
				}
			else if ( !strcmp( argv[i], "-cr-write" ) )
				{
					monitorCRwrite = true;
					moniCount++;
					i += 1;
					continue;
				}
			else if ( !strcmp( argv[i], "-disk-reqs-written" ) )
				{
					monitorDisk_Reqs_Written = true;
					moniCount++;
					i += 1;
					continue;
				}
			else if ( !strcmp( argv[i], "-disk-blocks-read" ) )
				{
					monitorDisk_Blocks_Read = true;
					moniCount++;
					i += 1;
					continue;
				}
			else if ( !strcmp( argv[i], "-disk-blocks-written" ) )
				{
					monitorDisk_Blocks_Written = true;
					moniCount++;
					i += 1;
					continue;
				}
			else if ( !strcmp( argv[i], "-disk-bytes-read" ) )
				{
					monitorDisk_Bytes_Read = true;
					moniCount++;
					i += 1;
					continue;
				}
			else if ( !strcmp( argv[i], "-disk-bytes-written" ) )
				{
					monitorDisk_Bytes_Written = true;
					moniCount++;
					i += 1;
					continue;
				}
			else if ( !strcmp( argv[i], "-disk-id" ) )
				{
					long diskID;
					if ( argc <= i + 1 )
						{
							errorStr = "Missing disk-id specification.";
							break;
						}
					diskID = strtol( argv[i+1], &cperr, 0 );
					if ( *cperr )
						{
							errorStr = "Error converting numeric disk-id specifier.";
							break;
						}
					diskIDs.insert( diskIDs.end(), diskID );
					i += 2;
					continue;
				}
			else if (!strcmp(argv[i], "-pid")) {
				int pid;

				if(argc <= i + 1) {
					errorStr = "Missing process identifier.";
					break;
				}
				pid = strtol( argv[i+1], &cperr, 0 );
				if(*cperr) {
					errorStr = "Error converting numeric pid specifier.";
					break;
				}
				pIDs.push_back(pid);
				i += 2;
				continue;
			}
			else if (  !strcmp( argv[i], "-proc-cpu" ) ) {
				monitorProcess_CpuUsage = true;
				processMonitorCnt++;
				moniCount++;
				i += 1;
				continue;
			}
			else if (  !strcmp( argv[i], "-proc-cpu-sys" ) ) {
				monitorProcess_CpuUsage_System = true;
				processMonitorCnt++;
				moniCount++;
				i += 1;
				continue;
			}
			else if (  !strcmp( argv[i], "-proc-cpu-usr" ) ) {
				monitorProcess_CpuUsage_User = true;
				processMonitorCnt++;
				moniCount++;
				i += 1;
				continue;
			}
			else if (  !strcmp( argv[i], "-proc-mem-total" ) ) {
				monitorProcess_Size = true;
				processMonitorCnt++;
				moniCount++;
				i += 1;
				continue;
			}
			else if (  !strcmp( argv[i], "-proc-mem-resident" ) ) {
				monitorProcess_Mem_Resident = true;
				processMonitorCnt++;
				moniCount++;
				i += 1;
				continue;
			}
			else if (  !strcmp( argv[i], "-proc-mem-shared" ) ) {
				monitorProcess_Mem_Shared = true;
				processMonitorCnt++;
				moniCount++;
				i += 1;
				continue;
			}
			else if (  !strcmp( argv[i], "-proc-mem-stack" ) ) {
				monitorProcess_Mem_Stack = true;
				processMonitorCnt++;
				moniCount++;
				i += 1;
				continue;
			}
			else if (  !strcmp( argv[i], "-proc-mem-lib" ) ) {
				monitorProcess_Mem_Library = true;
				processMonitorCnt++;
				moniCount++;
				i += 1;
				continue;
			}
			else if (  !strcmp( argv[i], "-proc-mem-dirty" ) ) {
				monitorProcess_Mem_Dirty = true;
				processMonitorCnt++;
				moniCount++;
				i += 1;
				continue;
			}
			else if (  !strcmp( argv[i], "-mem-total" ) ) {
				monitorMem_Total = true;
				moniCount++;
				i += 1;
				continue;
			}
			else if (  !strcmp( argv[i], "-mem-free" ) ) {
				monitorMem_Free = true;
				moniCount++;
				i += 1;
				continue;
			}
			else if (  !strcmp( argv[i], "-mem-used" ) ) {
				monitorMem_Used = true;
				moniCount++;
				i += 1;
				continue;
			}
			else if (  !strcmp( argv[i], "-mem-shared" ) ) {
				monitorMem_Shared = true;
				moniCount++;
				i += 1;
				continue;
			}
			else if (  !strcmp( argv[i], "-mem-buffers" ) ) {
				monitorMem_Buffers = true;
				moniCount++;
				i += 1;
				continue;
			}
			else if (  !strcmp( argv[i], "-mem-cached" ) ) {
				monitorMem_Cached = true;
				moniCount++;
				i += 1;
				continue;
			}
			else if (  !strcmp( argv[i], "-mem-swap-total" ) ) {
				monitorMem_SwapTotal = true;
				moniCount++;
				i += 1;
				continue;
			}
			else if (  !strcmp( argv[i], "-mem-swap-free" ) ) {
				monitorMem_SwapFree = true;
				moniCount++;
				i += 1;
				continue;
			}
			else if (  !strcmp( argv[i], "-mem-swap-used" ) ) {
				monitorMem_SwapUsed = true;
				moniCount++;
				i += 1;
				continue;
			}
			else if (  !strcmp( argv[i], "-intr" ) ) {
				monitorInterrupt = true;
				if ( argc <= i + 1 ) {
					// if argument is last, set default value
					//if(find(IntrIds.begin(), IntrIds.end(), "all") == IntrIds.end()) {
					IntrIds.push_back("all");
					//}
					moniCount++;
					i += 1;
					continue;
				}
				if(*argv[i+1] == '-') { 
					// if no argument is specified, set default value
					//if(find(IntrIds.begin(), IntrIds.end(), "all") == IntrIds.end()) {
					IntrIds.push_back("all");
					//}
					moniCount++;
					i += 1;
					continue;
				}
				//if(find(IntrIds.begin(), IntrIds.end(), argv[i+1]) == IntrIds.end()) {
				IntrIds.push_back(argv[i+1]);
				moniCount++;
				//}
				i += 2;
				continue;
			}
			else if (  !strcmp( argv[i], "-intr-cpu-id" ) ) {
				if ( argc <= i + 1 ) {
					errorStr = "Missing cpu id.";
					break;
				}
				if (!strcmp(argv[i+1], "all")) {
					IntrCpuIds.push_back(INTR_ALLCPUS); 
				} else {
					int IntrCpuId = strtol( argv[i+1], &cperr, 0 );
					if ( *cperr ) {
						errorStr = "Error converting numeric cpu id.";
						break;
					}
					IntrCpuIds.push_back(IntrCpuId);
				}
				moniCount++;
				i += 2;
				continue;
			}
			else if (  !strcmp( argv[i], "-intr-cycles" ) ) {
				monitorInterruptCycles = true;
				if ( argc <= i + 1 ) {
					// if argument is last, set default value
					//if(find(IntrIds.begin(), IntrIds.end(), "all") == IntrIds.end()) {
					IntrCyclesIds.push_back("all");
					//}
					moniCount++;
					i += 1;
					continue;
				}
				if(*argv[i+1] == '-') { 
					// if no argument is specified, set default value
					//if(find(IntrIds.begin(), IntrIds.end(), "all") == IntrIds.end()) {
					IntrCyclesIds.push_back("all");
					//}
					moniCount++;
					i += 1;
					continue;
				}
				//if(find(IntrIds.begin(), IntrIds.end(), argv[i+1]) == IntrIds.end()) {
				IntrCyclesIds.push_back(argv[i+1]);
				moniCount++;
				//}
				i += 2;
				continue;
			}
			else if (  !strcmp( argv[i], "-intr-cycles-cpu-id" ) ) {
				if ( argc <= i + 1 ) {
					errorStr = "Missing cpu id.";
					break;
				}
				if (!strcmp(argv[i+1], "all")) {
					IntrCyclesCpuIds.push_back(INTR_ALLCPUS); 
				} else {
					int IntrCpuId = strtol( argv[i+1], &cperr, 0 );
					if ( *cperr ) {
						errorStr = "Error converting numeric cpu id.";
						break;
					}
					IntrCyclesCpuIds.push_back(IntrCpuId);
				}
				moniCount++;
				i += 2;
				continue;
			}
			else if (  !strcmp( argv[i], "-context" ) ) {
				monitorContextSwitches = true;
				moniCount++;
				i += 1;
				continue;
			}
			else if ( !strcmp( argv[i], "-cycles-user" ) )
				{
					monitorCyclesUser = true;
					moniCount++;
					i += 1;
					continue;
				}
			else if ( !strcmp( argv[i], "-cycles-intr" ) )
				{
					monitorCyclesIntr = true;
					moniCount++;
					i += 1;
					continue;
				}
			else if ( !strcmp( argv[i], "-cycles-softirq" ) )
				{
					monitorCyclesSoftIRQ = true;
					moniCount++;
					i += 1;
					continue;
				}
			else if ( !strcmp( argv[i], "-cycles-idle" ) )
				{
					monitorCyclesIdle = true;
					moniCount++;
					i += 1;
					continue;
				}
			else if ( !strcmp( argv[i], "-cycles-used" ) )
				{
					monitorCyclesUsed = true;
					moniCount++;
					i += 1;
					continue;
				}
			else if (  !strcmp( argv[i], "-mp" ) ) 
				{
					if ( argc <= i + 1 ) {
						errorStr = "Missing mount point identifier.";
						break;
					}
					monitorMountPoint = true;	
					mountPoints.push_back( argv[i+1] );    
					// path = argv[i+1];
					moniCount++;
					i += 2;
					continue;
				}
			else if (  !strcmp( argv[i], "-raid" ) ) 
				{
					if ( argc <= i + 1 ) {
						errorStr = "Missing RAID device number.";
						break;
					}
					monitorRAID5 = true;	
					RAID5Devices.push_back( atoi( argv[i+1] ) );    
					moniCount++;
					i += 2;
					continue;
				}
			else 
				{
					// print invalid argument 	    
					MLUCString buf =  "invalid argument: ";
					buf += argv[i];
					errorStr = (char*) buf.c_str();

					LOG( MLUCLog::kError, "Monitor", "Usage" )
						<< "Usage: " << argv[0] << usage << ENDLOG;
					LOG( MLUCLog::kError, "Monitor", "Usage" )
						<< "       " << errorStr << ENDLOG;
					return -1;
				}
		}
    if ( !errorStr && !moniCount )
		errorStr = "Must specify at least one monitoring object.";

    // if process monitoring is enabled but pid is missing then throw error
    if ( !errorStr && processMonitorCnt != 0 && pIDs.size() <= 0) {
		errorStr = "Process id is missing.";
    }
    // if pid is specified but no process monitoring object is specified
    if ( !errorStr && processMonitorCnt == 0 && pIDs.size() > 0) {
		errorStr = "Must specify at least one process monitoring object.";
    }

    if ( errorStr )
		{
			LOG( MLUCLog::kError, "Monitor", "Usage" )
				<< "Usage: " << argv[0] << usage << ENDLOG;
			LOG( MLUCLog::kError, "Monitor", "Usage" )
				<< "       " << errorStr << ENDLOG;
			return -1;
		}

    if ( gethostname( hostName, MAX_HOSTNAME_LENGTH )==-1 )
		{
			if ( testID )
				sprintf( filePrefix, "Monitor-%08u.%06u.%06u-%s-", date, time_s, time_us, testID );
			else
				sprintf( filePrefix, "Monitor-%08u.%06u.%06u-", date, time_s, time_us );
		}
    else
		{
			if ( testID )
				sprintf( filePrefix, "Monitor-%s-%08u.%06u.%06u-%s-", hostName, date, time_s, time_us, testID );
			else
				sprintf( filePrefix, "Monitor-%s-%08u.%06u.%06u-", hostName, date, time_s, time_us );
		}

    //sprintf( filePrefix, "Monitor-%08u.%06u.%06u-", date, time_s, time_us );
    sprintf( charDate, "%08u.%06u.%06u", date, time_s, time_us );
    const char* fileSuffix = ".mondat";


    if ( averageInterval!=0 && updateInterval != 0 )
		{
			averageCount = averageInterval / updateInterval;
			if ( averageCount <1 )
				averageCount = 1;
		}

    MLUCString filename;
    MLUCString fileDescr;
    MLUCString output;
    MLUCString output_ave;
    char IDtmp[32];
    MLUCValueMonitor* valMon;
    if ( cpuIDs.size()<=0 || allCPUs )
		cpuIDs.insert( cpuIDs.begin(), -1 );
    vector<long>::iterator cpuIter, cpuEnd;
    cpuIter = cpuIDs.begin();
    cpuEnd = cpuIDs.end();
    while ( cpuIter != cpuEnd )
		{
			sprintf( IDtmp, "-%03ld", *cpuIter );
			if ( monitorCPU_USR )
				{
					if ( *cpuIter ==-1 )
						{
							output =               "CPU Usr       :            ";
							output_ave =           "CPU Usr (avg) :            ";
							filename = filePrefix;
							filename += "CPU-Usr";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " CPU utilization Usr";
						}
					else
						{
							output =               "CPU";
							output += IDtmp;
							output +=                     " Usr       :        ";
							output_ave =           "CPU";
							output_ave += IDtmp;
							output_ave +=                 " Usr (avg) :        ";
							filename = filePrefix;
							filename += "CPU";
							filename += IDtmp;
							filename += "-Usr";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " CPU";
							fileDescr += IDtmp;
							fileDescr += " utilization Usr";
						}
					valMon = new MLUCValueMonitorCPU_User( filename.c_str(), fileDescr.c_str(),
														   output.c_str(), " %",
														   output_ave.c_str(), " %",
														   averageCount, *cpuIter);
					if ( valMon )
						monitors.insert( monitors.end(), valMon );
				}
			if ( monitorCPU_NICE )
				{
					if ( *cpuIter ==-1 )
						{
							output =               "CPU Nice       :           ";
							output_ave =           "CPU Nice (avg) :           ";
							filename = filePrefix;
							filename += "CPU-Nice";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " CPU utilization Nice";
						}
					else
						{
							output =               "CPU";
							output += IDtmp;
							output +=                     " Nice       :       ";
							output_ave =           "CPU";
							output_ave += IDtmp;
							output_ave +=                 " Nice (avg) :       ";
							filename = filePrefix;
							filename += "CPU";
							filename += IDtmp;
							filename += "-Nice";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " CPU";
							fileDescr += IDtmp;
							fileDescr += " utilization Nice";
						}
					valMon = new MLUCValueMonitorCPU_Nice( filename.c_str(), fileDescr.c_str(),
														   output.c_str(), " %",
														   output_ave.c_str(), " %",
														   averageCount, *cpuIter);
					if ( valMon )
						monitors.insert( monitors.end(), valMon );
				}
			if ( monitorCPU_SYS )
				{
					if ( *cpuIter ==-1 )
						{
							output =               "CPU Sys       :            ";
							output_ave =           "CPU Sys (avg) :            ";
							filename = filePrefix;
							filename += "CPU-Sys";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " CPU utilization Sys";
						}
					else
						{
							output =               "CPU";
							output += IDtmp;
							output +=                     " Sys       :        ";
							output_ave =           "CPU";
							output_ave += IDtmp;
							output_ave +=                 " Sys (avg) :        ";
							filename = filePrefix;
							filename += "CPU";
							filename += IDtmp;
							filename += "-Sys";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " CPU";
							fileDescr += IDtmp;
							fileDescr += " utilization Sys";
						}
					valMon = new MLUCValueMonitorCPU_Sys( filename.c_str(), fileDescr.c_str(),
														  output.c_str(), " %",
														  output_ave.c_str(), " %",
														  averageCount, *cpuIter);
					if ( valMon )
						monitors.insert( monitors.end(), valMon );
				}
			if ( monitorCPU_IDLE )
				{
					if ( *cpuIter ==-1 )
						{
							output =               "CPU Idle       :           ";
							output_ave =           "CPU Idle (avg) :           ";
							filename = filePrefix;
							filename += "CPU-Idle";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " CPU utilization Idle";
						}
					else
						{
							output =               "CPU";
							output += IDtmp;
							output +=                     " Idle       :       ";
							output_ave =           "CPU";
							output_ave += IDtmp;
							output_ave +=                 " Idle (avg) :       ";
							filename = filePrefix;
							filename += "CPU";
							filename += IDtmp;
							filename += "-Idle";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " CPU";
							fileDescr += IDtmp;
							fileDescr += " utilization Idle";
						}
					valMon = new MLUCValueMonitorCPU_Idle( filename.c_str(), fileDescr.c_str(),
														   output.c_str(), " %",
														   output_ave.c_str(), " %",
														   averageCount, *cpuIter);
					if ( valMon )
						monitors.insert( monitors.end(), valMon );
				}
			if ( monitorCPU_USED )
				{
					if ( *cpuIter ==-1 )
						{
							output =               "CPU Used       :           ";
							output_ave =           "CPU Used (avg) :           ";
							filename = filePrefix;
							filename += "CPU-Used";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " CPU utilization";
						}
					else
						{
							output =               "CPU";
							output += IDtmp;
							output +=                     " Used       :       ";
							output_ave =           "CPU";
							output_ave += IDtmp;
							output_ave +=                 " Used (avg) :       ";
							filename = filePrefix;
							filename += "CPU";
							filename += IDtmp;
							filename += "-Used";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " CPU";
							fileDescr += IDtmp;
							fileDescr += " utilization";
						}
					valMon = new MLUCValueMonitorCPU_Used( filename.c_str(), fileDescr.c_str(),
														   output.c_str(), " %",
														   output_ave.c_str(), " %",
														   averageCount, *cpuIter);
					if ( valMon )
						monitors.insert( monitors.end(), valMon );
				}
			cpuIter++;
		}

    if ( netIDs.size()<=0 || allNets )
		netIDs.insert( netIDs.begin(), -1 );
    vector<long>::iterator netIter, netEnd;
    netIter = netIDs.begin();
    netEnd = netIDs.end();
    while ( netIter != netEnd )
		{
			sprintf( IDtmp, "-%03ld", *netIter );
			if ( monitorNet )
				{
					if ( *netIter == -1 )
						{
							output =               "Net       :                ";
							output_ave =           "Net (avg) :                ";
							filename = filePrefix;
							filename += "Net";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Net Bytes In";
						}
					else
						{
							output =               "Net";
							output += IDtmp;
							output +=                     "       :            ";
							output_ave =           "Net";
							output_ave += IDtmp;
							output_ave +=                 " (avg) :            ";
							filename = filePrefix;
							filename += "Net-";
							filename += IDtmp;
							filename += "-";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Net-";
							fileDescr += IDtmp;
							fileDescr += " Bytes In";
						}
					valMon = new MLUCValueMonitorNet( filename.c_str(), fileDescr.c_str(),
													  output.c_str(), " Bytes",
													  output_ave.c_str(), " Bytes",
													  averageCount,
													  -1,
													  *netIter);
					if ( valMon )
						monitors.insert( monitors.end(), valMon );
				}
			if ( monitorNet_Bytes_In )
				{
					if ( *netIter==-1 )
						{
							output =               "Net Bytes In       :       ";
							output_ave =           "Net Bytes In (avg) :       ";
							filename = filePrefix;
							filename += "Net-BytesIn";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Net Bytes In";
						}
					else
						{
							output =               "Net";
							output += IDtmp;
							output +=                     " Bytes In       :   ";
							output_ave =           "Net";
							output_ave += IDtmp;
							output_ave +=                 " Bytes In (avg) :   ";
							filename = filePrefix;
							filename += "Net";
							filename += IDtmp;
							filename += "-BytesIn";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Net";
							fileDescr += IDtmp;
							fileDescr += " Bytes In";
						}

					valMon = new MLUCValueMonitorNet( filename.c_str(), fileDescr.c_str(),
													  output.c_str(), " Bytes",
													  output_ave.c_str(), " Bytes",
													  averageCount,
													  0,
													  *netIter);
					if ( valMon )
						monitors.insert( monitors.end(), valMon );
				}

			if ( monitorNet_Bytes_Out )
				{
					if ( *netIter==-1 )
						{
							output =               "Net Bytes Out       :      ";
							output_ave =           "Net Bytes Out (avg) :      ";
							filename = filePrefix;
							filename += "Net-BytesOut";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Net Bytes Out";
						}
					else
						{
							output =               "Net";
							output += IDtmp;
							output +=                     " Bytes Out     :    ";
							output_ave =           "Net";
							output_ave += IDtmp;
							output_ave +=                 " Bytes Out (avg):   ";
							filename = filePrefix;
							filename += "Net";
							filename += IDtmp;
							filename += "-BytesOut";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Net";
							fileDescr += IDtmp;
							fileDescr += " Bytes Out";
						}

					valMon = new MLUCValueMonitorNet( filename.c_str(), fileDescr.c_str(),
													  output.c_str(), " Bytes",
													  output_ave.c_str(), " Bytes",
													  averageCount,
													  1,
													  *netIter);
					if ( valMon )
						monitors.insert( monitors.end(), valMon );
				}

			if ( monitorNet_Packets_In )
				{
					if ( *netIter==-1 )
						{
							output =               "Net Packets In     :       ";
							output_ave =           "Net Packets In (avg):      ";
							filename = filePrefix;
							filename += "Net-PacketsIn";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Net Packets In";
						}
					else
						{
							output =               "Net";
							output += IDtmp;
							output +=                     " Packets In      :  ";
							output_ave =           "Net";
							output_ave += IDtmp;
							output_ave +=                 " Packets In (avg):  ";
							filename = filePrefix;
							filename += "Net";
							filename += IDtmp;
							filename += "-PacketsIn";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Net";
							fileDescr += IDtmp;
							fileDescr += " PacketsIn";
						}

					valMon = new MLUCValueMonitorNet( filename.c_str(), fileDescr.c_str(),
													  output.c_str(), " Packets",
													  output_ave.c_str(), " Packets",
													  averageCount,
													  2,
													  *netIter);
					if ( valMon )
						monitors.insert( monitors.end(), valMon );
				}
			if ( monitorNet_Packets_Out )
				{
					if ( *netIter==-1 )
						{
							output =               "Net Packets Out       :    ";
							output_ave =           "Net Packets Out (avg) :    ";
							filename = filePrefix;
							filename += "Net-PacketsOut";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Net Packets Out";
						}
					else
						{
							output =               "Net";
							output += IDtmp;
							output +=                     " Packets Out      : ";
							output_ave =           "Net";
							output_ave += IDtmp;
							output_ave +=                 " Packets Out (avg): ";
							filename = filePrefix;
							filename += "Net";
							filename += IDtmp;
							filename += "-PacketsOut";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Net";
							fileDescr += IDtmp;
							fileDescr += " PacketsOut";
						}

					valMon = new MLUCValueMonitorNet( filename.c_str(), fileDescr.c_str(),
													  output.c_str(), " Packets",
													  output_ave.c_str(), " Packets",
													  averageCount,
													  3,
													  *netIter);
					if ( valMon )
						monitors.insert( monitors.end(), valMon );
				}

			if ( monitorNet_Errors_In )
				{
					if ( *netIter==-1 )
						{
							output =               "Net Errors In     :       ";
							output_ave =           "Net Errors In (avg):      ";
							filename = filePrefix;
							filename += "Net-ErrorsIn";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Net Errors In";
						}
					else
						{
							output =               "Net";
							output += IDtmp;
							output +=                     " Errors In      :  ";
							output_ave =           "Net";
							output_ave += IDtmp;
							output_ave +=                 " Errors In (avg):  ";
							filename = filePrefix;
							filename += "Net";
							filename += IDtmp;
							filename += "-ErrorsIn";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Net";
							fileDescr += IDtmp;
							fileDescr += " ErrorsIn";
						}

					valMon = new MLUCValueMonitorNet( filename.c_str(), fileDescr.c_str(),
													  output.c_str(), " Errors",
													  output_ave.c_str(), " Errors",
													  averageCount,
													  4,
													  *netIter);
					if ( valMon )
						monitors.insert( monitors.end(), valMon );
				}
			if ( monitorNet_Errors_Out )
				{
					if ( *netIter==-1 )
						{
							output =               "Net Errors Out       :    ";
							output_ave =           "Net Errors Out (avg) :    ";
							filename = filePrefix;
							filename += "Net-ErrorsOut";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Net Errors Out";
						}
					else
						{
							output =               "Net";
							output += IDtmp;
							output +=                     " Errors Out      : ";
							output_ave =           "Net";
							output_ave += IDtmp;
							output_ave +=                 " Errors Out (avg): ";
							filename = filePrefix;
							filename += "Net";
							filename += IDtmp;
							filename += "-ErrorsOut";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Net";
							fileDescr += IDtmp;
							fileDescr += " ErrorsOut";
						}

					valMon = new MLUCValueMonitorNet( filename.c_str(), fileDescr.c_str(),
													  output.c_str(), " Errors",
													  output_ave.c_str(), " Errors",
													  averageCount,
													  5,
													  *netIter);
					if ( valMon )
						monitors.insert( monitors.end(), valMon );
				}
			if ( monitorNet_Collisions_Out )
				{
					if ( *netIter==-1 )
						{
							output =               "Net Collisions Out     :       ";
							output_ave =           "Net Collisions Out (avg):      ";
							filename = filePrefix;
							filename += "Net-CollisionsOut";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Net Collisions Out";
						}
					else
						{
							output =               "Net";
							output += IDtmp;
							output +=                     " Collisions Out      :  ";
							output_ave =           "Net";
							output_ave += IDtmp;
							output_ave +=                 " Collisions Out (avg):  ";
							filename = filePrefix;
							filename += "Net";
							filename += IDtmp;
							filename += "-CollisionsOut";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Net";
							fileDescr += IDtmp;
							fileDescr += " CollisionsOut";
						}

					valMon = new MLUCValueMonitorNet( filename.c_str(), fileDescr.c_str(),
													  output.c_str(), " Collisions",
													  output_ave.c_str(), " Collisions",
													  averageCount,
													  6,
													  *netIter);
					if ( valMon )
						monitors.insert( monitors.end(), valMon );
				}
			if ( monitorNet_Total_Errors )
				{
					if ( *netIter==-1 )
						{
							output =               "Net Total Errors       :    ";
							output_ave =           "Net Total Errors (avg) :    ";
							filename = filePrefix;
							filename += "Net-TotalErrors";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Net Total Errors";
						}
					else
						{
							output =               "Net";
							output += IDtmp;
							output +=                     " Total Errors      : ";
							output_ave =           "Net";
							output_ave += IDtmp;
							output_ave +=                 " Total Errors (avg): ";
							filename = filePrefix;
							filename += "Net";
							filename += IDtmp;
							filename += "-TotalErrors";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Net";
							fileDescr += IDtmp;
							fileDescr += " TotalErrors";
						}

					valMon = new MLUCValueMonitorNet( filename.c_str(), fileDescr.c_str(),
													  output.c_str(), " Total Errors",
													  output_ave.c_str(), " Total Errors",
													  averageCount,
													  7,
													  *netIter);
					if ( valMon )
						monitors.insert( monitors.end(), valMon );
				}

			netIter++;
		}



    if ( monitorPipe_Total )
		{
			output =                       "Pipe Total       :         ";
			output_ave =                   "Pipe Total (avg) :         ";
			filename = filePrefix;
			filename += "Pipe-Total";
			filename += fileSuffix;
			fileDescr = "Monitor ";
			fileDescr += charDate;
			fileDescr += " Pipe Throughput total";
			valMon = new MLUCValueMonitorPipe( filename.c_str(), fileDescr.c_str(),
											   output.c_str(), " Bytes",
											   output_ave.c_str(), " Bytes",
											   averageCount, 0 );
			if ( valMon )
				monitors.insert( monitors.end(), valMon );
		}

    if ( monitorPipe_In )
		{
			output =                       "Pipe In       :            ";
			output_ave =                   "Pipe In (avg) :            ";
			filename = filePrefix;
			filename += "Pipe-In";
			filename += fileSuffix;
			fileDescr = "Monitor ";
			fileDescr += charDate;
			fileDescr += " Pipe Input total";
			valMon = new MLUCValueMonitorPipe( filename.c_str(), fileDescr.c_str(),
											   output.c_str(), " Bytes",
											   output_ave.c_str(), " Bytes",
											   averageCount, 1 );
			if ( valMon )
				monitors.insert( monitors.end(), valMon );
		}

    if ( monitorPipe_Out )
		{
			output =                       "Pipe Out       :           ";
			output_ave =                   "Pipe Out (avg) :           ";
			filename = filePrefix;
			filename += "Pipe-Out";
			filename += fileSuffix;
			fileDescr = "Monitor ";
			fileDescr += charDate;
			fileDescr += " Pipe Ouput total";
			valMon = new MLUCValueMonitorPipe( filename.c_str(), fileDescr.c_str(),
											   output.c_str(), " Bytes",
											   output_ave.c_str(), " Bytes",
											   averageCount, 2 );
			if ( valMon )
				monitors.insert( monitors.end(), valMon );
		}

    if ( monitorPipe_Diff )
		{
			output =                       "Pipe Diff       :          ";
			output_ave =                   "Pipe Diff (avg) :          ";
			filename = filePrefix;
			filename += "Pipe-Diff";
			filename += fileSuffix;
			fileDescr = "Monitor ";
			fileDescr += charDate;
			fileDescr += " Pipe I/O Diff total";
			valMon = new MLUCValueMonitorPipe( filename.c_str(), fileDescr.c_str(),
											   output.c_str(), " Bytes",
											   output_ave.c_str(), " Bytes",
											   averageCount, 3 );
			if ( valMon )
				monitors.insert( monitors.end(), valMon );
		}

    if ( monitorPipe_Avg )
		{
			output =                       "Pipe Avg       :           ";
			output_ave =                   "Pipe Avg (avg) :           ";
			filename = filePrefix;
			filename += "Pipe-Avg";
			filename += fileSuffix;
			fileDescr = "Monitor ";
			fileDescr += charDate;
			fileDescr += " Pipe Average I/O total";
			valMon = new MLUCValueMonitorPipe( filename.c_str(), fileDescr.c_str(),
											   output.c_str(), " Bytes",
											   output_ave.c_str(), " Bytes",
											   averageCount, 4 );
			if ( valMon )
				monitors.insert( monitors.end(), valMon );
		}


    if ( monitorNBD )
		{
			output =                       "NBD            :           ";
			output_ave =                   "NBD (avg)      :           ";
			filename = filePrefix;
			filename += "NBD";
			filename += fileSuffix;
			fileDescr = "Monitor ";
			fileDescr += charDate;
			fileDescr += " NBD I/O ";
			valMon = new MLUCValueMonitorNBD( filename.c_str(), fileDescr.c_str(),
											  output.c_str(), " Bytes",
											  output_ave.c_str(), " Bytes",
											  averageCount, 4 );
			if ( valMon )
				monitors.insert( monitors.end(), valMon );
		}

    if ( monitorCRread )
		{
			output =                       "CR read        :           ";
			output_ave =                   "CR read (avg)  :           ";
			filename = filePrefix;
			filename += "CRread";
			filename += fileSuffix;
			fileDescr = "Monitor ";
			fileDescr += charDate;
			fileDescr += " CR Read ";
			valMon = new MLUCValueMonitorCR( filename.c_str(), fileDescr.c_str(),
											  output.c_str(), " Blocks",
											  output_ave.c_str(), " Blocks",
											  averageCount, 0 );
			if ( valMon )
				monitors.insert( monitors.end(), valMon );
		}
    if ( monitorCRwrite )
		{
			output =                       "CR write       :           ";
			output_ave =                   "CR write (avg) :           ";
			filename = filePrefix;
			filename += "CRwrite";
			filename += fileSuffix;
			fileDescr = "Monitor ";
			fileDescr += charDate;
			fileDescr += " CR Write ";
			valMon = new MLUCValueMonitorCR( filename.c_str(), fileDescr.c_str(),
											  output.c_str(), " Blocks",
											  output_ave.c_str(), " Blocks",
											  averageCount, 1 );
			if ( valMon )
				monitors.insert( monitors.end(), valMon );
		}


    // disk stuff
    if ( diskIDs.size()<=0 )
		diskIDs.insert( diskIDs.begin(), 0 );
    vector<long>::iterator diskIter, diskEnd;
    diskIter = diskIDs.begin();
    diskEnd = diskIDs.end();
    while ( diskIter != diskEnd )
		{
			sprintf( IDtmp, "-%03ld", *diskIter );
			if ( monitorDisk_Reqs_Read )
				{
					if ( *diskIter==0 )
						{
							output =               "Disk Reqs Read       :        ";
							output_ave =           "Disk Reqs Read (avg) :        ";
							filename = filePrefix;
							filename += "Disk-ReqsRead";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Disk Reqs Read";
						}
					else
						{
							output =               "Disk";
							output += IDtmp;
							output +=                     " Reqs Read         :   ";
							output_ave =           "Disk";
							output_ave += IDtmp;
							output_ave +=                 " Reqs Read (avg)   :   ";
							filename = filePrefix;
							filename += "Disk";
							filename += IDtmp;
							filename += "-ReqsRead";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Disk";
							fileDescr += IDtmp;
							fileDescr += " Reqs Read";
						}

					valMon = new MLUCValueMonitorDisk( filename.c_str(), fileDescr.c_str(),
													   output.c_str(), " Reqs",
													   output_ave.c_str(), " Reqs",
													   averageCount,
													   0,
													   *diskIter);
					if ( valMon )
						monitors.insert( monitors.end(), valMon );
				}

			if ( monitorDisk_Reqs_Written )
				{
					if ( *diskIter==0 )
						{
							output =               "Disk Reqs Written       :     ";
							output_ave =           "Disk Reqs Written (avg) :     ";
							filename = filePrefix;
							filename += "Disk-ReqsWritten";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Disk Reqs Written";
						}
					else
						{
							output =               "Disk";
							output += IDtmp;
							output +=                     " Reqs Written      :   ";
							output_ave =           "Disk";
							output_ave += IDtmp;
							output_ave +=                 " Reqs Written (avg):   ";
							filename = filePrefix;
							filename += "Disk";
							filename += IDtmp;
							filename += "-ReqsWritten";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Disk";
							fileDescr += IDtmp;
							fileDescr += " Reqs Written";
						}

					valMon = new MLUCValueMonitorDisk( filename.c_str(), fileDescr.c_str(),
													   output.c_str(), " Reqs",
													   output_ave.c_str(), " Reqs",
													   averageCount,
													   1,
													   *diskIter);
					if ( valMon )
						monitors.insert( monitors.end(), valMon );
				}

			if ( monitorDisk_Blocks_Read )
				{
					if ( *diskIter==0 )
						{
							output =               "Disk Blocks Read       :      ";
							output_ave =           "Disk Blocks Read (avg) :      ";
							filename = filePrefix;
							filename += "Disk-BlocksRead";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Disk Blocks Read";
						}
					else
						{
							output =               "Disk";
							output += IDtmp;
							output +=                     " Blocks Read       :   ";
							output_ave =           "Disk";
							output_ave += IDtmp;
							output_ave +=                 " Blocks Read (avg) :   ";
							filename = filePrefix;
							filename += "Disk";
							filename += IDtmp;
							filename += "-BlocksRead";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Disk";
							fileDescr += IDtmp;
							fileDescr += " BlocksRead";
						}

					valMon = new MLUCValueMonitorDisk( filename.c_str(), fileDescr.c_str(),
													   output.c_str(), " Blks",
													   output_ave.c_str(), " Blks",
													   averageCount,
													   2,
													   *diskIter);
					if ( valMon )
						monitors.insert( monitors.end(), valMon );
				}
			if ( monitorDisk_Blocks_Written )
				{
					if ( *diskIter==0 )
						{
							output =               "Disk Blocks Written       :   ";
							output_ave =           "Disk Blocks Written (avg) :   ";
							filename = filePrefix;
							filename += "Disk-BlocksWritten";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Disk Blocks Written";
						}
					else
						{
							output =               "Disk";
							output += IDtmp;
							output +=                     " Blocks Written      : ";
							output_ave =           "Disk";
							output_ave += IDtmp;
							output_ave +=                 " Blocks Written (avg): ";
							filename = filePrefix;
							filename += "Disk";
							filename += IDtmp;
							filename += "-BlocksWritten";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Disk";
							fileDescr += IDtmp;
							fileDescr += " BlocksWritten";
						}

					valMon = new MLUCValueMonitorDisk( filename.c_str(), fileDescr.c_str(),
													   output.c_str(), " Blks",
													   output_ave.c_str(), " Blks",
													   averageCount,
													   3,
													   *diskIter);
					if ( valMon )
						monitors.insert( monitors.end(), valMon );
				}
			if ( monitorDisk_Bytes_Read )
				{
					if ( *diskIter==0 )
						{
							output =               "Disk Bytes Read       :      ";
							output_ave =           "Disk Bytes Read (avg) :      ";
							filename = filePrefix;
							filename += "Disk-BytesRead";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Disk Bytes Read";
						}
					else
						{
							output =               "Disk";
							output += IDtmp;
							output +=                     " Bytes Read      :    ";
							output_ave =           "Disk";
							output_ave += IDtmp;
							output_ave +=                 " Bytes Read (avg):    ";
							filename = filePrefix;
							filename += "Disk";
							filename += IDtmp;
							filename += "-BytesWritten";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Disk";
							fileDescr += IDtmp;
							fileDescr += " BytessWritten";
						}

					valMon = new MLUCValueMonitorDisk( filename.c_str(), fileDescr.c_str(),
													   output.c_str(), " Bytes",
													   output_ave.c_str(), " Bytes",
													   averageCount,
													   4,
													   *diskIter);
					if ( valMon )
						monitors.insert( monitors.end(), valMon );
				}


			if ( monitorDisk_Bytes_Written )
				{
					if ( *diskIter==0 )
						{
							output =               "Disk Bytes Written        :  ";
							output_ave =           "Disk Bytes Written (avg)  :  ";
							filename = filePrefix;
							filename += "Disk-BytesWritten";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Disk Bytes Written";
						}
					else
						{
							output =               "Disk";
							output += IDtmp;
							output +=                     " Bytes Written       :";
							output_ave =           "Disk";
							output_ave += IDtmp;
							output_ave +=                 " Bytes Written (avg) :";
							filename = filePrefix;
							filename += "Disk";
							filename += IDtmp;
							filename += "-BytesWritten";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Disk";
							fileDescr += IDtmp;
							fileDescr += " BytesWritten";
						}

					valMon = new MLUCValueMonitorDisk( filename.c_str(), fileDescr.c_str(),
													   output.c_str(), " Bytes",
													   output_ave.c_str(), " Bytes",
													   averageCount,
													   5,
													   *diskIter);
					if ( valMon )
						monitors.insert( monitors.end(), valMon );
				}
			diskIter++;
		}

    // -----------------------------------------------------------------------
    // process monitoring
    // -----------------------------------------------------------------------
    for(vector<int>::iterator pidIter =  pIDs.begin();
		pidIter != pIDs.end(); pidIter++) {    // looping through all pids

		string ProcessFilename;

		GetFilenameFromPid(*pidIter, ProcessFilename);

		if(monitorProcess_CpuUsage) {
			sprintf(IDtmp, "%d",*pidIter);
			output  = "Process Usage (";
			output += IDtmp;
			output += ") :     ";
			output_ave = "Process Usage (avg) :      ";
			filename = filePrefix;
			filename += ProcessFilename.c_str();
			filename += "-";
			filename += IDtmp;
			filename += "-Cpu-Usage";
			filename += fileSuffix;
			fileDescr = "Monitor ";
			fileDescr += charDate;
			fileDescr += " Process Usage total";
			valMon = new MLUCValueMonitorProcess_CpuUsage( filename.c_str(),
														   fileDescr.c_str(),
														   output.c_str(), " %",
														   output_ave.c_str(), " %",
														   averageCount,*pidIter );
			if ( valMon )
				monitors.insert( monitors.end(), valMon );
		}

		if(monitorProcess_CpuUsage_User) {
			sprintf(IDtmp, "%d",*pidIter);
			output  = "Process Usage(usr) (";
			output += IDtmp;
			output += ") :";
			output_ave = "Process Usage(usr) (avg) : ";
			filename = filePrefix;
			filename += ProcessFilename.c_str();
			filename += "-";
			filename += IDtmp;
			filename += "-Cpu-Usage-User";
			filename += fileSuffix;
			fileDescr = "Monitor ";
			fileDescr += charDate;
			fileDescr += " Process Usage(usr) total";
			valMon = new MLUCValueMonitorProcess_CpuUsage_User( filename.c_str(),
																fileDescr.c_str(),
																output.c_str(), " %",
																output_ave.c_str(), " %",
																averageCount,*pidIter );
			if ( valMon )
				monitors.insert( monitors.end(), valMon );
		}

		if(monitorProcess_CpuUsage_System) {
			sprintf(IDtmp, "%d",*pidIter);
			output  = "Process Usage(sys) (";
			output += IDtmp;
			output += ") :";
			output_ave = "Process Usage(sys) (avg) : ";
			filename = filePrefix;
			filename += ProcessFilename.c_str();
			filename += "-";
			filename += IDtmp;
			filename += "-Cpu-Usage-System";
			filename += fileSuffix;
			fileDescr = "Monitor ";
			fileDescr += charDate;
			fileDescr += " Process Usage(sys) total";
			valMon = new MLUCValueMonitorProcess_CpuUsage_System( filename.c_str(),
																  fileDescr.c_str(),
																  output.c_str(), " %",
																  output_ave.c_str(), " %",
																  averageCount,*pidIter );
			if ( valMon )
				monitors.insert( monitors.end(), valMon );
		}

		if(monitorProcess_Size) {
			sprintf(IDtmp, "%d",*pidIter);
			output  = "Process Size (";
			output += IDtmp;
			output += ") :    ";
			output_ave = "Process Size (avg) :      ";
			filename = filePrefix;
			filename += ProcessFilename.c_str();
			filename += "-";
			filename += IDtmp;
			filename += "-Process-Size";
			filename += fileSuffix;
			fileDescr = "Monitor ";
			fileDescr += charDate;
			fileDescr += " Process Size total";
			valMon = new MLUCValueMonitorProcess_Size( filename.c_str(),
													   fileDescr.c_str(),
													   output.c_str(), " kB",
													   output_ave.c_str(), " kB",
													   averageCount,*pidIter );
			if ( valMon )
				monitors.insert( monitors.end(), valMon );
		}

		if(monitorProcess_Mem_Resident) {
			sprintf(IDtmp, "%d",*pidIter);
			output  = "Process Resident Mem (";
			output += IDtmp;
			output += ") :";
			output_ave = "Process Resident Mem (avg) :";
			filename = filePrefix;
			filename += ProcessFilename.c_str();
			filename += "-";
			filename += IDtmp;
			filename += "-Process-Resident-Mem";
			filename += fileSuffix;
			fileDescr = "Monitor ";
			fileDescr += charDate;
			fileDescr += " Process Resident Mem total";
			valMon = new MLUCValueMonitorProcess_Mem_Resident( filename.c_str(),
															   fileDescr.c_str(),
															   output.c_str(), " kB",
															   output_ave.c_str(), " kB",
															   averageCount,*pidIter );
			if ( valMon )
				monitors.insert( monitors.end(), valMon );
		}

		if(monitorProcess_Mem_Shared) {
			sprintf(IDtmp, "%d",*pidIter);
			output  = "Process Shared Mem (";
			output += IDtmp;
			output += ") :";
			output_ave = "Process Shared Mem (avg) :";
			filename = filePrefix;
			filename += ProcessFilename.c_str();
			filename += "-";
			filename += IDtmp;
			filename += "-Process-Shared-Mem";
			filename += fileSuffix;
			fileDescr = "Monitor ";
			fileDescr += charDate;
			fileDescr += " Process Shared Mem total";
			valMon = new MLUCValueMonitorProcess_Mem_Shared( filename.c_str(),
															 fileDescr.c_str(),
															 output.c_str(), " kB",
															 output_ave.c_str(), " kB",
															 averageCount,*pidIter );
			if ( valMon )
				monitors.insert( monitors.end(), valMon );
		}

		if(monitorProcess_Mem_Stack) {
			sprintf(IDtmp, "%d",*pidIter);
			output  = "Process Stack Mem (";
			output += IDtmp;
			output += ") :";
			output_ave = "Process Stack Mem (avg) : ";
			filename = filePrefix;
			filename += ProcessFilename.c_str();
			filename += "-";
			filename += IDtmp;
			filename += "-Process-Stack-Mem";
			filename += fileSuffix;
			fileDescr = "Monitor ";
			fileDescr += charDate;
			fileDescr += " Process Stack Mem total";
			valMon = new MLUCValueMonitorProcess_Mem_Stack( filename.c_str(),
															fileDescr.c_str(),
															output.c_str(), " kB",
															output_ave.c_str(), " kB",
															averageCount,*pidIter );
			if ( valMon )
				monitors.insert( monitors.end(), valMon );
		}

		if(monitorProcess_Mem_Library) {
			sprintf(IDtmp, "%d",*pidIter);
			output  = "Process Library Mem (";
			output += IDtmp;
			output += ") :";
			output_ave = "Process Library Mem (avg) :";
			filename = filePrefix;
			filename += ProcessFilename.c_str();
			filename += "-";
			filename += IDtmp;
			filename += "-Process-Library-Mem";
			filename += fileSuffix;
			fileDescr = "Monitor ";
			fileDescr += charDate;
			fileDescr += " Process Library Mem total";
			valMon = new MLUCValueMonitorProcess_Mem_Library( filename.c_str(),
															  fileDescr.c_str(),
															  output.c_str(), " kB",
															  output_ave.c_str(), " kB",
															  averageCount,*pidIter );
			if ( valMon )
				monitors.insert( monitors.end(), valMon );
		}

		if(monitorProcess_Mem_Dirty) {
			sprintf(IDtmp, "%d",*pidIter);
			output  = "Process Dirty Pages (";
			output += IDtmp;
			output += ") :";
			output_ave = "Process Dirty Pages (avg) :";
			filename = filePrefix;
			filename += ProcessFilename.c_str();
			filename += "-";
			filename += IDtmp;
			filename += "-Process-Dirty-Pages";
			filename += fileSuffix;
			fileDescr = "Monitor ";
			fileDescr += charDate;
			fileDescr += " Process Dirty Pages total";
			valMon = new MLUCValueMonitorProcess_Mem_Dirty( filename.c_str(),
															fileDescr.c_str(),
															output.c_str(), " Pages",
															output_ave.c_str(), " Pages",
															averageCount, *pidIter );
			if ( valMon )
				monitors.insert( monitors.end(), valMon );
		}
    }

    // -----------------------------------------------------------------------
    // memory monitoring
    // -----------------------------------------------------------------------
    if(monitorMem_Total) {
		output     = "Memory Total       :      ";
		output_ave = "Memory Total (avg) :      ";
		filename   = filePrefix;
		filename  += "Memory-Total";
		filename  += fileSuffix;
		fileDescr  = "Monitor ";
		fileDescr += charDate;
		fileDescr += " Memory Total";
		valMon = new MLUCValueMonitorMem_Total (filename.c_str(),
												fileDescr.c_str(),
												output.c_str(), " kB",
												output_ave.c_str(), " kB",
												averageCount);
		if ( valMon )
			monitors.insert( monitors.end(), valMon );
    }

    if(monitorMem_Free) {
		output     = "Memory Free       :       ";
		output_ave = "Memory Free (avg) :       ";
		filename   = filePrefix;
		filename  += "Memory-Free";
		filename  += fileSuffix;
		fileDescr  = "Monitor ";
		fileDescr += charDate;
		fileDescr += " Memory Free";
		valMon = new MLUCValueMonitorMem_Free (filename.c_str(),
											   fileDescr.c_str(),
											   output.c_str(), " kB",
											   output_ave.c_str(), " kB",
											   averageCount);
		if ( valMon )
			monitors.insert( monitors.end(), valMon );
    }
    if(monitorMem_Used) {
		output     = "Memory Used       :       ";
		output_ave = "Memory Used (avg) :       ";
		filename   = filePrefix;
		filename  += "Memory-Used";
		filename  += fileSuffix;
		fileDescr  = "Monitor ";
		fileDescr += charDate;
		fileDescr += " Memory Used";
		valMon = new MLUCValueMonitorMem_Used (filename.c_str(),
											   fileDescr.c_str(),
											   output.c_str(), " kB",
											   output_ave.c_str(), " kB",
											   averageCount);
		if ( valMon )
			monitors.insert( monitors.end(), valMon );
    }

    if(monitorMem_Shared) {
		output     = "Memory Shared       :     ";
		output_ave = "Memory Shared (avg) :     ";
		filename   = filePrefix;
		filename  += "Memory-Shared";
		filename  += fileSuffix;
		fileDescr  = "Monitor ";
		fileDescr += charDate;
		fileDescr += " Memory Shared";
		valMon = new MLUCValueMonitorMem_Shared (filename.c_str(),
												 fileDescr.c_str(),
												 output.c_str(), " kB",
												 output_ave.c_str(), " kB",
												 averageCount);
		if ( valMon )
			monitors.insert( monitors.end(), valMon );
    }

    if(monitorMem_Buffers) {
		output     = "Memory Buffers       :    ";
		output_ave = "Memory Buffers (avg) :    ";
		filename   = filePrefix;
		filename  += "Memory-Buffers";
		filename  += fileSuffix;
		fileDescr  = "Monitor ";
		fileDescr += charDate;
		fileDescr += " Memory Buffers";
		valMon = new MLUCValueMonitorMem_Buffers (filename.c_str(),
												  fileDescr.c_str(),
												  output.c_str(), " kB",
												  output_ave.c_str(), " kB",
												  averageCount);
		if ( valMon )
			monitors.insert( monitors.end(), valMon );
    }

    if(monitorMem_Cached) {
		output     = "Memory Cached       :     ";
		output_ave = "Memory Cached (avg) :     ";
		filename   = filePrefix;
		filename  += "Memory-Cached";
		filename  += fileSuffix;
		fileDescr  = "Monitor ";
		fileDescr += charDate;
		fileDescr += " Memory Cached";
		valMon = new MLUCValueMonitorMem_Cached (filename.c_str(),
												 fileDescr.c_str(),
												 output.c_str(), " kB",
												 output_ave.c_str(), " kB",
												 averageCount);
		if ( valMon )
			monitors.insert( monitors.end(), valMon );
    }

    if(monitorMem_SwapTotal) {
		output     = "Memory Swap Total       : ";
		output_ave = "Memory Swap Total (avg) : ";
		filename   = filePrefix;
		filename  += "Memory-Swap-Total";
		filename  += fileSuffix;
		fileDescr  = "Monitor ";
		fileDescr += charDate;
		fileDescr += " Memory Swap Total";
		valMon = new MLUCValueMonitorMem_SwapTotal (filename.c_str(),
													fileDescr.c_str(),
													output.c_str(), " kB",
													output_ave.c_str(), " kB",
													averageCount);
		if ( valMon )
			monitors.insert( monitors.end(), valMon );
    }

    if(monitorMem_SwapFree) {
		output     = "Memory Swap Free       :  ";
		output_ave = "Memory Swap Free (avg) :  ";
		filename   = filePrefix;
		filename  += "Memory-Swap-Free";
		filename  += fileSuffix;
		fileDescr  = "Monitor ";
		fileDescr += charDate;
		fileDescr += " Memory Swap Free";
		valMon = new MLUCValueMonitorMem_SwapFree (filename.c_str(),
												   fileDescr.c_str(),
												   output.c_str(), " kB",
												   output_ave.c_str(), " kB",
												   averageCount);
		if ( valMon )
			monitors.insert( monitors.end(), valMon );
    }

    if(monitorMem_SwapUsed) {
		output     = "Memory Swap Used       :  ";
		output_ave = "Memory Swap Used (avg) :  ";
		filename   = filePrefix;
		filename  += "Memory-Swap-Used";
		filename  += fileSuffix;
		fileDescr  = "Monitor ";
		fileDescr += charDate;
		fileDescr += " Memory Swap Used";
		valMon = new MLUCValueMonitorMem_SwapUsed (filename.c_str(),
												   fileDescr.c_str(),
												   output.c_str(), " kB",
												   output_ave.c_str(), " kB",
												   averageCount);
		if ( valMon )
			monitors.insert( monitors.end(), valMon );
    }

    // -------------------------------------------------------------------------
    // interrupt monitoring
    // -------------------------------------------------------------------------
    if(monitorInterrupt) {
		// if container is empty set to default value
		if(IntrCpuIds.size() == 0) { 
			IntrCpuIds.push_back(INTR_ALLCPUS);
		}

		vector<string>::iterator IterIntrId = IntrIds.begin();
		vector<int>::iterator IterIntrCpuId = IntrCpuIds.begin();
		for(;IterIntrId != IntrIds.end(); IterIntrId++ ) {
			for ( IterIntrCpuId = IntrCpuIds.begin(); IterIntrCpuId != IntrCpuIds.end(); IterIntrCpuId++ ) {
	
				output     = "Interrupt ";
				filename   = filePrefix;
				filename  += "Interrupt-";
				fileDescr  = "Monitor ";
				fileDescr += charDate;
				if(*IterIntrId == "all") {
					output    += "all";
					filename  += "all";
					if(*IterIntrCpuId == INTR_ALLCPUS) {
					} else {
						sprintf(IDtmp, "%d", *IterIntrCpuId);
						output    += " (cpu";
						output    += IDtmp;
						output    += ")";
						filename  += "-cpu";
						filename  += IDtmp;
					}
				} else {
					output += IterIntrId->c_str();
					filename += IterIntrId->c_str();
					if(*IterIntrCpuId == INTR_ALLCPUS) {
					} else {
						sprintf(IDtmp, "%d", *IterIntrCpuId);
						output    += " (cpu";
						output    += IDtmp;
						output    += ")";
						filename  += "-cpu";
						filename += IDtmp;
					}
				}

				output_ave = output.c_str();
				output_ave +=" (avg) :\t";
				fileDescr += " ";
				fileDescr += output.c_str();

				output    += "       :\t";
				filename  += fileSuffix;
				valMon = new MLUCValueMonitorInterrupt(filename.c_str(),
													   fileDescr.c_str(),
													   output.c_str(), " Intr",
													   output_ave.c_str(), " Intr",
													   averageCount,
													   IterIntrId->c_str(),
													   *IterIntrCpuId);
				if ( valMon )  
					monitors.insert( monitors.end(), valMon );
			}
		}
    }

    
    // -------------------------------------------------------------------------
    // interrupt cycles  monitoring
    // -------------------------------------------------------------------------

    if(monitorInterruptCycles) {
		// if container is empty set to default value
		if(IntrCyclesCpuIds.size() == 0) { 
			IntrCyclesCpuIds.push_back(INTR_ALLCPUS);
		}

		vector<string>::iterator IterIntrId = IntrCyclesIds.begin();
		vector<int>::iterator IterIntrCpuId = IntrCyclesCpuIds.begin();
		for(;IterIntrId != IntrCyclesIds.end(); IterIntrId++ ) {
			for ( IterIntrCpuId = IntrCyclesCpuIds.begin(); IterIntrCpuId != IntrCyclesCpuIds.end(); IterIntrCpuId++ ) {
	
				output     = "Intr. Cycles ";
				filename   = filePrefix;
				filename  += "InterruptCycles-";
				fileDescr  = "Monitor ";
				fileDescr += charDate;
				if(*IterIntrId == "all") {
					output    += "all";
					filename  += "all";
					if(*IterIntrCpuId == INTR_ALLCPUS) {
					} else {
						sprintf(IDtmp, "%d", *IterIntrCpuId);
						output    += " (cpu";
						output    += IDtmp;
						output    += ")";
						filename  += "-cpu";
						filename  += IDtmp;
					}
				} else {
					output += IterIntrId->c_str();
					filename += IterIntrId->c_str();
					if(*IterIntrCpuId == INTR_ALLCPUS) {
					} else {
						sprintf(IDtmp, "%d", *IterIntrCpuId);
						output    += " (cpu";
						output    += IDtmp;
						output    += ")";
						filename  += "-cpu";
						filename += IDtmp;
					}
				}

				output_ave = output.c_str();
				output_ave +=" (avg) :\t";
				fileDescr += " ";
				fileDescr += output.c_str();

				output    += "       :\t";
				filename  += fileSuffix;
				valMon = new MLUCValueMonitorInterruptCycles(filename.c_str(),
															 fileDescr.c_str(),
															 output.c_str(), " %",
															 output_ave.c_str(), " %",
															 averageCount,
															 IterIntrId->c_str(),
															 *IterIntrCpuId);
				if ( valMon )  
					monitors.insert( monitors.end(), valMon );
			}
		}
    }

    // -------------------------------------------------------------------------
    //  mount point monitoring 
    // -------------------------------------------------------------------------    
    
    if( monitorMountPoint ) {
		char pathTmp[128];
		char pathCopy[128];
		char *path;

		for(vector<char *>::iterator mpIter =  mountPoints.begin();
			mpIter != mountPoints.end(); mpIter++) {    
	
			path = *mpIter;
			strcpy( pathCopy, path );
	
			// replace slashes with underscores
			int len = strlen( pathCopy );
			for( int i=0; i<len; i++ ) {
				if( *(pathCopy+i) == '/' ) {
					*(pathCopy+i) = '_'; 
				}
			}

			sprintf( pathTmp, "Usage %s           :", pathCopy );
			output = pathTmp;

			sprintf( pathTmp, "Usage %s (ave)     :", pathCopy );
			output_ave =  pathTmp;

			filename   = filePrefix;

			sprintf( pathTmp, "Usage-%s", pathCopy );
			filename  += pathTmp;

			filename  += fileSuffix;

			fileDescr  = "Monitor ";
			fileDescr += charDate;

			sprintf( pathTmp, "Usage %s", pathCopy );
			fileDescr += pathTmp;

			valMon = new MLUCValueMonitorMountPoint(filename.c_str(),
													fileDescr.c_str(),
													output.c_str(), " MB",
													output_ave.c_str(), " MB",
													averageCount, path);
			if (valMon) {
				monitors.insert( monitors.end(), valMon );
			}
      
		}
    }


    // -------------------------------------------------------------------------
    //  RAID5 monitoring 
    // -------------------------------------------------------------------------    
    
    if( monitorRAID5 ) {

		char raid5Tmp[128];

		for(vector<int>::iterator raid5Iter =  RAID5Devices.begin();
			raid5Iter != RAID5Devices.end(); raid5Iter++) {    
	
			sprintf( raid5Tmp, "Status md %d           :", *raid5Iter );
			output = raid5Tmp;

			sprintf( raid5Tmp, "Status md %d (avg.)    :", *raid5Iter );
			output_ave =  raid5Tmp;

			filename   = filePrefix;

			sprintf( raid5Tmp, "Status-md%d", *raid5Iter );
			filename  += raid5Tmp;

			filename  += fileSuffix;

			fileDescr  = "Monitor ";
			fileDescr += charDate;

			sprintf( raid5Tmp, "Status-md%d", *raid5Iter );
			fileDescr += raid5Tmp;

			valMon = new MLUCValueMonitorRAID5(filename.c_str(),
											   fileDescr.c_str(),
											   output.c_str(), " ",
											   output_ave.c_str(), " ",
											   averageCount, *raid5Iter);
			if (valMon) {
				monitors.insert( monitors.end(), valMon );
			}
      
		}
    }

    
    // -------------------------------------------------------------------------
    // context switch monitoring
    // -------------------------------------------------------------------------
    if(monitorContextSwitches) {
		output     = "Context Switches        :    ";
		output_ave = "Context Switches (avg)  :    ";
		filename   = filePrefix;
		filename  += "ContextSwitches";
		filename  += fileSuffix;
		fileDescr  = "Monitor ";
		fileDescr += charDate;
		fileDescr += " Context Switches";
		valMon = new MLUCValueMonitorContextSwitches(filename.c_str(),
													 fileDescr.c_str(),
													 output.c_str(), "",
													 output_ave.c_str(), "",
													 averageCount);
		if (valMon)
			monitors.insert( monitors.end(), valMon );
    }
    
    /*
	  if ( cpuIDs.size()<=0 || allCPUs )
	  cpuIDs.insert( cpuIDs.begin(), -1 );
    */
    cpuIter = cpuIDs.begin();
    cpuEnd = cpuIDs.end();
    while ( cpuIter != cpuEnd )
		{
			sprintf( IDtmp, "-%03ld", *cpuIter );
			if ( monitorCyclesUser )
				{
					if ( *cpuIter ==-1 )
						{
							output =               "Cycles User       :        ";
							output_ave =           "Cycles User (avg) :        ";
							filename = filePrefix;
							filename += "Cycles-User";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Cycle utilization";
						}
					else
						{
							output =               "Cycles";
							output += IDtmp;
							output +=                     " User       :    ";
							output_ave =           "Cycles";
							output_ave += IDtmp;
							output_ave +=                 " User (avg) :    ";
							filename = filePrefix;
							filename += "Cycles";
							filename += IDtmp;
							filename += "-User";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Cycles";
							fileDescr += IDtmp;
							fileDescr += " utilization";
						}
					valMon = new MLUCValueMonitorCycles( filename.c_str(), fileDescr.c_str(),
														 output.c_str(), " %",
														 output_ave.c_str(), " %",
														 averageCount, 
														 MLUCValueMonitorCycles::USER, *cpuIter);
					if ( valMon )
						monitors.insert( monitors.end(), valMon );
				}
			if ( monitorCyclesIntr )
				{
					if ( *cpuIter ==-1 )
						{
							output =               "Cycles Intr       :        ";
							output_ave =           "Cycles Intr (avg) :        ";
							filename = filePrefix;
							filename += "Cycles-Intr";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Cycle utilization";
						}
					else
						{
							output =               "Cycles";
							output += IDtmp;
							output +=                     " Intr       :    ";
							output_ave =           "Cycles";
							output_ave += IDtmp;
							output_ave +=                 " Intr (avg) :    ";
							filename = filePrefix;
							filename += "Cycles";
							filename += IDtmp;
							filename += "-Intr";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Cycles";
							fileDescr += IDtmp;
							fileDescr += " utilization";
						}
					valMon = new MLUCValueMonitorCycles( filename.c_str(), fileDescr.c_str(),
														 output.c_str(), " %",
														 output_ave.c_str(), " %",
														 averageCount, 
														 MLUCValueMonitorCycles::INTR, *cpuIter);
					if ( valMon )
						monitors.insert( monitors.end(), valMon );
				}
			if ( monitorCyclesSoftIRQ )
				{
					if ( *cpuIter ==-1 )
						{
							output =               "Cycles SoftIRQ       :     ";
							output_ave =           "Cycles SoftIRQ (avg) :     ";
							filename = filePrefix;
							filename += "Cycles-SoftIRQ";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Cycle utilization";
						}
					else
						{
							output =               "Cycles";
							output += IDtmp;
							output +=                     " SoftIRQ       : ";
							output_ave =           "Cycles";
							output_ave += IDtmp;
							output_ave +=                 " SoftIRQ (avg) : ";
							filename = filePrefix;
							filename += "Cycles";
							filename += IDtmp;
							filename += "-SoftIRQ";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Cycles";
							fileDescr += IDtmp;
							fileDescr += " utilization";
						}
					valMon = new MLUCValueMonitorCycles( filename.c_str(), fileDescr.c_str(),
														 output.c_str(), " %",
														 output_ave.c_str(), " %",
														 averageCount, 
														 MLUCValueMonitorCycles::SOFTIRQ, *cpuIter);
					if ( valMon )
						monitors.insert( monitors.end(), valMon );
				}
			if ( monitorCyclesIdle )
				{
					if ( *cpuIter ==-1 )
						{
							output =               "Cycles Idle       :        ";
							output_ave =           "Cycles Idle (avg) :        ";
							filename = filePrefix;
							filename += "Cycles-Idle";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Cycle utilization";
						}
					else
						{
							output =               "Cycles";
							output += IDtmp;
							output +=                     " Idle       :    ";
							output_ave =           "Cycles";
							output_ave += IDtmp;
							output_ave +=                 " Idle (avg) :    ";
							filename = filePrefix;
							filename += "Cycles";
							filename += IDtmp;
							filename += "-Idle";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Cycles";
							fileDescr += IDtmp;
							fileDescr += " utilization";
						}
					valMon = new MLUCValueMonitorCycles( filename.c_str(), fileDescr.c_str(),
														 output.c_str(), " %",
														 output_ave.c_str(), " %",
														 averageCount, 
														 MLUCValueMonitorCycles::IDLE, *cpuIter);
					if ( valMon )
						monitors.insert( monitors.end(), valMon );
				}
			if ( monitorCyclesUsed )
				{
					if ( *cpuIter ==-1 )
						{
							output =               "Cycles Used       :        ";
							output_ave =           "Cycles Used (avg) :        ";
							filename = filePrefix;
							filename += "Cycles-Used";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Cycle utilization";
						}
					else
						{
							output =               "Cycles";
							output += IDtmp;
							output +=                     " Used       :    ";
							output_ave =           "Cycles";
							output_ave += IDtmp;
							output_ave +=                 " Used (avg) :    ";
							filename = filePrefix;
							filename += "Cycles";
							filename += IDtmp;
							filename += "-Used";
							filename += fileSuffix;
							fileDescr = "Monitor ";
							fileDescr += charDate;
							fileDescr += " Cycles";
							fileDescr += IDtmp;
							fileDescr += " utilization";
						}
					valMon = new MLUCValueMonitorCycles( filename.c_str(), fileDescr.c_str(),
														 output.c_str(), " %",
														 output_ave.c_str(), " %",
														 averageCount, 
														 MLUCValueMonitorCycles::USED, *cpuIter);
					if ( valMon )
						monitors.insert( monitors.end(), valMon );
				}
			cpuIter++;
		}



    signal( SIGQUIT, QuitSignalHandler );

    vector<MLUCValueMonitor*>::iterator iter, end;

    iter = monitors.begin();
    end = monitors.end();
    while ( iter != end )
		{
			if ( (*iter) )
				(*iter)->StartMonitoring();
			iter++;
		}


    uint64 value;
    int ret;
    while ( !gQuit )
		{
			iter = monitors.begin();
			end = monitors.end();
			while ( iter != end )
				{
					if ( (*iter) )
						{
							ret = (*iter)->GetValue( value );
							if ( ret )
								{
									LOG( MLUCLog::kError, "Monitor", "Error Getting Value" )
										<< "Error getting value from monitoring object: "
										<< strerror(ret) << " (" << MLUCLog::kDec << ret
										<< ")." << ENDLOG;
									iter++;
									continue;
								}
							ret = (*iter)->OutputValue( value );
							printf( "\n" );
						}
					iter++;
				}
			printf( "-------------------------------------------------------------------------------\n\n" );
			usleep( updateInterval*1000 );
		}

    LOG( MLUCLog::kError, "Monitor", "Quitting" )
		<< "Quitting now..." << ENDLOG;

    iter = monitors.begin();
    end = monitors.end();
    while ( iter != end )
		{
			if ( (*iter) )
				(*iter)->StopMonitoring();
			iter++;
		}


    iter = monitors.begin();
    end = monitors.end();
    while ( iter != end )
		{
			if ( (*iter) )
				delete (*iter);
			iter++;
		}
    return 0;
}

void QuitSignalHandler( int )
{
    gQuit = true;
}

bool GetFilenameFromPid(int pid, string& filename)
{
#if __GNUC__>=3
        std::ostringstream statfile;
#else
	ostrstream statfile;
#endif

	statfile << "/proc/" << pid << "/status";
#if __GNUC__>=3
	ifstream fin(statfile.str().c_str());
#else
	ifstream fin(statfile.str());
#endif

	if(!fin) {
		return false;
	}

	string line;
	do{
		fin >> filename;
	} while(filename != "Name:");
	fin >> filename;

	return true;
}

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
