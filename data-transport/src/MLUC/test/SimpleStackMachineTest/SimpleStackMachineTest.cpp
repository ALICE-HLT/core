/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/


#include "MLUCSimpleStackMachineAssembler.hpp"
#include "MLUCSimpleStackMachine.hpp"
#include "MLUCTests.hpp"
#include <iostream>

MLUCINITTESTS();

int main( int, char** )
    {

    MLUCSimpleStackMachineAssembler assembler;
    MLUCSimpleStackMachineAssembler::TError asmRet;

    MLUCSimpleStackMachine machine;
    MLUCSimpleStackMachine::TError machRet;
    
    uint64 result;
    

    asmRet = assembler.Assemble( "NOP" );
    MLUCTEST( asmRet.Ok(), "Assembly 1" );
    if ( MLUCLASTTESTFAILURE() )
	{
	std::cerr << asmRet.Description() << std::endl;
	}
    MLUCTEST( assembler.GetCodeWordCount()==1, "Assembly 1 code length" );
    if ( MLUCLASTTESTFAILURE() )
	{
	std::cerr << "Assembly code length: " << assembler.GetCodeWordCount() << std::endl;
	}
    else
	{
	MLUCTEST( assembler.GetCode()[0]==MLUCSimpleStackMachine::kNOP, "Assembly 1 code" );
	}

    asmRet = assembler.Assemble( "LNOT;LAND;NOP" );
    MLUCTEST( asmRet.Ok(), "Assembly 2" );
    if ( MLUCLASTTESTFAILURE() )
	{
	std::cerr << asmRet.Description() << std::endl;
	}
    MLUCTEST( assembler.GetCodeWordCount()==3, "Assembly 2 code length" );
    if ( MLUCLASTTESTFAILURE() )
	{
	std::cerr << "Assembly code length: " << assembler.GetCodeWordCount() << std::endl;
	}
    else
	{
	MLUCTEST( assembler.GetCode()[0]==MLUCSimpleStackMachine::kLNOT, "Assembly 2 code" );
	MLUCTEST( assembler.GetCode()[1]==MLUCSimpleStackMachine::kLAND, "Assembly 2 code" );
	MLUCTEST( assembler.GetCode()[2]==MLUCSimpleStackMachine::kNOP, "Assembly 2 code" );
	}


    asmRet = assembler.Assemble( "LNOT; LAND ; NOP" );
    MLUCTEST( asmRet.Ok(), "Assembly 3" );
    if ( MLUCLASTTESTFAILURE() )
	{
	std::cerr << asmRet.Description() << std::endl;
	}
    MLUCTEST( assembler.GetCodeWordCount()==3, "Assembly 3 code length" );
    if ( MLUCLASTTESTFAILURE() )
	{
	std::cerr << "Assembly code length: " << assembler.GetCodeWordCount() << std::endl;
	}
    else
	{
	MLUCTEST( assembler.GetCode()[0]==MLUCSimpleStackMachine::kLNOT, "Assembly 3 code" );
	MLUCTEST( assembler.GetCode()[1]==MLUCSimpleStackMachine::kLAND, "Assembly 3 code" );
	MLUCTEST( assembler.GetCode()[2]==MLUCSimpleStackMachine::kNOP, "Assembly 3 code" );
	}


    asmRet = assembler.Assemble( "label1:NOP" );
    MLUCTEST( asmRet.Ok(), "Assembly 4" );
    if ( MLUCLASTTESTFAILURE() )
	{
	std::cerr << "Error: " << asmRet.Description() << " - line nr.: " << asmRet.LineNr() << std::endl;
	}

    asmRet = assembler.Assemble( "label1:NOP ; JUMP label1" );
    MLUCTEST( asmRet.Ok(), "Assembly 5" );
    if ( MLUCLASTTESTFAILURE() )
	{
	std::cerr << "Error: " << asmRet.Description() << " - line nr.: " << asmRet.LineNr() << std::endl;
	}
    MLUCTEST( assembler.GetCodeWordCount()==3, "Assembly 5 code length" );
    if ( MLUCLASTTESTFAILURE() )
	{
	std::cerr << "Assembly code length: " << assembler.GetCodeWordCount() << std::endl;
	}
    else
	{
	MLUCTEST( assembler.GetCode()[0]==MLUCSimpleStackMachine::kNOP, "Assembly 5 code" );
	MLUCTEST( assembler.GetCode()[1]==MLUCSimpleStackMachine::kJUMP, "Assembly 5 code" );
	MLUCTEST( assembler.GetCode()[2]==0, "Assembly 5 code" ); // JUMP address
	}


    asmRet = assembler.Assemble( "NOP;label1:NOP ; JUMP label1 ; HALT" );
    MLUCTEST( asmRet.Ok(), "Assembly 6" );
    if ( MLUCLASTTESTFAILURE() )
	{
	std::cerr << "Error: " << asmRet.Description() << " - line nr.: " << asmRet.LineNr() << std::endl;
	}
    MLUCTEST( assembler.GetCodeWordCount()==5, "Assembly 6 code length" );
    if ( MLUCLASTTESTFAILURE() )
	{
	std::cerr << "Assembly code length: " << assembler.GetCodeWordCount() << std::endl;
	}
    else
	{
	MLUCTEST( assembler.GetCode()[0]==MLUCSimpleStackMachine::kNOP, "Assembly 6 code" );
	MLUCTEST( assembler.GetCode()[1]==MLUCSimpleStackMachine::kNOP, "Assembly 6 code" );
	MLUCTEST( assembler.GetCode()[2]==MLUCSimpleStackMachine::kJUMP, "Assembly 6 code" );
	MLUCTEST( assembler.GetCode()[3]==1, "Assembly 6 code" ); // JUMP address
	MLUCTEST( assembler.GetCode()[4]==MLUCSimpleStackMachine::kHALT, "Assembly 6 code" ); // JUMP address
	}


    asmRet = assembler.Assemble( "PUSHCONST 2 ; PUSHCONST 2 ; ADD" );
    MLUCTEST( asmRet.Ok(), "Assembly 7" );
    if ( MLUCLASTTESTFAILURE() )
	{
	std::cerr << "Error: " << asmRet.Description() << " - line nr.: " << asmRet.LineNr() << std::endl;
	}
    MLUCTEST( assembler.GetCodeWordCount()==5, "Assembly 7 code length" );
    if ( MLUCLASTTESTFAILURE() )
	{
	std::cerr << "Assembly code length: " << assembler.GetCodeWordCount() << std::endl;
	}
    else
	{
	MLUCTEST( assembler.GetCode()[0]==MLUCSimpleStackMachine::kPUSHCONST, "Assembly 7 code" );
	MLUCTEST( assembler.GetCode()[1]==2, "Assembly 7 code" );
	MLUCTEST( assembler.GetCode()[2]==MLUCSimpleStackMachine::kPUSHCONST, "Assembly 7 code" );
	MLUCTEST( assembler.GetCode()[3]==2, "Assembly 7 code" );
	MLUCTEST( assembler.GetCode()[4]==MLUCSimpleStackMachine::kADD, "Assembly 7 code" ); // JUMP address
	}

    machRet = machine.SetCode( assembler );
    MLUCTEST( machRet.Ok(), "Set Code 7" );
    if ( MLUCLASTTESTFAILURE() )
	{
	std::cerr << "Error: " << machRet.Description() << " - Code word count: " << machine.GetCodeWordCount() << " - IP: " << machine.GetIP() << std::endl;
	}
    machRet = machine.Run();
    MLUCTEST( machRet.Ok(), "Run Code 7" );
    if ( MLUCLASTTESTFAILURE() )
	{
	std::cerr << "Error: " << machRet.Description() << " - Code word count: " << machine.GetCodeWordCount() << " - IP: " << machine.GetIP() << std::endl;
	}
    machRet = machine.GetResult( result );
    MLUCTEST( machRet.Ok(), "Get Result 7" );
    if ( MLUCLASTTESTFAILURE() )
	{
	std::cerr << "Error: " << machRet.Description() << std::endl;
	}
    MLUCTEST( result==4, "Result Check 7" );
    
    asmRet = assembler.Assemble( "PUSHCONST 2 ; DUP ; ADD ; PUSHCONST 2 ; SUB ; PUSHCONST 3 ; MULT ; PUSHCONST 2 ; DIV ; PUSHCONST 2 ; MOD" );
    MLUCTEST( asmRet.Ok(), "Assembly 8" );
    if ( MLUCLASTTESTFAILURE() )
	{
	std::cerr << "Error: " << asmRet.Description() << " - line nr.: " << asmRet.LineNr() << std::endl;
	}
    machRet = machine.SetCode( assembler );
    MLUCTEST( machRet.Ok(), "Set Code 8" );
    if ( MLUCLASTTESTFAILURE() )
	{
	std::cerr << "Error: " << machRet.Description() << " - Code word count: " << machine.GetCodeWordCount() << " - IP: " << machine.GetIP() << std::endl;
	}
    machRet = machine.Run();
    MLUCTEST( machRet.Ok(), "Run Code 8" );
    if ( MLUCLASTTESTFAILURE() )
	{
	std::cerr << "Error: " << machRet.Description() << " - Code word count: " << machine.GetCodeWordCount() << " - IP: " << machine.GetIP() << std::endl;
	}
    machRet = machine.GetResult( result );
    MLUCTEST( machRet.Ok(), "Get Result 8" );
    if ( MLUCLASTTESTFAILURE() )
	{
	std::cerr << "Error: " << machRet.Description() << std::endl;
	}
    MLUCTEST( result==1, "Result Check 8" );
    if ( MLUCLASTTESTFAILURE() )
	{
	std::cerr << "Result: " << result << std::endl;
	}


    asmRet = assembler.Assemble( "PUSHCONST 1 ; PUSHCONST 2 ; PUSHCONST 3 ; PUSHCONST 4 ; PUSHCONST 5 ; PUSHCONST 2 ; SWAPNR ; PUSHCONST 4 ; SWAPNR" );
    MLUCTEST( asmRet.Ok(), "Assembly 9" );
    if ( MLUCLASTTESTFAILURE() )
	{
	std::cerr << "Error: " << asmRet.Description() << " - line nr.: " << asmRet.LineNr() << std::endl;
	}
    machRet = machine.SetCode( assembler );
    MLUCTEST( machRet.Ok(), "Set Code 9" );
    if ( MLUCLASTTESTFAILURE() )
	{
	std::cerr << "Error: " << machRet.Description() << " - Code word count: " << machine.GetCodeWordCount() << " - IP: " << machine.GetIP() << std::endl;
	}
    machRet = machine.Run();
    MLUCTEST( machRet.Ok(), "Run Code 9" );
    if ( MLUCLASTTESTFAILURE() )
	{
	std::cerr << "Error: " << machRet.Description() << " - Code word count: " << machine.GetCodeWordCount() << " - IP: " << machine.GetIP() << std::endl;
	}
    machRet = machine.GetResult( result );
    MLUCTEST( machRet.Ok(), "Get Result 9" );
    if ( MLUCLASTTESTFAILURE() )
	{
	std::cerr << "Error: " << machRet.Description() << std::endl;
	}
    MLUCTEST( result==1, "Result Check 9" );
    if ( MLUCLASTTESTFAILURE() )
	{
	std::cerr << "Result: " << result << std::endl;
	}


    asmRet = assembler.Assemble( "PUSHCONST 3 ; PUSHCONST 2 ; PUSHCONST 9 ; PUSHCONST 2 ; MOVETOTOPNR ; SUB ; SWAP; DIV" );
    MLUCTEST( asmRet.Ok(), "Assembly 10" );
    if ( MLUCLASTTESTFAILURE() )
	{
	std::cerr << "Error: " << asmRet.Description() << " - line nr.: " << asmRet.LineNr() << std::endl;
	}
    machRet = machine.SetCode( assembler );
    MLUCTEST( machRet.Ok(), "Set Code 10" );
    if ( MLUCLASTTESTFAILURE() )
	{
	std::cerr << "Error: " << machRet.Description() << " - Code word count: " << machine.GetCodeWordCount() << " - IP: " << machine.GetIP() << std::endl;
	}
    machRet = machine.Run();
    MLUCTEST( machRet.Ok(), "Run Code 10" );
    if ( MLUCLASTTESTFAILURE() )
	{
	std::cerr << "Error: " << machRet.Description() << " - Code word count: " << machine.GetCodeWordCount() << " - IP: " << machine.GetIP() << std::endl;
	}
    machRet = machine.GetResult( result );
    MLUCTEST( machRet.Ok(), "Get Result 10" );
    if ( MLUCLASTTESTFAILURE() )
	{
	std::cerr << "Error: " << machRet.Description() << std::endl;
	}
    MLUCTEST( result==3, "Result Check 10" );
    if ( MLUCLASTTESTFAILURE() )
	{
	std::cerr << "Result: " << result << std::endl;
	}


    asmRet = assembler.Assemble( "PUSHCONST 5 ; loop:PUSHCONST 1 ; SUB ; DUP ; DROP ; DUP ; JUMPNOTNULL loop ; " );
    MLUCTEST( asmRet.Ok(), "Assembly 11" );
    if ( MLUCLASTTESTFAILURE() )
	{
	std::cerr << "Error: " << asmRet.Description() << " - line nr.: " << asmRet.LineNr() << std::endl;
	}
    else
	{
	machRet = machine.SetCode( assembler );
	MLUCTEST( machRet.Ok(), "Set Code 11" );
	if ( MLUCLASTTESTFAILURE() )
	    {
	    std::cerr << "Error: " << machRet.Description() << " - Code word count: " << machine.GetCodeWordCount() << " - IP: " << machine.GetIP() << std::endl;
	    }
	else
	    {
	    machine.Reset();
	    while ( !machine.IsStopped() )
		{
		machine.Step();
		std::vector<uint64> stack;
		machine.GetStack( stack );
		unsigned long cnt=stack.size();
		std::cout << "------------------------------------------" << std::endl;
		for ( unsigned long nn=0; nn<cnt; nn++ )
		    std::cout << nn << " : " << stack[nn] << std::endl;
		}
	    std::cout << "------------------------------------------" << std::endl;
	    }
	}
    
    MLUCTESTEND();
    }

    



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
