/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCLog.hpp"
#include "MLUCLogServer.hpp"
#include "MLUCBufferingLogServer.hpp"


int main( int, char** )
    {
    gLogLevel = MLUCLog::kAll;
    MLUCBufferingLogServer bufferLog( 4, 2000 );
    MLUCStdoutLogServer sOut;
    bufferLog.AddServer( sOut );
    gLog.AddServer( bufferLog );
    bufferLog.StartThread();

    LOG( MLUCLog::kDebug, "BufferLogTest", "Msg 1" )
	<< "Log test message 1." << ENDLOG;
    LOG( MLUCLog::kInformational, "BufferLogTest", "Msg 2" )
	<< "Log test message 2." << ENDLOG;
    LOG( MLUCLog::kWarning, "BufferLogTest", "Msg 3" )
	<< "Log test message 3." << ENDLOG;
    LOG( MLUCLog::kError, "BufferLogTest", "Msg 4" )
	<< "Log test message 4." << ENDLOG;
    LOG( MLUCLog::kFatal, "BufferLogTest", "Msg 5" )
	<< "Log test message 5." << ENDLOG;
    sleep( 5 );
    bufferLog.StopThread();
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
