/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCHistogram.hpp"
#include <stdio.h>
#include <errno.h>
#include <string.h>

int main( int, char** )
    {
    const uint64 lower=2000;
    const uint64 upper=4000;
    const uint64 width=10;
    signed long long i, j, n, m, b;
    MLUCHistogram histo( lower, upper, width );
    
    n = (upper-lower)/width;
    for ( i = -1; i <= n; i++ )
	{
	m = (upper-lower) - i*width;
	b = lower+i*width;
	for ( j = 0; j < m; j++ )
	    histo.Add( b );
	}

    int ret;
    const char* histofile = "TestHistogram.histo";
    ret = histo.Write( histofile );
    if ( ret )
	printf( "Error writing histogram to file %s: %s (%d).\n", histofile, strerror(ret), ret );
    return 0;
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
