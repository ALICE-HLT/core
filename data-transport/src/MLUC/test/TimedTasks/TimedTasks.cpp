/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCTimedTask.hpp"
#include "MLUCLog.hpp"
#include "MLUCLogServer.hpp"
#include <string.h>
#include <stdlib.h>
#include <fstream>
#include <iostream>

class TestTimedTask: public MLUCTimedTask
    {
    public:

	TestTimedTask( uint32 loop )
		{
		fLoop = loop;
		}

	void SetLoop( uint32 loop )
		{
		fLoop = loop;
		}
	
	virtual void DoTask();
	virtual void TaskCompleted();
	virtual void TaskTimedOut( MLUCThread& taskThread );

    protected:

	uint32 fLoop;

    private:
    };


void TestTimedTask::DoTask()
    {
    LOG( MLUCLog::kInformational, "TestTimedTask::DoTask", "Task started" )
	<< "Task started with " << MLUCLog::kDec << fLoop << " loop iterations." << ENDLOG;
    uint32 i, n=0;
    for ( i = 0; i < fLoop; i++ )
	n += i;
    }

void TestTimedTask::TaskCompleted()
    {
    LOG( MLUCLog::kInformational, "TestTimedTask::DoTask", "Task finished" )
	<< "Task finished successfully with " << MLUCLog::kDec << fLoop << " loop iterations." << ENDLOG;
    }

void TestTimedTask::TaskTimedOut( MLUCThread& taskThread )
    {
    LOG( MLUCLog::kInformational, "TestTimedTask::DoTask", "Task timed out" )
	<< "Task timed out while trying to do " << MLUCLog::kDec << fLoop << " loop iterations. Aborting task..." << ENDLOG;
    taskThread.Abort();
    }

int main( int argc, char** argv )
    {
    MLUCFilteredStdoutLogServer sOut;
    gLogLevel = MLUCLog::kAll;
    gLog.AddServer( sOut );
    ofstream of;
    of.open( "TimedTasks.log" );
    MLUCStreamLogServer streamOut( of );
    gLog.AddServer( streamOut );


    if ( argc != 3 )
	{
	LOG( MLUCLog::kError, "TimedTasks", "Usage" )
	    << "Usage: " << argv[0] << " loop-counter timeout_in_ms" << ENDLOG;
	return -1;
	}
    uint32 loop, timeout;
    char* err;
    loop = strtoul( argv[1], &err, 0 );
    if ( *err )
	{
	LOG( MLUCLog::kError, "TimedTasks", "Usage" )
	    << "Usage: " << argv[0] << " loop-counter timeout_in_ms - Error in loop counter argument" << ENDLOG;
	return -1;
	}
    timeout = strtoul( argv[2], &err, 0 );
    if ( *err )
	{
	LOG( MLUCLog::kError, "TimedTasks", "Usage" )
	    << "Usage: " << argv[0] << " loop-counter timeout_in_ms - Error in timeout argument" << ENDLOG;
	return -1;
	}

    TestTimedTask task( loop );
    MLUCTimer timer;
    MLUCTimedTaskHandler taskHandler( &timer );
    timer.Start();
    bool finished;
    if ( !taskHandler.RunTask( task, timeout, finished ) )
	{
	LOG( MLUCLog::kError, "TimedTasks", "Task start" )
	    << "Unable to start task..." << ENDLOG;
	return -1;
	}
    else
	{
	LOG( MLUCLog::kInformational, "TimedTasks", "Task start" )
	    << "Task " << (finished ? "finished successfully" : "timed out") << ENDLOG;
	}
    return 0;
    }















/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
