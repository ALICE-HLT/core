/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCLog.hpp"
#include "MLUCLogServer.hpp"
#include "MLUCHistogram.hpp"
#include <sys/time.h>

// All time units in microsec. (us.)

double CalcTimeDiff_us( struct timeval* t1, struct timeval* t2 )
    {
    double tmp;
    tmp = t2->tv_sec-t1->tv_sec;
    tmp *= 1000000;
    tmp += t2->tv_usec-t1->tv_usec;
    return tmp;
    }


int main( int argc, char** argv )
    {
    int i;
    char* cpErr;
    const char* errorStr = NULL;

    MLUCFilteredStdoutLogServer sOut;
    gLogLevel = MLUCLog::kAll;
    gLog.AddServer( sOut );

    char *name = NULL;

    unsigned long count = 0x100000;

    const char* usage1 = "Usage: ";
    const char* usage2 = " (-V <verbosity>) -name <name> (-count <max-event-nr>)";
    
    i = 1;
    while ( (i < argc) && !errorStr )
	{
	if ( !strcmp( argv[i], "-name" ) )
	    {
	    if ( argc <= i+1 )
		{
		errorStr = "Missing name parameter";
		break;
		}
	    name = argv[i+1];
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-V" ) )
	    {
	    if ( argc <= i +1 )
		{
		errorStr = "Missing verbosity level specifier.";
		break;
		}
	    uint32 tmpll = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorStr = "Error converting verbosity level specifier..";
		break;
		}
	    gLogLevel = tmpll;
	    i += 2;
	    continue;
	    }
	if ( !strcmp( argv[i], "-count" ) )
	    {
	    if ( argc <= i +1 )
		{
		errorStr = "Missing count specifier.";
		break;
		}
	    count = strtoul( argv[i+1], &cpErr, 0 );
	    if ( *cpErr )
		{
		errorStr = "Error converting count specifier..";
		break;
		}
	    i += 2;
	    continue;
	    }
	errorStr = "Unknown parameter";
	}

    if ( !errorStr )
	{
	if ( !name )
	    errorStr = "Must specify -name <name> argument";
	}
    if ( errorStr )
	{
	LOG( MLUCLog::kError, "gettimeofdayTest", "Command line arguments" )
	    << usage1 << argv[0] << usage2 << ENDLOG;
	LOG( MLUCLog::kError, "gettimeofdayTest", "Command line arguments" )
	    << errorStr << ENDLOG;
	return -1;
	}
    
    MLUCFileLogServer fileOut( name, ".log", 3000000 );
    gLog.AddServer( fileOut );
    

    unsigned long n;
    struct timeval start, end;
    double tdiff;

    MLUCHistogram histo1( 0, 1000000, 10 );
    MLUCHistogram histo2( 0, 1000000, 1 );

    LOG( MLUCLog::kInformational, "gettimeofdayTest", "Status" )
	<< "Doing " << MLUCLog::kDec << count << " gettimeofday tests." << ENDLOG;

    for ( n = 0; n < count; n++ )
	{
	gettimeofday( &start, NULL );
	gettimeofday( &end, NULL );
	tdiff = CalcTimeDiff_us( &start, &end );
	histo1.Add( tdiff );
	histo2.Add( tdiff );
	}

    MLUCString histoname;

    histoname = name;
    histoname += "-1.histo";
    histo1.Write( histoname.c_str() );

    histoname = name;
    histoname += "-2.histo";
    histo2.Write( histoname.c_str() );

    return 0;
    }
    



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
