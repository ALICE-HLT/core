#ifndef _MLUCLIST_HPP_
#define _MLUCLIST_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: MLUCList.hpp 884 2006-02-19 15:28:46Z timm $ 
**
***************************************************************************
*/

#include "MLUCTypes.h"
#include "MLUCObjectCache.hpp"
#include <list>

#if __GNUC__>=3
using namespace std;
#endif

template<class TElementType>
class MLUCList
    {
    protected:
	struct TElementContainer
	    {
		TElementType fContent;
		TElementContainer* fPrevious;
		TElementContainer* fNext;
		void ResetCachedObject()
			{
			//fNext = fPrevious = 0;
			}
	    };
    public:

	class TIterator
	    {
	    public:
		TIterator()
			{
			fList = NULL;
			fElement = NULL;
			}
		TIterator( const TIterator& iter )
			{
			fList = iter.fList;
			fElement = iter.fElement;
			}
		~TIterator()
			{
			}

		TIterator& operator= ( const TIterator& iter )
			{
			fList = iter.fList;
			fElement = iter.fElement;
			return *this;
			}

		operator bool() const
			{
			if ( !fList )
			    return false;
			if ( !fElement )
			    return false;
			if ( fElement->fPrevious == &(fList->fStart) )
			    return false;
			return true;
			}
		TIterator& operator ++()
			{
			if ( fList && fElement )
			    fElement = fElement->fNext;
			return *this;
			}
		TIterator& operator --()
			{
			if ( fList && fElement )
			    fElement = fElement->fPrevious;
			return *this;
			}
		TElementType* operator->()
			{
			if ( !fList )
			    return NULL;
			if ( !fElement )
			    return NULL;
			return &(fElement->fContent);
			}
		TElementType& operator*()
			{
			if ( !fList )
			    return *(TElementType*)NULL;
			if ( !fElement )
			    return *(TElementType*)NULL;
			return fElement->fContent;
			}
		TIterator operator+( int by )
			{
			if ( by<0 )
			    return this->operator-(-by);
			TIterator newIter( *this );
			for ( int i=0; i<by; i++ )
			    newIter.fElement = newIter.fElement->fNext;
			return newIter;
			}
		TIterator operator-( int by )
			{
			if ( by<0 )
			    return this->operator+(-by);
			TIterator newIter( *this );
			for ( int i=0; i<by; i++ )
			    newIter.fElement = newIter.fElement->fPrevious;
			return newIter;
			}
		bool operator==( TIterator const& comp )
			{
			return comp.fList==fList && comp.fElement==fElement;
			}
		bool operator!=( TIterator const& comp )
			{
			return comp.fList!=fList || comp.fElement!=fElement;
			}
	    protected:
		TIterator( MLUCList<TElementType>* list, TElementContainer *element ):
		    fList( list ),
		    fElement( element )
			{
			}

		    friend class MLUCList<TElementType>;

		MLUCList<TElementType>* fList;
		TElementContainer *fElement;

	    };

	MLUCList( unsigned bufferSizeExp2 = 1, bool grow = true ):
	    fGrow( grow ), fElementCache( bufferSizeExp2, grow )
		{
		Init( bufferSizeExp2 );
		}
	~MLUCList()
		{
		while ( !IsEmpty() )
		    Erase( Begin() );
		};

	TIterator Insert( TIterator const& iterBefore, TElementType element )
		{
		TElementContainer* cont = fElementCache.Get();
		if ( !cont )
		    return End();
		cont->fContent = element;
		cont->fPrevious = iterBefore.fElement->fPrevious;
		cont->fNext = iterBefore.fElement;
		iterBefore.fElement->fPrevious = cont;
		cont->fPrevious->fNext = cont;
		fDataCnt++;
		return TIterator( this, cont );
		}
	void Erase( TIterator const& remove )
		{
		if ( remove.fElement==&fEnd || remove.fElement==&fStart )
		    return;
		remove.fElement->fPrevious->fNext = remove.fElement->fNext;
		remove.fElement->fNext->fPrevious = remove.fElement->fPrevious;
		fElementCache.Release( remove.fElement );
		fDataCnt--;
		}

	TIterator Begin()
		{
		return TIterator( this, fStart.fNext );
		}
	TIterator End()
		{
		return TIterator( this, &fEnd );
		}

	bool IsEmpty() const
		{
		return fStart.fNext==&fEnd;
		}

	unsigned long GetCnt() const
		{
		return fDataCnt;
		}


    protected:

	void Init( int bufferSizeExp2 );

	TElementContainer fStart;
	TElementContainer fEnd;

	unsigned long fDataCnt;
	bool fGrow;
	MLUCObjectCache<TElementContainer> fElementCache;


    private:
    };


template<class TElementType>
inline void MLUCList<TElementType>::Init( int /*bufferSizeExp2*/ )
    {
    fDataCnt = 0;
    fStart.fNext = &fEnd;
    fStart.fPrevious = &fStart;
    fEnd.fNext = &fEnd;
    fEnd.fPrevious = &fStart;
    }




/*
***************************************************************************
**
** $Author: timm $ - Initial Version by Timm Morten Steinbeck
**
** $Id: MLUCList.hpp 884 2006-02-19 15:28:46Z timm $ 
**
***************************************************************************
*/

#endif // _MLUCLIST_HPP_
