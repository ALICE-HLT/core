#include "MLUCLogServer.hpp"
#include "MLUCString.hpp"
#include "MLUCDynamicLibrary.hpp"
#include "MLUCLog.hpp"

class MLUCDynamicLibraryLogServer: public MLUCLogServer
    {
    public:
        MLUCDynamicLibraryLogServer(const char* pLibName, const char* pFacilityName);
        ~MLUCDynamicLibraryLogServer();
        
        virtual void LogLine( const char* origin, const char* keyword, const char* file, int linenr, 
                              const char* compile_date, const char* compile_time, MLUCLog::TLogLevel logLvl, 
                              const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
			      unsigned long runNumber, 
#endif
			      uint32 ldate, 
			      uint32 ltime_s, uint32 ltime_us, uint32 msgNr, const char* line );
        
        virtual void LogLine( const char* origin, const char* keyword, const char* file, int linenr, 
                              const char* compile_date, const char* compile_time, MLUCLog::TLogLevel logLvl, 
                              const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
			      unsigned long runNumber, 
#endif
			      uint32 ldate, 
			      uint32 ltime_s, uint32 ltime_us, uint32 msgNr, uint32 subMsgNr, const char* line );


    protected:
	
        MLUCString fLibName;
	MLUCString fFacilityName;
        MLUCDynamicLibrary fDynLibrary;
        MLUCLogServer* fLogServer;
        void* fHandle;
        
        typedef MLUCLogServer* (*CreateLogServerFunc)(const char* pFacilityName);
	
    };
