#ifndef _MLUCSTRING_HPP_
#define _MLUCSTRING_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCTypes.h"
#include <vector>
#include <sys/time.h>
#include <cstdio>

class MLUCString
    {
    public:


	typedef unsigned long TSizeType;
	static const unsigned long kNoPos = ~0UL;

	MLUCString();
	MLUCString( const char* content );
	MLUCString( const MLUCString& content );
	MLUCString( const char& content );

	~MLUCString();

	const char* c_str() const
		{
		return (fContents ? fContents : "");
		}

	TSizeType Size() const
		{
		return fLength;
		}

	TSizeType Length() const
		{
		return fLength;
		}

	MLUCString& operator += ( const MLUCString& str )
		{
		return Append( str );
		}
	MLUCString& operator += ( const char* str )
		{
		return Append( str );
		}
	MLUCString& operator += ( const char& c )
		{
		return Append( c );
		}
	MLUCString& Append( const MLUCString& str );
	MLUCString& Append( const char* str );
	MLUCString& Append( const char& c );


	MLUCString& operator = ( const MLUCString& str )
		{
		return Set( str );
		}
	MLUCString& operator = ( const char* str )
		{
		return Set( str );
		}
	MLUCString& operator = ( const char& c )
		{
		return Set( c );
		}
	MLUCString& Set( const MLUCString& str );
	MLUCString& Set( const char* str );
	MLUCString& Set( const char& c );

	
	char& operator[] ( TSizeType ndx )
		{
		if ( ndx < fLength )
		    return fContents[ ndx ];
		else
		    return fDefault;
		}

	const char& operator[] ( TSizeType ndx ) const
		{
		if ( ndx < fLength )
		    return fContents[ ndx ];
		else
		    return fDefault;
		}

	bool operator== ( const MLUCString& str ) const;
	bool operator== ( const char* str ) const;
	bool operator== ( const char& c ) const;

	bool operator!= ( const MLUCString& str ) const;
	bool operator!= ( const char* str ) const;
	bool operator!= ( const char& c ) const;

	bool IsEmpty() const;
	bool IsWhiteSpace() const;

	void ToLower();
	void ToUpper();

	TSizeType Find( const char* substr, TSizeType startPos=0 ) const;
	TSizeType Find( const char& c, TSizeType startPos=0 ) const;
	TSizeType Find( const MLUCString& substr, TSizeType startPos=0 ) const;
	MLUCString Substr( TSizeType start, TSizeType len ) const;
	MLUCString Substr( TSizeType start ) const;

	void Erase( TSizeType start, TSizeType len );

	void Split( std::vector<MLUCString>& tokens, char separator = ' ' ) const;
	void Split( std::vector<MLUCString>& tokens, const char* separators ) const;


	
	void SetSizeIncrement( unsigned sizeIncrement )
		{
		fSizeIncrement = sizeIncrement;
		}

	static MLUCString TimeStamp2String( struct timeval timestamp )
		{
		return TimeStamp2String( timestamp.tv_sec, timestamp.tv_usec );
		}
	static MLUCString TimeStamp2String( unsigned long secs, unsigned long microsecs = ~0UL );



    protected:

	char* fContents;
	TSizeType fLength;
	TSizeType fAllocLength;
	char fDefault;

	unsigned fSizeIncrement;

    private:
    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _MLUCSTRING_HPP_
