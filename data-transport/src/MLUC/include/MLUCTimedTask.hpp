/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _MLUCTIMEDTASK_HPP_
#define _MLUCTIMEDTASK_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCTypes.h"
#include "MLUCConditionSem.hpp"
#include "MLUCThread.hpp"
#include "MLUCTimer.hpp"
#include <unistd.h>
#include <pthread.h>



class MLUCTimedTask
    {
    public:

	virtual ~MLUCTimedTask() {};

	virtual void DoTask() = 0;
	virtual void TaskCompleted() = 0;
	virtual void TaskTimedOut( MLUCThread& taskThread ) = 0;

    };

class MLUCTimedTaskHandler
    {
    public:

	MLUCTimedTaskHandler();
	MLUCTimedTaskHandler( MLUCTimer* timer );
	virtual ~MLUCTimedTaskHandler();

	void SetTimer( MLUCTimer* timer )
		{
		fTimer = timer;
		}

	virtual bool RunTask( MLUCTimedTask* task, uint32& timeout_ms, bool& taskCompleted, bool waitForCompletion = true );
	virtual bool RunTask( MLUCTimedTask& task, uint32& timeout_ms, bool& taskCompleted, bool waitForCompletion = true );
	virtual bool HasTaskFinished();
	virtual bool HasTaskFinished( bool& taskCompleted );

    protected:

	MLUCTimedTask* fTask;


	virtual void RunTask();
	virtual void RunTimeout();

	class MLUCTimedTaskHandlerTaskThread: public MLUCThread
	    {
	    public:
		MLUCTimedTaskHandlerTaskThread( MLUCTimedTaskHandler& task ):
		    fTaskHandler( task )
			{
			}

		virtual void Run();

	    protected:

		MLUCTimedTaskHandler& fTaskHandler;

	    };

	class MLUCTimedTaskHandlerTimeoutThread: public MLUCThread
	    {
	    public:
		MLUCTimedTaskHandlerTimeoutThread( MLUCTimedTaskHandler& task ):
		    fTaskHandler( task )
			{
			}

		virtual void Run();

	    protected:

		MLUCTimedTaskHandler& fTaskHandler;

	    };

	MLUCTimedTaskHandlerTaskThread fTaskThread;
	bool fTaskStarted;
	bool fTaskEnded;
	bool fTaskTimedOut;

	MLUCTimer* fTimer;
	MLUCTimedTaskHandlerTimeoutThread fTimerThread;
	MLUCTimerSignal fTimeoutSignal;

	bool fSignalTaskEnd;
	MLUCConditionSem<uint8> fTaskEndedSignal;
	MLUCConditionSem<uint8> fTaskStartSignal;

	pthread_mutex_t fDataMutex;
	pthread_mutex_t fTaskEndMutex;

	    friend class MLUCTimedTaskHandlerTaskThread;
	    friend class MLUCTimedTaskHandlerTimeoutThread;
    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _MLUCTIMEDTASK_HPP_
