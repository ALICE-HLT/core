/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _MLUCVALUEMONITORDISK_HPP_
#define _MLUCVALUEMONITORDISK_HPP_
/*
***************************************************************************
**
** $Author$ - Initial Version by Arne Wiebalck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCValueMonitor.hpp"
#include "MLUCLog.hpp"

#include <fstream>
#include <cstdlib>
#include <errno.h>


/**
 * @class MLUCValueMonitorDisk
 * @brief A class for disk I/O monitoring.
 * @author $Author$ - Initial Version by Arne Wiebalck 
 * @version $Id$
 */
class MLUCValueMonitorDisk : public MLUCValueMonitor {

public:
	/**
	 * @brief Constructor to enable output to file and stdout.
	 * Using this constructor enables output to a file as well as to the 
	 * standard output. The filename is passed to the constructor as well as a
	 * string containing a description of the measurement that will be written 
	 * to the beginning of the file. 
	 * For output to stdout prefixes and suffixes are provided that will be output
	 * respectively before and after the monitored value. Another pre-/suffix pair
	 * is provided for the averaged value. For the averaged value the number of the 
	 * last values over which the average is to be taken can be determined as well. 
	 *
	 * @param filename A string holding the name of the file to which the values monitored are output. 
	 * @param fileDescrHeader A string holding a description of the monitoring taking place. This string
	 * will be written to the start of the output file. 
	 * @param output_prefix A string holding the textual prefix that is to be output before
	 * the measured value.
	 * @param output_suffix A string holding the textual suffix that is to be output after
	 * the measured value.
	 * @param avg_output_prefix A string holding the textual prefix that is to be output before
	 * the average of the measured value.
	 * @param avg_output_suffix A string holding the textual suffix that is to be output after
	 * the average of the measured value.
	 * @param average_items The number of the most recent values over which the average is to be calculated.
	 * @param throughput_type A short holding the type of throughput to be measured. Currently this can be 
	 * BytesRead (0, default), BytesWritten (1), PacketsRead (2) and PacketsWritten (3). 
	 * @param net_number The ID of the disk to be monitored. If no ID is given, the first one will be taken..
	 */
	MLUCValueMonitorDisk( const char* filename, 
			     const char* fileDescrHeader, 
			     const char* output_prefix, 
			     const char* output_suffix, 
			     const char* avg_output_prefix, 
			     const char* avg_output_suffix, 
			     unsigned int average_items,
			     short throughput_type = 0, 
			     short disk_id = 0 );
	/**
	 * @brief Constructor to enable only output to stdout.
	 * Using this constructor enables output to the 
	 * standard output. 
	 * Prefixes and suffixes are provided that will be output
	 * respectively before and after the monitored value. Another pre-/suffix pair
	 * is provided for the averaged value. For the averaged value the number of the 
	 * last values over which the average is to be taken can be determined as well. 
	 *
	 * @param output_prefix A string holding the textual prefix that is to be output before
	 * the measured value.
	 * @param output_suffix A string holding the textual suffix that is to be output after
	 * the measured value.
	 * @param avg_output_prefix A string holding the textual prefix that is to be output before
	 * the average of the measured value.
	 * @param avg_output_suffix A string holding the textual suffix that is to be output after
	 * the average of the measured value.
	 * @param average_items The number of the most recent values over which the average is to be calculated.
	 * @param throughput_type A short holding the type of throughput to be measured. Currently this can be 
	 * BytesRead (0, default), BytesWritten (1), PacketsRead (2) and PacketsWritten (3). 
	 * @param net_number The ID of the disk to be monitored. If no ID is given, the first one will be taken..
	*/
	MLUCValueMonitorDisk( const char* output_prefix, 
			     const char* output_suffix, 
			     const char* avg_output_prefix, 
			     const char* avg_output_suffix, 
			     unsigned int average_items,
			     short throughput_type = 0, 
			     short disk_id = 0 );
	
	/**
	 * @brief Constructor to enable only output to a file.
	 * Using this constructor enables output to a file. 
	 * The filename is passed to the constructor as well as a
	 * string containing a description of the measurement that will be written 
	 * to the beginning of the file. 
	 * For the averaged value the number of the last values over which the average 
	 * is to be taken can be determined as well. 
	 *
	 * @param filename A string holding the name of the file to which the values monitored are output. 
	 * @param fileDescrHeader A string holding a description of the monitoring taking place. This string
	 * will be written to the start of the output file. 
	 * @param average_items The number of the most recent values over which the average is to be calculated.
	 * @param throughput_type A short holding the type of throughput to be measured. Currently this can be 
	 * BytesRead (0, default), BytesWritten (1), PacketsRead (2) and PacketsWritten (3). 
	 * @param net_number The ID of the disk to be monitored. If no ID is given, the first one will be taken..
	 */
	MLUCValueMonitorDisk( const char* filename, 
			     const char* fileDescrHeader, 
			     unsigned int average_items, 
			     short throughput_type = 0, 
			     short disk_id = 0 );

	/**
	 * @brief Constructorwith no output.
	 * For the averaged value the number of the last values over which the average 
	 * is to be taken can be determined as well. 
	 *
	 * @param average_items The number of the most recent values over which the average is to be calculated.
	 * @param throughput_type A short holding the type of throughput to be measured. Currently this can be 
	 * BytesRead (0, default), BytesWritten (1), PacketsRead (2) and PacketsWritten (3). 
	 * @param net_number The ID of the disk to be monitored. If no ID is given, the first one will be taken..
	 */
	MLUCValueMonitorDisk( unsigned int average_items, 
			     short throughput_type = 0, 
			     short disk_id = 0 );

	/**
	 * @brief Function to pick out the number of network devices in the system.
	 * @param None.
	 * @return A short holding a error return code corresponding to the C errno variable. (0 means success)
	 */
	int GetNumberOfDisks();

	/**
	 * @brief Implementation of the Interface function from MLUCValueMonitor class.  
	 * @param MeasuredValued Reference to the monitored value.
	 * @return An integer holding a error return code corresponding to the C errno variable. (0 means success)
	 */
	int GetValue( uint64& MeasuredValue );

protected:
	/**
	 * The number of network devices in the system.
	 */
	short fNumber_Of_Disks;

	/**
	 * The type of network throughput to be measured.
	 *  0: bytes read
	 *  1: blks written
	 *  2: packets read
	 *  3: blks written
	 */
	short fType_Of_Throughput;

	/**
	 * The ID of the network device to be monitored.
	 */
	short fDisk_ID;
	
	/**
	 * Flag for the first readout. In the constructor this flag is set to true, since 
	 * one needs at least two values to measure something. After the first readout
	 * the flag is set to false.
	 */
	bool fFirstReadOut;

	/**
	 * The values of the the last and the current readout are stored in these variables.
	 */
	unsigned long fOld_Sum; 
	unsigned long fNew_Sum;

	unsigned long fBytesReadOld, fBytesWrittenOld, fPacketsReadOld, fPacketsWrittenOld, fBytesReadNew, 
		fBytesWrittenNew, fPacketsReadNew, fPacketsWrittenNew;
};


#endif // _MLUCVALUEMONITORDISK_HPP_
