#ifndef _MLUCINDEXEDVECTOR_HPP_
#define _MLUCINDEXEDVECTOR_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#define DEBUG_INDEXED_VECTOR

#if 0
#define PROFILE_INDEXED_VECTOR
#endif


#include "MLUCTypes.h"
#include <vector>
#ifdef DEBUG_INDEXED_VECTOR
#include <stdio.h>
#include "MLUCString.hpp"
#endif
#include <cstring>
#if defined(PROFILE_INDEXED_VECTOR) or defined(DEBUG_INDEXED_VECTOR)
#include "MLUCLog.hpp"
#endif

#if __GNUC__>=3
using namespace std;
#endif

template<class TElementType,class TIndexType>
class MLUCIndexedVectorBaseSearcher;

template<class TElementType, class TIndexType>
class MLUCIndexedVector
    {
    public:

	class TIterator
	    {
	    public:
		TIterator()
			{
			fVector = NULL;
			fItemNdx = 0;
			fCnt = 0;
			}
		TIterator( const TIterator& iter )
			{
			fVector = iter.fVector;
			fItemNdx = iter.fItemNdx;
			fCnt = iter.fCnt;
			}
		~TIterator()
			{
			}

		TIterator& operator= ( const TIterator& iter )
			{
			fVector = iter.fVector;
			fItemNdx = iter.fItemNdx;
			fCnt = iter.fCnt;
			return *this;
			}

		operator bool() const
			{
			if ( !fVector )
			    return false;
			if ( !fVector->fHashes )
			    return false;
			if ( !fVector->fDataSize && fItemNdx >= fVector->fData.size() )
			    return false;
			if ( fVector->fDataSize && fCnt >= fVector->fDataCnt+fVector->fInvalidDataCnt )
			    return false;
			return true;
			}
		TIterator& operator ++()
			{
			if ( fVector )
			    {
			    if ( !fVector->fDataSize )
				{
				if ( fItemNdx < fVector->fData.size() )
				    fItemNdx++;
				}
			    else
				{
				do
				    {
				    fItemNdx = (fItemNdx+1) & fVector->fDataMask;
				    fCnt++;
				    }
				while ( fCnt<fVector->fDataCnt+fVector->fInvalidDataCnt && !(*(fVector->fUsedStart+fItemNdx)) );
				}
			    }
			return *this;
			}
		TElementType* operator->()
			{
			if ( !fVector )
			    return NULL;
			if ( !fVector->fDataSize && fItemNdx >= fVector->fData.size() )
			    return NULL;
			if ( fVector->fDataSize && fCnt >= fVector->fDataCnt+fVector->fInvalidDataCnt )
			    return NULL;
			return fVector->fDataStart+fItemNdx;
			}
		TElementType& operator*()
			{
			if ( !fVector )
			    return *(TElementType*)NULL;
			if ( !fVector->fDataSize && fItemNdx >= fVector->fData.size() )
			    return *(TElementType*)NULL;
			if ( fVector->fDataSize && fCnt >= fVector->fDataCnt+fVector->fInvalidDataCnt )
			    return *(TElementType*)NULL;
			return *(fVector->fDataStart+fItemNdx);
			}
		unsigned long GetNdx() const
			{
			return fItemNdx;
			}
	    protected:
		TIterator( MLUCIndexedVector<TElementType,TIndexType>* vector, unsigned long itemNdx ):
		    fVector( vector ),
		    fItemNdx( itemNdx )
			{
			fCnt = 0;
			}

		    friend class MLUCIndexedVector<TElementType,TIndexType>;

		MLUCIndexedVector<TElementType,TIndexType>* fVector;
		unsigned long fItemNdx;
		unsigned long fCnt;

	    };

	typedef void (*TIterationFunc)( TElementType& el, void* arg );
	typedef void (*TConstIterationFunc)( const TElementType& el, void* arg );
	//typedef int (*TCompareFunc)( const TElementType& el1, const TElementType& el2 );

	MLUCIndexedVector( unsigned indexHashBits, int bufferSizeExp2 = -1, bool grow = true ):
	    fDefaultElementSet( false ), fGrow( grow )
#ifdef PROFILE_INDEXED_VECTOR
	    ,
	    fFindCallCount(0),
	    fFindIterationCount(0),
	    fFindReverseCallCount(0),
	    fFindReverseIterationCount(0),
	    fRemoveCallCount(0),
	    fRemoveIterationCount(0)
#endif
		{
		Init( indexHashBits, bufferSizeExp2 );
		}
	MLUCIndexedVector( unsigned indexHashBits, int bufferSizeExp2, TElementType defaultElement, bool grow = true ):
	    fDefaultElement( defaultElement ), fDefaultElementSet( true ), fGrow( grow )
#ifdef PROFILE_INDEXED_VECTOR
	    ,
	    fFindCallCount(0),
	    fFindIterationCount(0),
	    fFindReverseCallCount(0),
	    fFindReverseIterationCount(0),
	    fRemoveCallCount(0),
	    fRemoveIterationCount(0)
#endif
		{
		Init( indexHashBits, bufferSizeExp2 );
		}
	~MLUCIndexedVector()
		{
		if ( fHashes )
		    delete [] fHashes;
		fHashes = NULL;
		
#ifdef PROFILE_INDEXED_VECTOR
		LOG( MLUCLog::kImportant, "MLUCIndexedVector", "Profile Data" )
		    << "Profile data for MLUCIndexedVector 0x" << MLUCLog::kHex << (unsigned long long)this
		    << ": " << MLUCLog::kDec 
		    << "Find call count: " << fFindCallCount
		    << " - "
		    << "Find iteration count: " << fFindIterationCount
		    << " - "
		    << "FindReverse call count: " << fFindReverseCallCount
		    << " - "
		    << "FindReverse iteration count: " << fFindReverseIterationCount
		    << " - "
		    << "Remove call count: " << fRemoveCallCount
		    << " - "
		    << "Remove iteration count: " << fRemoveIterationCount
		    << ENDLOG;
#endif
		}

	bool Add( const TElementType& newElement, TIndexType index )
		{
		unsigned long empty;
		return Add( newElement, index, empty );
		}
	bool Add( const TElementType& newElement, TIndexType index, unsigned long& newElementNdx )
		{
		if ( !fDataSize )
		    {
#if __GNUC__>=3
		    typename vector<TElementType>::iterator p;
#else
		    vector<TElementType>::iterator p;
#endif
		    p = fData.insert( fData.end(), newElement );
		    fIndices.insert( fIndices.end(), index );
		    newElementNdx = p-fData.begin();
		    }
		else
		    {
		    if ( fInvalidDataCnt+fDataCnt==fDataSize && fInvalidDataCnt>0 )
			Compact( NULL, true );
		    else if ( fDataCnt==fDataSize )
			{
			if ( fGrow )
			    Resize( true );
			else
			    return false;
			}
		    newElementNdx = fNextFree;
		    *(fDataStart+fNextFree) = newElement;
		    *(fUsedStart+fNextFree) = true;
		    *(fIndicesStart+fNextFree) = index;
		    fNextFree = (fNextFree+1) & fDataMask;
		    }
		fDataCnt++;
		unsigned long hashIndex = (index/fPreDivisor) & fHashMask;
		fHashes[ hashIndex ].Add( index, newElementNdx );
		return true;
		}

	//bool Insert( const TElementType& newElement, TCompareFunc compFunc );
	//bool Insert( const TElementType& newElement, TCompareFunc compFunc, unsigned long& newElementNdx );

	bool Remove( unsigned long elementNdx )
		{
		TIndexType hashIndex;
		bool hashOk=false;
		if ( !fDataSize )
		    {
		    if ( elementNdx>=fData.size() )
			return false;
		    hashOk = true;
		    hashIndex = fIndices[elementNdx];
		    fData.erase( fData.begin()+elementNdx );
		    fIndices.erase( fIndices.begin()+elementNdx );
		    fDataCnt--;
		    }
		else
		    {
		    if ( elementNdx>=fDataSize || fDataCnt<=0 )
			return false;
		    hashIndex = *(fIndicesStart+elementNdx);
		    hashOk = true;
		    //     if ( !(fDataStart+elementNdx)->fValid )
		    // 	return true;
		    *(fUsedStart+elementNdx) = false;
		    if ( fDefaultElementSet )
			*(fDataStart+elementNdx) = fDefaultElement;
		    fDataCnt--;
		    if ( elementNdx==fFirstUsed )
			{
			fFirstUsed = (fFirstUsed+1) & fDataMask;
			while ( !(*(fUsedStart+fFirstUsed)) && fFirstUsed!=fNextFree )
			    {
			    fFirstUsed = (fFirstUsed+1) & fDataMask;
			    fInvalidDataCnt--;
			    }
			}
		    else if ( elementNdx == ((fNextFree-1) & fDataMask) )
			{
			fNextFree = elementNdx;
			while ( !(*(fUsedStart+((fNextFree-1) & fDataMask))) && fFirstUsed!=fNextFree )
			    {
			    fNextFree = (fNextFree-1) & fDataMask;
			    fInvalidDataCnt--;
			    }
			
			}
		    else
			fInvalidDataCnt++;
		    if ( (fDataCnt < (fDataSize>>2)) && (fDataSize>fOrigDataSize) )
			{
			Compact( NULL, false );
			Resize( false ); // We shrink
			hashOk = false; // Hash has been rebuilt completely after resize, data used as rebuilt source does not include removed element anymore, so no need to remove element from hash
			}
		    }
		if ( hashOk )
		    fHashes[(hashIndex / fPreDivisor) & fHashMask].Remove( hashIndex );
		return true;
		}
	bool RemoveFirst()
		{
		TIndexType hashIndex;
		bool hashOk=false;
		if ( !fDataSize )
		    {
		    if ( fData.size()<=0 )
			return false;
		    hashOk = true;
		    hashIndex = *(fIndices.begin());
		    fData.erase( fData.begin() );
		    fIndices.erase( fIndices.begin() );
		    fDataCnt--;
		    }
		else
		    {
		    if ( fDataCnt<=0 )
			return false;
		    hashOk = true;
		    hashIndex = *(fIndicesStart+fFirstUsed);
		    *(fUsedStart+fFirstUsed) = false;
		    if ( fDefaultElementSet )
			*(fDataStart+fFirstUsed) = fDefaultElement;
		    fDataCnt--;
		    fFirstUsed = (fFirstUsed+1) & fDataMask;
		    while ( !(*(fUsedStart+fFirstUsed)) && fFirstUsed!=fNextFree )
			{
			fFirstUsed = (fFirstUsed+1) & fDataMask;
			fInvalidDataCnt--;
			}
		    if ( (fDataCnt < (fDataSize>>2)) && (fDataSize>fOrigDataSize) )
			{
			Compact( NULL, false );
			Resize( false ); // We shrink
			hashOk = false; // Hash has been rebuilt completely after resize, data used as rebuilt source does not include removed element anymore, so no need to remove element from hash
			}
		    }
		if ( hashOk )
		    fHashes[(hashIndex / fPreDivisor) & fHashMask].Remove( hashIndex );
		return true;
		}

	void Clear()
		{
		fDataCnt = 0;
		fInvalidDataCnt = 0;
		fFirstUsed = fNextFree = 0;
		while ( fDataSize>fOrigDataSize )
		    Resize( false );
		RebuildHash();
		}

	bool FindElement( TIndexType index, unsigned long& elementNdx )
		{
		unsigned long hashIndex = (index / fPreDivisor) & fHashMask;
		return fHashes[hashIndex].Find( index, elementNdx );
		}
	bool FindElementReverse( TIndexType index, unsigned long& elementNdx )
		{
		unsigned long hashIndex = (index / fPreDivisor) & fHashMask;
		return fHashes[hashIndex].FindReverse( index, elementNdx );
		}

	void Iterate( TIterationFunc iterFunc, void* arg )
		{
		if ( !fDataSize )
		    {
		    unsigned long n = 0;
#if __GNUC__>=3
		    typename vector<TElementType>::iterator iter, end;
#else
		    vector<TElementType>::iterator iter, end;
#endif
		    iter = fData.begin();
		    end = fData.end();
		    while ( iter != end )
			{
			(*iterFunc)( *iter, arg );
			iter++;
			n++;
			}
		    }
		else
		    {
		    unsigned long i = 0;
		    unsigned long n = fFirstUsed;
		    while ( i++<fDataCnt+fInvalidDataCnt )
			{
			if ( *(fUsedStart+n) )
			     (*iterFunc)( *(fDataStart+n), arg );
			n = (n+1) & fDataMask;
			}
		    }
		}
	void Iterate( TConstIterationFunc iterFunc, void* arg ) const
		{
		if ( !fDataSize )
		    {
		    unsigned long n = 0;
#if __GNUC__>=3
		    typename vector<TElementType>::const_iterator iter, end;
#else
		    vector<TElementType>::const_iterator iter, end;
#endif
		    iter = fData.begin();
		    end = fData.end();
		    while ( iter != end )
			{
			(*iterFunc)( *iter, arg );
			iter++;
			n++;
			}
		    }
		else
		    {
		    unsigned long i = 0;
		    unsigned long n = fFirstUsed;
		    while ( i++<fDataCnt+fInvalidDataCnt )
			{
			if ( *(fUsedStart+n) )
			     (*iterFunc)( *(fDataStart+n), arg );
			n = (n+1) & fDataMask;
			}
		    }
		}
	
	bool IsValid( unsigned long elementNdx ) const 
		{
		if ( !fDataSize )
		    {
		    if ( elementNdx >= fData.size() )
			return false;
		    return true;
		    }
		else
		    {
		    if ( elementNdx >= fDataSize )
			return false;
		    if ( !(*(fUsedStart+elementNdx)) )
			 return false;
		    return true;
		    }
		}

	TElementType Get( unsigned long elementNdx )
		{
		if ( !fDataSize )
		    {
		    if ( elementNdx>=fData.size() )
			return fDefaultElement;
		    return *(fData.begin()+elementNdx);
		    }
		else
		    {
		    if ( fFirstUsed<fNextFree )
			{
			if ( elementNdx<fFirstUsed || elementNdx>=fNextFree )
			    return fDefaultElement;
			}
		    else
			{
			if ( elementNdx<fFirstUsed && elementNdx>=fNextFree )
			    return fDefaultElement;
			}
#ifndef QUICKNDIRTY
		    if ( !*(fUsedStart+elementNdx) )
			return fDefaultElement;
#endif
		    return *(fDataStart+elementNdx);
		    }
		}
	TElementType Get( const TIterator& iter )
		{
		if ( !fDataSize )
		    {
		    if ( iter.fItemNdx>=fData.size() )
			return fDefaultElement;
		    return *(fData.begin()+iter.fItemNdx);
		    }
		else
		    {
		    if ( fFirstUsed<fNextFree )
			{
			if ( iter.fItemNdx<fFirstUsed || iter.fItemNdx>=fNextFree )
			    return fDefaultElement;
			}
		    else
			{
			if ( iter.fItemNdx<fFirstUsed && iter.fItemNdx>=fNextFree )
			    return fDefaultElement;
			}
#ifndef QUICKNDIRTY
		    if ( !*(fUsedStart+iter.fItemNdx) )
			return fDefaultElement;
#endif
		    return *(fDataStart+iter.fItemNdx);
		    }
		}
	TElementType* GetPtr( unsigned long elementNdx )
		{
		if ( !fDataSize )
		    {
		    if ( elementNdx>=fData.size() )
			return NULL;
#if __GNUC__>=3
		    return (fData.begin().base()+elementNdx);
#else
		    return (fData.begin()+elementNdx);
#endif
		    }
		else
		    {
		    if ( fFirstUsed<fNextFree )
			{
			if ( elementNdx<fFirstUsed || elementNdx>=fNextFree )
			    return NULL;
			}
		    else
			{
			if ( elementNdx<fFirstUsed && elementNdx>=fNextFree )
			    return NULL;
			}
#ifndef QUICKNDIRTY
		    if ( !*(fUsedStart+elementNdx) )
			return NULL;
#endif
		    return (fDataStart+elementNdx);
		    }
		}
	TElementType* GetPtr( const TIterator& iter )
		{
		if ( !fDataSize )
		    {
		    if ( iter.fItemNdx>=fData.size() )
			return NULL;
#if __GNUC__>=3
		    return (fData.begin().base()+iter.fItemNdx);
#else
		    return (fData.begin()+iter.fItemNdx);
#endif
		    }
		else
		    {
		    if ( fFirstUsed<fNextFree )
			{
			if ( iter.fItemNdx<fFirstUsed || iter.fItemNdx>=fNextFree )
			    return NULL;
			}
		    else
			{
			if ( iter.fItemNdx<fFirstUsed && iter.fItemNdx>=fNextFree )
			    return NULL;
			}
#ifndef QUICKNDIRTY
		    if ( !*(fUsedStart+iter.fItemNdx) )
			return NULL;
#endif
		    return (fDataStart+iter.fItemNdx);
		    }
		}
	bool Get( unsigned long elementNdx, TElementType& element )
		{
		if ( !fDataSize )
		    {
		    if ( elementNdx>=fData.size() )
			return false;
		    element = *(fData.begin()+elementNdx);
		    return true;
		    }
		else
		    {
		    if ( fFirstUsed<fNextFree )
			{
			if ( elementNdx<fFirstUsed || elementNdx>=fNextFree )
			    return false;
			}
		    else
			{
			if ( elementNdx<fFirstUsed && elementNdx>=fNextFree )
			    return false;
			}
		    element = *(fDataStart+elementNdx);
		    return true;
		    }
		}
	bool Get( const TIterator& iter, TElementType& element )
		{
		if ( !fDataSize )
		    {
		    if ( iter.fItemNdx>=fData.size() )
			return false;
		    element = *(fData.begin()+iter.fItemNdx);
		    return true;
		    }
		else
		    {
		    if ( fFirstUsed<fNextFree )
			{
			if ( iter.fItemNdx<fFirstUsed || iter.fItemNdx>=fNextFree )
			    return false;
			}
		    else
			{
			if ( iter.fItemNdx<fFirstUsed && iter.fItemNdx>=fNextFree )
			    return false;
			}
		    element = *(fDataStart+iter.fItemNdx);
		    return true;
		    }
		}

	TElementType GetFirst()
		{
		if ( fDataCnt<=0 )
		    return fDefaultElement;
		if ( !fDataSize )
		    {
		    return *fData.begin();
		    }
		else
		    {
		    return *(fDataStart+fFirstUsed);
		    }
		}
	TElementType* GetFirstPtr()
		{
		if ( fDataCnt<=0 )
		    return NULL;
		if ( !fDataSize )
		    {
#if __GNUC__>=3
		    return fData.begin().base();
#else
		    return fData.begin();
#endif
		    }
		else
		    {
		    return fDataStart+fFirstUsed;
		    }
		}
	bool GetFirst( TElementType& element )
		{
		if ( fDataCnt<=0 )
		    return false;
		if ( !fDataSize )
		    {
		    element = *fData.begin();
		    }
		else
		    {
		    element = *(fDataStart+fFirstUsed);
		    }
		return true;
		}

	bool IsFixedSize() const
		{
		return fOrigDataSize!=0;
		}

// 	int GetSize()
	unsigned long GetSize() const
		{
		return fDataSize;
		}
	unsigned long GetOrigSize() const
		{
		return fOrigDataSize;
		}

	bool IsGrowable() const
		{
		return fGrow;
		}
	unsigned long GetStartNdx() const
		{
		if ( !fDataSize )
		    return 0;
		else
		    return fFirstUsed;
		}
	unsigned long GetEndNdx() const // Returns the index that is 1 beyond the last valid index
		{
		if ( !fDataSize )
		    return fData.size();
		else
		    return fNextFree;
		}
	unsigned long GetMask() const
		{
		if ( !fDataSize )
		    return ~(unsigned long)0;
		else
		    return fDataMask;
		}
	unsigned long GetCnt() const
		{
		return fDataCnt;
		}

	TIterator Begin()
		{
		if ( !fDataSize )
		    return TIterator( this, 0 );
		else
		    return TIterator( this, fFirstUsed );
		}

#ifdef DEBUG_INDEXED_VECTOR
	bool IsConsistent()
		{
		unsigned long cnt=0;
		for ( unsigned long h = 0; h < fHashCnt; h++ )
		    cnt += fHashes[h].fCnt;
		if ( cnt != fDataCnt )
		    {
		    LOG( MLUCLog::kError, "MLUCIndexedVector::IsConsistent", "Inconsistency (0)" )
			<< "Inconsistency (0): " << MLUCLog::kDec << cnt << " != " << fDataCnt << ENDLOG;
		    return false;
		    }
		for ( unsigned long h = 0; h < fHashCnt; h++ )
		    {
		    cnt=0;
		    for ( unsigned long n=0; n<fHashes[h].fSize; n++ )
			{
			if ( fHashes[h].fCnt>0 && fHashes[h].fHashData[fHashes[h].fStartNdx].fVectorIndex==~(unsigned long)0 )
			    {
			    LOG( MLUCLog::kError, "MLUCIndexedVector::IsConsistent", "Inconsistency (1)" )
				<< "Inconsistency (1): " << MLUCLog::kDec 
				<< "fHashes[" << h << "].fHashData[fHashes[ " << h << "].fStartNdx].fVectorIndex: " <<  fHashes[h].fHashData[fHashes[h].fStartNdx].fVectorIndex
				<< " - "
				<< "fHashes[" << h << "].fStartNdx: " << fHashes[h].fStartNdx
				<< ENDLOG;
			    return false;
			    }
			if ( fHashes[h].fHashData[n].fVectorIndex!=~(unsigned long)0 && !(*fUsedStart+fHashes[h].fHashData[n].fVectorIndex) )
			    {
			    LOG( MLUCLog::kError, "MLUCIndexedVector::IsConsistent", "Inconsistency (2)" )
				<< "Inconsistency (2): " << MLUCLog::kDec 
				<< "fHashes[" << h << ".fHashData[" << n << "].fVectorIndex: " << fHashes[h].fHashData[n].fVectorIndex
				<< " - "
				<< "(*fUsedStart+fHashes[" << h << "].fHashData[" << n << "].fVectorIndex): " << !(*fUsedStart+fHashes[h].fHashData[n].fVectorIndex)
				<< ENDLOG;
			    return false;
			    }
			if ( fHashes[h].fHashData[n].fVectorIndex != ~(unsigned long)0 )
			    cnt++;
			}
		    if ( cnt != fHashes[h].fCnt )
			{
			LOG( MLUCLog::kError, "MLUCIndexedVector::IsConsistent", "Inconsistency (3)" )
			    << "Inconsistency (3): " << MLUCLog::kDec 
			    << "fHashes[" << h << "].fCnt: " << fHashes[h].fCnt
			    << " - "
			    << "cnt : " << cnt
			    << ENDLOG;
			return false;
			}
		    }
		return true;
		}
	void AsString( MLUCString& str )
		{
		str = "";
		char tmp[4096];
		sprintf( tmp, "Storage: size: %10lu / 0x%08lX - data cnt: %10lu / 0x%08lX - invalid data cnt: %10lu / 0x%08lX - mask: %10lu / 0x%08lX - first used: %10lu / 0x%08lX - next free: %10lu / 0x%08lX - hash cnt: %10lu / 0x%08lX - hash mask: %10lu / 0x%08lX\n", 
			(unsigned long)fDataSize, (unsigned long)fDataSize, (unsigned long)fDataCnt, (unsigned long)fDataCnt, (unsigned long)fInvalidDataCnt, (unsigned long)fInvalidDataCnt, (unsigned long)fDataMask, (unsigned long)fDataMask, (unsigned long)fFirstUsed, (unsigned long)fFirstUsed, (unsigned long)fNextFree, (unsigned long)fNextFree,
			(unsigned long)fHashCnt, (unsigned long)fHashCnt, (unsigned long)fHashMask, (unsigned long)fHashMask);
		str += tmp;
		sprintf( tmp, "fGrowCnt: %lu / fShrinkCnt: %lu / fRebuildHashCnt: %lu / fCompactCnt: %lu\n", 
			 fGrowCnt, fShrinkCnt, fRebuildHashCnt, fCompactCnt );
		str += tmp;
		if ( fDataSize )
		    {
		    sprintf( tmp, "%08lX - %08lX\n", (unsigned long)fUsedStart, (unsigned long)fIndicesStart );
		    str += tmp;
		    for ( unsigned long i = 0; i < fDataSize; i++ )
			{
			MLUCString tmpStr;
			tmpStr << *(fIndicesStart+i);
			sprintf( tmp, "    %10lu / 0x%08lX: %s - %s -", i, i, ( *(fUsedStart+i) ? "  used" : "unused" ), tmpStr.c_str() );
			str += tmp;
			unsigned size;
			if ( sizeof(TElementType)>=16 )
			    size = 16;
			else
			    size = sizeof(TElementType);
			uint8* data = (uint8*)( fDataStart+i);
			for ( unsigned long n = 0; n < size; n++ )
			    {
			    sprintf( tmp, " 0x%02lX", (unsigned long)( data[n] ) );
			    str += tmp;
			    }
			str += "\n";
			}
		    }
		else
		    {
		    unsigned long cnt = fData.size();
		    for ( unsigned long i = 0; i < cnt; i++ )
			{
			MLUCString tmpStr;
			tmpStr << fIndicesStart[i];
			sprintf( tmp, "    %10lu / 0x%08lX:  used - %s -", i, i, tmpStr.c_str() );
			str += tmp;
			unsigned size;
			if ( sizeof(TElementType)>=16 )
			    size = 16;
			else
			    size = sizeof(TElementType);
			uint8* data = (uint8*)&( fData[i] );
			for ( unsigned long n = 0; n < size; n++ )
			    {
			    sprintf( tmp, " 0x%02lX", (unsigned long)( data[n] ) );
			    str += tmp;
			    }
			str += "\n";
			}
		    }
		sprintf( tmp, "Hashes: 0x%08lX\n", (unsigned long)fHashes );
		str += tmp;
		for ( unsigned long h = 0; h < fHashCnt; h++ )
		    {
		    fHashes[h].AsString( h, str );
		    }
		}
#endif

	void SetPreDivisor( unsigned long preDivisor )
		{
		fPreDivisor = preDivisor;
		}
	
	unsigned long GetHashBucketSizeDelta() const
		{
		if ( fHashCnt<=1 )
		    return 0;
		unsigned long max, min;
		max = min = fHashes[0].GetCnt();
		for ( unsigned long h = 1; h < fHashCnt; h++ )
		    {
		    if ( fHashes[h].GetCnt()<min )
			min = fHashes[h].GetCnt();
		    if ( fHashes[h].GetCnt()>max )
			max = fHashes[h].GetCnt();
		    }
		return max-min;
		}

    protected:

	void Init( unsigned indexHashBits, int bufferSizeExp2 );
	void Compact( const TElementType* newElement, bool rebuildHash ); // Only called in fixed size mode.
	void Resize( bool grow ); // Only called in fixed size mode.
	void RebuildHash();

	TElementType fDefaultElement;
	bool fDefaultElementSet;

	vector<TElementType> fData;
	TElementType* fDataStart;
	vector<unsigned char> fUsed;
	unsigned char* fUsedStart;
	vector<TIndexType> fIndices;
	TIndexType* fIndicesStart;

	unsigned long fDataCnt;
	unsigned long fInvalidDataCnt;
	unsigned long fDataSize;
	unsigned long fOrigDataSize;
	unsigned long fDataMask;
	unsigned long fFirstUsed;
	unsigned long fNextFree;
	bool fGrow;

	struct THashData
	    {
		TIndexType fHashIndex;
		unsigned long fVectorIndex;
		THashData()
			{
			fHashIndex = TIndexType();
			fVectorIndex = ~(unsigned long)0;
			}
	    };

	class THash
	    {
	    public:
		THash( int bufferSizeExp2 ):
		    fParent(NULL)
			{
			fBufferSizeExp2 = bufferSizeExp2;
			if ( bufferSizeExp2<0 )
			    {
			    fSize = 0;
			    fHashData = NULL;
			    fMask = ~(unsigned long)0;
			    }
			else
			    {
			    fSize = 1 << bufferSizeExp2;
			    fHashData = new THashData[ fSize ];
			    if ( !fHashData )
				fSize = 0;
			    fMask = fSize-1;
			    }
			fInitialSize = fSize;
			fStartNdx = 0;
			fEndNdx = 0;
			fLastFoundNdx = ~(unsigned long)0;
			fCnt = 0;
			fGrowCnt = fShrinkCnt = 0;
			}
		THash():
		    fParent(NULL)
			{
			fBufferSizeExp2 = -1;
			fSize = 0;
			fHashData = NULL;
			fMask = ~(unsigned long)0;
			fInitialSize = fSize;
			fStartNdx = 0;
			fEndNdx = 0;
			fLastFoundNdx = ~(unsigned long)0;
			fCnt = 0;
			fGrowCnt = fShrinkCnt = 0;
			}
		~THash()
			{
			if ( fHashData )
			    delete [] fHashData;
			fHashData = NULL;
			}
		bool Add( TIndexType hashIndex, unsigned long vectorIndex, bool overwrite=false )
			{
			if ( fCnt >= fSize )
			    {
			    fGrowCnt++;
			    unsigned long newSize;
			    if ( fSize==0 )
				newSize = 2;
			    else
				newSize = fSize<<1;
			    THashData* newData = new THashData[ newSize ];
			    if ( !newSize )
				return false;
			    if ( fBufferSizeExp2==-1 || (fStartNdx==0 && fEndNdx==0) )
				{
				// One block from beginning to end
				memcpy( newData, fHashData, sizeof(THashData)*fSize );
				memset( newData+fSize, 0xFFFFFFFF, sizeof(THashData)*newSize-sizeof(THashData)*fSize );
				if ( fBufferSizeExp2!=-1 )
				    {
				    fStartNdx = 0;
				    fEndNdx = fSize;
				    }
				}
			    else
				{
				// Wrapped around in two blocks, one at the beginning, one at the end
				// Remember, we double the size
				// Also, the buffer is full, so start ndx == end ndx
				memcpy( newData, fHashData, sizeof(THashData)*fEndNdx );
				memcpy( newData+fStartNdx+fSize, fHashData+fStartNdx, sizeof(THashData)*(fSize-fStartNdx) );
				memset( newData+fEndNdx, 0xFFFFFFFF, sizeof(THashData)*fSize );
				if ( fBufferSizeExp2!=-1 )
				    fStartNdx += fSize;
				}
			    
			    delete [] fHashData;
			    fHashData = newData;
			    fSize = newSize;
			    if ( fBufferSizeExp2==-1 )
				{
				fHashData[fCnt].fVectorIndex = vectorIndex;
				fHashData[fCnt].fHashIndex = hashIndex;
				}
			    else
				{
				fMask = fSize-1;
				if ( fHashData[fEndNdx].fVectorIndex!=~(unsigned long)0 )
				    return false; // Internal error
				fHashData[fEndNdx].fVectorIndex = vectorIndex;
				fHashData[fEndNdx].fHashIndex = hashIndex;
				fEndNdx = (fEndNdx+1) & fMask;
				}
			    fCnt++;
			    fLastFoundNdx = ~(unsigned long)0;
			    return true;
			    }
			else
			    {
			    if ( fBufferSizeExp2==-1 )
				{
				fHashData[fCnt].fVectorIndex = vectorIndex;
				fHashData[fCnt].fHashIndex = hashIndex;
				fCnt++;
				return true;
				}
			    else
				{
				if ( fHashData[fEndNdx].fVectorIndex == ~(unsigned long)0 || overwrite )
				    {
				    fHashData[fEndNdx].fVectorIndex = vectorIndex;
				    fHashData[fEndNdx].fHashIndex = hashIndex;
				    fCnt++;
				    fEndNdx = (fEndNdx+1) & fMask;
				    return true;
				    }
				unsigned long ndx = (fEndNdx+1) & fMask;
				do
				    {
				    if ( fHashData[ndx].fVectorIndex == ~(unsigned long)0 || overwrite )
					{
					fHashData[ndx].fVectorIndex = vectorIndex;
					fHashData[ndx].fHashIndex = hashIndex;
					fCnt++;
					return true;
					}
				    ndx = (ndx+1) & fMask;
				    }
				while ( ndx != fEndNdx );
				}
			    
			    }
			return false;
			}
		bool Remove( TIndexType hashIndex )
			{
#ifdef PROFILE_INDEXED_VECTOR
			if ( fParent )
			    ++fParent->fRemoveCallCount;
#endif
			bool success = false;
			if ( fBufferSizeExp2==-1 )
			    {
			    if ( fLastFoundNdx!=~(unsigned long)0 && fHashData[fLastFoundNdx].fHashIndex==hashIndex )
				{
				for ( unsigned long n = fLastFoundNdx; n<fCnt-1; n++ )
				    fHashData[n] = fHashData[n+1];
				fCnt--;
				fHashData[fCnt].fVectorIndex = ~(unsigned long)0;
				fHashData[fCnt].fHashIndex = TIndexType();
				success = true;
				}
			    else
				{
				for ( unsigned long ndx = 0; ndx < fCnt; ndx++ )
				    {
				    if ( fHashData[ndx].fHashIndex == hashIndex )
					{
					for ( unsigned long n = ndx; n<fCnt-1; n++ )
					    fHashData[n] = fHashData[n+1];
					fCnt--;
					fHashData[fCnt].fVectorIndex = ~(unsigned long)0;
					fHashData[fCnt].fHashIndex = TIndexType();
					success = true;
					break;
					}
				    }
				}
			    }
			else
			    {
#ifdef PROFILE_INDEXED_VECTOR
			    if ( fParent )
				++fParent->fRemoveIterationCount;
#endif
			    unsigned long removeIndex = ~(unsigned long)0;
			    if ( fHashData[fStartNdx].fHashIndex == hashIndex )
				{
				fHashData[fStartNdx].fHashIndex = TIndexType();
				fHashData[fStartNdx].fVectorIndex = ~(unsigned long)0;
				fStartNdx = (fStartNdx+1) & fMask;
				fCnt--;
				if ( fCnt>0 )
				    {
				    while ( fHashData[fStartNdx].fVectorIndex == ~(unsigned long)0 )
					fStartNdx = (fStartNdx+1) & fMask;
				    }
				success = true;
				removeIndex = fStartNdx;
				}
			    else if ( fLastFoundNdx!=~(unsigned long)0 && fHashData[fLastFoundNdx].fHashIndex==hashIndex )
				{
				fHashData[fLastFoundNdx].fHashIndex = TIndexType();
				fHashData[fLastFoundNdx].fVectorIndex = ~(unsigned long)0;
				fCnt--;
				success = true;
				removeIndex = fLastFoundNdx;
				}
			    else
				{
				unsigned long ndx = (fStartNdx+1) & fMask;
				unsigned long cnt = 1; // First element already tested separately
				do
				    {
#ifdef PROFILE_INDEXED_VECTOR
				    if ( fParent )
					++fParent->fRemoveIterationCount;
#endif
				    if ( fHashData[ndx].fVectorIndex != ~(unsigned long)0 )
					{
					if ( fHashData[ndx].fHashIndex == hashIndex )
					    {
					    fHashData[ndx].fHashIndex = TIndexType();
					    fHashData[ndx].fVectorIndex = ~(unsigned long)0;
					    fCnt--;
					    success = true;
					    removeIndex = ndx;
					    break;
					    }
					cnt++;
					}
				    ndx = (ndx+1) & fMask;
				    }
				while ( ndx != fStartNdx && cnt<fCnt );
				}
			    if ( success && removeIndex==((fEndNdx-1) & fMask) && fCnt>0 )
				{
				fEndNdx = (fEndNdx-1) & fMask;
				while ( fHashData[(fEndNdx-1) & fMask].fVectorIndex == ~(unsigned long)0 )
				    fEndNdx = (fEndNdx-1) & fMask;
				}
			    if ( success && fCnt==0 )
				fEndNdx = fStartNdx;
			    }
			if ( success )
			    {
			    fLastFoundNdx = ~(unsigned long)0;
			    if ( (fCnt <= fSize>>2) && fSize>fInitialSize )
				{
				Shrink();
				}
			    return true;
			    }
			else
			    return false;
			}
		bool Find( TIndexType hashIndex, unsigned long& vectorIndex )
			{
#ifdef PROFILE_INDEXED_VECTOR
			if ( fParent )
			    ++fParent->fFindCallCount;
#endif
			if ( fBufferSizeExp2==-1 )
			    {
			    for ( unsigned long i = 0; i < fCnt; i++ )
				{
				if ( fHashData[i].fHashIndex == hashIndex )
				    {
				    vectorIndex = fHashData[i].fVectorIndex;
				    fLastFoundNdx = i;
				    return true;
				    }
				}
			    }
			else
			    {
			    unsigned long ndx = fStartNdx;
			    unsigned long cnt = 0;
			    do
				{
#ifdef PROFILE_INDEXED_VECTOR
				if ( fParent )
				    ++fParent->fFindIterationCount;
#endif
				if ( fHashData[ndx].fVectorIndex != ~(unsigned long)0 )
				    {
				    if ( fHashData[ndx].fHashIndex == hashIndex )
					{
					vectorIndex = fHashData[ndx].fVectorIndex;
					fLastFoundNdx = ndx;
					return true;
					}
				    cnt++;
				    }
				ndx = (ndx+1) & fMask;
				}
			    while ( ndx != fStartNdx && cnt < fCnt );
			    }
			fLastFoundNdx = vectorIndex = ~(unsigned long)0;
			return false;
			}
		bool FindReverse( TIndexType hashIndex, unsigned long& vectorIndex )
			{
#ifdef PROFILE_INDEXED_VECTOR
			if ( fParent )
			    ++fParent->fFindReverseCallCount;
#endif
			if ( fBufferSizeExp2==-1 )
			    {
			    for ( unsigned long i = fCnt; i >0; i-- )
				{
				if ( fHashData[i-1].fHashIndex == hashIndex )
				    {
				    vectorIndex = fHashData[i-1].fVectorIndex;
				    fLastFoundNdx = i-1;
				    return true;
				    }
				}
			    }
			else
			    {
			    unsigned long ndx = fEndNdx;
			    unsigned long cnt = 0;
			    do
				{
#ifdef PROFILE_INDEXED_VECTOR
				if ( fParent )
				    ++fParent->fFindReverseIterationCount;
#endif
				ndx = (ndx-1) & fMask;
				if ( fHashData[ndx].fVectorIndex != ~(unsigned long)0 )
				    {
				    if ( fHashData[ndx].fHashIndex == hashIndex )
					{
					vectorIndex = fHashData[ndx].fVectorIndex;
					fLastFoundNdx = ndx;
					return true;
					}
				    cnt++;
				    }
				}
			    while ( ndx != fEndNdx && cnt < fCnt );
			    }
			fLastFoundNdx = vectorIndex = ~(unsigned long)0;
			return false;
			}
		void Reset()
			{
			fCnt = 0;
			fStartNdx = 0;
			fEndNdx = 0;
			fLastFoundNdx = ~(unsigned long)0;
#if 0
			for ( unsigned long i = 0; i<fSize; i++ )
			    {
			    fHashData[i].fVectorIndex = ~(unsigned long)0;
			    fHashData[i].fHashIndex = TIndexType();
			    }
#endif
			}
		void ClearRemaining()
			{
			for ( unsigned long i = fCnt; i<fSize; i++ )
			    {
			    fHashData[i].fVectorIndex = ~(unsigned long)0;
			    fHashData[i].fHashIndex = TIndexType();
			    }
			}
		
		unsigned long GetCnt() const
			{
			return fCnt;
			}
#ifdef DEBUG_INDEXED_VECTOR
		void AsString( unsigned long h, MLUCString& str )
			{
			char tmp[4096];
			sprintf( tmp, "Hash table this: 0x%08lX\n", (unsigned long)this );
			str += tmp;
			sprintf( tmp, "  Hash table %10lu / 0x%08lX - size: %10lu / 0x%08lX - cnt: %10lu / 0x%08lX - mask: %10lu / 0x%08lX - start ndx: %10lu / 0x%08lX - end ndx: %10lu / 0x%08lX - gow cnt: %lu - shrink cnt: %lu\n", 
				 (unsigned long)h, (unsigned long)h, (unsigned long)fSize, (unsigned long)fSize, (unsigned long)fCnt, (unsigned long)fCnt, (unsigned long)fMask, (unsigned long)fMask, (unsigned long)fStartNdx, (unsigned long)fStartNdx, (unsigned long)fEndNdx, (unsigned long)fEndNdx, fGrowCnt, fShrinkCnt );
			str += tmp;
			for ( unsigned long i = 0; i < fSize; i++ )
			    {
			    MLUCString tmpStr;
			    tmpStr << fHashData[i].fHashIndex;
			    sprintf( tmp, "    %10lu / 0x%08lX: %s - %s - %10lu / 0x%08lX\n", 
				     i, i, ( fHashData[i].fVectorIndex!=~(unsigned long)0 ? "  used" : "unused" ),
				     tmpStr.c_str(), fHashData[i].fVectorIndex, fHashData[i].fVectorIndex );
			    str += tmp;
			    }
			}
#endif
	    protected:
		void SetSize( int bufferSizeExp2 = -1 )
			{
			if ( fCnt>0 )
			    return;
			if ( fHashData )
			    delete [] fHashData;
			fBufferSizeExp2 = bufferSizeExp2;
			if ( bufferSizeExp2==-1 )
			    {
			    fSize = 0;
			    fHashData = NULL;
			    fMask = ~(unsigned long)0;
			    }
			else
			    {
			    fSize = 1 << bufferSizeExp2;
			    fHashData = new THashData[ fSize ];
			    if ( !fHashData )
				fSize = 0;
			    fMask = fSize-1;
			    }
			fInitialSize = fSize;
			fStartNdx = 0;
			fEndNdx = 0;
			fLastFoundNdx = ~(unsigned long)0;
			fCnt = 0;
			}
		void SetParent( MLUCIndexedVector<TElementType,TIndexType>* parent )
			{
			fParent = parent;
			}
		void Shrink()
			{
			fShrinkCnt++;
			unsigned long newSize = fSize>>1;
			THashData* newData = new THashData[newSize];
			if ( !newData )
			    return;
			if ( fBufferSizeExp2==-1 )
			    {
			    memcpy( newData, fHashData, sizeof(THashData)*fCnt );
			    }
			else
			    {
			    unsigned long ndx = fStartNdx;
			    unsigned long cnt = 0;
			    do
				{
				if ( fHashData[ndx].fVectorIndex!=~(unsigned long)0 )
				    {
				    newData[cnt] = fHashData[ndx];
				    cnt++;
				    }
				ndx = (ndx+1) & fMask;
				}
			    while ( ndx != fStartNdx && cnt < fCnt );
			    fMask = newSize-1;
			    fStartNdx = 0;
			    fEndNdx = cnt;
			    }
			delete [] fHashData;
			fSize = newSize;
			fHashData = newData;
			fLastFoundNdx = ~(unsigned long)0;
			}

		    friend class MLUCIndexedVector<TElementType,TIndexType>;
		MLUCIndexedVector<TElementType,TIndexType>* fParent;

		int fBufferSizeExp2;
		unsigned long fSize;
		unsigned long fInitialSize;
		unsigned long fCnt;
		unsigned long fMask;
		THashData* fHashData;
		unsigned long fStartNdx;
		unsigned long fEndNdx;
		unsigned long fLastFoundNdx;
		unsigned long fGrowCnt;
		unsigned long fShrinkCnt;



	    private:

		// Inhibit  copy constructor and assignment operator
		THash( const THash& ) {}
		THash& operator=( const THash& ) { return *this; }
		
	    };

	THash* fHashes;
	unsigned long fHashCnt;
	unsigned long fHashMask;

	    friend class MLUCIndexedVectorBaseSearcher<TElementType,TIndexType>;

      unsigned long fGrowCnt;
      unsigned long fShrinkCnt;
      unsigned long fCompactCnt;
      unsigned long fRebuildHashCnt;


	unsigned long fPreDivisor;

#ifdef PROFILE_INDEXED_VECTOR
	unsigned long long fFindCallCount;
	unsigned long long fFindIterationCount;
	unsigned long long fFindReverseCallCount;
	unsigned long long fFindReverseIterationCount;
	unsigned long long fRemoveCallCount;
	unsigned long long fRemoveIterationCount;
#endif

    private:

	// Inhibit  copy constructor and assignment operator
	MLUCIndexedVector( const MLUCIndexedVector& ) {};
	MLUCIndexedVector& operator=( const MLUCIndexedVector& ) { return *this; }


    };

template<class TElementType,class TIndexType>
class MLUCIndexedVectorBaseSearcher
    {
    protected:
	MLUCIndexedVectorBaseSearcher()
		{
		}
#if __GNUC__>=3
	static void GetVectorData( MLUCIndexedVector<TElementType,TIndexType>& vect, typename vector<TElementType>::iterator& dataBegin, 
			    typename vector<TElementType>::iterator& dataEnd, unsigned long& dataSize, 
			    unsigned long& dataCnt, unsigned long& invalidDataCnt, unsigned char*& usedStart, 
			    TElementType*& dataStart, unsigned long& firstUsed, unsigned long& nextFree, 
			    unsigned long& dataMask )
#else
	static void GetVectorData( MLUCIndexedVector<TElementType,TIndexType>& vect, vector<TElementType>::iterator& dataBegin, 
			    vector<TElementType>::iterator& dataEnd, unsigned long& dataSize, 
			    unsigned long& dataCnt, unsigned long& invalidDataCnt, unsigned char*& usedStart, 
			    TElementType*& dataStart, unsigned long& firstUsed, unsigned long& nextFree, 
			    unsigned long& dataMask )

#endif
		{
		dataBegin = vect.fData.begin();
		dataEnd = vect.fData.end();
		dataSize = vect.fDataSize;
		dataCnt = vect.fDataCnt;
		invalidDataCnt = vect.fInvalidDataCnt;
		usedStart = vect.fUsedStart;
		dataStart = vect.fDataStart;
		firstUsed = vect.fFirstUsed;
		nextFree = vect.fNextFree;
		dataMask = vect.fDataMask;
		}
#if __GNUC__>=3
	static void GetHashData( MLUCIndexedVector<TElementType,TIndexType>& vect, unsigned long& hashMask, typename MLUCIndexedVector<TElementType,TIndexType>::THash*& hashes )
#else
	static void GetHashData( MLUCIndexedVector<TElementType,TIndexType>& vect, unsigned long& hashMask, MLUCIndexedVector<TElementType,TIndexType>::THash*& hashes )
#endif
		{
		hashMask = vect.fHashMask;
		hashes = vect.fHashes;
		}
    };

template<class TElementType,class TIndexType,class TSearchDataType>
class MLUCIndexedVectorSearcher: public MLUCIndexedVectorBaseSearcher<TElementType,TIndexType>
    {
    public:

	typedef bool (*TSearchFunc)( const TElementType& el, const TSearchDataType& searchData );
	typedef bool (*TSearchFuncUInt64)( const TElementType& el, uint64 searchData );
	typedef bool (*TSearchFuncPtr)( const TElementType& el, void* arg );

	static bool FindElement( MLUCIndexedVector<TElementType,TIndexType>& vect, TIndexType index, unsigned long& elementNdx )
		{
		unsigned long hashMask;
#if __GNUC__>=3
		typename MLUCIndexedVector<TElementType,TIndexType>::THash* hashes;
#else
		MLUCIndexedVector<TElementType,TIndexType>::THash* hashes;
#endif
		GetHashData( hashMask, hashes );
		unsigned long hashIndex = index & hashMask;
		return hashes[hashIndex].Find( index, elementNdx );
		}

	static bool FindElement( MLUCIndexedVector<TElementType,TIndexType>& vect, TSearchFunc searchFunc, const TSearchDataType& searchData, unsigned long& elementNdx )
		{
#if __GNUC__>=3
		typename vector<TElementType>::iterator dataIter;
		typename vector<TElementType>::iterator dataEnd;
#else
		vector<TElementType>::iterator dataIter;
		vector<TElementType>::iterator dataEnd;
#endif
		unsigned long dataSize; 
		unsigned long dataCnt;
		unsigned long invalidDataCnt;
		unsigned char* usedStart; 
		TElementType* dataStart;
		unsigned long firstUsed;
		unsigned long nextFree; 
		unsigned long dataMask;
		MLUCIndexedVectorSearcher::GetVectorData( vect, dataIter, dataEnd, dataSize, 
			       dataCnt, invalidDataCnt, usedStart, 
			       dataStart, firstUsed, nextFree, 
			       dataMask );
		if ( !dataSize )
		    {
		    unsigned long n = 0;
		    while ( dataIter != dataEnd )
			{
			if ( (*searchFunc)( *dataIter, searchData ) )
			    {
			    elementNdx = n;
			    return true;
			    }
			dataIter++;
			n++;
			}
		    }
		else
		    {
		    unsigned long i = 0;
		    unsigned long n = firstUsed;
		    while ( i<dataCnt+invalidDataCnt && (!(*(usedStart+n)) || !(*searchFunc)( *(dataStart+n), searchData )) )
			{
			n = (n+1) & dataMask;
			i++;
			}
// 		    if ( *(fUsedStart+n) && (*searchFunc)( *(fDataStart+n), searchData ) )
		    if ( i<dataCnt+invalidDataCnt && *(usedStart+n) )
			{
			elementNdx = n;
			return true;
			}
		    }
		return false;
		}

	static bool FindElement( MLUCIndexedVector<TElementType,TIndexType>& vect, TSearchFuncUInt64 searchFunc, uint64 searchData, unsigned long& elementNdx )
		{
#if __GNUC__>=3
		typename vector<TElementType>::iterator dataIter;
		typename vector<TElementType>::iterator dataEnd;
#else
		vector<TElementType>::iterator dataIter;
		vector<TElementType>::iterator dataEnd;
#endif
		unsigned long dataSize; 
		unsigned long dataCnt;
		unsigned long invalidDataCnt;
		unsigned char* usedStart; 
		TElementType* dataStart;
		unsigned long firstUsed;
		unsigned long nextFree; 
		unsigned long dataMask;
		GetVectorData( vect, dataIter, dataEnd, dataSize, 
			       dataCnt, invalidDataCnt, usedStart, 
			       dataStart, firstUsed, nextFree, 
			       dataMask );
		if ( !dataSize )
		    {
		    unsigned long n = 0;
		    while ( dataIter != dataEnd )
			{
			if ( (*searchFunc)( *dataIter, searchData ) )
			    {
			    elementNdx = n;
			    return true;
			    }
			dataIter++;
			n++;
			}
		    }
		else
		    {
		    unsigned long i = 0;
		    unsigned long n = firstUsed;
		    while ( i<dataCnt+invalidDataCnt && (!(*(usedStart+n)) || !(*searchFunc)( *(dataStart+n), searchData )) )
			{
			n = (n+1) & dataMask;
			i++;
			}
// 		    if ( *(fUsedStart+n) && (*searchFunc)( *(fDataStart+n), searchData ) )
		    if ( i<dataCnt+invalidDataCnt && *(usedStart+n) )
			{
			elementNdx = n;
			return true;
			}
		    }
		return false;
		}

	static bool FindElement( MLUCIndexedVector<TElementType,TIndexType>& vect, TSearchFuncPtr searchFunc, void* arg, unsigned long& elementNdx )
		{
#if __GNUC__>=3
		typename vector<TElementType>::iterator dataIter;
		typename vector<TElementType>::iterator dataEnd;
#else
		vector<TElementType>::iterator dataIter;
		vector<TElementType>::iterator dataEnd;
#endif
		unsigned long dataSize; 
		unsigned long dataCnt;
		unsigned long invalidDataCnt;
		unsigned char* usedStart; 
		TElementType* dataStart;
		unsigned long firstUsed;
		unsigned long nextFree; 
		unsigned long dataMask;
		GetVectorData( vect, dataIter, dataEnd, dataSize, 
			       dataCnt, invalidDataCnt, usedStart, 
			       dataStart, firstUsed, nextFree, 
			       dataMask );
		if ( !dataSize )
		    {
		    unsigned long n = 0;
		    while ( dataIter != dataEnd )
			{
			if ( (*searchFunc)( *dataIter, arg ) )
			    {
			    elementNdx = n;
			    return true;
			    }
			dataIter++;
			n++;
			}
		    }
		else
		    {
		    unsigned long i = 0;
		    unsigned long n = firstUsed;
		    while ( i<dataCnt+invalidDataCnt && (!(*(usedStart+n)) || !(*searchFunc)( *(dataStart+n), arg )) )
			{
			n = (n+1) & dataMask;
			i++;
			}
// 		    if ( *(fUsedStart+n) && (*searchFunc)( *(fDataStart+n), arg ) )
		    if ( i<dataCnt+invalidDataCnt && *(usedStart+n) )
			{
			elementNdx = n;
			return true;
			}
		    }
		return false;
		}

	static bool FindElement( MLUCIndexedVector<TElementType,TIndexType>& vect, unsigned long startNdx, bool directionForward, TSearchFunc searchFunc, const TSearchDataType& searchData, unsigned long& elementNdx );

	// Search starts from the end.
	static bool FindElementReverse( MLUCIndexedVector<TElementType,TIndexType>& vect, TSearchFuncUInt64 searchFunc, uint64 searchData, unsigned long& elementNdx )
		{
#if __GNUC__>=3
		typename vector<TElementType>::iterator dataIter;
		typename vector<TElementType>::iterator dataBegin;
#else
		vector<TElementType>::iterator dataIter;
		vector<TElementType>::iterator dataBegin;
#endif
		unsigned long dataSize; 
		unsigned long dataCnt;
		unsigned long invalidDataCnt;
		unsigned char* usedStart; 
		TElementType* dataStart;
		unsigned long firstUsed;
		unsigned long nextFree; 
		unsigned long dataMask;
		GetVectorData( vect, dataBegin, dataIter, dataSize, 
			       dataCnt, invalidDataCnt, usedStart, 
			       dataStart, firstUsed, nextFree, 
			       dataMask );
		if ( !dataSize )
		    {
		    unsigned long n = dataSize;
		    if ( dataIter != dataBegin )
			{
			do
			    {
			    dataIter--;
			    n--;
			    if ( (*searchFunc)( *dataIter, searchData ) )
				{
				elementNdx = n;
				return true;
				}
			    }
			while ( dataIter != dataBegin );
			}
		    }
		else
		    {
		    unsigned long i = 0;
		    unsigned long n = (nextFree-1) & dataMask;
		    while ( i<dataCnt+invalidDataCnt && (!(*(usedStart+n)) || !(*searchFunc)( *(dataStart+n), searchData )) )
			{
			n = (n-1) & dataMask;
			i++;
			}
// 		    if ( *(fUsedStart+n) && (*searchFunc)( *(fDataStart+n), searchData ) )
		    if ( i<dataCnt+invalidDataCnt && *(usedStart+n) )
			{
			elementNdx = n;
			return true;
			}
		    }
		return false;
		}

    protected:
	MLUCIndexedVectorSearcher()
		{
		}

    };


template<class TElementType, class TIndexType, class TSearchDataType>
inline bool MLUCIndexedVectorSearcher<TElementType,TIndexType,TSearchDataType>::FindElement( MLUCIndexedVector<TElementType,TIndexType>& vect, unsigned long startNdx, bool directionForward, TSearchFunc searchFunc, const TSearchDataType& searchData, unsigned long& elementNdx )
    {
#if __GNUC__>=3
    typename vector<TElementType>::iterator dataBegin;
    typename vector<TElementType>::iterator dataEnd;
#else
    vector<TElementType>::iterator dataBegin;
    vector<TElementType>::iterator dataEnd;
#endif
    unsigned long dataSize; 
    unsigned long dataCnt;
    unsigned long invalidDataCnt;
    unsigned char* usedStart; 
    TElementType* dataStart;
    unsigned long firstUsed;
    unsigned long nextFree; 
    unsigned long dataMask;
    GetVectorData( vect, dataBegin, dataEnd, dataSize, 
		   dataCnt, invalidDataCnt, usedStart, 
		   dataStart, firstUsed, nextFree, 
		   dataMask );
    if ( dataCnt<=0 )
	return false;
    if ( !dataSize )
	{
	if ( dataBegin+startNdx>=dataEnd )
	    return false;
#if __GNUC__>=3
	typename vector<TElementType>::iterator iter = dataBegin+startNdx;
	typename vector<TElementType>::iterator boundary = (directionForward ? dataEnd : dataBegin);
#else
	vector<TElementType>::iterator iter = dataBegin+startNdx;
	vector<TElementType>::iterator boundary = (directionForward ? dataEnd : dataBegin);
#endif
	int step = (directionForward ? 1 : -1);
	int offset = (directionForward ? 0 : -1);
	while ( iter!=boundary )
	    {
	    if ( (*searchFunc)( *(iter+offset), searchData ) )
		{
		elementNdx = startNdx;
		return true;
		}
	    iter += step;
	    startNdx += step;
	    }
	return false;
	}
    else
	{
	unsigned long i = 0;
	unsigned long n = firstUsed;
	int step = (directionForward ? 1 : -1);
	//unsigned long boundary = (directionForward ? fNextFree : (fFirstUsed-1) & fDataMask);
	if ( firstUsed<nextFree )
	    {
	    if ( startNdx<firstUsed || startNdx>=nextFree )
		return false;
	    }
	else
	    {
	    if ( startNdx<firstUsed && startNdx>=nextFree )
		return false;
	    }
	unsigned long cnt;
	if ( directionForward )
	    {
	    if ( firstUsed<nextFree )
		cnt = nextFree-startNdx;
	    else
		{
		if ( startNdx<firstUsed )
		    cnt = nextFree-startNdx;
		else
		    cnt = nextFree+dataSize-startNdx;
		}
	    }
	else
	    {
	    if ( firstUsed<nextFree )
		cnt = startNdx-firstUsed+1;
	    else
		{
		if ( startNdx<firstUsed )
		    cnt = startNdx+dataSize-firstUsed+1;
		else
		    cnt = startNdx-firstUsed+1;
		}
	    }
	while ( i++<cnt && (!(*(usedStart+n)) || !(*searchFunc)( *(dataStart+n), searchData )) )
	    n = (n+step) & dataMask;
	if ( (usedStart+n) && (*searchFunc)( *(dataStart+n), searchData ) )
	    {
	    elementNdx = n;
	    return true;
	    }
	else
	    return false;
	}
    }

template<class TElementType, class TIndexType>
inline void MLUCIndexedVector<TElementType,TIndexType>::Init( unsigned indexHashBits, int bufferSizeExp2 )
    {
    fGrowCnt = 0;
    fShrinkCnt = 0;
    fCompactCnt = 0;
    fRebuildHashCnt = 0;
    fDataCnt = 0;
    fInvalidDataCnt = 0;
    fFirstUsed = fNextFree = 0;
    int hashExp2 = -1; 
    if ( bufferSizeExp2 < 0 )
	{
	fDataSize = 0;
	fOrigDataSize = 0;
	fDataMask = ~(unsigned long)0;
	fDataStart = NULL;
	fUsedStart = NULL;
	//printf( "fUsedStart: 0x%08lX\n", (unsigned long)fUsedStart );
	fIndicesStart = NULL;
	//printf( "fIndicesStart: 0x%08lX\n", (unsigned long)fUsedStart );
	}
    else
	{
	fOrigDataSize = fDataSize = 1 << bufferSizeExp2;
	fDataMask = fDataSize-1;
	fData.resize( fDataSize, fDefaultElement );
	fUsed.resize( fDataSize, false );
	fIndices.resize( fDataSize, TIndexType() );
#if __GNUC__>=3
	fDataStart = fData.begin().base();
	fUsedStart = fUsed.begin().base();
	//printf( "fUsedStart: 0x%08lX\n", (unsigned long)fUsedStart );
	fIndicesStart = fIndices.begin().base();
	//printf( "fIndicesStart: 0x%08lX\n", (unsigned long)fUsedStart );
#else
	fDataStart = fData.begin();
	fUsedStart = fUsed.begin();
	//printf( "fUsedStart: 0x%08lX\n", (unsigned long)fUsedStart );
	fIndicesStart = fIndices.begin();
	//printf( "fIndicesStart: 0x%08lX\n", (unsigned long)fUsedStart );
#endif
	if ( (unsigned)bufferSizeExp2 <= 3+indexHashBits )
	    hashExp2 = 3;
	else
	    hashExp2 = bufferSizeExp2-indexHashBits;
	}
    fHashCnt = 1 << indexHashBits;
    fHashMask = fHashCnt-1;
    fHashes = new THash[ fHashCnt ];
    for ( unsigned long n=0; n < fHashCnt; n++ )
	{
	fHashes[n].SetSize( hashExp2 );
	fHashes[n].SetParent( this );
	}
    fPreDivisor = 1;
    }

template<class TElementType, class TIndexType>
inline void MLUCIndexedVector<TElementType,TIndexType>::Compact( const TElementType*, bool rebuildHash )
    {
    fCompactCnt++;
    unsigned long i = 0;
    unsigned long dest, src;
    for ( i = 0, dest = src = fFirstUsed; i<(fDataCnt+fInvalidDataCnt); i++, src = (src+1) & fDataMask )
	{
	if ( *(fUsedStart+src) )
	    {
	    if ( dest != src )
		{
		*(fUsedStart+dest) = true;
		*(fDataStart+dest) = *(fDataStart+src);
		*(fIndicesStart+dest) = *(fIndicesStart+src);
		}
	    dest = (dest+1) & fDataMask;
	    }
	}
    for ( i = dest; i!= fNextFree; i=(i+1) & fDataMask )
	{
	*(fUsedStart+i) = false;
	if ( fDefaultElementSet )
	    {
	    *(fDataStart+i) = fDefaultElement;
	    *(fIndicesStart+i) = TIndexType();
	    }
	}
    fNextFree = dest;
    fInvalidDataCnt = 0;
    if ( rebuildHash )
	RebuildHash();
    }

template<class TElementType, class TIndexType>
inline void MLUCIndexedVector<TElementType,TIndexType>::Resize( bool grow )
    {
    unsigned long newSize;
    //TElementType elm( fDefaultElement, false );
    // Do we grow or shrink?
    if ( grow )
	{
	// We grow...
	fGrowCnt++;
	newSize = fDataSize<<1;
	if ( fDefaultElementSet )
	    {
	    fData.resize( newSize, fDefaultElement );
	    }
	else
	    {
	    fData.resize( newSize );
	    }
	fUsed.resize( newSize, false );
	fIndices.resize( newSize, TIndexType() );
#if __GNUC__>=3
	fDataStart = fData.begin().base();
	fUsedStart = fUsed.begin().base();
	//printf( "fUsedStart: 0x%08lX\n", (unsigned long)fUsedStart );
	fIndicesStart = fIndices.begin().base();
	//printf( "fIndicesStart: 0x%08lX\n", (unsigned long)fUsedStart );
#else
	fDataStart = fData.begin();
	fUsedStart = fUsed.begin();
	//printf( "fUsedStart: 0x%08lX\n", (unsigned long)fUsedStart );
	fIndicesStart = fIndices.begin();
	//printf( "fIndicesStart: 0x%08lX\n", (unsigned long)fUsedStart );
#endif
	// Note: We double the size...
	if ( fNextFree<fFirstUsed || (fNextFree==fFirstUsed && (fDataCnt+fInvalidDataCnt>0)) )
	    {
	    // The used area is split into two parts, one at the beginning
	    // and one at the end of the (old) array.
	    // In other words, the buffer has partly wrapped around.
	    // We copy the block at the beginning of the buffer to the end of the block at 
	    // the end (of the old size buffer, now in the middle)
	    unsigned long i;
#if 0
	    memcpy( fDataStart+fDataSize, fDataStart, fNextFree*sizeof(TElementType) );
	    memcpy( fUsedStart+fDataSize, fUsedStart, fNextFree*sizeof(unsigned char) );
	    memcpy( fIndicesStart+fDataSize, fIndicesStart, fNextFree*sizeof(TIndexType) );
#else
	    for ( i=0; i<fNextFree; i++ )
		{
		*(fDataStart+fDataSize+i) = *(fDataStart+i);
		*(fUsedStart+fDataSize+i) = *(fUsedStart+i);
		*(fIndicesStart+fDataSize+i) = *(fIndicesStart+i);
		}
#endif
	    for ( i = 0; i < fNextFree; i++ )
		{
		if ( fDefaultElementSet )
		    *(fDataStart+i) = fDefaultElement;
		*(fUsedStart+i) = false;
		*(fIndicesStart+i) = TIndexType();
		}
	    fNextFree += fDataSize;
	    }
	fDataSize = newSize;
	fDataMask = fDataSize-1;
	}
    else
	{
	// We shrink...
	fShrinkCnt++;
	newSize = fDataSize>>1;
	// Note: fNextFree==fFirstUsed means it is totally empty, 
	// as we use at most 1/4 of the buffer.
	if ( fNextFree < fFirstUsed )
	    {
	    // We have the used area split into two parts, one at the beginning
	    // and one at the end of the (old) array.
	    // In other words, the buffer has partly wrapped around.
	    // Note: We half the size...
	    // This means that newSize is both the new size and the amount by which we shrink.
	    // Also, newSize is then the amount by which we have to move the used slots 
	    // at the (old) end of the buffer toward its beginning (its new end). And since we half the size, we can be
	    // sure not to have any overlap between the src and dest. (We are at most a quarter the old size)
#if 0
	    memcpy( fDataStart+fFirstUsed-newSize, fDataStart+fFirstUsed, (fDataSize-fFirstUsed)*sizeof(TElementType) );
	    memcpy( fUsedStart+fFirstUsed-newSize, fUsedStart+fFirstUsed, (fDataSize-fFirstUsed)*sizeof(unsigned char) );
	    memcpy( fIndicesStart+fFirstUsed-newSize, fIndicesStart+fFirstUsed, (fDataSize-fFirstUsed)*sizeof(TIndexType) );
#else
	    unsigned long i;
	    for ( i = 0; i<(fDataSize-fFirstUsed); i++ )
		{
		*(fDataStart+fFirstUsed-newSize+i) = *(fDataStart+fFirstUsed+i);
		*(fUsedStart+fFirstUsed-newSize+i) = *(fUsedStart+fFirstUsed+i);
		*(fIndicesStart+fFirstUsed-newSize+i) = *(fIndicesStart+fFirstUsed+i);
		}
#endif
	    fFirstUsed -= newSize;
	    }
	else
	    {
	    // The used slots are in one block in the middle of the buffer,
	    // not wrapped around.
	    // We move the block to the beginning of the buffer to be sure we do not
	    // cut of any valid data.
	    unsigned long i, n, newEnd = fNextFree-fFirstUsed; // The amount of entries, also the 
	                                                       // new end point, once the block is moved to the beginning of the buffer.
	    if ( fFirstUsed>0 )
		{
		// The used block is not yet at the beginning of the buffer.
		if ( newEnd > fFirstUsed )
		    {
		    // The new end points is greater than the old starting point
		    // => the old and new block locations overlap.
		    i = 0;
		    while ( newEnd > i )
			{
			if ( newEnd-i>=fFirstUsed )
			    n = fFirstUsed;
			else
			    n = newEnd-i;
#if 0
			memcpy( fDataStart+i, fDataStart+i+fFirstUsed, n*sizeof(TElementType) );
			memcpy( fUsedStart+i, fUsedStart+i+fFirstUsed, n*sizeof(unsigned char) );
			memcpy( fIndicesStart+i, fIndicesStart+i+fFirstUsed, n*sizeof(TIndexType) );
#else
			unsigned long b;
			for ( b = 0; b < n; b++ )
			    {
			    *(fDataStart+i+b) = *(fDataStart+i+fFirstUsed+b);
			    *(fUsedStart+i+b) = *(fUsedStart+i+fFirstUsed+b);
			    *(fIndicesStart+i+b) = *(fIndicesStart+i+fFirstUsed+b);
			    }
#endif
			i += n;
			}
		    i = newEnd;
		    for ( n = 0; n < fNextFree-newEnd; n++ )
			{
			if ( fDefaultElementSet )
			    *(fDataStart+i) = fDefaultElement;
			*(fUsedStart+i) = false;
			*(fIndicesStart+i) = TIndexType();
			i = (i+1) & fDataMask;
			}
		    }
		else
		    {
		    // The old and new block locations are completely separate.
#if 0
		    memcpy( fDataStart, fDataStart+fFirstUsed, newEnd*sizeof(TElementType) );
		    memcpy( fUsedStart, fUsedStart+fFirstUsed, newEnd*sizeof(unsigned char) );
		    memcpy( fIndicesStart, fIndicesStart+fFirstUsed, newEnd*sizeof(TIndexType) );
#else
		    unsigned long n;
		    for ( n=0; n < newEnd; n++ )
			{
			*(fDataStart+n) = *(fDataStart+fFirstUsed+n);
			*(fUsedStart+n) = *(fUsedStart+fFirstUsed+n);
			*(fIndicesStart+n) = *(fIndicesStart+fFirstUsed+n);
			}
#endif
		    i = fFirstUsed;
		    for ( n = 0; n < newEnd; n++ )
			{
			if ( fDefaultElementSet )
			    *(fDataStart+i) = fDefaultElement;
			*(fUsedStart+i) = false;
			*(fIndicesStart+i) = TIndexType();
			i = (i+1) & fDataMask;
			}
		    }
		fFirstUsed = 0;
		fNextFree = newEnd;
		}
	    }
	fData.resize( newSize );
	fUsed.resize( newSize );
	fIndices.resize( newSize );
#if __GNUC__>=3
	fDataStart = fData.begin().base();
	fUsedStart = fUsed.begin().base();
	//printf( "fUsedStart: 0x%08lX\n", (unsigned long)fUsedStart );
	fIndicesStart = fIndices.begin().base();
	//printf( "fIndicesStart: 0x%08lX\n", (unsigned long)fUsedStart );
#else
	fDataStart = fData.begin();
	fUsedStart = fUsed.begin();
	//printf( "fUsedStart: 0x%08lX\n", (unsigned long)fUsedStart );
	fIndicesStart = fIndices.begin();
	//printf( "fIndicesStart: 0x%08lX\n", (unsigned long)fUsedStart );
#endif
	fDataSize = newSize;
	fDataMask = fDataSize-1;
	}
    RebuildHash();
    }

template<class TElementType, class TIndexType>
inline void MLUCIndexedVector<TElementType,TIndexType>::RebuildHash()
    {
    fRebuildHashCnt++;
    unsigned long i, ndx;
    for ( i = 0; i < fHashCnt; i++ )
	fHashes[i].Reset();
    for ( i = 0, ndx = fFirstUsed; i < (fDataCnt+fInvalidDataCnt); i++, ndx=(ndx+1) & fDataMask )
	{
	if ( *(fUsedStart+ndx) )
	    fHashes[ ((*(fIndicesStart+ndx)) / fPreDivisor) & fHashMask ].Add( *(fIndicesStart+ndx), ndx, true );
	}
    for ( i = 0; i < fHashCnt; i++ )
	fHashes[i].ClearRemaining();
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _MLUCINDEXEDVECTOR_HPP_
