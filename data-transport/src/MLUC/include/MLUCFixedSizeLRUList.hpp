#ifndef _MLUCFIXEDSIZELRULIST_HPP_
#define _MLUCFIXEDSIZELRULIST_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCTypes.h"

#if __GNUC__>=3
using namespace std;
#endif

template<class TElementType>
class MLUCFixedSizeLRUList
    {
       protected:
	struct TElementContainer
	    {
		TElementType fContent;
		TElementContainer* fPrevious;
		TElementContainer* fNext;
		void ResetCachedObject()
			{
			//fNext = fPrevious = 0;
			}
	    };
    public:

	class TIterator
	    {
	    public:
		TIterator()
			{
			fList = NULL;
			fElement = NULL;
			}
		TIterator( const TIterator& iter )
			{
			fList = iter.fList;
			fElement = iter.fElement;
			}
		~TIterator()
			{
			}

		TIterator& operator= ( const TIterator& iter )
			{
			fList = iter.fList;
			fElement = iter.fElement;
			return *this;
			}

		operator bool() const
			{
			if ( !fList )
			    return false;
			if ( !fElement )
			    return false;
			if ( fElement->fPrevious == &(fList->fStart) )
			    return false;
			return true;
			}
		TIterator& operator ++()
			{
			if ( fList && fElement )
			    fElement = fElement->fNext;
			return *this;
			}
		TIterator& operator --()
			{
			if ( fList && fElement )
			    fElement = fElement->fPrevious;
			return *this;
			}
		TElementType* operator->()
			{
			if ( !fList )
			    return NULL;
			if ( !fElement )
			    return NULL;
			return &(fElement->fContent);
			}
		TElementType& operator*()
			{
			if ( !fList )
			    return *(TElementType*)NULL;
			if ( !fElement )
			    return *(TElementType*)NULL;
			return fElement->fContent;
			}
		TIterator operator+( int by )
			{
			if ( by<0 )
			    return this->operator-(-by);
			TIterator newIter( *this );
			for ( int i=0; i<by; i++ )
			    newIter.fElement = newIter.fElement->fNext;
			return newIter;
			}
		TIterator operator-( int by )
			{
			if ( by<0 )
			    return this->operator+(-by);
			TIterator newIter( *this );
			for ( int i=0; i<by; i++ )
			    newIter.fElement = newIter.fElement->fPrevious;
			return newIter;
			}
		bool operator==( TIterator const& comp )
			{
			return comp.fList==fList && comp.fElement==fElement;
			}
		bool operator!=( TIterator const& comp )
			{
			return comp.fList!=fList || comp.fElement!=fElement;
			}
	    protected:
		TIterator( MLUCFixedSizeLRUList<TElementType>* list, TElementContainer *element ):
		    fList( list ),
		    fElement( element )
			{
			}

		    friend class MLUCFixedSizeLRUList<TElementType>;

		MLUCFixedSizeLRUList<TElementType>* fList;
		TElementContainer *fElement;

	    };


	MLUCFixedSizeLRUList( unsigned long size ):
	    fSize(0),
	    fUsedCnt(0)
		{
		fStart.fNext = &fEnd;
		fStart.fPrevious = &fStart;
		fEnd.fNext = &fEnd;
		fEnd.fPrevious = &fStart;
		for ( unsigned long ii=0; ii<size; ii++ )
		    {
		    TElementContainer* cont = new TElementContainer();
		    if ( !cont )
			break;
		    cont->fPrevious = &fStart;
		    cont->fNext = fStart.fNext;
		    fStart.fNext = cont;
		    cont->fNext->fPrevious = cont;
		    ++fSize;
		    }
		}
	~MLUCFixedSizeLRUList()
		{
		};

	TIterator Update( TElementType element )
		{
		TIterator iter = Begin(), end = End();
		unsigned long ndx = 0;
		bool found=false;
		while ( iter != end && ndx<fUsedCnt )
		    {
		    if ( *iter == element )
			{
			found = true;
			// Existing item found
			break;
			}
		    ++iter;
		    ++ndx;
		    }
		if ( !found && iter!=end )
		    {
		    // Item not found, but unused item container found
		    // Add/set item
		    *iter = element;
		    fUsedCnt++;
		    found = true;
		    }
		if ( !found )
		    {
		    // Item not found, no unused item container available
		    // Set last item (least recently used) to this one
		    --iter;
		    *iter = element;
		    }
		// Move item to beginning of list
		iter.fElement->fPrevious->fNext = iter.fElement->fNext;
		iter.fElement->fNext->fPrevious = iter.fElement->fPrevious;
		iter.fElement->fPrevious = &fStart;
		iter.fElement->fNext = fStart.fNext;
		fStart.fNext->fPrevious = iter.fElement;
		fStart.fNext = iter.fElement;
		return iter;
		}

	TIterator Begin()
		{
		return TIterator( this, fStart.fNext );
		}
	TIterator End()
		{
		return TIterator( this, &fEnd );
		}

	bool IsEmpty() const
		{
		return fStart.fNext==&fEnd;
		}

	unsigned long GetCnt() const
		{
		return fSize;
		}



    protected:



	TElementContainer fStart;
	TElementContainer fEnd;

	unsigned long fSize;
	unsigned long fUsedCnt;

    };






/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _MLUCFIXEDSIZELRULIST_HPP_
