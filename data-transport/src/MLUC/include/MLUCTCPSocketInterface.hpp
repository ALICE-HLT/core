#ifndef _MLUCTCPSOCKETINTERFACE_HPP_
#define _MLUCTCPSOCKETINTERFACE_HPP_

#include <vector>
#include <MLUCTypes.h>
#include <MLUCMutex.hpp>
#include <sys/socket.h>

class MLUCTCPSocketListener;

class MLUCTCPSocketBaseInterface
    {
    public:

	virtual ~MLUCTCPSocketBaseInterface() {};

	virtual int Bind( unsigned short port ) = 0;

	virtual int Connect( char const* host, short const port, int& newSock ) = 0;
	virtual int Disconnect( int sock ) = 0;

	virtual int AddListener( MLUCTCPSocketListener* listener ) = 0;
	virtual int DelListener( MLUCTCPSocketListener* listener ) = 0;

	virtual int RequestRead( int sock, bool req ) = 0;
	virtual int RequestWrite( int sock, bool req ) = 0;

	virtual int Read( int sock, uint8* data, const unsigned long sz ) = 0;
	virtual int Write( int sock, uint8 const* data, const unsigned long sz ) = 0;

	/*
	  virtual bool SetDelay( int sock, bool delay );
	*/

	virtual int NetworkLoop() = 0;
	virtual bool QuitNetworkLoop( bool wait=true ) = 0;

    protected:

	struct TSocketData
	    {
		int fSocket;
		unsigned fReadRequest;
		unsigned fWriteRequest;
		TSocketData( int sock=-1 ):
		    fSocket(sock),
		    fReadRequest(0),
		    fWriteRequest(0)
			{}
	    };



    };

class MLUCTCPSocketListener
    {
    public:
	virtual int NewConnection( MLUCTCPSocketBaseInterface& /*netint*/, int /*newSock*/, struct sockaddr_in* /*inAddr*/, socklen_t /*inAddrLen*/ ) { return 0; };
	virtual int Writeable( MLUCTCPSocketBaseInterface& /*netint*/, int /*sock*/ ) { return 0; };
	virtual int Readable( MLUCTCPSocketBaseInterface& /*netint*/, int /*sock*/ ) { return 0; };
	virtual int Closed( MLUCTCPSocketBaseInterface& /*netint*/, int /*sock*/ ) { return 0; };
	virtual void Period( MLUCTCPSocketBaseInterface& /*netint*/ ) {};
	virtual ~MLUCTCPSocketListener() {}
    };

template<typename TMutexType,typename TMutexLockerType>
class MLUCTCPSocketInterface: public MLUCTCPSocketBaseInterface
    {
    public:

	MLUCTCPSocketInterface();
	virtual ~MLUCTCPSocketInterface();

	virtual int Bind( unsigned short port );

	virtual int Connect( char const* host, short const port, int& newSock );
	virtual int Disconnect( int sock );

	virtual int AddListener( MLUCTCPSocketListener* listener );
	virtual int DelListener( MLUCTCPSocketListener* listener );

	virtual int RequestRead( int sock, bool req );
	virtual int RequestWrite( int sock, bool req );

	virtual int Read( int sock, uint8* data, const unsigned long sz );
	virtual int Write( int sock, uint8 const* data, const unsigned long sz );

	/*
	  virtual bool SetDelay( int sock, bool delay );
	*/

	virtual int NetworkLoop();
	virtual bool QuitNetworkLoop( bool wait=true );

	void SetQuitVariable( bool* quit );

	void SetSelectWaitInterval( unsigned long microSecs )
		{
		fSelectSleepInterval_us = microSecs;
		}

    protected:

	//typedef TMutexType::TLocker TMutexLocker;
	
	bool FindSocket( int sock, std::vector<MLUCTCPSocketBaseInterface::TSocketData>::iterator& iter );
	bool FindSocket( int sock )
		{
		std::vector<MLUCTCPSocketBaseInterface::TSocketData>::iterator iter;
		return FindSocket( sock, iter );
		}

	virtual int AddListener( MLUCTCPSocketListener* listener, bool lock );
	virtual int DelListener( MLUCTCPSocketListener* listener, bool lock );


	bool fQuitVal;
	bool* fQuit;
	bool fQuitted;

	unsigned long fSelectSleepInterval_us;

	int fListenSocket;
	int fBackLog;

	TMutexType fSocketMutex;
	std::vector<MLUCTCPSocketBaseInterface::TSocketData> fSockets;
	TMutexType fListenerMutex;
	std::vector<MLUCTCPSocketListener*> fListeners;

	bool fInListenerLoop;
	TMutexType fRemoveListenerMutex;
	std::vector<MLUCTCPSocketListener*> fRemoveListeners;

    private:
	MLUCTCPSocketInterface( MLUCTCPSocketInterface<TMutexType,TMutexLockerType> const& ) {}
	MLUCTCPSocketInterface& operator=( MLUCTCPSocketInterface<TMutexType,TMutexLockerType> const& ) { return *this; }


    };

typedef MLUCTCPSocketInterface<MLUCMutex,MLUCMutex::TLocker> MLUCLockedTCPSocketInterface;
typedef MLUCTCPSocketInterface<MLUCDummyMutex,MLUCDummyMutex::TLocker> MLUCUnlockedTCPSocketInterface;


#endif // _MLUCTCPSOCKETINTERFACE_HPP_
