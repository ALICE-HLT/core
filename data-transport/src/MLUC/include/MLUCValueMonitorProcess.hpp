#ifndef _MLUCVALUEMONITORPROCESS_HPP_
#define _MLUCVALUEMONITORPROCESS_HPP_

#include "MLUCValueMonitor.hpp"
#include "MLUCString.hpp"
#include "MLUCLog.hpp"

#include <cstdlib>
#include <errno.h>
#if __GNUC__>=3
#include <sstream>
#else
#include <strstream>
#endif
//#if __GNUC__==2
//#include <sstream>
//#endif

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

class MLUCValueMonitorProcess_Time : public MLUCValueMonitor {
public:
 /**
  * @brief Constructor to enable output to file and stdout.
  * Using this constructor enables output to a file as well as to the 
  * standard output. The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written 
  * to the beginning of the file. 
  * For output to stdout prefixes and suffixes are provided that will be output
  * respectively before and after the monitored value. Another pre-/suffix pair
  * is provided for the averaged value. For the averaged value the number of the 
  * last values over which the average is to be taken can be determined as well. 
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output. 
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param output_prefix A string holding the textual prefix that is to be 
  * output before the measured value.
  * @param output_suffix A string holding the textual suffix that is to be
  * output after the measured value.
  * @param avg_output_prefix A string holding the textual prefix that is to be
  * output before the average of the measured value.
  * @param avg_output_suffix A string holding the textual suffix that is to be
  * output after the average of the measured value.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_Time( const char* filename, 
				const char* fileDescrHeader, 
				const char* output_prefix, 
				const char* output_suffix, 
				const char* avg_output_prefix, 
				const char* avg_output_suffix, 
				unsigned int average_items,
				int pid) :
    MLUCValueMonitor( filename, 
		      fileDescrHeader, 
		      output_prefix, 
		      output_suffix, 
		      avg_output_prefix, 
		      avg_output_suffix, 
		      average_items )
  {
    Init(pid);
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file. 
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written 
  * to the beginning of the file. 
  * For the averaged value the number of the last values over which the average 
  * is to be taken can be determined as well. 
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output. 
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_Time( const char* output_prefix, 
				const char* output_suffix, 
				const char* avg_output_prefix, 
				const char* avg_output_suffix, 
				unsigned int average_items,
				int pid):
    MLUCValueMonitor( output_prefix, 
		      output_suffix, 
		      avg_output_prefix, 
		      avg_output_suffix, 
		      average_items )
  {
    Init(pid);
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file. 
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written 
  * to the beginning of the file. 
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well. 
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output. 
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file. 
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_Time( const char* filename, 
				const char* fileDescrHeader, 
				unsigned int average_items, 
				int pid):
    MLUCValueMonitor( filename, fileDescrHeader, average_items )
  {
    Init(pid);
  };

 /**
  * @brief Constructor with no output.
  * For the averaged value the number of the last values over which the average 
  * is to be taken can be determined as well. 
  *
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_Time( unsigned int average_items, 
				int pid):
    MLUCValueMonitor( average_items )
  {
    Init(pid);
  };
  
  int GetValue( uint64& MeasuredValue );

protected:
  
  /**
  * Flag for the first readout. In the constructor this flag is set to true, 
  * since one needs at least two values to measure something. After the first
  *  readout the flag is set to false.
  */
  bool fFirstReadOut;

  /**
   * The ID of the process to be monitored.
   */
  int fpid;

  /**
  * Contains the full pathname of the monitoring file. (/proc/pid/file)
  */
#if __GNUC__>=3
  ostringstream fFilename;
#else
  ostrstream fFilename;
#endif

private:
  /**
  * Will be executed in each constructor. This is just for convenience
  */
  void Init(int );
};

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

class MLUCValueMonitorProcess_CpuUsage : public MLUCValueMonitor {
public:
 /**
  * @brief Constructor to enable output to file and stdout.
  * Using this constructor enables output to a file as well as to the 
  * standard output. The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written 
  * to the beginning of the file. 
  * For output to stdout prefixes and suffixes are provided that will be output
  * respectively before and after the monitored value. Another pre-/suffix pair
  * is provided for the averaged value. For the averaged value the number of the 
  * last values over which the average is to be taken can be determined as well. 
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output. 
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param output_prefix A string holding the textual prefix that is to be 
  * output before the measured value.
  * @param output_suffix A string holding the textual suffix that is to be
  * output after the measured value.
  * @param avg_output_prefix A string holding the textual prefix that is to be
  * output before the average of the measured value.
  * @param avg_output_suffix A string holding the textual suffix that is to be
  * output after the average of the measured value.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_CpuUsage( const char* filename, 
				const char* fileDescrHeader, 
				const char* output_prefix, 
				const char* output_suffix, 
				const char* avg_output_prefix, 
				const char* avg_output_suffix, 
				unsigned int average_items,
				int pid) :
    MLUCValueMonitor( filename, 
		      fileDescrHeader, 
		      output_prefix, 
		      output_suffix, 
		      avg_output_prefix, 
		      avg_output_suffix, 
		      average_items )
  {
    Init(pid);
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file. 
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written 
  * to the beginning of the file. 
  * For the averaged value the number of the last values over which the average 
  * is to be taken can be determined as well. 
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output. 
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_CpuUsage( const char* output_prefix, 
				const char* output_suffix, 
				const char* avg_output_prefix, 
				const char* avg_output_suffix, 
				unsigned int average_items,
				int pid):
    MLUCValueMonitor( output_prefix, 
		      output_suffix, 
		      avg_output_prefix, 
		      avg_output_suffix, 
		      average_items ) 
  {
    Init(pid);
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file. 
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written 
  * to the beginning of the file. 
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well. 
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output. 
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file. 
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_CpuUsage( const char* filename, 
				const char* fileDescrHeader, 
				unsigned int average_items, 
				int pid):
    MLUCValueMonitor( filename, fileDescrHeader, average_items )
  {
    Init(pid);
  };

 /**
  * @brief Constructor with no output.
  * For the averaged value the number of the last values over which the average 
  * is to be taken can be determined as well. 
  *
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_CpuUsage( unsigned int average_items, 
				int pid):
    MLUCValueMonitor( average_items )
  {
    Init(pid);
  };
  
  int GetValue( uint64& MeasuredValue );

protected:
  
  /**
  * Flag for the first readout. In the constructor this flag is set to true, 
  * since one needs at least two values to measure something. After the first
  *  readout the flag is set to false.
  */
  bool fFirstReadOut;

  /**
   * The ID of the process to be monitored.
   */
  int fpid;

  /**
  * Contains the full pathname of the monitoring file. (/proc/pid/file)
  */
#if __GNUC__>=3
  ostringstream fFilename;
#else
  ostrstream fFilename;
#endif

  unsigned long fOldTotalCpuTime, fOldProcessTime;
private:
  /**
  * Will be executed in each constructor. This is just for convenience
  */
  void Init(int pid);
};

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

class MLUCValueMonitorProcess_CpuUsage_User : public MLUCValueMonitor {
public:
 /**
  * @brief Constructor to enable output to file and stdout.
  * Using this constructor enables output to a file as well as to the 
  * standard output. The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written 
  * to the beginning of the file. 
  * For output to stdout prefixes and suffixes are provided that will be output
  * respectively before and after the monitored value. Another pre-/suffix pair
  * is provided for the averaged value. For the averaged value the number of the 
  * last values over which the average is to be taken can be determined as well. 
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output. 
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param output_prefix A string holding the textual prefix that is to be 
  * output before the measured value.
  * @param output_suffix A string holding the textual suffix that is to be
  * output after the measured value.
  * @param avg_output_prefix A string holding the textual prefix that is to be
  * output before the average of the measured value.
  * @param avg_output_suffix A string holding the textual suffix that is to be
  * output after the average of the measured value.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_CpuUsage_User( const char* filename, 
				const char* fileDescrHeader, 
				const char* output_prefix, 
				const char* output_suffix, 
				const char* avg_output_prefix, 
				const char* avg_output_suffix, 
				unsigned int average_items,
				int pid) :
    MLUCValueMonitor( filename, 
		      fileDescrHeader, 
		      output_prefix, 
		      output_suffix, 
		      avg_output_prefix, 
		      avg_output_suffix, 
		      average_items )
  {
    Init(pid);
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file. 
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written 
  * to the beginning of the file. 
  * For the averaged value the number of the last values over which the average 
  * is to be taken can be determined as well. 
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output. 
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_CpuUsage_User( const char* output_prefix, 
				const char* output_suffix, 
				const char* avg_output_prefix, 
				const char* avg_output_suffix, 
				unsigned int average_items,
				int pid):
    MLUCValueMonitor( output_prefix, 
		      output_suffix, 
		      avg_output_prefix, 
		      avg_output_suffix, 
		      average_items )
  {
    Init(pid);
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file. 
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written 
  * to the beginning of the file. 
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well. 
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output. 
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file. 
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_CpuUsage_User( const char* filename, 
				const char* fileDescrHeader, 
				unsigned int average_items, 
				int pid):
    MLUCValueMonitor( filename, fileDescrHeader, average_items )
  {
    Init(pid);
  };

 /**
  * @brief Constructor with no output.
  * For the averaged value the number of the last values over which the average 
  * is to be taken can be determined as well. 
  *
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_CpuUsage_User( unsigned int average_items, 
				int pid):
    MLUCValueMonitor( average_items )
  {
    Init(pid);
  };
  
  int GetValue( uint64& MeasuredValue );

protected:
  
  /**
  * Flag for the first readout. In the constructor this flag is set to true, 
  * since one needs at least two values to measure something. After the first
  *  readout the flag is set to false.
  */
  bool fFirstReadOut;

  /**
   * The ID of the process to be monitored.
   */
  int fpid;

  /**
  * Contains the full pathname of the monitoring file. (/proc/pid/file)
  */
#if __GNUC__>=3
  ostringstream fFilename;
#else
  ostrstream fFilename;
#endif

  unsigned long fOldTotalCpuTime, fOldProcessTime;
private:
  /**
  * Will be executed in each constructor. This is just for convenience
  */
  void Init(int pid);
};

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

class MLUCValueMonitorProcess_CpuUsage_System : public MLUCValueMonitor {
public:
 /**
  * @brief Constructor to enable output to file and stdout.
  * Using this constructor enables output to a file as well as to the 
  * standard output. The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written 
  * to the beginning of the file. 
  * For output to stdout prefixes and suffixes are provided that will be output
  * respectively before and after the monitored value. Another pre-/suffix pair
  * is provided for the averaged value. For the averaged value the number of the 
  * last values over which the average is to be taken can be determined as well. 
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output. 
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param output_prefix A string holding the textual prefix that is to be 
  * output before the measured value.
  * @param output_suffix A string holding the textual suffix that is to be
  * output after the measured value.
  * @param avg_output_prefix A string holding the textual prefix that is to be
  * output before the average of the measured value.
  * @param avg_output_suffix A string holding the textual suffix that is to be
  * output after the average of the measured value.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_CpuUsage_System( const char* filename, 
				const char* fileDescrHeader, 
				const char* output_prefix, 
				const char* output_suffix, 
				const char* avg_output_prefix, 
				const char* avg_output_suffix, 
				unsigned int average_items,
				int pid) :
    MLUCValueMonitor( filename, 
		      fileDescrHeader, 
		      output_prefix, 
		      output_suffix, 
		      avg_output_prefix, 
		      avg_output_suffix, 
		      average_items )
  {
    Init(pid);
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file. 
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written 
  * to the beginning of the file. 
  * For the averaged value the number of the last values over which the average 
  * is to be taken can be determined as well. 
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output. 
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_CpuUsage_System( const char* output_prefix, 
				const char* output_suffix, 
				const char* avg_output_prefix, 
				const char* avg_output_suffix, 
				unsigned int average_items,
				int pid):
    MLUCValueMonitor( output_prefix, 
		      output_suffix, 
		      avg_output_prefix, 
		      avg_output_suffix, 
		      average_items )
  {
    Init(pid);
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file. 
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written 
  * to the beginning of the file. 
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well. 
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output. 
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file. 
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_CpuUsage_System( const char* filename, 
				const char* fileDescrHeader, 
				unsigned int average_items, 
				int pid):
    MLUCValueMonitor( filename, fileDescrHeader, average_items )
  {
    Init(pid);
  };

 /**
  * @brief Constructor with no output.
  * For the averaged value the number of the last values over which the average 
  * is to be taken can be determined as well. 
  *
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_CpuUsage_System( unsigned int average_items, 
				int pid):
    MLUCValueMonitor( average_items )
  {
    Init(pid);
  };
  
  int GetValue( uint64& MeasuredValue );

protected:
  
  /**
  * Flag for the first readout. In the constructor this flag is set to true, 
  * since one needs at least two values to measure something. After the first
  *  readout the flag is set to false.
  */
  bool fFirstReadOut;

  /**
   * The ID of the process to be monitored.
   */
  int fpid;

  /**
  * Contains the full pathname of the monitoring file. (/proc/pid/file)
  */
#if __GNUC__>=3
  ostringstream fFilename;
#else
  ostrstream fFilename;
#endif

  unsigned long fOldTotalCpuTime, fOldProcessTime;
private:
  /**
  * Will be executed in each constructor. This is just for convenience
  */
  void Init(int pid);
};

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

class MLUCValueMonitorProcess_Size : public MLUCValueMonitor {
public:
 /**
  * @brief Constructor to enable output to file and stdout.
  * Using this constructor enables output to a file as well as to the 
  * standard output. The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written 
  * to the beginning of the file. 
  * For output to stdout prefixes and suffixes are provided that will be output
  * respectively before and after the monitored value. Another pre-/suffix pair
  * is provided for the averaged value. For the averaged value the number of the 
  * last values over which the average is to be taken can be determined as well. 
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output. 
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param output_prefix A string holding the textual prefix that is to be 
  * output before the measured value.
  * @param output_suffix A string holding the textual suffix that is to be
  * output after the measured value.
  * @param avg_output_prefix A string holding the textual prefix that is to be
  * output before the average of the measured value.
  * @param avg_output_suffix A string holding the textual suffix that is to be
  * output after the average of the measured value.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_Size( const char* filename, 
				const char* fileDescrHeader, 
				const char* output_prefix, 
				const char* output_suffix, 
				const char* avg_output_prefix, 
				const char* avg_output_suffix, 
				unsigned int average_items,
				int pid) :
    MLUCValueMonitor( filename, 
		      fileDescrHeader, 
		      output_prefix, 
		      output_suffix, 
		      avg_output_prefix, 
		      avg_output_suffix, 
		      average_items )
  {
    Init(pid);
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file. 
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written 
  * to the beginning of the file. 
  * For the averaged value the number of the last values over which the average 
  * is to be taken can be determined as well. 
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output. 
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_Size( const char* output_prefix, 
				const char* output_suffix, 
				const char* avg_output_prefix, 
				const char* avg_output_suffix, 
				unsigned int average_items,
				int pid):
    MLUCValueMonitor( output_prefix, 
		      output_suffix, 
		      avg_output_prefix, 
		      avg_output_suffix, 
		      average_items )
  {
    Init(pid);
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file. 
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written 
  * to the beginning of the file. 
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well. 
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output. 
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file. 
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_Size( const char* filename, 
				const char* fileDescrHeader, 
				unsigned int average_items, 
				int pid):
    MLUCValueMonitor( filename, fileDescrHeader, average_items )
  {
    Init(pid);
  };

 /**
  * @brief Constructor with no output.
  * For the averaged value the number of the last values over which the average 
  * is to be taken can be determined as well. 
  *
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_Size( unsigned int average_items, 
				int pid):
    MLUCValueMonitor( average_items )
  {
    Init(pid);
  };
  
  int GetValue( uint64& MeasuredValue );

protected:
  /** 
   * Contains the size of a memory page in KBytes.
   */
  int fPageSize;

  /**
   * The ID of the process to be monitored.
   */
  int fpid;

  /**
  * Contains the full pathname of the monitoring file. (/proc/pid/file)
  */
#if __GNUC__>=3
  ostringstream fFilename;
#else
  ostrstream fFilename;
#endif

private:
  /**
  * Will be executed in each constructor. This is just for convenience
  */
  void Init(int pid);
};

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

class MLUCValueMonitorProcess_Mem_Resident : public MLUCValueMonitor {
public:
 /**
  * @brief Constructor to enable output to file and stdout.
  * Using this constructor enables output to a file as well as to the 
  * standard output. The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written 
  * to the beginning of the file. 
  * For output to stdout prefixes and suffixes are provided that will be output
  * respectively before and after the monitored value. Another pre-/suffix pair
  * is provided for the averaged value. For the averaged value the number of the 
  * last values over which the average is to be taken can be determined as well. 
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output. 
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param output_prefix A string holding the textual prefix that is to be 
  * output before the measured value.
  * @param output_suffix A string holding the textual suffix that is to be
  * output after the measured value.
  * @param avg_output_prefix A string holding the textual prefix that is to be
  * output before the average of the measured value.
  * @param avg_output_suffix A string holding the textual suffix that is to be
  * output after the average of the measured value.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_Mem_Resident( const char* filename, 
				const char* fileDescrHeader, 
				const char* output_prefix, 
				const char* output_suffix, 
				const char* avg_output_prefix, 
				const char* avg_output_suffix, 
				unsigned int average_items,
				int pid) :
    MLUCValueMonitor( filename, 
		      fileDescrHeader, 
		      output_prefix, 
		      output_suffix, 
		      avg_output_prefix, 
		      avg_output_suffix, 
		      average_items )
  {
    Init(pid);
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file. 
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written 
  * to the beginning of the file. 
  * For the averaged value the number of the last values over which the average 
  * is to be taken can be determined as well. 
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output. 
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_Mem_Resident( const char* output_prefix, 
				const char* output_suffix, 
				const char* avg_output_prefix, 
				const char* avg_output_suffix, 
				unsigned int average_items,
				int pid):
    MLUCValueMonitor( output_prefix, 
		      output_suffix, 
		      avg_output_prefix, 
		      avg_output_suffix, 
		      average_items )
  {
    Init(pid);
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file. 
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written 
  * to the beginning of the file. 
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well. 
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output. 
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file. 
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_Mem_Resident( const char* filename, 
				const char* fileDescrHeader, 
				unsigned int average_items, 
				int pid):
    MLUCValueMonitor( filename, fileDescrHeader, average_items )
  {
    Init(pid);
  };

 /**
  * @brief Constructor with no output.
  * For the averaged value the number of the last values over which the average 
  * is to be taken can be determined as well. 
  *
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_Mem_Resident( unsigned int average_items, 
				int pid):
    MLUCValueMonitor( average_items )
  {
    Init(pid);
  };
  
  int GetValue( uint64& MeasuredValue );

protected:
  /** 
   * Contains the size of a memory page in KBytes.
   */
  int fPageSize;

  /**
   * The ID of the process to be monitored.
   */
  int fpid;

  /**
  * Contains the full pathname of the monitoring file. (/proc/pid/file)
  */
#if __GNUC__>=3
  ostringstream fFilename;
#else
  ostrstream fFilename;
#endif

private:
  /**
  * Will be executed in each constructor. This is just for convenience
  */
  void Init(int pid);
};

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

class MLUCValueMonitorProcess_Mem_Shared : public MLUCValueMonitor {
public:
 /**
  * @brief Constructor to enable output to file and stdout.
  * Using this constructor enables output to a file as well as to the 
  * standard output. The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written 
  * to the beginning of the file. 
  * For output to stdout prefixes and suffixes are provided that will be output
  * respectively before and after the monitored value. Another pre-/suffix pair
  * is provided for the averaged value. For the averaged value the number of the 
  * last values over which the average is to be taken can be determined as well. 
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output. 
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param output_prefix A string holding the textual prefix that is to be 
  * output before the measured value.
  * @param output_suffix A string holding the textual suffix that is to be
  * output after the measured value.
  * @param avg_output_prefix A string holding the textual prefix that is to be
  * output before the average of the measured value.
  * @param avg_output_suffix A string holding the textual suffix that is to be
  * output after the average of the measured value.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_Mem_Shared( const char* filename, 
				const char* fileDescrHeader, 
				const char* output_prefix, 
				const char* output_suffix, 
				const char* avg_output_prefix, 
				const char* avg_output_suffix, 
				unsigned int average_items,
				int pid) :
    MLUCValueMonitor( filename, 
		      fileDescrHeader, 
		      output_prefix, 
		      output_suffix, 
		      avg_output_prefix, 
		      avg_output_suffix, 
		      average_items )
  {
    Init(pid);
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file. 
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written 
  * to the beginning of the file. 
  * For the averaged value the number of the last values over which the average 
  * is to be taken can be determined as well. 
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output. 
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_Mem_Shared( const char* output_prefix, 
				const char* output_suffix, 
				const char* avg_output_prefix, 
				const char* avg_output_suffix, 
				unsigned int average_items,
				int pid):
    MLUCValueMonitor( output_prefix, 
		      output_suffix, 
		      avg_output_prefix, 
		      avg_output_suffix, 
		      average_items )
  {
    Init(pid);
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file. 
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written 
  * to the beginning of the file. 
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well. 
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output. 
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file. 
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_Mem_Shared( const char* filename, 
				const char* fileDescrHeader, 
				unsigned int average_items, 
				int pid):
    MLUCValueMonitor( filename, fileDescrHeader, average_items )
  {
    Init(pid);
  };

 /**
  * @brief Constructor with no output.
  * For the averaged value the number of the last values over which the average 
  * is to be taken can be determined as well. 
  *
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_Mem_Shared( unsigned int average_items, 
				int pid):
    MLUCValueMonitor( average_items )
  {
    Init(pid);
  };
  
  int GetValue( uint64& MeasuredValue );

protected:
  /** 
   * Contains the size of a memory page in KBytes.
   */
  int fPageSize;

  /**
   * The ID of the process to be monitored.
   */
  int fpid;

  /**
  * Contains the full pathname of the monitoring file. (/proc/pid/file)
  */
#if __GNUC__>=3
  ostringstream fFilename;
#else
  ostrstream fFilename;
#endif

private:
  /**
  * Will be executed in each constructor. This is just for convenience
  */
  void Init(int pid);
};

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

class MLUCValueMonitorProcess_Mem_Stack : public MLUCValueMonitor {
public:
 /**
  * @brief Constructor to enable output to file and stdout.
  * Using this constructor enables output to a file as well as to the 
  * standard output. The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written 
  * to the beginning of the file. 
  * For output to stdout prefixes and suffixes are provided that will be output
  * respectively before and after the monitored value. Another pre-/suffix pair
  * is provided for the averaged value. For the averaged value the number of the 
  * last values over which the average is to be taken can be determined as well. 
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output. 
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param output_prefix A string holding the textual prefix that is to be 
  * output before the measured value.
  * @param output_suffix A string holding the textual suffix that is to be
  * output after the measured value.
  * @param avg_output_prefix A string holding the textual prefix that is to be
  * output before the average of the measured value.
  * @param avg_output_suffix A string holding the textual suffix that is to be
  * output after the average of the measured value.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_Mem_Stack( const char* filename, 
				const char* fileDescrHeader, 
				const char* output_prefix, 
				const char* output_suffix, 
				const char* avg_output_prefix, 
				const char* avg_output_suffix, 
				unsigned int average_items,
				int pid) :
    MLUCValueMonitor( filename, 
		      fileDescrHeader, 
		      output_prefix, 
		      output_suffix, 
		      avg_output_prefix, 
		      avg_output_suffix, 
		      average_items )
  {
    Init(pid);
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file. 
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written 
  * to the beginning of the file. 
  * For the averaged value the number of the last values over which the average 
  * is to be taken can be determined as well. 
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output. 
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_Mem_Stack( const char* output_prefix, 
				const char* output_suffix, 
				const char* avg_output_prefix, 
				const char* avg_output_suffix, 
				unsigned int average_items,
				int pid):
    MLUCValueMonitor( output_prefix, 
		      output_suffix, 
		      avg_output_prefix, 
		      avg_output_suffix, 
		      average_items )
  {
    Init(pid);
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file. 
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written 
  * to the beginning of the file. 
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well. 
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output. 
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file. 
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_Mem_Stack( const char* filename, 
				const char* fileDescrHeader, 
				unsigned int average_items, 
				int pid):
    MLUCValueMonitor( filename, fileDescrHeader, average_items )
  {
    Init(pid);
  };

 /**
  * @brief Constructor with no output.
  * For the averaged value the number of the last values over which the average 
  * is to be taken can be determined as well. 
  *
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_Mem_Stack( unsigned int average_items, 
				int pid):
    MLUCValueMonitor( average_items )
  {
    Init(pid);
  };
  
  int GetValue( uint64& MeasuredValue );

protected:
  /** 
   * Contains the size of a memory page in KBytes.
   */
  int fPageSize;

  /**
   * The ID of the process to be monitored.
   */
  int fpid;

  /**
  * Contains the full pathname of the monitoring file. (/proc/pid/file)
  */
#if __GNUC__>=3
  ostringstream fFilename;
#else
  ostrstream fFilename;
#endif

private:
  /**
  * Will be executed in each constructor. This is just for convenience
  */
  void Init(int pid);
};

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

class MLUCValueMonitorProcess_Mem_Library : public MLUCValueMonitor {
public:
 /**
  * @brief Constructor to enable output to file and stdout.
  * Using this constructor enables output to a file as well as to the 
  * standard output. The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written 
  * to the beginning of the file. 
  * For output to stdout prefixes and suffixes are provided that will be output
  * respectively before and after the monitored value. Another pre-/suffix pair
  * is provided for the averaged value. For the averaged value the number of the 
  * last values over which the average is to be taken can be determined as well. 
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output. 
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param output_prefix A string holding the textual prefix that is to be 
  * output before the measured value.
  * @param output_suffix A string holding the textual suffix that is to be
  * output after the measured value.
  * @param avg_output_prefix A string holding the textual prefix that is to be
  * output before the average of the measured value.
  * @param avg_output_suffix A string holding the textual suffix that is to be
  * output after the average of the measured value.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_Mem_Library( const char* filename, 
				const char* fileDescrHeader, 
				const char* output_prefix, 
				const char* output_suffix, 
				const char* avg_output_prefix, 
				const char* avg_output_suffix, 
				unsigned int average_items,
				int pid) :
    MLUCValueMonitor( filename, 
		      fileDescrHeader, 
		      output_prefix, 
		      output_suffix, 
		      avg_output_prefix, 
		      avg_output_suffix, 
		      average_items )
  {
    Init(pid);
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file. 
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written 
  * to the beginning of the file. 
  * For the averaged value the number of the last values over which the average 
  * is to be taken can be determined as well. 
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output. 
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_Mem_Library( const char* output_prefix, 
				const char* output_suffix, 
				const char* avg_output_prefix, 
				const char* avg_output_suffix, 
				unsigned int average_items,
				int pid):
    MLUCValueMonitor( output_prefix, 
		      output_suffix, 
		      avg_output_prefix, 
		      avg_output_suffix, 
		      average_items )
  {
    Init(pid);
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file. 
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written 
  * to the beginning of the file. 
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well. 
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output. 
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file. 
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_Mem_Library( const char* filename, 
				const char* fileDescrHeader, 
				unsigned int average_items, 
				int pid):
    MLUCValueMonitor( filename, fileDescrHeader, average_items )
  {
    Init(pid);
  };

 /**
  * @brief Constructor with no output.
  * For the averaged value the number of the last values over which the average 
  * is to be taken can be determined as well. 
  *
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_Mem_Library( unsigned int average_items, 
				int pid):
    MLUCValueMonitor( average_items )
  {
    Init(pid);
  };
  
  int GetValue( uint64& MeasuredValue );

protected:
  /** 
   * Contains the size of a memory page in KBytes.
   */
  int fPageSize;

  /**
   * The ID of the process to be monitored.
   */
  int fpid;

  /**
  * Contains the full pathname of the monitoring file. (/proc/pid/file)
  */
#if __GNUC__>=3
  ostringstream fFilename;
#else
  ostrstream fFilename;
#endif

private:
  /**
  * Will be executed in each constructor. This is just for convenience
  */
  void Init(int pid);
};

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

class MLUCValueMonitorProcess_Mem_Dirty : public MLUCValueMonitor {
public:
 /**
  * @brief Constructor to enable output to file and stdout.
  * Using this constructor enables output to a file as well as to the 
  * standard output. The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written 
  * to the beginning of the file. 
  * For output to stdout prefixes and suffixes are provided that will be output
  * respectively before and after the monitored value. Another pre-/suffix pair
  * is provided for the averaged value. For the averaged value the number of the 
  * last values over which the average is to be taken can be determined as well. 
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output. 
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param output_prefix A string holding the textual prefix that is to be 
  * output before the measured value.
  * @param output_suffix A string holding the textual suffix that is to be
  * output after the measured value.
  * @param avg_output_prefix A string holding the textual prefix that is to be
  * output before the average of the measured value.
  * @param avg_output_suffix A string holding the textual suffix that is to be
  * output after the average of the measured value.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_Mem_Dirty( const char* filename, 
				const char* fileDescrHeader, 
				const char* output_prefix, 
				const char* output_suffix, 
				const char* avg_output_prefix, 
				const char* avg_output_suffix, 
				unsigned int average_items,
				int pid) :
    MLUCValueMonitor( filename, 
		      fileDescrHeader, 
		      output_prefix, 
		      output_suffix, 
		      avg_output_prefix, 
		      avg_output_suffix, 
		      average_items )
  {
    Init(pid);
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file. 
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written 
  * to the beginning of the file. 
  * For the averaged value the number of the last values over which the average 
  * is to be taken can be determined as well. 
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output. 
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_Mem_Dirty( const char* output_prefix, 
				const char* output_suffix, 
				const char* avg_output_prefix, 
				const char* avg_output_suffix, 
				unsigned int average_items,
				int pid):
    MLUCValueMonitor( output_prefix, 
		      output_suffix, 
		      avg_output_prefix, 
		      avg_output_suffix, 
		      average_items )
  {
    Init(pid);
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file. 
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written 
  * to the beginning of the file. 
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well. 
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output. 
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file. 
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_Mem_Dirty( const char* filename, 
				const char* fileDescrHeader, 
				unsigned int average_items, 
				int pid):
    MLUCValueMonitor( filename, fileDescrHeader, average_items )
  {
    Init(pid);
  };

 /**
  * @brief Constructor with no output.
  * For the averaged value the number of the last values over which the average 
  * is to be taken can be determined as well. 
  *
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  * @param pid The ID of the process to be monitored.
  */
  MLUCValueMonitorProcess_Mem_Dirty( unsigned int average_items, 
				int pid):
    MLUCValueMonitor( average_items )
  {
    Init(pid);
  };
  
  int GetValue( uint64& MeasuredValue );

protected:
  /**
   * The ID of the process to be monitored.
   */
  int fpid;

  /**
  * Contains the full pathname of the monitoring file. (/proc/pid/file)
  */
#if __GNUC__>=3
  ostringstream fFilename;
#else
  ostrstream fFilename;
#endif

private:
  /**
  * Will be executed in each constructor. This is just for convenience
  */
  void Init(int pid);
};

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------


#endif // _MLUCVALUEMONITORPROCESS_HPP_
