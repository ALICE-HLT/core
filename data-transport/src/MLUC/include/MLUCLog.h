#ifndef _MLUCLOG_H_
#define _MLUCLOG_H_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#ifdef __cplusplus
extern "C" {
#endif

#define MLUCLOG_LVL_NONE 0x00U
#define MLUCLOG_LVL_BENCHMARK 0x01U
#define MLUCLOG_LVL_DEBUG 0x02U
#define MLUCLOG_LVL_INFO 0x04U
#define MLUCLOG_LVL_WARNING 0x08U
#define MLUCLOG_LVL_ERROR 0x10U
#define MLUCLOG_LVL_FATAL 0x20U
#define MLUCLOG_LVL_IMPORTANT 0x40U
#define MLUCLOG_LVL_PRIMARY 0x80U
#define MLUCLOG_LVL_ALL 0x7FU

#define MLUCLOG_OUT_STDOUT 0x01
#define MLUCLOG_OUT_STDERR 0x02
#define MLUCLOG_OUT_FILE 0x03

#define CLOG( lvl, origin, keyword, ... ) if (gCLogLevel & lvl) { LogMsg( lvl, origin, keyword, __FILE__, __LINE__, __DATE__,  __TIME__, __VA_ARGS__ ); }
    /*#define CLOG( lvl, origin, keyword, msg, s... ) if (gCLogLevel & lvl) { LogMsg( lvl, origin, keyword, __FILE__, __LINE__, __DATE__,  __TIME__, msg, s ); }*/

#define SETCLOGLEVEL( lvl ) { gCLogLevel = lvl; }

#define SETCLOGOUTPUT( type, param1, param2 ) SetLogOutput( type, param1, param2 )
#define REMOVECLOGOUTPUT( type ) RemoveLogOutput( type )


    void LogMsg( unsigned logLvl, const char* origin, const char* keyword, const char* file, int line, 
		 const char* compile_date, const char* compile_time, char* fmt, ... );

    void SetLogOutput( unsigned type, const void* param1, const void* param2 );
    void RemoveLogOutput( unsigned type );


    extern unsigned gCLogLevel;



#ifdef __cplusplus
}
#endif

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif /* _MLUCLOG_H_ */
