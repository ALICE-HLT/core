#ifndef _MLUCCRC_H_
#define _MLUCCRC_H_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCTypes.h"

/**
 * This class calculates CRC checksums over given data.
 *
 * @short A CRC calculating class.
 * @author $Author$
 * @version $Id$
 */
class MLUCCRC
    {
    public:
	
	/**
	 * Constructor.
	 * Initializes the objects internal tables.
	 */
	MLUCCRC()
		{
		int i, j;  long crc;
		for(i=0; i <256; i++)
		    {
		    crc = (i << 24); /* Put i into MSB */
		    for(j=0; j < 8; j++) /* 8 reductions */
			crc = (crc << 1) ^ ((crc & 0x80000000L) ? 0x04c11db7L : 0);
		    fCRC32Table[i] = crc;
		    }
		}

	/**
	 * Add the byte passed in ch to to the already calculated crc.
	 *
	 * @param ch A byte value to add to the crc checksum.
	 * @param crc The checksum calculated so far. 
	 * @return The newly calculated checksum including the value of ch.
	 */
	uint32 CRC32( uint8 ch, uint32 crc )
		{
		crc = fCRC32Table[((crc >> 24) ^ ch) & 255] ^ (crc << 8);
		return crc & 0xFFFFFFFF;
		}
	
	/**
	 * Calculate a checksum over the given data (with the passed size.)
	 *
	 * @param data A pointer to the data that is to be crc'ed.
	 * @param dataSize The number of bytes that are to be crc'ed.
	 * @param crc An optional previously calculated crc to use. When using this, 
	 * the resulting checksum will include both this passed offset as well as the
	 * data passed to this function.
	 * @return A 32 bit checksum over the given data (optionally including the given start crc.)
	 */
	uint32 MakeCRC32( void* data, unsigned long dataSize, uint32 crc = 0 )
		{
		unsigned long i;
		for ( i = 0; i < dataSize; i++, ((uint8*)data)++ )
		    crc = CRC32( *(uint8*)data, crc );
		return crc;
		}

	/**
	 * Calculate a checksum over the given data (with the passed size) with a given step size
	 *
	 * @param data A pointer to the data that is to be crc'ed.
	 * @param dataSize The number of bytes that are to be crc'ed.
	 * @param step Determines in which strides to use bytes for the crc.
	 * @param crc A previously calculated crc to use. When using this, 
	 * the resulting checksum will include both this passed offset as well as the
	 * data passed to this function.
	 * @param currentStep The current step offset at the beginning (input) and end (output) of the function.
	 * @return A 32 bit checksum over the given data (optionally including the given start crc.)
	 */
	uint32 MakeCRC32Stepped( void* data, unsigned long dataSize, unsigned long step, uint32 crc, unsigned long& currentStep )
		{
		unsigned long i;
		for ( i = 0; i < dataSize; i++, ((uint8*)data)++, currentStep++ )
		    {
		    if ( currentStep % step )
			crc = CRC32( *(uint8*)data, crc );
		    }
		return crc;
		}

	/**
	 * Calculate a checksum over the given data (with the passed size) with a given step size
	 *
	 * @param data A pointer to the data that is to be crc'ed.
	 * @param dataSize The number of bytes that are to be crc'ed.
	 * @param step Determines in which strides to use bytes for the crc.
	 * @return A 32 bit checksum over the given data (optionally including the given start crc.)
	 */
	uint32 MakeCRC32Stepped( void* data, unsigned long dataSize, unsigned long step )
		{
		unsigned long cStep;
		return MakeCRC32Stepped( data, dataSize, step, 0, cStep );
		}


	/**
	 * Copy some data while calculating a crc checksum over it.
	 *
	 * @param dest Pointer to the data's destination.
	 * @param src  Pointer to the data's source
	 * @param size The amount of data to copy/crc.
	 * @param crc An optional previously calculated crc to use. When using this, 
	 * the resulting checksum will include both this passed offset as well as the
	 * data passed to this function.
	 * @return A 32 bit checksum over the given data (optionally including the given start crc.)
	 */
	uint32 MemcpyCRC( void* dest, void* src, unsigned long size, uint32 crc = 0 )
		{
		register unsigned long i;
		register uint8 tmp;
		for ( i = 0; i < size; i++, ((uint8*)src)++, ((uint8*)dest)++ )
		    {
		    tmp = *(uint8*)src;
		    crc = CRC32( tmp, crc );
		    *(uint8*)dest = tmp;
		    }
		return crc;
		}

	/**
	 * Copy some data while calculating a crc checksum using every n'th byte over it.
	 *
	 * @param dest Pointer to the data's destination.
	 * @param src  Pointer to the data's source
	 * @param size The amount of data to copy/crc.
	 * @param step Determines in which strides to use bytes for the crc.
	 * @param crc A previously calculated crc to use. When using this, 
	 * the resulting checksum will include both this passed offset as well as the
	 * data passed to this function.
	 * @param currentStep The current step offset at the beginning (input) and end (output) of the function.
	 * @return A 32 bit checksum over the given data including the given start crc.
	 */
	uint32 MemcpyCRCStepped( void* dest, void* src, unsigned long size, long step, uint32 crc, unsigned long& currentStep )
		{
		register unsigned long i;
		register uint8 tmp;
		for ( i = 0; i < size; i++, ((uint8*)src)++, ((uint8*)dest)++, currentStep++ )
		    {
		    tmp = *(uint8*)src;
		    if ( currentStep % step )
			crc = CRC32( tmp, crc );
		    *(uint8*)dest = tmp;
		    }
		return crc;
		}
	
	/**
	 * Copy some data while calculating a crc checksum using every n'th byte over it.
	 *
	 * @param dest Pointer to the data's destination.
	 * @param src  Pointer to the data's source
	 * @param size The amount of data to copy/crc.
	 * @param step Determines in which strides to use bytes for the crc.
	 * @return A 32 bit checksum over the given data.
	 */
	uint32 MemcpyCRCStepped( void* dest, void* src, unsigned long size, long step )
		{
		unsigned long cStep;
		return MemcpyCRCStepped( dest, src, size, step, 0, cStep );
		}

    protected:

	/**
	 * A table holding some data needed for crc calculation.
	 */
	uint32 fCRC32Table[ 256 ];

    };




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif /* _MLUCCRC_H_ */
