#ifndef _MLUCSYSMESLOGSERVER_HPP_
#define _MLUCSYSMESLOGSERVER_HPP_


#include "MLUCTypes.h"
#include "MLUCLog.hpp"
#include "MLUCLogServer.hpp"

class MLUCSysMESLogServer: public MLUCLogServer
	{
	public:

		virtual ~MLUCSysMESLogServer() {};
		    
		virtual void LogLine( const char* origin, const char* keyword, const char* file, int linenr, 
				      const char* compile_date, const char* compile_time, MLUCLog::TLogLevel logLvl, 
				      const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
				      unsigned long runNumber, 
#endif
				      uint32 ldate, 
				      uint32 ltime_s, uint32 ltime_us, uint32 msgNr, const char* line );


		virtual void LogLine( const char* origin, const char* keyword, const char* file, int linenr, 
				      const char* compile_date, const char* compile_time, MLUCLog::TLogLevel, 
				      const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
				      unsigned long runNumber, 
#endif
				      uint32 ldate, 
				      uint32 ltime_s, uint32 ltime_us, uint32 msgNr, uint32 subMsgNr, const char* line );

	protected:
	private:
	};

#endif // _MLUCSYSMESLOGSERVER_HPP_
