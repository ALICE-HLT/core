/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _MLUCTIMER_HPP_
#define _MLUCTIMER_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/


//#define _XOPEN_SOURCE 500
#include "MLUCTypes.h"
#include "MLUCConditionSem.hpp"
#include "MLUCThread.hpp"
#include <vector>
#include <sys/time.h>
#include <unistd.h>
#include <pthread.h>


class MLUCTimerCallback;


class MLUCTimer
    {
    public:

	MLUCTimer();
	~MLUCTimer();

	void Start();
	void Stop();
	uint32 AddTimeout( uint32 timeout_ms, MLUCTimerCallback* callback, uint64 notData );
	//uint32 AddTimeout( uint32 timeout_ms, MLUCTimerCallback* callback, uint64 notData );
	void CancelTimeout( uint32 id );
	void NewTimeout( uint32 id, uint32 new_timeout_ms );

    protected:

	class MLUCTimerThread: public MLUCThread
	    {
	    public:

		MLUCTimerThread( MLUCTimer& timer );

	    protected:

		virtual void Run();

		MLUCTimer &fTimer;

	    private:
	    };

	typedef struct 
	    {
		uint32 fID;
		uint32 fTimeout; // in ms;
		MLUCTimerCallback* fCallback;
		uint64 fNotData;
		//uint32 fID;
	    }
	TimeoutData;

	uint32 fNextID;


	void TimerLoop();

	void InsertTimeout( TimeoutData& data );

	void TimerExpired();

	uint32 DiffMS( struct timeval& tv1, struct timeval tv2 );

	MLUCTimerThread fThread;


	vector<TimeoutData> fTimeouts;

	struct timeval fTimerStarted;

	pthread_mutex_t fAccessSem;

	pthread_cond_t fCondition;
	pthread_mutex_t fConditionSem;

	bool fQuit;

	friend class MLUCTimerThread;

    private:
    };


class MLUCTimerCallback
    {
    public:

	virtual ~MLUCTimerCallback() {};

	virtual void TimerExpired( uint64 notData ) = 0;

    };


class MLUCTimerSignal: public MLUCTimerCallback, public MLUCConditionSem<uint64>
    {
    public:

	MLUCTimerSignal() {};
	virtual ~MLUCTimerSignal() {};

	virtual void TimerExpired( uint64 notData );
	
	//MLUCConditionSem fSem;

    };






/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _MLUCTIMER_HPP_
