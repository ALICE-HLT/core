/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _MLUCPLATFORMATTRIBUTES_HPP_
#define _MLUCPLATFORMATTRIBUTES_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

class MLUCPlatformAttributes
     {
     protected:
	 struct TAlignment64TestStructure
	     {
		 uint64 f64Fill;
		 uint64 f64Test64;
		 uint32 f32Fill;
		 uint64 f64Test32;
		 uint16 f16Fill;
		 uint64 f64Test16;
		 uint8  f8Fill;
		 uint64 f64Test8;
	     };
	 struct TAlignment32TestStructure
	     {
		 uint64 f64Fill;
		 uint32 f32Test64;
		 uint32 f32Fill;
		 uint32 f32Test32;
		 uint16 f16Fill;
		 uint32 f32Test16;
		 uint8  f8Fill;
		 uint32 f32Test8;
	     };
	 struct TAlignment16TestStructure
	     {
		 uint64 f64Fill;
		 uint16 f16Test64;
		 uint32 f32Fill;
		 uint16 f16Test32;
		 uint16 f16Fill;
		 uint16 f16Test16;
		 uint8  f8Fill;
		 uint16 f16Test8;
	     };
	 struct TAlignment8TestStructure
	     {
		 uint64 f64Fill;
		 uint8 f8Test64;
		 uint32 f32Fill;
		 uint8 f8Test32;
		 uint16 f16Fill;
		 uint8 f8Test16;
		 uint8  f8Fill;
		 uint8 f8Test8;
	     };
	 struct TAlignmentDoubleTestStructure
	     {
		 uint64 f64Fill;
		 double fDoubleTest64;
		 uint32 f32Fill;
		 double fDoubleTest32;
		 uint16 f16Fill;
		 double fDoubleTest16;
		 uint8  f8Fill;
		 double fDoubleTest8;
	     };
	 struct TAlignmentFloatTestStructure
	     {
		 uint64 f64Fill;
		 float fFloatTest64;
		 uint32 f32Fill;
		 float fFloatTest32;
		 uint16 f16Fill;
		 float fFloatTest16;
		 uint8  f8Fill;
		 float fFloatTest8;
	     };
	 
     public:
	 
	 static const int gkByteOrderAttributeIndex        = 0;
	 static const int gkAlignmentAttributeIndexStart   = 2;
	 static const int gk64BitAlignmentAttributeIndex   = gkAlignmentAttributeIndexStart;
	 static const int gk32BitAlignmentAttributeIndex   = gkAlignmentAttributeIndexStart+1;
	 static const int gk16BitAlignmentAttributeIndex   = gkAlignmentAttributeIndexStart+2;
	 static const int gk8BitAlignmentAttributeIndex    = gkAlignmentAttributeIndexStart+3;
	 static const int gkFloatAlignmentAttributeIndex   = gkAlignmentAttributeIndexStart+4;
	 static const int gkDoubleAlignmentAttributeIndex  = gkAlignmentAttributeIndexStart+5;
	 static const int gkLastAlignmentAttributeIndex    = gkDoubleAlignmentAttributeIndex;
	 static const int gkAlignmentAttributeCount        = gkLastAlignmentAttributeIndex-gkAlignmentAttributeIndexStart+1;
	 static const int gkAttributeCount                 = 8;
	 
	 static const uint8 gkUnknownAttribute = 0;
	 static const uint8 gkUnspecifiedAttribute = ~(uint8)0;
	 
	 static const uint8 gkUnknownAlignment = gkUnknownAttribute;
	 static const uint8 gkUnspecifiedAlignment = gkUnspecifiedAttribute;
	 
	 /* For byte order as well as alignment 255 means unspecified, 
	    let it be filled in by the system to the current values.
	    The unknown placeholder in contrast really mean unknown, 
	    they won't be filled in by the system to something which is probably wrong.
	 */
	 
	 /* Keep this consistent with BCLNetworkData.hpp kLittleEndian/kBigEndian */
	 static const uint8 gkUnknownByteOrder      = gkUnknownAttribute;
	 static const uint8 gkLittleEndianByteOrder = 1;
	 static const uint8 gkBigEndianByteOrder    = 2;
	 static const uint8 gkUnspecifiedByteOrder  = gkUnspecifiedAttribute;
	 
	 static const uint8 gkNetworkByteOrder      = gkBigEndianByteOrder;
	 
#if defined(__i386__) or defined(__arm__) or defined(__x86_64__)
	 static const uint8 gkNativeByteOrder = gkLittleEndianByteOrder;
#else
#if defined(__powerpc__)
	 static const uint8 gkNativeByteOrder = gkBigEndianByteOrder;
#else
#error Byte format (little/big endian) currently not defined for platforms other than intel i386 compatible, x86_64, arm or powerpc...
#endif
#endif

	 static uint8 DetermineUInt64Alignment()
		 {
		 TAlignment64TestStructure test;
		 if ( (unsigned long)(&test.f64Test64) != ((unsigned long)(&test.f64Fill))+sizeof(test.f64Fill) )
		     {
		     // Alignment is beyond 64 bit, this is, to the best of my knowledge, currently unheard of.
		     return ~(uint8)0;
		     }
		 if ( (unsigned long)(&test.f64Test32) != ((unsigned long)(&test.f32Fill))+sizeof(test.f32Fill) )
		     {
		     // The 64 bit element does not immedately follow the 32 bit element, 
		     // therefore the alignment has to be greater than 4.
		     return (uint8)8;
		     }
		 if ( (unsigned long)(&test.f64Test16) != ((unsigned long)(&test.f16Fill))+sizeof(test.f16Fill) )
		     {
		     // The 64 bit element does not immedately follow the 16 bit element, 
		     // therefore the alignment has to be greater than 2.
		     return (uint8)4;
		     }
		 if ( (unsigned long)(&test.f64Test8) != ((unsigned long)(&test.f8Fill))+sizeof(test.f8Fill) )
		     {
		     // The 64 bit element does not immedately follow the 8 bit element, 
		     // therefore the alignment has to be greater than 1.
		     return (uint8)2;
		     }
		 return 1;
		 }
	 
	 static uint8 DetermineUInt32Alignment()
		 {
		 TAlignment32TestStructure test;
		 if ( (unsigned long)(&test.f32Test64) != ((unsigned long)(&test.f64Fill))+sizeof(test.f64Fill) )
		     {
		     // Alignment is beyond 64 bit, this is, to the best of my knowledge, currently unheard of.
		     return ~(uint8)0;
		     }
		 if ( (unsigned long)(&test.f32Test32) != ((unsigned long)(&test.f32Fill))+sizeof(test.f32Fill) )
		     {
		     // The 32 bit element does not immedately follow the 32 bit element, 
		     // therefore the alignment has to be greater than 4.
		     return (uint8)8;
		     }
		 if ( (unsigned long)(&test.f32Test16) != ((unsigned long)(&test.f16Fill))+sizeof(test.f16Fill) )
		     {
		     // The 32 bit element does not immedately follow the 16 bit element, 
		     // therefore the alignment has to be greater than 2.
		     return (uint8)4;
		     }
		 if ( (unsigned long)(&test.f32Test8) != ((unsigned long)(&test.f8Fill))+sizeof(test.f8Fill) )
		     {
		     // The 32 bit element does not immedately follow the 8 bit element, 
		     // therefore the alignment has to be greater than 1.
		     return (uint8)2;
		     }
		 return 1;
		 }
	 
	 static uint8 DetermineUInt16Alignment()
		 {
		 TAlignment16TestStructure test;
		 if ( (unsigned long)(&test.f16Test64) != ((unsigned long)(&test.f64Fill))+sizeof(test.f64Fill) )
		     {
		     // Alignment is beyond 64 bit, this is, to the best of my knowledge, currently unheard of.
		     return ~(uint8)0;
		     }
		 if ( (unsigned long)(&test.f16Test32) != ((unsigned long)(&test.f32Fill))+sizeof(test.f32Fill) )
		     {
		     // The 16 bit element does not immedately follow the 32 bit element, 
		     // therefore the alignment has to be greater than 4.
		     return (uint8)8;
		     }
		 if ( (unsigned long)(&test.f16Test16) != ((unsigned long)(&test.f16Fill))+sizeof(test.f16Fill) )
		     {
		     // The 16 bit element does not immedately follow the 16 bit element, 
		     // therefore the alignment has to be greater than 2.
		     return (uint8)4;
		     }
		 if ( (unsigned long)(&test.f16Test8) != ((unsigned long)(&test.f8Fill))+sizeof(test.f8Fill) )
		     {
		     // The 16 bit element does not immedately follow the 8 bit element, 
		     // therefore the alignment has to be greater than 1.
		     return (uint8)2;
		     }
		 return 1;
		 }
	 
	 static uint8 DetermineUInt8Alignment()
		 {
		 TAlignment8TestStructure test;
		 if ( (unsigned long)(&test.f8Test64) != ((unsigned long)(&test.f64Fill))+sizeof(test.f64Fill) )
		     {
		     // Alignment is beyond 64 bit, this is, to the best of my knowledge, currently unheard of.
		     return ~(uint8)0;
		     }
		 if ( (unsigned long)(&test.f8Test32) != ((unsigned long)(&test.f32Fill))+sizeof(test.f32Fill) )
		     {
		     // The 8 bit element does not immedately follow the 32 bit element, 
		     // therefore the alignment has to be greater than 4.
		     return (uint8)8;
		     }
		 if ( (unsigned long)(&test.f8Test16) != ((unsigned long)(&test.f16Fill))+sizeof(test.f16Fill) )
		     {
		     // The 8 bit element does not immedately follow the 16 bit element, 
		     // therefore the alignment has to be greater than 2.
		     return (uint8)4;
		     }
		 if ( (unsigned long)(&test.f8Test8) != ((unsigned long)(&test.f8Fill))+sizeof(test.f8Fill) )
		     {
		     // The 8 bit element does not immedately follow the 8 bit element, 
		     // therefore the alignment has to be greater than 1.
		     return (uint8)2;
		     }
		 return 1;
		 }
	 
	 static uint8 DetermineDoubleAlignment()
		 {
		 TAlignmentDoubleTestStructure test;
		 if ( (unsigned long)(&test.fDoubleTest64) != ((unsigned long)(&test.f64Fill))+sizeof(test.f64Fill) )
		     {
		     // Alignment is beyond 64 bit, this is, to the best of my knowledge, currently unheard of.
		     return ~(uint8)0;
		     }
		 if ( (unsigned long)(&test.fDoubleTest32) != ((unsigned long)(&test.f32Fill))+sizeof(test.f32Fill) )
		     {
		     // The double element does not immedately follow the 32 bit element, 
		     // therefore the alignment has to be greater than 4.
		     return (uint8)8;
		     }
		 if ( (unsigned long)(&test.fDoubleTest16) != ((unsigned long)(&test.f16Fill))+sizeof(test.f16Fill) )
		     {
		     // The double element does not immedately follow the 16 bit element, 
		     // therefore the alignment has to be greater than 2.
		     return (uint8)4;
		     }
		 if ( (unsigned long)(&test.fDoubleTest8) != ((unsigned long)(&test.f8Fill))+sizeof(test.f8Fill) )
		     {
		     // The double element does not immedately follow the 8 bit element, 
		     // therefore the alignment has to be greater than 1.
		     return (uint8)2;
		     }
		 return 1;
		 }
	 
	 static uint8 DetermineFloatAlignment()
		 {
		 TAlignmentFloatTestStructure test;
		 if ( (unsigned long)(&test.fFloatTest64) != ((unsigned long)(&test.f64Fill))+sizeof(test.f64Fill) )
		     {
		     // Alignment is beyond 64 bit, this is, to the best of my knowledge, currently unheard of.
		     return ~(uint8)0;
		     }
		 if ( (unsigned long)(&test.fFloatTest32) != ((unsigned long)(&test.f32Fill))+sizeof(test.f32Fill) )
		     {
		     // The float element does not immedately follow the 32 bit element, 
		     // therefore the alignment has to be greater than 4.
		     return (uint8)8;
		     }
		 if ( (unsigned long)(&test.fFloatTest16) != ((unsigned long)(&test.f16Fill))+sizeof(test.f16Fill) )
		     {
		     // The float element does not immedately follow the 16 bit element, 
		     // therefore the alignment has to be greater than 2.
		     return (uint8)4;
		     }
		 if ( (unsigned long)(&test.fFloatTest8) != ((unsigned long)(&test.f8Fill))+sizeof(test.f8Fill) )
		     {
		     // The float element does not immedately follow the 8 bit element, 
		     // therefore the alignment has to be greater than 1.
		     return (uint8)2;
		     }
		 return 1;
		 }


	 static void FillBlockAttributes( uint8 attributes[8] )
		 {
		 for ( int j = 0; j < gkAttributeCount; j++ )
		     attributes[j] = gkUnspecifiedAttribute;
		 attributes[gkByteOrderAttributeIndex] = gkNativeByteOrder;
		 attributes[gk64BitAlignmentAttributeIndex] = DetermineUInt64Alignment();
		 attributes[gk32BitAlignmentAttributeIndex] = DetermineUInt32Alignment();
		 attributes[gk16BitAlignmentAttributeIndex] = DetermineUInt16Alignment();
		 attributes[gk8BitAlignmentAttributeIndex] = DetermineUInt8Alignment();
		 attributes[gkFloatAlignmentAttributeIndex] = DetermineFloatAlignment();
		 attributes[gkDoubleAlignmentAttributeIndex] = DetermineDoubleAlignment();
		 }

	 static void FillUnspecifiedBlockAttributes( uint8 attributes[8] )
		 {
		 if ( attributes[gkByteOrderAttributeIndex]==gkUnspecifiedAttribute )
		     attributes[gkByteOrderAttributeIndex] = gkNativeByteOrder;
		 if ( attributes[gk64BitAlignmentAttributeIndex]==gkUnspecifiedAttribute )
		     attributes[gk64BitAlignmentAttributeIndex] = DetermineUInt64Alignment();
		 if ( attributes[gk32BitAlignmentAttributeIndex]==gkUnspecifiedAttribute )
		     attributes[gk32BitAlignmentAttributeIndex] = DetermineUInt32Alignment();
		 if ( attributes[gk16BitAlignmentAttributeIndex]==gkUnspecifiedAttribute )
		     attributes[gk16BitAlignmentAttributeIndex] = DetermineUInt16Alignment();
		 if ( attributes[gk8BitAlignmentAttributeIndex]==gkUnspecifiedAttribute )
		     attributes[gk8BitAlignmentAttributeIndex] = DetermineUInt8Alignment();
		 if ( attributes[gkFloatAlignmentAttributeIndex]==gkUnspecifiedAttribute )
		     attributes[gkFloatAlignmentAttributeIndex] = DetermineFloatAlignment();
		 if ( attributes[gkDoubleAlignmentAttributeIndex]==gkUnspecifiedAttribute )
		     attributes[gkDoubleAlignmentAttributeIndex] = DetermineDoubleAlignment();
		 }
	 
	 static uint8 GetBlockAttribute( unsigned attributeIndex )
		 {
		 switch ( attributeIndex )
		     {
		     case gkByteOrderAttributeIndex:
			 return gkNativeByteOrder;
		     case gk64BitAlignmentAttributeIndex:
			 return DetermineUInt64Alignment();
		     case gk32BitAlignmentAttributeIndex:
			 return DetermineUInt32Alignment();
		     case gk16BitAlignmentAttributeIndex:
			 return DetermineUInt16Alignment();
		     case gk8BitAlignmentAttributeIndex:
			 return DetermineUInt8Alignment();
		     case gkFloatAlignmentAttributeIndex:
			 return DetermineFloatAlignment();
		     case gkDoubleAlignmentAttributeIndex:
			 return DetermineDoubleAlignment();
		     default:
			 return gkUnspecifiedAttribute;
		     }
		 }



	 
	 
     };


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif /* _MLUCPLATFORMATTRIBUTES_HPP_ */
