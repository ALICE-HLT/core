/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _MLUCHISTOGRAM_HPP_
#define _MLUCHISTOGRAM_HPP_
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck 
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCTypes.h"
#include <stdlib.h>

/**
 * @brief A simple histogram class for 64 uints.
 * This class is a very simple histogram for 64 bit uint values.
 * 
 * @author $Author$ - Initial Version by Timm Morten Steinbeck
 * @version $Id$
 */
class MLUCHistogram
    {
    public:

	/**
	 * Constructor.
	 * Parameters that need to be specified are the lower bound
	 * of the histogram (\a lower), the upper bound (\a upper)
	 * and the width of the histogram bins (\a witdh).
	 *
	 * @param lower The lower bound of the histogram.
	 * @param upper The upper bound of the histogram.
	 * @param width The width of a histogram bin.
	 */
	MLUCHistogram( double lower, double upper, double width );

	//MLUCHistogram( const char* filename );

	/**
	 * Add an entry to the histogram.
	 * The bin into which the passed value fits is incremented by one.
	 *
	 * @param value The value to be added to the histogram.
	 * @return \c void
	 */
	void Add( const double& value )
		{
		if ( value<fLower )
		    {
		    fUnderflows++;
		    fEntryCnt++;
		    return;
		    }
		if ( value>=fUpper )
		    {
		    fOverflows++;
		    fEntryCnt++;
		    return;
		    }
		uint32 ndx;
		ndx = GetBin( value );
		if ( ndx<fBinCnt )
		    {
		    fBins[ ndx ]++;
		    fEntryCnt++;
		    }
		}

	/**
	 * Get the number of bins.
	 *
	 * @return A uint32 holding the number of bins (excluding over-/underflows).
	 */
	uint32 GetBinCount();

	/**
	 * Get the number of entries in the bin with the given index.
	 * 
	 * @param ndx The index of the bin from which to get the entry count.
	 * @return The number of entries in the given bin.
	 */
	uint32 GetBinEntryCnt( uint32 ndx );

	/**
	 * Get the lower edge of the specified bin.
	 *
	 * @param ndx The index of the bin for which to return the lower edge.
	 * @return The lower edge of the specified bin.
	 */
	double GetBinLow( uint32 ndx );

	/**
	 * Get the upper edge of the specified bin.
	 *
	 * @param ndx The index of the bin for which to return the upper edge.
	 * @return The upper edge of the specified bin.
	 */
	double GetBinHigh( uint32 ndx );

	/**
	 * Get the index of the bin for the specified value.
	 *
	 * @param value The value for which the bin index is to be determined.
	 * @return The bin index corresponding to the specified value.
	 */
	uint32 GetBin( const double& value )
		{
		if ( value<fLower || value>=fUpper )
		    return ~(uint32)0;
		return (uint32)( (value-fLower)/fBinWidth );
		}

	/**
	 * Get the number of overflows (values above the highest bin).
	 *
	 * @return The number of overflows.
	 */
	uint32 GetOverflows();

	/**
	 * Get the number of underflows (values below the lowest bin).
	 *
	 * @return The number of underflows.
	 */
	uint32 GetUnderflows();

	/**
	 * Get the total number of entries in this histogram.
	 *
	 * @return The total number of entries.
	 */
	uint64 GetEntryCnt();

// 	/**
// 	 * Calculate some statiscal values for the histogram, like
// 	 * mean and RMS.
// 	 *
// 	 * @param mean A reference to a 64 bit uint where the histogram's mean will be returned.
// 	 * @param rms A reference to a 64 bit uint where the histogram's rms will be returned.
// 	 */
// 	void GetStats( uint64& mean, uint64& rms );

// 	/**
// 	 * Calculate some statiscal values for the histogram, like
// 	 * mean, RMS, and median.
// 	 *
// 	 * @param mean A reference to a 64 bit uint where the histogram's mean will be returned.
// 	 * @param rms A reference to a 64 bit uint where the histogram's rms will be returned.
// 	 * @param median A reference to a 64 bit uint where the histogram's median will be returned.
// 	 */
// 	void GetStats( uint64& mean, uint64& rms, uint64& median );


	/**
	 * Write the histogram to a file of the given name.
	 * File format:
	 *  8 byte double upper boundary
	 *  8 byte double lower boundary
	 *  8 byte double bin width
	 *  32 bit uint overflow count
	 *  32 bit uint underflow count
	 *  32 bit uints bins. bin count == (upper-lower)/width
	 *
	 * @param filename The name of the file into which to write the histogram.
	 * @return An integer given the error code corresponding to the errno definition. (0 is success).
	 */
	int Write( const char* filename );
	// int Read( const char* filename );

    protected:

	/**
	 * The total number of entries in the histogram.
	 */
	uint64 fEntryCnt;

	/**
	 * The number of overflows.
	 */
	uint32 fOverflows;

	/**
	 * The number of underflows.
	 */
	uint32 fUnderflows;

	/**
	 * The lower edge of the histogram.
	 */
	double fLower;

	/**
	 * The upper edge of the histogram.
	 */
	double fUpper;

	/**
	 * The width of each bin.
	 */
	double fBinWidth;

	/**
	 * The number of bins.
	 */
	uint32 fBinCnt;

	/**
	 * A pointer top the bin array itself.
	 */
	uint32 *fBins;
	
    };

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck 
**
** $Id$ 
**
***************************************************************************
*/

#endif // _MLUCHISTOGRAM_HPP_
