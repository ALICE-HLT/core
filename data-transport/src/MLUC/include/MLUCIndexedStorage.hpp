#ifndef _MLUCINDEXEDSTORAGE_HPP_
#define _MLUCINDEXEDSTORAGE_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCTypes.h"

template<class Index, class Data, int hashWidthLog2, bool growable>
class MLUCIndexedStorage
    {
    public:

	typedef bool (*TSearchFunc)( const Data& data, void* arg );

	MLUCIndexedStorage( int sizeLog2 );
	~MLUCIndexedStorage();

	bool Add( Index index, Data data )
		{
		}

	Data& Get( Index index )
		{
		}

	Data* GetPtr( Index index )
		{
		}

	bool Del( Index index )
		{
		}

	Index FindIndex( TSearchFunc searchFunc, void* searchFuncArg )
		{
		}

	Data& FindData( TSearchFunc searchFunc, void* searchFuncArg )
		{
		return *FindDataPtr( searchFunc, searchFuncArg );
		}
	Data* FindDataPtr( TSearchFunc searchFunc, void* searchFuncArg )
		{
		}

    protected:

	const unsigned kHashCount = 1<<hashWidthLog2;
	const unsigned kHashMask = (1<<hashWidthLog2)-1;

	unsigned long fUsedCounts[ kHashCount+1 ];
	unsigned long fCounts[ kHashCount+1 ];
	unsigned long fStarts[ kHashCount+1 ];
	unsigned long fEnds[ kHashCount+1 ];
	Index* fIndices[ kHashCount+1 ];
	Data** fDatas[ kHashCount+1 ];

	void Grow( unsigned hashIndex )
		{
		}
	void Shrink( unsigned hashIndex )
		{
		}
	void Compact( unsigned hashIndex )
		{
		}
	


    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _MLUCINDEXEDSTORAGE_HPP_
