/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _MLUCVALUEMONITOR_HPP_
#define _MLUCVALUEMONITOR_HPP_
/*
***************************************************************************
**
** $Author$ - Initial Version by Arne Wiebalck & Timm Morten Steinbeck 
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCTypes.h"
#include "MLUCString.hpp"

/// 
/**
 * @class MLUCValueMonitor
 * @brief A base class for monitoring and outputting of quantities including automatic averaging.
 * A base class that provides services for monitoring specific quantities.
 * The class provides facilities for output to a console, output to a file with timestamps,
 * and automatic averaging over the last N values. 
 * The actual measurement of the quantities to be monitored has to be implemented in a 
 * derived class by overwriting the pure virtual method GetValue() in this class.
 * 
 * File format:
 *      32 bit uint for endianness determination (should be '1234').
 *      32 bit uint giving size of struct timeval.
 *      32 bit uint giving length of file description header text
 *      file description text (length see before)
 *      timestamp (struct timeval) / value (uint64) pairs until EOF.
 *
 * @author $Author$ - Initial Version by Arne Wiebalck & Timm Steinbeck
 * @version $Id$
 */
class MLUCValueMonitor
    {
    public:

	/**
	 * @brief Constructor that performs no automatic output.
	 * Using this constructor enables neither output to a file nor to
	 * standard output. 
	 * For the averaged value the number of the  last values over which 
	 * the average is to be taken can be determined as well. 
	 *
	 * @param average_items The number of the most recent values over which the average is to be calculated.
	 */
	MLUCValueMonitor( unsigned int average_items );

	/**
	 * @brief Constructor to enable output to file and stdout.
	 * Using this constructor enables output to a file as well as to the 
	 * standard output. The filename is passed to the constructor as well as a
	 * string containing a description of the measurement that will be written 
	 * to the beginning of the file. 
	 * For output to stdout prefixes and suffixes are provided that will be output
	 * respectively before and after the monitored value. Another pre-/suffix pair
	 * is provided for the averaged value. For the averaged value the number of the 
	 * last values over which the average is to be taken can be determined as well. 
	 *
	 * @param filename A string holding the name of the file to which the values monitored are output. 
	 * @param fileDescrHeader A string holding a description of the monitoring taking place. This string
	 * will be written to the start of the output file. 
	 * @param output_prefix A string holding the textual prefix that is to be output before
	 * the measured value.
	 * @param output_suffix A string holding the textual suffix that is to be output after
	 * the measured value.
	 * @param avg_output_prefix A string holding the textual prefix that is to be output before
	 * the average of the measured value.
	 * @param avg_output_suffix A string holding the textual suffix that is to be output after
	 * the average of the measured value.
	 * @param average_items The number of the most recent values over which the average is to be calculated.
	 */
	MLUCValueMonitor( const char* filename, const char* fileDescrHeader, 
			  const char* output_prefix, const char* output_suffix, 
			  const char* avg_output_prefix, const char* avg_output_suffix, 
			  unsigned int average_items );

	/**
	 * @brief Constructor to enable only output to stdout.
	 * Using this constructor enables output to the 
	 * standard output. 
	 * Prefixes and suffixes are provided that will be output
	 * respectively before and after the monitored value. Another pre-/suffix pair
	 * is provided for the averaged value. For the averaged value the number of the 
	 * last values over which the average is to be taken can be determined as well. 
	 *
	 * @param output_prefix A string holding the textual prefix that is to be output before
	 * the measured value.
	 * @param output_suffix A string holding the textual suffix that is to be output after
	 * the measured value.
	 * @param avg_output_prefix A string holding the textual prefix that is to be output before
	 * the average of the measured value.
	 * @param avg_output_suffix A string holding the textual suffix that is to be output after
	 * the average of the measured value.
	 * @param average_items The number of the most recent values over which the average is to be calculated.
	 */
	MLUCValueMonitor( const char* output_prefix, const char* output_suffix, 
			  const char* avg_output_prefix, const char* avg_output_suffix, 
			  unsigned int average_items );

	/**
	 * @brief Constructor to enable only output to a file.
	 * Using this constructor enables output to a file. 
	 * The filename is passed to the constructor as well as a
	 * string containing a description of the measurement that will be written 
	 * to the beginning of the file. 
	 * For the averaged value the number of the last values over which the average 
	 * is to be taken can be determined as well. 
	 *
	 * @param filename A string holding the name of the file to which the values monitored are output. 
	 * @param fileDescrHeader A string holding a description of the monitoring taking place. This string
	 * will be written to the start of the output file. 
	 * @param average_items The number of the most recent values over which the average is to be calculated.
	 */
	MLUCValueMonitor( const char* filename, const char* fileDescrHeader, unsigned int average_items );

	MLUCValueMonitor( const char* fileDescrHeader, unsigned int average_items );

	/**
	 * @brief Descructor.
	 * The destructor of the class.
	 *
	 * @param None
	 */
	virtual ~MLUCValueMonitor();

	/**
	 * @brief The method to actually perform the measurement of the values to be monitored.
	 * This method has to be overwritten in derived classes to provide the actual readout of 
	 * the monitored values. 
	 *
	 * @return A integer holding a error return code corresponding to the C errno variable. (0 means success)
	 * @param measuredValue A 64 bit unsigned integer holding the quantity to be measured.
	 */
	virtual int GetValue( uint64& measuredValue ) = 0;

	virtual int GetValue( uint64& measuredValue, struct timeval& timestamp );
	
	/**
	 * @brief The main method that handles the monitoring tasks.
	 * The method that does the main jobs of calculating the average value,
	 * outputting to stdout and outputting to file as necessary.
	 *
	 * @param value The newest measured value as returned by GetValue().
	 * @param outputAvg Specifies whether to output a calculated average value.
	 * @return A integer holding a error return code corresponding to the C errno variable. (0 means success)
	 */
	int OutputValue( uint64 value, bool outputAvg = true );

	int OutputValue( uint64 value, const struct timeval& timestamp, bool outputAvg = true );

	/**
	 * @brief Method to tell the object to start monitoring.
	 * This method must be called to tell the object that monitoring is to start.
	 * Amongst others, this method opens (if desired) the monitoring output file.
	 * 
	 * @param None
	 * @return A integer holding a error return code corresponding to the C errno variable. (0 means success)
	 */
	int StartMonitoring();

	/**
	 * @brief Method to tell the object to stop monitoring.
	 * This method must be called to tell the object that monitoring is to stop.
	 * Amongst others, this method closes (if desired) the monitoring output file.
	 * 
	 * @param None
	 * @return A integer holding a error return code corresponding to the C errno variable. (0 means success)
	 */
	int StopMonitoring();

	const char* GetDescription() const
		{
		return fFileDescrHeader.c_str();
		}

	/**
	 * Enable or disable the printing of a timestampe value in the output.
	 */
	bool SetTimeStampOutput( bool out )
		{
		bool tmp = fTimeStampOutput;
		fTimeStampOutput = out;
		return tmp;
		}

	void DisableFileOutput()
		{
		fDoFileOutput = false;
		}

    protected:

	/**
	 * @brief Pointer to an array holding the last measured values for average calculation.
	 * This is a pointer to an array (created in the constructor) that holds the measured values
	 * over which the calculation of the average is performed.
	 * This array is used as a circular buffer.
	 */
	uint64 *fAverageHistory;

	/**
	 * The number of items to be used for the average history. This is the size
	 * of the fAverageHistory array. 
	 */
	int fHistoryItemCount;

	/**
	 * The number of items available for the average history. This is the number
	 * of items actually present in the fAverageHistory array.
	 */
	int fHistoryAvailable;

	/**
	 * The index of the last filled item in the fAverageHistory array.
	 */
	int fLastHistory;

	/** 
	 * A bool determining wether output to a file is performed.
	 */
	bool fDoFileOutput;

	/** 
	 * A bool determining wether output to stdout is performed.
	 */
	bool fDoStdoutOutput;

	/**
	 * The name of the file to which output is made.
	 */
	MLUCString fFilename;

	/**
	 * The file descriptor of the file to which output is made.
	 */
	int fFile;

	/**
	 * A string holding a textual description of the current monitoring 
	 * performed. Basically a run description.
	 */
	MLUCString fFileDescrHeader;

 	/**
	 * A string holding a prefix that is printed to stdout before the monitored
	 * value.
	 */
	MLUCString fOutputPrefix;

	/**
	 * A string holding a suffix that is printed to stdout after the monitored
	 * value.
	 */
	MLUCString fOutputSuffix;

 	/**
	 * A string holding a prefix that is printed to stdout before the average 
	 * of the monitored value.
	 */
	MLUCString fAvgOutputPrefix;

 	/**
	 * A string holding a usffix that is printed to stdout after the average 
	 * of the monitored value.
	 */
	MLUCString fAvgOutputSuffix;

	/**
	 * Specifies whether timestamps should be printed in the object's output.
	 */
	bool fTimeStampOutput;
	
    private:
    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Arne Wiebalck & Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
#endif // _MLUCVALUEMONITOR_HPP_
