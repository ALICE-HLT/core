/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _MLUCBUFFERINGLOGSERVER_HPP_
#define _MLUCBUFFERINGLOGSERVER_HPP_


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCLogServer.hpp"
#include "MLUCThread.hpp"
#include <sys/time.h>
#include <unistd.h>
#include <pthread.h>
#include <vector>

/**
 * A buffering log server class. This class buffers the last N
 * log messages and display them when no log activity has been 
 * detected for a configurable amount of time.
 *
 * @short A buffering log server class
 * @author $Author$ - Initial version by Timm Morten Steinbeck
 * @version $Id$
 * @see MLUCLog
 * @see MLUCLogServer
 */
class MLUCBufferingLogServer: public MLUCLogServer
    {
    public:

	/**
	 * Constructor.
	 *
	 * @param bufferCount The number of messages to keep in the buffer.
	 * @param waitTime_ms The time to wait for non-activity before flushing the buffer.
	 */
	MLUCBufferingLogServer( unsigned int bufferCount, unsigned int waitTime_ms );

	/**
	 * Destructor
	 */
	virtual ~MLUCBufferingLogServer();
	    
	/**
	 * Start the background wait thread.
	 *
	 * @return void
	 */
	void StartThread();

	/**
	 * Stop the background wait thread.
	 *
	 * @return void
	 */
	void StopThread();
	    
	/**
	 * Method to add a logging server, to which messages are 
	 * passed for output. 
	 *
	 * @return void
	 * @param server Pointer to a MLUCLogServer object to add to the list.
	 * @see DelServer
	 */
	void AddServer( MLUCLogServer* server );

	/**
	 * Method to add a logging server, to which messages are 
	 * passed for output. 
	 *
	 * @return void
	 * @param server Reference to a MLUCLogServer object to add to the list.
	 * @see DelServer
	 */
	void AddServer( MLUCLogServer& server )
		{
		AddServer( &server );
		}

	/**
	 * Method to remove a previously added logging server from the list.
	 *
	 * @return void
	 * @param server Pointer to the logging server to remove from the list.
	 * @see AddServer
	 */
	void DelServer( MLUCLogServer* server );

	/**
	 * Method to remove a previously added logging server from the list.
	 *
	 * @return void
	 * @param server Reference to the logging server to remove from the list.
	 * @see AddServer
	 */
	void DelServer( MLUCLogServer& server )
		{
		DelServer( &server );
		}


	/**
	 * Logging method for normal (non-modulo) messages.
	 * This has to be overridden by derived classes.
	 *
	 * @return void
	 * @param origin Textual origin description
	 * @param keyword Textual keyword(s)
	 * @param file Name of the originating source file
	 * @param linenr Line number in the originating source file
	 * @param compile_date Source compilation date
	 * @param compile_time Source compilation time
	 * @param logLvl The logging level of this message
	 * @param hostname Name of the originating host
	 * @param id The id of the originating log object (process)
	 * @param ldate Date at which the message was logged
	 * @param ltime_s Time at which the message was logged (sec. part)
	 * @param ltime_us Time at which the message was logged (usec. part)
	 * @param msgNr The sequence number of this message
	 * @param line The logged message itself
	 */
	virtual void LogLine( const char* origin, const char* keyword, const char* file, int linenr, 
			      const char* compile_date, const char* compile_time, MLUCLog::TLogLevel logLvl, 
			      const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
			      unsigned long runNumber, 
#endif
			      uint32 ldate, 
			      uint32 ltime_s, uint32 ltime_us, uint32 msgNr, const char* line );

	/**
	 * Logging method for modulo type messages.
	 * This has to be overridden by derived classes.
	 *
	 * @return void
	 * @param origin Textual origin description
	 * @param keyword Textual keyword(s)
	 * @param file Name of the originating source file
	 * @param linenr Line number in the originating source file
	 * @param compile_date Source compilation date
	 * @param compile_time Source compilation time
	 * @param logLvl The logging level of this message
	 * @param hostname Name of the originating host
	 * @param id The id of the originating log object (process)
	 * @param ldate Date at which the message was logged
	 * @param ltime_s Time at which the message was logged (sec. part)
	 * @param ltime_us Time at which the message was logged (usec. part)
	 * @param msgNr The sequence number of this message
	 * @param submsgNr The modulo sequence number of this message
	 * @param line The logged message itself
	 */
	virtual void LogLine( const char* origin, const char* keyword, const char* file, int linenr, 
			      const char* compile_date, const char* compile_time, MLUCLog::TLogLevel, 
			      const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
			      unsigned long runNumber, 
#endif
			      uint32 ldate, 
			      uint32 ltime_s, uint32 ltime_us, uint32 msgNr, uint32 subMsgNr, const char* line );

	void FlushMessages();

    protected:


	/**
	 * An array (vector) of pointers to MLUCLogServer objects.
	 * Each of these log server objects gets each complete log message.
	 *
	 * @see MLUCLogServer
	 */
	vector<MLUCLogServer*> fServers;

	pthread_mutex_t fMutex;

	struct timeval fLastLogTime;
	
	class TLogMsgData
	    {
	    public:

		TLogMsgData( const char* origin, const char* keyword, const char* file, int linenr, 
			     const char* compile_date, const char* compile_time, MLUCLog::TLogLevel loglevel, 
			     const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
			     unsigned long runNumber, 
#endif
			     uint32 ldate, 
			     uint32 ltime_s, uint32 ltime_us, uint32 msgNr, uint32 subMsgNr, char* line )
			{
			fOrigin = origin;
			fKeyword = keyword;
			fFile = file;
			fLine = line;
			fLineNr = linenr;
			fCompileDate = compile_date;
			fCompileTime = compile_time;
			fLogLevel = loglevel;
			fHostname = hostname;
			fID = id;
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
			fRunNumber = runNumber;
#endif
			fLDate = ldate;
			fLTime_s = ltime_s;
			fLTime_us = ltime_us;
			fMsgNr = msgNr;
			fHasSubMsgNr = true;
			fSubMsgNr = subMsgNr;
			fLine = line;
			}

		TLogMsgData( const char* origin, const char* keyword, const char* file, int linenr, 
			     const char* compile_date, const char* compile_time, MLUCLog::TLogLevel loglevel, 
			     const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
			     unsigned long runNumber, 
#endif
			     uint32 ldate, 
			     uint32 ltime_s, uint32 ltime_us, uint32 msgNr, char* line )
			{
			fOrigin = origin;
			fKeyword = keyword;
			fFile = file;
			fLine = line;
			fLineNr = linenr;
			fCompileDate = compile_date;
			fCompileTime = compile_time;
			fLogLevel = loglevel;
			fHostname = hostname;
			fID = id;
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
			fRunNumber = runNumber;
#endif
			fLDate = ldate;
			fLTime_s = ltime_s;
			fLTime_us = ltime_us;
			fMsgNr = msgNr;
			fHasSubMsgNr = false;
			fSubMsgNr = ~(uint32)0;
			fLine = line;
			}
		MLUCString fOrigin;
		MLUCString fKeyword;
		const char* fFile;
		int fLineNr;
		const char* fCompileDate;
		const char* fCompileTime;
		MLUCLog::TLogLevel fLogLevel;
		MLUCString fHostname;
		MLUCString fID;
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
		unsigned long fRunNumber;
#endif
		uint32 fLDate;
		uint32 fLTime_s; 
		uint32 fLTime_us;
		uint32 fMsgNr;
		bool fHasSubMsgNr;
		uint32 fSubMsgNr;
		MLUCString fLine;
	    };

	vector<TLogMsgData> fLogMsgs;

	void MonitoringFunc();

	class TMonitoringThread: public MLUCThread
	    {
	    public:

		TMonitoringThread( MLUCBufferingLogServer* logServer ):
		    fLogServer( logServer )
			{
			}

	    protected:

		void Run()
			{
			fLogServer->MonitoringFunc();
			}

		MLUCBufferingLogServer* fLogServer;
	    };

	    friend class TMonitoringThread;

	TMonitoringThread fMonitorThread;
	bool fQuitMonitor;

	unsigned int fBufferCount;
	unsigned int fWaitTime;

    private:
    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _MLUCBUFFERINGLOGSERVER_HPP_
