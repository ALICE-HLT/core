#ifndef _MLUCDYNAMICALLOCCACHE_HPP_
#define _MLUCDYNAMICALLOCCACHE_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCTypes.h"
#include "MLUCIndexedVector.hpp"
#include <pthread.h>


/**
 * A class that works as a cache for allocated objects.
 * It keeps a configurable number (power of 2) elements
 * of allocated objects in list. The list is filled on demand
 * by allocation requests. When the list is filled 
 * requests will be served if possible from the list, otherwise
 * list elements will be freed and new elements will be allocated.
 * 
 * A user of the class will not see this difference in using 
 * the class, at most a performance difference might be visible.
 *
 * @short A class to dynamically cache allocated objects.
 * @author $Author$
 * @version $Id$
 */
class MLUCDynamicAllocCache
    {
    public:

	/**
	 * Constructor. Takes as its arguments an exponent to two specifying
	 * the number of object slots to be available.
	 * E.g. if cntExp2 is 4, the 2^4==16 elements will be available
	 *
	 * @param cntExp2 2^cntExp2 is the number of elements that can be 
	 * kept available
	 * @param grow A bool specyfing whether the cache is allowed to 
	 * grow in size (number of slots)
	 * @param throrough A bool specyfing whether by default the smallest matching 
	 * element is to be searched for (true) or whether the first match
	 * should be taken (false)
	 */
	MLUCDynamicAllocCache( unsigned cntExp2, bool grow, bool thorough );

	/**
	 * Destructor. Cleans up all elements handled by this object.
	 */
	~MLUCDynamicAllocCache();

	/**
	 * Get an element of the specified size. If possible one of the
	 * already allocated elements will be used, otherwise a new element
	 * will be allocated.
	 *
	 * @param size The size of the requested memory in bytes.
	 * @return A pointer to the requested element or NULL on failure.
	 */
	uint8* Get( unsigned long size )
		{
		return Get( size, fThorough );
		}

	/**
	 * Get an element of the specified size. If possible one of the
	 * already allocated elements will be used, otherwise a new element
	 * will be allocated.
	 *
	 * @param size The size of the requested memory in bytes.
	 * @return A pointer to the requested element or NULL on failure.
	 * @param throrough A bool specyfing whether the smallest matching 
	 * element is to be searched for (true) or whether the first match
	 * should be taken (false)
	 */
	uint8* Get( unsigned long size, bool thorough );

	/**
	 * Release an element previously obtained by a call to Get.
	 *
	 * @param ptr The pointer to the previously requested element.
	 * @return void
	 */
	void Release( uint8* ptr );

    protected:

	/**
	 * Typedef declaration for the pointer type stored in the internal arrays.
	 */
	typedef uint8* TPtr;

	/**
	 * A bool flag specyfing whether the smallest matching 
	 * element is to be searched for (true) or whether the first match
	 * should be taken (false)
	 */
	bool fThorough;

	/**
	 * A bool flag specifying whether the number of elements is allowed 
	 * to increase or not.
	 */
	bool fGrow;

	/**
	 * Structure definition holding the state of allocated objects.
	 */
	struct TPtrData
	    {
		TPtrData()
			{
			fPtr = NULL;
			fSize = 0;
			fUsed = false;
			}
		TPtr fPtr;
		unsigned long fSize;
		bool fUsed;
	    };

	/**
	 * The array of currently available/allocated memory blocks.
	 */
	TPtrData* fPtrs;

	unsigned long fDataSize;
	unsigned long fOrigDataSize;
	unsigned long fMask;
	unsigned long fLastNdx;
	unsigned long fLastReleaseNdx;

	unsigned long fUsedCnt;
	unsigned long fUnusedCnt;

	/**
	 * Factor to determine when an existing block is too large.
	 * If a block's size is larger than the requested size shifted
	 * by this amount then the block is released and is newly allocated
	 * with just the requested size.
	 */
	unsigned long fTooLargeShift;
	

	/**
	 * A mutex regulating access to the array holding the
	 * elements.
	 */
	pthread_mutex_t fMutex;

	void Shrink();

	MLUCIndexedVector<unsigned long,unsigned long> fPtr2IndexMap;

    private:
    };




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _MLUCDYNAMICALLOCCACHE_HPP_
