#ifndef _MLUCALLOCCACHE_HPP_
#define _MLUCALLOCCACHE_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCVector.hpp"
#include <pthread.h>


/**
 * A class that works as a cache for allocated objects.
 * It keeps a configurable number (power of 2) elements
 * of a fixed size available in a list. These elements 
 * can be requested and are then marked as used. When 
 * such an element is freed again, it is again available.
 * This process is done without actually doing any new/?alloc
 * or delete/free.
 * When more elements are requested than are available, the 
 * elements needed additionally are allocated normally via
 * new and freed with delete.
 * A user of the class will not see this difference in using 
 * the class, at most a performance difference might be visible.
 *
 * @short A class to cache allocated objects.
 * @author $Author$
 * @version $Id$
 */
class MLUCAllocCache
    {
    public:

	/**
	 * Constructor. Takes as its arguments the size of the objects
	 * that will be made available and an exponent two specifying
	 * the number of objects to be pre-allocated.
	 * E.g. if cntExp2 is 4, the 2^4==16 elements will be available
	 * pre-allocated.
	 *
	 * @param size The size of the elements available from this object.
	 * @param cntExp2 2^cntExp2 is the number of elements that will be 
	 * kept pre-allocated.
	 */
	MLUCAllocCache( unsigned long size, unsigned cntExp2 );

	/**
	 * Destructor. Cleans up all elements handled by this object.
	 */
	~MLUCAllocCache();

	/**
	 * Get an element of the specified size. If available one of the
	 * pre-allocated elements will be used, otherwise a new element
	 * will be allocated.
	 *
	 * @return A pointer to the requested element or NULL on failure.
	 */
	uint8* Get();

	/**
	 * Release an element previously obtained by a call to Get.
	 *
	 * @param ptr The pointer to the previously requested element.
	 * @return void
	 */
	void Release( uint8* ptr );

	/**
	 * Check wether the state of the object is ok.
	 * 
	 * @return A boolean telling wether the state of this object is ok (true) or not (false).
	 */
	operator bool()
		{
		return fOk;
		}

    protected:

	/**
	 * Typedef declaration for the pointer type stored in the internal arrays.
	 */
	typedef uint8* TPtr;

	/**
	 * The size of the elements handled by this object."
	 */
	unsigned long fSize;

	/**
	 * A flag holding the state of this object.
	 */
	bool fOk;

	/**
	 * A list of pre-allocated unused/free elements.
	 */
	MLUCVector<TPtr> fFree;

	/**
	 * A list of pre-allocated used elements.
	 */
	MLUCVector<TPtr> fUsed;

	/**
	 * A list of elements that were allocated on demand when the 
	 * pre-allocated elements were exhausted.
	 */
	MLUCVector<TPtr> fAllocated;

	/**
	 * A mutex regulating access to the lists holding the various
	 * elements.
	 */
	pthread_mutex_t fMutex;

	/**
	 * Function supplied to the list as a search function when
	 * searching for an element in them.
	 *
	 * @param ptr The pointer to the element in the list that is compared with.
	 * @param ref The comparison element, the pointer to the element that is searched for.
	 * @return true when the element in the list is the one that is looked for, e.g. when
	 * it is equal to the supplied reference element.
	 */
	static bool FindPtr( const TPtr& ptr, const TPtr& ref ); 

    private:
    };




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _MLUCALLOCCACHE_HPP_
