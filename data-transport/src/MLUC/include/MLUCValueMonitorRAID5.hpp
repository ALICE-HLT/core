/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _MLUCVALUEMONITORRAID5_HPP_
#define _MLUCVALUEMONITORRAID5_HPP_
/*
***************************************************************************
**
** $Author$ - Initial Version by Arne Wiebalck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCValueMonitor.hpp"
#include "MLUCLog.hpp"

#include <fstream>
#include <cstdlib>
#include <errno.h>


/* Status of a device inside a RAID */

#define NO_DEVICE 0x0
#define DEVICE_OK 0x1
#define RESYNC    0x2
#define FAULTY    0x3

/// 
/**
 * @class MLUCValueMonitorRAID5
 * @brief A class for RAID5 monitoring.
 * @author $Author$ - Initial Version by Arne Wiebalck
 * @version $Id$
 */
class MLUCValueMonitorRAID5 : public MLUCValueMonitor {

public:
  /**
   * @brief Constructor to enable output to file and stdout.
   * Using this constructor enables output to a file as well as to the 
   * standard output. The filename is passed to the constructor as well as a
   * string containing a description of the measurement that will be written 
   * to the beginning of the file. 
   * For output to stdout prefixes and suffixes are provided that will be output
   * respectively before and after the monitored value. Another pre-/suffix pair
   * is provided for the averaged value. For the averaged value the number of the 
   * last values over which the average is to be taken can be determined as well. 
   *
   * @param filename A string holding the name of the file to which the values monitored are output. 
   * @param fileDescrHeader A string holding a description of the monitoring taking place. This string
   * will be written to the start of the output file. 
   * @param output_prefix A string holding the textual prefix that is to be output before
   * the measured value.
   * @param output_suffix A string holding the textual suffix that is to be output after
   * the measured value.
   * @param avg_output_prefix A string holding the textual prefix that is to be output before
   * the average of the measured value.
   * @param avg_output_suffix A string holding the textual suffix that is to be output after
   * the average of the measured value.
   * @param average_items The number of the most recent values over which the average is to be calculated.
   * @param RAID_ID The RAID device ID. This is meant for monitoring different RAIDs in a system. 
   * 
   */
  MLUCValueMonitorRAID5( const char* filename, 
			 const char* fileDescrHeader, 
			 const char* output_prefix, 
			 const char* output_suffix, 
			 const char* avg_output_prefix, 
			 const char* avg_output_suffix, 
			 unsigned int average_items,
			 unsigned int RAID_ID );
  /**
   * @brief Constructor to enable only output to stdout.
   * Using this constructor enables output to the 
   * standard output. 
   * Prefixes and suffixes are provided that will be output
   * respectively before and after the monitored value. Another pre-/suffix pair
   * is provided for the averaged value. For the averaged value the number of the 
   * last values over which the average is to be taken can be determined as well. 
   *
   * @param output_prefix A string holding the textual prefix that is to be output before
   * the measured value.
   * @param output_suffix A string holding the textual suffix that is to be output after
   * the measured value.
   * @param avg_output_prefix A string holding the textual prefix that is to be output before
   * the average of the measured value.
   * @param avg_output_suffix A string holding the textual suffix that is to be output after
   * the average of the measured value.
   * @param average_items The number of the most recent values over which the average is to be calculated.
   * @param RAID_ID The RAID device ID. This is meant for monitoring different RAIDs in a system. 
   *
   */
  MLUCValueMonitorRAID5( const char* output_prefix, 
			 const char* output_suffix, 
			 const char* avg_output_prefix, 
			 const char* avg_output_suffix, 
			 unsigned int average_items,
			 unsigned int RAID_ID );
	
  /**
   * @brief Constructor to enable only output to a file.
   * Using this constructor enables output to a file. 
   * The filename is passed to the constructor as well as a
   * string containing a description of the measurement that will be written 
   * to the beginning of the file. 
   * For the averaged value the number of the last values over which the average 
   * is to be taken can be determined as well. 
   *
   * @param filename A string holding the name of the file to which the values monitored are output. 
   * @param fileDescrHeader A string holding a description of the monitoring taking place. This string
   * will be written to the start of the output file. 
   * @param average_items The number of the most recent values over which the average is to be calculated.
   * @param RAID_ID The RAID device ID. This is meant for monitoring different RAIDs in a system. 
   * 
   */
  MLUCValueMonitorRAID5( const char* filename, 
			 const char* fileDescrHeader, 
			 unsigned int average_items, 
			 unsigned int RAID_ID );

  /**
   * @brief Constructor with no output.
   * For the averaged value the number of the last values over which the average 
   * is to be taken can be determined as well. 
   *
   * @param average_items The number of the most recent values over which the average is to be calculated.
   * @param cpu_number The ID of the cpu to be monitored. If no ID is given the overall CPU usage will
   * be taken.
   */
  MLUCValueMonitorRAID5( unsigned int average_items, 
			 unsigned int RAID_ID );
  
  /**
   * @brief Implementation of the Interface function from MLUCValueMonitor class.  
   * @param MeasuredValued Reference to the monitored value.
   * @return An integer holding a error return code corresponding to the C errno variable. (0 means success)
   */
  int GetValue( uint64& MeasuredValue );


  int GetNumberOfDevices( int ID, uint64& status );

  private:
  
  int fID;
  
};


#endif // _MLUCVALUEMONITORRAID5_HPP_
