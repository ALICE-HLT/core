/************************************************************************
 **
 **
 ** This file is property of and copyright by the Technical Computer
 ** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
 ** University, Heidelberg, Germany, 2001
 ** This file has been written by Timm Morten Steinbeck, 
 ** timm@kip.uni-heidelberg.de
 **
 **
 ** See the file license.txt for details regarding usage, modification,
 ** distribution and warranty.
 ** Important: This file is provided without any warranty, including
 ** fitness for any particular purpose.
 **
 **
 ** Newer versions of this file's package will be made available from 
 ** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
 ** or the corresponding page of the Heidelberg Alice Level 3 group.
 **
 *************************************************************************/
#ifndef _MLUCLOG_HPP_
#define _MLUCLOG_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCTypes.h"
#include "MLUCString.hpp"
#include "MLUCLRUList.hpp"
//#include "MLUCLogServer.hpp"
#include "MLUCThread.hpp"
#include <sys/utsname.h>
#include <vector>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <time.h>
#include <stdio.h>
#include <sys/param.h>
#include <netdb.h>
#ifndef NO_STACK_TRACE
#include <execinfo.h>
#endif


#if __GNUC__>=3
using namespace std;
#endif

#ifdef SYS_NMLN
#define MAX_HOSTNAME_LENGTH SYS_NMLN
#else
#ifdef HOST_NAME_MAX
#define MAX_HOSTNAME_LENGTH HOST_NAME_MAX
#else
#ifdef MAXHOSTNAMELEN
#define MAX_HOSTNAME_LENGTH MAXHOSTNAMELEN
#else
#error Do not know how to define maximum hostname length. Cannot find either SYS_NMLN (Linux) nor HOST_NAME_MAX or MAXHOSTNAMELEN (BSD/Darwin/Mac OS X)
#endif
#endif
#endif

#if 0
#define MLUCLOG_RUNNUMBER_SUPPORT
#endif

class MLUCLogServer;




/**
 * This is a general logging class. It features mutliple levels of
 * message severity (in a bitmask), with the supplied macros very small
 * overhead for not logging messages and multiple logging targets to which
 * properly processed (adding of host, time/date, compile time/date, source
 * file/line) are sent.
 *
 * @short A logging class
 * @author $Author$ - Initial version by Timm Morten Steinbeck
 * @version $Id$
 * @see MLUCLogServer
 */
class MLUCLog
    {
    public:

	/**
	 * enum for the different logging levels. These are set as a bitmask and a message
	 * will be logged, if the appropriate bit is set. Thus for example kDebug and kWarning 
	 * messages could be printed out while kInformational messages would be suppressed.
	 */
	enum TLogLevel { kNone = 0, kBenchmark = 0x01, kDebug = 0x02, kInformational = 0x04, kWarning = 0x08, 
			 kError = 0x10 , kFatal = 0x20, kImportant = 0x40, kPrimary = 0x80, kAll = 0xFF };


	/**
	 * Additional commands to be embedded into the logging stream
	 * for changing the output precision or the output format 
	 * between hex and dec.
	 */
	enum TLogCmd { kEnd, kPrec, kHex, kDec };

	
	class TLogMsg
	    {
	    protected:

		/**
		 * enum for the internal state machine.
		 *
		 * @see #fState
		 */
		enum TState { kIdle, kInLog, kWaitPrec };

	    public:

		TLogMsg():
		    fLog( NULL),
		    fState(kIdle),
		    fSequence(~(uint32)0),
		    fLine(""),
		    fOrigin(""),
		    fKeyword(""),
		    fCompileDate(""),
		    fCompileTime(""),
		    fLogLevel(MLUCLog::TLogLevel(0)),
		    fFile(""),
		    fLineNr(0),
		    fIsMod(false),
		    fModSequence(0),
		    fFormat(kEnd),
		    fPrec(0)
			{
			memset( fHostname, 0, MAX_HOSTNAME_LENGTH+2 );
			fLDate = 0;
			fLTime_s = 0;
			fLTime_us = 0;
			}
		TLogMsg( TLogMsg& lm )
			{
			fLog = lm.fLog;
			fState = lm.fState;
			fSequence = lm.fSequence;
			fLine = lm.fLine;
			fOrigin = lm.fOrigin;
			fKeyword = lm.fKeyword;
			memcpy( fHostname, lm.fHostname, sizeof(fHostname) );
			fCompileDate = lm.fCompileDate;
			fCompileTime = lm.fCompileTime;
			fLogLevel = lm.fLogLevel;
			fFile = lm.fFile;
			fLineNr = lm.fLineNr;
			fLDate = lm.fLDate;
			fLTime_s = lm.fLTime_s;
			fLTime_us = lm.fLTime_us;
			fIsMod = lm.fIsMod;
			fModSequence = lm.fModSequence;
			memcpy( fBuffer, lm.fBuffer, sizeof(fBuffer) );
			fFormat = lm.fFormat;
			fPrec = lm.fPrec;
			lm.fState = kIdle;
			lm.fLog = NULL;
			}
		~TLogMsg()
			{
			if ( fLog )
			    {
			    if ( fState==kInLog )
				FlushServers();
			    fLog->fPrec = fPrec;
			    fLog->fFormat = fFormat;
			    }
			}
		TLogMsg& operator=( TLogMsg& lm )
			{
			fLog = lm.fLog;
			fState = lm.fState;
			fSequence = lm.fSequence;
			fLine = lm.fLine;
			fOrigin = lm.fOrigin;
			fKeyword = lm.fKeyword;
			memcpy( fHostname, lm.fHostname, sizeof(fHostname) );
			fCompileDate = lm.fCompileDate;
			fCompileTime = lm.fCompileTime;
			fLogLevel = lm.fLogLevel;
			fFile = lm.fFile;
			fLineNr = lm.fLineNr;
			fLDate = lm.fLDate;
			fLTime_s = lm.fLTime_s;
			fLTime_us = lm.fLTime_us;
			fIsMod = lm.fIsMod;
			fModSequence = lm.fModSequence;
			memcpy( fBuffer, lm.fBuffer, sizeof(fBuffer) );
			fFormat = lm.fFormat;
			fPrec = lm.fPrec;
			lm.fState = kIdle;
			lm.fLog = NULL;
			return *this;
			}


		
		/**
		 * Stream operator for the TLogCmd commands (e.g. for changing precision
		 * or output format).
		 * 
		 * @return A reference to this object.
		 * @param cmd The TLogCmd cmd to stream into the log.
		 * @see #TLogCmd
		 */
		TLogMsg& operator << ( const TLogCmd& cmd )
			{
			switch ( cmd )
			    {
			    case kEnd: 
				{
				if ( fState==kInLog ) 
				    FlushServers(); 
				fState = kIdle; 
				//pthread_mutex_unlock( &fMutex );
				break;
				}
			    case kPrec: 
				if ( fState==kInLog )
				    fState = kWaitPrec; 
				break;
			    case kHex: 
			    case kDec: 
				fFormat = cmd; 
				break;
			    }
			return *this;
			}
		
		/**
		 * Stream operator to stream a character into the logging message.
		 *
		 * @return A reference to this object.
		 * @param c The character to stream into the log message.
		 */
		TLogMsg& operator << ( const char& c )
			{
			if ( fState==kIdle )
			    return *this;
			if ( fState==kWaitPrec )
			    fState = kInLog;
			fLine += c;
			return *this;
			}
		
		/**
		 * Stream operator to stream, a string (const char*) into the logging message.
		 *
		 * @return A reference to this object.
		 * @param c The const char* to stream into the log object.
		 */
		TLogMsg& operator << ( const char* c )
			{
			if ( fState==kIdle )
			    return *this;
			if ( fState==kWaitPrec )
			    fState = kInLog;
			fLine += c;
			return *this;
			}
		
		/**
		 * Stream operator to stream a string object into the logging object.
		 *
		 * @return A reference to this object.
		 * @param s The string to stream into the log object.
		 */
		// 	MLUCLog& operator << ( const string& s )
		// 	    {
		// 	    if ( fState==kIdle )
		// 		return *this;
		// 	    if ( fState==kWaitPrec )
		// 		fState = kInLog;
		// 	    fLine += s;
		// 	    return *this;
		// 	    }
		
		/**
		 * Stream operator to stream an int object into the logging object.
		 *
		 * @return A reference to this object.
		 * @param n The int to stream into the log object.
		 */
		TLogMsg& operator << ( signed int n )
			{
			return operator << ( (signed long long int)n );
			}

		/**
		 * Stream operator to stream a short int into the logging object.
		 *
		 * @return A reference to this object.
		 * @param n The short int to stream into the logging object.
		 */
		TLogMsg& operator << ( signed short int n )
			{
			return operator << ( (signed long long int)n );
			}

		/**
		 * Stream operator to stream a long int into the logging object.
		 *
		 * @return A reference to this object.
		 * @param n The long int to stream into the logging object.
		 */
		TLogMsg& operator << ( signed long int n )
			{
			return operator << ( (signed long long int)n );
			}

		TLogMsg& operator << ( signed long long int n )
			{
			if ( fState==kIdle )
			    return *this;
			if ( fState==kWaitPrec )
			    {
			    if ( n>=0 )
				fPrec = n;
			    fState = kInLog;
			    return *this;
			    }
			char* c = fBuffer;
			unsigned long long t;
			int s;
			bool dd = false;
			uint64 mod;
			uint64 i;
			if ( n < 0 )
			    {
			    *c++ = '-';
			    n *= -1;
			    }
			switch ( fFormat )
			    {
			    case kDec:
				mod = 1000000000000000000ULL;
				while ( mod > 0 )
				    {
				    if ( (t=n/mod) || dd)
					{
					*c++ = t+'0';
					dd = true;
					}
				    n -= mod*t;
				    mod /= 10;
				    }
				break;
			    case kHex:
				mod = 0xF000000000000000ULL;
				s = 60;
				while ( mod>0 )
				    {
				    if ( (t=n&mod) || dd )
					{
					t >>= s;
					dd = true;
					if ( t < 0xA )
					    *c++ = '0'+t;
					else
					    *c++ = 'A'+t-0xA;
					}
				    mod >>= 4;
				    s -= 4;
				    }
				break;
			    default:
				// Internal Error
				break;
			    }
			*c++ = 0;
			if ( fPrec>0 )
			    {
			    for ( i = c-fBuffer-1; i < fPrec; i++ )
				*(fBuffer+i+1) = '0';
			    *(fBuffer+i+1) = 0;
			    if ( (uint32)(c-fBuffer-1) > fPrec )
				{
				*(fBuffer+fPrec-1) = '>';
				*(fBuffer+fPrec)=0;
				}
			    fLine += c;
			    }
			else
			    {
			    if ( c==fBuffer+1 )
				{
				*fBuffer = '0';
				*(fBuffer+1) = 0;
				}
			    }
			fLine += fBuffer;
			return *this;
			}

		/**
		 * Stream operator to stream an unsigned int into the logging object.
		 *
		 * @return A reference to this object.
		 * @param n The unsigned int to stream into the logging object.
		 */
		TLogMsg& operator << ( unsigned int n )
			{
			return operator << ( (unsigned long long int)n );
			}

		/**
		 * Stream operator to stream an unsigned short int into the logging object.
		 *
		 * @return A reference to this object.
		 * @param n The unsigned short int to stream into the logging object.
		 */
		TLogMsg& operator << ( unsigned short int n )
			{
			return operator << ( (unsigned long long int)n );
			}

		/**
		 * Stream operator to stream an unsigned long int into the logging object.
		 *
		 * @return A reference to this object.
		 * @param n The unsigned long int to stream into the logging object.
		 */
		TLogMsg& operator << ( unsigned long int n )
			{
			return operator << ( (unsigned long long int)n );
			}

		TLogMsg& operator << ( unsigned long long int n )
			{
			if ( fState==kIdle )
			    return *this;
			if ( fState==kWaitPrec )
			    {
			    fPrec = n;
			    fState = kInLog;
			    return *this;
			    }
			char* c = fBuffer;
			unsigned long long t;
			int s;
			uint64 i, mod;
			bool dd = false;
			switch ( fFormat )
			    {
			    case kDec:
				mod = 10000000000000000000ULL;
				while ( mod > 0 )
				    {
				    if ( (t=n/mod) || dd )
					{
					*c++ = t+'0';
					dd = true;
					}
				    n -= mod*t;
				    mod /= 10;
				    }
				break;
			    case kHex:
				mod = 0xF000000000000000ULL;
				s = 60;;
				while ( mod>0 )
				    {
				    if ( (t=n&mod) || dd )
					{
					t >>= s;
					dd = true;
					if ( t < 0xA )
					    *c++ = '0'+t;
					else
					    *c++ = 'A'+t-0xA;
					}
				    mod >>= 4;
				    s -= 4;
				    }
				break;
			    default:
				// Internal error:
				break;
			    }
			*c++ = 0;
			if ( fPrec>0 )
			    {
			    for ( i = c-fBuffer-1; i < fPrec; i++ )
				*(fBuffer+i+1) = '0';
			    *(fBuffer+i+1) = 0;
			    if ( (uint32)(c-fBuffer-1) > fPrec )
				{
				*(fBuffer+fPrec-1) = '>';
				*(fBuffer+fPrec)=0;
				}
			    fLine += c;
			    }
			else
			    {
			    if ( c==fBuffer+1 )
				{
				*fBuffer = '0';
				*(fBuffer+1) = 0;
				}
			    }
			fLine += fBuffer;
			return *this;
			}

		/**
		 * Stream operator to stream a double into the logging object.
		 *
		 * @return A reference to this object.
		 * @param n The double to stream into the logging object.
		 */
		TLogMsg& operator << ( const double& d )
			{
			  //uint32 n;
			// 			if ( fState==kIdle )
			// 				return *this;
			if ( fState==kWaitPrec )
			    fState = kInLog;
			// 			n = sprintf( fBuffer, "%0*.*g", fPrec, fPrec, d );
			// 			if ( n > fPrec )
			// 				{
			// 				*(fBuffer-fPrec-1) = '>';
			// 				*(fBuffer+fPrec)=0;
			// 				}
			//n = 
			  sprintf( fBuffer, "%g", d );
			fLine += fBuffer;
			return *this;
			}




	    protected:
		TLogMsg( MLUCLog* myLog, TLogCmd format, uint32 prec, uint32 sequence, TLogLevel logLvl, const char* origin, const char* keyword, const char* file, int line, 
			 const char* compile_date, const char* compile_time, bool isMod=false, uint32 modSequence=0 ):
		    fLog( myLog),
		    fState(kInLog),
		    fSequence(sequence),
		    fLine(""),
		    fOrigin(origin),
		    fKeyword(keyword),
		    fCompileDate(compile_date),
		    fCompileTime(compile_time),
		    fLogLevel(logLvl),
		    fFile(file),
		    fLineNr(line),
		    fIsMod(isMod),
		    fModSequence(modSequence),
		    fFormat(format),
		    fPrec(prec)
			{
			if ( !fLog )
			    fState = kIdle;
			struct tm* t;
			//time_t tt;
			struct timeval tv;
			
			//time( &tt );
			gettimeofday( &tv, NULL );
			t = localtime( &(tv.tv_sec) );
			t->tm_mon++;
			t->tm_year += 1900;
			// #define VALGRIND_PARANOIA
#ifdef VALGRIND_PARANOIA
			memset( fHostname, 0, MAX_HOSTNAME_LENGTH+2 );
#endif
			gethostname( fHostname, MAX_HOSTNAME_LENGTH );
			fLDate = t->tm_year*10000+t->tm_mon*100+t->tm_mday;
			fLTime_s = t->tm_hour*10000+t->tm_min*100+t->tm_sec;
			fLTime_us = tv.tv_usec;
			}

		void FlushServers()
			{
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
			if ( fLog )
			    fLog->FlushServers( fOrigin.c_str(), fKeyword.c_str(), fFile, fLineNr, fCompileDate, fCompileTime,
						fLogLevel, fHostname, fLog->fID.c_str(), fLog->fRunNumber, fLDate, fLTime_s, fLTime_us, 
						fSequence, fIsMod, fModSequence, fLine.c_str() );
#else
			if ( fLog )
			    fLog->FlushServers( fOrigin.c_str(), fKeyword.c_str(), fFile, fLineNr, fCompileDate, fCompileTime,
						fLogLevel, fHostname, fLog->fID.c_str(), fLDate, fLTime_s, fLTime_us, 
						fSequence, fIsMod, fModSequence, fLine.c_str() );
#endif
			}

		/**
		 * Pointer to parent log object
		 */
		MLUCLog* fLog;

		/**
		 * The state that the (state machine of the) object is currently in.
		 *
		 * @see #TState
		 */
		TState fState;

		/**
		 * Sequence number of this message.
		 */
		uint32 fSequence;

		/**
		 * The member variable containing the log message itself. Every 
		 * element streamed into this logging object gets added to this
		 * variable.
		 */
		MLUCString fLine;

		/**
		 * string holding the origin description of the current message
		 */
		MLUCString fOrigin;

		/**
		 * string holding the keyword description of the current message
		 */
		MLUCString fKeyword;

		/**
		 * Variable used to store the hostname of this (the originating) host
		 */
		char fHostname[ MAX_HOSTNAME_LENGTH+2 ];

		/**
		 * Variable to store the compile date of the originating source code.
		 */
		const char* fCompileDate;
		
		/**
		 * Variable to store the compile time of the originating source code.
		 */
		const char* fCompileTime;

		/**
		 * Variable to store the log level of the actual message.
		 */
		TLogLevel fLogLevel;

		/**
		 * Variable to store the name of the originating source file.
		 */
		const char* fFile;
		
		/**
		 * Variable to store the originating source code line.
		 */
		int fLineNr;
		
		/**
		 * Variable to store the date at which the message was generated.
		 */
		uint32 fLDate;
		
		/**
		 * Variable to store the time at which the message was generated (sec. part).
		 */
		uint32 fLTime_s;
		
		/**
		 * Variable to store the time at which the message was generated (usec. part).
		 */
		uint32 fLTime_us;

		/**
		 * Variable to store wether the actual message is to be treated in the 
		 * modulo number way...
		 *
		 * @see #fModSequence
		 */
		bool fIsMod;
		
		/**
		 * The current modulo sequence number for the current message.
		 * Valid only if fIsMod is true.
		 *
		 * @see #fIsMod
		 */
		uint32 fModSequence;

		/**
		 * Buffer used in converting streamed elements to strings.
		 */
		char fBuffer[256];

		/**
		 * The format that is currently to be used for printing out integer numbers.
		 * Can be either kDec or kHex.
		 *
		 * @see #TLogCmd
		 */
		TLogCmd fFormat;
		
		/**
		 * The current precision/width to use for number output.
		 */
		uint32 fPrec;

		    friend class MLUCLog;

	    private:
		
		TLogMsg& operator=( const TLogMsg& );
		TLogMsg( const TLogMsg& lm );

		
	    };


	/**
	 * The class global loglevel variable.
	 * The actually used loglevel variable is just a 
	 * reference, which by default is set to this variable.
	 */
	//static TLogLevel gLogLevel;
	static int gLogLevel;

	/**
	 * The class global logging object.
	 * The actually used logging object is just a
	 * reference, which by default is set to this object.
	 */
	static MLUCLog gLog;


	/**
	 * Constructor.
	 */
	MLUCLog();

	/**
	 * Destructor.
	 */
	~MLUCLog();

	/**
	 * Set the ID/name of this log instance (respectively this process).
	 * This name will appear in all the log message to be able to identify
	 * processes.
	 *
	 * @return void
	 * @param id The ID to set for this process.
	 */
	void SetID( const char* id )
		{
		fID = id;
		}

#ifdef MLUCLOG_RUNNUMBER_SUPPORT
	/** 
	 * Set the number of the current run to be used in successive log messages
	 * for this process.
	 *
	 * @return void
	 * @param nr The runnumber to be used
	 */
	void SetRunNumber( const unsigned long nr )
		{
		fRunNumber = nr;
		}

	/** 
	 * Reset the number of the current run to be used in successive log messages
	 * for this process. This signals that no valid run is currently active.
	 *
	 * @return void
	 */
	void ResetRunNumber()
		{
		fRunNumber = 0;
		}
#endif

	bool SetStackTraceComparisonDepth( unsigned long depth );

	void SetStackTraceReductionMinTimeLimit( unsigned long long time_limit_us )
		{
		fStackTraceReductionMinTimeLimit_us = time_limit_us;
		}
	unsigned long long GetStackTraceReductionMinTimeLimit() const
		{
		return fStackTraceReductionMinTimeLimit_us;
		}
	

	/**
	 * Method to add a logging server, to which processed messages are 
	 * passed. 
	 *
	 * @return void
	 * @param server Pointer to a MLUCLogServer object to add to the list.
	 * @see DelServer
	 */
	void AddServer( MLUCLogServer* server );

	/**
	 * Method to add a logging server, to which processed messages are 
	 * passed. 
	 *
	 * @return void
	 * @param server Reference to a MLUCLogServer object to add to the list.
	 * @see DelServer
	 */
	void AddServer( MLUCLogServer& server )
		{
		AddServer( &server );
		}

	/**
	 * Method to remove a previously added logging server from the list.
	 *
	 * @return void
	 * @param server Pointer to the logging server to remove from the list.
	 * @see AddServer
	 */
	void DelServer( MLUCLogServer* server );

	/**
	 * Method to remove a previously added logging server from the list.
	 *
	 * @return void
	 * @param server Reference to the logging server to remove from the list.
	 * @see AddServer
	 */
	void DelServer( MLUCLogServer& server )
		{
		DelServer( &server );
		}

	/**
	 * The method called when a new logging message is to be started. 
	 * There should be no need to call this method manually, this should
	 * be done by the LOG macro below.
	 *
	 * @return A reference to this object.
	 * @param logLvl The severity loglevel of this message. One of the above TLogLevel types.
	 * @param origin A string holding a description of the originating code/object/class or module of this message.
	 * @param keyword A string holding a keyword describing the kind of error that occured. Intended for machine reading purposes.
	 * @param file A const char* to the source file from which the call originates. In the LOG macro this is 
	 * the result of the __FILE__ preprocessor directive.
	 * @param line The line number of the line in the source file from which the call originates. In the LOG macro this is 
	 * the result of the __LINE__ preprocessor directive.
	 * @param compile_date The date at which the originating source code was compiled as a const char*. In the LOG macro this is
	 * a result of the __DATE__ preprocessor directive.
	 * @param compile_time The time at which the originating source code was compiled as a const char*. In the LOG macro this is
	 * a result of the __TIME__ preprocessor directive.
	 * @see #TLogLevel
	 * @see #LOG
	 */
	TLogMsg LogMsg( TLogLevel logLvl, const char* origin, const char* keyword, const char* file, int line, 
			const char* compile_date, const char* compile_time )
		{
		pthread_mutex_lock( &fMutex );
		return StartLog( logLvl, origin, keyword, file, line, compile_date, compile_time );
		}

	/**
	 * The method called when a new logging message is to be started. 
	 * There should be no need to call this method manually, this should
	 * be done by the LOGM macro below. This method has a parameter modulo which
	 * serves as a restriction mechanism to slow down floods of the same message.
	 * If this modulo parameter is present, every message from this call (determined 
	 * by source file/line) up to the specified modulo number will be printed out, and 
	 * from then on until the restart of the system, only every message that is cleanly 
	 * divisible by the specified modulo number will be printed. All printed messages will
	 * contain the sequence number of this message.
	 *
	 * @return A reference to this object.
	 * @param logLvl The severity loglevel of this message. One of the above TLogLevel types.
	 * @param origin A string holding a description of the originating code/object/class or module of this message.
	 * @param keyword A string holding a keyword describing the kind of error that occured. Intended for machine reading purposes.
	 * @param file A const char* to the source file from which the call originates. In the LOGM macro this is 
	 * the result of the __FILE__ preprocessor directive.
	 * @param line The line number of the line in the source file from which the call originates. In the LOGM macro this is 
	 * the result of the __LINE__ preprocessor directive.
	 * @param compile_date The date at which the originating source code was compiled as a const char*. In the LOGM macro this is
	 * a result of the __DATE__ preprocessor directive.
	 * @param compile_time The time at which the originating source code was compiled as a const char*. In the LOGM macro this is
	 * a result of the __TIME__ preprocessor directive.
	 * @param modulo The modulo number used for suppresion of message floods. (See method description)
	 * @see #TLogLevel
	 * @see #LOGM
	 */
	TLogMsg LogMsg( TLogLevel logLvl, const char* origin, const char* keyword, const char* file, int line, 
			const char* compile_date, const char* compile_time, uint32 modulo )
		{
		pthread_mutex_lock( &fMutex );
		uint32 modL, i, n;
		// XXX
		modL = line % 256;
		TModMsgDataVector& data = fModMsgData[modL];
		n = data.size();
		bool found=false;
		uint32 modSequence=0;
		for ( i = 0; i < n; i++ )
		    {
		    if ( (data[i].fLine == line) && !strcmp( data[i].fFile, file ) )
			{
			found = true;
			modSequence = data[i].fCount++;
			break;
			}
		    }
		if ( !found )
		    {
		    TModMsgData dat;
		    dat.fFile = file;
		    dat.fLine = line;
		    dat.fModulo = modulo;
		    modSequence = 0;
		    dat.fCount = 1;
		    data.insert( data.end(), dat );
		    i = n;
		    }

		bool modActive = true;
		if ( (modSequence > modulo) && (modSequence % modulo) )
		    modActive = false;
		return StartLog( logLvl, origin, keyword, file, line, 
				 compile_date, compile_time, true, modSequence, modActive );
		}

    protected:

        struct TStackTraceElement
            {
            public:
                void** fStackTrace;
                unsigned long fStackTraceDepth;
                struct timeval fLastOccurance;
                unsigned long fCnt;
                unsigned long fLastLogCnt;
                TStackTraceElement()
                        {
                        fStackTrace=NULL;
                        fStackTraceDepth=0;
                        fCnt=0;
                        fLastLogCnt = 0;
                        fLastOccurance.tv_sec = fLastOccurance.tv_usec = 0;
                        }
                TStackTraceElement( const TStackTraceElement& copy )
                        {
                        fStackTrace = new void*[ copy.fStackTraceDepth ];
                        fStackTraceDepth= fStackTrace ? copy.fStackTraceDepth : 0;
                        if ( fStackTrace )
                            memcpy( fStackTrace, copy.fStackTrace, sizeof(void*)*fStackTraceDepth );
                        fCnt=copy.fCnt;
                        fLastLogCnt = copy.fLastLogCnt;
                        fLastOccurance = copy.fLastOccurance;
                        }
                ~TStackTraceElement()
                        {
                        if ( fStackTrace )
                            delete [] fStackTrace;
                        fStackTrace=NULL;
                        }
                bool operator==( const TStackTraceElement& comp )
                        {
                        unsigned long cnt= comp.fStackTraceDepth<fStackTraceDepth ? comp.fStackTraceDepth : fStackTraceDepth;
                        for ( unsigned long ii=0;ii<cnt;ii++ )
                            {
                            if ( comp.fStackTrace[ii] != fStackTrace[ii] )
                                return false;
                            }
                        return true;
                        }
                TStackTraceElement& operator=( const TStackTraceElement& copy )
                        {
                        if ( fStackTrace )
                            delete [] fStackTrace;
                        fStackTrace = new void*[ copy.fStackTraceDepth ];
                        fStackTraceDepth= fStackTrace ? copy.fStackTraceDepth : 0;
                        if ( fStackTrace )
                            memcpy( fStackTrace, copy.fStackTrace, sizeof(void*)*fStackTraceDepth );
                        fCnt=copy.fCnt;
                        fLastLogCnt = copy.fLastLogCnt;
                        return *this;
                        }
            };
        

	TLogMsg StartLog( TLogLevel logLvl, const char* origin, const char* keyword, const char* file, int line, 
			  const char* compile_date, const char* compile_time, bool isMod=false, uint32 modSequence=0, bool modActive=true )
		{
		uint32 sequence = 0;
		MLUCTHREAD_PUSH_CLEANUP_FUNC( MLUCThread::MutexUnlockCleanup, &fMutex );
		pthread_mutex_lock( &fMutex );
		sequence = ++fSequence;
		//pthread_mutex_unlock( &fMutex );
		MLUCTHREAD_POP_CLEANUP_FUNC( true );
		return TLogMsg( modActive  ? this : NULL, fFormat, fPrec, sequence, logLvl, origin, keyword, file, line, 
				compile_date, compile_time, isMod, modSequence );
		}
			
	/**
	 * Method to flush the recored logging message to all 
	 * connected (added) servers.
	 *
	 * @return void
	 */
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
	void FlushServers( const char* origin, const char* keyword, const char* file, int linenr, 
			   const char* compile_date, const char* compile_time, MLUCLog::TLogLevel, 
			   const char* hostname, const char* id, unsigned long runNumber, uint32 ldate, 
			   uint32 ltime_s, uint32 ltime_us, uint32 msgNr, bool isMod, uint32 subMsgNr, 
			   const char* line );
#else
	void FlushServers( const char* origin, const char* keyword, const char* file, int linenr, 
			   const char* compile_date, const char* compile_time, MLUCLog::TLogLevel, 
			   const char* hostname, const char* id, uint32 ldate, 
			   uint32 ltime_s, uint32 ltime_us, uint32 msgNr, bool isMod, uint32 subMsgNr, 
			   const char* line );
#endif

	/**
	 * The id of the log object (respectively this process).
	 */
	MLUCString fID;

#ifdef MLUCLOG_RUNNUMBER_SUPPORT
	/**
	 * The number of the current run to be used for the log object (respectively this process).
	 * The default value of 0 means that no valid run is currently active.
	 */
	unsigned long fRunNumber;
#endif

	/**
	 * The format that is currently to be used for printing out integer numbers.
	 * Can be either kDec or kHex.
	 *
	 * @see #TLogCmd
	 */
	TLogCmd fFormat;

	/**
	 * The current precision/width to use for number output.
	 */
	uint32 fPrec;

	/**
	 * An array of pointers to MLUCLogServer objects.
	 * Each of these log server objects gets each complete log message.
	 * Final element is NULL as delimiter
	 *
	 * @see MLUCLogServer
	 *
	 * @link aggregation 
	 * @supplierCardinality 0..*
	 * @directed
	 */
	MLUCLogServer** fServers;
	//vector<MLUCLogServer*> fServers;

	/**
	 * Counter for the number of messages logged.
	 * Gets increased for every message sent to this object.
	 */
	uint32 fSequence;

	/**
	 * Struct holding the data for the modulo messages.
	 * This includes the file and line number for identification,
	 * the modulo number to use and the actual count of messages
	 * already processed.
	 */
	struct TModMsgData
	    {
		const char* fFile;
		int fLine;
		uint32 fModulo;
		uint32 fCount;
	    };

	/**
	 * Simple typedef...
	 */
	typedef struct TModMsgData TModMsgData;
		
	/**
	 * Simple typedef so that kdoc recognizes the 
	 * following array definition.
	 */
	typedef vector<TModMsgData> TModMsgDataVector;

	/**
	 * Array for the modulo messages.
	 * Messages are stored (line number % 256) in a vector
	 * and then searched for (by full line number, file name) 
	 * in that vector.
	 */
	TModMsgDataVector fModMsgData[256];


    private:
    public:
	/**
	 * mutex semaphore protecting access to the log server.
	 * Also provides a way to enforce ENDLOG usage, since it will be locked
	 * when starting a new message and unlocked when putting the ENDLOG marker...
	 * Please note: it is only public because of some cleanup function difficulties.
	 * DO NOT USE THIS MUTEX EXTERNALLY!!!!!!!!!!
	 */
	pthread_mutex_t fMutex;

    protected:
        
        /**
         * # of stack entries to be skipped
         * Constant holding the number of stack traces entries
         * to be skipped at the beginning of the stack (close to calling function)
         * because they are identical anyway (inernal to logging system)
         */
        static const unsigned gkStackTraceSkipLevels;

        /**
         * Number of stack trace elements/return addresses to use for log message reduction
         * The given number of strack trace return addresses will be used for comparisons
         * to determine whether a given log call/message comes from the same origin in the code.
         */
        unsigned long fStackTraceComparisonDepth;
        
        /**
         * Pointer to buffer used for stack trace comparison
         * The buffer pointed to by the member will be used to store the call stack elements/return
         * addresses used for comparisons to determine whether a given log call/message comes from
         * the same origin in the code.
         */
        void** fStackTraceComparisonBuffer;

        /**
         * Element used for comparison and submitting new elements to lru list
         */
        TStackTraceElement fStackTraceComparisonElement;
        
        /**
         * LRU list of the most recently used stacktrace locations
         * This is a least-recently-used list of the last N locations from which
         * logging calls were made.
         */
        MLUCLRUList<TStackTraceElement> fStackTraceLRUList;

        unsigned long long fStackTraceReductionMinTimeLimit_us;

        unsigned long long fStackTraceReductionStart;
        unsigned long long fStackTraceReductionIncrement;
    };



/**
 * Simplest logging macro. Logs every occuring message. Parameters are loggng level, origin description (string/char*)
 * and keyword (string/char*).
 * Other message content is then streamed "into this macro" followed by an ENDLOG.
 */
#define LOG( lvl, origin, keyword ) if (gLogLevel & (lvl)) { MLUCTHREAD_PUSH_CLEANUP_FUNC( MLUCThread::MutexUnlockCleanup, &gLog.fMutex ); gLog.LogMsg( (lvl), origin, keyword, __FILE__, __LINE__, __DATE__,  __TIME__ )
#define LOGC( logLevels, lvl, origin, keyword ) if ((logLevels) & (lvl)) { MLUCTHREAD_PUSH_CLEANUP_FUNC( MLUCThread::MutexUnlockCleanup, &gLog.fMutex ); gLog.LogMsg( (lvl), origin, keyword, __FILE__, __LINE__, __DATE__,  __TIME__ )
#define ROOTLOG( lvl, origin, keyword ) if (*gLogLevelP & (lvl)) { MLUCTHREAD_PUSH_CLEANUP_FUNC( MLUCThread::MutexUnlockCleanup, &gLogP->fMutex ); gLogP->LogMsg( lvl, origin, keyword, __FILE__, __LINE__, __DATE__,  __TIME__ )

#define LOGD( lvl, origin, keyword ) if (gLogLevelP & (lvl)) { MLUCTHREAD_PUSH_CLEANUP_FUNC( MLUCThread::MutexUnlockCleanup, &gLog.fMutex ); gLog.LogMsg( lvl, origin, keyword, __FILE__, __LINE__, __DATE__,  __TIME__ )

//#define GETLOG(lvl, origin, keyword ) gLog.LogMsg( (lvl), origin, keyword, __FILE__, __LINE__, __DATE__,  __TIME__ )
#define LOGG( lvl, origin, keyword, logObj ) if (gLogLevel & (lvl)) { MLUCTHREAD_PUSH_CLEANUP_FUNC( MLUCThread::MutexUnlockCleanup, &gLog.fMutex ); MLUCLog::TLogMsg logObj = gLog.LogMsg( (lvl), origin, keyword, __FILE__, __LINE__, __DATE__,  __TIME__ )
#define LOGCG( logLevels, lvl, origin, keyword, logObj ) if ((logLevels) & (lvl)) { MLUCTHREAD_PUSH_CLEANUP_FUNC( MLUCThread::MutexUnlockCleanup, &gLog.fMutex ); MLUCLog::TLogMsg logObj = gLog.LogMsg( (lvl), origin, keyword, __FILE__, __LINE__, __DATE__,  __TIME__ )

/**
 * Logging macro for modulo messages. Logs every occuring message until the specified modulo number and from then on
 * only every modulo'nth message.. Parameters are logging level, origin description (string/char*), keyword (string/char*).
 * and the modulo number to use. 
 * Other message content is then streamed "into this macro" followed by an ENDLOG.
 */
#define LOGM( lvl, origin, keyword, modulo ) if (gLogLevel & (lvl)) { MLUCTHREAD_PUSH_CLEANUP_FUNC( MLUCThread::MutexUnlockCleanup, &gLog.fMutex ); gLog.LogMsg( (lvl), (const char*)origin, (const char*)keyword, __FILE__, __LINE__, __DATE__,  __TIME__, (uint32)modulo )
#define LOGMC( logLevels, lvl, origin, keyword, modulo ) if ((logLevels) & (lvl)) { MLUCTHREAD_PUSH_CLEANUP_FUNC( MLUCThread::MutexUnlockCleanup, &gLog.fMutex ); gLog.LogMsg( (lvl), (const char*)origin, (const char*)keyword, __FILE__, __LINE__, __DATE__,  __TIME__, (uint32)modulo )
#define ROOTLOGM( lvl, origin, keyword, modulo ) if (*gLogLevelP & (lvl)) { MLUCTHREAD_PUSH_CLEANUP_FUNC( MLUCThread::MutexUnlockCleanup, &gLogP->fMutex ); gLogP->LogMsg( lvl, (const char*)origin, (const char*)keyword, __FILE__, __LINE__, __DATE__,  __TIME__, (uint32)modulo )


#define LOGMD( lvl, origin, keyword, modulo ) if (gLogLevel & (lvl)) { MLUCTHREAD_PUSH_CLEANUP_FUNC( MLUCThread::MutexUnlockCleanup, &gLog.fMutex ); gLog.LogMsg( lvl, (const char*)origin, (const char*)keyword, __FILE__, __LINE__, __DATE__,  __TIME__, (uint32)modulo )

#define DOLOG(lvl) (gLogLevel & (lvl))

#define DOLOGC(loglevels, lvl) ((loglevels) & (lvl))


/**
 * The ENDLOG macro which has to be used to finish a logging message.
 */
#define ENDLOG (MLUCLog::kEnd) ; MLUCTHREAD_POP_CLEANUP_FUNC( true ); }

/**
 * Global reference which specifies the log levels to use. This is a bitfield
 * consisting of the different loglevels which can thus be set/unset independently.
 */
//extern MLUCLog::TLogLevel& gLogLevel;
extern int& gLogLevel;

extern int* gLogLevelP;

/**
 * Global reference to the logging object which gets used by the LOG/LOGM macros
 * for logging messages. Can be changed if needed.
 */
extern MLUCLog& gLog;

extern MLUCLog* gLogP;



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _MLUCLOG_HPP_
