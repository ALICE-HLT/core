#ifndef _MLUCVALUEMONITORMEM_HPP_
#define _MLUCVALUEMONITORMEM_HPP_

#include "MLUCValueMonitor.hpp"
#include "MLUCString.hpp"
#include "MLUCLog.hpp"

#include <cstdlib>
#include <errno.h>
#include <sstream>

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

const unsigned MEMTOTAL  = 0;
const unsigned MEMFREE   = 1;
const unsigned MEMUSED   = 2;
const unsigned MEMSHARED = 3;
const unsigned BUFFERS   = 4;
const unsigned CACHED    = 5;
const unsigned SWAPTOTAL = 6;
const unsigned SWAPFREE  = 7;
const unsigned SWAPUSED  = 8;

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

int GetMemVal(unsigned MemType, unsigned &val);

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

class MLUCValueMonitorMem_Total : public MLUCValueMonitor {
public:
 /**
  * @brief Constructor to enable output to file and stdout.
  * Using this constructor enables output to a file as well as to the
  * standard output. The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written
  * to the beginning of the file.
  * For output to stdout prefixes and suffixes are provided that will be output
  * respectively before and after the monitored value. Another pre-/suffix pair
  * is provided for the averaged value. For the averaged value the number of the
  * last values over which the average is to be taken can be determined as well.
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output.
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param output_prefix A string holding the textual prefix that is to be
  * output before the measured value.
  * @param output_suffix A string holding the textual suffix that is to be
  * output after the measured value.
  * @param avg_output_prefix A string holding the textual prefix that is to be
  * output before the average of the measured value.
  * @param avg_output_suffix A string holding the textual suffix that is to be
  * output after the average of the measured value.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorMem_Total( const char* filename,
				const char* fileDescrHeader,
				const char* output_prefix,
				const char* output_suffix,
				const char* avg_output_prefix,
				const char* avg_output_suffix,
				unsigned int average_items) :
    MLUCValueMonitor( filename,
		      fileDescrHeader,
		      output_prefix,
		      output_suffix,
		      avg_output_prefix,
		      avg_output_suffix,
		      average_items )
  {
    Init();
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file.
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written
  * to the beginning of the file.
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well.
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output.
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorMem_Total( const char* output_prefix,
				const char* output_suffix,
				const char* avg_output_prefix,
				const char* avg_output_suffix,
				unsigned int average_items):
    MLUCValueMonitor( output_prefix,
		      output_suffix,
		      avg_output_prefix,
		      avg_output_suffix,
		      average_items )
  {
    Init();
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file.
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written
  * to the beginning of the file.
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well.
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output.
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorMem_Total( const char* filename,
				const char* fileDescrHeader,
				unsigned int average_items):
    MLUCValueMonitor( filename, fileDescrHeader, average_items )
  {
    Init();
  };

 /**
  * @brief Constructor with no output.
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well.
  *
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorMem_Total( unsigned int average_items):
    MLUCValueMonitor( average_items )
  {
    Init();
  };

  int GetValue( uint64& MeasuredValue );

protected:


private:
  /**
   * Identifies the type of the memory which will be monitored.
   */
  unsigned fMemType;

  /**
  * Will be executed in each constructor. This is just for convenience
  */
  void Init();
};

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

class MLUCValueMonitorMem_Free : public MLUCValueMonitor {
public:
 /**
  * @brief Constructor to enable output to file and stdout.
  * Using this constructor enables output to a file as well as to the
  * standard output. The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written
  * to the beginning of the file.
  * For output to stdout prefixes and suffixes are provided that will be output
  * respectively before and after the monitored value. Another pre-/suffix pair
  * is provided for the averaged value. For the averaged value the number of the
  * last values over which the average is to be taken can be determined as well.
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output.
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param output_prefix A string holding the textual prefix that is to be
  * output before the measured value.
  * @param output_suffix A string holding the textual suffix that is to be
  * output after the measured value.
  * @param avg_output_prefix A string holding the textual prefix that is to be
  * output before the average of the measured value.
  * @param avg_output_suffix A string holding the textual suffix that is to be
  * output after the average of the measured value.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorMem_Free( const char* filename,
				const char* fileDescrHeader,
				const char* output_prefix,
				const char* output_suffix,
				const char* avg_output_prefix,
				const char* avg_output_suffix,
				unsigned int average_items) :
    MLUCValueMonitor( filename,
		      fileDescrHeader,
		      output_prefix,
		      output_suffix,
		      avg_output_prefix,
		      avg_output_suffix,
		      average_items )
  {
    Init();
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file.
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written
  * to the beginning of the file.
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well.
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output.
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorMem_Free( const char* output_prefix,
				const char* output_suffix,
				const char* avg_output_prefix,
				const char* avg_output_suffix,
				unsigned int average_items):
    MLUCValueMonitor( output_prefix,
		      output_suffix,
		      avg_output_prefix,
		      avg_output_suffix,
		      average_items )
  {
    Init();
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file.
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written
  * to the beginning of the file.
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well.
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output.
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorMem_Free( const char* filename,
				const char* fileDescrHeader,
				unsigned int average_items):
    MLUCValueMonitor( filename, fileDescrHeader, average_items )
  {
    Init();
  };

 /**
  * @brief Constructor with no output.
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well.
  *
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorMem_Free( unsigned int average_items):
    MLUCValueMonitor( average_items )
  {
    Init();
  };

  int GetValue( uint64& MeasuredValue );

protected:


private:
  /**
   * Identifies the type of the memory which will be monitored.
   */
  unsigned fMemType;

  /**
  * Will be executed in each constructor. This is just for convenience
  */
  void Init();
};

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

class MLUCValueMonitorMem_Used : public MLUCValueMonitor {
public:
 /**
  * @brief Constructor to enable output to file and stdout.
  * Using this constructor enables output to a file as well as to the
  * standard output. The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written
  * to the beginning of the file.
  * For output to stdout prefixes and suffixes are provided that will be output
  * respectively before and after the monitored value. Another pre-/suffix pair
  * is provided for the averaged value. For the averaged value the number of the
  * last values over which the average is to be taken can be determined as well.
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output.
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param output_prefix A string holding the textual prefix that is to be
  * output before the measured value.
  * @param output_suffix A string holding the textual suffix that is to be
  * output after the measured value.
  * @param avg_output_prefix A string holding the textual prefix that is to be
  * output before the average of the measured value.
  * @param avg_output_suffix A string holding the textual suffix that is to be
  * output after the average of the measured value.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorMem_Used( const char* filename,
				const char* fileDescrHeader,
				const char* output_prefix,
				const char* output_suffix,
				const char* avg_output_prefix,
				const char* avg_output_suffix,
				unsigned int average_items) :
    MLUCValueMonitor( filename,
		      fileDescrHeader,
		      output_prefix,
		      output_suffix,
		      avg_output_prefix,
		      avg_output_suffix,
		      average_items )
  {
    Init();
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file.
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written
  * to the beginning of the file.
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well.
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output.
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorMem_Used( const char* output_prefix,
				const char* output_suffix,
				const char* avg_output_prefix,
				const char* avg_output_suffix,
				unsigned int average_items):
    MLUCValueMonitor( output_prefix,
		      output_suffix,
		      avg_output_prefix,
		      avg_output_suffix,
		      average_items )
  {
    Init();
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file.
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written
  * to the beginning of the file.
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well.
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output.
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorMem_Used( const char* filename,
				const char* fileDescrHeader,
				unsigned int average_items):
    MLUCValueMonitor( filename, fileDescrHeader, average_items )
  {
    Init();
  };

 /**
  * @brief Constructor with no output.
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well.
  *
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorMem_Used( unsigned int average_items):
    MLUCValueMonitor( average_items )
  {
    Init();
  };

  int GetValue( uint64& MeasuredValue );

protected:


private:
  /**
   * Identifies the type of the memory which will be monitored.
   */
  unsigned fMemType;

  /**
  * Will be executed in each constructor. This is just for convenience
  */
  void Init();
};

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

class MLUCValueMonitorMem_Shared : public MLUCValueMonitor {
public:
 /**
  * @brief Constructor to enable output to file and stdout.
  * Using this constructor enables output to a file as well as to the
  * standard output. The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written
  * to the beginning of the file.
  * For output to stdout prefixes and suffixes are provided that will be output
  * respectively before and after the monitored value. Another pre-/suffix pair
  * is provided for the averaged value. For the averaged value the number of the
  * last values over which the average is to be taken can be determined as well.
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output.
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param output_prefix A string holding the textual prefix that is to be
  * output before the measured value.
  * @param output_suffix A string holding the textual suffix that is to be
  * output after the measured value.
  * @param avg_output_prefix A string holding the textual prefix that is to be
  * output before the average of the measured value.
  * @param avg_output_suffix A string holding the textual suffix that is to be
  * output after the average of the measured value.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorMem_Shared( const char* filename,
				const char* fileDescrHeader,
				const char* output_prefix,
				const char* output_suffix,
				const char* avg_output_prefix,
				const char* avg_output_suffix,
				unsigned int average_items) :
    MLUCValueMonitor( filename,
		      fileDescrHeader,
		      output_prefix,
		      output_suffix,
		      avg_output_prefix,
		      avg_output_suffix,
		      average_items )
  {
    Init();
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file.
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written
  * to the beginning of the file.
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well.
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output.
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorMem_Shared( const char* output_prefix,
				const char* output_suffix,
				const char* avg_output_prefix,
				const char* avg_output_suffix,
				unsigned int average_items):
    MLUCValueMonitor( output_prefix,
		      output_suffix,
		      avg_output_prefix,
		      avg_output_suffix,
		      average_items )
  {
    Init();
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file.
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written
  * to the beginning of the file.
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well.
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output.
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorMem_Shared( const char* filename,
				const char* fileDescrHeader,
				unsigned int average_items):
    MLUCValueMonitor( filename, fileDescrHeader, average_items )
  {
    Init();
  };

 /**
  * @brief Constructor with no output.
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well.
  *
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorMem_Shared( unsigned int average_items):
    MLUCValueMonitor( average_items )
  {
    Init();
  };

  int GetValue( uint64& MeasuredValue );

protected:


private:
  /**
   * Identifies the type of the memory which will be monitored.
   */
  unsigned fMemType;

  /**
  * Will be executed in each constructor. This is just for convenience
  */
  void Init();
};

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

class MLUCValueMonitorMem_Buffers : public MLUCValueMonitor {
public:
 /**
  * @brief Constructor to enable output to file and stdout.
  * Using this constructor enables output to a file as well as to the
  * standard output. The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written
  * to the beginning of the file.
  * For output to stdout prefixes and suffixes are provided that will be output
  * respectively before and after the monitored value. Another pre-/suffix pair
  * is provided for the averaged value. For the averaged value the number of the
  * last values over which the average is to be taken can be determined as well.
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output.
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param output_prefix A string holding the textual prefix that is to be
  * output before the measured value.
  * @param output_suffix A string holding the textual suffix that is to be
  * output after the measured value.
  * @param avg_output_prefix A string holding the textual prefix that is to be
  * output before the average of the measured value.
  * @param avg_output_suffix A string holding the textual suffix that is to be
  * output after the average of the measured value.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorMem_Buffers( const char* filename,
				const char* fileDescrHeader,
				const char* output_prefix,
				const char* output_suffix,
				const char* avg_output_prefix,
				const char* avg_output_suffix,
				unsigned int average_items) :
    MLUCValueMonitor( filename,
		      fileDescrHeader,
		      output_prefix,
		      output_suffix,
		      avg_output_prefix,
		      avg_output_suffix,
		      average_items )
  {
    Init();
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file.
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written
  * to the beginning of the file.
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well.
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output.
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorMem_Buffers( const char* output_prefix,
				const char* output_suffix,
				const char* avg_output_prefix,
				const char* avg_output_suffix,
				unsigned int average_items):
    MLUCValueMonitor( output_prefix,
		      output_suffix,
		      avg_output_prefix,
		      avg_output_suffix,
		      average_items )
  {
    Init();
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file.
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written
  * to the beginning of the file.
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well.
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output.
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorMem_Buffers( const char* filename,
				const char* fileDescrHeader,
				unsigned int average_items):
    MLUCValueMonitor( filename, fileDescrHeader, average_items )
  {
    Init();
  };

 /**
  * @brief Constructor with no output.
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well.
  *
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorMem_Buffers( unsigned int average_items):
    MLUCValueMonitor( average_items )
  {
    Init();
  };

  int GetValue( uint64& MeasuredValue );

protected:


private:
  /**
   * Identifies the type of the memory which will be monitored.
   */
  unsigned fMemType;

  /**
  * Will be executed in each constructor. This is just for convenience
  */
  void Init();
};

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

class MLUCValueMonitorMem_Cached : public MLUCValueMonitor {
public:
 /**
  * @brief Constructor to enable output to file and stdout.
  * Using this constructor enables output to a file as well as to the
  * standard output. The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written
  * to the beginning of the file.
  * For output to stdout prefixes and suffixes are provided that will be output
  * respectively before and after the monitored value. Another pre-/suffix pair
  * is provided for the averaged value. For the averaged value the number of the
  * last values over which the average is to be taken can be determined as well.
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output.
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param output_prefix A string holding the textual prefix that is to be
  * output before the measured value.
  * @param output_suffix A string holding the textual suffix that is to be
  * output after the measured value.
  * @param avg_output_prefix A string holding the textual prefix that is to be
  * output before the average of the measured value.
  * @param avg_output_suffix A string holding the textual suffix that is to be
  * output after the average of the measured value.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorMem_Cached( const char* filename,
				const char* fileDescrHeader,
				const char* output_prefix,
				const char* output_suffix,
				const char* avg_output_prefix,
				const char* avg_output_suffix,
				unsigned int average_items) :
    MLUCValueMonitor( filename,
		      fileDescrHeader,
		      output_prefix,
		      output_suffix,
		      avg_output_prefix,
		      avg_output_suffix,
		      average_items )
  {
    Init();
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file.
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written
  * to the beginning of the file.
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well.
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output.
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorMem_Cached( const char* output_prefix,
				const char* output_suffix,
				const char* avg_output_prefix,
				const char* avg_output_suffix,
				unsigned int average_items):
    MLUCValueMonitor( output_prefix,
		      output_suffix,
		      avg_output_prefix,
		      avg_output_suffix,
		      average_items )
  {
    Init();
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file.
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written
  * to the beginning of the file.
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well.
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output.
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorMem_Cached( const char* filename,
				const char* fileDescrHeader,
				unsigned int average_items):
    MLUCValueMonitor( filename, fileDescrHeader, average_items )
  {
    Init();
  };

 /**
  * @brief Constructor with no output.
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well.
  *
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorMem_Cached( unsigned int average_items):
    MLUCValueMonitor( average_items )
  {
    Init();
  };

  int GetValue( uint64& MeasuredValue );

protected:


private:
  /**
   * Identifies the type of the memory which will be monitored.
   */
  unsigned fMemType;

  /**
  * Will be executed in each constructor. This is just for convenience
  */
  void Init();
};

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

class MLUCValueMonitorMem_SwapTotal : public MLUCValueMonitor {
public:
 /**
  * @brief Constructor to enable output to file and stdout.
  * Using this constructor enables output to a file as well as to the
  * standard output. The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written
  * to the beginning of the file.
  * For output to stdout prefixes and suffixes are provided that will be output
  * respectively before and after the monitored value. Another pre-/suffix pair
  * is provided for the averaged value. For the averaged value the number of the
  * last values over which the average is to be taken can be determined as well.
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output.
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param output_prefix A string holding the textual prefix that is to be
  * output before the measured value.
  * @param output_suffix A string holding the textual suffix that is to be
  * output after the measured value.
  * @param avg_output_prefix A string holding the textual prefix that is to be
  * output before the average of the measured value.
  * @param avg_output_suffix A string holding the textual suffix that is to be
  * output after the average of the measured value.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorMem_SwapTotal( const char* filename,
				const char* fileDescrHeader,
				const char* output_prefix,
				const char* output_suffix,
				const char* avg_output_prefix,
				const char* avg_output_suffix,
				unsigned int average_items) :
    MLUCValueMonitor( filename,
		      fileDescrHeader,
		      output_prefix,
		      output_suffix,
		      avg_output_prefix,
		      avg_output_suffix,
		      average_items )
  {
    Init();
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file.
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written
  * to the beginning of the file.
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well.
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output.
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorMem_SwapTotal( const char* output_prefix,
				const char* output_suffix,
				const char* avg_output_prefix,
				const char* avg_output_suffix,
				unsigned int average_items):
    MLUCValueMonitor( output_prefix,
		      output_suffix,
		      avg_output_prefix,
		      avg_output_suffix,
		      average_items )
  {
    Init();
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file.
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written
  * to the beginning of the file.
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well.
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output.
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorMem_SwapTotal( const char* filename,
				const char* fileDescrHeader,
				unsigned int average_items):
    MLUCValueMonitor( filename, fileDescrHeader, average_items )
  {
    Init();
  };

 /**
  * @brief Constructor with no output.
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well.
  *
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorMem_SwapTotal( unsigned int average_items):
    MLUCValueMonitor( average_items )
  {
    Init();
  };

  int GetValue( uint64& MeasuredValue );

protected:


private:
  /**
   * Identifies the type of the memory which will be monitored.
   */
  unsigned fMemType;

  /**
  * Will be executed in each constructor. This is just for convenience
  */
  void Init();
};

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

class MLUCValueMonitorMem_SwapFree : public MLUCValueMonitor {
public:
 /**
  * @brief Constructor to enable output to file and stdout.
  * Using this constructor enables output to a file as well as to the
  * standard output. The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written
  * to the beginning of the file.
  * For output to stdout prefixes and suffixes are provided that will be output
  * respectively before and after the monitored value. Another pre-/suffix pair
  * is provided for the averaged value. For the averaged value the number of the
  * last values over which the average is to be taken can be determined as well.
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output.
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param output_prefix A string holding the textual prefix that is to be
  * output before the measured value.
  * @param output_suffix A string holding the textual suffix that is to be
  * output after the measured value.
  * @param avg_output_prefix A string holding the textual prefix that is to be
  * output before the average of the measured value.
  * @param avg_output_suffix A string holding the textual suffix that is to be
  * output after the average of the measured value.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorMem_SwapFree( const char* filename,
				const char* fileDescrHeader,
				const char* output_prefix,
				const char* output_suffix,
				const char* avg_output_prefix,
				const char* avg_output_suffix,
				unsigned int average_items) :
    MLUCValueMonitor( filename,
		      fileDescrHeader,
		      output_prefix,
		      output_suffix,
		      avg_output_prefix,
		      avg_output_suffix,
		      average_items )
  {
    Init();
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file.
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written
  * to the beginning of the file.
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well.
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output.
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorMem_SwapFree( const char* output_prefix,
				const char* output_suffix,
				const char* avg_output_prefix,
				const char* avg_output_suffix,
				unsigned int average_items):
    MLUCValueMonitor( output_prefix,
		      output_suffix,
		      avg_output_prefix,
		      avg_output_suffix,
		      average_items )
  {
    Init();
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file.
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written
  * to the beginning of the file.
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well.
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output.
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorMem_SwapFree( const char* filename,
				const char* fileDescrHeader,
				unsigned int average_items):
    MLUCValueMonitor( filename, fileDescrHeader, average_items )
  {
    Init();
  };

 /**
  * @brief Constructor with no output.
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well.
  *
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorMem_SwapFree( unsigned int average_items):
    MLUCValueMonitor( average_items )
  {
    Init();
  };

  int GetValue( uint64& MeasuredValue );

protected:


private:
  /**
   * Identifies the type of the memory which will be monitored.
   */
  unsigned fMemType;

  /**
  * Will be executed in each constructor. This is just for convenience
  */
  void Init();
};

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

class MLUCValueMonitorMem_SwapUsed : public MLUCValueMonitor {
public:
 /**
  * @brief Constructor to enable output to file and stdout.
  * Using this constructor enables output to a file as well as to the
  * standard output. The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written
  * to the beginning of the file.
  * For output to stdout prefixes and suffixes are provided that will be output
  * respectively before and after the monitored value. Another pre-/suffix pair
  * is provided for the averaged value. For the averaged value the number of the
  * last values over which the average is to be taken can be determined as well.
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output.
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param output_prefix A string holding the textual prefix that is to be
  * output before the measured value.
  * @param output_suffix A string holding the textual suffix that is to be
  * output after the measured value.
  * @param avg_output_prefix A string holding the textual prefix that is to be
  * output before the average of the measured value.
  * @param avg_output_suffix A string holding the textual suffix that is to be
  * output after the average of the measured value.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorMem_SwapUsed( const char* filename,
				const char* fileDescrHeader,
				const char* output_prefix,
				const char* output_suffix,
				const char* avg_output_prefix,
				const char* avg_output_suffix,
				unsigned int average_items) :
    MLUCValueMonitor( filename,
		      fileDescrHeader,
		      output_prefix,
		      output_suffix,
		      avg_output_prefix,
		      avg_output_suffix,
		      average_items )
  {
    Init();
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file.
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written
  * to the beginning of the file.
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well.
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output.
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorMem_SwapUsed( const char* output_prefix,
				const char* output_suffix,
				const char* avg_output_prefix,
				const char* avg_output_suffix,
				unsigned int average_items):
    MLUCValueMonitor( output_prefix,
		      output_suffix,
		      avg_output_prefix,
		      avg_output_suffix,
		      average_items )
  {
    Init();
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file.
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written
  * to the beginning of the file.
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well.
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output.
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorMem_SwapUsed( const char* filename,
				const char* fileDescrHeader,
				unsigned int average_items):
    MLUCValueMonitor( filename, fileDescrHeader, average_items )
  {
    Init();
  };

 /**
  * @brief Constructor with no output.
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well.
  *
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorMem_SwapUsed( unsigned int average_items):
    MLUCValueMonitor( average_items )
  {
    Init();
  };

  int GetValue( uint64& MeasuredValue );

protected:


private:
  /**
   * Identifies the type of the memory which will be monitored.
   */
  unsigned fMemType;

  /**
  * Will be executed in each constructor. This is just for convenience
  */
  void Init();
};

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

#endif   // _MLUCVALUEMONITORMEM_HPP_
