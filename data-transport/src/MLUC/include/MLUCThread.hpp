/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _MLUCTHREAD_HPP_
#define _MLUCTHREAD_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

class MLUCThread;

#include "MLUCTypes.h"
#include <pthread.h>
#include <list>
#if 0
#include "MLUCThreadFIFO.hpp"
#endif

#define MLUCTHREAD_PUSH_CLEANUP_FUNC( func, arg ) pthread_cleanup_push( func, arg )
#define MLUCTHREAD_POP_CLEANUP_FUNC( executeFunc ) pthread_cleanup_pop( executeFunc )

/**
 * A class for encapsulating thread handling.
 * The thread class has an abstract method Run, which must be overridden 
 * by the user to actually do the real work.
 * Furthermore, each thread object has an associated MLUCThreadFIFO object, 
 * via which messages can be send to it from the outside world (OLD, previous statement does not apply any more!). A thread thus
 * can be basically an independently runnning message handler.
 *
 * @short A class for encapsulating thread handling.
 * @author $Author$
 * @version $Id$
 */
class MLUCThread
	{
	public:

		/**
		 * An enum for the various states which can be returned by this class' methods.
		 */
	    enum TState { kFailed = 0, kStarted, kAborted, kRestarted, kJoined, kNiced };
	
		/**
		 * Default Constructor. Does not start the thread.
		 * Allocates an ordinary and an emergency buffer in the FIFO with size 1024 and 256 respectively.
		 */
		MLUCThread();

#if 0
		/**
		 * Constructor. Does not start the thread.
		 *
		 * @param bufferSize The size of the (ordinary) message buffer in the FIFO object.
		 * @param emergencySize The size of the emergency (high priority) buffer in the FIFO object.
		 */
		MLUCThread( uint32 bufferSize, uint32 emergencySize );
#endif

		/**
		 * Destructor.
		 */
		virtual ~MLUCThread();

		/**
		 * Start this thread. This will result in this object's Run method
		 * being called.
		 *
		 * @return The status value of the thread, either kFailed ot kStarted
		 */
		virtual TState Start();

		/**
		 * Stop this thread. Aborts the execution of this thread.
		 *
		 * @return The status value of the abort process, either kFailed or kAborted
		 */
		virtual TState Abort();

		/**
		 * Does a quick restart of this thread, without actually really restarting it...
		 *
		 * @return The status value of the quick restart process, either kFailed or kRestarted
		 */
		virtual TState QuickRestart();

		/**
		 * Join with this thread, that is wait, until this thread has executed.
		 *
		 * @return The status value of the join process, either kFailed or kJoined
		 */
	  virtual TState Join();

	  virtual TState Nice();

#if 0
		/**
		 * The FIFO object used for sending messages to this thread. 
		 * Only the Write* methods should be used from outside the parent
		 * thread object.
		 */
	    MLUCThreadFIFO fFifo;
#endif
	    
	    MLUCThreadID GetThreadID()
		    {
		    return (MLUCThreadID)fThread;
		    }

	    bool IsRunning() const
		    {
		    return fRunning;
		    }

	    typedef void (*TCleanupFunc)( void* );

	    static void MutexUnlockCleanup( void* mutex );

	    static void Kill( int signal, bool excludeThisThread );


	protected:


		virtual void Run() = 0;

		static void* ThreadStart( void* obj );

		pthread_t fThread;
		pthread_attr_t fThreadAttrs;
	    bool fRunning;
	    bool fStarted;

	    static pthread_mutex_t fgThreadDirMutex;
	    static std::list<pthread_t> fgThreadDir;

	private:
	};


template<class T>
class MLUCObjectThread: public MLUCThread
    {
    public:
	
	MLUCObjectThread( T* object, void (T::*method)( void ) ):
	    fObject( object ), fMethod( method )
		{
		}

    protected:

	T* fObject;
	void (T::*fMethod)(void);

	virtual void Run()
		{
		(fObject->*fMethod)();
		}
    };


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/


#endif // _MLUCTHREAD_HPP_
