#ifndef _MLUCVECTOR_HPP_
#define _MLUCVECTOR_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCTypes.h"
#include <vector>

#if __GNUC__>=3
using namespace std;
#endif

template<class TElementType>
class MLUCVectorBaseSearcher;


template<class TElementType>
class MLUCVector
    {
    public:

	class TIterator
	    {
	    public:
		TIterator()
			{
			fVector = NULL;
			fItemNdx = 0;
			fCnt = 0;
			}
		TIterator( const TIterator& iter )
			{
			fVector = iter.fVector;
			fItemNdx = iter.fItemNdx;
			fCnt = iter.fCnt;
			}
		~TIterator()
			{
			}

		TIterator& operator= ( const TIterator& iter )
			{
			fVector = iter.fVector;
			fItemNdx = iter.fItemNdx;
			fCnt = iter.fCnt;
			return *this;
			}

		operator bool() const
			{
			if ( !fVector )
			    return false;
			if ( !fVector->fDataSize && fItemNdx >= fVector->fData.size() )
			    return false;
			if ( fVector->fDataSize && fCnt >= fVector->fDataCnt+fVector->fInvalidDataCnt )
			    return false;
			return true;
			}
		TIterator& operator ++()
			{
			if ( fVector )
			    {
			    if ( !fVector->fDataSize )
				{
				if ( fItemNdx < fVector->fData.size() )
				    fItemNdx++;
				}
			    else
				{
				do
				    {
				    fItemNdx = (fItemNdx+1) & fVector->fDataMask;
				    fCnt++;
				    }
				while ( fCnt<fVector->fDataCnt+fVector->fInvalidDataCnt && !(*(fVector->fUsedStart+fItemNdx)) );
				}
			    }
			return *this;
			}
		TElementType* operator->()
			{
			if ( !fVector )
			    return NULL;
			if ( !fVector->fDataSize && fItemNdx >= fVector->fData.size() )
			    return NULL;
			if ( fVector->fDataSize && fCnt >= fVector->fDataCnt+fVector->fInvalidDataCnt )
			    return NULL;
			return fVector->fDataStart+fItemNdx;
			}
		TElementType& operator*()
			{
			if ( !fVector )
			    return *(TElementType*)NULL;
			if ( !fVector->fDataSize && fItemNdx >= fVector->fData.size() )
			    return *(TElementType*)NULL;
			if ( fVector->fDataSize && fCnt >= fVector->fDataCnt+fVector->fInvalidDataCnt )
			    return *(TElementType*)NULL;
			return *(fVector->fDataStart+fItemNdx);
			}
		unsigned long GetNdx() const
			{
			return fItemNdx;
			}
	    protected:
		TIterator( MLUCVector<TElementType>* vector, unsigned long itemNdx ):
		    fVector( vector ),
		    fItemNdx( itemNdx )
			{
			fCnt = 0;
			}

		    friend class MLUCVector<TElementType>;

		MLUCVector<TElementType>* fVector;
		unsigned long fItemNdx;
		unsigned long fCnt;

	    };

	typedef void (*TIterationFunc)( const TElementType& el, void* arg );
	//typedef int (*TCompareFunc)( const TElementType& el1, const TElementType& el2 );

	MLUCVector( int bufferSizeExp2 = -1, bool grow = true ):
	    fDefaultElementSet( false ), fGrow( grow )
		{
		Init( bufferSizeExp2 );
		}
	MLUCVector( int bufferSizeExp2, TElementType defaultElement, bool grow = true ):
	    fDefaultElement( defaultElement ), fDefaultElementSet( true ), fGrow( grow )
		{
		Init( bufferSizeExp2 );
		}

	bool SetPreAlloc( int bufferSizeExp2 )
		{
		if ( bufferSizeExp2 <= 0 )
		    return false;
		if ( fDataSize )
		    return false;
		if ( fDataCnt )
		    return false;
		Init( bufferSizeExp2 );
		return true;
		}

	bool Add( const TElementType& newElement )
		{
		unsigned long empty;
		return Add( newElement, empty );
		}
	bool Add( const TElementType& newElement, unsigned long& newElementNdx )
		{
		if ( !fDataSize )
		    {
#if __GNUC__>=3
		    typename vector<TElementType>::iterator p;
#else
		    vector<TElementType>::iterator p;
#endif
		    p = fData.insert( fData.end(), newElement );
		    newElementNdx = p-fData.begin();
		    }
		else
		    {
		    if ( fInvalidDataCnt+fDataCnt==fDataSize && fInvalidDataCnt>0 )
			Compact( NULL );
		    else if ( fDataCnt==fDataSize )
			{
			if ( fGrow )
			    Resize( true );
			else
			    return false;
			}
		    newElementNdx = fNextFree;
		    *(fDataStart+fNextFree) = newElement;
		    *(fUsedStart+fNextFree) = true;
		    fNextFree = (fNextFree+1) & fDataMask;
		    }
		fDataCnt++;
		return true;
		}

	//bool Insert( const TElementType& newElement, TCompareFunc compFunc );
	//bool Insert( const TElementType& newElement, TCompareFunc compFunc, unsigned long& newElementNdx );

	bool Remove( unsigned long elementNdx )
		{
		if ( !fDataSize )
		    {
		    if ( elementNdx>=fData.size() )
			return false;
		    fData.erase( fData.begin()+elementNdx );
		    fDataCnt--;
		    }
		else
		    {
		    if ( elementNdx>=fDataSize || fDataCnt<=0 )
			return false;
		    //     if ( !(fDataStart+elementNdx)->fValid )
		    // 	return true;
		    *(fUsedStart+elementNdx) = false;
		    if ( fDefaultElementSet )
			*(fDataStart+elementNdx) = fDefaultElement;
		    fDataCnt--;
		    if ( elementNdx==fFirstUsed )
			{
			fFirstUsed = (fFirstUsed+1) & fDataMask;
			while ( !(*(fUsedStart+fFirstUsed)) && fFirstUsed!=fNextFree )
			    {
			    fFirstUsed = (fFirstUsed+1) & fDataMask;
			    fInvalidDataCnt--;
			    }
			}
		    else if ( elementNdx == ((fNextFree-1) & fDataMask) )
			{
			fNextFree = elementNdx;
			while ( !(*(fUsedStart+((fNextFree-1) & fDataMask))) && fFirstUsed!=fNextFree )
			    {
			    fNextFree = (fNextFree-1) & fDataMask;
			    fInvalidDataCnt--;
			    }
			
			}
		    else
			fInvalidDataCnt++;
		    if ( (fDataCnt < (fDataSize>>2)) && (fDataSize>fOrigDataSize) )
			{
			Compact( NULL );
			Resize( false ); // We shrink
			}
		    }
		return true;
		}
	bool RemoveFirst()
		{
		if ( !fDataSize )
		    {
		    if ( fData.size()<=0 )
			return false;
		    fData.erase( fData.begin() );
		    fDataCnt--;
		    }
		else
		    {
		    if ( fDataCnt<=0 )
			return false;
		    *(fUsedStart+fFirstUsed) = false;
		    if ( fDefaultElementSet )
			*(fDataStart+fFirstUsed) = fDefaultElement;
		    fDataCnt--;
		    fFirstUsed = (fFirstUsed+1) & fDataMask;
		    while ( !(*(fUsedStart+fFirstUsed)) && fFirstUsed!=fNextFree )
			{
			fFirstUsed = (fFirstUsed+1) & fDataMask;
			fInvalidDataCnt--;
			}
		    if ( (fDataCnt < (fDataSize>>2)) && (fDataSize>fOrigDataSize) )
			{
			Compact( NULL );
			Resize( false ); // We shrink
			}
		    }
		return true;
		}

	void Clear()
		{
		fDataCnt = 0;
		fInvalidDataCnt = 0;
		fFirstUsed = fNextFree = 0;
		while ( fDataSize>fOrigDataSize )
		    Resize( false );
		}

	void Iterate( TIterationFunc iterFunc, void* arg )
		{
		if ( !fDataSize )
		    {
		    unsigned long n = 0;
#if __GNUC__>=3
		    typename vector<TElementType>::iterator iter, end;
#else
		    vector<TElementType>::iterator iter, end;
#endif
		    iter = fData.begin();
		    end = fData.end();
		    while ( iter != end )
			{
			(*iterFunc)( *iter, arg );
			iter++;
			n++;
			}
		    }
		else
		    {
		    unsigned long i = 0;
		    unsigned long n = fFirstUsed;
		    while ( i++<fDataCnt+fInvalidDataCnt )
			{
			if ( *(fUsedStart+n) )
			     (*iterFunc)( *(fDataStart+n), arg );
			n = (n+1) & fDataMask;
			}
		    }
		}
	
	bool IsValid( unsigned long elementNdx ) const 
		{
		if ( !fDataSize )
		    {
		    if ( elementNdx >= fData.size() )
			return false;
		    return true;
		    }
		else
		    {
		    if ( elementNdx >= fDataSize )
			return false;
		    if ( !(*(fUsedStart+elementNdx)) )
			 return false;
		    return true;
		    }
		}

	TElementType Get( unsigned long elementNdx )
		{
		if ( !fDataSize )
		    {
		    if ( elementNdx>=fData.size() )
			return fDefaultElement;
		    return *(fData.begin()+elementNdx);
		    }
		else
		    {
		    if ( fFirstUsed<fNextFree )
			{
			if ( elementNdx<fFirstUsed || elementNdx>=fNextFree )
			    return fDefaultElement;
			}
		    else
			{
			if ( elementNdx<fFirstUsed && elementNdx>=fNextFree )
			    return fDefaultElement;
			}
		    return *(fDataStart+elementNdx);
		    }
		}
	TElementType Get( const TIterator& iter )
		{
		if ( !fDataSize )
		    {
		    if ( iter.fItemNdx>=fData.size() )
			return fDefaultElement;
		    return *(fData.begin()+iter.fItemNdx);
		    }
		else
		    {
		    if ( fFirstUsed<fNextFree )
			{
			if ( iter.fItemNdx<fFirstUsed || iter.fItemNdx>=fNextFree )
			    return fDefaultElement;
			}
		    else
			{
			if ( iter.fItemNdx<fFirstUsed && iter.fItemNdx>=fNextFree )
			    return fDefaultElement;
			}
		    return *(fDataStart+iter.fItemNdx);
		    }
		}
	TElementType* GetPtr( unsigned long elementNdx )
		{
		if ( !fDataSize )
		    {
		    if ( elementNdx>=fData.size() )
			return NULL;
#if __GNUC__>=3
		    return (fData.begin().base()+elementNdx);
#else
		    return (fData.begin()+elementNdx);
#endif
		    }
		else
		    {
		    if ( fFirstUsed<fNextFree )
			{
			if ( elementNdx<fFirstUsed || elementNdx>=fNextFree )
			    return NULL;
			}
		    else
			{
			if ( elementNdx<fFirstUsed && elementNdx>=fNextFree )
			    return NULL;
			}
		    return (fDataStart+elementNdx);
		    }
		}
	TElementType* GetPtr( const TIterator& iter )
		{
		if ( !fDataSize )
		    {
		    if ( iter.fItemNdx>=fData.size() )
			return NULL;
#if __GNUC__>=3
		    return (fData.begin().base()+iter.fItemNdx);
#else
		    return (fData.begin()+iter.fItemNdx);
#endif
		    }
		else
		    {
		    if ( fFirstUsed<fNextFree )
			{
			if ( iter.fItemNdx<fFirstUsed || iter.fItemNdx>=fNextFree )
			    return NULL;
			}
		    else
			{
			if ( iter.fItemNdx<fFirstUsed && iter.fItemNdx>=fNextFree )
			    return NULL;
			}
		    return (fDataStart+iter.fItemNdx);
		    }
		}
	bool Get( unsigned long elementNdx, TElementType& element )
		{
		if ( !fDataSize )
		    {
		    if ( elementNdx>=fData.size() )
			return false;
		    element = *(fData.begin()+elementNdx);
		    return true;
		    }
		else
		    {
		    if ( fFirstUsed<fNextFree )
			{
			if ( elementNdx<fFirstUsed || elementNdx>=fNextFree )
			    return false;
			}
		    else
			{
			if ( elementNdx<fFirstUsed && elementNdx>=fNextFree )
			    return false;
			}
		    element = *(fDataStart+elementNdx);
		    return true;
		    }
		}
	bool Get( const TIterator& iter, TElementType& element )
		{
		if ( !fDataSize )
		    {
		    if ( iter.fItemNdx>=fData.size() )
			return false;
		    element = *(fData.begin()+iter.fItemNdx);
		    return true;
		    }
		else
		    {
		    if ( fFirstUsed<fNextFree )
			{
			if ( iter.fItemNdx<fFirstUsed || iter.fItemNdx>=fNextFree )
			    return false;
			}
		    else
			{
			if ( iter.fItemNdx<fFirstUsed && iter.fItemNdx>=fNextFree )
			    return false;
			}
		    element = *(fDataStart+iter.fItemNdx);
		    return true;
		    }
		}

	TElementType GetFirst()
		{
		if ( fDataCnt<=0 )
		    return fDefaultElement;
		if ( !fDataSize )
		    {
		    return *fData.begin();
		    }
		else
		    {
		    return *(fDataStart+fFirstUsed);
		    }
		}
	TElementType* GetFirstPtr()
		{
		if ( fDataCnt<=0 )
		    return NULL;
		if ( !fDataSize )
		    {
#if __GNUC__>=3
		    return fData.begin().base();
#else
		    return fData.begin();
#endif
		    }
		else
		    {
		    return fDataStart+fFirstUsed;
		    }
		}
	bool GetFirst( TElementType& element )
		{
		if ( fDataCnt<=0 )
		    return false;
		if ( !fDataSize )
		    {
		    element = *fData.begin();
		    }
		else
		    {
		    element = *(fDataStart+fFirstUsed);
		    }
		return true;
		}

	bool IsFixedSize() const
		{
		return fOrigDataSize!=0;
		}

// 	int GetSize()
	unsigned long GetSize() const
		{
		return fDataSize;
		}

	bool IsGrowable() const
		{
		return fGrow;
		}
	unsigned long GetStartNdx() const
		{
		if ( !fDataSize )
		    return 0;
		else
		    return fFirstUsed;
		}
	unsigned long GetEndNdx() const // Returns the index that is 1 beyond the last valid index
		{
		if ( !fDataSize )
		    return fData.size();
		else
		    return fNextFree;
		}
	unsigned long GetMask() const
		{
		if ( !fDataSize )
		    return ~(unsigned long)0;
		else
		    return fDataMask;
		}
	unsigned long GetCnt() const
		{
		return fDataCnt;
		}

	TIterator Begin()
		{
		if ( !fDataSize )
		    return TIterator( this, 0 );
		else
		    return TIterator( this, fFirstUsed );
		}

    protected:

	void Init( int bufferSizeExp2 );
	void Compact( const TElementType* newElement ); // Only called in fixed size mode.
	void Resize( bool grow ); // Only called in fixed size mode.

	TElementType fDefaultElement;
	bool fDefaultElementSet;

	vector<TElementType> fData;
	TElementType* fDataStart;
	vector<unsigned char> fUsed;
	unsigned char* fUsedStart;

	unsigned long fDataCnt;
	unsigned long fInvalidDataCnt;
	unsigned long fDataSize;
	unsigned long fOrigDataSize;
	unsigned long fDataMask;
	unsigned long fFirstUsed;
	unsigned long fNextFree;
	bool fGrow;

	    friend class MLUCVectorBaseSearcher<TElementType>;

    private:
    };


template<class TElementType>
class MLUCVectorBaseSearcher
    {
    protected:
	MLUCVectorBaseSearcher()
		{
		}

#if __GNUC__>=3
	static void GetVectorData( MLUCVector<TElementType>& vect, typename vector<TElementType>::iterator& dataBegin, 
			    typename vector<TElementType>::iterator& dataEnd, unsigned long& dataSize, 
			    unsigned long& dataCnt, unsigned long& invalidDataCnt, unsigned char*& usedStart, 
			    TElementType*& dataStart, unsigned long& firstUsed, unsigned long& nextFree, 
			    unsigned long& dataMask )
#else
	static void GetVectorData( MLUCVector<TElementType>& vect, vector<TElementType>::iterator& dataBegin, 
			    vector<TElementType>::iterator& dataEnd, unsigned long& dataSize, 
			    unsigned long& dataCnt, unsigned long& invalidDataCnt, unsigned char*& usedStart, 
			    TElementType*& dataStart, unsigned long& firstUsed, unsigned long& nextFree, 
			    unsigned long& dataMask )
#endif
		{
		dataBegin = vect.fData.begin();
		dataEnd = vect.fData.end();
		dataSize = vect.fDataSize;
		dataCnt = vect.fDataCnt;
		invalidDataCnt = vect.fInvalidDataCnt;
		usedStart = vect.fUsedStart;
		dataStart = vect.fDataStart;
		firstUsed = vect.fFirstUsed;
		nextFree = vect.fNextFree;
		dataMask = vect.fDataMask;
		}

    };

template<class TElementType,class TSearchDataType>
class MLUCVectorSearcher: public MLUCVectorBaseSearcher<TElementType>
    {
    public:
	typedef bool (*TSearchFunc)( const TElementType& el, const TSearchDataType& searchData );
	typedef bool (*TSearchFuncPtr)( const TElementType& el, void* arg );

	static bool FindElement( MLUCVector<TElementType>& vect, TSearchFunc searchFunc, TSearchDataType searchData, unsigned long& elementNdx )
		{
#if __GNUC__>=3
		typename vector<TElementType>::iterator dataIter;
		typename vector<TElementType>::iterator dataEnd;
#else
		vector<TElementType>::iterator dataIter;
		vector<TElementType>::iterator dataEnd;
#endif
		unsigned long dataSize; 
		unsigned long dataCnt;
		unsigned long invalidDataCnt;
		unsigned char* usedStart; 
		TElementType* dataStart;
		unsigned long firstUsed;
		unsigned long nextFree; 
		unsigned long dataMask;
		MLUCVectorSearcher::GetVectorData( vect, dataIter, dataEnd, dataSize, 
			       dataCnt, invalidDataCnt, usedStart, 
			       dataStart, firstUsed, nextFree, 
			       dataMask );
		if ( !dataSize )
		    {
		    unsigned long n = 0;
		    while ( dataIter != dataEnd )
			{
			if ( (*searchFunc)( *dataIter, searchData ) )
			    {
			    elementNdx = n;
			    return true;
			    }
			dataIter++;
			n++;
			}
		    }
		else
		    {
		    unsigned long i = 0;
		    unsigned long n = firstUsed;
		    while ( i<dataCnt+invalidDataCnt && (!(*(usedStart+n)) || !(*searchFunc)( *(dataStart+n), searchData )) )
			{
			n = (n+1) & dataMask;
			i++;
			}
// 		    if ( *(fUsedStart+n) && (*searchFunc)( *(fDataStart+n), searchData ) )
		    if ( i<dataCnt+invalidDataCnt && *(usedStart+n) )
			{
			elementNdx = n;
			return true;
			}
		    }
		return false;
		}

	static bool FindElement( MLUCVector<TElementType>& vect, TSearchFuncPtr searchFunc, void* arg, unsigned long& elementNdx )
		{
#if __GNUC__>=3
		typename vector<TElementType>::iterator dataIter;
		typename vector<TElementType>::iterator dataEnd;
#else
		vector<TElementType>::iterator dataIter;
		vector<TElementType>::iterator dataEnd;
#endif
		unsigned long dataSize; 
		unsigned long dataCnt;
		unsigned long invalidDataCnt;
		unsigned char* usedStart; 
		TElementType* dataStart;
		unsigned long firstUsed;
		unsigned long nextFree; 
		unsigned long dataMask;
		GetVectorData( vect, dataIter, dataEnd, dataSize, 
			       dataCnt, invalidDataCnt, usedStart, 
			       dataStart, firstUsed, nextFree, 
			       dataMask );
		if ( !dataSize )
		    {
		    unsigned long n = 0;
		    while ( dataIter != dataEnd )
			{
			if ( (*searchFunc)( *dataIter, arg ) )
			    {
			    elementNdx = n;
			    return true;
			    }
			dataIter++;
			n++;
			}
		    }
		else
		    {
		    unsigned long i = 0;
		    unsigned long n = firstUsed;
		    while ( i<dataCnt+invalidDataCnt && (!(*(usedStart+n)) || !(*searchFunc)( *(dataStart+n), arg )) )
			{
			n = (n+1) & dataMask;
			i++;
			}
// 		    if ( *(fUsedStart+n) && (*searchFunc)( *(fDataStart+n), arg ) )
		    if ( i<dataCnt+invalidDataCnt && *(usedStart+n) )
			{
			elementNdx = n;
			return true;
			}
		    }
		return false;
		}

	static bool FindElement( MLUCVector<TElementType>& vect, unsigned long startNdx, bool directionForward, TSearchFunc searchFunc, TSearchDataType searchData, unsigned long& elementNdx );

	// Search starts from the end.
	static bool FindElementReverse( MLUCVector<TElementType>& vect, TSearchFunc searchFunc, TSearchDataType searchData, unsigned long& elementNdx )
		{
#if __GNUC__>=3
		typename vector<TElementType>::iterator dataBegin;
		typename vector<TElementType>::iterator dataIter;
#else
		vector<TElementType>::iterator dataBegin;
		vector<TElementType>::iterator dataIter;
#endif
		unsigned long dataSize; 
		unsigned long dataCnt;
		unsigned long invalidDataCnt;
		unsigned char* usedStart; 
		TElementType* dataStart;
		unsigned long firstUsed;
		unsigned long nextFree; 
		unsigned long dataMask;
		GetVectorData( vect, dataBegin, dataIter, dataSize, 
			       dataCnt, invalidDataCnt, usedStart, 
			       dataStart, firstUsed, nextFree, 
			       dataMask );
		if ( !dataSize )
		    {
		    unsigned long n = dataSize;
		    if ( dataIter != dataBegin )
			{
			do
			    {
			    dataIter--;
			    n--;
			    if ( (*searchFunc)( *dataIter, searchData ) )
				{
				elementNdx = n;
				return true;
				}
			    }
			while ( dataIter != dataBegin );
			}
		    }
		else
		    {
		    unsigned long i = 0;
		    unsigned long n = (nextFree-1) & dataMask;
		    while ( i<dataCnt+invalidDataCnt && (!(*(usedStart+n)) || !(*searchFunc)( *(dataStart+n), searchData )) )
			{
			n = (n-1) & dataMask;
			i++;
			}
// 		    if ( *(vect.fUsedStart+n) && (*searchFunc)( *(dataStart+n), searchData ) )
		    if ( i<dataCnt+invalidDataCnt && *(usedStart+n) )
			{
			elementNdx = n;
			return true;
			}
		    }
		return false;
		}

    protected:
	MLUCVectorSearcher()
		{
		}



    };



template<class TElementType,class TSearchDataType>
inline bool MLUCVectorSearcher<TElementType,TSearchDataType>::FindElement( MLUCVector<TElementType>& vect, unsigned long startNdx, bool directionForward, TSearchFunc searchFunc, const TSearchDataType searchData, unsigned long& elementNdx )
    {
#if __GNUC__>=3
    typename vector<TElementType>::iterator dataBegin;
    typename vector<TElementType>::iterator dataEnd;
#else
    vector<TElementType>::iterator dataBegin;
    vector<TElementType>::iterator dataEnd;
#endif
    unsigned long dataSize; 
    unsigned long dataCnt;
    unsigned long invalidDataCnt;
    unsigned char* usedStart; 
    TElementType* dataStart;
    unsigned long firstUsed;
    unsigned long nextFree; 
    unsigned long dataMask;
    MLUCVectorSearcher::GetVectorData( vect, dataBegin, dataEnd, dataSize, 
		   dataCnt, invalidDataCnt, usedStart, 
		   dataStart, firstUsed, nextFree, 
		   dataMask );
    if ( dataCnt<=0 )
	return false;
    if ( !dataSize )
	{
	if ( dataBegin+startNdx>=dataEnd )
	    return false;
#if __GNUC__>=3
	typename vector<TElementType>::iterator iter = dataBegin+startNdx;
	typename vector<TElementType>::iterator boundary = (directionForward ? dataEnd : dataBegin);
#else
	vector<TElementType>::iterator iter = dataBegin+startNdx;
	vector<TElementType>::iterator boundary = (directionForward ? dataEnd : dataBegin);
#endif
	int step = (directionForward ? 1 : -1);
	int offset = (directionForward ? 0 : -1);
	while ( iter!=boundary )
	    {
	    if ( (*searchFunc)( *(iter+offset), searchData ) )
		{
		elementNdx = startNdx;
		return true;
		}
	    iter += step;
	    startNdx += step;
	    }
	return false;
	}
    else
	{
	unsigned long i = 0;
	unsigned long n = firstUsed;
	int step = (directionForward ? 1 : -1);
	//unsigned long boundary = (directionForward ? vect.fNextFree : (vect.fFirstUsed-1) & dataMask);
	if ( firstUsed<nextFree )
	    {
	    if ( startNdx<firstUsed || startNdx>=nextFree )
		return false;
	    }
	else
	    {
	    if ( startNdx<firstUsed && startNdx>=nextFree )
		return false;
	    }
	unsigned long cnt;
	if ( directionForward )
	    {
	    if ( firstUsed<nextFree )
		cnt = nextFree-startNdx;
	    else
		{
		if ( startNdx<firstUsed )
		    cnt = nextFree-startNdx;
		else
		    cnt = nextFree+dataSize-startNdx;
		}
	    }
	else
	    {
	    if ( firstUsed<nextFree )
		cnt = startNdx-firstUsed+1;
	    else
		{
		if ( startNdx<firstUsed )
		    cnt = startNdx+dataSize-firstUsed+1;
		else
		    cnt = startNdx-firstUsed+1;
		}
	    }
	while ( i++<cnt && (!(*(usedStart+n)) || !(*searchFunc)( *(dataStart+n), searchData )) )
	    n = (n+step) & dataMask;
	if ( (usedStart+n) && (*searchFunc)( *(dataStart+n), searchData ) )
	    {
	    elementNdx = n;
	    return true;
	    }
	else
	    return false;
	}
    }

template<class TElementType>
inline void MLUCVector<TElementType>::Init( int bufferSizeExp2 )
    {
    fDataCnt = 0;
    fInvalidDataCnt = 0;
    fFirstUsed = fNextFree = 0;
    if ( bufferSizeExp2 < 0 )
	{
	fDataSize = 0;
	fOrigDataSize = 0;
	fDataMask = ~(unsigned long)0;
	fDataStart = NULL;
	fUsedStart = NULL;
	}
    else
	{
	fOrigDataSize = fDataSize = 1 << bufferSizeExp2;
	fDataMask = fDataSize-1;
	fData.resize( fDataSize, fDefaultElement );
	fUsed.resize( fDataSize, false );
#if __GNUC__>=3
	fDataStart = fData.begin().base();
	fUsedStart = fUsed.begin().base();
#else
	fDataStart = fData.begin();
	fUsedStart = fUsed.begin();
#endif
	}
    }

template<class TElementType>
inline void MLUCVector<TElementType>::Compact( const TElementType* )
    {
    unsigned long i = 0;
    unsigned long dest, src;
    for ( i = 0, dest = src = fFirstUsed; i<(fDataCnt+fInvalidDataCnt); i++, src = (src+1) & fDataMask )
	{
	if ( *(fUsedStart+src) )
	    {
	    if ( dest != src )
		{
		*(fUsedStart+dest) = true;
		*(fDataStart+dest) = *(fDataStart+src);
		}
	    dest = (dest+1) & fDataMask;
	    }
	}
    for ( i = dest; i!= fNextFree; i=(i+1) & fDataMask )
	{
	*(fUsedStart+i) = false;
	if ( fDefaultElementSet )
	    *(fDataStart+i) = fDefaultElement;
	}
    fNextFree = dest;
    fInvalidDataCnt = 0;
    }

template<class TElementType>
inline void MLUCVector<TElementType>::Resize( bool grow )
    {
    unsigned long newSize;
    //TElementType elm( fDefaultElement, false );
    // Do we grow or shrink?
    if ( grow )
	{
	// We grow...
	newSize = fDataSize<<1;
	if ( fDefaultElementSet )
	    {
	    fData.resize( newSize, fDefaultElement );
	    }
	else
	    {
	    fData.resize( newSize );
	    }
	fUsed.resize( newSize, false );
#if __GNUC__>=3
	fDataStart = fData.begin().base();
	fUsedStart = fUsed.begin().base();
#else
	fDataStart = fData.begin();
	fUsedStart = fUsed.begin();
#endif
	// Note: We double the size...
	if ( fNextFree<fFirstUsed || (fNextFree==fFirstUsed && (fDataCnt+fInvalidDataCnt>0)) )
	    {
	    // The used area is split into two parts, one at the beginning
	    // and one at the end of the (old) array.
	    // In other words, the buffer has partly wrapped around.
	    // We copy the block at the beginning of the buffer to the end of the block at 
	    // the end (of the old size buffer, now in the middle)
#if 0 // memcpy cannot be used, because of assignment operators et al...
	    memcpy( fDataStart+fDataSize, fDataStart, fNextFree*sizeof(TElementType) );
	    memcpy( fUsedStart+fDataSize, fUsedStart, fNextFree*sizeof(unsigned char) );
#endif
	    unsigned long i;
	    for ( i = 0; i < fNextFree; i++ )
		{
		*(fDataStart+fDataSize+i) = *(fDataStart+i);
		*(fUsedStart+fDataSize+i) = *(fUsedStart+i);
		if ( fDefaultElementSet )
		    *(fDataStart+i) = fDefaultElement;
		*(fUsedStart+i) = false;
		}
	    fNextFree += fDataSize;
	    }
	fDataSize = newSize;
	fDataMask = fDataSize-1;
	}
    else
	{
	// We shrink...
	newSize = fDataSize>>1;
	// Note: fNextFree==fFirstUsed means it is totally empty, 
	// as we use at most 1/4 of the buffer.
	if ( fNextFree < fFirstUsed )
	    {
	    // We have the used area split into two parts, one at the beginning
	    // and one at the end of the (old) array.
	    // In other words, the buffer has partly wrapped around.
	    // Note: We half the size...
	    // This means that newSize is both the new size and the amount by which we shrink.
	    // Also, newSize is then the amount by which we have to move the used slots 
	    // at the (old) end of the buffer toward its beginning (its new end). And since we half the size, we can be
	    // sure not to have any overlap between the src and dest. (We are at most a quarter of the old size)
#if 0 // memcpy cannot be used, because of assignment operators et al...
	    memcpy( fDataStart+fFirstUsed-newSize, fDataStart+fFirstUsed, (fDataSize-fFirstUsed)*sizeof(TElementType) );
	    memcpy( fUsedStart+fFirstUsed-newSize, fUsedStart+fFirstUsed, (fDataSize-fFirstUsed)*sizeof(unsigned char) );
#endif
	    unsigned long i;
	    for ( i = 0; i < (fDataSize-fFirstUsed); i++ )
		{
		*(fDataStart+fFirstUsed-newSize+i) = *(fDataStart+fFirstUsed+i);
		*(fUsedStart+fFirstUsed-newSize+i) = *(fUsedStart+fFirstUsed+i);
		}
	    fFirstUsed -= newSize;
	    }
	else
	    {
	    // The used slots are in one block in the middle of the buffer,
	    // not wrapped around.
	    // We move the block to the beginning of the buffer to be sure we do not
	    // cut of any valid data.
	    unsigned long i, n, newEnd = fNextFree-fFirstUsed; // The amount of entries, also the 
	                                                       // new end point, once the block is moved to the beginning of the buffer.
	    if ( fFirstUsed>0 )
		{
		// The used block is not yet at the beginning of the buffer.
		if ( newEnd > fFirstUsed )
		    {
		    // The new end points is greater than the old starting point
		    // => the old and new block locations overlap.
		    i = 0;
		    while ( newEnd > i )
			{
			if ( newEnd-i>=fFirstUsed )
			    n = fFirstUsed;
			else
			    n = newEnd-i;
#if 0 // memcpy cannot be used, because of assignment operators et al...
			memcpy( fDataStart+i, fDataStart+i+fFirstUsed, n*sizeof(TElementType) );
			memcpy( fUsedStart+i, fUsedStart+i+fFirstUsed, n*sizeof(unsigned char) );
#endif
			unsigned long b;
			for ( b = 0; b < n; b++ )
			    {
			    *(fDataStart+i+b) = *(fDataStart+i+fFirstUsed+b);
			    *(fUsedStart+i+b) = *(fUsedStart+i+fFirstUsed+b);
			    }
			i += n;
			}
		    i = newEnd;
		    for ( n = 0; n < fNextFree-newEnd; n++ )
			{
			if ( fDefaultElementSet )
			    *(fDataStart+i) = fDefaultElement;
			*(fUsedStart+i) = false;
			i = (i+1) & fDataMask;
			}
		    }
		else
		    {
		    // The old and new block locations are completely separate.
#if 0 // memcpy cannot be used, because of assignment operators et al...
		    memcpy( fDataStart, fDataStart+fFirstUsed, newEnd*sizeof(TElementType) );
		    memcpy( fUsedStart, fUsedStart+fFirstUsed, newEnd*sizeof(unsigned char) );
#endif
		    i = fFirstUsed;
		    for ( n = 0; n < newEnd; n++ )
			{
			*(fDataStart+n) = *(fDataStart+fFirstUsed+n);
			*(fUsedStart+n) = *(fUsedStart+fFirstUsed+n);
			if ( fDefaultElementSet )
			    *(fDataStart+i) = fDefaultElement;
			*(fUsedStart+i) = false;
			i = (i+1) & fDataMask;
			}
		    }
		fFirstUsed = 0;
		fNextFree = newEnd;
		}
	    }
	fData.resize( newSize );
	fUsed.resize( newSize );
#if __GNUC__>=3
	fDataStart = fData.begin().base();
	fUsedStart = fUsed.begin().base();
#else
	fDataStart = fData.begin();
	fUsedStart = fUsed.begin();
#endif
	fDataSize = newSize;
	fDataMask = fDataSize-1;
	}
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _MLUCVECTOR_HPP_
