#ifndef _MLUCSIMPLESTACKMACHINEASSEMBLER_HPP_
#define _MLUCSIMPLESTACKMACHINEASSEMBLER_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include <MLUCTypes.h>
#include <MLUCString.hpp>
#include <vector>
#include <map>


class MLUCSimpleStackMachineAssembler
    {
    public:

	class TError
	    {
	    public:
		enum TCode { kOk=0, kLabelError, kUInt64ConstantParseError, kUknonwnMnemonic, kOutOfMemory, TCodeAllocationError, kInvalidJumpTargetLabel };
		TError():
		    fOk(true),
		    fCode(kOk),
		    fDescription(NULL),
		    fLine(NULL),
		    fLineNr(0)
			{ }
		TError( TCode code, const char* descr ):
		    fOk(false),
		    fCode(code),
		    fDescription(descr),
		    fLine(NULL),
		    fLineNr(~0U)
			{ }
		TError( TCode code, const char* descr, const char* line, unsigned lineNr ):
		    fOk(false),
		    fCode(code),
		    fDescription(descr),
		    fLine(line),
		    fLineNr(lineNr)
			{ }
		TError( TError const& copy, const char* line, unsigned lineNr ):
		    fOk(false),
		    fCode(copy.fCode),
		    fDescription(copy.fDescription),
		    fLine(line),
		    fLineNr(lineNr)
			{ }
		bool Ok() const { return fOk; }
		TCode Code() const { return fCode; }
		const char* Description() const { return fDescription; }
		const char* Line() const { return fLine; }
		unsigned LineNr() const { return fLineNr; }
	    protected:
		bool fOk;
		TCode fCode;
		const char* fDescription;
		const char* fLine;
		unsigned fLineNr;
	    };

	MLUCSimpleStackMachineAssembler();
	virtual ~MLUCSimpleStackMachineAssembler();

	void AddSymbol( const char* ID, uint64 value );

	TError Assemble( const char* text )
		{
		return Assemble( MLUCString(text) );
		}
	TError Assemble( MLUCString const& text );

	unsigned long GetCodeWordCount() const;
	uint64 const* GetCode() const;

    protected:

	TError AddWords( unsigned long wordCnt, uint64* words );
	TError AddWords( std::vector<uint64> words );
	TError AddWord( uint64 words );
	TError AddWords( uint64 word0, uint64 word1 );
	TError AddWords( uint64 word0, uint64 word1, uint64 word2 );

	virtual TError Assemble( std::vector<MLUCString>& tokens );

	TError GetParameter( MLUCString const&, uint64& );
	void StripEmptyTokens( std::vector<MLUCString>& tokens );

	TError SetCodeAllocation( unsigned long newSize );


	bool fFirstPass;
	unsigned long fIP;
	std::map<MLUCString,unsigned long> fLabels;

	std::map<MLUCString,uint64> fSymbols;

	unsigned long fCodeWordCount;
	unsigned fCodeAllocationWordCount;
	uint64* fCode;

    private:
    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _MLUCSIMPLESTACKMACHINEASSEMBLER_HPP_
