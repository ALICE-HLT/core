#ifndef _MLUCMUTEX_HPP_
#define _MLUCMUTEX_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCTypes.h"
#include <pthread.h>
#include <cerrno>

class MLUCMutex
    {
    public:

	class TLocker
	    {
	    public:
		TLocker( MLUCMutex& mutex, bool lock=true ):
		    fMutex(&mutex),
		    fLocked(false)
			{
			if ( lock && fMutex )
			    {
			    fMutex->Lock();
			    fLocked = true;
			    }
			}
		TLocker():
		    fMutex(NULL),
		    fLocked(false)
			{
			}
		~TLocker()
			{
			if ( fLocked && fMutex )
			    fMutex->Unlock();
			}
		bool Set( MLUCMutex& mutex, bool lock=true )
			{
			if ( fMutex && fLocked )
			    fMutex->Unlock();
			fLocked = false;
			fMutex = &mutex;
			if ( lock && fMutex )
			    {
			    bool ret = fMutex->Lock();
			    fLocked = ret;
			    return ret;
			    }
			return true;
			}
		void Reset()
			{
			if ( fMutex && fLocked )
			    fMutex->Unlock();
			fMutex = NULL;
			fLocked = false;
			}
		bool Lock()
			{
			if ( !fMutex )
			    return false;
			bool ret = fMutex->Lock();
			fLocked = ret;
			return ret;
			}
		bool TryLock( bool& success )
			{
			if ( !fMutex )
			    return false;
			bool ret = fMutex->TryLock( success );
			fLocked = success;
			return ret;
			}
		bool Unlock()
			{
			if ( !fMutex )
			    return false;
			fLocked = false;
			return fMutex->Unlock();
			}
	    protected:
		MLUCMutex* fMutex;
		bool fLocked;
	    };

	MLUCMutex()
		{
		pthread_mutex_init( &fMutex, NULL );
		}
	~MLUCMutex()
		{
		pthread_mutex_destroy( &fMutex );
		}

    protected:

	bool Lock()
		{
		int ret = pthread_mutex_lock( &fMutex );
		if ( ret )
		    return false;
		return true;
		}
	bool TryLock( bool& success )
		{
		int ret = pthread_mutex_trylock( &fMutex );
		success = false;
		if ( ret && ret!=EBUSY )
		    return false;
		if ( !ret )
		    success = true;
		return true;
		}
	bool Unlock()
		{
		int ret = pthread_mutex_unlock( &fMutex );
		if ( ret )
		    return false;
		return true;
		}

	pthread_mutex_t fMutex;
	
    };


class MLUCDummyMutex
    {
    public:

	class TLocker
	    {
	    public:
		TLocker( MLUCDummyMutex& )
			{
			}
		TLocker( MLUCDummyMutex&, bool )
			{
			}
		~TLocker()
			{
			}
		bool Lock()
			{
			return true;
			}
		bool TryLock( bool& success )
			{
			success = true;
			return true;
			}
		bool Unlock()
			{
			return true;
			}
	    protected:
	    };

	MLUCDummyMutex()
		{
		}
	~MLUCDummyMutex()
		{
		}

    protected:

    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _MLUCMUTEX_HPP_
