#ifndef _MLUCDYNAMICLIBRARY_HPP_
#define _MLUCDYNAMICLIBRARY_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include <MLUCTypes.h>
#include <dlfcn.h>
#include <MLUCString.hpp>

class MLUCDynamicLibrary
    {
    public:
	
 	MLUCDynamicLibrary();
	~MLUCDynamicLibrary();

	int Open( const char* libName );
	int Close();

	int GetSym( const char* symName, void*& sym );

	const char* GetLastError()
		{
		return fLastError.c_str();
		}

    protected:

	const char* fLibName;
	void* fHandle;

	MLUCString fLastError;

    };


template<class T>
int GetDLSym( MLUCDynamicLibrary& lib, const char* symName, T& sym )
    {
    void* st=NULL;
    int ret;

    ret = lib.GetSym( symName, st );
    sym = reinterpret_cast<T>( reinterpret_cast<unsigned long>( st ) );
    return ret;
    }


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _MLUCDYNAMICLIBRARY_HPP_
