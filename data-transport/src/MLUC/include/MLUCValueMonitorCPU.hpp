/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _MLUCVALUEMONITORCPU_HPP_
#define _MLUCVALUEMONITORCPU_HPP_
/*
***************************************************************************
**
** $Author$ - Initial Version by Arne Wiebalck & Timm Morten Steinbeck 
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCValueMonitor.hpp"
#include "MLUCLog.hpp"

#include <fstream>
#include <cstdlib>
#include <errno.h>

enum kDataType { USER, NICE, SYS, IDLE, USED }; 

/// 
/**
 * @class MLUCValueMonitorCPU
 * @brief The base class for CPU monitoring.
 * @author $Author$ - Initial Version by Arne Wiebalck & Timm Steinbeck
 * @version $Id$
 */
class MLUCValueMonitorCPU : public MLUCValueMonitor {

public:
	/**
	 * @brief Constructor to enable output to file and stdout.
	 * Using this constructor enables output to a file as well as to the 
	 * standard output. The filename is passed to the constructor as well as a
	 * string containing a description of the measurement that will be written 
	 * to the beginning of the file. 
	 * For output to stdout prefixes and suffixes are provided that will be output
	 * respectively before and after the monitored value. Another pre-/suffix pair
	 * is provided for the averaged value. For the averaged value the number of the 
	 * last values over which the average is to be taken can be determined as well. 
	 *
	 * @param filename A string holding the name of the file to which the values monitored are output. 
	 * @param fileDescrHeader A string holding a description of the monitoring taking place. This string
	 * will be written to the start of the output file. 
	 * @param output_prefix A string holding the textual prefix that is to be output before
	 * the measured value.
	 * @param output_suffix A string holding the textual suffix that is to be output after
	 * the measured value.
	 * @param avg_output_prefix A string holding the textual prefix that is to be output before
	 * the average of the measured value.
	 * @param avg_output_suffix A string holding the textual suffix that is to be output after
	 * the average of the measured value.
	 * @param average_items The number of the most recent values over which the average is to be calculated.
	 * @param cpu_number The ID of the cpu to be monitored. If no ID is given the overall CPU usage will
	 * be taken.
	 */
	MLUCValueMonitorCPU( const char* filename, 
			     const char* fileDescrHeader, 
			     const char* output_prefix, 
			     const char* output_suffix, 
			     const char* avg_output_prefix, 
			     const char* avg_output_suffix, 
			     unsigned int average_items,
			     short cpu_number = -1 );
	/**
	 * @brief Constructor to enable only output to stdout.
	 * Using this constructor enables output to the 
	 * standard output. 
	 * Prefixes and suffixes are provided that will be output
	 * respectively before and after the monitored value. Another pre-/suffix pair
	 * is provided for the averaged value. For the averaged value the number of the 
	 * last values over which the average is to be taken can be determined as well. 
	 *
	 * @param output_prefix A string holding the textual prefix that is to be output before
	 * the measured value.
	 * @param output_suffix A string holding the textual suffix that is to be output after
	 * the measured value.
	 * @param avg_output_prefix A string holding the textual prefix that is to be output before
	 * the average of the measured value.
	 * @param avg_output_suffix A string holding the textual suffix that is to be output after
	 * the average of the measured value.
	 * @param average_items The number of the most recent values over which the average is to be calculated.
	 * @param cpu_number The ID of the cpu to be monitored. If no ID is given the overall CPU usage will
	 * be taken.
	 */
	MLUCValueMonitorCPU( const char* output_prefix, 
			     const char* output_suffix, 
			     const char* avg_output_prefix, 
			     const char* avg_output_suffix, 
			     unsigned int average_items,
			     short cpu_number = -1 );
	
	/**
	 * @brief Constructor to enable only output to a file.
	 * Using this constructor enables output to a file. 
	 * The filename is passed to the constructor as well as a
	 * string containing a description of the measurement that will be written 
	 * to the beginning of the file. 
	 * For the averaged value the number of the last values over which the average 
	 * is to be taken can be determined as well. 
	 *
	 * @param filename A string holding the name of the file to which the values monitored are output. 
	 * @param fileDescrHeader A string holding a description of the monitoring taking place. This string
	 * will be written to the start of the output file. 
	 * @param average_items The number of the most recent values over which the average is to be calculated.
	 * @param cpu_number The ID of the cpu to be monitored. If no ID is given the overall CPU usage will
	 * be taken.
	 */
	MLUCValueMonitorCPU( const char* filename, 
			     const char* fileDescrHeader, 
			     unsigned int average_items, 
			     short cpu_number = -1 );

	/**
	 * @brief Constructor with no output.
	 * For the averaged value the number of the last values over which the average 
	 * is to be taken can be determined as well. 
	 *
	 * @param average_items The number of the most recent values over which the average is to be calculated.
	 * @param cpu_number The ID of the cpu to be monitored. If no ID is given the overall CPU usage will
	 * be taken.
	 */
	MLUCValueMonitorCPU( unsigned int average_items, 
			     short cpu_number = -1 );

	/**
	 * @brief Function calculating the CPU usage for User, Nice, System or Idle.
	 * @param type An enum selecting the type of cpu usage to be calculated. This can
         * be USER, NICE, SYS or IDLE. 
	 * @param Value Calculated CPU usage for the selected item.
	 * @return An integer holding a error return code corresponding to the C errno variable. (0 means success)
	 */
	virtual int CalculateValue( enum kDataType type, uint64& Value );

	/**
	 * @brief Function to pick out the number of CPUs in the system.
	 * @param None.
	 * @return A short holding a error return code corresponding to the C errno variable. (0 means success)
	 */
	int GetNumberOfCPUs();

protected:
	/**
	 * The number of CPUs in the system.
	 */
	short kNumber_Of_CPUs;

	/**
	 * The ID of the CPU to be monitored.
	 */
	short         kCPU_Number;
	
	/**
	 * Flag for the first readout. In the constructor this flag is set to true, since 
	 * one needs at least two values to measure something. After the first readout
	 * the flag is set to false.
	 */
	bool          kFirstReadOut;

	/**
	 * The values of the the last and the current readout are stored in these variables.
	 */
	unsigned long kUserOldValue, kUserNewValue, kNiceOldValue, kNiceNewValue, kSysOldValue, kSysNewValue, kIdleOldValue, kIdleNewValue;
};


/**
 * @class MLUCValueMonitorCPU_User
 * @brief Class for CPU User usage monitoring.
 * @author $Author$ - Initial Version by Arne Wiebalck & Timm Steinbeck
 * @version $Id$
 */
class MLUCValueMonitorCPU_User : public MLUCValueMonitorCPU {
public:
	/**
	 * @brief Constructor to enable output to file and stdout.
	 * Using this constructor enables output to a file as well as to the 
	 * standard output. The filename is passed to the constructor as well as a
	 * string containing a description of the measurement that will be written 
	 * to the beginning of the file. 
	 * For output to stdout prefixes and suffixes are provided that will be output
	 * respectively before and after the monitored value. Another pre-/suffix pair
	 * is provided for the averaged value. For the averaged value the number of the 
	 * last values over which the average is to be taken can be determined as well. 
	 *
	 * @param filename A string holding the name of the file to which the values monitored are output. 
	 * @param fileDescrHeader A string holding a description of the monitoring taking place. This string
	 * will be written to the start of the output file. 
	 * @param output_prefix A string holding the textual prefix that is to be output before
	 * the measured value.
	 * @param output_suffix A string holding the textual suffix that is to be output after
	 * the measured value.
	 * @param avg_output_prefix A string holding the textual prefix that is to be output before
	 * the average of the measured value.
	 * @param avg_output_suffix A string holding the textual suffix that is to be output after
	 * the average of the measured value.
	 * @param average_items The number of the most recent values over which the average is to be calculated.
	 * @param cpu_number The ID of the cpu to be monitored. If no ID is given the overall CPU usage will
	 * be taken.
	 */
	 MLUCValueMonitorCPU_User( const char* filename, 
				   const char* fileDescrHeader, 
				   const char* output_prefix, 
				   const char* output_suffix, 
				   const char* avg_output_prefix, 
				   const char* avg_output_suffix, 
				   unsigned int average_items,
				   short cpu_number = -1 );

	 /**
	  * @brief Constructor to enable only output to stdout.
	  * Using this constructor enables output to the 
	  * standard output. 
	  * Prefixes and suffixes are provided that will be output
	  * respectively before and after the monitored value. Another pre-/suffix pair
	  * is provided for the averaged value. For the averaged value the number of the 
	  * last values over which the average is to be taken can be determined as well. 
	  *
	  * @param output_prefix A string holding the textual prefix that is to be output before
	  * the measured value.
	  * @param output_suffix A string holding the textual suffix that is to be output after
	  * the measured value.
	  * @param avg_output_prefix A string holding the textual prefix that is to be output before
	  * the average of the measured value.
	  * @param avg_output_suffix A string holding the textual suffix that is to be output after
	  * the average of the measured value.
	  * @param average_items The number of the most recent values over which the average is to be calculated.
	  * @param cpu_number The ID of the cpu to be monitored. If no ID is given the overall CPU usage will
	  * be taken.
	  */
	 MLUCValueMonitorCPU_User( const char* output_prefix, 
				   const char* output_suffix, 
				   const char* avg_output_prefix, 
				   const char* avg_output_suffix, 
				   unsigned int average_items,
				   short cpu_number = -1 );
	/**
	 * @brief Constructor to enable only output to a file.
	 * Using this constructor enables output to a file. 
	 * The filename is passed to the constructor as well as a
	 * string containing a description of the measurement that will be written 
	 * to the beginning of the file. 
	 * For the averaged value the number of the last values over which the average 
	 * is to be taken can be determined as well. 
	 *
	 * @param filename A string holding the name of the file to which the values monitored are output. 
	 * @param fileDescrHeader A string holding a description of the monitoring taking place. This string
	 * will be written to the start of the output file. 
	 * @param average_items The number of the most recent values over which the average is to be calculated.
	 * @param cpu_number The ID of the cpu to be monitored. If no ID is given the overall CPU usage will
	 * be taken.
	 */
	MLUCValueMonitorCPU_User( const char* filename, 
				  const char* fileDescrHeader, 
				  unsigned int average_items, 
				  short cpu_number = -1 );

	/**
	 * @brief Constructor with no ouput.
	 * For the averaged value the number of the last values over which the average 
	 * is to be taken can be determined as well. 
	 *
	 * @param average_items The number of the most recent values over which the average is to be calculated.
	 * @param cpu_number The ID of the cpu to be monitored. If no ID is given the overall CPU usage will
	 * be taken.
	 */
	MLUCValueMonitorCPU_User( unsigned int average_items, 
				  short cpu_number = -1 );

	/**
	 * @brief Implementation of the Interface function from MLUCValueMonitor class.  
	 * @param MeasuredValued Reference to the monitored value.
	 * @return An integer holding a error return code corresponding to the C errno variable. (0 means success)
	 */
	int GetValue( uint64& MeasuredValue );
};


/**
 * @class MLUCValueMonitorCPU_Nice
 * @brief Class for CPU Nice usage monitoring.
 * @author $Author$ - Initial Version by Arne Wiebalck & Timm Steinbeck
 * @version $Id$
 */
class MLUCValueMonitorCPU_Nice : public MLUCValueMonitorCPU {
public:
	/**
	 * @brief Constructor to enable output to file and stdout.
	 * Using this constructor enables output to a file as well as to the 
	 * standard output. The filename is passed to the constructor as well as a
	 * string containing a description of the measurement that will be written 
	 * to the beginning of the file. 
	 * For output to stdout prefixes and suffixes are provided that will be output
	 * respectively before and after the monitored value. Another pre-/suffix pair
	 * is provided for the averaged value. For the averaged value the number of the 
	 * last values over which the average is to be taken can be determined as well. 
	 *
	 * @param filename A string holding the name of the file to which the values monitored are output. 
	 * @param fileDescrHeader A string holding a description of the monitoring taking place. This string
	 * will be written to the start of the output file. 
	 * @param output_prefix A string holding the textual prefix that is to be output before
	 * the measured value.
	 * @param output_suffix A string holding the textual suffix that is to be output after
	 * the measured value.
	 * @param avg_output_prefix A string holding the textual prefix that is to be output before
	 * the average of the measured value.
	 * @param avg_output_suffix A string holding the textual suffix that is to be output after
	 * the average of the measured value.
	 * @param average_items The number of the most recent values over which the average is to be calculated.
	 * @param cpu_number The ID of the cpu to be monitored. If no ID is given the overall CPU usage will
	 * be taken.
	 */
	MLUCValueMonitorCPU_Nice( const char* filename, 
				  const char* fileDescrHeader, 
				  const char* output_prefix, 
				  const char* output_suffix, 
				  const char* avg_output_prefix, 
				  const char* avg_output_suffix, 
				  unsigned int average_items,
				  short cpu_number = -1 );
	/**
	  * @brief Constructor to enable only output to stdout.
	  * Using this constructor enables output to the 
	  * standard output. 
	  * Prefixes and suffixes are provided that will be output
	  * respectively before and after the monitored value. Another pre-/suffix pair
	  * is provided for the averaged value. For the averaged value the number of the 
	  * last values over which the average is to be taken can be determined as well. 
	  *
	  * @param output_prefix A string holding the textual prefix that is to be output before
	  * the measured value.
	  * @param output_suffix A string holding the textual suffix that is to be output after
	  * the measured value.
	  * @param avg_output_prefix A string holding the textual prefix that is to be output before
	  * the average of the measured value.
	  * @param avg_output_suffix A string holding the textual suffix that is to be output after
	  * the average of the measured value.
	  * @param average_items The number of the most recent values over which the average is to be calculated.
	  * @param cpu_number The ID of the cpu to be monitored. If no ID is given the overall CPU usage will
	  * be taken.
	  */
	MLUCValueMonitorCPU_Nice( const char* output_prefix, 
				  const char* output_suffix, 
				  const char* avg_output_prefix, 
				  const char* avg_output_suffix, 
				  unsigned int average_items,
				  short cpu_number = -1 );

	/**
	 * @brief Constructor to enable only output to a file.
	 * Using this constructor enables output to a file. 
	 * The filename is passed to the constructor as well as a
	 * string containing a description of the measurement that will be written 
	 * to the beginning of the file. 
	 * For the averaged value the number of the last values over which the average 
	 * is to be taken can be determined as well. 
	 *
	 * @param filename A string holding the name of the file to which the values monitored are output. 
	 * @param fileDescrHeader A string holding a description of the monitoring taking place. This string
	 * will be written to the start of the output file. 
	 * @param average_items The number of the most recent values over which the average is to be calculated.
	 * @param cpu_number The ID of the cpu to be monitored. If no ID is given the overall CPU usage will
	 * be taken.
	 */
	MLUCValueMonitorCPU_Nice( const char* filename, 
				  const char* fileDescrHeader, 
				  unsigned int average_items, 
				  short cpu_number = -1 );

	/**
	 * @brief Constructor with no output.
	 * For the averaged value the number of the last values over which the average 
	 * is to be taken can be determined as well. 
	 *
	 * @param average_items The number of the most recent values over which the average is to be calculated.
	 * @param cpu_number The ID of the cpu to be monitored. If no ID is given the overall CPU usage will
	 * be taken.
	 */
	MLUCValueMonitorCPU_Nice( unsigned int average_items, 
				  short cpu_number = -1 );

	/**
	 * @brief Implementation of the Interface function from MLUCValueMonitor class.  
	 * @param MeasuredValued Reference to the monitored value.
	 * @return An integer holding a error return code corresponding to the C errno variable. (0 means success)
	 */
	int GetValue( uint64& MeasuredValue );
};

/**
 * @class MLUCValueMonitorCPU_Sys
 * @brief Class for CPU System usage monitoring.
 * @author $Author$ - Initial Version by Arne Wiebalck & Timm Steinbeck
 * @version $Id$
 */
class MLUCValueMonitorCPU_Sys : public MLUCValueMonitorCPU {
public:
	/**
	 * @brief Constructor to enable output to file and stdout.
	 * Using this constructor enables output to a file as well as to the 
	 * standard output. The filename is passed to the constructor as well as a
	 * string containing a description of the measurement that will be written 
	 * to the beginning of the file. 
	 * For output to stdout prefixes and suffixes are provided that will be output
	 * respectively before and after the monitored value. Another pre-/suffix pair
	 * is provided for the averaged value. For the averaged value the number of the 
	 * last values over which the average is to be taken can be determined as well. 
	  *
	  * @param filename A string holding the name of the file to which the values monitored are output. 
	  * @param fileDescrHeader A string holding a description of the monitoring taking place. This string
	  * will be written to the start of the output file. 
	  * @param output_prefix A string holding the textual prefix that is to be output before
	  * the measured value.
	  * @param output_suffix A string holding the textual suffix that is to be output after
	  * the measured value.
	  * @param avg_output_prefix A string holding the textual prefix that is to be output before
	  * the average of the measured value.
	  * @param avg_output_suffix A string holding the textual suffix that is to be output after
	  * the average of the measured value.
	  * @param average_items The number of the most recent values over which the average is to be calculated.
	  * @param cpu_number The ID of the cpu to be monitored. If no ID is given the overall CPU usage will
	  * be taken.
	  */
	 MLUCValueMonitorCPU_Sys( const char* filename, 
				  const char* fileDescrHeader, 
				  const char* output_prefix, 
				  const char* output_suffix, 
				  const char* avg_output_prefix, 
				  const char* avg_output_suffix, 
				  unsigned int average_items,
				  short cpu_number = -1 );

	 /**
	  * @brief Constructor to enable only output to stdout.
	  * Using this constructor enables output to the 
	  * standard output. 
	  * Prefixes and suffixes are provided that will be output
	  * respectively before and after the monitored value. Another pre-/suffix pair
	  * is provided for the averaged value. For the averaged value the number of the 
	  * last values over which the average is to be taken can be determined as well. 
	  *
	  * @param output_prefix A string holding the textual prefix that is to be output before
	  * the measured value.
	  * @param output_suffix A string holding the textual suffix that is to be output after
	  * the measured value.
	  * @param avg_output_prefix A string holding the textual prefix that is to be output before
	  * the average of the measured value.
	  * @param avg_output_suffix A string holding the textual suffix that is to be output after
	  * the average of the measured value.
	  * @param average_items The number of the most recent values over which the average is to be calculated.
	  * @param cpu_number The ID of the cpu to be monitored. If no ID is given the overall CPU usage will
	  * be taken.
	  */
	 MLUCValueMonitorCPU_Sys( const char* output_prefix, 
				  const char* output_suffix, 
				  const char* avg_output_prefix, 
				  const char* avg_output_suffix, 
				  unsigned int average_items,
				  short cpu_number = -1 );
	
	/**
	 * @brief Constructor to enable only output to a file.
	 * Using this constructor enables output to a file. 
	 * The filename is passed to the constructor as well as a
	 * string containing a description of the measurement that will be written 
	 * to the beginning of the file. 
	 * For the averaged value the number of the last values over which the average 
	 * is to be taken can be determined as well. 
	 *
	 * @param filename A string holding the name of the file to which the values monitored are output. 
	 * @param fileDescrHeader A string holding a description of the monitoring taking place. This string
	 * will be written to the start of the output file. 
	 * @param average_items The number of the most recent values over which the average is to be calculated.
	 * @param cpu_number The ID of the cpu to be monitored. If no ID is given the overall CPU usage will
	 * be taken.
	 */
	MLUCValueMonitorCPU_Sys( const char* filename, 
				 const char* fileDescrHeader, 
				 unsigned int average_items, 
				 short cpu_number = -1 );
	
	/**
	 * @brief Constructor with no output.
	 * For the averaged value the number of the last values over which the average 
	 * is to be taken can be determined as well. 
	 *
	 * @param average_items The number of the most recent values over which the average is to be calculated.
	 * @param cpu_number The ID of the cpu to be monitored. If no ID is given the overall CPU usage will
	 * be taken.
	 */
	MLUCValueMonitorCPU_Sys( unsigned int average_items, 
				 short cpu_number = -1 );
	
	/**
	 * @brief Implementation of the Interface function from MLUCValueMonitor class.  
	 * @param MeasuredValued Reference to the monitored value.
	 * @return An integer holding a error return code corresponding to the C errno variable. (0 means success)
	 */
	int GetValue( uint64& MeasuredValue );
};

/**
 * @class MLUCValueMonitorCPU_Idle
 * @brief Class for CPU Idle usage monitoring.
 * @author $Author$ - Initial Version by Arne Wiebalck & Timm Steinbeck
 * @version $Id$
 */
class MLUCValueMonitorCPU_Idle : public MLUCValueMonitorCPU {
public:
	/**
	 * @brief Constructor to enable output to file and stdout.
	 * Using this constructor enables output to a file as well as to the 
	 * standard output. The filename is passed to the constructor as well as a
	 * string containing a description of the measurement that will be written 
	 * to the beginning of the file. 
	 * For output to stdout prefixes and suffixes are provided that will be output
	 * respectively before and after the monitored value. Another pre-/suffix pair
	 * is provided for the averaged value. For the averaged value the number of the 
	 * last values over which the average is to be taken can be determined as well. 
	  *
	  * @param filename A string holding the name of the file to which the values monitored are output. 
	  * @param fileDescrHeader A string holding a description of the monitoring taking place. This string
	  * will be written to the start of the output file. 
	  * @param output_prefix A string holding the textual prefix that is to be output before
	  * the measured value.
	  * @param output_suffix A string holding the textual suffix that is to be output after
	  * the measured value.
	  * @param avg_output_prefix A string holding the textual prefix that is to be output before
	  * the average of the measured value.
	  * @param avg_output_suffix A string holding the textual suffix that is to be output after
	  * the average of the measured value.
	  * @param average_items The number of the most recent values over which the average is to be calculated.
	  * @param cpu_number The ID of the cpu to be monitored. If no ID is given the overall CPU usage will
	  * be taken.
	  */
	 MLUCValueMonitorCPU_Idle( const char* filename, 
				   const char* fileDescrHeader, 
				   const char* output_prefix, 
				   const char* output_suffix, 
				   const char* avg_output_prefix, 
				   const char* avg_output_suffix, 
				   unsigned int average_items,
				   short cpu_number = -1 );
	
	 /**
	  * @brief Constructor to enable only output to stdout.
	  * Using this constructor enables output to the 
	  * standard output. 
	  * Prefixes and suffixes are provided that will be output
	  * respectively before and after the monitored value. Another pre-/suffix pair
	  * is provided for the averaged value. For the averaged value the number of the 
	  * last values over which the average is to be taken can be determined as well. 
	  *
	  * @param output_prefix A string holding the textual prefix that is to be output before
	  * the measured value.
	  * @param output_suffix A string holding the textual suffix that is to be output after
	  * the measured value.
	  * @param avg_output_prefix A string holding the textual prefix that is to be output before
	  * the average of the measured value.
	  * @param avg_output_suffix A string holding the textual suffix that is to be output after
	  * the average of the measured value.
	  * @param average_items The number of the most recent values over which the average is to be calculated.
	  * @param cpu_number The ID of the cpu to be monitored. If no ID is given the overall CPU usage will
	  * be taken.
	  */
	 MLUCValueMonitorCPU_Idle( const char* output_prefix, 
				   const char* output_suffix, 
				   const char* avg_output_prefix, 
				   const char* avg_output_suffix, 
				   unsigned int average_items,
				   short cpu_number = -1 );

	/**
	 * @brief Constructor to enable only output to a file.
	 * Using this constructor enables output to a file. 
	 * The filename is passed to the constructor as well as a
	 * string containing a description of the measurement that will be written 
	 * to the beginning of the file. 
	 * For the averaged value the number of the last values over which the average 
	 * is to be taken can be determined as well. 
	 *
	 * @param filename A string holding the name of the file to which the values monitored are output. 
	 * @param fileDescrHeader A string holding a description of the monitoring taking place. This string
	 * will be written to the start of the output file. 
	 * @param average_items The number of the most recent values over which the average is to be calculated.
	 * @param cpu_number The ID of the cpu to be monitored. If no ID is given the overall CPU usage will
	 * be taken.
	 */
	MLUCValueMonitorCPU_Idle( const char* filename, 
				  const char* fileDescrHeader, 
				  unsigned int average_items, 
				  short cpu_number = -1 );
	/**
	 * @brief Constructor with no output.
	 * For the averaged value the number of the last values over which the average 
	 * is to be taken can be determined as well. 
	 *
	 * @param average_items The number of the most recent values over which the average is to be calculated.
	 * @param cpu_number The ID of the cpu to be monitored. If no ID is given the overall CPU usage will
	 * be taken.
	 */
	MLUCValueMonitorCPU_Idle( unsigned int average_items, 
				  short cpu_number = -1 );
	/**
	 * @brief Implementation of the Interface function from MLUCValueMonitor class.  
	 * @param MeasuredValued Reference to the monitored value.
	 * @return An integer holding a error return code corresponding to the C errno variable. (0 means success)
	 */
	int GetValue( uint64& MeasuredValue );
};




/**
 * @class MLUCValueMonitorCPU_Used
 * @brief Class for CPU used (non idle) usage monitoring.
 * @author $Author$ - Initial Version by Arne Wiebalck & Timm Steinbeck
 * @version $Id$
 */
class MLUCValueMonitorCPU_Used : public MLUCValueMonitorCPU {
public:
	/**
	 * @brief Constructor to enable output to file and stdout.
	 * Using this constructor enables output to a file as well as to the 
	 * standard output. The filename is passed to the constructor as well as a
	 * string containing a description of the measurement that will be written 
	 * to the beginning of the file. 
	 * For output to stdout prefixes and suffixes are provided that will be output
	 * respectively before and after the monitored value. Another pre-/suffix pair
	 * is provided for the averaged value. For the averaged value the number of the 
	 * last values over which the average is to be taken can be determined as well. 
	  *
	  * @param filename A string holding the name of the file to which the values monitored are output. 
	  * @param fileDescrHeader A string holding a description of the monitoring taking place. This string
	  * will be written to the start of the output file. 
	  * @param output_prefix A string holding the textual prefix that is to be output before
	  * the measured value.
	  * @param output_suffix A string holding the textual suffix that is to be output after
	  * the measured value.
	  * @param avg_output_prefix A string holding the textual prefix that is to be output before
	  * the average of the measured value.
	  * @param avg_output_suffix A string holding the textual suffix that is to be output after
	  * the average of the measured value.
	  * @param average_items The number of the most recent values over which the average is to be calculated.
	  * @param cpu_number The ID of the cpu to be monitored. If no ID is given the overall CPU usage will
	  * be taken.
	  */
	 MLUCValueMonitorCPU_Used( const char* filename, 
				   const char* fileDescrHeader, 
				   const char* output_prefix, 
				   const char* output_suffix, 
				   const char* avg_output_prefix, 
				   const char* avg_output_suffix, 
				   unsigned int average_items,
				   short cpu_number = -1 );
	
	 /**
	  * @brief Constructor to enable only output to stdout.
	  * Using this constructor enables output to the 
	  * standard output. 
	  * Prefixes and suffixes are provided that will be output
	  * respectively before and after the monitored value. Another pre-/suffix pair
	  * is provided for the averaged value. For the averaged value the number of the 
	  * last values over which the average is to be taken can be determined as well. 
	  *
	  * @param output_prefix A string holding the textual prefix that is to be output before
	  * the measured value.
	  * @param output_suffix A string holding the textual suffix that is to be output after
	  * the measured value.
	  * @param avg_output_prefix A string holding the textual prefix that is to be output before
	  * the average of the measured value.
	  * @param avg_output_suffix A string holding the textual suffix that is to be output after
	  * the average of the measured value.
	  * @param average_items The number of the most recent values over which the average is to be calculated.
	  * @param cpu_number The ID of the cpu to be monitored. If no ID is given the overall CPU usage will
	  * be taken.
	  */
	 MLUCValueMonitorCPU_Used( const char* output_prefix, 
				   const char* output_suffix, 
				   const char* avg_output_prefix, 
				   const char* avg_output_suffix, 
				   unsigned int average_items,
				   short cpu_number = -1 );

	/**
	 * @brief Constructor to enable only output to a file.
	 * Using this constructor enables output to a file. 
	 * The filename is passed to the constructor as well as a
	 * string containing a description of the measurement that will be written 
	 * to the beginning of the file. 
	 * For the averaged value the number of the last values over which the average 
	 * is to be taken can be determined as well. 
	 *
	 * @param filename A string holding the name of the file to which the values monitored are output. 
	 * @param fileDescrHeader A string holding a description of the monitoring taking place. This string
	 * will be written to the start of the output file. 
	 * @param average_items The number of the most recent values over which the average is to be calculated.
	 * @param cpu_number The ID of the cpu to be monitored. If no ID is given the overall CPU usage will
	 * be taken.
	 */
	MLUCValueMonitorCPU_Used( const char* filename, 
				  const char* fileDescrHeader, 
				  unsigned int average_items, 
				  short cpu_number = -1 );
	/**
	 * @brief Constructor with no output.
	 * For the averaged value the number of the last values over which the average 
	 * is to be taken can be determined as well. 
	 *
	 * @param average_items The number of the most recent values over which the average is to be calculated.
	 * @param cpu_number The ID of the cpu to be monitored. If no ID is given the overall CPU usage will
	 * be taken.
	 */
	MLUCValueMonitorCPU_Used( unsigned int average_items, 
				  short cpu_number = -1 );
	/**
	 * @brief Implementation of the Interface function from MLUCValueMonitor class.  
	 * @param MeasuredValued Reference to the monitored value.
	 * @return An integer holding a error return code corresponding to the C errno variable. (0 means success)
	 */
	int GetValue( uint64& MeasuredValue );
};


#endif // _MLUCVALUEMONITORCPU_HPP_
