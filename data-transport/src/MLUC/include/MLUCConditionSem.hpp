/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _MLUCCONDITIONSEM_HPP_
#define _MLUCCONDITIONSEM_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCTypes.h"
#include "MLUCVector.hpp"
#include <pthread.h>
#include <sys/time.h>
#include <unistd.h>
#include <errno.h>


/**
 * This is class use as a condition semaphore or signal.
 * It can be used to signal certain conditions between threads.
 * Most importantly it provides a mechanism to have a queue of
 * so called notification data which can be added by one (or more)
 * threads and taken out by another. (It could be taken out by multiple
 * threads as well, but this is discouraged.)
 *
 * @short A thread signal class.
 * @author $Author$
 * @version $Id$
 */
template<class TNotificationDataType>
class MLUCConditionSem
    {
    public:

	/**
	 * Constructor initializes the internal structures.
	 * If the integer paramter slotCntExp2 is given and greater than
	 * or equal to zero, a fixed number of internal slots for notification
	 * data are allocated. The number of slots allocated is 2^slotCntExp2.
	 * For large number of notification data items or this is significantly
	 * faster than a completely dynamically sized array. If the pre-allocated 
	 * slots number is too small, it is doubled in size. If it has been resized
	 * and less than a quarter of the slots are used the number of slots is 
	 * halfed again, but always is at least the initially pre-allocated number.
	 * Initially the object will be in the locked state. (@see #Lock, @see #Unlock)
	 * 
	 * @param slotCntExp2 An integer parameter specifying wether notification 
	 * data slots should be pre-allocated (-1) or the number of slots to
	 * pre-allocate (>=0) (number of slots == 2^slotCntExp2).
	 */
	MLUCConditionSem( int slotCntExp2 = -1 );

	/**
	 * Destructor.
	 */
	virtual ~MLUCConditionSem();

	/**
	 * Wait for a signal to be sent to this thread.
	 * This method returns only when an error occured or when another
	 * thread called the #Signal method.
	 * The object should be locked (@see #Lock) upon calling this method,
	 * will be unlocked atomically prior to performing the wait and will be 
	 * locked again when the method is left.
	 *
	 * @return void
	 */
	void Wait();

	/**
	 * Wait for a signal to be sent to this thread or for the timeout to run out.
	 * This method returns when an error occured, another
	 * thread called the #Signal method or the timeout specified runs out.
	 * The object should be locked (@see #Lock) upon calling this method,
	 * will be unlocked atomically prior to performing the wait and will be 
	 * locked again when the method is left.
	 *
	 * @param timeout_ms A 32 bit unsigned integer specifying a maxmimum time to wait in this method in milliseconds.
	 * @return A bool, true when the method exited because Signal was called, and false when the timeout expired.
	 */
	bool Wait( uint32 timeout_ms );

	/**
	 * Wait for a signal to be sent to this thread or for the timeout to run out.
	 * This method returns when an error occured, another
	 * thread called the #Signal method or the timeout specified runs out.
	 * The object should be locked (@see #Lock) upon calling this method,
	 * will be unlocked atomically prior to performing the wait and will be
	 * locked again when the method is left.
	 *
	 * @param timeout A timeval structure specifying a maxmimum time to wait in this method.
	 * @return A bool, true when the method exited because Signal was called, and false when the timeout expired.
	 */
	bool Wait( const struct timeval* timeout );

	/**
	 * Send a signal to any thread waiting on this object. 
	 * This wakes up any thread in this objects Wait method.
	 * The method tries to acquire the obejct's lock
	 * (@see #Lock, #Unlock) before sending the signal.
	 *
	 * @return void
	 */
	void Signal();

	/**
	 * Lock the object.
	 * 
	 * @see #Unlock
	 * @return void
	 */
	void Lock();
	/**
	 * Unlock the object.
	 * 
	 * @see #Lock
	 * @return void
	 */
	void Unlock();

	/**
	 * Add a notification datum to the object.
	 * The 64 bit unsigned integer is placed at the 
	 * end of the notification data queue.
	 * This method is thread safe with the other
	 * notification data mtehods.
	 *
	 * @param data A 64 bit unsigned integer notification data item.
	 * @return void
	 */
	void AddNotificationData( TNotificationDataType data );
	/**
	 * Check wether some notification data is available in this object.
	 *
	 * @return A bool saying wether notification data is available (true) or not (false).
	 */
	bool HaveNotificationData();
	/**
	 * Get the first item of notification data from the head of the queue
	 * without removing it from the queue.
	 *
	 * @return The notification data item at the head of the queue.
	 */
	TNotificationDataType PeekNotificationData();
	/**
	 * Get the first item of notification data from the head of the queue
	 * and remove it from the queue.
	 *
	 * @return The notification data item at the head of the queue.
	 */
	TNotificationDataType PopNotificationData();

	/**
	 * Determine the number of notification data items in the object.
	 *
	 * @return An unsigned long holding the number of notification data items in the object.
	 */
	unsigned long GetNotificationDataCnt();

    protected:

	/**
	 * The low-level pthread condition semaphore used for signalling.
	 */
	pthread_cond_t fCondition;

	/**
	 * The low-level pthread mutex semaphore used for regulating access
	 * to the #fCondition condition semaphore.
	 */
	pthread_mutex_t fConditionSem;

	/**
	 * The low-level pthread mutex semaphore used for regulating access 
	 * to the notification data.
	 */
	pthread_mutex_t fAccessSem;
	/**
	 * The MLUCVector (dynamic array) class holding the
	 * notification data items.
	 */
	MLUCVector<TNotificationDataType> fNotificationData;

    private:
    };



template<class TNotificationDataType>
inline MLUCConditionSem<TNotificationDataType>::MLUCConditionSem( int slotCntEpx2 ):
    fNotificationData( slotCntEpx2, true )
    {
    pthread_mutex_init( &fConditionSem, NULL );
    pthread_cond_init( &fCondition, NULL );
    pthread_mutex_init( &fAccessSem, NULL );
    // XXX Lock mutexes??
    }

template<class TNotificationDataType>
inline MLUCConditionSem<TNotificationDataType>::~MLUCConditionSem()
    {
    pthread_mutex_destroy( &fConditionSem );
    pthread_cond_destroy( &fCondition );
    pthread_mutex_destroy( &fAccessSem );
    }

template<class TNotificationDataType>
inline void MLUCConditionSem<TNotificationDataType>::Wait()
    {
    pthread_cond_wait( &fCondition, &fConditionSem );
    }

// true when condition was signalled, false, when timeout expired
template<class TNotificationDataType>
inline bool MLUCConditionSem<TNotificationDataType>::Wait( uint32 timeout_ms )
    {
    int ret;
    struct timespec wakeup_time;
    struct timeval tv;
    wakeup_time.tv_sec = timeout_ms/1000;
    wakeup_time.tv_nsec = (timeout_ms%1000)*1000000;
    gettimeofday( &tv, NULL );
    wakeup_time.tv_sec += tv.tv_sec;
    wakeup_time.tv_nsec += (tv.tv_usec*1000);
    if ( wakeup_time.tv_nsec >= 1000000000 )
	{
	wakeup_time.tv_sec++;
	wakeup_time.tv_nsec -= 1000000000;
	}
    ret = pthread_cond_timedwait( &fCondition, &fConditionSem, &wakeup_time );
    if ( ret==ETIMEDOUT )
	return false;
    return true;
    }

// true when condition was signalled, false, when timeout expired
template<class TNotificationDataType>
inline bool MLUCConditionSem<TNotificationDataType>::Wait( const struct timeval* timeout )
    {
    if ( !timeout )
	return false;
    int ret;
    struct timespec wakeup_time;
    struct timeval tv;
    gettimeofday( &tv, NULL );
    wakeup_time.tv_sec = timeout->tv_sec+tv.tv_sec;
    wakeup_time.tv_nsec = (timeout->tv_usec+tv.tv_usec)*1000;
    while ( wakeup_time.tv_nsec >= 1000000000 )
	{
	wakeup_time.tv_sec++;
	wakeup_time.tv_nsec -= 1000000000;
	}
    ret = pthread_cond_timedwait( &fCondition, &fConditionSem, &wakeup_time );
    if ( ret==ETIMEDOUT )
	return false;
    return true;
    }

template<class TNotificationDataType>
inline void MLUCConditionSem<TNotificationDataType>::Signal()
    {
    pthread_mutex_lock( &fConditionSem );
    pthread_cond_broadcast( &fCondition );
    pthread_mutex_unlock( &fConditionSem );
    }

template<class TNotificationDataType>
inline void MLUCConditionSem<TNotificationDataType>::Lock()
    {
    pthread_mutex_lock( &fConditionSem );
    }

template<class TNotificationDataType>
inline void MLUCConditionSem<TNotificationDataType>::Unlock()
    {
    pthread_mutex_unlock( &fConditionSem );
    }


template<class TNotificationDataType>
inline void MLUCConditionSem<TNotificationDataType>::AddNotificationData( TNotificationDataType data )
    {
    pthread_mutex_lock( &fAccessSem );
    fNotificationData.Add( data );
    pthread_mutex_unlock( &fAccessSem );
    }

template<class TNotificationDataType>
inline bool MLUCConditionSem<TNotificationDataType>::HaveNotificationData()
    {
    bool ret;
    pthread_mutex_lock( &fAccessSem );
    ret = fNotificationData.GetCnt()>0;
    pthread_mutex_unlock( &fAccessSem );
    return ret;
    }

template<class TNotificationDataType>
inline TNotificationDataType MLUCConditionSem<TNotificationDataType>::PeekNotificationData()
    {
    TNotificationDataType ret;
    pthread_mutex_lock( &fAccessSem );
    ret = fNotificationData.GetFirst();
    pthread_mutex_unlock( &fAccessSem );
    return ret;
    }

template<class TNotificationDataType>
inline TNotificationDataType MLUCConditionSem<TNotificationDataType>::PopNotificationData()
    {
    TNotificationDataType ret;
    pthread_mutex_lock( &fAccessSem );
    ret = fNotificationData.GetFirst();
    fNotificationData.RemoveFirst();
    pthread_mutex_unlock( &fAccessSem );
    return ret;
    }

template<class TNotificationDataType>
inline unsigned long MLUCConditionSem<TNotificationDataType>::GetNotificationDataCnt()
    {
    unsigned long cnt;
    pthread_mutex_lock( &fAccessSem );
    cnt = fNotificationData.GetCnt();
    pthread_mutex_unlock( &fAccessSem );
    return cnt;
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _MLUCCONDITIONSEM_HPP_
