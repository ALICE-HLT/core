#ifndef _MLUCTESTS_HPP_
#define _MLUCTESTS_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include <MLUCTypes.h>

#define MLUCINITTESTS() bool gTestFailure=false; bool gLastTestFailure=false; unsigned gTestCounter=0; unsigned gTestFailureCounter=0;
#define MLUCDECLARETESTS() extern bool gTestFailure; extern bool gLastTestFailure; extern unsigned gTestCounter; extern unsigned gTestFailureCounter;

#define MLUCTEST( test, text ) { ++gTestCounter; if ( !(test) ) { ++gTestFailureCounter; std::cerr << __FILE__ << ":" << __LINE__ << " TEST FAILURE: " << text << std::endl; gLastTestFailure=gTestFailure=true; } else gLastTestFailure=false; }
#define MLUCLASTTESTFAILURE() gLastTestFailure
#define MLUCLASTTESTOK() (!gLastTestFailure)


#define MLUCTESTEND() { if ( gTestFailure ) { std::cerr << gTestCounter << " tests completed with " << gTestFailureCounter << " failures." << std::endl; return -1; } else { std::cout << gTestCounter << " tests completed successfully." << std::endl; return 0; } }


/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _MLUCTESTS_HPP_
