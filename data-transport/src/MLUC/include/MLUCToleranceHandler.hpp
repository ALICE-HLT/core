#ifndef _MLUCTOLERANCEHANDLER_HPP_
#define _MLUCTOLERANCEHANDLER_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCTypes.h"

class MLUCToleranceHandler
    {
    public:

	MLUCToleranceHandler()
		{
		fCnt = 0;
		fUpCnt = 0;
		fNdxDivisor = 1;
		}
	~MLUCToleranceHandler()
		{
		}

	void SetNdxDivisor( unsigned long ndxDiv )
		{
		fNdxDivisor = ndxDiv;
		}

	void Add()
		{
		fOk.insert( fOk.end(), true );
		fMaps.insert( fMaps.end(), 0 );
		*(fMaps.begin()+fUpCnt) = fCnt;
 		fCnt++;
		fUpCnt++;
		}

	void Del( unsigned long ndx )
		{
		if ( ndx >= fCnt )
		    return;
		if ( IsOk(ndx) )
		    {
		    fUpCnt--;
		    }
		fOk.erase( fOk.begin()+ndx );
		fMaps.erase( fMaps.begin() );
		fCnt--;
		unsigned long i, n, cnt;
		n = cnt = 0;
		for ( i = 0; i < fCnt; i++ )
		    {
		    if ( fOk[i] )
			{
			*(fMaps.begin()+n) = i; // i+cnt??
			n++;
			}
		    else
			cnt++;
		    }
		}

	void Set( unsigned long ndx, bool up )
		{
		if ( ndx >= fCnt )
		    return;
		if ( *(fOk.begin()+ndx) == up )
		    return;
		unsigned long i, n, cnt;
		fUpCnt += ( up ? +1 : -1 );
		*(fOk.begin()+ndx) = up;
		n = cnt = 0;
		for ( i = 0; i < fCnt; i++ )
		    {
		    if ( fOk[i] )
			{
			*(fMaps.begin()+n) = i; // i+cnt??
			n++;
			}
		    else
			cnt++;
		    }
		
		}
	
	bool IsOk( unsigned long ndx )
		{
		if ( ndx >= fCnt )
		    return false;
		return fOk[ ndx ];
		}

	bool Get( uint64 ndx, unsigned long& baseNdx, unsigned long& backupNdx )
		{
		if ( fCnt <= 0 )
		    {
		    return false;
		    }

		unsigned long realNdx = ndx/fNdxDivisor;
		
		unsigned mod = ( realNdx % fCnt );
		baseNdx = mod;
		if ( (mod>=fOk.size()) || (mod>=fMaps.size()) )
		    {
		    return false;
		    }
		
		if ( fOk[ mod ] )
		    {
		    backupNdx = mod;
		    }
		else
		    {
		    if ( fUpCnt<= 0 )
			return false;
		    //mod = (realNdx % fUpCnt );
		    mod = ( realNdx / fCnt) % fUpCnt;
		    if ( mod >= fMaps.size() )
			{
			return false;
			}
		    mod = fMaps[ mod ];
		    if ( (mod>=fOk.size()) || (mod>=fMaps.size()) )
			{
			return false;
			}
		    backupNdx = mod;
		    }
		
		return true;
		}

    protected:

	vector<bool> fOk;
	vector<unsigned long> fMaps;
	
	unsigned long fCnt;
	unsigned long fUpCnt;

	unsigned long fNdxDivisor;

    private:
    };




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _MLUCTOLERANCEHANDLER_HPP_
