/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _MLUCMSG_H_
#define _MLUCMSG_H_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include <limits.h>
#include <pthread.h>

#ifdef __cplusplus
extern "C" {
#endif


typedef unsigned char uint8;

#if USHRT_MAX==65535
typedef unsigned short uint16;
#else // USHRT_MAX==65535

#if UINT_MAX==65535
typedef unsigned int uint16;
#else // UINT_MAX==65535

#if ULONG_MAX==65535l
typedef unsigned long uint16;
#else // ULONG_MAX==65535l

#error Could not typedef uint16

#endif // ULONG_MAX==65535l

#endif // UINT_MAX==65535


#endif // USHRT_MAX==65535

#if USHRT_MAX==4294967295
typedef unsigned short uint32;
#else // USHRT_MAX==4294967295

#if UINT_MAX==4294967295
typedef unsigned int uint32;
#else // UINT_MAX==4294967295

#if ULONG_MAX==4294967295l
typedef unsigned long uint32;
#else // ULONG_MAX==4294967295l

#error Could not typedef uint32

#endif // ULONG_MAX==4294967295l

#endif // UINT_MAX==4294967295

#endif // USHRT_MAX==4294967295

#if ULONG_MAX==18446744073709551615UL
typedef unsigned long uint64;
#else // ULONG_MAX==18446744073709551615UL

#if defined __GNUC__
typedef unsigned long long uint64;
#else // defined __GNUC__

#error Could not typedef uint64

#endif // defined __GNUC__

#endif // ULONG_MAX==18446744073709551615UL



typedef pthread_t MLUCThreadID;





#ifdef __cplusplus
}
#endif

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif /* _MLUCMSG_H_ */
