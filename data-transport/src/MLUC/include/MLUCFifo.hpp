#ifndef _MLUCFIFO_HPP_
#define _MLUCFIFO_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#define PARANOID_LOCKING


#include "MLUCTypes.h"
#include "MLUCConditionSem.hpp"
#include "MLUCLog.hpp"
#ifdef PARANOID_LOCKING
#include "MLUCMutex.hpp"
#endif
#include <pthread.h>
#include <cstdlib>


#ifdef PARANOID_LOCKING
//#warning Using paranoid locking. This could possibly be removed. Has to be tested first though...
// XXXX TODO LOCKING XXXX
#endif

/**
 * A structure for messages passed between threads with the MLUCFifo
 * class.
 *
 * @see MLUCFifo
 * @short A class for messages in the MLUCFifo class.
 * @author $Author$
 * @version $ID$
 */
struct MLUCFifoMsg
    {
	/**
	 * Length of the message in bytes!
	 */
	unsigned fLength;
	/**
	 * Type of the message itself.
	 * A message specifier.
	 */
	unsigned fType;
	/**
	 * Start of the message payload.
	 */
	unsigned fData[0];
    };


#define PARANOIA

/**
 * A fifo class for sending messages between threads.
 * The class can either copy messages directly into the fifo
 * or it can reserve the amount of space specified by the user
 * nedded for the message and return a pointer to it, so that 
 * the message can be directly written into this buffer without
 * the need for an intermediate copy. The class takes care of 
 * only allocating contiguous space for messages.
 * Another special feature of the class is the availability of 
 * two buffers for writing, the larger "regular" buffer and the 
 * smaller "emergency" one. On reading the buffer cannot be 
 * explicitly specified, but instead when data is available from
 * the emergency buffer that is read out, and only when there is
 * no data in the emergency buffer is the regular one read out.
 * The buffers can be made resizable or fixed size. In the first 
 * case, resizing will be done in steps of powers of two bytes,
 * independently for the two buffers. Shrinking is done by halfing
 * a buffer's size, only when a buffer is at most a quarter full 
 * and never below the buffer's initial size.
 *
 * @see MLUCFifoMsg
 * @short A fifo class for inter-thread communication.
 * @author $Author$
 * @version $Id$
 */
class MLUCFifo
    {
    public:

	/**
	 * Constructor.
	 * Initializes the buffers to the given sizes. The sizes are given as exponents to 2. 
	 * So the regular buffer will be 2^initialSizeExp2 bytes large and the emergency 
	 * buffer 2^initialEmergencySizeExp2 bytes. When the third parameter is set to false
	 * the buffers will not be resized on overflow, but any write operation will fail.
	 *
	 * @param initialSizeExp2 Specifies the power of two to use as the initial buffer size
	 * for the regular buffer.
	 * @param initialEmergencySizeExp2 Specifies the power of two to use as the initial buffer
	 * size for the emergency buffer.
	 * @param grow Boolean flag specifying wether the buffers are resizeable (true, default)
	 * or fixed size (false).
	 */
	MLUCFifo( unsigned initialSizeExp2, unsigned initialEmergencySizeExp2, bool grow = true );

	/**
	 * Destructor.
	 */
	~MLUCFifo();

	/**
	 * Boolean conversion operator. Can be used to determine wether the fifo can be used.
	 *
	 * @return A boolean stating wether the fifo object is ok or not.
	 */
	operator bool() const;

	/**
	 * Copy the given message into the regular buffer.
	 *
	 * @param msg A pointer to the message to be copied into the buffer.
	 * The length of the message is read out from the message header.
	 * @return An integer value giving the status of the operation.
	 * This is 0 on success or an error value according to errno.h
	 * on failure.
	 */
	int Write( const MLUCFifoMsg* msg );

	/**
	 * Reserve a message slot of the given length.
	 * This method will reserve a contigious space of the given amount
	 * of bytes. Note that the length of the message written into the 
	 * slot as well as the message size given to CommitMsg has to be 
	 * identical to the value specified here.
	 * This method will lock write access and resize access to the fifo.
	 *
	 * @see CommitMsg
	 * @param msgLength The size of the message slot to reserve in bytes.
	 * @param msgPtr In this parameter the pointer to the start of the 
	 * reserved message slot is returned.
	 * @return An integer value giving the status of the operation.
	 * This is 0 on success or an error value according to errno.h
	 * on failure.
	 */
	int ReserveMsgSlot( unsigned msgLength, MLUCFifoMsg*& msgPtr );

	/**
	 * Commit a message in a previously reserved slot.
	 * The length specified here has to be identical to the msg length
	 * reserved in ReserveMsgSlot.
	 * This method will unlock write access and resize access to the fifo.
	 *
	 * @see ReserveMsgSlot
	 * @param msgLength The size of the reserved slot to commit.
	 * @return An integer value giving the status of the operation.
	 * This is 0 on success or an error value according to errno.h
	 * on failure.
	 */
	int CommitMsg( unsigned msgLength );

	/**
	 * Wait for data to be available for reading.
	 *
	 * @return An integer value giving the status of the operation.
	 * This is 0 on success or an error value according to errno.h
	 * on failure.
	 */
	int WaitForData();

	/**
	 * Read the next available message from the fifo. If a message
	 * is present in the emergency buffer, then that will be read, 
	 * otherwise a message will be read from the regular buffer. 
	 * If no message is available in either buffer, EAGAIN is
	 * returned.
	 * This method will allocate a chunk of memory for the message
	 * and return the pointer to that memory in the msg parameter.
	 * To free this memory FreeMsg should be called.
	 *
	 * @see FreeMsg
	 * @param msg A reference to a pointer to a fifo message. The
	 * pointer to the message read out will be placed in this 
	 * reference. If no message could be found, this is NULL.
	 * @return An integer value giving the status of the operation.
	 * This is 0 on success or an error value according to errno.h
	 * on failure. If no message is available EAGAIN is returned.
	 */
	int Read( MLUCFifoMsg*& msg );

	/**
	 * Free a message returned by a call to Read.
	 * 
	 * @see Read
	 * @param msg The pointer to the message to free as returned
	 * by Read.
	 * @return An integer value giving the status of the operation.
	 * This is 0 on success or an error value according to errno.h
	 * on failure.
	 */
	int FreeMsg( MLUCFifoMsg* msg );

	/**
	 * Return a pointer to the next message in the fifo.
	 * Contrary to Read this method will not allocate anything
	 * but returns a pointer directly into the fifo.
	 * If a message is available in the emergency buffer a pointer
	 * to that is returned. Otherwise a pointer to the regular
	 * buffer is returned if a message is available there. If now 
	 * message is available in either buffer a NULL pointer is 
	 * returned and the return value of the method itself is EAGAIN.
	 * Messages pointers received by this method should be freed by 
	 * FreeNextMsg.
	 *
	 * @see Read
	 * @see FreeNextMsg
	 * @param msg A reference to a pointer to a fifo message. The
	 * pointer to the message read out will be placed in this 
	 * reference. If no message could be found, this is NULL.
	 * @return An integer value giving the status of the operation.
	 * This is 0 on success or an error value according to errno.h
	 * on failure. If no message is available EAGAIN is returned.
	 */
	int GetNextMsg( MLUCFifoMsg*& msg );

	/**
	 * Free a message obtained from GetNextMsg.
	 *
	 * @see GetNextMsg
	 * @param msg The pointer to the message to free as returned
	 * by GetNextMsg.
	 * @return An integer value giving the status of the operation.
	 * This is 0 on success or an error value according to errno.h
	 * on failure.
	 */
	int FreeNextMsg( MLUCFifoMsg* msg );

	/**
	 * Copy the given message into the emergency buffer.
	 *
	 * @param msg A pointer to the message to be copied into the buffer.
	 * The length of the message is read out from the message header.
	 * @return An integer value giving the status of the operation.
	 * This is 0 on success or an error value according to errno.h
	 * on failure.
	 */
	int WriteEmergency( const MLUCFifoMsg* msg );

	/**
	 * Place a limit on the amount of buffer space used before allowing 
	 * to shrink the buffer. part is the exponent of two of the divisor
	 * of the buffer. E.g. if part is two, then at most a quarter (1/(2^2))
	 * of the buffer space can be used before allowing a shrink.
	 * part==2 is the smallest allowed part. A buffer shrink always halfs
	 * the buffer size.
	 *
	 * @param part 1/(2^part) is the maximum part of the buffer that can be used
	 * before allowing a shrink of the buffer.
	 * @return void.
	 */
	void SetMaxShrinkPart( unsigned part )
		{
		if ( part >= 2 )
		    fBuffer.fMaxShrinkPart = part;
		}

#ifdef PARANOIA
	unsigned long GetGrowCnt() const
		{
		return fBuffer.fGrowCnt;
		}
	unsigned long GetShrinkCnt() const
		{
		return fBuffer.fShrinkCnt;
		}
	void DumpBufferData( MLUCLog::TLogLevel level )
		{
		DumpBufferData( fBuffer, level );
		}
#endif

    protected:

	/**
	 * Helper structure holding all the information about one buffer.
	 * A FIFO class actually has two of these buffers, the normal and the emergency one.
	 */
	struct TBufferData
	    {

		/**
		 * Pointer to the buffer space itself.
		 */
		uint8* fBuffer;
		
		/**
		 * The buffer size.
		 */
		unsigned long fBufferSize;

		/** 
		 * The initial buffer size.
		 */
		unsigned long fBufferOrigSize;
		
		/**
		 * The current shrink limit.
		 * This places a limit on the amount of buffer space used before allowing 
		 * to shrink the buffer. part is the exponent of two of the divisor
		 * of the buffer. E.g. if part is two, then at most a quarter (1/(2^2))
		 * of the buffer space can be used before allowing a shrink.
		 * part==2 is the smallest allowed part. A buffer shrink always halfs
		 * the buffer size.
		 */
		unsigned fMaxShrinkPart;
		
		/**
		 * The buffer read index (/pointer).
		 */
		unsigned long fBufferRead;
		unsigned long fBufferReadWriteTmp;
		unsigned long fBufferWrite;
		unsigned long fBufferWriteReadTmp;

		bool fWriteWrap;
		
#ifdef PARANOIA
		unsigned long fReserveBufferSize;
		unsigned long fReserveBufferRead;
		unsigned long fReserveBufferWrite;

		unsigned long fGrowCnt;
		unsigned long fShrinkCnt;
#endif

		MLUCConditionSem<uint8>* fSignal;

		pthread_mutex_t fRelocMutex;
	    };


	/**
	 */
	static int Resize( TBufferData& buffer, bool grow );

	static int ReadFrom( TBufferData& buffer, MLUCFifoMsg*& msg, bool grow );

	static int GetNextMsgFrom( TBufferData& buffer, MLUCFifoMsg*& msg, bool grow );
	static int FreeNextMsgFrom( TBufferData& buffer, MLUCFifoMsg* msg, bool grow );

	static int WriteTo( TBufferData& buffer, const MLUCFifoMsg* msg, bool grow );

	static int ReserveMsgSlotTo( TBufferData& buffer, unsigned msgLength, MLUCFifoMsg*& msgPtr, bool grow );
	static int CommitMsgTo( TBufferData& buffer, unsigned msgLength, bool grow );

	static void GetUsableSpace( TBufferData& buffer, bool& wrap, unsigned long& part1, unsigned long& part2, bool update = false );
	static unsigned long GetUsedSpace( TBufferData& buffer, bool update = false );

#ifdef PARANOIA
	static void DumpBufferData( TBufferData& buffer, MLUCLog::TLogLevel level )
		{
		LOG( level, "MLUCFifo::DumpBufferData", "Buffer dump" )
 		    << "Dump: fBufferSize: " << MLUCLog::kDec << buffer.fBufferSize
		    << " - fBufferReadWriteTmp: " << buffer.fBufferReadWriteTmp << " - fBufferWriteReadTmp: " << buffer.fBufferWriteReadTmp
		    << " - fBufferRead: " << buffer.fBufferRead << " - fBufferWrite: " << buffer.fBufferWrite
		    << " ---- fReserveBufferSize: " << MLUCLog::kDec << buffer.fReserveBufferSize
		    << " - fReserveBufferRead: " << buffer.fReserveBufferRead << " - fReserveBufferWrite: " << buffer.fReserveBufferWrite
		    << "." << ENDLOG;
		}
#endif



	bool fGrow;

	TBufferData fBuffer;

	TBufferData fEmergency;


// 	uint8* fEmergency;
// 	unsigned long fEmergencySize;

// 	unsigned long fEmergencyRead;
// 	unsigned long fEmergencyWrite;

// 	unsigned long fEmergencyReadCnt;
// 	unsigned long fEmergencyWriteCnt;


	MLUCConditionSem<uint8> fSignal;

#ifdef PARANOID_LOCKING
	MLUCMutex fMutex;
	MLUCMutex::TLocker fMutexLocker;
#endif


    private:
	MLUCFifo():fMutexLocker(fMutex) {};
	MLUCFifo(MLUCFifo const&):fMutexLocker(fMutex) {};
	MLUCFifo& operator=(MLUCFifo const&) {return *this;}
    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _MLUCFIFO_HPP_
