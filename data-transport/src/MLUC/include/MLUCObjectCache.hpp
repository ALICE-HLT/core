#ifndef _MLUCOBJECTCACHE_HPP_
#define _MLUCOBJECTCACHE_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCVector.hpp"
#include "MLUCIndexedVector.hpp"
#include <pthread.h>

template<class T>
class MLUCObjectCache
    {
    public:
	
	MLUCObjectCache( unsigned cntExp2, bool grow=true ):
	    fFree( cntExp2 ), fUsed( 8, cntExp2 ), fGrow( grow )
		{
		unsigned long i, cnt;
		cnt = 1 << cntExp2;
		T* tmp;
		bool failed = false;
		fOk = true;
		fUsed.SetPreDivisor( sizeof(T) << 1 );
		for ( i = 0; i < cnt; i++ )
		    {
		    tmp = new T;
		    if ( tmp )
			fFree.Add( tmp );
		    else
			{
			failed = true;
			break;
			}
		    }
		
		if ( failed )
		    {
		    fOk = false;
		    while ( fFree.GetCnt()>0 )
			{
			tmp = fFree.GetFirst();
			delete tmp;
			fFree.RemoveFirst();
			}
		    }
		fOrigSize = fFree.GetSize();
		pthread_mutex_init( &fMutex, NULL );
		}

	~MLUCObjectCache()
		{
#ifdef PROFILE_INDEXED_VECTOR
		LOG( MLUCLog::kImportant, "MLUCObjectCache", "Profile Data" )
		    << "MLUCIndexedVector for MLUCObjectCache 0x" << MLUCLog::kHex << (unsigned long long)this
		    << " is at 0x" << (unsigned long long)&fUsed << ENDLOG;
#endif
		T* tmp;
		pthread_mutex_destroy( &fMutex );
		while ( fFree.GetCnt()>0 )
		    {
		    tmp = fFree.GetFirst();
		    delete tmp;
		    fFree.RemoveFirst();
		    }
		}
	
	T* Get()
		{
		pthread_mutex_lock( &fMutex );
		T* tmp;
		if ( fFree.GetCnt()>0 )
		    {
		    tmp = fFree.GetFirst();
		    fFree.RemoveFirst();
		    fUsed.Add( tmp, (unsigned long)tmp );
		    pthread_mutex_unlock( &fMutex );
		    return tmp;
		    }
		else if ( !fGrow )
		    {
		    pthread_mutex_unlock( &fMutex );
		    return 0;
		    }
		else
		    {
		    // Nothing available, double total size
		    unsigned long i, cnt = fFree.GetSize();
		    bool failure = false;
		    for ( i = 0; i < cnt; i++ )
			{
			tmp = new T;
			if ( tmp )
			    fFree.Add( tmp );
			else
			    {
			    failure = true;
			    break;
			    }
			}
		    if ( failure )
			{
			while ( fFree.GetCnt()>0 )
			    {
			    tmp = fFree.GetFirst();
			    fFree.RemoveFirst();
			    delete tmp;
			    }
			pthread_mutex_unlock( &fMutex );
			return 0;
			}
		    tmp = fFree.GetFirst();
		    fFree.RemoveFirst();
		    fUsed.Add( tmp, (unsigned long)tmp );
		    pthread_mutex_unlock( &fMutex );
		    return tmp;
		    }
		}

	void Release( T* ptr )
		{
		unsigned long ndx;
		pthread_mutex_lock( &fMutex );
		if ( fUsed.FindElement( (unsigned long)ptr, ndx ) )
		    {
		    fUsed.Remove( ndx );
		    ptr->ResetCachedObject();
		    fFree.Add( ptr );
		    }
		if ( ( (fFree.GetSize()-fFree.GetCnt()) < (fFree.GetSize()>>3) ) && fFree.GetSize()>fOrigSize ) // if amount used is less than 1/8 of what is available half the size (if the size was increased since the start...)
		    {
		    unsigned long i, cnt = fFree.GetSize()>>1;
		    T* tmp;
		    for ( i = 0; i<cnt && fFree.GetCnt()>0; i++ )
			{
			tmp = fFree.GetFirst();
			fFree.RemoveFirst();
			delete tmp;
			}
		    }
		pthread_mutex_unlock( &fMutex );
		}

	operator bool()
		{
		return fOk;
		}

    protected:

	typedef T* TPtr;

	unsigned long fSize;
	unsigned long fOrigSize;

	bool fOk;

	MLUCVector<TPtr> fFree;
	MLUCIndexedVector<TPtr,unsigned long> fUsed;

	bool fGrow;

	pthread_mutex_t fMutex;

	static bool FindPtr( const TPtr& ptr, const TPtr& ref )
		{
		return ptr == ref;
		}

    private:
    };




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _MLUCOBJECTCACHE_HPP_
