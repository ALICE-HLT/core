/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _MLUCLOGSERVER_HPP_
#define _MLUCLOGSERVER_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCTypes.h"
#include "MLUCLog.hpp"
#include "MLUCString.hpp"
#include <iostream>
#include <fstream>


/**
 * Abstract base class for the log servers. Derived classes can
 * be added into a logging object and will then receive all logged
 * messages and can further process them.
 *
 * @short A logging server abstract base class
 * @author $Author$ - Initial version by Timm Morten Steinbeck
 * @version $Id$
 * @see MLUCLog
 */
class MLUCLogServer
	{
	public:

	    virtual ~MLUCLogServer() {};

		/**
		 * Logging method for normal (non-modulo) messages.
		 * This has to be overridden by derived classes.
		 *
		 * @return void
		 * @param origin Textual origin description
		 * @param keyword Textual keyword(s)
		 * @param file Name of the originating source file
		 * @param linenr Line number in the originating source file
		 * @param compile_date Source compilation date
		 * @param compile_time Source compilation time
		 * @param logLvl The logging level of this message
		 * @param hostname Name of the originating host
		 * @param id The id of the originating log object (process)
		 * @param ldate Date at which the message was logged
		 * @param ltime_s Time at which the message was logged (sec. part)
		 * @param ltime_us Time at which the message was logged (usec. part)
		 * @param msgNr The sequence number of this message
		 * @param line The logged message itself
		 */
 		virtual void LogLine( const char* origin, const char* keyword, const char* file, int linenr, 
				      const char* compile_date, const char* compile_time, MLUCLog::TLogLevel logLvl, 
				      const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
				      unsigned long runNumber, 
#endif
				      uint32 ldate, 
				      uint32 ltime_s, uint32 ltime_us, uint32 msgNr, const char* line ) = 0;

		/**
		 * Logging method for modulo type messages.
		 * This has to be overridden by derived classes.
		 *
		 * @return void
		 * @param origin Textual origin description
		 * @param keyword Textual keyword(s)
		 * @param file Name of the originating source file
		 * @param linenr Line number in the originating source file
		 * @param compile_date Source compilation date
		 * @param compile_time Source compilation time
		 * @param logLvl The logging level of this message
		 * @param hostname Name of the originating host
		 * @param id The id of the originating log object (process)
		 * @param ldate Date at which the message was logged
		 * @param ltime_s Time at which the message was logged (sec. part)
		 * @param ltime_us Time at which the message was logged (usec. part)
		 * @param msgNr The sequence number of this message
		 * @param submsgNr The modulo sequence number of this message
		 * @param line The logged message itself
		 */
		virtual void LogLine( const char* origin, const char* keyword, const char* file, int linenr, 
				      const char* compile_date, const char* compile_time, MLUCLog::TLogLevel, 
				      const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
				      unsigned long runNumber, 
#endif
				      uint32 ldate, 
				      uint32 ltime_s, uint32 ltime_us, uint32 msgNr, uint32 subMsgNr, 
				      const char* line ) = 0;

	protected:
	private:
	};

/**
 * Log server class derived from MLUCLogServer.
 * Just discards every incoming message...
 *
 * @short A discarding logging server class
 * @author $Author$ - Initial version by Timm Morten Steinbeck
 * @version $Id$
 * @see MLUCLog
 * @see MLUCLogServer
 */
class MLUCDevNullLogServer: public MLUCLogServer
	{
	public:

	    virtual ~MLUCDevNullLogServer() {};

		/**
		 * Logging method for normal (non-modulo) messages.
		 * Discards every incoming message
		 *
		 * @return void
		 * @param origin Textual origin description
		 * @param keyword Textual keyword(s)
		 * @param file Name of the originating source file
		 * @param linenr Line number in the originating source file
		 * @param compile_date Source compilation date
		 * @param compile_time Source compilation time
		 * @param logLvl The logging level of this message
		 * @param hostname Name of the originating host
		 * @param id The id of the originating log object (process)
		 * @param ldate Date at which the message was logged
		 * @param ltime_s Time at which the message was logged (sec. part)
		 * @param ltime_us Time at which the message was logged (usec. part)
		 * @param msgNr The sequence number of this message
		 * @param line The logged message itself
		 */
 		virtual void LogLine( const char*, const char*, const char*, int, const char*,
				      const char*, MLUCLog::TLogLevel, const char*, const char*,
				      unsigned long, uint32, uint32, uint32, uint32, const char* ) { };

		/**
		 * Logging method for modulo type messages.
		 * Discards every incoming message
		 *
		 * @return void
		 * @param origin Textual origin description
		 * @param keyword Textual keyword(s)
		 * @param file Name of the originating source file
		 * @param linenr Line number in the originating source file
		 * @param compile_date Source compilation date
		 * @param compile_time Source compilation time
		 * @param logLvl The logging level of this message
		 * @param hostname Name of the originating host
		 * @param id The id of the originating log object (process)
		 * @param ldate Date at which the message was logged
		 * @param ltime_s Time at which the message was logged (sec. part)
		 * @param ltime_us Time at which the message was logged (usec. part)
		 * @param msgNr The sequence number of this message
		 * @param submsgNr The modulo sequence number of this message
		 * @param line The logged message itself
		 */
		virtual void LogLine( const char*, const char*, const char*, int, const char*,
				      const char*, MLUCLog::TLogLevel, const char*, const char*, 
				      unsigned long, uint32, uint32, uint32, uint32, uint32, const char* ) { };


	protected:
	private:
	};



/**
 * Log server class derived from MLUCLogServer.
 * Prints out incoming messages properly formatted to 
 * standard output.
 *
 * @short A stdout logging server class
 * @author $Author$ - Initial version by Timm Morten Steinbeck
 * @version $Id$
 * @see MLUCLog
 * @see MLUCLogServer
 */
class MLUCStdoutLogServer: public MLUCLogServer
	{
	public:

	    virtual ~MLUCStdoutLogServer() {};

		/**
		 * Logging method for normal (non-modulo) messages.
		 * Writes incoming messages properly formatted to stdout
		 *
		 * @return void
		 * @param origin Textual origin description
		 * @param keyword Textual keyword(s)
		 * @param file Name of the originating source file
		 * @param linenr Line number in the originating source file
		 * @param compile_date Source compilation date
		 * @param compile_time Source compilation time
		 * @param logLvl The logging level of this message
		 * @param hostname Name of the originating host
		 * @param id The id of the originating log object (process)
		 * @param ldate Date at which the message was logged
		 * @param ltime_s Time at which the message was logged (sec. part)
		 * @param ltime_us Time at which the message was logged (usec. part)
		 * @param msgNr The sequence number of this message
		 * @param line The logged message itself
		 */
		virtual void LogLine( const char* origin, const char* keyword, const char* file, int linenr, 
				      const char* compile_date, const char* compile_time, MLUCLog::TLogLevel logLvl, 
				      const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
				      unsigned long runNumber, 
#endif
				      uint32 ldate, 
				      uint32 ltime_s, uint32 ltime_us, uint32 msgNr, const char* line );

		/**
		 * Logging method for modulo type messages.
		 * Writes incoming messages properly formatted to stdout
		 *
		 * @return void
		 * @param origin Textual origin description
		 * @param keyword Textual keyword(s)
		 * @param file Name of the originating source file
		 * @param linenr Line number in the originating source file
		 * @param compile_date Source compilation date
		 * @param compile_time Source compilation time
		 * @param logLvl The logging level of this message
		 * @param hostname Name of the originating host
		 * @param id The id of the originating log object (process)
		 * @param ldate Date at which the message was logged
		 * @param ltime_s Time at which the message was logged (sec. part)
		 * @param ltime_us Time at which the message was logged (usec. part)
		 * @param msgNr The sequence number of this message
		 * @param submsgNr The modulo sequence number of this message
		 * @param line The logged message itself
		 */
		virtual void LogLine( const char* origin, const char* keyword, const char* file, int linenr, 
				      const char* compile_date, const char* compile_time, MLUCLog::TLogLevel, 
				      const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
				      unsigned long runNumber, 
#endif
				      uint32 ldate, 
				      uint32 ltime_s, uint32 ltime_us, uint32 msgNr, uint32 subMsgNr, const char* line );

	protected:
	private:
	};


/**
 * Log server class derived from MLUCLogServer.
 * Prints out incoming messages properly formatted to 
 * standard error..
 *
 * @short A stderr logging server class
 * @author $Author$ - Initial version by Timm Morten Steinbeck
 * @version $Id$
 * @see MLUCLog
 * @see MLUCLogServer
 */
class MLUCStderrLogServer: public MLUCLogServer
	{
	public:

	    virtual ~MLUCStderrLogServer() {};

		/**
		 * Logging method for normal (non-modulo) messages.
		 * Writes incoming messages properly formatted to stderr
		 *
		 * @return void
		 * @param origin Textual origin description
		 * @param keyword Textual keyword(s)
		 * @param file Name of the originating source file
		 * @param linenr Line number in the originating source file
		 * @param compile_date Source compilation date
		 * @param compile_time Source compilation time
		 * @param logLvl The logging level of this message
		 * @param hostname Name of the originating host
		 * @param id The id of the originating log object (process)
		 * @param ldate Date at which the message was logged
		 * @param ltime_s Time at which the message was logged (sec. part)
		 * @param ltime_us Time at which the message was logged (usec. part)
		 * @param msgNr The sequence number of this message
		 * @param line The logged message itself
		 */
		virtual void LogLine( const char* origin, const char* keyword, const char* file, int linenr, 
				      const char* compile_date, const char* compile_time, MLUCLog::TLogLevel logLvl, 
				      const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
				      unsigned long runNumber, 
#endif
				      uint32 ldate, 
				      uint32 ltime_s, uint32 ltime_us, uint32 msgNr, const char* line );

		/**
		 * Logging method for modulo type messages.
		 * Writes incoming messages properly formatted to stderr
		 *
		 * @return void
		 * @param origin Textual origin description
		 * @param keyword Textual keyword(s)
		 * @param file Name of the originating source file
		 * @param linenr Line number in the originating source file
		 * @param compile_date Source compilation date
		 * @param compile_time Source compilation time
		 * @param logLvl The logging level of this message
		 * @param hostname Name of the originating host
		 * @param id The id of the originating log object (process)
		 * @param ldate Date at which the message was logged
		 * @param ltime_s Time at which the message was logged (sec. part)
		 * @param ltime_us Time at which the message was logged (usec. part)
		 * @param msgNr The sequence number of this message
		 * @param submsgNr The modulo sequence number of this message
		 * @param line The logged message itself
		 */
		virtual void LogLine( const char* origin, const char* keyword, const char* file, int linenr, 
				      const char* compile_date, const char* compile_time, MLUCLog::TLogLevel, 
				      const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
				      unsigned long runNumber, 
#endif
				      uint32 ldate, 
				      uint32 ltime_s, uint32 ltime_us, uint32 msgNr, uint32 subMsgNr, const char* line );

	protected:
	private:
	};


/**
 * Log server class derived from MLUCLogServer.
 * Prints out incoming messages properly formatted into 
 * the specified output stream object.
 *
 * @short A stream logging server class
 * @author $Author$ - Initial version by Timm Morten Steinbeck
 * @version $Id$
 * @see MLUCLog
 * @see MLUCLogServer
 */
class MLUCStreamLogServer: public MLUCLogServer
	{
	public:

	    virtual ~MLUCStreamLogServer() {};

		/**
		 * Constructor. Takes as argument the stream into which incoming
		 * messages are to be written.
		 *
		 * @param str Reference to the output stream that will receive incoming log messages.
		 */
		MLUCStreamLogServer( ostream& str );

		/**
		 * Logging method for normal (non-modulo) messages.
		 * Writes incoming messages properly formatted to the output stream 
		 * associated with this object.
		 *
		 * @return void
		 * @param origin Textual origin description
		 * @param keyword Textual keyword(s)
		 * @param file Name of the originating source file
		 * @param linenr Line number in the originating source file
		 * @param compile_date Source compilation date
		 * @param compile_time Source compilation time
		 * @param logLvl The logging level of this message
		 * @param hostname Name of the originating host
		 * @param id The id of the originating log object (process)
		 * @param ldate Date at which the message was logged
		 * @param ltime_s Time at which the message was logged (sec. part)
		 * @param ltime_us Time at which the message was logged (usec. part)
		 * @param msgNr The sequence number of this message
		 * @param line The logged message itself
		 */
		virtual void LogLine( const char* origin, const char* keyword, const char* file, int linenr, 
				      const char* compile_date, const char* compile_time, MLUCLog::TLogLevel logLvl, 
				      const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
				      unsigned long runNumber, 
#endif
				      uint32 ldate, 
				      uint32 ltime_s, uint32 ltime_us, uint32 msgNr, const char* line );

		/**
		 * Logging method for modulo type messages.
		 * Writes incoming messages properly formatted to the output stream 
		 * associated with this object.
		 *
		 * @return void
		 * @param origin Textual origin description
		 * @param keyword Textual keyword(s)
		 * @param file Name of the originating source file
		 * @param linenr Line number in the originating source file
		 * @param compile_date Source compilation date
		 * @param compile_time Source compilation time
		 * @param logLvl The logging level of this message
		 * @param hostname Name of the originating host
		 * @param id The id of the originating log object (process)
		 * @param ldate Date at which the message was logged
		 * @param ltime_s Time at which the message was logged (sec. part)
		 * @param ltime_us Time at which the message was logged (usec. part)
		 * @param msgNr The sequence number of this message
		 * @param submsgNr The modulo sequence number of this message
		 * @param line The logged message itself
		 */
		virtual void LogLine( const char* origin, const char* keyword, const char* file, int linenr, 
				      const char* compile_date, const char* compile_time, MLUCLog::TLogLevel, 
				      const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
				      unsigned long runNumber, 
#endif
				      uint32 ldate, 
				      uint32 ltime_s, uint32 ltime_us, uint32 msgNr, uint32 subMsgNr, const char* line );

	protected:

		/**
		 * A reference to the output stream into which log messages
		 * are written.
		 */
		ostream& fStream;

	private:
	};


class MLUCFilteredStdoutLogServer: public MLUCLogServer
    {
	public:

	virtual void LogLine( const char* origin, const char* keyword, const char* file, int linenr, 
			      const char* compile_date, const char* compile_time, MLUCLog::TLogLevel logLvl, 
			      const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
			      unsigned long runNumber, 
#endif
			      uint32 ldate, 
			      uint32 ltime_s, uint32 ltime_us, uint32 msgNr, const char* line );

	virtual void LogLine( const char* origin, const char* keyword, const char* file, int linenr, 
			      const char* compile_date, const char* compile_time, MLUCLog::TLogLevel, 
			      const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
			      unsigned long runNumber, 
#endif
			      uint32 ldate, 
			      uint32 ltime_s, uint32 ltime_us, uint32 msgNr, uint32 subMsgNr, const char* line );

	virtual ~MLUCFilteredStdoutLogServer() {};

	protected:

	private:
    };

class MLUCFilteredStderrLogServer: public MLUCLogServer
    {
	public:


	virtual void LogLine( const char* origin, const char* keyword, const char* file, int linenr, 
			      const char* compile_date, const char* compile_time, MLUCLog::TLogLevel logLvl, 
			      const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
			      unsigned long runNumber, 
#endif
			      uint32 ldate, 
			      uint32 ltime_s, uint32 ltime_us, uint32 msgNr, const char* line );

	virtual void LogLine( const char* origin, const char* keyword, const char* file, int linenr, 
			      const char* compile_date, const char* compile_time, MLUCLog::TLogLevel, 
			      const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
				      unsigned long runNumber, 
#endif
			      uint32 ldate, 
			      uint32 ltime_s, uint32 ltime_us, uint32 msgNr, uint32 subMsgNr, const char* line );

	virtual ~MLUCFilteredStderrLogServer() {};

	protected:

	private:
    };


class MLUCSysLogServer: public MLUCLogServer
    {
    public:

	MLUCSysLogServer( const char* ident, int facility );
	virtual ~MLUCSysLogServer();

	virtual void LogLine( const char* origin, const char* keyword, const char* file, int linenr, 
			      const char* compile_date, const char* compile_time, MLUCLog::TLogLevel logLvl, 
			      const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
			      unsigned long runNumber, 
#endif
			      uint32 ldate, 
			      uint32 ltime_s, uint32 ltime_us, uint32 msgNr, const char* line );

	virtual void LogLine( const char* origin, const char* keyword, const char* file, int linenr, 
			      const char* compile_date, const char* compile_time, MLUCLog::TLogLevel, 
			      const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
			      unsigned long runNumber, 
#endif
			      uint32 ldate, 
			      uint32 ltime_s, uint32 ltime_us, uint32 msgNr, uint32 subMsgNr, const char* line );


    protected:

	MLUCString fIdent;
	int fFacility;

    private:
    };



class MLUCFileLogServer: public MLUCLogServer
    {
	public:

	MLUCFileLogServer( const char* file_prefix, const char* file_suffix, 
			   unsigned long msg_count_per_file, unsigned long overlap_msg_count = 0, unsigned long max_file_count = 0 );
	MLUCFileLogServer( const char* directory, const char* file_prefix, const char* file_suffix, 
			   unsigned long msg_count_per_file, unsigned long overlap_msg_count = 0, unsigned long max_file_count = 0 );
	virtual ~MLUCFileLogServer() {};


	virtual void LogLine( const char* origin, const char* keyword, const char* file, int linenr, 
			      const char* compile_date, const char* compile_time, MLUCLog::TLogLevel logLvl, 
			      const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
			      unsigned long runNumber, 
#endif
			      uint32 ldate, 
			      uint32 ltime_s, uint32 ltime_us, uint32 msgNr, const char* line );

	virtual void LogLine( const char* origin, const char* keyword, const char* file, int linenr, 
			      const char* compile_date, const char* compile_time, MLUCLog::TLogLevel, 
			      const char* hostname, const char* id, 
#ifdef MLUCLOG_RUNNUMBER_SUPPORT
			      unsigned long runNumber, 
#endif
			      uint32 ldate, 
			      uint32 ltime_s, uint32 ltime_us, uint32 msgNr, uint32 subMsgNr, const char* line );

	protected:

	void CloseFile();
	void OpenNextFile();

	MLUCString fDirectory;
	MLUCString fFilePrefix;
	MLUCString fFileSuffix;
	unsigned long fMsgsPerFile;
	unsigned long fOverlapMsgs;
	unsigned long fMaxFileCount;

	MLUCString fCurrentFileName;
	unsigned long fCurrentLineCount;
	unsigned long fCurrentFileCount;

	ofstream fFile;
	
	vector<MLUCString> fOverlapLines;

	private:
    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _MLUCLOGSERVER_HPP_
