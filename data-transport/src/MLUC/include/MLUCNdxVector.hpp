#ifndef _MLUCNDXVECTOR_HPP_
#define _MLUCNDXVECTOR_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCTypes.h"
#include <vector>

template<class TElementType, class TNdxType>
class MLUCNdxVector
    {
    public:

	typedef bool (*TSearchFunc)( const TElementType& el, uint64 searchData );
	//typedef int (*TCompareFunc)( const TElementType& el1, const TElementType& el2 );

	MLUCNdxVector( int bufferSizeExp2 = -1, bool grow = true ):
	    fDefaultElementSet( false ), fGrow( grow )
		{
		Init( bufferSizeExp2 );
		}
	MLUCNdxVector( int bufferSizeExp2, TNdxType defaultNdx, TElementType defaultElement, bool grow = true ):
	    fDefaultElement( defaultElement ), fDefaultNdx( defaultNdx ), fDefaultElementSet( true ), fGrow( grow )
		{
		Init( bufferSizeExp2 );
		}

	bool Add( const TNdxType& newNdx, const TElementType& newElement )
		{
		unsigned long empty;
		return Add( newNdx, newElement, empty );
		}
	bool Add( const TNdxType& newNdx, const TElementType& newElement, unsigned long& newElementNdx )
		{
		if ( !fDataSize )
		    {
		    vector<TElementType>::iterator p;
		    p = fData.insert( fData.end(), newElement );
		    fNdx.insert( fNdx.end(), newNdx );
		    newElementNdx = p-fData.begin();
		    }
		else
		    {
		    if ( fInvalidDataCnt+fDataCnt==fDataSize && fInvalidDataCnt>0 )
			Compact();
		    else if ( fDataCnt==fDataSize )
			{
			if ( fGrow )
			    Resize( true );
			else
			    return false;
			}
		    newElementNdx = fNextFree;
		    *(fDataStart+fNextFree) = newElement;
		    *(fNdxStart+fNextFree) = newNdx;
		    *(fUsedStart+fNextFree) = true;
		    fNextFree = (fNextFree+1) & fDataMask;
		    }
		fDataCnt++;
		return true;
		}

	//bool Insert( const TElementType& newElement, TCompareFunc compFunc );
	//bool Insert( const TElementType& newElement, TCompareFunc compFunc, unsigned long& newElementNdx );

	bool Remove( unsigned long elementNdx )
		{
		if ( !fDataSize )
		    {
		    if ( elementNdx>=fData.size() )
			return false;
		    fData.erase( fData.begin()+elementNdx );
		    fDataCnt--;
		    }
		else
		    {
		    if ( elementNdx>=fDataSize || fDataCnt<=0 )
			return false;
		    //     if ( !(fDataStart+elementNdx)->fValid )
		    // 	return true;
		    *(fUsedStart+elementNdx) = false;
		    if ( fDefaultElementSet )
			{
			*(fDataStart+elementNdx) = fDefaultElement;
			*(fNdxStart+elementNdx) = fDefaultNdx;
			}
		    fDataCnt--;
		    if ( elementNdx==fFirstUsed )
			{
			fFirstUsed = (fFirstUsed+1) & fDataMask;
			while ( !(*(fUsedStart+fFirstUsed)) && fFirstUsed!=fNextFree )
			    {
			    fFirstUsed = (fFirstUsed+1) & fDataMask;
			    fInvalidDataCnt--;
			    }
			}
		    else if ( elementNdx == ((fNextFree-1) & fDataMask) )
			{
			fNextFree = elementNdx;
			while ( !(*(fUsedStart+((fNextFree-1) & fDataMask))) && fFirstUsed!=fNextFree )
			    {
			    fNextFree = (fNextFree-1) & fDataMask;
			    fInvalidDataCnt--;
			    }
			
			}
		    else
			fInvalidDataCnt++;
		    if ( (fDataCnt < (fDataSize>>2)) && (fDataSize>fOrigDataSize) )
			{
			Compact();
			Resize( false ); // We shrink
			}
		    }
		return true;
		}
	bool RemoveFirst()
		{
		if ( !fDataSize )
		    {
		    if ( fData.size()<=0 )
			return false;
		    fData.erase( fData.begin() );
		    fDataCnt--;
		    }
		else
		    {
		    if ( fDataCnt<=0 )
			return false;
		    *(fUsedStart+fFirstUsed) = false;
		    if ( fDefaultElementSet )
			{
			*(fDataStart+fFirstUsed) = fDefaultElement;
			*(fNdxStart+fFirstUsed) = fDefaultNdx;
			}
		    fDataCnt--;
		    fFirstUsed = (fFirstUsed+1) & fDataMask;
		    while ( !(*(fUsedStart+fFirstUsed)) && fFirstUsed!=fNextFree )
			{
			fFirstUsed = (fFirstUsed+1) & fDataMask;
			fInvalidDataCnt--;
			}
		    if ( (fDataCnt < (fDataSize>>2)) && (fDataSize>fOrigDataSize) )
			{
			Compact();
			Resize( false ); // We shrink
			}
		    }
		return true;
		}

	bool FindElement( TSearchFunc searchFunc, uint64 searchData, unsigned long& elementNdx )
		{
		if ( !fDataSize )
		    {
		    unsigned long n = 0;
		    vector<TElementType>::iterator iter, end;
		    iter = fData.begin();
		    end = fData.end();
		    while ( iter != end )
			{
			if ( (*searchFunc)( *iter, searchData ) )
			    {
			    elementNdx = n;
			    return true;
			    }
			iter++;
			n++;
			}
		    }
		else
		    {
		    unsigned long i = 0;
		    unsigned long n = fFirstUsed;
		    while ( i++<fDataCnt+fInvalidDataCnt && (!(*(fUsedStart+n)) || !(*searchFunc)( *(fDataStart+n), searchData )) )
			n = (n+1) & fDataMask;
		    if ( *(fUsedStart+n) && (*searchFunc)( *(fDataStart+n), searchData ) )
			{
			elementNdx = n;
			return true;
			}
		    }
		return false;
		}

	bool FindElement( TNdxType searchData, unsigned long& elementNdx )
		{
		if ( !fDataSize )
		    {
		    unsigned long n = 0;
		    vector<TNdxType>::iterator iter, end;
		    iter = fNdx.begin();
		    end = fNdx.end();
		    while ( iter != end )
			{
			if ( *iter == searchData )
			    {
			    elementNdx = n;
			    return true;
			    }
			iter++;
			n++;
			}
		    }
		else
		    {
		    unsigned long i = 0;
		    unsigned long n = fFirstUsed;
		    while ( i++<fDataCnt+fInvalidDataCnt && (!(*(fUsedStart+n)) || *(fNdxStart+n)!=searchData ) )
			n = (n+1) & fDataMask;
		    if ( *(fUsedStart+n) && *(fNdxStart+n)==searchData )
			{
			elementNdx = n;
			return true;
			}
		    }
		return false;
		}

	bool FindElement( unsigned long startNdx, bool directionForward, TSearchFunc searchFunc, uint64 searchData, unsigned long& elementNdx );
	
	bool IsValid( unsigned long elementNdx ) const 
		{
		if ( !fDataSize )
		    {
		    if ( elementNdx >= fData.size() )
			return false;
		    return true;
		    }
		else
		    {
		    if ( elementNdx >= fDataSize )
			return false;
		    if ( !(*(fUsedStart+elementNdx)) )
			 return false;
		    return true;
		    }
		}

	TElementType Get( unsigned long elementNdx )
		{
		if ( !fDataSize )
		    {
		    if ( elementNdx>=fData.size() )
			return fDefaultElement;
		    return *(fData.begin()+elementNdx);
		    }
		else
		    {
		    if ( fFirstUsed<fNextFree )
			{
			if ( elementNdx<fFirstUsed || elementNdx>=fNextFree )
			    return fDefaultElement;
			}
		    else
			{
			if ( elementNdx<fFirstUsed && elementNdx>=fNextFree )
			    return fDefaultElement;
			}
		    return *(fDataStart+elementNdx);
		    }
		}
	TElementType* GetPtr( unsigned long elementNdx )
		{
		if ( !fDataSize )
		    {
		    if ( elementNdx>=fData.size() )
			return NULL;
		    return (fData.begin()+elementNdx);
		    }
		else
		    {
		    if ( fFirstUsed<fNextFree )
			{
			if ( elementNdx<fFirstUsed || elementNdx>=fNextFree )
			    return NULL;
			}
		    else
			{
			if ( elementNdx<fFirstUsed && elementNdx>=fNextFree )
			    return NULL;
			}
		    return (fDataStart+elementNdx);
		    }
		}
	bool Get( unsigned long elementNdx, TElementType& element )
		{
		if ( !fDataSize )
		    {
		    if ( elementNdx>=fData.size() )
			return false;
		    element = *(fData.begin()+elementNdx);
		    return true;
		    }
		else
		    {
		    if ( fFirstUsed<fNextFree )
			{
			if ( elementNdx<fFirstUsed || elementNdx>=fNextFree )
			    return false;
			}
		    else
			{
			if ( elementNdx<fFirstUsed && elementNdx>=fNextFree )
			    return false;
			}
		    element = *(fDataStart+elementNdx);
		    return true;
		    }
		}

	TElementType GetFirst()
		{
		if ( fDataCnt<=0 )
		    return fDefaultElement;
		if ( !fDataSize )
		    {
		    return *fData.begin();
		    }
		else
		    {
		    return *(fDataStart+fFirstUsed);
		    }
		}
	TElementType* GetFirstPtr()
		{
		if ( fDataCnt<=0 )
		    return NULL;
		if ( !fDataSize )
		    {
		    return fData.begin();
		    }
		else
		    {
		    return fDataStart+fFirstUsed;
		    }
		}
	bool GetFirst( TElementType& element )
		{
		if ( fDataCnt<=0 )
		    return false;
		if ( !fDataSize )
		    {
		    element = *fData.begin();
		    }
		else
		    {
		    element = *(fDataStart+fFirstUsed);
		    }
		return true;
		}

	bool IsFixedSize()
		{
		return fOrigDataSize!=0;
		}
	bool IsGrowable()
		{
		return fGrow;
		}
	unsigned long GetStartNdx()
		{
		if ( !fDataSize )
		    return 0;
		else
		    return fFirstUsed;
		}
	unsigned long GetEndNdx() // Returns the index that is 1 beyond the last valid index
		{
		if ( !fDataSize )
		    return fData.size();
		else
		    return fNextFree;
		}
	unsigned long GetMask()
		{
		if ( !fDataSize )
		    return ~(unsigned long)0;
		else
		    return fDataMask;
		}
	unsigned long GetCnt()
		{
		return fDataCnt;
		}

    protected:

	void Init( int bufferSizeExp2 );
	void Compact(); // Only called in fixed size mode.
	void Resize( bool grow ); // Only called in fixed size mode.

	TElementType fDefaultElement;
	TNdxType fDefaultNdx;
	bool fDefaultElementSet;

	vector<TElementType> fData;
	TElementType* fDataStart;
	vector<TNdxType> fNdx;
	TNdxType* fNdxStart;
	vector<unsigned char> fUsed;
	unsigned char* fUsedStart;

	unsigned long fDataCnt;
	unsigned long fInvalidDataCnt;
	unsigned long fDataSize;
	unsigned long fOrigDataSize;
	unsigned long fDataMask;
	unsigned long fFirstUsed;
	unsigned long fNextFree;
	bool fGrow;

    private:
    };





template<class TElementType>
inline bool MLUCNdxVector<TElementType>::FindElement( unsigned long startNdx, bool directionForward, TSearchFunc searchFunc, uint64 searchData, unsigned long& elementNdx )
    {
    if ( fDataCnt<=0 )
	return false;
    if ( !fDataSize )
	{
	vector<TElementType>::iterator begin, end;
	begin = fData.begin();
	end = fData.end();
	if ( startNdx>fData.size() )
	    startndx = fData.size();
	vector<TElementType>::iterator iter = begin+startNdx;
	vector<TElementType>::iterator boundary = (directionForward ? end : begin);
	int step = (directionForward ? 1 : -1);
	int offset = (directionForward ? 0 : -1);
	while ( iter!=boundary )
	    {
	    if ( (*searchFunc)( *(iter+offset), searchData ) )
		{
		elementNdx = n;
		return true;
		}
	    iter += step;
	    startNdx += step;
	    }
	return false;
	}
    else
	{
	unsigned long i = 0;
	unsigned long n = fFirstUsed;
	int step = (directionForward ? 1 : -1);
	unsigned long boundary = (directionForward ? fNextFree : (fFirstUsed-1) & fTimeoutMask);
	if ( fFirstUsed<fNextFree )
	    {
	    if ( startNdx<fFirstUsed || startNdx>=fNextFree )
		{
		if ( directionForward )
		    startNdx = fFirstUsed;
		else
		    startNdx = (fNextFree-1) & fTimeoutMask;
		}
	    }
	else
	    {
	    if ( startNdx<fFirstUsed && startNdx>=fNextFree )
		{
		if ( directionForward )
		    startNdx = fFirstUsed;
		else
		    startNdx = (fNextFree-1) & fTimeoutMask;
		}
	    }
	unsigned long cnt;
	if ( directionForward )
	    {
	    if ( fFirstUsed<fNextFree )
		cnt = fNextFree-startNdx;
	    else
		{
		if ( startNdx<fFirstUsed )
		    cnt = fNextFree-startNdx;
		else
		    cnt = fNextFree+fDataSize-startNdx;
		}
	    }
	else
	    {
	    if ( fFirstUsed<fNextFree )
		cnt = startNdx-fFirstUsed;
	    else
		{
		if ( startNdx<fFirstUsed )
		    cnt = startNdx+fDataSize-fFirstUsed;
		else
		    cnt = startNdx-fFirstUsed;
		}
	    }
	while ( i++<cnt && (!(*(fUsedStart+n)) || !(*searchFunc)( *(fDataStart+n), searchData )) )
	    n = (n+step) & fDataMask;
	if ( (fUsedStart+n) && (*searchFunc)( *(fDataStart+n), searchData ) )
	    {
	    elementNdx = n;
	    return true;
	    }
	else
	    return false;
	}
    }

template<class TElementType>
inline void MLUCNdxVector<TElementType>::Init( int bufferSizeExp2 )
    {
    fDataCnt = 0;
    fInvalidDataCnt = 0;
    fFirstUsed = fNextFree = 0;
    if ( bufferSizeExp2 < 0 )
	{
	fDataSize = 0;
	fOrigDataSize = 0;
	fDataMask = ~(unsigned long)0;
	fDataStart = NULL;
	fUsedStart = NULL;
	fNdxStart = NULL;
	}
    else
	{
	fOrigDataSize = fDataSize = 1 << bufferSizeExp2;
	fDataMask = fDataSize-1;
	fData.resize( fDataSize, fDefaultElement );
	fUsed.resize( fDataSize, false );
	fNdx.resize( fDataSize, fDefaultNdx );
	fDataStart = fData.begin();
	fUsedStart = fUsed.begin();
	fNdxStart = fNdx.begin();
	}
    }

template<class TElementType>
inline void MLUCNdxVector<TElementType>::Compact()
    {
    unsigned long i = 0;
    unsigned long dest, src;
    for ( i = 0, dest = src = fFirstUsed; i<(fDataCnt+fInvalidDataCnt); i++, src = (src+1) & fDataMask )
	{
	if ( *(fUsedStart+src) )
	    {
	    if ( dest != src )
		{
		*(fUsedStart+dest) = true;
		*(fDataStart+dest) = *(fDataStart+src);
		*(fNdxStart+dest) = *(fNdxStart+src);
		}
	    dest = (dest+1) & fDataMask;
	    }
	}
    for ( i = dest; i!= fNextFree; i=(i+1) & fDataMask )
	{
	*(fUsedStart+i) = false;
	if ( fDefaultElementSet )
	    {
	    *(fDataStart+i) = fDefaultElement;
	    *(fNdxStart+i) = fDefaultNdx;
	    }
	}
    fNextFree = dest;
    fInvalidDataCnt = 0;
    }

template<class TElementType>
inline void MLUCNdxVector<TElementType>::Resize( bool grow )
    {
    unsigned long newSize;
    //TElementType elm( fDefaultElement, false );
    // Do we grow or shrink?
    if ( grow )
	{
	// We grow...
	newSize = fDataSize<<1;
	if ( fDefaultElementSet )
	    {
	    fData.resize( newSize, fDefaultElement );
	    fNdx.resize( newSize, fDefaultNdx );
	    }
	else
	    {
	    fData.resize( newSize );
	    fNdx.resize( newSize );
	    }
	fUsed.resize( newSize, false );
	fDataStart = fData.begin();
	fUsedStart = fUsed.begin();
	fNdxStart = fNdx.begin();
	// Note: We double the size...
	if ( fNextFree<fFirstUsed || (fNextFree==fFirstUsed && (fDataCnt+fInvalidDataCnt>0)) )
	    {
	    // The used area is split into two parts, one at the beginning
	    // and one at the end of the (old) array.
	    // In other words, the buffer has partly wrapped around.
	    // We copy the block at the beginning of the buffer to the end of the block at 
	    // the end (of the old size buffer, now in the middle)
	    memcpy( fDataStart+fDataSize, fDataStart, fNextFree*sizeof(TElementType) );
	    memcpy( fNdxStart+fDataSize, fNdxStart, fNextFree*sizeof(TNdxType) );
	    memcpy( fUsedStart+fDataSize, fUsedStart, fNextFree*sizeof(unsigned char) );
	    unsigned long i;
	    for ( i = 0; i < fNextFree; i++ )
		{
		if ( fDefaultElementSet )
		    {
		    *(fDataStart+i) = fDefaultElement;
		    *(fNdxStart+i) = fDefaultNdx;
		    }
		*(fUsedStart+i) = false;
		}
	    fNextFree += fDataSize;
	    }
	fDataSize = newSize;
	fDataMask = fDataSize-1;
	}
    else
	{
	// We shrink...
	newSize = fDataSize>>1;
	// Note: fNextFree==fFirstUsed means it is totally empty, 
	// as we use at most 1/4 of the buffer.
	if ( fNextFree < fFirstUsed )
	    {
	    // We have the used area split into two parts, one at the beginning
	    // and one at the end of the (old) array.
	    // In other words, the buffer has partly wrapped around.
	    // Note: We half the size...
	    // This means that newSize is both the new size and the amount by which we shrink.
	    // Also, newSize is then the amount by which we have to move the used slots 
	    // at the (old) end of the buffer toward its beginning (its new end). And since we half the size, we can be
	    // sure not to have any overlap between the src and dest. (We are at most a quarter the old size)
	    memcpy( fDataStart+fFirstUsed-newSize, fDataStart+fFirstUsed, (fDataSize-fFirstUsed)*sizeof(TElementType) );
	    memcpy( fNdxStart+fFirstUsed-newSize, fNdxStart+fFirstUsed, (fDataSize-fFirstUsed)*sizeof(TNdxType) );
	    memcpy( fUsedStart+fFirstUsed-newSize, fUsedStart+fFirstUsed, (fDataSize-fFirstUsed)*sizeof(unsigned char) );
	    fFirstUsed -= newSize;
	    }
	else
	    {
	    // The used slots are in one block in the middle of the buffer,
	    // not wrapped around.
	    // We move the block to the beginning of the buffer to be sure we do not
	    // cut of any valid data.
	    unsigned long i, n, newEnd = fNextFree-fFirstUsed; // The amount of entries, also the 
	                                                       // new end point, once the block is moved to the beginning of the buffer.
	    if ( fFirstUsed>0 )
		{
		// The used block is not yet at the beginning of the buffer.
		if ( newEnd > fFirstUsed )
		    {
		    // The new end points is greater than the old starting point
		    // => the old and new block locations overlap.
		    i = 0;
		    while ( newEnd > i )
			{
			if ( newEnd-i>=fFirstUsed )
			    n = fFirstUsed;
			else
			    n = newEnd-i;
			memcpy( fDataStart+i, fDataStart+i+fFirstUsed, n*sizeof(TElementType) );
			memcpy( fNdxStart+i, fNdxStart+i+fFirstUsed, n*sizeof(TNdxType) );
			memcpy( fUsedStart+i, fUsedStart+i+fFirstUsed, n*sizeof(unsigned char) );
			i += n;
			}
		    i = newEnd;
		    for ( n = 0; n < fNextFree-newEnd; n++ )
			{
			if ( fDefaultElementSet )
			    {
			    *(fDataStart+i) = fDefaultElement;
			    *(fNdxStart+i) = fDefaultNdx;
			    }
			*(fUsedStart+i) = false;
			i = (i+1) & fDataMask;
			}
		    }
		else
		    {
		    // The old and new block locations are completely separate.
		    memcpy( fDataStart, fDataStart+fFirstUsed, newEnd*sizeof(TElementType) );
		    memcpy( fNdxStart, fNdxStart+fFirstUsed, newEnd*sizeof(TNdxType) );
		    memcpy( fUsedStart, fUsedStart+fFirstUsed, newEnd*sizeof(unsigned char) );
		    i = fFirstUsed;
		    for ( n = 0; n < newEnd; n++ )
			{
			if ( fDefaultElementSet )
			    {
			    *(fDataStart+i) = fDefaultElement;
			    *(fNdxStart+i) = fDefaultNdx;
			    }
			*(fUsedStart+i) = false;
			i = (i+1) & fDataMask;
			}
		    }
		fFirstUsed = 0;
		fNextFree = newEnd;
		}
	    }
	fData.resize( newSize );
	fUsed.resize( newSize );
	fNdx.resize( newSize );
	fDataStart = fData.begin();
	fUsedStart = fUsed.begin();
	fNdxStart = fNdx.begin();
	fDataSize = newSize;
	fDataMask = fDataSize-1;
	}
    }




/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _MLUCNDXVECTOR_HPP_
