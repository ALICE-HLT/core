#ifndef _MLUCSIMPLESTACKMACHINE_HPP_
#define _MLUCSIMPLESTACKMACHINE_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include <MLUCTypes.h>
#include <vector>

class MLUCSimpleStackMachineAssembler;

#ifdef EXTENDEDMLUCSTACKMACHINE
#define MLUCTOPOFSTACK() (fStack[fStackSize-1])
#define MLUCCHECKSTACKSIZE(MINSIZE) (fStackSize>=MINSIZE)
#define MLUCSTACKEMPTY() (!fStackSize)
#endif


class MLUCSimpleStackMachine
    {
    public:


	class TError
	    {
	    public:
		enum TCode { kOk=0, kInvalidCodeInputData, kOutOfMemory, kEmptyStack, kNotENoughOperandsOnStack, kUnknownInstructionCode, kMissingOperand, kHalted, kLastErrorCode };
		TError():
		    fOk(true),
		    fCode(kOk),
		    fDescription(NULL)
			{ }
		TError( TCode code, const char* descr ):
		    fOk(false),
		    fCode(code),
		    fDescription(descr)
			{ }
		bool Ok() const { return fOk; }
		TCode Code() const { return fCode; }
		const char* Description() const { return fDescription; }
	    protected:
		bool fOk;
		TCode fCode;
		const char* fDescription;
	    };



	enum TInstructions { kNOP=0, kHALT,
			     kLNOT=0x11, kLAND, kLOR, kLXOR, 
			     kBNOT=0x18, kBAND, kBOR, kBXOR, kBSHIFTLEFT, kBSHIFTRIGHT,
			     kADD=0x20, kSUB, kMULT, kDIV, kMOD, kINVERSE,
			     kEQUAL=0x30, kLESS, kGREATER,kISNULL, kNEGATIVE, kPOSITIVE,
			     kJUMP=0x38, kJUMPNULL, kJUMPNOTNULL,
			     kDROP=0x40, kCLEAR, kSWAP, kSWAPNR, kMOVETOTOPNR, 
			     kPUSHCONST=0x50, kDUP };

	MLUCSimpleStackMachine();
	virtual ~MLUCSimpleStackMachine();

	TError SetCode( unsigned long wordCount, uint64 const* words );
	TError SetCode( MLUCSimpleStackMachineAssembler const& assembler );

	TError Run();
	TError Step();
	TError Reset();

	bool IsHalted() const 
		{
		return fHalted;
		}
	bool IsStopped() const
		{
		return fHalted || fIP>=fCodeWordCount;
		}

	unsigned long GetCodeWordCount() const
		{
		return fCodeWordCount;
		}
	unsigned long GetIP() const
		{
		return fIP;
		}


	TError GetResult( uint64& result ) const;
	void GetStack( std::vector<uint64>& stack ) const; // Return the contents of the stack with the most recent element on the stack at the end of the vector

    protected:

	TError Pop( uint64& val, bool noShrink=false );
	TError Peek( uint64& val );
	TError Push( uint64 val );
	TError ChangeTop( uint64 val );
	TError ClearStack();

	virtual TError ExecNextStatement();

	unsigned long fCodeWordCount;
	uint64 * fCodeWords;
	unsigned long fIP;

	bool fHalted;

	unsigned long fStackSize;
	unsigned long fStackAllocSize;
	uint64* fStack;


    private:
    };



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _MLUCSIMPLESTACKMACHINE_HPP_
