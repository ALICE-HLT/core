#ifndef _MLUCMULTIREADERMUTEX_HPP_
#define _MLUCMULTIREADERMUTEX_HPP_

/************************************************************************
 **
 **
 ** This file is property of and copyright by the Technical Computer
 ** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
 ** University, Heidelberg, Germany, 2001
 ** This file has been written by Timm Morten Steinbeck, 
 ** timm@kip.uni-heidelberg.de
 **
 **
 ** See the file license.txt for details regarding usage, modification,
 ** distribution and warranty.
 ** Important: This file is provided without any warranty, including
 ** fitness for any particular purpose.
 **
 **
 ** Newer versions of this file's package will be made available from 
 ** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
 ** or the corresponding page of the Heidelberg Alice Level 3 group.
 **
 *************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#if 1
#define MULTIREADERMUTEX_PLAIN_MUTEX
#endif


#ifndef MULTIREADERMUTEX_PLAIN_MUTEX
#if 0
#define MULTIREADERMUTEX_DEBUG
#endif
#if 0
#define MULTIREADERMUTEX_DUMPSTATE
#endif


#include "MLUCTypes.h"
#include "MLUCVector.hpp"
#if defined(MULTIREADERMUTEX_DEBUG) || defined(MULTIREADERMUTEX_DUMPSTATE)
#include "MLUCString.hpp"
#endif
#include <pthread.h>
#include <cerrno>
#ifdef MULTIREADERMUTEX_DEBUG
#include <iostream>
#include <vector>
#endif
#ifdef MULTIREADERMUTEX_DUMPSTATE
#include <sys/types.h>
#include <unistd.h>
#include <fstream>
#endif

#ifdef MULTIREADERMUTEX_DUMPSTATE
#define DUMPSTATE(action) {DumpState( __FILE__, __LINE__, __FUNCTION__, (action) );}
#else
#define DUMPSTATE(action) {}
#endif

class MLUCMultiReaderMutex
    {
    public:
	class TWriteLocker;

	class TLocker
	    {
	    public:
		TLocker( MLUCMultiReaderMutex& mutex ):
		    fMutex(mutex),
		    fLockCount(0)
			{
			}
		~TLocker()
			{
			}
		virtual bool VLock() = 0;
		virtual bool VTryLock( bool& success ) = 0;
		virtual bool VUnlock() = 0;
	    protected:
		MLUCMultiReaderMutex& fMutex;
		unsigned fLockCount;
	    };

	class TReadLocker: public TLocker
	    {
	    public:
		TReadLocker( MLUCMultiReaderMutex& mutex, bool lock=true ):
		    TLocker( mutex )
			{
			if ( lock )
			    {
			    fMutex.ReadLock();
			    ++fLockCount;
			    }
			}
		~TReadLocker()
			{
			while ( fLockCount )
			    {
			    fMutex.ReadUnlock();
			    --fLockCount;
			    }
			}
		virtual bool VLock()
			{
			return Lock();
			}
		virtual bool VTryLock( bool& success )
			{
			return TryLock( success );
			}
		virtual bool VUnlock()
			{
			return Unlock();
			}
		bool Lock()
			{
			bool ret = fMutex.ReadLock();
			++fLockCount;
			return ret;
			}
		bool TryLock( bool& success )
			{
			bool ret = fMutex.ReadTryLock( success );
			if ( success )
			    ++fLockCount;
			return ret;
			}
		bool Unlock()
			{
			if ( fLockCount==0 )
			    return false;
			--fLockCount;
			return fMutex.ReadUnlock();
			}
	    protected:

		    friend class TWriteLocker;

	    };


	class TWriteLocker: public TLocker
	    {
	    public:
		TWriteLocker( MLUCMultiReaderMutex& mutex, bool lock=true ):
		    TLocker( mutex ),
		    fElevated(false)
			{
			if ( lock )
			    {
			    fMutex.WriteLock();
			    ++fLockCount;
			    }
			}
		TWriteLocker( MLUCMultiReaderMutex::TReadLocker& readLock ):
		    TLocker(readLock.fMutex),
		    fElevated(true)
			{
			if ( readLock.fLockCount>0 )
			    {
			    fMutex.ElevateWriteLock();
			    ++fLockCount;
			    }
			}
		~TWriteLocker()
			{
			while ( fLockCount )
			    {
			    if ( fElevated )
				fMutex.DeElevateWriteLock();
			    else
				fMutex.WriteUnlock();
			    --fLockCount;
			    }
			}
		virtual bool VLock()
			{
			return Lock();
			}
		virtual bool VTryLock( bool& success )
			{
			return TryLock( success );
			}
		virtual bool VUnlock()
			{
			return Unlock();
			}
		bool Lock()
			{
			bool ret;
			if ( fElevated )
			    ret = fMutex.ElevateWriteLock();
			else
			    ret = fMutex.WriteLock();
			++fLockCount;
			return ret;
			}
		bool TryLock( bool& success )
			{
			bool ret = fMutex.WriteTryLock( success );
			if ( success )
			    ++fLockCount;
			return ret;
			}
		bool Unlock()
			{
			if ( fLockCount==0 )
			    return false;
			--fLockCount;
			if ( fElevated )
			    return fMutex.DeElevateWriteLock();
			else
			    return fMutex.WriteUnlock();
			}
	    protected:
		bool fElevated;
	    };

	MLUCMultiReaderMutex():
	    fReaderCount(0),
	    fWaiters(2)
		{
		pthread_mutex_init( &fMutex, NULL );
		pthread_cond_init( &fSignal, NULL );
		}
	~MLUCMultiReaderMutex()
		{
		pthread_cond_destroy( &fSignal );
		pthread_mutex_destroy( &fMutex );
		}

#ifdef MULTIREADERMUTEX_DEBUG
	void PrintDebugState()
		{
		std::vector<TOwnerData>::iterator iter, end;
		iter = fOwnerData.begin();
		end = fOwnerData.end();
		if ( iter==end)
		    {
		    std::cout << "No owners" << std::endl;
		    }
		while ( iter != end )
		    {
		    std::cout << "Owner " << iter->fThread << " - " << (iter->fWriter ? "writer" : "reader") << " - count: " << iter->fCount << std::endl;
		    ++iter;
		    }
		}
#endif

    protected:

	bool ReadLock()
		{
		DUMPSTATE( "ReadLock WaitMutexEnter" );
		int ret = pthread_mutex_lock( &fMutex );
		DUMPSTATE( "ReadLock WaitMutexLeave" );
		if ( ret )
		    return false;
		if ( fWaiters.GetCnt()>0 )
		    {
		    pthread_t self = pthread_self();
		    bool success = false;
		    fWaiters.Add( self );
		    DUMPSTATE( "ReadLock WaitLoopEnter" );
		    do
			{
			DUMPSTATE( "ReadLock CondWaitEnter" );
			pthread_cond_wait( &fSignal, &fMutex );
			DUMPSTATE( "ReadLock CondWaitLeave" );
			if ( fWaiters.GetFirst()==self )
			    {
			    DUMPSTATE( "ReadLock Success" );
			    fWaiters.RemoveFirst();
			    pthread_cond_broadcast( &fSignal );
			    DUMPSTATE( "ReadLock Broadcast" );
			    success = true;
			    }
			}
		    while ( !success );
		    DUMPSTATE( "ReadLock WaitLoopLeave" );
		    }
		else
		    DUMPSTATE( "ReadLock Success" );
		    
		fReaderCount++;
#ifdef MULTIREADERMUTEX_DEBUG
		AddOwner(false);
#endif
		DUMPSTATE( "ReadLock Done" );
		pthread_mutex_unlock( &fMutex );
		return true;
		}
	bool ReadTryLock( bool& success )
		{
		int ret = pthread_mutex_trylock( &fMutex );
		success = false;
		if ( ret && ret!=EBUSY )
		    return false;
		if ( ret==EBUSY )
		    return true;
		if ( fWaiters.GetCnt()>0 )
		    {
		    pthread_mutex_unlock( &fMutex );
		    return true;
		    }
		success = true;
		fReaderCount++;
#ifdef MULTIREADERMUTEX_DEBUG
		AddOwner(false);
#endif
		pthread_mutex_unlock( &fMutex );
		return true;
		}
	bool ReadUnlock()
		{
		DUMPSTATE( "ReadUnlock WaitMutexEnter" );
		int ret = pthread_mutex_lock( &fMutex );
		DUMPSTATE( "ReadUnlock WaitMutexLeave" );
		if ( ret )
		    return false;
		fReaderCount--;
		DUMPSTATE( "ReadUnlock Broadcast" );
		pthread_cond_broadcast( &fSignal );
#ifdef MULTIREADERMUTEX_DEBUG
		DelOwner();
#endif
		DUMPSTATE( "ReadUnlock Done" );
		pthread_mutex_unlock( &fMutex );
		return true;
		}

	bool WriteLock()
		{
		DUMPSTATE( "WriteLock WaitMutexEnter" );
		int ret = pthread_mutex_lock( &fMutex );
		DUMPSTATE( "WriteLock WaitMutexLeave" );
		if ( ret )
		    return false;
		if ( fReaderCount>0 || fWaiters.GetCnt()>0 )
		    {
		    pthread_t self = pthread_self();
		    bool success = false;
		    fWaiters.Add( self );
		    DUMPSTATE( "WriteLock WaitLoopEnter" );
		    do
			{
			DUMPSTATE( "WriteLock CondWaitEnter" );
			pthread_cond_wait( &fSignal, &fMutex );
			DUMPSTATE( "WriteLock CondWaitLeave" );
			if ( fWaiters.GetFirst()==self && fReaderCount==0 )
			    {
			    DUMPSTATE( "WriteLock Success" );
			    fWaiters.RemoveFirst();
			    success = true;
			    }
			}
		    while ( !success );
		    DUMPSTATE( "WriteLock WaitLoopLeave" );
		    }
		else
		    DUMPSTATE( "WriteLock Success" );
#ifdef MULTIREADERMUTEX_DEBUG
		AddOwner(true);
#endif
		DUMPSTATE( "WriteLock Done" );
		return true;
		}
	bool ElevateWriteLock()
		{
		DUMPSTATE( "ElevateWriteLock WaitMutexEnter" );
		int ret = pthread_mutex_lock( &fMutex );
		DUMPSTATE( "ElevateWriteLock WaitMutexLeave" );
		if ( ret )
		    return false;
		--fReaderCount;
		DUMPSTATE( "ElevateWriteLock Broadcast" );
		pthread_cond_broadcast( &fSignal );
		if ( fReaderCount>0 || fWaiters.GetCnt()>0 )
		    {
		    pthread_t self = pthread_self();
		    bool success = false;
		    fWaiters.Add( self );
		    DUMPSTATE( "ElevateWriteLock WaitLoopEnter" );
		    do
			{
			DUMPSTATE( "ElevateWriteLock CondWaitEnter" );
			pthread_cond_wait( &fSignal, &fMutex );
			DUMPSTATE( "ElevateWriteLock CondWaitLeave" );
			if ( fWaiters.GetFirst()==self && fReaderCount==0 )
			    {
			    DUMPSTATE( "ElevateWriteLock Success" );
			    fWaiters.RemoveFirst();
			    success = true;
			    }
			}
		    while ( !success );
		    DUMPSTATE( "ElevateWriteLock WaitLoopLeave" );
		    }
		else
		    DUMPSTATE( "ElevateWriteLock Success" );
#ifdef MULTIREADERMUTEX_DEBUG
		AddOwner(true);
#endif
		DUMPSTATE( "ElevateWriteLock Done" );
		return true;
		}
	bool DeElevateWriteLock()
		{
		++fReaderCount;
		DUMPSTATE( "DeElevateWriteLock Broadcast" );
		pthread_cond_broadcast( &fSignal );
#ifdef MULTIREADERMUTEX_DEBUG
		DelOwner();
#endif
		DUMPSTATE( "DeElevateWriteLock Done" );
		return !pthread_mutex_unlock( &fMutex );
		}
	bool WriteTryLock( bool& success )
		{
		int ret = pthread_mutex_trylock( &fMutex );
		success = false;
		if ( ret && ret!=EBUSY )
		    return false;
		if ( ret==EBUSY )
		    return true;
		if ( fReaderCount>0 || fWaiters.GetCnt()>0 )
		    {
		    pthread_mutex_unlock( &fMutex );
		    return true;
		    }
		success = true;
#ifdef MULTIREADERMUTEX_DEBUG
		AddOwner(true);
#endif
		return true;
		}
	bool WriteUnlock()
		{
		DUMPSTATE( "WriteUnlock Broadcast" );
		pthread_cond_broadcast( &fSignal );
#ifdef MULTIREADERMUTEX_DEBUG
		DelOwner();
#endif
		DUMPSTATE( "WriteUnlock Done" );
		return !pthread_mutex_unlock( &fMutex );
		}


#ifdef MULTIREADERMUTEX_DUMPSTATE
	void DumpState( const char* file, unsigned line, const char* method, const char* action )
		{
		MLUCString name = "MLUCMultiReaderMutex-debug-";
		char tmp[256];
		snprintf( tmp, 256, "%d", (int)getpid() );
		name += tmp;
		name += ".out";
		std::ofstream ofile( name.c_str(), std::ios_base::app );
		struct timeval now;
		gettimeofday( &now, NULL );
		MLUCString timestamp = MLUCString::TimeStamp2String( now );
		
		ofile << timestamp.c_str() << " " << action << " " 
		      << file << ":" << line << ":" << method 
		      << " self: " << (unsigned)pthread_self()
		      << " ReaderCount: " << fReaderCount
		      << " WaiterCnt: " << fWaiters.GetCnt()
		      << " FirstWaiter: " << (unsigned)fWaiters.GetFirst()
		      << std::endl;
		
		}
#endif

	pthread_mutex_t fMutex;
	pthread_cond_t fSignal;

	unsigned long fReaderCount;

	MLUCVector<pthread_t> fWaiters;

#ifdef MULTIREADERMUTEX_DEBUG
	struct TOwnerData
	    {
		pthread_t fThread;
		bool fWriter;
		unsigned fCount;
		TOwnerData( bool writer ):
		    fThread(pthread_self()),
		    fWriter(writer),
		    fCount(1)
			{
			}
			
	    };
	std::vector<TOwnerData> fOwnerData;

	void AddOwner( bool writer )
		{
		std::vector<TOwnerData>::iterator iter, end;
		iter = fOwnerData.begin();
		end = fOwnerData.end();
		pthread_t self = pthread_self();
		while ( iter != end )
		    {
		    if ( iter->fThread==self )
			{
			++(iter->fCount);
			break;
			}
		    ++iter;
		    }
		if ( iter==end )
		    {
		    fOwnerData.push_back( TOwnerData( writer ) );
		    }
		}
	void DelOwner()
		{
		std::vector<TOwnerData>::iterator iter, end;
		iter = fOwnerData.begin();
		end = fOwnerData.end();
		pthread_t self = pthread_self();
		while ( iter != end )
		    {
		    if ( iter->fThread==self )
			{
			--(iter->fCount);
			if ( !iter->fCount )
			    fOwnerData.erase( iter );
			break;
			}
		    ++iter;
		    }
		}
	
#endif

	
    };


#else // MULTIREADERMUTEX_PLAIN_MUTEX

#include "MLUCMutex.hpp"


class MLUCMultiReaderMutex: public MLUCMutex
    {
    public:
	class TWriteLocker;

	class TReadLocker: public MLUCMutex::TLocker
	    {
	    public:
		TReadLocker( MLUCMultiReaderMutex& mutex, bool lock=true ):
		    MLUCMutex::TLocker( mutex, lock )
			{
			}
		~TReadLocker()
			{
			}
		virtual bool VLock()
			{
			return Lock();
			}
		virtual bool VTryLock( bool& success )
			{
			return TryLock( success );
			}
		virtual bool VUnlock()
			{
			return Unlock();
			}

		    friend class TWriteLocker;
	    };


	class TWriteLocker: public MLUCMutex::TLocker
	    {
	    public:
		TWriteLocker( MLUCMultiReaderMutex& mutex, bool lock=true ):
		    MLUCMutex::TLocker( mutex, lock ),
		    fElevateReadLock(NULL)
			{
			}
		TWriteLocker( MLUCMultiReaderMutex::TReadLocker& readLock ):
		    TLocker(*readLock.fMutex,false),
		    fElevateReadLock( &readLock )
			{
			}
		~TWriteLocker()
			{
			}
		virtual bool VLock()
			{
			return Lock();
			}
		virtual bool VTryLock( bool& success )
			{
			return TryLock( success );
			}
		virtual bool VUnlock()
			{
			return Unlock();
			}
		bool Lock()
			{
			if ( fElevateReadLock )
			    return fElevateReadLock->Lock();
			else
			    return MLUCMutex::TLocker::Lock();
			}
		bool TryLock( bool& success )
			{
			if ( fElevateReadLock )
			    return fElevateReadLock->TryLock(success);
			else
			    return MLUCMutex::TLocker::TryLock(success);
			}
		bool Unlock()
			{
			if ( fElevateReadLock )
			    return fElevateReadLock->Unlock();
			else
			    return MLUCMutex::TLocker::Unlock();
			}
	    protected:
		MLUCMultiReaderMutex::TReadLocker* fElevateReadLock;
	    };
	
    };




#endif // MULTIREADERMUTEX_PLAIN_MUTEX







/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _MLUCMULTIREADERMUTEX_HPP_
