#ifndef _MLUCTRACEBACKBUFFER_HPP_
#define _MLUCTRACEBACKBUFFER_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include "MLUCTypes.h"
#include "MLUCLog.hpp"
#include "MLUCString.hpp"
#include "MLUCVector.hpp"

#define ADDENTRY( var ) AddEntry( __FILE__, __LINE__, #var, var )
#define ADDARRAYENTRY( ptr, len ) AddArrayEntry( __FILE__, __LINE__, #ptr, (uint8*)ptr, (unsigned long)len )
#define ADDFIXEDARRAYENTRY( ptr, len ) AddFixedArrayEntry( __FILE__, __LINE__, #ptr, (uint8*)ptr, (unsigned long)len )

class MLUCTracebackBuffer
    {
    public:

	MLUCTracebackBuffer();
	~MLUCTracebackBuffer();

	void AddEntry( const char* file, int lineNr, const char* identifier, const char* value );
	void AddEntry( const char* file, int lineNr, const char* identifier, signed char value );
	void AddEntry( const char* file, int lineNr, const char* identifier, unsigned char value );
	void AddEntry( const char* file, int lineNr, const char* identifier, signed short value );
	void AddEntry( const char* file, int lineNr, const char* identifier, unsigned short value );
	void AddEntry( const char* file, int lineNr, const char* identifier, signed int value );
	void AddEntry( const char* file, int lineNr, const char* identifier, unsigned int value );
	void AddEntry( const char* file, int lineNr, const char* identifier, signed long value );
	void AddEntry( const char* file, int lineNr, const char* identifier, unsigned long value );
	void AddEntry( const char* file, int lineNr, const char* identifier, signed long long value );
	void AddEntry( const char* file, int lineNr, const char* identifier, unsigned long long value );
	void AddEntry( const char* file, int lineNr, const char* identifier, float value );
	void AddEntry( const char* file, int lineNr, const char* identifier, double value );
#if 0
	void AddEntry( const char* file, int lineNr, const char* identifier, long double value );
#endif
	void AddEntry( const char* file, int lineNr, const char* identifier, bool value );
	void AddEntry( const char* file, int lineNr, const char* identifier, const void* value );
	void AddArrayEntry( const char* file, int lineNr, const char* identifier, uint8* value, unsigned long len );
	void AddFixedArrayEntry( const char* file, int lineNr, const char* identifier, uint8* value, unsigned long len );

	void Clear();

	void DumpToLog( MLUCLog::TLogLevel loglevel, const char* keyword, const char* msgPrefix );

    protected:

	enum TType { kUndefined = 0, kString, kSChar, kUChar, kSShort, kUShort, kSInt, kUInt, 
	       kSLong, kULong, kSLongLong, kULongLong,
	       kFloat, kDouble, kLongDouble, kBool, kPtr, kArray, kFixedArray };

#define FIXEDARRAYLENGTH 8

	struct Entry
	    {

		Entry()
			{
			fValue.fString = NULL;
			fType = kUndefined;
			}
		~Entry()
			{
			fValue.fString = NULL;
			}
		TType fType;
		union
		    {
			char* fString;
			signed char fSChar;
			unsigned char fUChar;
			signed short fSShort;
			unsigned short fUShort;
			signed int fSInt;
			unsigned int fUInt;
			signed long fSLong;
			unsigned long fULong;
			signed long long fSLongLong;
			unsigned long long fULongLong;
			float fFloat;
			double fDouble;
#if 0
			long double fLongDouble;
#endif
			bool fBool;
			const void* fPtr;
			struct
			    {
				uint8* fPtr;
				unsigned long fLength;
			    } fArray;
			struct
			    {
				uint8 fData[FIXEDARRAYLENGTH];
			    } fFixedArray;
		    } fValue;
		const char* fFile;
		int fLine;
		const char* fIdentifier;
	    };

	struct LogData
	    {
		LogData( MLUCLog::TLogLevel loglevel, const char* keyword, const char* msgPrefix ):
		    fLoglevel( loglevel ),
		    fKeyword( keyword ),
		    fMsgPrefix( msgPrefix )
			{
			}

		MLUCLog::TLogLevel fLoglevel;
		const char* fKeyword;
		const char* fMsgPrefix;
	    };


	MLUCVector<Entry> fEntries;


	static void EntryLogIterationFunc( const Entry& el, void* arg );


    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _MLUCTRACEBACKBUFFER_HPP_
