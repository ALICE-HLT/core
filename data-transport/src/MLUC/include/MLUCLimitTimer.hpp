/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
#ifndef _MLUCTLIMITIMER_HPP_
#define _MLUCTLIMITIMER_HPP_

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/


//#define _XOPEN_SOURCE 500
#include "MLUCTypes.h"
#include "MLUCLog.hpp"
#include <sys/time.h>
#include <unistd.h>

#if 0
#define MLUCLIMITTIMER( timeLimit_us ) MLUCLimitTimer defaultMacroLimitTimer( timeLimit_us, __FILE__, __LINE__ )

#define MLUCLIMITTIMER_NAME( timerName, timeLimit_us ) MLUCLimitTimer timerName( timeLimit_us, __FILE__, __LINE__ )

#define MLUCLIMITTIMER_CREATE() MLUCLimitTimer defaultMacroLimitTimer
#define MLUCLIMITTIMER_START( timeLimit_us ) defaultMacroLimitTimer.Start( timeLimit_us, __FILE__, __LINE__ )
#define MLUCLIMITTIMER_STOP() defaultMacroLimitTimer.Stop()

#define MLUCLIMITTIMER_NAME_CREATE( timerName ) MLUCLimitTimer timerName
#define MLUCLIMITTIMER_NAME_START( timerName, timeLimit_us ) timerName.Start( timeLimit_us, __FILE__, __LINE__ )
#define MLUCLIMITTIMER_NAME_STOP( timerName ) timerName.Stop()

#else
#define MLUCLIMITTIMER( timeLimit_us )

#define MLUCLIMITTIMER_NAME( timerName, timeLimit_us )

#define MLUCLIMITTIMER_CREATE()
#define MLUCLIMITTIMER_START( timeLimit_us )
#define MLUCLIMITTIMER_STOP()

#define MLUCLIMITTIMER_NAME_CREATE( timerName )
#define MLUCLIMITTIMER_NAME_START( timerName, timeLimit_us )
#define MLUCLIMITTIMER_NAME_STOP( timerName )
#endif

#define MLUCLIMITTIMER_DEFAULT_TIMER_LIMIT_US 1000000

class MLUCLimitTimer
    {
    public:

	MLUCLimitTimer()
		{
		fRunning = false;
		}
	MLUCLimitTimer( unsigned long alarmLimit_us, const char* filename, unsigned lineNr )
		{
		Start( alarmLimit_us, filename, lineNr );
		}
	~MLUCLimitTimer()
		{
		Stop();
		}

	void Start( unsigned long alarmLimit_us, const char* filename, unsigned lineNr )
		{
		fAlarmLimit_us = alarmLimit_us;
		fLineNr = lineNr;
		fFilename = filename;
		fRunning = true;
		gettimeofday( &fStarted, NULL );
		}
	void Stop()
		{
		struct timeval stopped;
		gettimeofday( &stopped, NULL );
		if ( !fRunning )
		    return;
		unsigned long long delta_us = ((unsigned long long)(stopped.tv_sec-fStarted.tv_sec))*1000000ULL + ((unsigned long long)(stopped.tv_usec-fStarted.tv_usec));
		if ( delta_us>fAlarmLimit_us && delta_us<20*fAlarmLimit_us)
		    {
		    LOG( MLUCLog::kWarning, "MLUCLImitTimer::Stop", "Time limit exhausted" )
			<< fFilename << ":" << AliHLTLog::kDec << fLineNr << ": Time limit of " << MLUCLog::kDec << fAlarmLimit_us << " microsec. exhausted with "
			<< delta_us << " microsec." << ENDLOG;
		    }
		fRunning = false;
		}
    protected:

	bool fRunning;
	struct timeval fStarted;
	unsigned long fAlarmLimit_us;
	unsigned fLineNr;
	const char* fFilename;

    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _MLUCLIMITTIMER_HPP_
