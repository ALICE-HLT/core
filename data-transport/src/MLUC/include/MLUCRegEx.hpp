#ifndef _MLUCREGEX_HPP_
#define _MLUCREGEX_HPP_

/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/

/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#include <MLUCTypes.h>
#include "MLUCString.hpp"
#include <regex.h>
#include <vector>

using namespace std;

class MLUCRegEx
    {
    public:

	enum TPatternFlags { kPFNone = 0, kPFExtended = 1, kPFNoCase = 2, kPFNoNewLineWildcard = 4 };
	enum TSearchFlags { kSFNone = 0, kSFNoBOL = 1, kSFNoEOL = 2 };

	MLUCRegEx();
	~MLUCRegEx();

	bool SetPattern( const char* pattern, TPatternFlags flags = kPFNone );

	bool Search( const char* haystack, TSearchFlags flags = kSFNone );
	bool Search( const char* haystack, vector<MLUCString>& subStrs, TSearchFlags flags = kSFNone );

	const char* GetError() const
		{
		return fError.c_str();
		}

    protected:

	regex_t fRegEx;
	bool fRegExSet;

	MLUCString fError;

	

    };





/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/

#endif // _MLUCREGEX_HPP_
