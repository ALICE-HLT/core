#ifndef _MLUCVALUEMONITORCONTEXTSWITCHES_HPP_
#define _MLUCVALUEMONITORCONTEXTSWITCHES_HPP_

#include "MLUCValueMonitor.hpp"
#include "MLUCString.hpp"
#include "MLUCLog.hpp"

#include <cstdlib>
#include <errno.h>
#include <sstream>

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

class MLUCValueMonitorContextSwitches : public MLUCValueMonitor {
public:
 /**
  * @brief Constructor to enable output to file and stdout.
  * Using this constructor enables output to a file as well as to the
  * standard output. The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written
  * to the beginning of the file.
  * For output to stdout prefixes and suffixes are provided that will be output
  * respectively before and after the monitored value. Another pre-/suffix pair
  * is provided for the averaged value. For the averaged value the number of the
  * last values over which the average is to be taken can be determined as well.
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output.
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param output_prefix A string holding the textual prefix that is to be
  * output before the measured value.
  * @param output_suffix A string holding the textual suffix that is to be
  * output after the measured value.
  * @param avg_output_prefix A string holding the textual prefix that is to be
  * output before the average of the measured value.
  * @param avg_output_suffix A string holding the textual suffix that is to be
  * output after the average of the measured value.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorContextSwitches( const char* filename,
				const char* fileDescrHeader,
				const char* output_prefix,
				const char* output_suffix,
				const char* avg_output_prefix,
				const char* avg_output_suffix,
				unsigned int average_items) :
    MLUCValueMonitor( filename,
		      fileDescrHeader,
		      output_prefix,
		      output_suffix,
		      avg_output_prefix,
		      avg_output_suffix,
		      average_items )
  {
    Init();
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file.
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written
  * to the beginning of the file.
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well.
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output.
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorContextSwitches( const char* output_prefix,
				const char* output_suffix,
				const char* avg_output_prefix,
				const char* avg_output_suffix,
				unsigned int average_items):
    MLUCValueMonitor( output_prefix,
		      output_suffix,
		      avg_output_prefix,
		      avg_output_suffix,
		      average_items )
  {
    Init();
  };

 /**
  * @brief Constructor to enable only output to a file.
  * Using this constructor enables output to a file.
  * The filename is passed to the constructor as well as a
  * string containing a description of the measurement that will be written
  * to the beginning of the file.
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well.
  *
  * @param filename A string holding the name of the file to which the values
  * monitored are output.
  * @param fileDescrHeader A string holding a description of the monitoring
  * taking place. This string will be written to the start of the output file.
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorContextSwitches( const char* filename,
				const char* fileDescrHeader,
				unsigned int average_items):
    MLUCValueMonitor( filename, fileDescrHeader, average_items )
  {
    Init();
  };

 /**
  * @brief Constructor with no output.
  * For the averaged value the number of the last values over which the average
  * is to be taken can be determined as well.
  *
  * @param average_items The number of the most recent values over which the
  * average is to be calculated.
  */
  MLUCValueMonitorContextSwitches( unsigned int average_items):
    MLUCValueMonitor( average_items )
  {
    Init();
  };

  int GetValue( uint64& MeasuredValue );

protected:


private:
  /**
  * Flag for the first readout. In the constructor this flag is set to true, 
  * since one needs at least two values to measure something. After the first
  *  readout the flag is set to false.
  */
  bool fFirstReadOut;

  /**
   * The value of the the last readout is stored in this variable.
   */
  unsigned fOldContextSwitchVal;

  /**
  * Will be executed in each constructor. This is just for convenience
  */
  void Init();
};

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

#endif   // _MLUCVALUEMONITORCONTEXTSWITCHES_HPP_
