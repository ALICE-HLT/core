/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/



void ReadHistogramRoot( const char* filename, const char* histoName, const char* histoTitle, const char* xTitle, const char* yTitle, TH1D*& histo, ULong_t maxCount=0, Double_t factor=1.0 )
    {
    int ret;
    FILE* file;
    //TH1D* histo;
    histo = NULL;
    file = fopen( filename, "r" );
    if ( !file )
	{
	printf( "Error opening file %s for reading.\n", filename );
	return;
	}
    Double_t upper, lower, width;
    ULong_t over, under;
    ULong_t* bins;
    ret = fread( &upper, sizeof(upper), 1, file );
    if ( ret != 1 )
	{
	printf( "Error reading upper from file %s.\n", filename );
	fclose( file );
	return;
	}
    ret = fread( &lower, sizeof(lower), 1, file );
    if ( ret != 1 )
	{
	printf( "Error reading lower from file %s.\n", filename );
	fclose( file );
	return;
	}
    ret = fread( &width, sizeof(width), 1, file );
    if ( ret != 1 )
	{
	printf( "Error reading width from file %s.\n", filename );
	fclose( file );
	return;
	}
    ret = fread( &over, sizeof(over), 1, file );
    if ( ret != 1 )
	{
	printf( "Error reading over from file %s.\n", filename );
	fclose( file );
	return;
	}
    ret = fread( &under, sizeof(under), 1, file );
    if ( ret != 1 )
	{
	printf( "Error reading under from file %s.\n", filename );
	fclose( file );
	return;
	}
    ULong_t cnt, stepcnt;
    cnt  = (upper-lower)/width;
    stepcnt = cnt/10;
    bins = new ULong_t[ cnt ];
    if ( !bins )
	{
	printf( "Out of memory allocating space for %Lu bins.\n", cnt );
	fclose( file );
	return;
	}
    ret = fread( bins, sizeof(*bins), cnt, file );
    if ( ret != cnt )
	{
	printf( "Error reading %Lu bins from file %s.\n", cnt, filename );
	fclose( file );
	return;
	}
    fclose( file );
    histo = new TH1D( histoName, histoTitle, cnt, (Axis_t)lower, (Axis_t)upper );
    if ( !histo )
	{
	printf( "Error creating histogram.\n" );
	return NUL;
	}
    ULong_t i;

    ULong_t totalcount = under;
    printf( "lower: %f - upper: %f - width: %f\n", lower, upper, width );

    
    for ( i = 0; i < cnt; i++ )
	{
	histo->SetBinContent( (Int_t)i+1, (Stat_t)factor*bins[i] );
	totalcount += bins[i];
	if ( !(i % stepcnt) )
	    printf( "%u: -> %u (totalcount: %u)\n", i, bins[i], totalcount );
	if ( maxCount && maxCount<totalcount )
	    break;
	}

    histo->SetXTitle(xTitle);
    histo->SetYTitle(yTitle);

    printf( "Overflows: %u\n", over );
    printf( "Underflows: %u\n", under );
    return;
    }



/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/
