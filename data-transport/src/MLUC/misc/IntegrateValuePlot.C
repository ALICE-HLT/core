/************************************************************************
**
**
** This file is property of and copyright by the Technical Computer
** Science Group, Kirchhoff Institute for Physics, Ruprecht-Karls-
** University, Heidelberg, Germany, 2001
** This file has been written by Timm Morten Steinbeck, 
** timm@kip.uni-heidelberg.de
**
**
** See the file license.txt for details regarding usage, modification,
** distribution and warranty.
** Important: This file is provided without any warranty, including
** fitness for any particular purpose.
**
**
** Newer versions of this file's package will be made available from 
** http://web.kip.uni-heidelberg.de/Hardwinf/L3/ 
** or the corresponding page of the Heidelberg Alice Level 3 group.
**
*************************************************************************/
/*
***************************************************************************
**
** $Author$ - Initial Version by Timm Morten Steinbeck
**
** $Id$ 
**
***************************************************************************
*/


void IntegrateValuePlot( const char* filename, Double_t yScale, Double_t startThreshold, Double_t stopThreshold, Bool_t once, double& integral, ULong_t& samplecount )
    {
    int ret;
    Bool_t swap = false;
    FILE* file;
    file = fopen( filename, "r" );
    if ( !file )
	{
	printf( "Error opening file %s for reading.\n", filename );
	return;
	}
    UInt_t endian, timesize, fileDescrSize;
    ret = fread( &endian, sizeof(endian), 1, file );
    if ( ret != 1 )
	{
	printf( "Error reading endian from file %s.\n", filename );
	fclose( file );
	return;
	}
    if ( endian == 0x01020304 )
	swap = false;
    else
	swap = true;

    if ( swap )
	{
	printf( "Sorry, byte order swapping not yet implemented. Please find a proper machine.\n" );
	fclose( file );
	return;
	}

    ret = fread( &timesize, sizeof(timesize), 1, file );
    if ( ret != 1 )
	{
	printf( "Error reading timesize from file %s.\n", filename );
	fclose( file );
	return;
	}
//     if ( timesize != sizeof(struct timeval) )
// 	{
// 	printf( "Sorry, your struct timeval has the wrong size...\n" );
// 	fclose( file );
// 	return;
// 	}

    ret = fread( &fileDescrSize, sizeof(fileDescrSize), 1, file );
    if ( ret != 1 )
	{
	printf( "Error reading fileDescrsize from file %s.\n", filename );
	fclose( file );
	return;
	}
    
    UChar_t* fileDescr;
    fileDescr = new UChar_t[ fileDescrSize ];
    ret = fread( fileDescr, sizeof(UChar_t), fileDescrSize, file );
    if ( ret != fileDescrSize )
	{
	printf( "Error reading fileDescr from file %s.\n", filename );
	fclose( file );
	return;
	}
    
    samplecount = 0;
    //struct timeval t;
    UInt_t ts, tus;
    UChar_t value_a[8];
    Double_t td, vd; 
    integral=0;
    Bool_t process = false;
    while ( !feof( file ) )
	{
	ret = fread( &ts, sizeof(ts), 1, file );
	if ( ret != 1 )
	    {
	    printf( "Error reading sample seconds %lu from file %s.\n", samplecount, filename );
	    fclose( file );
	    break;
	    }
	ret = fread( &tus, sizeof(tus), 1, file );
	if ( ret != 1 )
	    {
	    printf( "Error reading sample useconds %lu from file %s.\n", samplecount, filename );
	    fclose( file );
	    break;
	    }
	ret = fread( value_a, sizeof(UChar_t), 8, file );
	if ( ret != 8 )
	    {
	    printf( "Error reading sample value %lu from file %s.\n", samplecount, filename );
	    fclose( file );
	    break;
	    }


	vd = 0;
	for ( Int_t i = 0; i < 8; i++ )
	    {
	    vd *=256;
	    vd += value_a[7-i];
	    //printf( "%d: vd %f - value %d\n", i, vd, (int)value_a[7-i] );
	    }
	vd *= yScale;

	if ( !process && vd >= startThreshold )
	    {
	    process = true;
	    printf( "Starting...\n" );
	    }
	else if ( process && vd < stopThreshold && once )
	    {
	    printf( "Aborting!\n" );
	    break;
	    }
	else if ( process && vd < stopThreshold && !once )
	    {
	    process = false;
	    printf( "Stopping...\n" );
	    }
	
	if ( process )
	    {
	    integral += vd;
	    samplecount++;
	    }
	// XXX
	}

    }
