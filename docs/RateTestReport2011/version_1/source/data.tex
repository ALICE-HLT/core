% Author: Artur Szostak
% Email:  artursz@iafrica.com

\chapter{Data}
\label{cha:data}

Tests to find \gls{HLT} and \gls{DAQ} bottlenecks were performed over the period 6th to 11th of April 2011.
Initially a baseline was established at a conservative trigger rate in technical runs.
This trigger rate was then increased by changing the values of the random trigger rate registers
inside the \gls{CTP}, RND1 and RND2\footnote{During technical runs the \gls{CTP} is configured to fire
random triggers based on the rates defined by the RND1 and RND2 registers.
Only during normal physics runs are these random triggers ignored.}.
By doing so a number of bottlenecks were identified in the \gls{HLT} and fixed.
For these tests it was most convenient to always set RND2 to zero and only change RND1 to
modify the effective trigger rate.
Once the internal \gls{HLT} bottlenecks were resolved two sets of tests were performed.

The first was a rate scan performed during runs 147837 and 147841.
The purpose being to identify the latency profile of the \gls{HLT} as a function of the trigger rate
in technical runs.
Since we do not have real physics in a technical run, but only noise and minimal event sizes,
the results only indicate a lower limit.
However, this is nevertheless useful information as it indicates the minimum overhead to be expected from \gls{HLT}.
In addition, the \gls{CPU} usage of the most heavily loaded processes were measured to cross check
if there is any correlation between \gls{CPU} usage and the latency.
To distinguish between problems in the framework or the analysis code,
the data transportation components (EventMergerNew processes) and the \gls{HLT} analysis components
(AliRootWrapperSubscriber processes) were considered separately for the \gls{CPU} load measurement.
No back pressure was seen from either the \gls{DAQ} or \gls{HLT} for this particular test set.
Neither the \gls{CPU} usage nor the network was saturated at any point in time during these tests.
The Infiniband network was under the highest load, but showed only $\approx$~50~MBytes/s transfers
from any machine for both the input or output\footnote{The maximum transfer speeds measured with the
current \gls{HLT} setup has been measured to be $\approx$~1~GBytes/s.}.
The detector with the highest dead time was the \gls{SPD} with $364$~\mus.
Thus, the theoretical maximum trigger rate is $2747.25 \pm 7.55$~Hz.
From the measurements, table~\ref{tbl:rate_scan_measurements} we see that we approach this
maximum within 4\%, which indicates the overhead and limitations in the \gls{CTP} configuration.
In particular, the \gls{CTP} was using bunch crossing masking which limits the overall trigger
rate even when the RND1 register is set to give the maximum possible random trigger rate.
To alleviate this effect we always chose a configuration for \gls{CTP} which contains a large number
of bunch crossings, i.e. a configuration for a large number of bunches in the \gls{LHC} per beam.

\begin{table}
\centering
\caption{Measurements taken over a range of trigger rates to scan the latency inside the \protect\glstext{HLT}.
The \protect\glstext{CPU} usage of the processes EventMergerNew (EM) and AliRootWrapperSubscriber (AWS)
with the heaviest load are also indicated, together with the node name on which this load was seen.}
\label{tbl:rate_scan_measurements}
\begin{tabular}{cccccccc}
\toprule
\protect\glstext{CTP} RND1 & \protect\glstext{HLT} event & Events in & Latency & \multicolumn{4}{c}{\protect\glstext{CPU} load [\%] (uncertainty $\approx$2\%)} \\
       rate [kHz]          &          rate [Hz]          &   chain   &  [ms]   & EM & node & AWS & node \\
\midrule
4$\times10^4$ & 2643.5 $\pm$  1.5 & 360   $\pm$ 120  & 136  $\pm$ 45   & 132 & cn023 & 103 & cn018 \\
5$\times10^3$ & 2514   $\pm$  6   & 215   $\pm$  65  & 86   $\pm$ 26   & 130 & cn023 &  97 & cn018 \\
400           & 2340   $\pm$ 10   & 100   $\pm$  20  & 42.7 $\pm$  8.5 & 125 & cn023 &  91 & cn018 \\
300           & 2240   $\pm$ 10   &  70   $\pm$  10  & 31.3 $\pm$  4.5 & 120 & cn023 &  87 & cn018 \\
200           & 2067.5 $\pm$  7.5 &  54.5 $\pm$  3.5 & 26.4 $\pm$  1.7 & 112 & cn023 &  79 & cn018 \\
150           & 1915   $\pm$ 15   &  50.5 $\pm$  3.5 & 26.4 $\pm$  1.8 & 106 & cn023 &  71 & feptriggerdet \\
100           & 1665   $\pm$ 15   &  42.5 $\pm$  2.5 & 25.5 $\pm$  1.5 &  93 & cn023 &  68 & feptriggerdet \\
 70           & 1445   $\pm$ 15   &  36   $\pm$  2   & 24.9 $\pm$  1.4 &  81 & cn023 &  64 & feptriggerdet \\
 50           & 1205   $\pm$ 25   &  31.5 $\pm$  2.5 & 26.1 $\pm$  2.1 &  68 & cn023 &  53 & feptriggerdet \\
 25           &  785   $\pm$ 15   &  21.5 $\pm$  2.5 & 27.4 $\pm$  3.2 &  44 & cn023 &  34 & feptriggerdet \\
 10           &  375   $\pm$ 15   &  12   $\pm$  3   & 32.0 $\pm$  8.1 &  21 & cn023 &  17 & feptriggerdet \\
  1           &   40   $\pm$  5   &   2   $\pm$  1   & 50   $\pm$ 26   & \multicolumn{4}{c}{not available} \\
\bottomrule
\end{tabular}
\end{table}


The second set of tests performed were exclusively with the \gls{TPC} to try and induce problems
seen with the \gls{DAQ} buffers on their \glspl{LDC}.
The \gls{HLT} internal latency was artificially increased by adding delay stages using DummyLoad
processes to the end of the processing chain.
In addition, the event size was increased in the \gls{TPC} by modifying the noise thresholds
such that the sizes correspond approximately to nominal p+p runs.
For every configuration the \gls{HLT} was run in modes A, B and B/test2.
Mode A gave a baseline reference.
Mode B/test2 was used to prove that there is no problem with the \gls{HLT} itself
by showing that no back pressure was asserted during such a run.
Comparing mode B to B/test2 runs we can see if the problem is on the \gls{DAQ} or \gls{HLT} side.
The results of these tests are summarised in table~\ref{tbl:hlt_mode_measurements}.


\begin{table}
\centering
\caption{Measurements taken for runs with different event sizes
and \protect\glstext{HLT} operational modes.
Event sizes and busy times are shown for the \protect\glstext{TPC}.
Trigger rate uncertainty is $\approx$~0.02~kHz.
Entries marked as N/A do not have that particular information available in
the given \protect\glstext{HLT} mode.}
\label{tbl:hlt_mode_measurements}
\begin{tabular}{ccccccccc}
\toprule
       &                       &  Event   &  Busy  & Trigger &         Events           & \protect\glstext{HLT} &         Pending          &   Back   \\
 run   & \protect\glstext{HLT} &   size   &  time  &   Rate  & in \protect\glstext{HLT} &        latency        &        decisions         & pressure \\
number &      mode             & [kBytes] & [\mus] &  [kHz]  &         chain            &          [ms]         & in \protect\glstext{DAQ} &  source  \\
\midrule
147755 & A       &  95 $\pm$ 1 & 419 $\pm$  1 & 2.35 &       N/A      &       N/A      &       N/A      & none \\
147758 & B/test2 &  95 $\pm$ 1 & 419 $\pm$  1 & 2.36 &  130 $\pm$  30 &   50 $\pm$  15 &       N/A      & none \\
147760 & B       &  95 $\pm$ 1 & 419 $\pm$  1 & 2.36 &  130 $\pm$  30 &   50 $\pm$  15 &  110 $\pm$  60 & none \\
148200 & A       & 589 $\pm$ 2 & 462 $\pm$  1 & 2.12 &       N/A      &       N/A      &       N/A      & none \\
148229 & B/test2 & 591 $\pm$ 2 & 462 $\pm$  1 & 2.09 &  750 $\pm$ 250 &  360 $\pm$ 120 &       N/A      & none \\
148234 & B       & 592 $\pm$ 2 & 462 $\pm$  1 & 2.08 &  750 $\pm$ 250 &  360 $\pm$ 120 &  750 $\pm$ 400 & none \\
148258 & B/test2 & 590 $\pm$ 5 & 464 $\pm$  1 & 2.05 & 1900 $\pm$ 700 &  905 $\pm$ 330 &       N/A      & none \\
148259 & B       & 590 $\pm$ 5 & 465 $\pm$  5 & 2.06 & 1850 $\pm$ 650 &  880 $\pm$ 310 & 1600 $\pm$ 400 & DAQ  \\
148263 & B/test2 & 594 $\pm$ 2 & 461 $\pm$  4 & 2.03 & 2200 $\pm$ 400 & 1050 $\pm$ 190 &       N/A      & none \\
148265 & B       & 594 $\pm$ 2 & 490 $\pm$ 29 & 1.99 & 2050 $\pm$ 150 &  980 $\pm$  70 & 2125 $\pm$ 375 & DAQ  \\
\bottomrule
\end{tabular}
\end{table}
