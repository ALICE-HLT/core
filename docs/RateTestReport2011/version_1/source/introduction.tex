% Author: Artur Szostak
% Email:  artursz@iafrica.com

\chapter{Introduction}
\label{cha:introduction}

The \gls{HLT} system was designed to reconstruct and filter events triggered by the
\gls{CTP} at a maximum rate of 200~Hz for central heavy-ion beams and 1~kHz for proton-proton
beams~\cite{CDR-HLT, TDR-TRG-HLT-DAQ-CTRL}.
In addition, there were no maximum latency requirements placed on the \gls{HLT} system
in the design.
Thus, the maximum latency is effectively only constrained by the size of the \gls{DAQ} buffers.

Since 2010 the \gls{LHC} has started ramping up luminosity delivered to the experiments significantly.
At the same time the \gls{ALICE} detector has had significant improvements made to the \gls{FEE}
firmware.
This combination has resulted in both higher event readout rate and larger event sizes due
to pileup in the \gls{ALICE} detector.
This can be seen by plotting the distribution of event recording rates for runs in the two
periods 2010 and 2011,
and the average event sizes per run in the same periods, figure~\ref{fig:mean_rates_and_sizes}.
As can be seen, the mean of the distributions has shifted to higher values in both cases.
The average recording rate went up to 795~Hz from 465~Hz and the average event size increased
to 1281~kBytes from 709~kBytes.
In addition, the peak event recording rates has reached almost 3~kHz in 2011.

The larger event sizes and higher readout rates has resulted in much higher loads on the \gls{HLT} system.
Several bottlenecks inside the \gls{HLT} and \gls{DAQ} systems caused a reduction
in the effective event recording rate, when \gls{HLT} is included into a run in mode B.
This was seen when comparing event recording rates of long runs with \gls{HLT} in mode A and
B\footnote{\protect\gls{HLT} in mode A is equivalent to \protect\gls{HLT} not participating
in a run, while mode B is equivalent to \protect\gls{HLT} receiving data and sending results to
\protect\gls{DAQ}, but \protect\gls{DAQ} not performing filtering based on \protect\gls{HLT}
trigger decisions.}
under similar beam conditions and detector configurations, seen in table~\ref{tbl:comparative_run_info}.
Up to a 20\% reduction in event rate is seen between runs 146517 (without \protect\gls{HLT})
and 146518 (with \protect\gls{HLT}).


\begin{table}
\centering
\caption{Event rates for comparative runs taken under same detector and beam conditions.}
\label{tbl:comparative_run_info}
\begin{tabular}{cccc}
\toprule
Run    & \protect\gls{HLT} & Event rate & Duration \\
number &       mode        &    [Hz]    & [minutes] \\
\midrule
146515 & B & 748. & 64. \\
146517 & A & 863. & 158. \\
146518 & B & 697. & 89. \\
\bottomrule
\end{tabular}
\end{table}


\begin{figure}
\centering
\includegraphics[width=0.49\textwidth]{graphs/meanrates}
\includegraphics[width=0.49\textwidth]{graphs/meansize}
\caption{Average event recording rate (left) and event size (right) distributions
per run for the periods 2010 (blue dotted line) and 2011 (solid black line).
The 2011 data only contains runs up to the 15th April, run 148625.
The mean and \protect\gls{RMS} values are also indicated for each distribution,
with the top values given for the 2010 runs and the bottom values for the 2011 runs.
}
\label{fig:mean_rates_and_sizes}
\end{figure}

A significant effort was expended to try identify and resolve the rate reduction problem,
involving a large number of tests.
The following chapters summarise these tests and the procedures used to distinguish between
problems within the \gls{HLT} and external issues.

\section{Readout Scheme}
\label{sec:readout_scheme}

The basic readout scheme involving \gls{HLT} is described to provide context for
technical discussions of the \gls{HLT} and \gls{DAQ} interactions in the following
sections.

The readout of the \gls{ALICE} detectors proceeds when the \gls{CTP} produces a \gls{L2a} trigger.
Once the \glspl{FEE} receive this signal the raw data is collected, packed and transmitted
over \glspl{DDL} from the detectors to the \gls{DAQ}'s \glspl{LDC}.
Inside the \glspl{LDC} the data is read out and stored in a buffer.
Then the same optical signal received over the \glspl{DDL} is split
and transmitted from the splitter cards to the \gls{HLT},
which receives a copy of the data on its \glspl{HRORC}.
The raw data is then processed inside the \gls{HLT} through a chain of distributed components
that perform reconstruction and compute a final trigger decision on the data.
The trigger decision and \gls{ESD} object are transmitted back to the \gls{DAQ} over
output \glspl{DDL}.
Once the \gls{HLT} decision is read out in the \gls{HLT}'s \glspl{LDC} inside the \gls{DAQ},
the decision is transmitted to all detector \glspl{LDC} where the data for a given event
has been already read out and is waiting in the \gls{DAQ} buffers.
Upon receiving the decision a \gls{LDC} can then either discard or transmit the sub event
data to the \gls{GDC} which is collecting all sub event fragments and building the complete
event for transmission to mass storage.

If at any point during this whole readout process either the \gls{HLT} or the \gls{DAQ}
cannot cope with the amount of data being received then the \glspl{DDL} assert an \gls{XOFF}.
This signal propagates back to the detectors \glspl{FEE} which in turn will stop transmitting
more data.
The \gls{CTP} then sees the detectors as busy, the busy time increases and no further triggers
will be generated until the \gls{XOFF} signal is canceled.
This is called back pressure.
In severe cases the whole \gls{ALICE} detector can get almost completely stuck with high busy times,
effectively bringing the readout rate to a halt,
without actually causing a technical failure of the run.

It is during the period when the raw data must wait inside the \gls{DAQ} buffers on the
\glspl{LDC} for the \gls{HLT} decision to be computed and distributed that the
interaction between the \gls{HLT} and \gls{DAQ} is most sensitive.
If the decision takes too long to arrive then the buffers can get full leading to
forced \glspl{XOFF} being generated from the \gls{DAQ} and back pressure.
Further details about this problem are covered in section~\ref{sec:latency_interaction}.


\section{Possible Rate Reduction Causes}
\label{sec:reduction_causes}

In this section the causes for experiencing a rate reduction during a physics run are
discussed to give a background for the tests performed.

Once obvious technical failures of \gls{HLT} are eliminated from the list of problems
there are a number of subtle problems that can arise which do not lead to a obvious
failure of the run, however the run can nevertheless be affected by a reduction
in the data recording rate.

\begin{description}
\item[Faulty data links]
The \gls{HLT} receives and transmits all data to and from \gls{DAQ} via \glspl{DDL}.
Although much care has been taken to detect or prevent glitches and technical failures
of these devices or the connecting fibers,
one cannot rule out that glitches in the transmission do not result in corruption of
the exchanged messages and therefore a slowdown of the rate.
This scenario could manifest itself with the corrupt \gls{HLT} trigger decisions being
discarded.
The \gls{DAQ} is forced to buffer the received data from the detector \glspl{FEE} over
all links.
If the rate of the lost \gls{HLT} decisions is high enough then the \gls{DAQ} buffers
would soon fill up and cause back pressure.

\item[Missing data paths within the configuration]
\gls{HLT} configurations must have all input \glspl{DDL} connected via a data path to
the output \glspl{DDL}.
In addition, the data path must go through the global trigger component.
The data path is established when, starting from the \gls{DDL} output writer component,
every component subscribes to a parent component that in turn subscribes to its parents
and so on until one reaches a \gls{HRORC} publisher component for the input \glspl{DDL}.
One can understand the problem in terms of acyclic graphs,
where every node is a component and an arc is a subscription.
The requirement is then that every source node (\gls{HRORC} publisher) is connected
via the global trigger to a sink node (\gls{DDL} output writer).
Also, every part of the graph must be connected, reachable,
contain no disjoint or orphan parts and contain no cycles.
If these requirements are not met, the situation can arise that data is received
over a number of input links, but for which no \gls{HLT} trigger decision is generated
nor sent to \gls{DAQ} because the data never arrives at the global trigger component to
be calculated nor the \gls{DAQ} to process.
In such a case the \gls{DAQ} buffers will become full over time since some decisions
never arrive, at which point back pressure is permanently asserted and the readout rate
drops to zero.

\item[Components too slow]
If the individual components are too slow to keep up with a particular \gls{CTP} trigger
rate then the \gls{HLT}'s maximum processing rate for the given configuration will be
quickly reached.
Once this happens the \gls{DAQ} buffers will start filling up since it will be receiving
data at a much higher rate than \gls{HLT} decisions.
Since it is forced to keep the data in the \gls{LDC} buffers until a decision actually
arrives from the \gls{HLT}, the buffers will soon get full, \glspl{XOFF} asserted and
the back pressure will reduce the event recording rate.

\item[High latency]
A high latency of the \gls{HLT} decision arriving at all the \glspl{LDC} can generate
artificial back pressure in the \gls{DAQ} because of finite buffer sizes.
If it takes a long period of time for the decision to arrive in the \glspl{LDC}
the \gls{DAQ} must keep the event in the buffers for that whole period of time.
If this period is much longer than the average period between new arriving events
the buffers can get full, because the old events are not releasing the buffers
fast enough to make space for new ones.

\item[Buffers too small]
If either the \gls{DAQ} or \gls{HLT} buffers are too small to handle the event rate
then the trigger rate will stall in a similar manner to high latency.
In the extreme case an event which is too large will block the whole system because
not enough buffer space can be allocated,
though this is typically avoided by configuring as large a buffer space as is possible
on the host machines.

\end{description}


\section{Latency Interaction}
\label{sec:latency_interaction}

There is a coupling between the \gls{HLT} and \gls{DAQ} because \gls{DAQ} must keep all
event fragments for which it has not yet received a decision in the buffers on the \glspl{LDC}.
Filtering is performed there when the \gls{HLT} decision is received and propagated to
each and every \gls{LDC}.
Specifically, there is an implicit limit in the trigger rate possible before \gls{DAQ}
is forced to apply back pressure by asserting the \gls{XOFF} signals in the \glspl{DDL}.
As soon as the back pressure is applied the \gls{CTP} is forced to reduce the trigger rate
because the detectors will be indicated as busy.
i.e. the detectors will be waiting for the \glspl{XOFF} to be disabled before more data
can be transfered to \gls{DAQ}.

The key parameters that affect the rate in this way are the \gls{HLT} latency and size
of the \gls{DAQ} buffers in the \glspl{LDC}.
The \gls{HLT} latency is the period of time between the \glspl{LDC} receiving the raw data
for a new event and the \gls{HLT} decision arriving in all the \glspl{LDC}.
The latency is dominated by the compute time required to reconstruct the raw data and
make a trigger decision in the \gls{HLT}.
Any network latencies inside the \gls{HLT} or over the \glspl{DDL} between \gls{HLT}
and \gls{DAQ} are negligible in comparison.
One can relate all these quantities by the following equation:
\begin{equation}
r \le \dfrac{\min\{s\}}{\max\{\tau\}}
\label{equ:latency_buffer_relation}
\end{equation}
where $r$ is the sustained \gls{CTP} trigger rate,
$\min\{s\}$ is the size of the smallest buffer on the \glspl{LDC}
and $\max\{\tau\}$ is the largest \gls{HLT} latency for delivering a decision to \gls{DAQ}.
This relation works as long as the assumption holds that there are no other effects,
such as internal \gls{HLT} bottlenecks or buffer management overhead in the \gls{DAQ}.
In such cases the maximum will be reduced further by these effects.
The purpose of the tests performed as described in this report is to confirm if and where
are the intrinsic bottlenecks inside the \gls{HLT} or \gls{DAQ}.
