#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass beamer
\begin_preamble
\usetheme{Warsaw}
% or ...

\setbeamercovered{transparent}
% or whatever (possibly just delete it)
\end_preamble
\use_default_options false
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman times
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_amsmath 2
\use_esint 0
\use_mhchem 1
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 2
\tocdepth 2
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Overview 
\end_layout

\begin_layout Author
Øystein S.
 Haaland
\end_layout

\begin_layout Institute
Institutt for Fysikk og Teknologi
\begin_inset Newline newline
\end_inset

University of Bergen
\end_layout

\begin_layout Date
HLT meeting, 24.03 2010
\end_layout

\begin_layout BeginFrame
Outline
\end_layout

\begin_layout Standard
\begin_inset CommandInset toc
LatexCommand tableofcontents

\end_inset


\end_layout

\begin_layout Section
The parts
\end_layout

\begin_layout BeginFrame
The parts
\end_layout

\begin_layout Itemize
rinetd: portforwarding daemon
\end_layout

\begin_layout Itemize
clusterapi: xml-rpc daemon
\end_layout

\begin_layout Itemize
avahi: service discovery daemon running on every node
\end_layout

\begin_layout Itemize
dcs net: network from hlt cluster to acr
\end_layout

\begin_layout Itemize
cern net: network accessible from any cern machine
\end_layout

\begin_layout Itemize
vpn: incomming remote connections
\end_layout

\begin_layout Section
The figure
\end_layout

\begin_layout BeginFrame
The figure
\end_layout

\begin_layout Standard
\begin_inset Graphics
	filename infra_online_display.svg
	lyxscale 50
	scale 17

\end_inset


\end_layout

\begin_layout Section
The steps
\end_layout

\begin_layout BeginFrame
The steps
\end_layout

\begin_layout Enumerate
Every dump tells avahi about which data blocks it can provide.
\end_layout

\begin_layout Enumerate
The local avahi daemon announces this information to all other avahi daemons
 on the local network
\end_layout

\begin_layout Enumerate
The dump information is therefore available in dcs, vobox and gw machines
\end_layout

\begin_layout Enumerate
Traffic from clients to tcpdumps is port-forwarded over portal and gw machines
\end_layout

\begin_layout Enumerate
A xml-rpc daemon makes the tcpdump information together with the port mapping
 available in a convenient xml format to clients such as the onlne display
 in the acr.
\end_layout

\begin_layout Enumerate
The client can now present in a gui what infomartion is available and where
 
\end_layout

\begin_layout Section
Comments
\end_layout

\begin_layout BeginFrame
Comments
\end_layout

\begin_layout Itemize
In addition to the big screen in the acr, there are connections from the
 CERN network and via vpn.
 Access from CERN is over vobox machines and vpn over gw machines.
 These have been left out of the figure to keep it simple.
\end_layout

\begin_layout Itemize
The daemon will give correct connection information depending on which network
 the request arrives from (i.e.
 internally on the HLT network, a direct connection is made instead of via
 port-mapping).
\end_layout

\begin_layout Itemize
The xml-rpc daemon knows about the port-mapping defined in the configuration
 of rinetd.
\end_layout

\begin_layout EndFrame

\end_layout

\end_body
\end_document
