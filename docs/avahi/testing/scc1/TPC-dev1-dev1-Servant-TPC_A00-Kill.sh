#!/bin/bash
echo Killing processes on servant node dev1/dev1 \(System hostname: `hostname -s`\)
#killall TaskManager
#sleep 1
#killall -9 TaskManager
TMPIDs=`ps x | grep "TaskManager /afsuser/oystein/chain/scc1/TPC-dev1-dev1-Servant-TPC_A00.xml"|grep -v grep |awk {'print $1'}`

for pid in ${TMPIDs} ; do
    kill ${pid}
done


for pid in ${TMPIDs} ; do
    kill -9 ${pid}
done

/afs/.alihlt.cern.ch/packages/data-transport/HLT-current-20090114.141011-HEAD_2009-01-15-patch-FDSET/src/SimpleChainConfig1/scripts/remote_starter.py -shell ssh -nodecommand dev1 " /afsuser/oystein/chain/scc1/TPC-dev1-dev1-Slave-Kill.sh"
