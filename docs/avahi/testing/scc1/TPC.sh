#!/bin/bash
if [[ `hostname -s` != "dev1" ]] ; then echo This script must be started on host 'dev1' ; exit 1 ; fi
if ! /afsuser/oystein/chain/scc1/TPC-dev1-dev1-Master-Check.sh ; then echo There have been errors ; echo Aborting TaskManager execution ; exit 1 ; fi
echo Starting Master TaskManager
/opt/HLT/data-transport/HLT-current-20090114.141011-HEAD_2009-03-09/bin/Linux-x86_64-debug/TaskManager /afsuser/oystein/chain/scc1/TPC-dev1-dev1-Master.xml -V 0x39 -nostdout 
