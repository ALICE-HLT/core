duration=60*15
pause=30

date | tee testfile

endTime=$(( `date +%s` + duration ))

while [ `date +%s` -le $endTime ]
do
    echo -n `date +%s` | tee -a testfile
    echo ": "`avahi-browse -t -a | wc -l ` | tee -a testfile
    sleep $pause
done

date | tee -a testfile
