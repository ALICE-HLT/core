# Script used to upload pdf files to the HLT sharpoint webspace

echo "Uploading files:"

cd /tmp/create_documents/output/

read -p "Type in CERN username: " username
read -s -p "Type in CERN password: " password

echo -e "\n"

for upFile in $( ls *.pdf )
do
	echo "Uploading $upFile"
	curl --ntlm -H "X-FORMS_BASED_AUTH_ACCEPTED:f" --user $username:$password --upload-file $upFile "https://espace.cern.ch/alice-hlt/upload/"
done

