\chapter{Default setup\label{ch:setup}}
This chapter gives a description of default  applications that 
running in routine operation of the HLT and how they should be set up.

\section{Logging into the machine in the control room}\index{login}
The hostname of the machine is \Machine{aldaqacr03} (see sticker on the keyboard).

\begin{itemize}
 \item Username: hlt
 \item Password: ........
\end{itemize}

This machine is used as a frontend to the HLT cluster, and all HLT operations are initiated from \Machine{portal-esc0}.

\section{Access to HLT-cluster}
To connect to the HLT-cluster with the proper options, a script is provided. Use this when logging on. 
From \Machine{aldaqacr03} open a \Command{konsole} window (on the taskbar in the middle lower screen) and enter the command in the example below.
% What should username be ? hlt, or name of operator?

Syntax: \verb| $ ConnectToHLT <HLT_username> <operator> | 

Here, \verb|<HLT_username>| is your HLT cluster username, and 
\verb|<operator>| is the system you wish to operate. Possibilities are:
HLT, TRD, ITS, TPC, MUON, EMCAL, PHOS.

Example: \verb| $ ConnectToHLT bob ITS |

This will bring you to \Machine{portal-ecs0} from where all operations regarding the operation of the HLT chain should be performed. 

Note that ``Bob'' then becomes the user ``its-operator'', taken from the lower case version of \verb|<operator>|.

After entering this command, you will be prompted twice for your cluster password, and may need to reply ``yes'' to a prompt.

Assuming you are authenticated, a \Command{konsole} window (Section~\ref{sec:konsole}) will appear on the screen.

This can be positioned in the lower middle screen.

\section{Non-GUI applications}
Certain applications of the HLT do not have a GUI. They are run from a konsole window placed by default in the lower middle screen (Figure~\ref{fig:hltscreens}). Some GUI applications are also started from the konsole. This section describes the non-GUI applications.

\subsection{konsole}\label{sec:konsole}\index{konsole}
\Command{konsole} is the standard \Command{term} from \gls{kde}, but is included here since it is used as a base for starting the HLT applications.
 
After logging into the HLT cluster and having become the desired user, a \Command{konsole} window with several tabs is automatically started. This permits each application to log messages to its own window. In addition, certain gui applications are also started from \Command{konsole} windows, in particular ECSGUI\index{ECSGUI!konsole}, TMGUI\index{TMGUI!konsole}, InfoBrowser\index{InfoBrowser!konsole} and StatusScan\index{status!StatusScan}.

\begin{figure}[htb]
 \centering
 \includegraphics[scale=0.5]{screenshots/konsole-tabs.png}
 \caption{Tabs on the konsole window.\label{fig:konsoletabs}}
\end{figure}


This \Command{konsole} window is placed by default in the lower middle screen, see Figure~\ref{fig:hltscreens}.

Some shortcuts for \Command{konsole}:
\begin{itemize}
 \item Ctrl + Shift + n: open new tab.
 \item Shift + Right-Key: change one tab to the right.
 \item Shift + Left-Key: change one tab to the left.
 \item Shift + Ctrl + Right-Key: Move a tab one step to the right.
 \item Shift + Ctrl + Left-Key: Move a tab one step to the left.
\end{itemize}

To open a new tab, double-click on an empty part of the konsole tab bar, or click on the icon on the left hand side of the tab bar. Clicking on the konsole button on the window manager bar at the bottom of the screen will start a new konsole.

\subsection{RunManager}\label{sec:runmanager}\index{RunManager}
RunManager is the top-level TaskManager. It controls the whole HLT hierarchy. This should also be running all the time. 
The RunManager can be started and stopped by from the {\bf Partition State} portlet of the ESMP GUI \ref{sec:esmp}.

\subsubsection{Strating from a terminal}
Alternatively, it can be started by the \Command{RunManager.sh} shell script. 

To start, go to the \textit{RunManager} tab in the \Command{konsole} and run:

\verb| $ RunManager.sh |

To stop use:

\verb| $ RunManager.sh stop |

(In case RunManager.sh stop does not work, it can be killed manually by issuing:
\verb| $ killall -9 TaskManager |
).

Use \Command{RunManager.sh --help} to view the possible options.

\subsection{ECS-proxy}\label{sec:ecsproxy}\index{ECS-proxy}
ECS-proxy should be running all the time. It is the interface between ECS and the HLT. It transmits the state transitions from ECS to the RunManager\index{RunManager}. For a diagram of these states see Appendix~\ref{ch:diagECSp}.

The ECSGUI\index{ECSGUI!ECS-proxy} talks to this proxy to control the HLT.

The proxy can be started and stopped by from the {\bf Partition State} portlet of the ESMP GUI \ref{sec:esmp}.
The partition name has to be provided as argument
Examples for partitions are:
\begin{description}
\item[global] : ALICE, PHYSICS, ALICE\_U.
\item[standalone] : TPC, TRD, EMCAL, FMD.
\end{description}
The partition names are defined by ECS and might change. The purpose of the ECS-proxy is to connect any HLT operator to any ECS partition.

For standalone running, the default is pp-STANDALONE. This is valid for HLT standalone operation, as well as for standalone detector operation in the sense of ECS.

For global partitions, the default is pp-PHYSICS.

\subsubsection{Strating from a terminal}
Alternatively, in one of the \Command{konsole} tabs, execute:

\verb| $ start_ECS.sh |

Use \Command{start\_ECS.sh --help} to view the possible options.

The ECS-proxy can be started with several options:
\begin{description}
\item[\texttt{-standalone}] : For standalone tests, the real ECS is emulated inside the HLT (and the system is controlled via ECSGUI).
\item[\texttt{-partition=<Partition>}] : where \texttt{Partition>} can be any ECS partition 
\begin{itemize}
 \item a global partition
 \item a standalone detector partition.
\end{itemize}  
\end{description}

\subsection{StatusScan}\index{status!StatusScan}
StatusScan provides status information on the running chain. It can be started in the StatusScan tab of konsole by entering:

\verb| $ StatusScan.sh |

There is no \Command{help} or other options, program just needs to be started.

The ESMP GUI \ref{sec:esmp} provides a portlet for the StatusScan.

\section{GUI software}
This section describes the standard GUI applications used for HLT control and monitoring. The sequence follows roughly the positions from left to right of the windows in Figure~\ref{fig:hltscreens}

\subsection{Firefox}\index{firefox!links}
Start up firefox as detailed in Section~\ref{sec:help}.

There are links in the bookmark button bar to start up those applications below that run from a web browser, see Figure~\ref{fig:firefox-bm}.

\begin{figure}[tb]
 \centering
 \includegraphics[scale=0.5]{screenshots/firefox-bookmark.png}
 \caption{Bookmark buttons in firefox allow starting applications.\label{fig:firefox-bm}}
\end{figure}

\subsection{Webgui}\label{sec:esmp}\index{Webgui}
Webgui is a shorthand for ESMP2 Gui. This runs in the full-screen \Command{firefox} window placed by default in the  upper left hand screen. The default view is the \textit{Partition View}, shown in Figure~\ref{fig:webgui}. 

This application can be started from firefox\index{firefox!Webgui}.

The Webgui contains a number of subwindows intensified by operator (detector component) and function. These are spawned from the \textit{Mainmenu}.
Each subwindow represents a program; there are two types: Portlets and Applications. Portlets have a simple function, such as listing a statistic or status\index{status!portlet}.
Applications are larger codes such as the \textit{Configuration Manager}.

Portlets pop up a small window within the \textit{Partition View}. This can be dragged into a new position and will snap into place in one of the three columns, perhaps displacing the portlet below. Applications open a new window which can be dragged to an available screen and maximized if required.

\begin{figure}
 \centering
 \includegraphics[width=0.8\textwidth]{screenshots/webgui.png}
 \caption{Screenshot of the Webgui. This screenshot shows the typical configuration. The \textit{Mainmenu} portlet is in the left-hand column; below it is the \textit{Active Configurations} portlet. The \textit{Stausscan} portlet is in the right-hand column and several \textit{Statistic} portlets in the middle column. \label{fig:webgui}}
\end{figure}

\subsubsection{Portlets}\label{sec:portlets}
The following Portlets are available. The information in them is best determined by exploration.

\begin{description}
 \item[GFX state] - List statistics of the current running chain.
 \item[State] - State and information on the current runnning chain.
 \item[Active Configurations] - Lists the  current active configurations of the HLT that will be loaded by the RunManager\index{RunManager} for the chosen beamtype and runtype (pp-PHYSICS/pp-STANDALONE/DDL\_TEST/\dots) It also permits the available configurations to be listed and activated. See Figure~\ref{fig:webgui}. 
 \item[Node-List] - List the nodes in the HLT cluster.
 \item[DDL-List] - List the \gls{ddl}s on the nodes and information about them.
 \item[Statusscan] - Status\index{status!portlet} of the currently running chain. Provides similar information to that found in the Status scan tab of the lower middle screen \Command{konsole}.
 \item[Statistic] - This portlet provides a time series plot of an item in the Stausscan display. You can cycle through and select an item by clicking in the arrows at the bottom of the window. Several instances can be open at the same time, as in Figure~\ref{fig:webgui}. 
\item[Components] - Lists the various HLT components in the current chain, their type and the node they are running on.
 \item[Loaded Modules] - Lists the versions, etc. of the software modules in use.
 \item[Users on Cluster] - Lists the current users logged in to the HLT cluster.
 \item[Operator SMS] - Send a short message to other users of Webgui\index{Webgui!SMS}.
\end{description}

\subsubsection{Applications}
The following Applications are available. Note that some of these may require some time to execute, so they should probably not be started by inexperienced shifters without advice.

\begin{description}
 \item[Configuration manager] - Provides an editor for configuration files and an interface to the verification and activation processes.
 \item[Configuration Graph] - Generates a graphical view of the currently running chain.
\ \item[Component Browser] - .
 \item[Eventflow analyser] - Provides a view of the current state of buffers in the running chain. Useful for debugging. Filters should be chosen to narrow the range of selected components in view, otherwise information flow might be large.
 \item[Infologger Browser] - Reimplementation of the stand-alone InfoBrowser\index{InfoBrowser}. The InfoBrowser is located by default in the top middle screen. For now the command line version (Section~\ref{sec:infobrowser}) should be used.
 \item[Logfile Browser] - Browser for log files.
 \item[Statistic Browser] - Plots graphs of chain statistics.
\end{description}

\subsubsection{Starting the ECS-proxy and the RunManager}\index{RunManager}\index{ECS-proxy}
The starting and stopping of these is for now done via the command line (Sections~\ref{sec:ecsproxy} and \ref{sec:runmanager}). At some stage this will be implemented from the \textit{State} portlet.

\subsection{ECSGUI}\index{ECSGUI}
The ECSGUI is used to control the HLT. By default it is placed in the lower left hand screen with TMGUI\index{TMGUI!position}. (It's the small windowon screen, shown in Figure~\ref{fig:ecs_gui2}).

To start it issue:

\verb| $ ECSGUI.sh <options> |

Use the same options as have been used when starting the ECS-proxy\index{ECS-proxy}.

To assume control of the HLT, click the \textit{Connectivity} button and choose HLT. The lock button is green when HLT is in control of itself, red when controlled by others and grey when none has control.

\begin{figure}
 \centering
 \includegraphics[viewport=0 0 161 148]{screenshots/ecs_gui.png}
 % ecs_gui.png: 161x148 pixel, 72dpi, 5.68x5.22 cm, bb=0 0 161 148
 \caption{Screenshot of the ECSGUI\label{fig:ecs_gui2}}
\end{figure}


The use of ECSGUI is described in Section~\ref{sec:ECSuse}.

\subsection{TMGUI}\index{TMGUI}
The functionality of TMGUI is supposed to be integrated into the WebgGui, but for now it's still a useful tool for situations that needs manual intervention. 
By default this is placed in the lower left hand screen. It is shown in Figure~\ref{fig:tm_gui}.

Start it with:

\verb| $ TMGUI.sh |

\begin{figure}
 \centering
 \includegraphics[width=0.8\textwidth]{screenshots/tmgui2.png}
% \includegraphics[width=373pt,bb=0 0 659 681]{screenshots/tm_gui.png}
 % tm_gui.png: 659x681 pixel, 72dpi, 23.25x24.02 cm, bb=0 0 659 681
 \caption{Screenshot of TMGUI\label{fig:tm_gui}}
\end{figure}

The TMGUI can be used to dynamically change the level of messages displayed in the InfoBrowser\index{InfoBrowser!debug level}. This can be useful in debugging, or to reduce the flow of messages.

In case of timeouts or the changes from state to state takes very long, then TMGUI can be used to manually manipulate processes or even abort them.

The HLT chain is arranged as a hierarchy. At the top is a master TaskManager. This in turn controls TaskManagers for the various subsystems. On each node there is a slave TaskManager controlling the HLT tasks on that node.   The right hand \textit{Children} window permits one to navigate through this hierarchy. Each line shows a TaskManager  or HLT component. Clicking on the line bring up in the left hand \textit{Commands} window  the list of commands that can be applied to the process. Many of these should only be used by experts, but the Verbosity commands can be executed by double clicking to set the debug levels of the messages produced (Table~\ref{tab:debug_levels}). Note that the \textit{SetVerbosity} commands apply to all \gls{component}s in the selected task, while \textit{SetSubSystemVerbosity} commands apply only to the analysis components and not the data transport components.

In case of serious problems in the chain, one can navigate to the top of the hierarchy (\textit{Connect to Parent} button at top left faded), select the MasterTM, and then select ABORT from the command list. 


A state diagram for TMGUI can be found in Appendix~\ref{ch:diagTM}.

\subsection{InfoBrowser}\label{sec:infobrowser}\index{InfoBrowser}
The InfoBrowser is used as a central place to show log messages and debug messages for the HLT software. By default it is placed in the upper middle screen.  It is shown in Figure~\ref{fig:infobrowser}.

From the cluster, the InfoBrowser can be started with the InfoBrowser alias:

\begin{itemize}
 \item \verb| $ InfoBrowser |
\end{itemize}

% insert screenshot of infoBrowser
\begin{figure}
 \centering
  \includegraphics[width=0.8\textwidth]{screenshots/infobrowser.png}
% \includegraphics[width=373pt,bb=0 0 1266 993]{screenshots/screen_upper_middle.png}
 % screen_upper_middle.png: 1266x993 pixel, 72dpi, 44.66x35.03 cm, bb=0 0 1266 993
 \caption{Screenshot of InfoBrowser\label{fig:infobrowser}}
\end{figure}

Filters on the messages can be entered in the fields at the bottom part of the browser, using sql syntax. for instance, you should restrict messages to those appropriate by entering the operator name (e.g. its-operator) in the \textit{Username} field.

\newpage


\subsection{SysMES}\index{SysMES}
SysMES is used for monitoring the cluster nodes. It can also be started from the default firefox\index{firefox!SysMES} page. For fullscreen, use F11. By default it is placed in the upper right screen.  It is shown in Figure~\ref{fig:sysmes}.\\
\newline
The main screen (see Figure ~\ref{fig:sysmes}) gives an overview over all the nodes of the cluster. Each node is represented by a (colored) box, paired boxes represent a node and its respective remote management device (eg CHARM-card, IPMI-BMC). By hovering the mouse pointer over a specific box (node icon) the name of the node can be seen.\\
A node or remote management device runs a number of monitors which check the functionality of certain node-specific subsystems, e.g. hardware, operating system and application properties. The results of those checks is represented by using certain colors for each box. A green box indicates a healthy node. All non-green boxes indicate a node suffering from a problem. It is the responsibility of the hlt-operator to make sure that those problems will not interfere with a running hlt chain. A description of how to interpret the results shown by SysMES is given below.

\newpage

\begin{figure}
 \centering
 \includegraphics[width=0.8\textwidth]{screenshots/sysmes2.png}
 %\includegraphics[width=373pt,bb=0 0 1261 667]{screenshots/sysmes.png}
 % sysmes.png: 1261x667 pixel, 72dpi, 44.49x23.53 cm, bb=0 0 1261 667
 \caption{Screenshot of SysMES. Each small square represents either a node in the cluster, or a CHARM card.\label{fig:sysmes}}
\end{figure}



In general, a node icon (box) can have one of the following color values: {green, red, orange, yellow, black}.
The interpretation of the box colors depends on the (functional) machine type the box represents:
\begin{itemize}
\item INFRA nodes/CHARM-cards are displayed in the top row of the {SysMES overview page}~\ref{fig:sysmes}.
\item FEP/CN nodes/CHARM-cards are displayed in the three middle rows of the SysMES overview page.
\item DEV nodes/CHARM-cards are displayed in the bottom row of the SysMES overview page.
\end{itemize}
Table ~\ref{table:sysmes_guideline} can be used as a guideline to deal with the certain color values.\\
\newline

\begin{table}[ht]
\centering
\begin{tabular} {| l | l | l |}
\hline
\textbf{Color} & \textbf{Meaning} & \textbf{Operator hint} \\
\hline
GREEN  & All OK                            & Do nothing \\
\hline
YELLOW & Minor Problem, HLT-Admin has been & Do nothing\\
       &informed via mail                  & \\
\hline
ORANGE & Major Problem, SysMES is taking   & Do nothing, but be aware \\
       & care of it                        & Box might turn red \\
\hline
RED    & Major Problem, SysMES was not     & See {Red Boxes}~\ref{subsubsec:red} \\
       & able to solve problem             & \\
\hline
BLACK  & Unknown State                     & See {Black Boxes}~\ref{subsubsec:black} \\
\hline
\end{tabular}
\caption{Box color interpretation for SysMES overview page}
\label{table:sysmes_guideline}
\end{table}


\subsubsection{Red Boxes}
\label{subsubsec:red}

\begin{enumerate}
\item If a box representing an INFRA node/CHARM-card turns red please call a HLT-Admin.
\item If a box representing a FEP/CN node/CHARM-card turns red the availabilty of the node can 
not be guaranteed anymore. That means that an ongoing run which this particular node is participating 
in might fail. Depending on the importance of the current run the operator either
\begin{itemize}
\item stops the run and takes this node out of the node-list or 
\item lets the run go on (taking the risk of node-failure) and makes sure that for 
the next run this node is taken out of the node-list. 
See \ref{sec:troubleshooting_rednode}, \ref{sec:helper_scripts}.
\end{itemize}
\item If a box representing a DEV node/CHARM-card turns red the specific node has imminent problems. 
SysMES has already notified an HLT-Admin via mail. Since development (DEV) nodes are not part of the 
production system the operator does not have to act.
\end{enumerate}

\subsubsection{Black Boxes} 
\label{subsubsec:black}
If a box is black, one or more of the following can be true:

\begin{itemize}
\item the node is down
\item the node is not reachable via network
\item the sysmes client is down or disfunctional
\end{itemize}

Unfortunately the current SysMES-GUI does not distinguish between a node shut down on purpose, being in repair or 
having just crashed. This will change with the new GUI release.
For now, have a look at the SysMES-GUI during shift hand-over and keep in mind which boxes are black. If new boxes become black 
during your shift, do the following:

\begin{enumerate}
\item If a box representing a CHARM-card turns black do nothing.
\item If a box for an INFRA node turns black, try to do an ssh login to that node on console. If this succeeds do nothing. Else
call a HLT-Admin.
\item If a box for a FEP/CN node turns black, try to do an ssh login to that node on console. If this succeeds do nothing. Else
try to take the node out of the node-list. (\ref{sec:troubleshooting_rednode}, \ref{sec:helper_scripts})
\item If a box for a DEV node turns black do nothing.
\end{enumerate}  

Due to the complexity and the distributed nature of the HLT-Cluster the guidelines given above can only be used as
a heuristic. A number of small errors can indicate a bigger problem, which so far cannot be detected automatically.
Therefor the operator is advised to use his common sense. If a larger number of boxes is not green and there is or soon
will be an important run, a phone call to a HLT-Admin might be a good idea.\\


\subsection{Logbook}\label{sec:logbook}\index{logbook}
The logbook (see Figure \ref{fig:logbook}) is used to document the operation of HLT. Starting firefox\index{firefox!logbook} locally on the \gls{acr} machine will bring up a start page where logbook can be selected from the bookmark button bar. Place the logbook in the lower right screen. F11 for fullscreen. Login with your CERN username and choose HLT.

After you have logged in for the first time, ask Jochen to enable your account for adding a log entry (otherwise you will have read-only access).

\begin{figure}
 \centering
 \includegraphics[width=0.8\textwidth]{screenshots/logbook2.png}
% \includegraphics[width=373pt,bb=0 0 1280 1024]{screenshots/screen_lower_right.png}
 % screen_lower_right.png: 1280x1024 pixel, 72dpi, 45.16x36.12 cm, bb=0 0 1280 1024
 \caption{Screenshot of the logbook\label{fig:logbook}}
\end{figure}

The logbook provides several functionalities: the log message view and the run statistics view.
\subsubsection{Run Statistics View}
Here one can get all information about the current and previous runs.

\subsection{AliEVE}\index{AliEVE}
AliEVE supports display of the clusters and tracks found by the HLT on the large screen on the left-hand side of the ACR wall.

{\center \large \textbf NEVER open the eventdisplay on the HLT terminal machine}

This will freeze the machine due to a bug in the graphics driver.

To start up the display:
\begin{enumerate}
\item Use te wireless keyboard to control the large screen
\item Right click on the empty screen and choose one of the default displays from the context menu (e.g. EVE ALICE), or
\item Open a konsole and enter:
    \verb| $ eve |
\end{enumerate}

In the left frame of the display (looks like a file browser) choose {\textit Homer Manager}. The buttons for event navigation will appear. Press e.g. {\textit NextEvent}. By clicking on the HLT logo the loop modus is activated.