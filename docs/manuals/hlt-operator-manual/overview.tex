\chapter{Overview}\label{ch:overview}

\section{High-Level Trigger (HLT)}
\label{hlt}


\subsection{Introduction}
\label{hlt:Introduction}

According to the simulation studies~\cite{intro:TP}, 
the amount of data produced in the TPC alone, in a single central nucleus--nucleus collision, corresponds to about 75 MB assuming $\dNdeta = 8000$ at mid-rapidity. The data rate for all detectors, resulting after a trigger selection, can easily reach 25 GB/s, while the physics content of a large number of events might be small and the DAQ archiving rate is about 1 GB/s. Therefore on-line processing is advised in order to select relevant events or sub-events and to compress the data without loosing their physics content. The overall physics requirements of the High-Level Trigger~\cite{hlt:anders-qm04} are categorised as follows:


\begin{description}
\item[Trigger] Accept or reject events based on detailed online analysis.

\item[Select] Select a physics region of interest within the event by performing only a partial readout.

\item[Compress] Reduce the event size without loss of physics information by applying compression algorithms on the accepted and selected data.
\end{description}


\subsection{Architecture}
\label{hlt:Hardware}

The HLT implements a processing hierarchy as outlined in Fig. \ref{hlt:fig-architecture}.
The raw data of all ALICE detectors are received via 454 Detector Data Links (DDLs)
at layer 1.
The first processing layer performs basic calibration and extracts hits and clusters (layer 2).
This is done in part with hardware coprocessors (see Section~\ref{hlt:RORC}) and therefore simultaneously
with the receiving of the data.
The third layer reconstructs the event for each detector individually.
Layer 4 combines the processed and calibrated information of all detectors and
reconstructs the whole event.
Using the reconstructed physics observables layer 5 performs the selection of events
or regions of interest, based on run specific physics selection criteria.
The selected data is further subjected to complex data compression algorithms.

\begin{figure}[hbt]
\begin{center}
\includegraphics[width=9cm]{fighlt/HLTArchitecture}
\end{center}
\vspace{-20pt}
\caption{The six architectural layers of the HLT.}
\label{hlt:fig-architecture}
\end{figure}

In order to meet the high computing demands, 
the HLT consists of a PC farm of up to 1000 multi-processor computers.
The data processing is carried out by individual software components running in parallel on the nodes of the computing cluster.
In order to keep inter-node network traffic to a minimum and for the means of parallelisation, the HLT data processing follows the natural hierarchical structure.
Local data processing of raw data is performed directly on the Front-End Processors (FEPs), hosting the H-RORCs.
Global data processing, with already reduced data, is done on the compute nodes.
The trigger decision, Event Summary Data (ESD) of reconstructed events and compressed data are transferred back to the DAQ via the HLT output DDLs.


\subsubsection{Physical cluster layout}

The HLT physical cluster layout consists of 51 
19" racks located in two counting rooms (CRs) at the ALICE experimental area at Point2 at CERN. 
The first installation stage includes 81 FEP nodes and 15 infrastructure 
nodes, each equipped with one CHARM (Computer Health and Remote Management) PCI card for Cluster Monitoring.

All the nodes are 
off-the-shelf PCs with two AMD Dualcore Opteron 2\,GHz CPUs, 8\,GB RAM and two Gigabit Ethernet connections. Each FEP contains two H-RORCs with two DDLs receiving the detector data.
The infrastructure nodes consist of four 
server nodes, two gateway nodes and 9 portal nodes.
The gateway nodes connect the cluster to the CERN network.
Two server nodes are used as monitoring servers for the Cluster Management Framework SysMES (see Section~\ref{hlt:ClusterManagement})
and two as AFS file-servers.
Each file-server implements a net storage capacity of 2 TB on a hardware Raid 6.
The portal nodes are used for connections to various ALICE subsystems (see Section~\ref{hlt:Interfaces}).
In addition, three multipurpose portal nodes and two nodes of every type were installed for redundancy reasons.
All the nodes are connected via 1-to-1~Gigabit Ethernet switches, 
while the FEPs use two Gigabit Ethernet ports. 
The infrastructure nodes and racks, housing the FEPs and compute nodes,
are interconnected via an InfiniBand backbone network.

All ALICE detectors are connected to the HLT, following the read-out granularity (number of DDLs) of
each sub-detector: TPC 216, TRD 18, PHOS 20, DiMUON 23, ITS (SPD
and SSD) 36, EMCAL 24, HMPID 20, FMD 3, and ACORDE, Trigger, ZDC,
T0 and V0 1 DDL each. 12 DDLs transfer the results of the HLT to DAQ.

\subsubsection{HLT-Readout Receiver Card (H-RORC)}
\label{hlt:RORC}

The H-RORC (Fig.~\ref{hlt:fig-RORC}) is a Virtex-4 FPGA based PCI card designed for both (i) receiving and pre-processing of the detector RAW data of all ALICE detectors and (ii) transmitting processed events out of the HLT computing farm to the DAQ. The H-RORC therefore implements the interface between the HLT system and the ALICE data transport links DDL.

\begin{figure}[hbt]
\begin{center}
\includegraphics[width=8cm, viewport=0 0 350 220]{fighlt/H-RORC}
\end{center}
\vspace{-10pt}
\caption{H-RORC data pump and FPGA coprocessor.}
\label{hlt:fig-RORC}
\end{figure} 

In order to receive these data, the H-RORC is able to host two Destination Interface Units (DIUs). Each link has a maximum bandwidth of 160\,MB/s giving a maximal total incoming data rate of 320\,MB/s. Incoming data can be transferred directly into the main memory of the Front-End Processors via PCI DMA burst transfers with a rate of up to 370\,MB/s per H-RORC. The FEPs are capable of handling two H-RORCs with two DDL interfaces each, running at full speed. In order to send processed events out of the HLT, the H-RORC can host up to two Source Interface Units (SIUs) to send data via DDL fibres to the DAQ. All transactions are PCI DMA based.

Data can also be pre-processed inside the FPGA \cite{hlt:TPCref6}. This is supported by four independent Double Data Rate Synchronous Dynamic Random Access Memory (DDR-SDRAM) modules of 32\,MB, each allowing data storage with up to 2.3\,GB/s, and two fast full-duplex serial links connect each H-RORC to its neighbours. There are two on-line-processing modules being prepared, the TPC and the DiMuon Cluster Finder. 


\subsection{Cluster management}
\label{hlt:ClusterManagement}

\begin{figure}[hbt]
\begin{center}
\includegraphics[width=8cm, viewport=0 0 400 270]{fighlt/charm-c1}
\end{center}
\vspace{-20pt}
\caption{CHARM remote management card.}
\label{hlt:fig-charm}
\end{figure} 


The HLT Cluster is managed using the SysMES Framework (System Management for Networked Embedded Systems and Clusters) and Lemon (LHC Era Monitoring).
SysMES is a scalable, decentralised, fault tolerant, dynamic and rule-based
tool set for the monitoring of networks of target systems based on industry standards.
The resources to be monitored are the applications running on the computer nodes as well as their hardware, the network switches and the RMS (Rack Monitoring System). In addition each computer implements a remote administration sensor and actuator (CHARM card Fig.~\ref{hlt:fig-charm}), allowing full remote control of the individual node, also implementing autonomous error detection and correction functionality.

The system monitoring data is visualized by Lemon servers and the SysMES servers will react accordingly.
The data transportation framework is designed to handle dynamic re-routing of the data flow during run-time. This is utilised to keep the analysis chains running even if severe problems (like hardware failure) occur, such that no data loss or damage of the online analysis will happen. Therefore an intermediate layer between SysMES and the PubSub
framework (see Section~\ref{hlt:PubSub}) is being developed, consisting of two applications, the Configuration Manager and the Resource Manager.


\subsection{Software architecture}
\label{hlt:Software}
The HLT software is divided into two functional parts, the data transportation framework and the data 
analysis. The first part is mainly of technical nature, it contains the communication of the 
components, steering and data transfer between components/nodes. 
The second part contains the physics analysis.
Development of Analysis Components is independent from data transportation which allows
for a high degree of flexibility. The Analysis Components can be run in the offline analysis
without changes, making a direct comparison of results possible.
\subsubsection{Data transportation - Publisher Subscriber framework}
\label{hlt:PubSub}
Based on the Publisher Subscriber (PubSub) principle,
a generic data transportation and steering framework has been developed,
the HLT PubSub framework \cite{hlt:phd-timm-pubsub,hlt:timm-tns2002,hlt:timm-chep04,hlt:timm-2002}. 
The PubSub framework  carries out the task of communication, data transportation and load 
distribution within the HLT. Figure~\ref{hlt:fig-pubsubkey} illustrates key principles and 
components of the PubSub framework. 

\begin{figure}%[h]
\begin{center}
%\resizebox*{0.75\columnwidth}{!}{
\includegraphics[width=0.75\textwidth,viewport=0 0 630 400]{fighlt/PubSubOverview-simplified}
%}
\parbox{0.75\columnwidth}{
\caption{Illustration of key principles and components of the HLT Publisher Subscriber framework.}
\label{hlt:fig-pubsubkey}
}
\end{center}
\end{figure}

Communication takes place in a pipe-lined data flow push architecture. Each component receives 
data, processes it and forwards new 
output data to the next component in the chain. 
The entire communication mechanism is designed for a low processing overhead, primarily through
 the use of shared memory and the exchange of descriptors.
Figure\,\ref{hlt:fig-pubsubkey} also depicts key components of the Publisher Subscriber Framework used to 
shape the data-flow in the system.
An event merger component can merge data streams consisting of data blocks of the same event
into one stream with multiple blocks per event. Communication 
between different nodes is supported by dedicated network bridge components. These connect to other components 
using the common interface and tunnel data over the network. 
The scatterer and gatherer pair of components provide capabilities for load balancing and 
distribution among multiple nodes or 
one multiprocessor 
node having multiple physical and/or logical processors. The scatterer splits a single data stream consisting 
of multiple events into several data streams, each transporting a correspondingly smaller 
number of events. Each event is forwarded 
by the scatterer unchanged.
On the other end the gatherer collects those streams again into a single stream. 
An interface that allows access to data at any point in a processing chain, e.g. for monitoring, is described
in Section~\ref{hlt:monitoring}.


\subsubsection{HLT analysis framework}
\label{hlt:AnalysisFramework}
The HLT analysis framework \cite{hlt:matthias-tns} encapsulates the data and physics analysis and 
separates it from the data transportation. 
A class hierarchy hides the complex interface logic from the derived component classes implementing the actual physics analysis
algorithms. The framework integrates the HLT analysis components into the offline AliRoot reconstruction and models 
a behaviour similar to the PubSub Framework for the components. A C interface layer provides access to the analysis 
components for external applications, which include a PubSub interface wrapper program as well as a simple standalone
run environment for the components.

The online and offline environment are two different running modes
of the HLT analysis. The first is the main running mode,
while the latter one is frequently used for the evaluation of the
HLT algorithms and analysis.
Therefore, binary compatibility is considered to be a necessary feature
of the system.
The same shared library has to run in AliRoot  and PubSub. In the online HLT, a special
processing component, the AliRootWrapperSubscriber, can load an analysis module and instantiate the actual analysis component via the external C interface
and implements all the other online  functionality which is necessary to plug the analysis code into
a HLT processing chain.
As a result, the component libraries compiled in the AliRoot framework can directly be loaded and
used from the PubSub system.


\subsection{HLT interfaces to other online systems and offline}
\label{hlt:Interfaces}

The HLT communicates with the other ALICE systems through various external interfaces and portal nodes (Fig. \ref{hlt:fig-interfaces}, \cite{hlt:sebastian-chep06}). 
Fail safety and redundancy in the design of these interfaces will avoid single points of failure and reduces the risk of time delays or data loss. 


\subsubsection{Offline interface}
The Offline Conditions Data Base (OCDB) stores all produced calibration and condition settings. 
A special application, the Taxi, requests new calibration data in regular time intervals from the OCDB and synchronises them with the local HLT copy of the database which is available on all processing nodes and allows Detector Algorithms to access the calibration data of previous runs in the same way as in the offline environment. 
After each run, updated calibration data is shipped back to the OCDB via a portal. A special PubSub data sink component collects the produced calibration data inside the HLT cluster and saves them to the File Exchange Server (FXS). A MySQL DB is used for storing additional meta data. The offline Shuttle will contact both after each run to fetch the updated data.

\begin{figure}[hbt]
\begin{center}
%\includegraphics[width=7.5cm]{fighlt/HLT-Interfaces}
\includegraphics[width=0.75\textwidth, viewport=0 0 500 400]{fighlt/HLT-Interfaces}
\end{center}
\vspace{-10pt}
\caption{HLT interfaces to the other systems of ALICE.}
\label{hlt:fig-interfaces}
\end{figure}


\subsubsection{Detector Control System (DCS) interface}
Permanently monitored values like temperatures are requested in regular time intervals by the Pendolino software from the DCS Archive DB through the DCS interface during the run.
These fetched values are then provided to the Detector Algorithms on the HLT cluster nodes.
Vice versa, the HLT writes back data to the DCS via the Front-End-Device Application Programming Interface (FedAPI), which is common among all detectors. It consists of a DIM-server (Distributed Information Management) on the HLT-side, and a DIM-client, embedded in the PVSS (Process Visualisation and Steering System) of the DCS. The server publishes monitored data and the client includes them into the DCS.


\subsubsection{Experiment Control System (ECS) interface} All the interactions with the other systems are governed by states and state transitions issued from the ECS. For this purpose a finite state machine runs on the corresponding interface portal. It accepts transition commands and returns the current state of the HLT through an HLT proxy interface software. In addition, ECS hands in running conditions like run number and trigger settings before the start of each run.


\subsubsection{Monitoring tools}
\label{hlt:monitoring}
In order to export data from the HLT at any point in the analysis chain, 
a tool called HOMER (HLT On-line Monitoring Environment including ROOT) was developed.
It provides a general access method via two user transparent mechanisms of connecting to PubSub components:
via a TCP/IP port or shared memory. A reader library
allows access to data blocks and can directly be used from the interactive ROOT prompt.

The open concept of HOMER makes detector monitoring independent of the specific
monitoring back-end. The offline monitoring AliEve is also used for general detector 
online monitoring applications.  AliEve is part of AliRoot and combines an event display
 including 3D visualisation with tools for investigation and browsing of ROOT 
data structures and histograms.
Specific monitoring components run on the HLT which accumulate results 
of the analysis of many events. The data can be in any format, also a complex ROOT data structure, e.g. a histogram.
This makes the HLT a versatile monitoring tool which allows not only examination of the quality of the
raw data, but also the monitoring of analysed data online.  


\subsection{Online data processing}
\label{hlt:DataProcessing}

The studies described in the following subsections were all performed on data simulated with 
the ALICE offline software AliRoot. 


\subsubsection{Event reconstruction}
\label{hlt:EventReconstruction}

The Time Projection Chamber (TPC) is read out by 557\,568 channels,
delivering event sizes up to 75 MB for central Pb--Pb collisions.
Fast online reconstruction algorithms consisting of a Cluster Finder and a
 Track Follower were developed
\cite{hlt:TPCref1,hlt:TPCref2,hlt:TPCref3,hlt:TPCref4,hlt:TPCref5,hlt:TPCref6,hlt:TPCref7}.
By using the 2D position in the pad-row plane and the drift time in the gas,
the Cluster Finder reconstructs 3D space-points using the centre of gravity approach.
The Track Follower maps the space-points into conformal space where helical tracks can
be fitted by a linear parametrisation.
A helix-fit of the tracks provides the kinematic properties of the particles.
The tracking is done on the sector level.
If tracks cross more than one sector, they are merged~\cite{hlt:TPCref1}.
The algorithms were originally developed for the STAR L3 trigger~\cite{hlt:Adler:2002ab}.
Beyond multiplicities $\dNdeta$~of~$4000$ the Cluster Finder efficiency
deteriorates due to a significant number of overlapping clusters.
A fast Hough-transform tracker was developed for this scenario,
using the raw data directly~\cite{hlt:TPCref7}.

The Transition Radiation Detector (TRD) reconstruction on HLT was designed
to directly use the algorithms implemented for the offline analysis.
The offline HLT-TRD cluster finder and the HLT-TRD tracker are able to handle a full
non-zero suppressed TRD event (load of 43.2 MB) in real time.
In the case of zero-suppressed data the volume of a raw Pb--Pb $\dNdeta = 8000$
event does not exceed 7.1 MB including a 20\% coding overhead.
Given the design rate of the HLT-TRD event processing chain as of 200 Hz the HLT cluster
allocates 6 processing nodes per TRD super-module to fulfill the timing requirements.
In addition to the components responsible of the reconstruction the TRD-HLT processing
chain incorporates the offline model to produce the calibration reference data and
delivers a collection of relevant histograms to the interface to OCDB.

For the Photon Spectrometer (PHOS) detector the main objective is to measure the exact timing and energy
information of an electromagnetic shower in a typical $3 \times 3$ crystal matrix of the calorimeter.
Therefore the signal will be oversampled in order to measure the pulse shape.
Consequently, at a trigger rate of 1 kHz the 5 PHOS modules will deliver a maximum of
3 GB/s for non-zero-suppressed and non-baseline-corrected events.
Based on the Peak Finder algorithm,
processing components were developed to analyse the pulse shape and reduce the data
to the relevant time and energy information.
Furthermore, algorithms for cluster and shower reconstruction will be run on the HLT.

In the Muon Spectrometer the primary goal of the dimuon High-Level Trigger (dHLT)
is to improve the sharpness
of the \pt cut compared to what is obtained by the L0 trigger.
dHLT performs fast, online, partial event reconstruction~\cite{intro:TDR10} on stations 4 and 5,
which allows it to improve the \pt resolution by more than a factor of two.
Thus a better separation between background and interesting signal events is possible
reducing the background data accepted by the spectrometer.
The dHLT can handle at least a 1 kHz event rate with a data rate of 500~MB/s.
Event reconstruction starts with hit reconstruction,
which uses a simple 3 pad cluster finding algorithm.
The efficiency achieved is above 98\%, and a resolution of 100~$\mu$m and 1~mm along
the {\em y} and {\em x} directions (perpendicular and horizontal to the ground respectively) is reached respectively.
For tracking, a track following algorithm~\cite{hlt:MansoAlgo} is used,
giving a single muon reconstruction efficiency above 97\% and is independent
of the event size.
From the found tracks the \pt is estimated using a simple calculation involving the
particle's deflection angle~\cite{intro:TDR1}.
This gives a \pt resolution of about 0.13~GeV/\textit{c} and 0.18~GeV/\textit{c}
around the 1~GeV/\textit{c} and 2~GeV/\textit{c} \pt cuts respectively.
The total processing time for a central event is about 2.5~ms.
Further details about the algorithms can be found in the
dHLT Project Review~\cite{hlt:dHLTReview}.


\subsubsection{Online data compression}

For the TPC detector,
the data rate is expected to reach up to 15 GB/s after zero suppression.
Any means for reducing the data size online is therefore desirable in order to
increase the number of events which can be collected and written to mass storage.
Lossless and lossy data compression techniques like entropy encoding and vector quantisation
were evaluated using simulated TPC data~\cite{hlt:dieter-tns2006}.
The results indicate that standard compression algorithms
achieve compression factors of about 2.
However, HLT online pattern recognition results in track and cluster
information which effectively models the TPC data and can be utilised to compress
such data more efficiently and reduce the data size to about 11\%,
with a negligible loss of tracking performance for the expected particle
multiplicities at LHC \cite{hlt:TPCref1, hlt:compresion-1}.

In PHOS, after zero-suppression and skipping of empty channels in the front-end electronics,
the data rate into HLT is about 300 MB/s.
Online processing of the pulse shapes results in an energy and a time-of-flight information per
channel.
This information is forwarded to DAQ for achieving at a data rate of about 20 MB/s
which corresponds to a data volume reduction factor of 15.


\subsubsection{Calibration procedure}
\label{hlt:calibration}

Detector algorithms are implemented by detector groups in order to create
useful calibration data, which is archived in the OCDB.
These algorithms are designed to run in both offline and HLT framework.
In the HLT, the detector algorithms are embedded in normal HLT processing components.
A special base class for those calibration components has been added to the HLT
analysis framework, as calibration algorithms mainly accumulate data of several
events and deliver the result at the end of run.
The calibration data can be shipped
via 3 parallel output streams namely the OCDB, to TCP ports (HLT online monitoring)
and to the DAQ via the data stream.
Calibration components publish results at the end of the run via the 
offline interface to the OCDB (see Section~\ref{hlt:Interfaces}).

For the TPC Calibration the general off-line calibration classes for signal and
noise have already been integrated in the HLT framework.
They were successfully tested, processing more than
one million raw data events recorded during the TPC commissioning phase.

Benchmarks indicate less than 16\%
of the total time is spent inside the HLT framework.

The calibration parameters for PHOS calibration mainly consist of energy
and time-of-flight distribution for each detection channel,
for relative calibration as well as \pizero\ invariant mass histograms for
absolute calibration.
Using simulated data, the calibration components were successfully tested
at the HLT cluster.

