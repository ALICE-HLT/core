\chapter{Routine operation of the HLT}\label{ch:normal}
This chapter covers the basic tasks of the \gls{hlt} shifter\index{shifter!tasks} --- running the HLT from the \Command{ECSGUI}\index{ECSGUI} application, and modifying existing configurations of the \gls{chain}, the set of processes that constitute the HLT.

It is assumed that the software is set up and running. For starting up the system and the HLT software, see Chapter~\ref{ch:setup}.

The HLT is operated from the machine \Machine{aldaqacr03} (see sticker on the keyboard) in the \gls{acr}.
 
Actual  control of the HLT is done via the machine \Machine{portal-ecs0}. 

%\section{Log into the machine in the control room}
%Hostname of the machine is \Machine{aldaqacr03} (sticker on the keyboard).

%\begin{itemize}
% \item Username: hlt
% \item Password: ........
%\end{itemize}

%This machine is used for access to the HLT cluster and display for 
%the operating software. 

%Actual  control of the HLT is done via the machine \Machine{portal-ecs0}. 

\section{Getting help for HLT software}\label{sec:help}\index{help}
Click the window manager menu button on the left of the taskbar in the lower middle screen, and select the \Command{firefox}\index{firefox!start} icon in the pop-up.
\begin{figure}[hbt]
 \centering
 \includegraphics[width=8cm]{screenshots/wm-start.png}
 \caption{Window manager menu button\label{fig:wm_start}}
\end{figure}


This should bring up the documentation pages of HLT. On the first page there is a short description of the startup procedure. Follow the instructions here and use the links to start HLT software.
\begin{figure}[hbt]
 \centering
 \includegraphics[width=4cm]{screenshots/wmmenu.png}
 \caption{Start firefox to get to startup instructions\label{fig:firefox_start}}
\end{figure}

\section{Task of the HLT shifter}
The \gls{hlt} operates via a \gls{chain} of software processes running on the nodes of the HLT cluster. The \gls{configuration}\index{configuration!purpose} of the chain should match the experimental configuration of \gls{ecs} and provide the ability to monitor the HLT operation. 

To this end a set of prepared configurations\index{configuration!prepared} are created. These are categorised by beam type, e.g. pp, and by run type (mode): PHYSICS, STANDALONE, DDL\_TEST.
For each type there may be development (devel) versions, production versions and valid-test versions.\index{configuration!types}

One of the available configurations in each of the categories selected by beam type and run type  is activated for use (at present pp-PHYSICS, pp-STANDALONE, DDL\_TEST). These active configurations are shown in the Webgui\index{Webgui!portlet} \gls{portlet} \textit{Active Configurations} (Section~\ref{sec:portlets}). They may be drawn from the devel, production and valid-test sets, but ideally for routine running they should all be production versions.

The typical task of the HLT shifter is to activate (Section~\ref{sec:activate}) a configuration from the set of prepared configurations, to start up (\ref{sec:start}) and shut down (\ref{sec:stop}) the HLT in the case of standalone operation, and to create a new configuration (\ref{sec:editconf}) (usually by slightly modifying an old configuration) if necessary.

\subsection{Shift report}
The shifter is asked to provide a shift report at the end of his shift. The report includes:
\begin{enumerate}
\item Short report on all actions during the shift
\item Summary of runs taken during the shift, and the HLT mode.
\item Problems and irregularities
\item Hardware and Software interventions
\end{enumerate}

Just open an email at the beginning of your shift and make notes about the activity. At the end of the shift send the report by email to either the HLT mailing list {\tt alice-hlt-core@cern.ch}, or if you are not a member of the list to {\tt Jochen.Thaeder@cern.ch} and {\tt Matthias.Richter@cern.ch}. At the end of the shift add the report to the ALICE Logbook \ref{sec:logbook}. Also add the report to the ALICE Logbook \ref{sec:logbook}. Please note: Jochen has to approve your rights to add messages to the logbook after your first login.

\section{Day-to-day operation}
This first part will only describe day-to-day operation of the HLT. All software should normally be setup and ready for use when an operator logs into the system so that only operation of the chain itself should be needed. In later chapters, the software and procedures to operate and debug HLT are described in more detail.



\newpage
\subsection{Checklist}
\colorbox{Yellow1}{
\begin{minipage}{0.80\textwidth}
\begin{itemize}
\item ECS GUI: here the current status of the ECS is displayed. Shifter has to follow when the run is started and stopped
\item Make sure that the running mode is according to the shift plan. As a general rule HLT runs in mode B or C.
\item Statistics: Watch the ESMP portlets for
\begin{itemize}
\item Received event count: linear increasing within the run
\item Average received event rate during last second: small fluctuations arounf the trigger frequency
\item Events in chain: stable number, sudden increase indicates pileup of events in the chain
\item Task Manager components: stable troughout the run
\end{itemize}
\item Sysmes: Nodes should not change status to red or black. notice changes to yellow or orange
\item InfoBrowser: Errors and Warnings might occur, however they do not need to be fatal. As a general rule a severe problem is indicated by lots of errors in the browser.
\item Event display: eye scan for principal operation and for strange events. The terminal also print information on the trigger decision and the counters for the trigger menu.
\item Shift report: it is easier to start it right from the beginning rather than writing everything at the end
\item DAQ logbook: already during the run, the trigger counters must be checked
\end{itemize}
\end{minipage}
}

\subsection{Brief description of operator software}
During routine running of the HLT the following operator software
will be available on the six screens of the HLT operator machine.
\begin{description}
 \item[Webgui] - Webgui\index{Webgui} is the informal term for the ESMP2 GUI which will eventually be the main user interface  for HLT control. Currently, the Webgui provides status\index{status!Webgui} information for all running analysis chains and an editor for creating and changing configurations. This is also where configurations can be verified and stored to the version control system.%subversion. 
(At the date of the manual, manual configuration is recommended. See Section~\ref{sec:manual-config}).
More functionality will be moved into the Webgui as the software matures.
 \item[TMGUI]\index{TMGUI} - Is used for finer grained control of a chain. It makes it possible to manually kill hanging processes and to change the debug level.
 \item[ECSGUI]\index{ECSGUI} - This is the user interface for control of the HLT in stand-alone mode. This GUI provides HLT with the same states and transitions that would be received when running under the control of \gls{ecs}. When ECS has control, the state of the \gls{chain} is shown in this  GUI.
 \item[InfoBrowser]\index{InfoBrowser} - Logs error messages from all software components in the HLT chain.
 \item[SysMES]\index{SysMES} - Status\index{status!SysMES} view of the cluster hardware and software.
 \item[Logbook]\index{logbook} - Log events related to shift operation here.
\end{description}


\subsection{Default organization of screens}
An overview of how the different programs are laid out on the screens by default can be seen in Figure \ref{fig:hltscreens}. The television screen to the far left in the \gls{acr} is also at HLT's disposal.

\begin{figure}
 \centering
 \includegraphics[width=\textwidth]{screenshots/all-screens2.png}
 \caption{The upper row of screens has, from left to right,
Webgui\index{Webgui!position}, InfoBrowser\index{InfoBrowser!position} and SysMES\index{SysMES!position}; the lower screens contain TMGUI\index{TMGUI!position} and ECSGUI\index{ECSGUI!position}, the konsole\index{konsole!position} and an editor for an HLT configuration.}\label{fig:hltscreens}
\end{figure}

\subsection{Stand-alone operation}
Usually, for an operator on shift, the actual starting and stopping of HLT is handled by \gls{ecs}. However, for internal testing, HLT can be controlled by ECSGUI, or in the future by Webgui. If ECS is in control, the lock is red and the button on ECSGUI is greyed out.

\subsection{ECSGUI}\label{sec:ECSuse}\index{ECSGUI}
\begin{center}
\colorbox{Yellow1}{
\begin{minipage}{0.95\textwidth}
WARNING! Do NOT close the ECSGUI\index{ECSGUI!lock} without releasing the lock. If the lock for some reason is lost  and it's not possible to get it back, ask ECS to to execute:

\Command{\# /ecs/HLT/Linux/checklockHLT <detectorname>}
\end{minipage}
}
\end{center}

Currently, the Webgui\index{Webgui} is not capable of starting and stopping a configuration in stand-alone mode. However, this can be done by the ECSGUI\index{ECSGUI!use}, Figure~\ref{fig:ecs_gui}.

\begin{figure}
 \centering
 \includegraphics[viewport=0 0 161 148]{screenshots/ecs_gui.png}
 % ecs_gui.png: 161x148 pixel, 72dpi, 5.68x5.22 cm, bb=0 0 161 148
 \caption{Screenshot of the ECSGUI\label{fig:ecs_gui}}
\end{figure}

Take control of the HLT by clicking connectivity and choosing HLT. The lock should turn green, indicating stand-alone operation. If the lock is red, then ECS is in control of HLT and will be able to change the HLT's state. To change state manually, click the green button and choose the desired state.

The status\index{status!ECSGUI} in the ECSGUI\index{ECSGUI!status} will change while state is changing and then change again when the new state is reached. Be patient while waiting for the states to change. For large configurations, they can take as long as several minutes.\index{configuration!slow}

From any state you can reverse the process by choosing an appropriate action, if some problem occurs.

While the state is in transition, the status is indicated in ECSGUI as INITIALIZING, CONFIGURING, ENGAGING, DISENGAGING, \dots For a diagram of these states see Appendix~\ref{ch:diagECSp}.

\subsubsection{Start a chain}\label{sec:start}\index{chain!start}
A chain is started from either the OFF or INITIALIZED state for a fresh startup, or from the CONFIGURED state if the configuration is not changed. (OFF and INITIALIZED are \gls{ecs} states that the HLT does not distinguish).\index{configuration!start}  

If no configuration both on the HLT side and on the DAQ side has changed, (i.e. no changes of detector, limits or trigger menu for the HLT), then the chain can be started from the CONFIGURED state. 

\begin{enumerate}
 \item The initial state after a fresh startup will be OFF. 

   In this state choose INITIALIZE. 

   The state will change (through INITIALIZING) to INITIALIZED.
 \item When just starting, stopping and reconfiguring, the state will usually be INITIALIZED, CONFIGURED or READY. 

    From the INITIALIZED state, choose CONFIGURE. 

    Accept the default options in the window that pops up. 

    The state will change (through CONFIGURING) to CONFIGURED --- this may take some time.
 \item When in CONFIGURED state, choose ENGAGE. 
    A window will pop up: use E for HLT\_MODE and 123 (an arbitrary number) for RUN\_NUMBER. 

    Other option(s) can remain at default. 

    ENGAGE should transit the state through ENGAGING to READY.
 \item The last state change from READY is START, which should bring the chain (through STARTING) into the RUNNING state.
\end{enumerate}

For any of the steps above, the state transition might fail and one would have to start debugging. See later sections for guidance for how this can be done.

\subsubsection{Stop a chain}\label{sec:stop}\index{chain!stop}
Stopping a chain follows the reverse order of starting a chain.

\begin{enumerate}
 \item State will initially be RUNNING. 
     Choose STOP. The state will change (though COMPLETING) to READY.
 \item From READY, choose DISENGAGE to change state through DISENGAGING to CONFIGURED.
 \item If the chain is stopped in order to start a new configuration, choose RESET to bring the state back to INITIALIZE; then CONFIGURE should be chosen (see above).
\end{enumerate}


\subsection{Create or edit a configuration}\label{sec:editconf}\index{configuration!create}\index{configuration!edit}
The Webgui\index{Webgui!configuration} provides a \textit{Configuration Manager} view (see Figure \ref{fig:taskman_conf}) where the configuration file that describes an HLT chain can be created or edited. A link to open this manager can be found under \textit{Applications} in the \textit{Mainmenu} \gls{portlet} of the Webgui \textit{Partition view} page. 

The configuration file is an \gls{xml} file (extension \verb|.xml|) that describes the structure of the HLT chain.  After creation or editing, it must be verified for xml validity and for the ability of the configuration to be created, and then activated before it can be used.\index{configuration!file}

The Configuration\index{configuration!manual} can also be created or edited manually. See Section~\ref{sec:manual-config}. This is the recommended way at the date of the manual.

Configuration, including verification and activation, can be done while the chain is running; the new data will be used when CONFIGURE is next chosen in the ECSGUI\index{ECSGUI!state}.

A configuration in the portlet window is green by default. If the configuration has been edited since the last activation, it will be shown on orange. Note that the edited version will not be used until it has been activated. A configuration that has been deleted since activation will be shown in red. In all cases the version that is shown is active and will be used by ECSGUI.

To edit a configuration, do:\index{configuration!edit}

\begin{enumerate}
 \item On the bar at the bottom, next to ``Name'' there is a small arrow;
clicking on this reveals the available configurations in the categories development, valid-test, or production. You may need to click on the ``+'' sign next to the header to expand the list of file names.
 \item Double-click on a configuration to bring it up in an editor.
 \item Edit the file.
 \item When changes has been made, click on the button  ``Save \& verify'' to verify the file after saving, ``Save only'' if you wish to continue editing later or ``Cancel'' if you wish to cancel the entire edit.
\item If ``Save \& verify'' is chosen, add a description of the changes in the checkin window of the version control system. Then ctrl-o, enter, ctrl-x to save and exit (see messages on screen). 
\end{enumerate}

To create a new configuration:\index{configuration!create}

% Check for correctness and add pictures where needed
\begin{enumerate}
 \item Open an existing configuration by double-clicking it.
 \item Save the configuration under a different name by editing the name field and clicking save.
 \item Add a suitable description in the checkin window and save and exit.
\end{enumerate}

When this is done, changes can be made by following the edit procedure above. Remember to also edit the other fields -- like author, date and description -- if needed.

\begin{figure}
 \centering
 \includegraphics[width=0.8\textwidth]{screenshots/confmanager.png} 
%\includegraphics[width=373pt,viewport=0 0 1280 1024]{screenshots/taskman_conf.png}
 % taskman_conf.png: 1280x1024 pixel, 72dpi, 45.16x36.12 cm, bb=0 0 1280 1024
 \caption{Configuration Manager application of the Webgui\label{fig:taskman_conf}}
\end{figure}

\subsection{Activate a configuration}\label{sec:activate}\index{configuration!activate}
The Webgui also provides an \textit{Active Configurations} \gls{portlet} (see figure~\ref{fig:conf-portlet}) where one can set and change the active configurations. A link to the \textit{Active Configurations} \gls{portlet}  can be found under \textit{Applications} in the \textit{Mainmenu} \gls{portlet} of the Webgui.


\begin{figure}[tb]
 \centering
 \includegraphics[width=0.5\textwidth]{screenshots/activeconfig.png}
 \caption{Screenshot of Active Configuration portlet.\label{fig:conf-portlet}}
\end{figure}

\begin{enumerate}
 \item Click on the ``Activate Configuration'' button of the  \textit{Active Configurations} portlet.
 \item A dialog will pop-up where the run-type (Figure~\ref{fig:confdialog}) and configuration (Figure~\ref{fig:conflist}) can be chosen. % Insert picture
\begin{figure}[htb]
 \centering
 \includegraphics[scale=0.5]{screenshots/activeconfig-dialog.png}
 \caption{Screenshot of pop-up.\label{fig:confdialog}}
\end{figure}

\begin{figure}[htb]
 \centering
 \includegraphics[scale=0.5]{screenshots/activeconfig-list.png}
 \caption{Screenshot of list of configurations.\label{fig:conflist}}
\end{figure}

 \item Then confirm by clicking ``ok''.
\end{enumerate}



% Use the Activate a Configuration window (see figure \ref{fig:activate_config}) in the webgui to select the desired configuration to be prepared. First select the detector to be used by clicking the tabs at the top of the window. Then select a configuration for each of the run-types (listed to the left) that should be active. Global is for global partition i.e. a global configuration consisting of all detectors.

% \begin{figure}
%  \centering
%  \includegraphics[width=373pt,viewport=0 0 669 582]{screenshots/activate_config.png}
 % activate_config.png: 669x582 pixel, 72dpi, 23.60x20.53 cm, bb=0 0 669 582
%  \caption{Activate configuration window\label{fig:activate_config}}
% \end{figure}


