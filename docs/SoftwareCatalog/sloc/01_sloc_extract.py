#!/usr/bin/env python

import re, os, sys
import locale

import numpy

# Output seems to be for US locale, so set it to that for now.
import locale
locale.setlocale(locale.LC_ALL, 'en_US.utf8')

slocStr = "Total Physical Source Lines of Code \(SLOC\)"
devStr = "Development Effort Estimate, Person-Years \(Person-Months\)"
scheduleStr = "Schedule Estimate, Years \(Months\)"
developersStr = "Estimated Average Number of Developers \(Effort/Schedule\)"
costStr = "Total Estimated Cost to Develop"

slocRegExp = re.compile("^" + slocStr)
effortRegExp = re.compile("^" + devStr)
scheduleRegExp = re.compile("^" + scheduleStr)
developersRegExp = re.compile("^" + developersStr)
costRegExp = re.compile("^" + costStr)

statList = []

for fileName in sys.argv[1:]:
    
    slocPackageStats = {}

    for line in open(fileName).readlines():
        
        if slocRegExp.match(line):
            tmp = line.split("=")[1].strip()
            slocPackageStats["slocStr"] = locale.atoi(tmp)
            
        elif effortRegExp.match(line):
            tmp = line.split("=")[1].strip()
            pYears, pMonths = tmp.split("(")
            pMonths = pMonths.split(")")[0]
            slocPackageStats["pYearsStr"] = locale.atof(pYears)
            slocPackageStats["pMonthsStr"] = locale.atof(pMonths)
            
        elif scheduleRegExp.match(line):
            tmp = line.split("=")[1].strip()
            sYears, sMonths = tmp.split("(")
            sMonths = sMonths.split(")")[0]
            slocPackageStats["scheduleYearsStr"] = locale.atof(sYears)
            slocPackageStats["scheduleMonthsStr"] = locale.atof(sMonths)
            
        elif developersRegExp.match(line):
            tmp = line.split("=")[1].strip()
            slocPackageStats["developersStr"] = locale.atof(tmp)
            
        elif costRegExp.match(line):
            tmp = line.split("=")[1].strip()
            slocPackageStats["costStr"] = locale.atof(tmp.split("$")[1].strip())
    
    package = os.path.basename(fileName)
    package = os.path.splitext(package)[0]
    
    statList.append((
        package,
        slocPackageStats["slocStr"], 
        slocPackageStats["pYearsStr"], slocPackageStats["pMonthsStr"],
        slocPackageStats["scheduleYearsStr"], slocPackageStats["scheduleMonthsStr"],
        slocPackageStats["developersStr"], slocPackageStats["costStr"]
    ))


# Print SLOC statistic to stdout for easy redirection to file.
statsArray = numpy.array(
    statList, dtype=[
        ("Package", str, 30), 
        ("SLOC", int), 
        ("PYears", float), ("PMonths", float), 
        ("SYears", float), ("SMonths", float), 
        ("Developers", float), ("Cost", int)
    ]
).view(numpy.recarray)


# Print the array
print str(statsArray.dtype.names).strip("(").strip(")")

for row in statsArray:
    print str(row).strip("(").strip(")")


print "'Totals', ",
print statsArray.SLOC.sum(), ",",
print statsArray.PYears.sum(), ",", statsArray.PMonths.sum(), ",",
print statsArray.SYears.sum(), ",", statsArray.SMonths.sum(), ",",
print statsArray.Developers.sum(), ",",
print statsArray.Cost.sum()
