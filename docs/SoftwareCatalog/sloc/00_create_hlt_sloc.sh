# HLT internal packages
DIRS="BCL \
Components \
EVTREPLAY \
Framework \
H-RORC \
MLUC \
PSI2 \
RFLASH \
RunManager \
SimpleChainConfig1 \
SimpleChainConfig2 \
TPC-OnlineDisplay \
TRD-Readout \
TaskManager \
Utility_Software \
bigphys \
control \
interfaces \
tools \
util \
utilities"

for compDir in $DIRS ; do sloccount ../../../$compDir > hlt/$compDir.sloc ; done

# aliroot
# svn co https://alisoft.cern.ch/AliRoot/branches/v5-01-Release/

# cd hlt	# ALSO HLT INTERNAL
# sloccount . > hlt/aliroot-hlt.sloc

# cd aliroot 
# sloccount . > other/aliroot.sloc


# root
# mkdir /tmp/root-sloccount
# cd /tmp/root-sloccount
# cd wget ftp://root.cern.ch/root/root_v5.30.01.source.tar.gz
#sloccount . > other/root.sloc

# infologger
#sloccount . > other/infologger.sloc
