#!/usr/bin/env python

import sys, re
from operator import itemgetter

langTotRegExp = re.compile("^Totals grouped by language \(dominant language first\):")
langDict = {}

for fileName in sys.argv[1:]:
    
    parsingLanguages = False
    
    for line in open(fileName).readlines():
        
        if langTotRegExp.match(line):
            parsingLanguages = True
        
        elif parsingLanguages:
            
            if line == "\n":
                break
                
            else:
                tmpStr = line.split("(")[0]
                lang, amount = tmpStr.split(":")
                
                lang = lang.strip()
                amount = amount.strip()
                
                if not lang in langDict:
                    langDict[lang] = 0
                
                langDict[lang] += int(amount)


total = sum(langDict.values())

print "Language\tLines\tPercentage"

for key, val in sorted(langDict.iteritems(), key=itemgetter(1), reverse=True):
    print "%s\t%s\t%f" % (key, val, (float(val)/float(total)) * 100)

print "Total\t%s\t100" % total