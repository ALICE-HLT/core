import sys

def main():
    try:
        from pygraph.classes.graph import graph
        from pygraph.classes.digraph import digraph
        from pygraph.algorithms.searching import breadth_first_search
        from pygraph.readwrite.dot import write
    except ImportError, e:
        print "Could not import python-graph. Please install it with your package manager."
        print "You can also find it here: http://code.google.com/p/python-graph/"
        sys.exit()
    
    # Setup the graph
    
    dtdGraph = digraph()
    
    nodeDependencies = {}
    
    for line in open("data_transport_dependencies"):
        if "->" in line:
            node, dependencies = line.split("->")
            node = node.strip()
            
            if not dtdGraph.has_node(node):
                dtdGraph.add_node(node)
            
            for dep in dependencies.split(","):
                dep = dep.strip()
                
                if not dtdGraph.has_node(dep):
                    dtdGraph.add_node(dep)
                
                edge = (node, dep)
                
                if not dtdGraph.has_edge(edge):
                    dtdGraph.add_edge(edge)
                        

    
    dot = write(dtdGraph)
    
    filename = "hlt_sw_dependencies.dot"
    
    f = open(filename, "w")
    f.write(dot)
    f.close()
    
    print "Graph written to dot file: %s" % filename
    
    print "\nIt can now be opened by xdot: '$ xdot %s'" % filename

    print "\nTo produce SVG from the dot file, do: '$ dot -Tsvg -o outfile.svg %s'" % filename


if __name__ == "__main__":
    main()

