cd ChainVisualization

if [ ! -d build ]
then
	mkdir build
fi

# Umbrello can be used to create svg (or other formats) from 
# the UML diagrams in the xmi file.
umbrello --export svg --directory build eyelt.xmi
umbrello --export svg --directory build xdot.xmi

