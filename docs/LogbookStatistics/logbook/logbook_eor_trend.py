import datetime

from sqlalchemy import func
from sqlalchemy.sql import select, desc

import numpy as np

import sys
sys.path.append("..")

from logbook import engine, logbook
from logbook import physicsRuns, physicsWithHltFailed


# Needs to crete alias for use as subquery.
physicsWithHltFailedAlias = physicsWithHltFailed.alias()


def createEorReasonCountQuery(reason):

    retQuery = select(
        [
            func.YEARWEEK(func.FROM_UNIXTIME(logbook.c.time_created)).label("yearweek"),
            func.count(logbook.c.eor_reason).label("reasonCount")
        ],
        logbook.c.eor_reason == reason
    ).group_by(
        func.YEARWEEK(func.FROM_UNIXTIME(logbook.c.time_created))
    )
    
    return retQuery


# Setup subqueries for all the different eor_reason
eorReasons = select([physicsWithHltFailedAlias.c.eor_reason]).group_by(
    physicsWithHltFailedAlias.c.eor_reason
)

header = [("timestamp", datetime.datetime)]
eorQueries = []

for item in engine.execute(eorReasons).fetchall():
    if item[0] != None:
        header.append((item[0], int))
        eorQueries.append(createEorReasonCountQuery(item[0]).alias())


# Get all the weeks involved
yearweekTable = select(
    [
        func.YEARWEEK(func.FROM_UNIXTIME(physicsWithHltFailedAlias.c.time_created)).label("yearweek")
    ]
).group_by(
    func.YEARWEEK(func.FROM_UNIXTIME(physicsWithHltFailedAlias.c.time_created))
).alias()


# List of columns to be selected. Reverting back to normal date type.
tmpList = [func.STR_TO_DATE(yearweekTable.c.yearweek + "Monday", '%X%V%W')]
tmpList = tmpList + [ q.c.reasonCount for q in eorQueries ]

# Setup all the joins
j = yearweekTable.outerjoin(
    eorQueries[0], yearweekTable.c.yearweek == eorQueries[0].c.yearweek)

for i in range(len(eorQueries) - 1):
    j = j.outerjoin(eorQueries[i+1], yearweekTable.c.yearweek == eorQueries[i+1].c.yearweek)


# Setup the main query
query = select(
    tmpList, 
    from_obj=[j]
)

eorList = []

# Finally print the result
for item in engine.execute(query).fetchall():
    # First is date and has different type
    tmpList = [item[0:1][0]]
    # Rest is long and None. Convert all to int. None -> 0
    tmpList = tmpList + [ int(val or 0) for val in item[1:] ]
    eorList.append(tuple(tmpList))

eorTrendArray = np.array(eorList, dtype=header).view(np.recarray)

np.savez(
    "../dataset/logbook_eor_trend.npz",
    eorTrendArray=eorTrendArray
)
