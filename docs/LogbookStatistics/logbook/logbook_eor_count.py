from sqlalchemy import create_engine, MetaData
from sqlalchemy import Table, Column, TIMESTAMP
from sqlalchemy import func
from sqlalchemy.sql import select, and_, or_, not_, desc

import numpy as np

import sys
sys.path.append("..")

from logbook import engine, logbook, physicsWithHlt


print "Number of physics runs: ", engine.execute(physicsWithHlt).rowcount


# Need to use NULL-safe equlity operator: "<=>"
# http://dev.mysql.com/doc/refman/5.5/en/comparison-operators.html#operator_equal-to
print "Successful runs (daq_success == 1 and ecs_success == 1): ",
query = physicsWithHlt.where(
    and_(logbook.c.ecs_success.op("<=>")(1), logbook.c.daq_success.op("<=>")(1))
)
print engine.execute(query).rowcount


print "Failed runs ((NOT daq_success == 1) or (NOT ecs_success == 1)): ",
query = physicsWithHlt.where(
    and_(
        or_(
            not_(logbook.c.ecs_success.op("<=>")(1)), 
            not_(logbook.c.daq_success.op("<=>")(1))
        )
    )
)
print engine.execute(query).rowcount


# All run types

physicsWithHltAlias = physicsWithHlt.alias()

print "\nAll failures (not grouped)."
print "============================="

allFailureList = []

baseQuery = select(
    [
        physicsWithHltAlias.c.eor_reason,
        func.count(physicsWithHltAlias.c.eor_reason).label("count")
    ]
).group_by(
    physicsWithHltAlias.c.eor_reason
).order_by(desc("count"))

for eor_reason, amount in engine.execute(baseQuery).fetchall():
    allFailureList.append((eor_reason, amount))
    print "%s: %s" % (eor_reason, amount)

allFailureArray = np.array(
    allFailureList, dtype=[("eor_reason", str, 20),("count", int)]
).view(np.recarray)


print "\nAll SOR_failure (not grouped)"
print "==============================="

sorFailureAllList = []

baseQuery = select(
    [
        physicsWithHltAlias.c.eor_reason,
        func.count(physicsWithHltAlias.c.eor_reason).label("count")
    ], 
    physicsWithHltAlias.c.eor_reason.like("SOR_failure%")
).group_by(
    physicsWithHltAlias.c.eor_reason
).order_by(desc("count"))

for eor_reason, amount in engine.execute(baseQuery).fetchall():
    sorFailureAllList.append((eor_reason, amount))
    print "%s: %s" % (eor_reason, amount)

sorFailureAllArray = np.array(
    sorFailureAllList, dtype=[("eor_reason", str, 20),("count", int)]
).view(np.recarray)


print "\nSOR_failure grouped"
print "======================"

sorFailureGroupedList = []

baseQuery = select(
    [
        func.SUBSTRING_INDEX(physicsWithHltAlias.c.eor_reason, "_", 2),
        func.count(physicsWithHltAlias.c.eor_reason).label("count")
    ], 
    physicsWithHltAlias.c.eor_reason.like("SOR_failure%")
).group_by(
    func.SUBSTRING_INDEX(physicsWithHltAlias.c.eor_reason, "_", 2)
).order_by(desc("count"))

for eor_reason, amount in engine.execute(baseQuery).fetchall():
    sorFailureGroupedList.append((eor_reason, amount))
    print "%s: %s" % (eor_reason, amount)

sorFailureGroupedArray = np.array(
    sorFailureGroupedList, dtype=[("eor_reason", str, 20),("count", int)]
).view(np.recarray)


print "\nSubsystem_failure"
print "==================="

subsysFailureList = []

baseQuery = select(
    [
        physicsWithHltAlias.c.eor_reason, 
        func.count(physicsWithHltAlias.c.eor_reason).label("count")
    ], 
    physicsWithHltAlias.c.eor_reason.like("Subsystem_failure%")
).group_by(
    physicsWithHltAlias.c.eor_reason
).order_by(desc("count"))

for eor_reason, amount in engine.execute(baseQuery).fetchall():
    subsysFailureList.append((eor_reason, amount))
    print "%s: %s" % (eor_reason, amount)

subsysFailureArray = np.array(
    subsysFailureList, dtype=[("eor_reason", str, 20),("count", int)]
).view(np.recarray)


from logbook import physicsWithHltFailed
physicsWithHltFailedAlias = physicsWithHltFailed.alias()

# For failed runs
#################


print "\nSOR_failure grouped. Failed runs."
print "==================================="

sorFailureGroupedFailedList = []

baseQuery = select(
    [
        func.SUBSTRING_INDEX(physicsWithHltFailedAlias.c.eor_reason, "_", 2),
        func.count(physicsWithHltFailedAlias.c.eor_reason).label("count")
    ], 
    physicsWithHltFailedAlias.c.eor_reason.like("SOR_failure%")
).group_by(
    func.SUBSTRING_INDEX(physicsWithHltFailedAlias.c.eor_reason, "_", 2)
).order_by(desc("count"))

for eor_reason, amount in engine.execute(baseQuery).fetchall():
    sorFailureGroupedFailedList.append((eor_reason, amount))
    print "%s: %s" % (eor_reason, amount)

sorFailureGroupedFailedArray = np.array(
    sorFailureGroupedFailedList, dtype=[("eor_reason", str, 20),("count", int)]
).view(np.recarray)


print "\nSubsystem_failure. Failed runs."
print "================================="

subsysFailureFailedList = []

baseQuery = select(
    [
        physicsWithHltFailedAlias.c.eor_reason, 
        func.count(physicsWithHltFailedAlias.c.eor_reason).label("count")
    ], 
    physicsWithHltFailedAlias.c.eor_reason.like("Subsystem_failure%")
).group_by(
    physicsWithHltFailedAlias.c.eor_reason
).order_by(desc("count"))

for eor_reason, amount in engine.execute(baseQuery).fetchall():
    subsysFailureFailedList.append((eor_reason, amount))
    print "%s: %s" % (eor_reason, amount)

subsysFailureFailedArray = np.array(
    subsysFailureFailedList, dtype=[("eor_reason", str, 20),("count", int)]
).view(np.recarray)


print "\nSOR_failure grouped for HLT. Failed runs."
print "==========================================="

sorFailureGroupedHltList = []

baseQuery = select(
    [
        func.SUBSTRING(physicsWithHltFailedAlias.c.eor_reason, 13),
        func.count(physicsWithHltFailedAlias.c.eor_reason).label("count")
    ], 
    physicsWithHltFailedAlias.c.eor_reason.like("SOR_failure:HLT%")
).group_by(
    func.SUBSTRING(physicsWithHltFailedAlias.c.eor_reason, 13)
).order_by(desc("count"))

for eor_reason, amount in engine.execute(baseQuery).fetchall():
    sorFailureGroupedHltList.append((eor_reason, amount))
    print "%s: %s" % (eor_reason, amount)

sorFailureGroupedHltArray = np.array(
    sorFailureGroupedHltList, dtype=[("eor_reason", str, 20),("count", int)]
).view(np.recarray)


# eor_reason distribution first for all, then for 
# successful and then failed

# Base query for the rest
#########################
baseQuery = select(
    [
        func.SUBSTRING_INDEX(physicsWithHltAlias.c.eor_reason, ":", 1),
        func.count(physicsWithHltAlias.c.eor_reason).label("count")
    ]
).group_by(
    func.SUBSTRING_INDEX(physicsWithHltAlias.c.eor_reason, ":", 1)
).order_by(desc("count"))


print "\neor_reason for all runs (grouped)."
print "===================================="

allRunsList = []

for eor_reason, amount in engine.execute(baseQuery).fetchall():
    allRunsList.append((eor_reason, amount))
    print "%s: %s" % (eor_reason, amount)

allRunsArray = np.array(
    allRunsList, dtype=[("eor_reason", str, 20),("count", int)]
).view(np.recarray)


print "\neor_reason for successful runs (grouped)."
print "==========================================="

successfulRunsList = []

query = baseQuery.where(and_(
    physicsWithHltAlias.c.ecs_success.op("<=>")(1), 
    physicsWithHltAlias.c.daq_success.op("<=>")(1),
))

for eor_reason, amount in engine.execute(query).fetchall():
    successfulRunsList.append((eor_reason, amount))
    print "%s: %s" % (eor_reason, amount)

successfulRunsArray = np.array(
    successfulRunsList, dtype=[("eor_reason", str, 20),("count", int)]
).view(np.recarray)


print "\neor_reason for failed runs (grouped)."
print "======================================="

failedRunsList = []

query = baseQuery.where(or_(
    not_(physicsWithHltAlias.c.ecs_success.op("<=>")(1)), 
    not_(physicsWithHltAlias.c.daq_success.op("<=>")(1))
))

for eor_reason, amount in engine.execute(query).fetchall():
    failedRunsList.append((eor_reason, amount))
    print "%s: %s" % (eor_reason, amount)

failedRunsArray = np.array(
    failedRunsList, dtype=[("eor_reason", str, 20),("count", int)]
).view(np.recarray)

print "\nStoring to file: '../dataset/logbook_eor.npz'"

np.savez(
    "../dataset/logbook_eor.npz", 
    allFailureArray=allFailureArray,
    sorFailureAllArray=sorFailureAllArray,
    sorFailureGroupedArray=sorFailureGroupedArray,
    sorFailureGroupedFailedArray=sorFailureGroupedFailedArray,
    sorFailureGroupedHltArray=sorFailureGroupedHltArray,
    subsysFailureArray=subsysFailureArray,
    subsysFailureFailedArray=subsysFailureFailedArray,
    allRunsArray=allRunsArray, 
    successfulRunsArray=successfulRunsArray, 
    failedRunsArray=failedRunsArray
)
