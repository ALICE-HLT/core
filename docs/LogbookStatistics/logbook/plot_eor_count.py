import numpy as np
import matplotlib.pyplot as plt
import pylab
from matplotlib.patches import Polygon
from matplotlib.ticker import MaxNLocator


# End of run reasons for all runs
#################################
npFileDict = np.load("../dataset/logbook_eor.npz")
allRunsArray = npFileDict["allRunsArray"]

numberOfEor = len(allRunsArray)
eorNames = allRunsArray["eor_reason"]
eorCounts = allRunsArray["count"]

text = 'Number of runs grouped by end of run reason.'

fig = plt.figure()
fig.canvas.set_window_title(text)

ax1 = fig.add_subplot(111)
ax1.set_title(text)

pos = np.arange(numberOfEor)
pylab.yticks(pos, eorNames)

rects = ax1.barh(pos, eorCounts, align='center')

# Write the value beside each bar
for rect in rects:
    
    # Make integer for easier handling
    width = int(rect.get_width())
    
    # Shift the text to the right side of the right edge
    rankStr = str(width)
    xloc = width + 500
    
    #Center the text vertically in the bar
    yloc = rect.get_y()+rect.get_height()/2.0
    ax1.text(xloc, yloc, rankStr, 
        horizontalalignment='left',verticalalignment='center')

fig.savefig('../figures/eor_all.svg')


# End of run reason for successful runs
#######################################
successfulRunsArray = npFileDict["successfulRunsArray"]

numberOfEor = len(successfulRunsArray)
eorNames = successfulRunsArray["eor_reason"]
eorCounts = successfulRunsArray["count"]

text = "Number of runs grouped by end of run reason. Only successful runs."

fig = plt.figure()
fig.canvas.set_window_title(text)

ax1 = fig.add_subplot(111)
ax1.set_title(text)

pos = np.arange(numberOfEor)
pylab.yticks(pos, eorNames)

rects = ax1.barh(pos, eorCounts, align='center')

# Write the value beside each bar
for rect in rects:
    
    # Make integer for easier handling
    width = int(rect.get_width())
    
    # Shift the text to the right side of the right edge
    rankStr = str(width)
    xloc = width + 500
    
    #Center the text vertically in the bar
    yloc = rect.get_y()+rect.get_height()/2.0
    ax1.text(xloc, yloc, rankStr, 
        horizontalalignment='left',verticalalignment='center')

fig.savefig('../figures/eor_successful.svg')


# End of run reason for failed runs
###################################
failedRunsArray = npFileDict["failedRunsArray"]

numberOfEor = len(failedRunsArray)
eorNames = failedRunsArray["eor_reason"]
eorCounts = failedRunsArray["count"]

text = "Number of runs grouped by end of run reason. Only failed runs."

fig = plt.figure()
fig.canvas.set_window_title(text)

ax1 = fig.add_subplot(111)
ax1.set_title(text)

pos = np.arange(numberOfEor)
pylab.yticks(pos, eorNames)

rects = ax1.barh(pos, eorCounts, align='center')

# Write the value beside each bar
for rect in rects:
    
    # Make integer for easier handling
    width = int(rect.get_width())
    
    # Shift the text to the right side of the right edge
    rankStr = str(width)
    xloc = width + 500
    
    #Center the text vertically in the bar
    yloc = rect.get_y()+rect.get_height()/2.0
    ax1.text(xloc, yloc, rankStr, 
        horizontalalignment='left',verticalalignment='center')

fig.savefig('../figures/eor_failed.svg')


# SOR_failure grouped for failed runs
#####################################
sorFailureGroupedFailedArray = npFileDict["sorFailureGroupedFailedArray"]

numberOfEor = len(sorFailureGroupedFailedArray)
eorNames = sorFailureGroupedFailedArray["eor_reason"]
eorCounts = sorFailureGroupedFailedArray["count"]

text = "SOR_failure grouped for failed runs."

fig = plt.figure()
fig.canvas.set_window_title(text)

ax1 = fig.add_subplot(111)
ax1.set_title(text)

pos = np.arange(numberOfEor)
pylab.yticks(pos, eorNames)

rects = ax1.barh(pos, eorCounts, align='center')

# Write the value beside each bar
for rect in rects:
    
    # Make integer for easier handling
    width = int(rect.get_width())
    
    # Shift the text to the right side of the right edge
    rankStr = str(width)
    xloc = width + 500
    
    #Center the text vertically in the bar
    yloc = rect.get_y()+rect.get_height()/2.0
    ax1.text(xloc, yloc, rankStr, 
        horizontalalignment='left',verticalalignment='center')

fig.savefig('../figures/sor_failure_grouped.svg')


# Subsystem_failure for failed runs
###################################
subsysFailureFailedArray = npFileDict["subsysFailureFailedArray"]

numberOfEor = len(subsysFailureFailedArray)
eorNames = subsysFailureFailedArray["eor_reason"]
eorCounts = subsysFailureFailedArray["count"]

text = "Subsystem_failure for failed runs."

fig = plt.figure()
fig.canvas.set_window_title(text)

ax1 = fig.add_subplot(111)
ax1.set_title(text)

pos = np.arange(numberOfEor)
pylab.yticks(pos, eorNames)

rects = ax1.barh(pos, eorCounts, align='center')

# Write the value beside each bar
for rect in rects:
    
    # Make integer for easier handling
    width = int(rect.get_width())
    
    # Shift the text to the right side of the right edge
    rankStr = str(width)
    xloc = width + 500
    
    #Center the text vertically in the bar
    yloc = rect.get_y()+rect.get_height()/2.0
    ax1.text(xloc, yloc, rankStr, 
        horizontalalignment='left',verticalalignment='center')

fig.savefig('../figures/subsystem_failure.svg')


# SOR_failure grouped for HLT, failed runs
##########################################
sorFailureGroupedHltArray = npFileDict["sorFailureGroupedHltArray"]

numberOfEor = len(sorFailureGroupedHltArray)
eorNames = sorFailureGroupedHltArray["eor_reason"]
eorCounts = sorFailureGroupedHltArray["count"]

text = "SOR_failure grouped for HLT, failed runs."

fig = plt.figure()
fig.canvas.set_window_title(text)

ax1 = fig.add_subplot(111)
ax1.set_title(text)

pos = np.arange(numberOfEor)
pylab.yticks(pos, eorNames)

rects = ax1.barh(pos, eorCounts, align='center')

# Write the value beside each bar
for rect in rects:
    
    # Make integer for easier handling
    width = int(rect.get_width())
    
    # Shift the text to the right side of the right edge
    rankStr = str(width)
    xloc = width + 500
    
    #Center the text vertically in the bar
    yloc = rect.get_y()+rect.get_height()/2.0
    ax1.text(xloc, yloc, rankStr, 
        horizontalalignment='left',verticalalignment='center')

fig.savefig('../figures/sor_failure_hlt.svg')


plt.show()
