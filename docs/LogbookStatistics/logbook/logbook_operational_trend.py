import sys
sys.path.append("..")

import datetime

from sqlalchemy import func, String
from sqlalchemy.sql import select, and_, or_, not_, desc

import numpy as np

from logbook import engine, logbook
from logbook import physicsRuns, physicsWithHlt


# Needs to crete alias for use as subquery.
physicsRunsAlias = physicsRuns.alias()
physicsWithHltAlias = physicsWithHlt.alias()


print "\nTotal number of 'PHYSICS_1' runs per week."
print "============================================"
physicsAll = select(
    [
        func.YEARWEEK(func.FROM_UNIXTIME(physicsRunsAlias.c.time_created)).label("yearweek"),
        func.sum(physicsRunsAlias.c.runDuration).label("duration")
    ]
).group_by(
    func.YEARWEEK(func.FROM_UNIXTIME(physicsRunsAlias.c.time_created))
).alias()

for item in engine.execute(physicsAll):
    print item


print "\nTotal number of 'PHYSICS_1' runs with HLT per week."
print "====================================================="
physicsHlt = select(
    [
        func.YEARWEEK(func.FROM_UNIXTIME(physicsWithHltAlias.c.time_created)).label("yearweek"),
        func.sum(physicsWithHltAlias.c.runDuration).label("duration")
    ]
).group_by(
    func.YEARWEEK(func.FROM_UNIXTIME(physicsWithHltAlias.c.time_created))
).alias()

for item in engine.execute(physicsHlt):
    print item


print "\nAll PHYSICS_1 and those with HLT combined per week"
print "===================================================="
query = select(
    [
        func.STR_TO_DATE(physicsAll.c.yearweek + "Sunday", '%X%V%W'),
        physicsAll.c.duration,
        physicsHlt.c.duration,
        (physicsHlt.c.duration * 100) / physicsAll.c.duration
    ],
    physicsAll.c.yearweek == physicsHlt.c.yearweek
)

operationalCountList = []

for date, all, hlt, perc in engine.execute(query).fetchall():
    operationalCountList.append((date, int(all or 0), int(hlt or 0), int(perc or 0)))
    print date, all, hlt, perc

operationalDurationWeek = np.array(
    operationalCountList, dtype=[
        ("datetime", datetime.datetime), ("all_duration", int), ("hlt_duration", int), ("perc", int)
    ]
).view(np.recarray)


print "\nTotal number of 'PHYSICS_1' runs per month."
print "============================================"
physicsAll = select(
    [
        func.LAST_DAY(func.FROM_UNIXTIME(physicsRunsAlias.c.time_created)).label("datetime"),
        func.sum(physicsRunsAlias.c.runDuration).label("duration")
    ]
).group_by(
    func.LAST_DAY(func.FROM_UNIXTIME(physicsRunsAlias.c.time_created))
).alias()

for item in engine.execute(physicsAll):
    print item


print "\nTotal number of 'PHYSICS_1' runs with HLT per month."
print "====================================================="
physicsHlt = select(
    [
        func.LAST_DAY(func.FROM_UNIXTIME(physicsWithHltAlias.c.time_created)).label("datetime"),
        func.sum(physicsWithHltAlias.c.runDuration).label("duration")
    ]
).group_by(
    func.LAST_DAY(func.FROM_UNIXTIME(physicsWithHltAlias.c.time_created))
).alias()

for item in engine.execute(physicsHlt):
    print item



print "\nAll PHYSICS_1 and those with HLT combined per month"
print "====================================================="
query = select(
    [
        physicsAll.c.datetime,
        physicsAll.c.duration,
        physicsHlt.c.duration,
        (physicsHlt.c.duration * 100) / physicsAll.c.duration
    ],
    physicsAll.c.datetime == physicsHlt.c.datetime
)

operationalCountList = []

for month, all, hlt, perc in engine.execute(query).fetchall():
    operationalCountList.append((month, int(all or 0), int(hlt or 0), int(perc or 0)))
    print month, all, hlt, perc

operationalDurationMonth = np.array(
    operationalCountList, dtype=[
        ("datetime", datetime.datetime), ("all_duration", int), ("hlt_duration", int), ("perc", int)
    ]
).view(np.recarray)



np.savez(
    "../dataset/logbook_operational.npz",
    operationalDurationWeek=operationalDurationWeek,
    operationalDurationMonth=operationalDurationMonth
)
