from sqlalchemy import func
from sqlalchemy.sql import select, and_, desc

import numpy as np

# TODO: See if this can be avoided. Could not get 
# reltive imports to work atm.
import sys
sys.path.append("..")

from logbook import engine, logbook


print "Retrieving number of runs for the different run types."
print "======================================================"
runTypeCountList = []

query = select([
    logbook.c.run_type,
    func.count(logbook.c.run_type).label("count")
]).group_by(logbook.c.run_type).order_by(desc("count"))

for run_type, amount in engine.execute(query).fetchall():
    runTypeCountList.append((run_type, amount))
    print run_type, amount

runTypeCount = np.array(
    runTypeCountList, dtype=[("run_type", str, 20), ("count", int)]
).view(np.recarray)


print "\nRetrieving number of runs grouped by their 'super-type'."
print "========================================================"
runTypeCountList = []

query = select([
    func.SUBSTRING_INDEX(logbook.c.run_type, "_", 1),
    func.count(logbook.c.run_type).label("count")
]).group_by(
    func.SUBSTRING_INDEX(logbook.c.run_type, "_", 1)
).order_by(desc("count"))

for run_type, amount in engine.execute(query).fetchall():
    runTypeCountList.append((run_type, amount))
    print run_type, amount

runTypeCountGrouped = np.array(
    runTypeCountList, dtype=[("run_type", str, 20), ("count", int)]
).view(np.recarray)


print "\nStoring:"
print "runTypeCount, runTypeCountGrouped"
print "to file: '../dataset/logbook_runtype.npz'"

np.savez(
    "../dataset/logbook_runtype.npz", 
    runTypeCount=runTypeCount, 
    runTypeCountGrouped=runTypeCountGrouped
)
