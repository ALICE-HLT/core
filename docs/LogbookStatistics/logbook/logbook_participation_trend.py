import sys
sys.path.append("..")

import datetime

from sqlalchemy import func, String
from sqlalchemy.sql import select, and_, or_, not_, desc

import numpy as np

from logbook import engine, logbook
from logbook import physicsRuns, physicsWithHlt


# Needs to crete alias for use as subquery.
physicsRunsAlias = physicsRuns.alias()
physicsWithHltAlias = physicsWithHlt.alias()


print "\nTotal number of 'PHYSICS_1' runs per week."
print "============================================"
physicsAll = select(
    [
        func.YEARWEEK(func.FROM_UNIXTIME(physicsRunsAlias.c.time_created)).label("yearweek"),
        func.count(func.YEARWEEK(func.FROM_UNIXTIME(physicsRunsAlias.c.time_created))).label("count")
    ]
).group_by(
    func.YEARWEEK(func.FROM_UNIXTIME(physicsRunsAlias.c.time_created))
).alias()

for item in engine.execute(physicsAll):
    print item


print "\nTotal number of 'PHYSICS_1' runs with HLT per week."
print "====================================================="
physicsHlt = select(
    [
        func.YEARWEEK(func.FROM_UNIXTIME(physicsWithHltAlias.c.time_created)).label("yearweek"),
        func.count(func.YEARWEEK(func.FROM_UNIXTIME(physicsWithHltAlias.c.time_created))).label("count")
    ]
).group_by(
    func.YEARWEEK(func.FROM_UNIXTIME(physicsWithHltAlias.c.time_created))
).alias()

for item in engine.execute(physicsHlt):
    print item


print "\nAll PHYSICS_1 and those with HLT combined per week"
print "===================================================="
query = select(
    [
        func.STR_TO_DATE(physicsAll.c.yearweek + "Monday", '%X%V%W'),
        physicsAll.c.count,
        physicsHlt.c.count,
        (physicsHlt.c.count * 100) / physicsAll.c.count
    ],
    physicsAll.c.yearweek == physicsHlt.c.yearweek
)

operationalCountList = []

for date, all, hlt, perc in engine.execute(query).fetchall():
    operationalCountList.append((date, all, hlt, perc))
    print date, all, hlt, perc

operationalCountWeek = np.array(
    operationalCountList, dtype=[
        ("datetime", datetime.datetime), ("all_count", int), ("hlt_count", int), ("perc", int)
    ]
).view(np.recarray)


print "\nTotal number of 'PHYSICS_1' runs per month."
print "============================================"
physicsAll = select(
    [
        func.LAST_DAY(func.FROM_UNIXTIME(physicsRunsAlias.c.time_created)).label("datetime"),
        func.count(func.LAST_DAY(func.FROM_UNIXTIME(physicsRunsAlias.c.time_created))).label("count")
    ]
).group_by(
    func.LAST_DAY(func.FROM_UNIXTIME(physicsRunsAlias.c.time_created))
).alias()

for item in engine.execute(physicsAll):
    print item


print "\nTotal number of 'PHYSICS_1' runs with HLT per month."
print "====================================================="
physicsHlt = select(
    [
        func.LAST_DAY(func.FROM_UNIXTIME(physicsWithHltAlias.c.time_created)).label("datetime"),
        func.count(func.LAST_DAY(func.FROM_UNIXTIME(physicsWithHltAlias.c.time_created))).label("count")
    ]
).group_by(
    func.LAST_DAY(func.FROM_UNIXTIME(physicsWithHltAlias.c.time_created))
).alias()

for item in engine.execute(physicsHlt):
    print item



print "\nAll PHYSICS_1 and those with HLT combined per month"
print "====================================================="
query = select(
    [
        physicsAll.c.datetime,
        physicsAll.c.count,
        physicsHlt.c.count,
        (physicsHlt.c.count * 100) / physicsAll.c.count
    ],
    physicsAll.c.datetime == physicsHlt.c.datetime
)

operationalCountList = []

for month, all, hlt, perc in engine.execute(query).fetchall():
    operationalCountList.append((month, all, hlt, perc))
    print month, all, hlt, perc

operationalCountMonth = np.array(
    operationalCountList, dtype=[
        ("datetime", datetime.datetime), ("all_count", int), ("hlt_count", int), ("perc", int)
    ]
).view(np.recarray)



np.savez(
    "../dataset/logbook_participation.npz",
    participationCountWeek=operationalCountWeek,
    participationCountMonth=operationalCountMonth
)
