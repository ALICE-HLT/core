import numpy as np
import matplotlib.pyplot as plt
import pylab
from matplotlib.patches import Polygon
from matplotlib.ticker import MaxNLocator


# Run type count
################
npFileDict = np.load("../dataset/logbook_runtype.npz")
runTypeCount = npFileDict["runTypeCount"]

numTests = len(runTypeCount)
testNames = runTypeCount["run_type"]
rankings = runTypeCount["count"]

text = 'Number of runs grouped by type.'

fig = plt.figure(figsize=(8, 10))
fig.canvas.set_window_title(text)

ax1 = fig.add_subplot(111)
ax1.set_title(text)
ax1.autoscale = True

pos = np.arange(numTests)
pylab.yticks(pos, testNames)

rects = ax1.barh(pos, rankings, align='center')

# Write the value beside each bar
for rect in rects:
    
    # Make integer for easier handling
    width = int(rect.get_width())
    
    # Shift the text to the right side of the right edge
    rankStr = str(width)
    xloc = width + 500
    
    #Center the text vertically in the bar
    yloc = rect.get_y()+rect.get_height()/2.0
    ax1.text(xloc, yloc, rankStr, 
        horizontalalignment='left',verticalalignment='center')

fig.savefig('../figures/run_type_count.svg')


# Run type count grouped
########################
npFileDict = np.load("../dataset/logbook_runtype.npz")
runTypeCount = npFileDict["runTypeCountGrouped"]

numTests = len(runTypeCount)
testNames = runTypeCount["run_type"]
rankings = runTypeCount["count"]

text = 'Number of runs grouped by "super"" type.'

fig = plt.figure()
fig.canvas.set_window_title(text)

ax1 = fig.add_subplot(111)
ax1.set_title(text)

pos = np.arange(numTests)
pylab.yticks(pos, testNames)

rects = ax1.barh(pos, rankings, align='center')

# Write the value beside each bar
for rect in rects:
    
    # Make integer for easier handling
    width = int(rect.get_width())
    
    # Shift the text to the right side of the right edge
    rankStr = str(width)
    xloc = width + 500
    
    #Center the text vertically in the bar
    yloc = rect.get_y()+rect.get_height()/2.0
    ax1.text(xloc, yloc, rankStr, 
        horizontalalignment='left',verticalalignment='center')


fig.savefig('../figures/run_type_count_grouped.svg')

plt.show()
