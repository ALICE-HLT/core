import numpy as np
import matplotlib.pyplot as plt
import pylab
from matplotlib.patches import Polygon
from matplotlib.ticker import MaxNLocator

import matplotlib.dates as mdates


# Get the array
npFileDict = np.load("../dataset/logbook_operational.npz")

# Start with weekly dataset
operationalCount = npFileDict["operationalDurationWeek"]

# Setup the dates
years = mdates.YearLocator()
weeks = mdates.WeekdayLocator()
yearsFmt = mdates.DateFormatter('%Y')

# Absolute numbers. Weekly.
###########################
text = "Amount of run-time with HLT participation (weekly)."

fig = plt.figure(figsize=(12,8))
fig.canvas.set_window_title(text)

ax = fig.add_subplot(111)
ax.set_title(text)
    
ax.plot(operationalCount["datetime"], 
    operationalCount["all_duration"], "o-", label="total time")
ax.plot(operationalCount["datetime"], 
    operationalCount["hlt_duration"], "o-", label="time with hlt")

ax.legend(loc='upper left', fancybox=True, shadow=True)

# Format the ticks
ax.xaxis.set_major_locator(years)
ax.xaxis.set_major_formatter(yearsFmt)
ax.xaxis.set_minor_locator(weeks)

ax.format_xdata = mdates.DateFormatter('%Y')
ax.grid(True)

fig.autofmt_xdate()

fig.savefig('../figures/logbook_operational_week.svg')


# Percentage. Weekly.
#####################
text = "Percentage of run-time with HLT participation (weekly)."

fig = plt.figure(figsize=(12,8))
fig.canvas.set_window_title(text)

ax = fig.add_subplot(111)
ax.set_title(text)
    
ax.plot(operationalCount["datetime"], operationalCount["perc"], "o-", label="%")

# Format the ticks
ax.xaxis.set_major_locator(years)
ax.xaxis.set_major_formatter(yearsFmt)
ax.xaxis.set_minor_locator(weeks)

ax.format_xdata = mdates.DateFormatter('%Y')
ax.grid(True)

fig.autofmt_xdate()

fig.savefig('../figures/logbook_operational_week_perc.svg')




# Change to monthly dataset
operationalCount = npFileDict["operationalDurationMonth"]

# Setup the dates
years = mdates.YearLocator()
months = mdates.MonthLocator()
yearsFmt = mdates.DateFormatter('%Y')
monthsFmt = mdates.DateFormatter('%b')

plt.rc(('xtick.major'), pad=20)

# Absolute numbers. Monthly.
############################
text = "Amount of run-time with HLT participation (monthly)."

fig = plt.figure(figsize=(12,8))
fig.canvas.set_window_title(text)

ax = fig.add_subplot(111)
ax.set_title(text)
    
ax.plot(operationalCount["datetime"], 
    operationalCount["all_duration"], "o-", label="total time")
ax.plot(operationalCount["datetime"], 
    operationalCount["hlt_duration"], "o-", label="time with hlt")

ax.legend(bbox_to_anchor=(0.05, 1.0), loc='upper left', ncol='2', fancybox=True, shadow=True)

# Format the ticks
ax.xaxis.set_major_locator(years)
ax.xaxis.set_major_formatter(yearsFmt)
ax.xaxis.set_minor_locator(months)
ax.xaxis.set_minor_formatter(monthsFmt)

ax.format_xdata = mdates.DateFormatter('%m')
ax.grid(True)

fig.autofmt_xdate()

fig.savefig('../figures/logbook_operational_month.svg')


# Percentage. Monthly.
######################
text = "Percentage of run-time with HLT participation (monthly)."

fig = plt.figure(figsize=(12,8))
fig.canvas.set_window_title(text)

ax = fig.add_subplot(111)
ax.set_title(text)
    
ax.plot(operationalCount["datetime"], operationalCount["perc"], "o-", label="%")

# Format the ticks
ax.xaxis.set_major_locator(years)
ax.xaxis.set_major_formatter(yearsFmt)
ax.xaxis.set_minor_locator(months)
ax.xaxis.set_minor_formatter(monthsFmt)



ax.format_xdata = mdates.DateFormatter('%m')
ax.grid(True)

fig.autofmt_xdate()

fig.savefig('../figures/logbook_operational_month_perc.svg')


plt.show()
