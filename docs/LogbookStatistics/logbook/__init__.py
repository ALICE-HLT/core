"""
 logbook module
 
 Common code for all the scripts that are used to gather
 information from the logbook
"""

from sqlalchemy import create_engine, MetaData
from sqlalchemy import Table, Column, TIMESTAMP
from sqlalchemy.sql import select, not_, or_, and_

connectionUrl = "mysql://hlt:hlt123@alice-logbook-copy.cern.ch/logbook"
engine = create_engine(connectionUrl, echo=False)

meta = MetaData()
meta.bind = engine

# Could also use SqlSoup for simple access:
#from sqlalchemy.ext.sqlsoup import SqlSoup
#db = SqlSoup(engine)
#print "Number of entries: ", db.logbook.count()


# Override columns to set suitable python types.
# Note that time_update has different type than the other
# time related columns.
logbook = Table('logbook', meta, 
    Column('DAQ_time_start', TIMESTAMP), 
    Column('DAQ_time_end', TIMESTAMP), 
    Column('time_created', TIMESTAMP), 
    Column('time_completed', TIMESTAMP),
    autoload=True)

meta.create_all()

# Select only the columns needed.
allRuns = select([
    logbook.c.daq_success,
    logbook.c.ecs_success,
    logbook.c.eor_reason,
    logbook.c.HLTmode,
    logbook.c.partition,
    logbook.c.runDuration,
    # time_created seems to be the most reliable timestamp.
    logbook.c.time_created
])

# Select all columns for runs paricipating in the PHYSICS_1 partition
physicsRuns = allRuns.where(logbook.c.partition == "PHYSICS_1")


# Select runs that hlt has been paricipating in 
# (everything that is not 'A' or 'Unknown')
physicsWithHlt = physicsRuns.where(
    not_(
        or_(
            (logbook.c.HLTmode == u'Unknow'),
            (logbook.c.HLTmode == u'A')
        )
    )
)


# Select successful runs. Succesful runs are defined as those who have
# both of "ecs_success" and "daq_success" flags as true:
#       (daq_success == 1 and ecs_success == 1)
physicsWithHltSuccessful = physicsWithHlt.where(
    and_(
        # Need to use NULL-safe equality operator: "<=>" to get it right.
        # http://dev.mysql.com/doc/refman/5.5/en/comparison-operators.html#operator_equal-to
        logbook.c.ecs_success.op("<=>")(1), 
        logbook.c.daq_success.op("<=>")(1)
    )
)


# Select failed runs. Failed runs are defined as those who have either 
# or both of "ecs_success" and "daq_success" flags as false:
#       ((NOT daq_success == 1) or (NOT ecs_success == 1))
physicsWithHltFailed = physicsWithHlt.where(
    and_(
        or_(
            # Need to use NULL-safe equality operator: "<=>" to get it right.
            # http://dev.mysql.com/doc/refman/5.5/en/comparison-operators.html#operator_equal-to
            not_(logbook.c.ecs_success.op("<=>")(1)), 
            not_(logbook.c.daq_success.op("<=>")(1))
        )
    )
)
