import numpy as np
import matplotlib.pyplot as plt
import pylab
from matplotlib.patches import Polygon
from matplotlib.ticker import MaxNLocator

import matplotlib.dates as mdates


# Get the array
npFileDict = np.load("../dataset/logbook_eor_trend.npz")
eorTrendArray = npFileDict["eorTrendArray"]


# Setup the dates
#months = mdates.MonthLocator()
years = mdates.YearLocator()
#days = mdates.DayLocator()
weeks = mdates.WeekdayLocator()
yearsFmt = mdates.DateFormatter('%Y')

fig = plt.figure()
ax = fig.add_subplot(111)

#print eorTrendArray.shape

for name in eorTrendArray.dtype.names[1:]:
    print name
    ax.plot(eorTrendArray["timestamp"], eorTrendArray[name], label=name)

#ax.legend()

# Format the ticks
ax.xaxis.set_major_locator(years)
ax.xaxis.set_major_formatter(yearsFmt)
ax.xaxis.set_minor_locator(weeks)

ax.format_xdata = mdates.DateFormatter('%Y%W')
ax.grid(True)

fig.autofmt_xdate()

#fig.savefig('../../figures/logbook_eor_trend.svg')

plt.show()
