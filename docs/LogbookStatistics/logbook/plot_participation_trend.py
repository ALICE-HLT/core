import numpy as np
import matplotlib.pyplot as plt
import pylab
from matplotlib.patches import Polygon
from matplotlib.ticker import MaxNLocator

import matplotlib.dates as mdates


# Get the array
npFileDict = np.load("../dataset/logbook_participation.npz")

# Start with weekly dataset
operationalCount = npFileDict["operationalCountWeek"]


# Setup the dates
years = mdates.YearLocator()
weeks = mdates.WeekdayLocator()
yearsFmt = mdates.DateFormatter('%Y')

# Absolute numbers. Weekly.
###########################
text = "Number of runs with HLT participation (weekly)."

fig = plt.figure()
fig.canvas.set_window_title(text)

ax = fig.add_subplot(111)
ax.set_title(text)
    
ax.plot(operationalCount["datetime"], 
    operationalCount["all_count"], "o-", label="all runs")
ax.plot(operationalCount["datetime"], 
    operationalCount["hlt_count"], "o-", label="runs with hlt")

ax.legend(loc='upper center', ncol='2', fancybox=True, shadow=True)

# Format the ticks
ax.xaxis.set_major_locator(years)
ax.xaxis.set_major_formatter(yearsFmt)
ax.xaxis.set_minor_locator(weeks)

ax.format_xdata = mdates.DateFormatter('%Y')
ax.grid(True)

fig.autofmt_xdate()

fig.savefig('../figures/logbook_participation_week.svg')


# Percentage. Weekly.
#####################
text = "Percentage of runs with HLT participation (weekly)."

fig = plt.figure()
fig.canvas.set_window_title(text)

ax = fig.add_subplot(111)
ax.set_title(text)
    
ax.plot(operationalCount["datetime"], operationalCount["perc"], "o-", label="%")

# Format the ticks
ax.xaxis.set_major_locator(years)
ax.xaxis.set_major_formatter(yearsFmt)
ax.xaxis.set_minor_locator(weeks)

ax.format_xdata = mdates.DateFormatter('%Y')
ax.grid(True)

fig.autofmt_xdate()

fig.savefig('../figures/logbook_participation_week_perc.svg')




# Change to monthly dataset
operationalCount = npFileDict["operationalCountMonth"]

# Setup the dates
months = mdates.MonthLocator()
years = mdates.YearLocator()
yearsFmt = mdates.DateFormatter('%Y')

# Absolute numbers. Monthly.
############################
text = "Number of runs with HLT participation (monthly)."

fig = plt.figure()
fig.canvas.set_window_title(text)

ax = fig.add_subplot(111)
ax.set_title(text)
    
ax.plot(operationalCount["datetime"], 
    operationalCount["all_count"], "o-", label="all runs")
ax.plot(operationalCount["datetime"], 
    operationalCount["hlt_count"], "o-", label="runs with hlt")

ax.legend(loc='upper center', ncol='2', fancybox=True, shadow=True)

# Format the ticks
ax.xaxis.set_major_locator(years)
ax.xaxis.set_major_formatter(yearsFmt)
ax.xaxis.set_minor_locator(months)

ax.format_xdata = mdates.DateFormatter('%Y%m')
ax.grid(True)

fig.autofmt_xdate()

fig.savefig('../figures/logbook_participation_month.svg')


# Percentage. Monthly.
######################
text = "Percentage of runs with HLT participation (monthly)."

fig = plt.figure()
fig.canvas.set_window_title(text)

ax = fig.add_subplot(111)
ax.set_title(text)
    
ax.plot(operationalCount["datetime"], operationalCount["perc"], "o-", label="%")

# Format the ticks
ax.xaxis.set_major_locator(years)
ax.xaxis.set_major_formatter(yearsFmt)
ax.xaxis.set_minor_locator(months)

ax.format_xdata = mdates.DateFormatter('%Y%m')
ax.grid(True)

fig.autofmt_xdate()

fig.savefig('../figures/logbook_participation_month_perc.svg')


plt.show()
