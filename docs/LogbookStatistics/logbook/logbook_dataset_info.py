from sqlalchemy import func
from sqlalchemy.sql import select, and_

# TODO: should not be needed..
import sys
sys.path.append("..")

from logbook import engine, logbook


# Dataset information
baseQuery = select([func.count(logbook.c.run)])
print "Number of entries:\t%s" % engine.execute(baseQuery).first()[0]


baseQuery = select([
    logbook.c.run,
    func.TIMESTAMP(func.FROM_UNIXTIME(logbook.c.DAQ_time_start)),
    func.TIMESTAMP(func.FROM_UNIXTIME(logbook.c.DAQ_time_end)),
    logbook.c.runDuration,
    logbook.c.eor_reason
])

# Consistency?
query = baseQuery.order_by(logbook.c.run).limit(100)
run, start, end, duration, reason = engine.execute(query).first()
print "Start of first run (%s) number:\t%s" % (run, start)


query = baseQuery.order_by(logbook.c.run.desc()).limit(100)
run, start, end, duration, reason = engine.execute(query).first()
print "End of last run (%s) number:\t%s" % (run, end)


# Run number for the first dates?
query = baseQuery.where(logbook.c.DAQ_time_start != None)
query = query.order_by(logbook.c.DAQ_time_start).limit(100)
run, start, end, duration, reason = engine.execute(query).first()
print "Run number for first datetime (%s - %s = %s):\t%s" % (start, end, duration, run)


# Run number for the last dates?
query = baseQuery.where(logbook.c.DAQ_time_end != None)
query = query.order_by(logbook.c.DAQ_time_end.desc()).limit(100)
run, start, end, duration, reason = engine.execute(query).first()
print "Run number for last datetime (%s - %s = %s):\t%s" % (start, end, duration, run)
