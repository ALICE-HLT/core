"""
 Listing all tables and all columns with their type.
"""

from sqlalchemy import create_engine, MetaData

engine = create_engine('mysql://hlt:hlt123@alice-logbook-copy.cern.ch/logbook')

# Reflects all in one go without overriding anything.
meta = MetaData()
meta.bind = engine

meta.reflect()

print "TABLES IN THE DATABASE:"
print "======================="
for t in meta.tables.keys():
    print t

logbook = meta.tables["logbook"]

for table in meta.tables:
    print "\nCOLUMNS IN THE %s TABLE:" % table
    print "=========================="
    for c in meta.tables[table].columns:
        print c, c.type
