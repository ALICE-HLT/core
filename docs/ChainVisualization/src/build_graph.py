import sys
from processlist import Host, Process, ProcessListParser

def main():
    try:
        from pygraph.classes.graph import graph
        from pygraph.classes.digraph import digraph
        from pygraph.algorithms.searching import breadth_first_search
        from pygraph.readwrite.dot import write
    except ImportError, e:
        print "Could not import python-graph. Please install it with your package manager."
        print "You can also find it here: http://code.google.com/p/python-graph/"
        sys.exit()
    
    # Setup the graph
    plGraph = graph()
    
    # Parse the process list
    parser = ProcessListParser()
    hosts = parser.getHosts()
    
    p = pub = sub = 0
    
    # Add all processes as nodes to the graph
    for host in hosts:
        p += len(host.processes)
        for proc in host.processes:
            plGraph.add_node(proc.name[0].replace("-", "_"))
            
            pub += len(proc.publisher)
            sub += len(proc.subscribe)
    
    print "# of processes: %d" % p
    print "# of publisher: %d" % pub
    print "# of subscribe: %d" % sub
    
    #pl = ProcessList(hosts)
    
    c = 0
    
    # Connect subscribe and publisher
    # subscribe[1] -> publisher[0]
    for host in hosts:
        for proc in host.processes:
            
            for publisher in parser.findSubscribedPublishers(proc):
                
                c+=1
                
                edge = (
                    proc.name[0].replace("-", "_"), 
                    publisher.name[0].replace("-", "_")
                )
                
                if not plGraph.has_edge(edge):
                    plGraph.add_edge(edge)
                
                # TODO: Report on any problems here
                #else:
                    #print "Edge already exist."
    
    print "# of edges form subscribe to existing publisher: %d" % c
    
    c = 0
    # Connect bridgeheads
    # pbh.msg[1] -> sbh.msg[0]
    for host in hosts:
        for proc in host.processes:
            if proc.type == "pbh":
                
                for sbh in parser.findSubscribeBridgeHeads(proc):
                    
                    c += 1
                    
                    edge = (
                        proc.name[0].replace("-", "_"), 
                        sbh.name[0].replace("-", "_")
                    )
                    
                    if not plGraph.has_edge(edge):
                        plGraph.add_edge(edge)
                    
                    # TODO: Report on any problems here
                    #else:
                        #print "Edge already exist."
    
    print "# of edges from pbh to sbh: %s" % c
    
    
    ## Normal graph
    #dot = write(plGraph)
    
    # Graph with DAQ as root
    plGraph.add_node("DAQ")
    plGraph.add_edge(("DAQ", "ALICE_HOWS-fephltout0-000".replace("-", "_")))
    plGraph.add_edge(("DAQ", "ALICE_HOWS-fephltout1-000".replace("-", "_")))
    plGraph.add_edge(("DAQ", "ALICE_HOWS-fephltout1-001".replace("-", "_")))
    plGraph.add_edge(("DAQ", "ALICE_HOWS-fephltout2-000".replace("-", "_")))
    
    st0, order = breadth_first_search(plGraph, root="DAQ")
    
    # Grpah with one OutputWriter as root
    #st0, order = breadth_first_search(
        #plGraph, 
        #root="ALICE_HOWS-fephltout0-000".replace("-", "_")
    #)
    
    gst = digraph()
    gst.add_spanning_tree(st0)
    dot = write(gst)
    
    filename = "test.dot"
    
    f = open(filename, "w")
    f.write(dot)
    f.close()
    
    print "Graph written to dot file: %s" % filename
    
    print "It can now be opened by xdot: '$ python xdot.py %s'" % filename


if __name__ == "__main__":
    main()
    
    #plGraph.add_node("ALICE")
    #for host in hosts:
        #for proc in host.processes:
            #if "RORCPublisher" in proc.binary:
                #plGraph.add_edge(("ALICE", proc.name[0].replace("-", "_")))

    #st1, order = breadth_first_search(plGraph, root="ALICE")