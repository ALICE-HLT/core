"""
DOCUMENTATION
=============
-subscribe, publisher?, -msg, -blobmsg all take pairs of arguments, 
that are identifiers for the communication, the first is the local 
to the process, the next is the remote reference.

-subscribe and -publisher are for communication within a node?
and  -msg, -blobmsg are for communication between nodes.
"""
import re

DEFAULT_FILENAME = "ALICE-ProcessList.out"

# Lines with nodename can have alpha, numbers and "-". Ends with a colon.
hostRegexp = re.compile("^[a-z,0-9,\-]*:")

# Lines with processes starts with 6 or 7 spaces and is followed by 2-3 characters
# and a colon.
processRegexp = re.compile("^[ ]{6}.*$")

# Types of processes
#processTypes = ["src", "es", "prc", "eg", "sbh", "pbh", "em", "snk", "tf"]

class Host:
    """
    Stores the name of the host and a list of all its processes.
    """
    name = None
    def __init__(self, name):
        self.name = name
        self.processes = []


class Process:
    """
    TODO: Define all allowable options for this object and check that 
    all member variables that should be set is set and none other 
    when done parsing the supplied processStrig.
    
    TODO: Could make a base class with common member variables/command-line 
    options for all process types and then add to these in sub-classes.
    """
    def __init__(self, procType, binary, options):
        """
        Parse the process string and make the options member variables 
        of the object.
        """
        
        self.type = procType
        self.binary = binary
        
        self.subscribe = []
        self.publisher = []
        
        for key, value in options.iteritems():
            setattr(self, key, value)
    
    def __repr__(self):
        return "%s(%r)" % (self.__class__, self.__dict__)


class ProcessListParser:
    """
    Parse the process list
    
    Make dictionaries for cheap lookup of mapping between hosts and processes?
    """    
    def __init__(self, filename = DEFAULT_FILENAME):
        
        self.hosts = []         # List of hosts
        self.duplicates = []    # List of duplicates
        
        # Total file size of ProcessList file
        self.totalFileSize = 0
        
        # Reference to the current host when parsing.
        currentHost = None
        
        for line in open(filename):
            
            self.totalFileSize += len(line)
            
            if hostRegexp.match(line):
                # We have a new host
                hostName = line.split(":")[0]
                currentHost = Host(hostName)        # Change active host
                self.hosts.append(currentHost)      # Add to list of hosts
                
            elif processRegexp.match(line):
                # We have a process
                

                
                # Handle component type and name of the binary first.
                commandString = line.strip().split()
                
                procType = commandString[0].strip()[:-1]
                binary = commandString[1].strip()
                
                # Join the remaining back together and ignore parenthesis (for now..)
                # Adding a space in front to allow for splitting on " -" in parseOptionString
                optionString = " " + " ".join(commandString[2:]).replace("(", "").replace(")", "")
                
                # Could do something like this instead:
                #startIdx = line.index("-ctptriggerclassgroup")
                #endIdx = line.index(")")
                #process["ctptriggerclassgroup"] = line[startIdx:endIdx + 1]
                #line = line[0:startIdx] + line[endIdx + 1:]
                
                options, retDuplicates = self.parseOptionString(optionString)
                
                if len(retDuplicates) > 0:
                    self.duplicates.append(retDuplicates)
                
                process = Process(procType, binary, options)
                currentHost.processes.append(process)               # Add to current host
    
    
    def parseOptionString(self, optStr):
        
        options = {}
        duplicates = {}
        
        for opt in optStr.split(" -")[1:]:
        
            tmp = opt.split()
            key = tmp[0]
            values = tmp[1:]
            
            if key[0] == "-":
                print "TODO: raise exception"
            
            if not options.has_key(key):
                options[key] = []
            
            for v in values:
                if not v in options[key]:
                    options[key].append(v)
                # Slot options can have same value
                elif key == "slot":
                    options[key].append(v)
                else:
                    if not duplicates.has_key(key):
                        duplicates[key] = []
                    duplicates[key].append(v)
        
        return options, duplicates
    
    def getHosts(self):
        return self.hosts
    
    def getDuplicates(self):
        return self.duplicates
    
    def getTotalFileSize(self):
        return self.totalFileSize
    
    def findSubscribedPublishers(self, process):
        
        publisherProcesses = []
        
        # TODO: first loop could probably be avoided; 
        # there should only be one subscription?
        for s in process.subscribe:
            
            for host in self.hosts:
                for proc in host.processes:
                    if s in proc.publisher:
                        publisherProcesses.append(proc)
        
        return publisherProcesses
    
    
    def findSubscribeBridgeHeads(self, pbh):
        """
        Match local address of sbh with remote of pbh
        """
        sbhProcesses = []
        
        for host in self.hosts:
            for sbh in host.processes:
                if sbh.type == "sbh":
                    if sbh.msg[0] == pbh.msg[1]:
                        sbhProcesses.append(sbh)
        
        return sbhProcesses
        
