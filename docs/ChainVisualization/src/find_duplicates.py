from processlist import Host, Process, ProcessListParser

def main():
    parser = ProcessListParser()
    
    print "Printing duplicates:"
    
    dupSize = 0
    dupOptions = {}
    for dl in parser.getDuplicates():
        print dl
        for k in dl.keys():
            if not dupOptions.has_key(k):
                dupOptions[k] = 0
            dupOptions[k] += 1
        for v in dl.values():
            # Everything is a list for now...
            for i in v:
                dupSize += len(i)
    
    totalFileSize = parser.getTotalFileSize()
    
    print "\nNumber of duplicate options:"
    for k, v in dupOptions.iteritems():
        print k, ": ", v
    
    print "\nTotal size: %s" % totalFileSize
    print "Size of duplicates: %s" % dupSize

    print "Percentage of duplicates: ", (dupSize * 100)/totalFileSize

if __name__ == "__main__":
    main()
