#!/bin/bash
#xsltproc masterConfig.xslt $1.xml
xsltproc graphConvert.xslt $1.xml | tee $1.dot | dot -Tsvg -o$1.svg
#xsltproc masterConvert.xslt $1.xml | dot -Tpng -o$1.png

# Generate DDL Listing
grep ddlid $1.xml | awk -F'-ddlid ' '{ print $2 }' | awk -F'<' '{ print $1 }' | awk -F' ' '{ print $1 }' | sort > $1.ddl

# Generate Component Listing
echo "<components>" > $1.cmp
egrep -e 'Proc|Cmd|Shm' $1.xml >> $1.cmp
echo "</components>" >> $1.cmp

# Transformations for Firefox SVG
sed -e 's/font-size:8.00/font-size:8px; font-weight: bold;/g' $1.svg > $1.svg.bak
cp $1.svg.bak $1.svg
rm $1.svg.bak
