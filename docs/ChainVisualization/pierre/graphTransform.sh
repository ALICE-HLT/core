#!/bin/bash

function movePoint {
X=${1%,*};
Y=${1#*,};
X=`echo "scale=4; $X+3" | bc`;
Y=`echo "scale=4; $Y+3" | bc`;
echo "$X,$Y";
}

# MAIN FUNCTION
cat $1.svg |
{
while read LINE ; do
	if [ "${LINE:1:7}" == "polygon" ]; then
	
		CORDS=`echo $LINE | awk -F\" '{ print $4 }'`;

		NEWPOLY="<polygon style=\"fill:gray\"";
		NEWPOLY=$NEWPOLY" points=\"";
		I=0;
		for point in $CORDS; do
			NEWPOLY+=$(movePoint $point)" ";
			let I=$I+1;
		done
		NEWPOLY=$NEWPOLY"\"/>";

                echo $LINE >> $1.bak;
		if [ $I -eq 5 ]; then
#			echo $NEWPOLY >> $1.bak;
echo ""
		fi

	else
		# add definitions and javascript
		if [ "${LINE:0:14}" == '<g id="graph0"' ]; then
			echo '<defs>' >> $1.bak;
			echo '<linearGradient id="gradient1" x1="0%" y1="0%" x2="100%" y2="100%"><stop offset="0%" stop-color="white" /><stop offset="100%" stop-color="#AAAAAA" /></linearGradient>' >> $1.bak;
			echo '<linearGradient id="gradient2" x1="50%" y1="0%" x2="50%" y2="100%"><stop offset="0%" stop-color="white" /><stop offset="100%" stop-color="#AAAAAA" /></linearGradient>' >> $1.bak;
			echo '</defs>' >> $1.bak;
		fi
		echo $LINE >> $1.bak;
	fi
done
}

cat $1.bak | sed -e 's/fill:#123456;/fill:url(#gradient1);/g' > $1.bak2
cat $1.bak2 | sed -e 's/fill:#f0f0f0;/fill:url(#gradient2);/g' > $1.svg
rm $1.bak $1.bak2
