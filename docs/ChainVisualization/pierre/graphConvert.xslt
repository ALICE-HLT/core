<xsl:stylesheet version = '1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>

<xsl:output method="text"/>

<xsl:template match="/">
<xsl:variable name="detector" select="/SimpleChainConfig1/@ID"/>
digraph G {
	rankdir=TB;
	ranksep=4.0;
	node [fontsize=8];
	node [shape=box, color=gray, style="filled,setlinewidth(2)", fillcolor="#123456"];

	subgraph "cluster_DDLs" {
		label = "DDL's";
		color = gray;
		style = filled;
		fillcolor = "#F0F0F0";
		URL = "javascript:void(window.open('TaskmanagerGraphProvider?detector=<xsl:value-of select="$detector"/>&amp;ddl=overview','_blank','scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=400,width=200,innerHeight=400,innerWidth=200'))";

		node [fontsize=9];
		<xsl:for-each select="/*/Proc">
			<xsl:variable name="DQUOTE">"</xsl:variable>
			<xsl:variable name="cmd" select="translate(Cmd,$DQUOTE,'``')"/>

			<xsl:if test="number(substring-after($cmd,' -ddlid '))">
			"<xsl:value-of select="substring-after($cmd,' -ddlid ')"/>" [color=orange, URL="javascript:void(window.open('TaskmanagerGraphProvider?detector=<xsl:value-of select="$detector"/>&amp;ddl=<xsl:value-of select="substring-after($cmd,' -ddlid ')"/>','_blank','scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=160,width=300,innerHeight=160,innerWidth=300'))"]
			</xsl:if>
		</xsl:for-each>
	}

	<xsl:for-each select="/*/Node">
		subgraph "cluster_<xsl:value-of select="@ID"/>" {
			label = "<xsl:value-of select="@hostname"/>";
			color = gray;
			style = filled;
			fillcolor = "#F0F0F0";
			URL = "javascript:void(window.open('TaskmanagerGraphProvider?node=<xsl:value-of select="@hostname"/>','_blank','toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=160,width=300,innerHeight=160,innerWidth=300'))";

		<xsl:variable name="currentNode" select="@ID"/>
		
		<xsl:for-each select="/*/Proc">
			<xsl:if test="Node=$currentNode">
				<xsl:variable name="DQUOTE">"</xsl:variable>
				<xsl:variable name="cmd" select="translate(Cmd,$DQUOTE,'``')"/>

				<xsl:variable name="bcolor">
  				<xsl:choose>
					<xsl:when test="contains($cmd,'RORCPublisher')">blue</xsl:when>
					<xsl:when test="contains($cmd,'ClusterFinder')">green</xsl:when>
					<xsl:when test="contains($cmd,'SliceTracker')">darkred</xsl:when>
					<xsl:when test="contains($cmd,'GlobalMerger')">darkviolet</xsl:when>
					<xsl:when test="contains($cmd,'FilePublisher')">purple</xsl:when>
					<xsl:when test="contains($cmd,'EsdCo')">SaddleBrown</xsl:when>
					<xsl:when test="contains($cmd,'Dump')">yellow</xsl:when>
					<xsl:otherwise>gray</xsl:otherwise>
				</xsl:choose>
				</xsl:variable>

				<!-- Component Description -->
				"<xsl:value-of select="@ID"/>" [color=<xsl:value-of select="$bcolor"/>, URL="javascript:void(window.open('TaskmanagerGraphProvider?detector=<xsl:value-of select="$detector"/>&amp;component=<xsl:value-of select="@ID"/>','_blank','scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable=no,height=600,width=500,innerHeight=600,innerWidth=500'))", tooltip="<xsl:value-of select="translate(translate(Cmd,'&#10;',' '),$DQUOTE,'``')"/>"]

				<!-- DDL Links -->
				<xsl:if test="contains($cmd,' -ddlid ')">
					"<xsl:value-of select="substring-after($cmd,' -ddlid ')"/>" -> "<xsl:value-of select="@ID"/>" [color=orange]
				</xsl:if>

				<!-- DataTransport -->
				<xsl:for-each select="Parent">
					<xsl:variable name="parent" select="."/>
                                	<xsl:variable name="lcolor">
                               	 	<xsl:choose>
                                	        <xsl:when test="contains(/*/Proc[@ID=$parent]/Cmd,'RORCPublisher')">blue</xsl:when>
						<xsl:when test="contains(/*/Proc[@ID=$parent]/Cmd,'ClusterFinder')">green</xsl:when>
						<xsl:when test="contains(/*/Proc[@ID=$parent]/Cmd,'SliceTracker')">darkred</xsl:when>
						<xsl:when test="contains(/*/Proc[@ID=$parent]/Cmd,'GlobalMerger')">darkviolet</xsl:when>
						<xsl:when test="contains(/*/Proc[@ID=$parent]/Cmd,'FilePublisher')">purple</xsl:when>
						<xsl:when test="contains(/*/Proc[@ID=$parent]/Cmd,'EsdCo')">SaddleBrown</xsl:when>
						<xsl:when test="contains(/*/Proc[@ID=$parent]/Cmd,'Dump')">yellow</xsl:when>
                	                        <xsl:otherwise>gray</xsl:otherwise>
        	                        </xsl:choose>
	                                </xsl:variable>
					"<xsl:value-of select="."/>" -> "<xsl:value-of select="../@ID"/>" [color=<xsl:value-of select="$lcolor"/>] 
				</xsl:for-each>
			</xsl:if>
		</xsl:for-each>

		}
	</xsl:for-each>
}
</xsl:template>

</xsl:stylesheet>

