#!/bin/bash

# USAGE WITH HLT REPOS
# ====================
#
# This script will create latex files and pdf files from the lyx sources.
#
# To use it with the HLT repository, do:
# ./publish_documents.sh
#
# It might be cleaned up one day...
#
# ORIGINAL DOCS
# =============
# Give the repos to get the files from and the destination where the result should be stored:
#
# $ publish_documents.sh file:///var/git/public/documentation /var/www/org/flekke/documentation
#
# For running locally:
# $ publish_documents.sh file://.


# Server settings
# If run on this machine, then upload to web dir
server="sev"
repos=$1
dest_dir=$2

# General settings
work_dir="/tmp/create_documents"

source_dir=`pwd`

if [ `hostname` != $server ]
then
	dest_dir=$work_dir/dest_dir
	repos="file://`pwd`"
fi

function SETUP {
	mkdir -p $work_dir
	cd $work_dir
	mkdir -p output
}

function CLEANUP {
	rm -fr $work_dir
}

function CLONE_REPOS {
	cd $work_dir
	git clone $repos
}


function GENERATE_LATEX {
	doc=$1
	docBase=`basename $doc .lyx`

#	cd $work_dir
	
	# Generate xhtml
#	lyx -batch --export xhtml $doc

	# Generate pdf
	lyx -batch --export pdflatex $doc
	
	pushd `dirname $doc`
	# Needt to do this twice to be able to generate toc and more.
	pdflatex -output-directory $work_dir/output $docBase.tex
	pdflatex -output-directory $work_dir/output $docBase.tex
	popd
}

function GENERATE_DOCBOOK {
	doc=$1
	docBase=`dirname $doc`/`basename $doc .lyx`
	cd $work_dir
	echo "Generating docbook for: $doc"
	lyx -batch --export docbook-xml $doc
	ls
	echo xmllint --noout $docBase.xml

	echo "Create xhtml"
	xsltproc --output $work_dir/output/$docBase.xhtml /usr/share/sgml/docbook/xsl-stylesheets/xhtml-1_1/docbook.xsl $docBase.xml

	echo "Create pdf"
	xmlroff --out-file=$work_dir/output/$docBase.pdf --format=pdf --backend=cairo $docBase.xml /usr/share/sgml/docbook/xsl-stylesheets/fo/docbook.xsl
}

SETUP

#CLONE_REPOS

cd $source_dir

pwd

for doc in $( find . -name '*.lyx' )
do
	textclass=`grep textclass ${doc} | cut -d" " -f2`
	if [ ${textclass} == "article" ] || [ ${textclass} == "scrartcl" ]
	then
		echo "We have a latex document."
		GENERATE_LATEX ${doc}
	elif [ ${textclass} == "docbook" ]
	then
		echo "We have a docbook document"
		GENERATE_DOCBOOK ${doc}
	else
		echo "Unknown textclass: ${textclass} in ${doc}"
	fi
done


if [ `hostname` == $server ]
then
	# Remove any existing files in destination
	find $dest_dir/ -type f -delete

	# Copy files to destination
	find $work_dir/output -name '*.pdf' -exec mv {} $dest_dir \;
	find $work_dir/output -name '*.xhtml' -exec mv {} $dest_dir \;

	CLEANUP
fi

